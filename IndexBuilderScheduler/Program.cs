﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IndexBuilderScheduler
{
    class Program
    {
        static void Main()
        {
            ServiceController service = null;
            try
            {
                //service = new ServiceController("TrainingQAIncrementalUpdateService"); This name is for OLD\Training QA
                service = new ServiceController("IncrementalUpdateService");
                if(service.Status.Equals(ServiceControllerStatus.Running))
                {
                    service.Stop();
                }
                
                Console.WriteLine("Indexbuilder Scheduler Started at " + DateTime.Now.ToString());
                
                ServiceConsumer objBuilder = new ServiceConsumer();                
                objBuilder.RebuildGoogleSearchIndex();              
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
            finally 
            {
                //service = new ServiceController("TrainingQAIncrementalUpdateService"); This name is for OLD\Training QA
                service = new ServiceController("IncrementalUpdateService");
                if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                    service.Start();
            }               
        }
    }
}
