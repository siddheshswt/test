﻿// ***********************************************************************
// Assembly         : SSOTenaService
// Author           : Basantakumar Sahoo
// Created          : 01-29-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-25-2015
// ***********************************************************************
// <copyright file="SSOToken.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SSOTenaService
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;

    /// <summary>
    /// SSO Token Class
    /// </summary>
    public class SSOToken
    {
        /// <summary>
        /// The error description
        /// </summary>
        protected StringBuilder errorDescription = new StringBuilder();
        /// <summary>
        /// The key
        /// </summary>
        protected string key = ConfigurationManager.AppSettings.Get("aesKey");
        /// <summary>
        /// The iv
        /// </summary>
        protected string iv = ConfigurationManager.AppSettings.Get("aesIV");
        /// <summary>
        /// The error URL
        /// </summary>
        protected string errorURL = ConfigurationManager.AppSettings.Get("errorURL");
        /// <summary>
        /// The logout URL
        /// </summary>
        protected string logoutURL = ConfigurationManager.AppSettings.Get("logoutURL");
        /// <summary>
        /// The timeout URL
        /// </summary>
        protected string timeoutURL = ConfigurationManager.AppSettings.Get("timeoutURL");

        /// <summary>
        /// Gets the security token.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="utc">The UTC.</param>
        /// <param name="destURL">The dest URL.</param>
        /// <returns></returns>
        public string GetSecurityToken(string userId, string utc, string destURL)
        {
            string encoded = string.Empty;
            StringBuilder token = new StringBuilder();
            string dateFormat = @"ddd, dd MMM yyyy HH:mm:ss";

            // UserId
            if (userId == null)
            {
                return "";
            }

            if (userId.Length > 0)
            {
                token.Append(userId);
            }

            // append the timestamp, must be UTC
            token.Append(";").Append(string.IsNullOrEmpty(utc) ? DateTime.UtcNow.ToString(dateFormat, new CultureInfo("en-US")) : Convert.ToDateTime(utc, CultureInfo.InvariantCulture).ToString(dateFormat, new CultureInfo("en-US")));

            if (!string.IsNullOrEmpty(destURL))
            {
                token.Append(";").Append(destURL);
            }

            encoded = Encrypt(token.ToString());

            return encoded;
        }

        #region Encrypt/Decrypt

        /// <summary>
        /// Encrypts the specified clear.
        /// </summary>
        /// <param name="clear">The clear.</param>
        /// <returns></returns>
        public string Encrypt(string clear)
        {
            string encoded = string.Empty;
            UTF8Encoding textConverter = new UTF8Encoding();
            byte[] bToEncrypt = textConverter.GetBytes(clear);

            System.Security.Cryptography.RijndaelManaged aes = new RijndaelManaged();
            aes.Key = ConvertHexstringToByte(key);
            aes.IV = ConvertHexstringToByte(iv);
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            try
            {
                using (ICryptoTransform encryptor = aes.CreateEncryptor())
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(bToEncrypt, 0, bToEncrypt.Length);
                    cs.FlushFinalBlock();
                    encoded = BytesToHexString(ms.ToArray());
                    errorDescription.Length = 0;
                }
            }
            catch (Exception ex)
            {
                errorDescription.AppendFormat(CultureInfo.InvariantCulture, "Error: {0}", ex.Message);
            }
            finally
            {
                if (aes != null)
                {
                    aes.Dispose();
                }
            }
            return encoded;
        }

        /// <summary>
        /// Decrypts the specified clear.
        /// </summary>
        /// <param name="clear">The clear.</param>
        /// <returns></returns>
        public string Decrypt(string clear)
        {
            string decoded = string.Empty;
            UTF8Encoding textConverter = new UTF8Encoding();
            byte[] btoDecrypt = ConvertHexstringToByte(clear);
            byte[] decrypted = null;

            System.Security.Cryptography.RijndaelManaged aes = new RijndaelManaged();
            aes.Key = ConvertHexstringToByte(key);
            aes.IV = ConvertHexstringToByte(iv);

            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;

            try
            {
                using (ICryptoTransform decryptor = aes.CreateDecryptor())
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
                {
                    decrypted = new byte[btoDecrypt.Length];
                    cs.Write(btoDecrypt, 0, btoDecrypt.Length);
                    cs.FlushFinalBlock();
                    decoded = textConverter.GetString(ms.ToArray());
                    errorDescription.Length = 0;
                }
            }
            catch (Exception ex)
            {
                errorDescription.AppendFormat(CultureInfo.InvariantCulture, "Error: {0}", ex.Message);
            }
            finally
            {
                if (aes != null)
                {
                    aes.Dispose();
                }
            }
            return decoded;
        }

        #endregion

        #region utility Methods

        /// <summary>
        /// Gets the byte count.
        /// </summary>
        /// <param name="hexString">The hexadecimal string.</param>
        /// <returns></returns>
        public int GetByteCount(string hexString)
        {
            int numHexChars = 0;
            char c;

            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                {
                    numHexChars++;
                }
            }

            // if odd number of characters, discard last character
            if (numHexChars % 2 != 0)
            {
                numHexChars--;
            }

            return numHexChars / 2; // 2 characters per byte
        }

        /// <summary>
        /// Creates a byte array from the hexadecimal string. Each two characters are combined
        /// to create one byte. First two hexadecimal characters become first byte in returned array.
        /// Non-hexadecimal characters are ignored.
        /// </summary>
        /// <param name="hexString">string to convert to byte array</param>
        /// <param name="discarded">number of characters in string ignored</param>
        /// <returns>
        /// byte array, in the same left-to-right order as the hexString
        /// </returns>
        public byte[] GetBytes(string hexString, out int discarded)
        {
            discarded = 0;
            string newString = "";
            char c;

            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                {
                    newString += c;
                }
                else
                {
                    discarded++;
                }
            }

            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new string(new char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }

            return bytes;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(byte[] bytes)
        {
            string hexString = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                hexString += bytes[i].ToString("X2", CultureInfo.InvariantCulture);
            }

            return hexString;
        }

        /// <summary>
        /// Determines if given string is in proper hexadecimal string format
        /// </summary>
        /// <param name="hexString">Input string to check whether it is HexDigit String</param>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool InHexFormat(string hexString)
        {
            bool hexFormat = true;

            foreach (char digit in hexString)
            {
                if (!IsHexDigit(digit))
                {
                    hexFormat = false;
                    break;
                }
            }

            return hexFormat;
        }

        /// <summary>
        /// Returns true is c is a hexadecimal digit (A-F, a-f, 0-9)
        /// </summary>
        /// <param name="c">Character to test</param>
        /// <returns>
        /// true if hex digit, false if not
        /// </returns>
        public bool IsHexDigit(char c)
        {
            int numChar;
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            c = char.ToUpper(c, CultureInfo.InvariantCulture);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < (numA + 6))
            {
                return true;
            }

            if (numChar >= num1 && numChar < (num1 + 10))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Converts 1 or 2 character string into equivalant byte value
        /// </summary>
        /// <param name="hex">1 or 2 character string</param>
        /// <returns>
        /// The coverted byte
        /// </returns>
        /// <exception cref="System.ArgumentException">hex must be 1 or 2 characters in length</exception>
        private byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
            {
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            }

            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            return newByte;
        }

        /// <summary>
        /// Strings to hexadecimal string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public string StringToHexString(string str)
        {
            StringBuilder hexString = new StringBuilder(str.Length * 2);
            byte[] bytes = Encoding.ASCII.GetBytes(str);
            for (int counter = 0; counter < bytes.Length; counter++)
            {
                hexString.Append(string.Format(CultureInfo.InvariantCulture, "{0:X2}", bytes[counter]));
            }

            return hexString.ToString();
        }

        /// <summary>
        /// Byteses to hexadecimal string.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public string BytesToHexString(byte[] bytes)
        {
            StringBuilder hexString = new StringBuilder(bytes.Length * 2);

            for (int counter = 0; counter < bytes.Length; counter++)
            {
                hexString.Append(string.Format(CultureInfo.InvariantCulture, "{0:X2}", bytes[counter]));
            }

            return hexString.ToString();
        }

        /// <summary>
        /// Converts the hexstring to byte.
        /// </summary>
        /// <param name="hex_string">The hex_string.</param>
        /// <returns></returns>
        public byte[] ConvertHexstringToByte(string hex_string)
        {
            string hexString = hex_string;
            int discarded;
            byte[] byteArray = GetBytes(hexString, out discarded);
            string temp = "";
            for (int i = 0; i < byteArray.Length; i++)
            {
                temp += byteArray[i].ToString("D3", CultureInfo.InvariantCulture) + " ";
            }

            hex_string = temp;
            return byteArray;
        }

        #endregion utility Methods
    }
}