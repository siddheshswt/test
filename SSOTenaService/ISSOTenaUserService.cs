﻿// ***********************************************************************
// Assembly         : SSOTenaService
// Author           : Basantakumar Sahoo
// Created          : 01-29-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-25-2015
// ***********************************************************************
// <copyright file="ISSOTenaUserService.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SSOTenaService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.Text;

    using SCA.Indigo.Model;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    /// <summary>
    /// SSO Tena User Service Interface
    /// </summary>
    [ServiceContract]
    public interface ISSOTenaUserService
    {
        /// <summary>
        /// Checks the tena user.
        /// </summary>
        /// <param name="tenaUserName">Name of the tena user.</param>
        /// <returns></returns>
        [OperationContract]
        bool CheckTenaUser(string tenaUserName);

        /// <summary>
        /// Gets the user roles.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<TenaUserRole> GetUserRoles();

        /// <summary>
        /// Creates the tena user.
        /// </summary>
        /// <param name="tenaUser">The tena user.</param>
        /// <returns></returns>
        [OperationContract]
        string CreateTenaUser(TenaUser tenaUser);

        /// <summary>
        /// Updates the user role.
        /// </summary>
        /// <param name="tenaUser">The tena user.</param>
        /// <returns></returns>
        [OperationContract]
        string UpdateUserRole(TenaUser tenaUser);

        /// <summary>
        /// Updates the user status.
        /// </summary>
        /// <param name="tenaUserId">The tena user identifier.</param>
        /// <param name="deactivate">if set to <c>true</c> [deactivate].</param>
        /// <returns></returns>
        [OperationContract]
        string UpdateUserStatus(string tenaUserId, bool deactivate);

        /// <summary>
        /// Requests the login.
        /// </summary>
        /// <param name="tenaUserId">The tena user identifier.</param>
        /// <returns></returns>
        [OperationContract]
        string RequestLogin(string tenaUserId);

        /// <summary>
        /// Decrypts the token.
        /// </summary>
        /// <param name="encryptText">The encrypt text.</param>
        /// <returns></returns>
        [OperationContract]
        string DecryptToken(string encryptText);

        /// <summary>
        /// Encrypts the token.
        /// </summary>
        /// <param name="originalText">The original text.</param>
        /// <returns></returns>
        [OperationContract]
        string EncryptToken(string originalText);
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TenaUser
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>
        /// The country code.
        /// </value>
        [DataMember]
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is admin.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is admin; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TenaUser"/> is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if active; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        [DataMember]
        public int RoleId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TenaUserRole
    {
        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        [DataMember]
        public long RoleId { get; set; }

        /// <summary>
        /// Gets or sets the name of the role.
        /// </summary>
        /// <value>
        /// The name of the role.
        /// </value>
        [DataMember]
        public string RoleName { get; set; }
    }
}
