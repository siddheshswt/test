﻿// ***********************************************************************
// Assembly         : SSOTenaService
// Author           : Basantakumar Sahoo
// Created          : 01-29-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-25-2015
// ***********************************************************************
// <copyright file="SSOTenaUserService.svc.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SSOTenaService
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using SCA.Indigo.Common;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;

    /// <summary>
    /// SSO Tena User Service
    /// </summary>
    public class SSOTenaUserService : ISSOTenaUserService
    {
        /// <summary>
        /// Send the Role Master data to Tena User
        /// </summary>
        /// <returns>
        /// List of TenaUserRoles
        /// </returns>
        public List<TenaUserRole> GetUserRoles()
        {
            IRoleRepository roleRepository = new RoleRepository();

            List<TenaUserRole> userRoles = new List<TenaUserRole>();

            roleRepository.All.ToList().ForEach(d =>
            {
                if (d.RoleId > 0)
                {
                    userRoles.Add(new TenaUserRole() { RoleId = d.RoleId, RoleName = string.Format(CultureInfo.InvariantCulture, "{0} ( {1} )", d.RoleName, d.UserType) });
                }
            });

            roleRepository = null;

            return userRoles;
        }

        /// <summary>
        /// Check and return if the user is valid inthe SCA Indigo database.
        /// </summary>
        /// <param name="tenaUserName">Tena UserName</param>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool CheckTenaUser(string tenaUserName)
        {
            bool status = false;

            try
            {
                IUsersRepository usersRepository = new UsersRepository();
                List<Users> users = usersRepository.GetUsersByUserName(tenaUserName).ToList();
                usersRepository = null;

                status = users.Count > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return status;
        }

        #region Transaction

        /// <summary>
        /// Create new Tena User
        /// </summary>
        /// <param name="tenaUser">Tena User</param>
        /// <returns>
        /// String reference
        /// </returns>
        public string CreateTenaUser(TenaUser tenaUser)
        {
            string status = string.Empty;

            try
            {
                IUsersRepository usersRepository = new UsersRepository();
                UnitOfWork unitOfWork = usersRepository.UnitOfWork;

                Guid userId = Guid.NewGuid();

                // Insert into the Users Table
                Users newTenaUser = new Users()
                {
                    UserId = userId,
                    UserName = tenaUser.UserName,
                    PersonId = 1,
                    SecurityLevel = 1,
                    ValidFromDate = DateTime.UtcNow,
                    ValidToDate = DateTime.UtcNow.AddYears(10),
                    ModifiedBy = userId,
                    ModifiedDate = DateTime.UtcNow
                };

                // Add User Role
                IDXUserRole idxUserRole = new IDXUserRole()
                {
                    UserID = newTenaUser.UserId,
                    RoleID = tenaUser.RoleId,
                    ModifiedBy = newTenaUser.UserId,
                    ModifiedDate = DateTime.UtcNow
                };
                newTenaUser.IDXUserRoles.Add(idxUserRole);

                // New entity
                usersRepository.InsertTenaUser(newTenaUser);
                unitOfWork.Commit();

                unitOfWork = null;
                usersRepository = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return status;
        }

        /// <summary>
        /// Update the Role Id as per UserId
        /// </summary>
        /// <param name="tenaUser">Tena User</param>
        /// <returns>
        /// String reference
        /// </returns>
        public string UpdateUserRole(TenaUser tenaUser)
        {
            string status = string.Empty;
            try
            {
                IUsersRepository usersRepository = new UsersRepository();
                UnitOfWork unitOfWork = usersRepository.UnitOfWork;
                Users existTenaUser = null;

                if (tenaUser.UserName != null)
                {
                    existTenaUser = usersRepository.GetUsersByUserName(tenaUser.UserName).FirstOrDefault();
                }

                // Update the Role Id for the user
                if (existTenaUser != null && existTenaUser.IDXUserRoles != null)
                {
                    // Update the Role.
                    existTenaUser.IDXUserRoles.FirstOrDefault().RoleID = tenaUser.RoleId;

                    // Save the IDXUserRole
                    usersRepository.InsertOrUpdate(existTenaUser);
                    unitOfWork.Commit();
                }

                unitOfWork = null;
                usersRepository = null;
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            return status;
        }

        /// <summary>
        /// Chanage the IsAccountLocked property
        /// </summary>
        /// <param name="tenaUserName">Tena UserName</param>
        /// <param name="deactivate">is the user to be de-activated</param>
        /// <returns>
        /// String reference
        /// </returns>
        public string UpdateUserStatus(string tenaUserName, bool deactivate)
        {
            string status = string.Empty;
            try
            {
                IUsersRepository usersRepository = new UsersRepository();
                UnitOfWork unitOfWork = usersRepository.UnitOfWork;
                Users existTenaUser = null;

                if (!string.IsNullOrEmpty(tenaUserName))
                {
                    existTenaUser = usersRepository.GetUsersByUserName(tenaUserName).FirstOrDefault();
                }

                // Update the Role Id for the user
                if (existTenaUser != null)
                {
                    existTenaUser.IsAccountLocked = deactivate;

                    // Update the User.
                    usersRepository.InsertOrUpdate(existTenaUser);

                    // Save the User
                    unitOfWork.Commit();
                }

                unitOfWork = null;
                usersRepository = null;
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            return status;
        }

        #endregion

        /// <summary>
        /// Send Token to Tena User
        /// </summary>
        /// <param name="tenaUserId">Tena UserId</param>
        /// <returns>
        /// String reference
        /// </returns>
        public string RequestLogin(string tenaUserId)
        {
            string newToken = string.Empty;
            string status = string.Empty;

            try
            {
                if (CheckTenaUser(tenaUserId))
                {
                    string destURL = ConfigurationManager.AppSettings.Get("destURL");
                    SSOToken ssoToken = new SSOToken();
                    newToken = ssoToken.GetSecurityToken(tenaUserId, string.Empty, destURL);
                }
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            return newToken;
        }

        /// <summary>
        /// Decrypt the Token to Original String
        /// </summary>
        /// <param name="encryptText">Encrypted token</param>
        /// <returns>
        /// Decrypted token in String form
        /// </returns>
        public string DecryptToken(string encryptText)
        {
            SSOToken ssoToken = new SSOToken();
            return ssoToken.Decrypt(encryptText);
        }

        /// <summary>
        /// Encrypt the Token to Original String
        /// </summary>
        /// <param name="originalText">Input token to encrypted</param>
        /// <returns>
        /// The encrypted token
        /// </returns>
        public string EncryptToken(string originalText)
        {
            SSOToken ssoToken = new SSOToken();
            return ssoToken.Encrypt(originalText);
        }
    }
}
