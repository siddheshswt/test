﻿// ***********************************************************************
// <copyright file="ServiceConsumer.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the ServiceConsumer file.</summary>
// ***********************************************************************
namespace SCATaskScheduler
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Service Consumer class
    /// </summary>
    public class ServiceConsumer
    {
        /// <summary>
        /// Solution Path
        /// </summary>
        private static string solutionPath;   

        /// <summary>
        /// Initializes New Instance of ServiceConsumer class
        /// </summary>
        public ServiceConsumer()
        {
            solutionPath = ConfigurationManager.AppSettings["BasePath"].ToString();
            this.BaseUrl = ConfigurationManager.AppSettings["BaseUrl"].ToString();
        }

        #region Class level variables

        /// <summary>
        /// Gets or sets HttpClient
        /// </summary>
        public static HttpClient ObjHttpClient { get; set; }

        /// <summary>
        /// Gets or sets RequestMessage
        /// </summary>
        public string RequestMessage { get; set; }

        /// <summary>
        /// Gets or sets RequestUrl
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Gets or sets BaseUrl
        /// </summary>
        public string BaseUrl { get; set; }

        #endregion                

        /// <summary>
        /// Get Patient List
        /// </summary>
        public void SendCustomerPatinetToSAPPI()
        {
            this.RequestUrl = "SAPSender/GetAllCustomers/filter";
            this.GetHttpResponse("Customers");            
        }       

        /// <summary>
        /// returns Http Response(Calling method-Get)
        /// </summary>
        /// <param name="batchHeader">string batchHeader</param>        
        public void GetHttpResponse(string batchHeader)
        {
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                var objHttpClient = new HttpClient();

                // Set Time out 5 minut=300000 milisecond
                objHttpClient.Timeout = TimeSpan.FromMinutes(5);
                objHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));             
                var response = objHttpClient.GetAsync(new Uri(this.BaseUrl + "/" + this.RequestUrl)).Result;
                endTime = DateTime.Now;
                
                stringBuilder.Append(response.RequestMessage.ToString() + "\n");
                stringBuilder.Append(response.ToString());                
            }
            catch (InvalidOperationException e)
            {
                stringBuilder.Append(e.Message + " \n");
                stringBuilder.Append(e.StackTrace + " \n");
            }
            catch (Exception e)
            {
                stringBuilder.Append(e.Message + " \n");
                stringBuilder.Append(e.StackTrace + " \n");
            }

            LogToFile(startTime, endTime, batchHeader, stringBuilder.ToString(), "TaskLog");            
        }
        
        /// <summary>
        /// Log string message to specified log file
        /// </summary>
        /// <param name="startTime">DateTime startTime</param>
        /// <param name="endTime">DateTime endTime</param>
        /// <param name="batchHeader">Batch Header</param>
        /// <param name="logMessage">string logMessage</param>
        /// <param name="logFileName">string LogFileName</param>
        private static void LogToFile(DateTime startTime, DateTime endTime, string batchHeader, string logMessage, string logFileName)
        {
            // create a writer and open the file
            string strLogFilePath = Path.Combine(solutionPath, "LogFile\\" + logFileName + ".txt");
            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(string.Empty);
                textWriter.WriteLine("+++++++++++  " + batchHeader + " Batch Started at " + startTime + "  +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++ Batch End at " + endTime + "        +++++++++++++++");
            }
        }
    }
}
