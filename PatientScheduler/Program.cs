﻿// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Start point for Taskscheduler</summary>
// ***********************************************************************

namespace SCATaskScheduler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Start Point for Task scheduler
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Application starter
        /// </summary>
        /// <param name="args">string[] args</param>
        public static void Main(string[] args)
        {
            var objServiceConsumer = new ServiceConsumer();
            objServiceConsumer.SendCustomerPatinetToSAPPI();
        }
    }
}
