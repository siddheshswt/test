﻿using SCA.Indigo.Business.BusinessModels;
using SCA.Indigo.BusinessCore.Interfaces;
using SCA.Indigo.Common;
using SCA.Indigo.Common.Enums;
using SCA.Indigo.Infrastructure.Repositories;
using SCA.Indigo.Infrastructure.Repositories.CustomRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCA.Indigo.Model;


namespace SCA.Indigo.BusinessCore
{
	public class GoogleSearchCore:IGoogleSearchCore, IBuildIndexCore
	{        
        public bool BuildGoogleSearch(string indigoId = "", string objectType = "")
		{
			string userId = string.Empty; //TODO : to be changed
			IGoogleSearchRepository googleSearchrepository = null;
            IPatientRepository patientRepository = null;
            try
			{
                googleSearchrepository = new GoogleSearchRepository();
                
				var listOfDetailsForIndexing = new List<GoogleSearchIndex_Result>();
				#region Patient
                if (objectType == Convert.ToString((long)SCAEnums.ObjectType.Patient,CultureInfo.InvariantCulture) || string.IsNullOrEmpty(objectType))
                {
                    //PatientId SAPPatientNumber PatientName Postcode Correspondence_PostCode CarehomeName DateofBirth TelephoneNumber
                    var patientsToBeIndexed = googleSearchrepository.GetPatientsForIndexing(indigoId);
                    if (patientsToBeIndexed.Any())
                    {
                        listOfDetailsForIndexing.AddRange(patientsToBeIndexed);
                    }
                }
				#endregion


				#region Carehome
                if (objectType == Convert.ToString((long)SCAEnums.ObjectType.CareHome,CultureInfo.InvariantCulture) || string.IsNullOrEmpty(objectType))
                {
                    //PatientId SAPPatientNumber PatientName Postcode Correspondence_PostCode CarehomeName DateofBirth TelephoneNumber
                    var careHomesToBeIndexed = googleSearchrepository.GetCarehomesForIndexing(indigoId);
                    if (careHomesToBeIndexed.Any())
                    {
                        listOfDetailsForIndexing.AddRange(careHomesToBeIndexed);
                    }

                    if (!string.IsNullOrEmpty(objectType))
                    {
                        patientRepository = new PatientRepository();
                        var patientsForCareHomes = patientRepository.GetCareHomePatients(Convert.ToInt64(indigoId));
                        foreach (var records in patientsForCareHomes)
                        {

                            var patientsToBeIndexed = googleSearchrepository.GetPatientsForIndexing(Convert.ToString(records.PatientId));
                            if (patientsToBeIndexed.Any())
                            {
                                listOfDetailsForIndexing.AddRange(patientsToBeIndexed);
                            }
                        }
                    }
                }
				#endregion


				#region Customer
                if (objectType == Convert.ToString((long)SCAEnums.ObjectType.Customer,CultureInfo.InvariantCulture) || string.IsNullOrEmpty(objectType))
                {
                    //PatientId SAPPatientNumber PatientName Postcode Correspondence_PostCode CarehomeName DateofBirth TelephoneNumber
                    var customersToBeIndexed = googleSearchrepository.GetCustomersForIndexing(indigoId);
                    if (customersToBeIndexed.Any())
                    {
                        listOfDetailsForIndexing.AddRange(customersToBeIndexed);
                    }
                }
				#endregion

				if(!listOfDetailsForIndexing.Any())
				{
                    return false;
                }
					//update the index table
					List<Google> googleDetails = listOfDetailsForIndexing.Select(q=> 
														new Google(){
															IndigoId = q.IndigoId,
															ObjectType = q.IdType,
															Fields = q.Fields,
                                                            CustomerId = q.CustomerId,
                                                            CareHomeId = q.CareHomeId,
                                                            Status = q.Status,
                                                            PatientTypeID = q.PatientTypeId,
                                                            PatientType = q.PatientType,
                                                            FirstName = q.FirstName,
                                                            LastName = q.LastName,
                                                            CareHomeName = q.CareHomeName,
                                                            Postcode = q.Postcode,
                                                            DateofBirth = q.DateofBirth,
                                                            DeliveryDate = q.DeliveryDate
												}).ToList();

                    if (string.IsNullOrEmpty(objectType))
                    {
                        return BuildIndex(googleDetails, googleSearchrepository);
                    }
                    return BuildIncrementalIndex(googleDetails, googleSearchrepository);

				
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (NullReferenceException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			finally
			{
                if (googleSearchrepository != null)
                    googleSearchrepository.Dispose();

                if (patientRepository != null)
                    patientRepository.Dispose();
			}
			return false;
		}

		/// <summary>
		/// Truncate all the records and Build New index
		/// </summary>
		/// <param name="googleDetails"></param>
		/// <param name="repository"></param>
		/// <returns></returns>
		private bool BuildIndex(List<Google> googleDetails, IGoogleSearchRepository repository)
		{
			//Truncate

			var isTruncate = repository.TruncateGoogleTable();
			if (isTruncate)
			{
				//Add new records
				repository.Insert(googleDetails);
				IUnitOfWork unitOfWork = repository.UnitOfWork;
				unitOfWork.Commit();

			//	repository.CreateFullTextIndexGoogleTable(); // TODO : Discuss-Index creation not allowed through code
			}
			
			return true;
		}

        

        /// <summary>
        /// Delete the record and Build New index
        /// </summary>
        /// <param name="googleDetails"></param>
        /// <param name="repository"></param>
        /// <returns></returns>
        private bool BuildIncrementalIndex(List<Google> googleDetails, IGoogleSearchRepository repository)
        {
            Google google = new Google();            
            foreach (var records in googleDetails)
            {
                var result = repository.GetGoogleSearchForIndigoIDObjectType(records.IndigoId, records.ObjectType);
                if (result.Count>0)
                {
                    records.GoogleId = result[0].GoogleId;
                }
                
                repository.InsertOrUpdate(records);

                IUnitOfWork unitOfWork = repository.UnitOfWork;
                unitOfWork.Commit();                
            }

            return true;
        }


        /// <summary>
        /// This function will be constructed once we start moving the google search code
        /// </summary>
        /// <param name="indigoId"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool GoogleSearch(string indigoId, string objectType)
        {
            return true;
        }
    }
}
