﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCA.Indigo.BusinessCore.Interfaces;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Xml;
using SCA.Indigo.Common;
using System.Data;

namespace SCA.Indigo.BusinessCore
{
    /// <summary>
    /// Incremental Update Search Index Business Core Class
    /// </summary>    
    public class IncrementalUpdateCore : IIncrementalUpdateCore
    {
        private string _ipAddress { get; set; }
        private string _scaTaskScheduler { get; set; }
        private string _incrementalUpdateSearchIndexLog { get; set; }
        private string _archivalDirectoryLog { get; set; }

        public IncrementalUpdateCore(string IPAddress, string ServerAddress, string IncrementalUpdateSearchIndexLog, string ArchivePath)
        {
            _ipAddress = IPAddress;
            _scaTaskScheduler = ServerAddress;
            _incrementalUpdateSearchIndexLog = IncrementalUpdateSearchIndexLog;
            _archivalDirectoryLog = ArchivePath;
        }
        /// <summary>
        /// Read XML Files From Folder
        /// </summary>
        /// <returns></returns>
        public bool ReadXMLFilesFromFolder(IBuildIndexCore iBuildIndex, IFileCore iFileCore, string fileName, string userId)
        {
            bool isSuccess = false;
            try
            {
                string indigoId = "";
                string objectType = "";
                string elementName = null;
                var logDirectory = string.Empty;

                string serverPath = @"\\" + Path.Combine(_ipAddress, _scaTaskScheduler, _incrementalUpdateSearchIndexLog);

                /// create xml file directory
                Common.CommonHelper.CreateDirectoryIfNotExists(serverPath);
                logDirectory = Path.Combine(serverPath);

                #region Archieve
                var archivalDirectory = string.Empty;
                long incrementalUpdateFileNoOfDaysInArchival = 0;
                //create archive directory
                archivalDirectory = @"\\" + Path.Combine(_ipAddress, _scaTaskScheduler, _incrementalUpdateSearchIndexLog, _archivalDirectoryLog);

                Common.CommonHelper.CreateDirectoryIfNotExists(archivalDirectory);

                incrementalUpdateFileNoOfDaysInArchival = Convert.ToInt64(ConfigurationManager.AppSettings["IncrementalUpdateFileNoOfDaysInArchival"], CultureInfo.InvariantCulture);

                #endregion

                #region Archive Delete
                if (incrementalUpdateFileNoOfDaysInArchival > 0)
                {
                    iFileCore.DeleteArchivalFile(archivalDirectory, incrementalUpdateFileNoOfDaysInArchival);
                }
                #endregion

                string[] fileEntries = Directory.GetFiles(logDirectory, fileName);
                foreach (string strfileName in fileEntries)
                {
                    XmlTextReader reader = new XmlTextReader(strfileName);
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element: // The node is an element.

                                elementName = reader.Name;

                                break;
                            case XmlNodeType.Text: //Display the text in each element.

                                if (elementName.ToUpper() == "INDIGOID")
                                {
                                    indigoId = reader.Value.Trim();

                                }
                                else if (elementName.ToUpper() == "OBJECTTYPE")
                                {
                                    objectType = reader.Value.Trim();
                                }

                                break;
                        }
                    }
                    isSuccess = iBuildIndex.BuildGoogleSearch(indigoId, objectType);
                    reader.Close();
                    #region Archive Move
                    if (isSuccess)
                    {
                        iFileCore.MoveLogFileToArchival(serverPath, archivalDirectory, fileName);
                    }
                    #endregion
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {

            }
            return isSuccess;
        }

        /// <summary>
        /// Checks wheather file is ready for use.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool FileIsReady(string path, string userId)
        {
            try
            {
                using (var file = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    return true;
                }
            }
            catch (IOException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
            }
            return false;
        }
                  
    }
}
