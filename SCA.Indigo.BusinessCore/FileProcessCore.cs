﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SCA.Indigo.BusinessCore;
using SCA.Indigo.BusinessCore.Interfaces;

namespace SCA.Indigo.BusinessCore
{
    public class FileProcessCore : IFileCore
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="noOfDays"></param>
        public void DeleteArchivalFile(string path, long noOfDays)
        {
            if (!Directory.Exists(path))
            {
                return;
            }

            var lstFiles = new DirectoryInfo(path).GetFiles().Where(q => q.CreationTime < DateTime.Now.AddDays(-noOfDays)).ToList();

            if (lstFiles.Count > 0)
            {
                foreach (var file in lstFiles)
                {
                    file.Delete();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="archivalDirectory"></param>
        public void MoveLogFileToArchival(string sourceDirectory, string archivalDirectory, string fileName)
        {
            File.Move(sourceDirectory + "\\" + fileName, archivalDirectory + "\\" + fileName);
        }
    }
}
