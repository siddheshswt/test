﻿//-----------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.Host
{
    using System;
    using System.ServiceModel.Activation;
    using System.Web.Routing;   

    /// <summary>
    /// Use to defined global events for WCF service 
    /// </summary>    
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Global Application Start Method for service
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.Add(new ServiceRoute("Sample", new WebServiceHostFactory(), typeof(Sample)));
            RouteTable.Routes.Add(new ServiceRoute("Menu", new WebServiceHostFactory(), typeof(Menu)));
            RouteTable.Routes.Add(new ServiceRoute("Search", new WebServiceHostFactory(), typeof(Search)));
            RouteTable.Routes.Add(new ServiceRoute("AccountManagement", new WebServiceHostFactory(), typeof(AccountManagement)));
            RouteTable.Routes.Add(new ServiceRoute("CommonService", new WebServiceHostFactory(), typeof(CommonService)));
            RouteTable.Routes.Add(new ServiceRoute("Patient", new WebServiceHostFactory(), typeof(Patient)));
            RouteTable.Routes.Add(new ServiceRoute("Product", new WebServiceHostFactory(), typeof(Product)));
            RouteTable.Routes.Add(new ServiceRoute("Order", new WebServiceHostFactory(), typeof(Order)));
            RouteTable.Routes.Add(new ServiceRoute("SAPReceiver", new WebServiceHostFactory(), typeof(SAPRequestReceiver)));
            RouteTable.Routes.Add(new ServiceRoute("AuthorizePrescription", new WebServiceHostFactory(), typeof(AuthorizePrescription)));
            RouteTable.Routes.Add(new ServiceRoute("SAPSender", new WebServiceHostFactory(), typeof(SAPResponseSender)));
            RouteTable.Routes.Add(new ServiceRoute("UserAdminService", new WebServiceHostFactory(), typeof(UserAdminService)));
            RouteTable.Routes.Add(new ServiceRoute("CustomerParameter", new WebServiceHostFactory(), typeof(CustomerParameter)));
            RouteTable.Routes.Add(new ServiceRoute("Carehome", new WebServiceHostFactory(), typeof(CareHome)));
            RouteTable.Routes.Add(new ServiceRoute("Customer", new WebServiceHostFactory(), typeof(CustomerInformation)));
            RouteTable.Routes.Add(new ServiceRoute("Notes", new WebServiceHostFactory(), typeof(Notes)));
            RouteTable.Routes.Add(new ServiceRoute("AuditLog", new WebServiceHostFactory(), typeof(AuditLog)));
            RouteTable.Routes.Add(new ServiceRoute("Attachment", new WebServiceHostFactory(), typeof(Attachment)));
            RouteTable.Routes.Add(new ServiceRoute("ReportBuilderService", new WebServiceHostFactory(), typeof(ReportBuilderService)));
            RouteTable.Routes.Add(new ServiceRoute("MassDataImport", new WebServiceHostFactory(), typeof(MassDataImport)));
            RouteTable.Routes.Add(new ServiceRoute("MassDataChanges", new WebServiceHostFactory(), typeof(MassDataChanges)));
            RouteTable.Routes.Add(new ServiceRoute("Interaction", new WebServiceHostFactory(), typeof(Interaction)));
			RouteTable.Routes.Add(new ServiceRoute("GoogleSearch", new WebServiceHostFactory(), typeof(GoogleSearch)));
			RouteTable.Routes.Add(new ServiceRoute("NDDCalculation", new WebServiceHostFactory(), typeof(NDDCalculation)));
            RouteTable.Routes.Add(new ServiceRoute("IncrementalUpdateSearchIndex", new WebServiceHostFactory(), typeof(IncrementalUpdateSearchIndex)));
        }
    }
}
