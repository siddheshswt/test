// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="UsersRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Users Repository
    /// </summary>
    public interface IUsersRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Users> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Users> AllIncluding(params Expression<Func<Users, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Users Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        void InsertOrUpdate(Users user);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Gets the name of the users by user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        IQueryable<Users> GetUsersByUserName(string userName);

        /// <summary>
        /// Gets the name of the users by user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        IQueryable<Users> GetUsersByUserName(string userName, string password);

        /// <summary>
        /// Gets the users by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IQueryable<Users> GetUsersByUserId(string userId);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Inserts the tena user.
        /// </summary>
        /// <param name="tenaUser">The tena user.</param>
        void InsertTenaUser(Users tenaUser);

        /// <summary>
        /// Log User Login Details
        /// </summary>
        /// <param name="objLoginUserDetail"></param>
        void LogUserLoginDetails(LoginUserDetail objLoginUserDetail);
    }

    /// <summary>
    /// Users Repository
    /// </summary>
    public class UsersRepository : IUsersRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersRepository"/> class.
        /// </summary>
        public UsersRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="UsersRepository"/> class.
        /// </summary>
        ~UsersRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IUsersRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Users> All
        {
            get { return _dbContext.Users; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Users> AllIncluding(params Expression<Func<Users, object>>[] includeProperties)
        {
            IQueryable<Users> query = _dbContext.Users;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Users Find(long id)
        {
            return _dbContext.Users.Find(id);
        }

        /// <summary>
        /// Gets the name of the users by user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        public IQueryable<Users> GetUsersByUserName(string userName)
        {
            return GetUsersByUserName(userName, "");
        }

        /// <summary>
        /// Gets the name of the users by user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public IQueryable<Users> GetUsersByUserName(string userName, string password)
        {
            if (!string.IsNullOrWhiteSpace(password))
            {
                return All.Where(u => u.UserName == userName && u.Password == password);
            }

            return All.Where(u => u.UserName == userName);
        }

        /// <summary>
        /// Gets the users by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public IQueryable<Users> GetUsersByUserId(string userId)
        {
            return All.Where(u => u.UserId.ToString() == userId);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        public void InsertOrUpdate(Users user)
        {
            if (user.UserId == default(Guid))
            {
                // New entity
                _dbContext.Users.Add(user);
            }
            else
            {
                // Existing entity
                // _dbContext.DataContext.Users.Attach(user);
                _dbContext.DataContext.Entry(user).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Inserts the tena user.
        /// </summary>
        /// <param name="tenaUser">The tena user.</param>
        public void InsertTenaUser(Users tenaUser)
        {
            _dbContext.Users.Add(tenaUser);
        }

        public void LogUserLoginDetails(LoginUserDetail objLoginUserDetail)
        {
            // New entity
            _dbContext.LoginUserDetails.Add(objLoginUserDetail);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var user = _dbContext.Users.Find(id);
            _dbContext.Users.Remove(user);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}