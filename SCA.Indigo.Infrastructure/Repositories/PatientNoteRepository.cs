// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sagar Yerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="PatientNoteRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Patient Note Repository
    /// </summary>
    public interface IPatientNoteRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<PatientNote> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<PatientNote> AllIncluding(params Expression<Func<PatientNote, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        PatientNote Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="patientNote">The patient note.</param>
        void InsertOrUpdate(PatientNote patientNote);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the patient notes.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<PatientNote> GetPatientNotes(long patientId);
    }

    /// <summary>
    /// Patient Note Repository
    /// </summary>
    public class PatientNoteRepository : IPatientNoteRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The unit of work
        /// </summary>
        private UnitOfWork UnitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientNoteRepository"/> class.
        /// </summary>
        public PatientNoteRepository()
        {
            _dbContext = new DBContext();
            UnitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PatientNoteRepository"/> class.
        /// </summary>
        ~PatientNoteRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPatientNoteRepository.UnitOfWork
        {
            get { return UnitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<PatientNote> All
        {
            get { return _dbContext.PatientNotes; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<PatientNote> AllIncluding(params Expression<Func<PatientNote, object>>[] includeProperties)
        {
            IQueryable<PatientNote> query = _dbContext.PatientNotes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public PatientNote Find(long id)
        {
            return _dbContext.PatientNotes.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="patientnote">The patientnote.</param>
        public void InsertOrUpdate(PatientNote patientnote)
        {
            if (patientnote.PatientNoteId == default(long))
            {
                // New entity
                _dbContext.PatientNotes.Add(patientnote);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(patientnote).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Gets the patient notes.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<PatientNote> GetPatientNotes(long patientId)
        {
            return All.Where(p => p.PatientId == patientId);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var patientnote = _dbContext.PatientNotes.Find(id);
            _dbContext.PatientNotes.Remove(patientnote);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }   
}