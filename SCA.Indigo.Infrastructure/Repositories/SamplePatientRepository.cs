﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sachin Phaphale
// Created          : 06-Feb-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="SamplePatientRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Sample Patient Repository
    /// </summary>
    public interface ISamplePatientRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<SamplePatient> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<SamplePatient> AllIncluding(params Expression<Func<SamplePatient, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        SamplePatient Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="samplePatient">The sample patient.</param>
        void InsertOrUpdate(SamplePatient samplePatient);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Sample Patient Repository
    /// </summary>
    public class SamplePatientRepository : ISamplePatientRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="SamplePatientRepository"/> class.
        /// </summary>
        public SamplePatientRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="SamplePatientRepository"/> class.
        /// </summary>
        ~SamplePatientRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork ISamplePatientRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<SamplePatient> All
        {
            get { return _dbContext.SamplePatients; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<SamplePatient> AllIncluding(params Expression<Func<SamplePatient, object>>[] includeProperties)
        {
            IQueryable<SamplePatient> query = _dbContext.SamplePatients;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public SamplePatient Find(long id)
        {
            return _dbContext.SamplePatients.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="SamplePatient">The sample patient.</param>
        public void InsertOrUpdate(SamplePatient SamplePatient)
        {
            if (SamplePatient.SamplePatientId == default(long))
            {
                // New entity
                _dbContext.SamplePatients.Add(SamplePatient);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.SamplePatients.Attach(SamplePatient);
                _dbContext.DataContext.Entry(SamplePatient).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }   
    }
}
