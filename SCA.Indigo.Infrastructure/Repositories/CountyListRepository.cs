﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Jagdish
// Created          : 6-26-2014
// ***********************************************************************
// <copyright file="CountyListRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using SCA.Indigo.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// CountyList Repository Interface
    /// </summary>
    public interface ICountyListRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<CountyList> All { get; }

        /// <summary>
        /// Gets all county list by code.
        /// </summary>
        /// <param name="countyCodeList">The county code list.</param>
        /// <returns>County List</returns>
        IQueryable<CountyList> GetAllCountyListByCode(List<string> countyCodeList);
    }

    /// <summary>
    /// CountyList Repository Class
    /// </summary>
    public class CountyListRepository : ICountyListRepository
    {
                /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListRepository"/> class.
        /// </summary>
        public CountyListRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ListRepository"/> class.
        /// </summary>
        ~CountyListRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork ICountyListRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<CountyList> All
        {
            get { return _dbContext.CountyLists; }
        }

        /// <summary>
        /// Gets all county list by code.
        /// </summary>
        /// <param name="countyCodeList">The county code list.</param>
        /// <returns></returns>
        public IQueryable<CountyList> GetAllCountyListByCode(List<string> countyCodeList)
        {
            return All.Where(q => countyCodeList.Contains(q.CountyCode));
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }
}
