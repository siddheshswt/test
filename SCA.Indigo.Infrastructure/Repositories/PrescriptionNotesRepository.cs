﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : syerva
// Created          : 02-18-2015
//
// Last Modified By : syerva
// Last Modified On : 02-18-2015
// ***********************************************************************
// <copyright file="PrescriptionNotesRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using SCA.Indigo.Model;

    /// <summary>
    /// Prescription Notes Repository
    /// </summary>
    public interface IPrescriptionNotesRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<PrescriptionNote> All { get; }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="note">The note.</param>
        void InsertOrUpdate(PrescriptionNote note);

        /// <summary>
        /// Gets the notes by patient identifier.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<PrescriptionNote> GetNotesByPatientId(long patientId);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Prescription Notes Repository
    /// </summary>
    public class PrescriptionNotesRepository : IPrescriptionNotesRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrescriptionNotesRepository"/> class.
        /// </summary>
        public PrescriptionNotesRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PrescriptionNotesRepository"/> class.
        /// </summary>
        ~PrescriptionNotesRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPrescriptionNotesRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<PrescriptionNote> All
        {
            get { return _dbContext.PrescriptionNotes; }
        }

        /// <summary>
        /// Gets the notes by patient identifier.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<PrescriptionNote> GetNotesByPatientId(long patientId)
        {
            return All.Where(q => q.PatientId == patientId);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="note">The note.</param>
        public void InsertOrUpdate(PrescriptionNote note)
        {
            if (note.PrescriptionNoteId == default(long))
            {
                // New entity
                _dbContext.PrescriptionNotes.Add(note);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.PrescriptionNotes.Attach(note);
                _dbContext.DataContext.Entry(note).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
