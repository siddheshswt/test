// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXCustomerReasonCodeRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Reason Code
    /// </summary>
    public interface IIDXCustomerReasonCodeRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXCustomerReasonCodes> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerReasonCodes> AllIncluding(params Expression<Func<IDXCustomerReasonCodes, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXCustomerReasonCodes Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        void InsertOrUpdate(IDXCustomerReasonCodes user);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Gets the reason codes by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerReasonCodes> GetReasonCodesByCustomerId(long customerId);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();      
    }

    /// <summary>
    /// Reason Code
    /// </summary>
    public class IDXCustomerReasonCodeRepository : IIDXCustomerReasonCodeRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXCustomerReasonCodeRepository"/> class.
        /// </summary>
        public IDXCustomerReasonCodeRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXCustomerReasonCodeRepository"/> class.
        /// </summary>
        ~IDXCustomerReasonCodeRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXCustomerReasonCodeRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXCustomerReasonCodes> All
        {
            get { return _dbContext.IDXCustomerReasonCodes; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXCustomerReasonCodes> AllIncluding(params Expression<Func<IDXCustomerReasonCodes, object>>[] includeProperties)
        {
            IQueryable<IDXCustomerReasonCodes> query = _dbContext.IDXCustomerReasonCodes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXCustomerReasonCodes Find(long id)
        {
            return _dbContext.IDXCustomerReasonCodes.Find(id);
        }

        /// <summary>
        /// Gets the reason codes by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXCustomerReasonCodes> GetReasonCodesByCustomerId(long customerId)
        {
            return All.Where(q => q.CustomerID == customerId && q.IsRemoved != true);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="reasonCode">The reason code.</param>
        public void InsertOrUpdate(IDXCustomerReasonCodes reasonCode)
        {
            if (reasonCode.IDXCustomerReasonCodeId == default(long))
            {
                // New entity
                _dbContext.IDXCustomerReasonCodes.Add(reasonCode);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.IDXCustomerReasonCodes.Attach(reasonCode);
                _dbContext.DataContext.Entry(reasonCode).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var reasonCode = _dbContext.IDXCustomerReasonCodes.Find(id);
            _dbContext.IDXCustomerReasonCodes.Remove(reasonCode);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }        
    }    
}