// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="PrescriptionRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Prescription Repository
    /// </summary>
    public interface IPrescriptionRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Prescription> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Prescription> AllIncluding(params Expression<Func<Prescription, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Prescription Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="prescription">The prescription.</param>
        void InsertOrUpdate(Prescription prescription);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the patient prescription.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<Prescription> GetPatientPrescription(long patientId);

        /// <summary>
        /// Gets the patient prescription.
        /// </summary>
        /// <param name="patientIds">The patient ids.</param>
        /// <returns></returns>
        IQueryable<Prescription> GetPatientPrescription(List<long> patientIds);

        /// <summary>
        /// Gets the patient prescriptions.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="showRemoved">if set to <c>true</c> [show removed].</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> GetPatientPrescriptions(long patientId, bool showRemoved);

        /// <summary>
        /// Deletes the address.
        /// </summary>
        /// <param name="AddressId">The address identifier.</param>
        void DeleteAddress(long? AddressId);

        /// <summary>
        /// Gets the added Patient entities.
        /// </summary>
        /// <returns></returns>
        System.Collections.Generic.List<Patient> GetAddedPatientEntities();
    }

    /// <summary>
    /// Prescription Repository
    /// </summary>
    public class PrescriptionRepository : IPrescriptionRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrescriptionRepository"/> class.
        /// </summary>
        public PrescriptionRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PrescriptionRepository"/> class.
        /// </summary>
        ~PrescriptionRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Prescription> All
        {
            get { return _dbContext.Prescriptions; }
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPrescriptionRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all prescriptions.
        /// </summary>
        /// <value>
        /// All prescriptions.
        /// </value>
        public IQueryable<Prescription> AllPrescriptions
        {
            get { return _dbContext.Prescriptions; }
        }

        /// <summary>
        /// Gets all prescriptions products.
        /// </summary>
        /// <value>
        /// All prescriptions products.
        /// </value>
        public IQueryable<IDXPrescriptionProduct> AllPrescriptionsProducts
        {
            get { return _dbContext.IDXPrescriptionProducts; }
        }

        /// <summary>
        /// Gets the patient prescriptions.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="showRemoved">if set to <c>true</c> [show removed].</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> GetPatientPrescriptions(long patientId, bool showRemoved)
        {
            var prescriptions = AllPrescriptions.Where(p => p.PatientId == patientId);
            if (showRemoved)
            {
                return AllPrescriptionsProducts.Where(idx => prescriptions.Where(pt => pt.PrescriptionId == idx.PrescriptionId).Any());
            }

            return AllPrescriptionsProducts.Where(idx => prescriptions.Where(pt => pt.PrescriptionId == idx.PrescriptionId).Any()
                                                                            && (idx.IsRemoved == false || idx.IsRemoved == null));
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Prescription> AllIncluding(params Expression<Func<Prescription, object>>[] includeProperties)
        {
            IQueryable<Prescription> query = _dbContext.Prescriptions;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Prescription Find(long id)
        {
            return _dbContext.Prescriptions.Find(id);
        }

        /// <summary>
        /// Gets the patient prescription.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<Prescription> GetPatientPrescription(long patientId)
        {
            return All.Where(q => q.PatientId == patientId);
        }

        /// <summary>
        /// Gets the patient prescription.
        /// </summary>
        /// <param name="patientIds">The patient ids.</param>
        /// <returns></returns>
        public IQueryable<Prescription> GetPatientPrescription(List<long> patientIds)
        {
            return All.Where(q => patientIds.Contains(q.PatientId));
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="prescription">The prescription.</param>
        public void InsertOrUpdate(Prescription prescription)
        {
            if (prescription.PrescriptionId == default(long))
            {
                // New entity
                _dbContext.Prescriptions.Add(prescription);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Prescriptions.Attach(prescription);
                _dbContext.DataContext.Entry(prescription).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var prescription = _dbContext.Prescriptions.Find(id);
            _dbContext.Prescriptions.Remove(prescription);
        }

        /// <summary>
        /// Deletes the address.
        /// </summary>
        /// <param name="AddressId">The address identifier.</param>
        public void DeleteAddress(long? AddressId)
        {
            var address = _dbContext.Addresses.Find(AddressId);
            _dbContext.Addresses.Remove(address);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Gets the added Patient entities.
        /// </summary>
        /// <returns></returns>
        public System.Collections.Generic.List<Patient> GetAddedPatientEntities()
        {
            var patient = _dbContext.Patients.Local.ToList();
            return patient;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}