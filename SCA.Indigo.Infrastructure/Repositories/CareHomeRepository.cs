// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="CareHomeRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;
    using System.Collections.Generic;

    /// <summary>
    /// Interface Carehome
    /// </summary>
    public interface ICareHomeRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets the care home.
        /// </summary>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <returns></returns>
        IQueryable<CareHome> GetCareHome(long careHomeId);

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<CareHome> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<CareHome> AllIncluding(params Expression<Func<CareHome, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CareHome Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="careHome">The care home.</param>
        void InsertOrUpdate(CareHome careHome);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the care homes by NDD and customer.
        /// </summary>
        /// <param name="customerID">The customer identifier.</param>
        /// <param name="NDD">The NDD.</param>
        /// <returns></returns>
        IQueryable<CareHome> GetCareHomesByNDDAndCustomer(long customerID, DateTime NDD);

        /// <summary>
        /// Gets the care homes by sap identifier.
        /// </summary>
        /// <param name="careHomeSAPId">The care home sap identifier.</param>
        /// <returns></returns>
        IQueryable<CareHome> GetCareHomesBySAPId(List<string> careHomeSAPId);

        /// <summary>
        /// Gets the care homes by load identifier.
        /// </summary>
        /// <param name="LoadId">The load identifier.</param>
        /// <returns></returns>
        IQueryable<CareHome> GetCareHomesByLoadId(long LoadId);

        /// <summary>
        /// Gets the care homes by load identifier.
        /// </summary>
        /// <param name="sapCareHomeIdList">The sap care home identifier list.</param>
        /// <returns></returns>
        IQueryable<CareHome> GetAllCareHomesBySapCareHomeId(List<string> sapCareHomeIdList);

        /// <summary>
        /// Gets the added carehome entities.
        /// </summary>
        /// <returns></returns>
        System.Collections.Generic.List<CareHome> GetAddedCarehomeEntities();
    }

    /// <summary>
    /// Carehome Repository
    /// </summary>
    public class CareHomeRepository : ICareHomeRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="CareHomeRepository"/> class.
        /// </summary>
        public CareHomeRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CareHomeRepository"/> class.
        /// </summary>
        ~CareHomeRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork ICareHomeRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<CareHome> All
        {
            get { return _dbContext.CareHomes; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<CareHome> AllIncluding(params Expression<Func<CareHome, object>>[] includeProperties)
        {
            IQueryable<CareHome> query = _dbContext.CareHomes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets the care home.
        /// </summary>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <returns></returns>
        public IQueryable<CareHome> GetCareHome(long careHomeId)
        {
            return All.Where(q => q.CareHomeId == careHomeId);
        }

        /// <summary>
        /// Gets the care homes by NDD and customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="NDD">The NDD.</param>
        /// <returns></returns>
        public IQueryable<CareHome> GetCareHomesByNDDAndCustomer(long customerId, DateTime NDD)
        {
            return All.Where(q => q.CustomerId == customerId && q.NextDeliveryDate <= NDD);
        }

        /// <summary>
        /// Gets the care homes by sap identifier.
        /// </summary>
        /// <param name="careHomeSAPId">The care home sap identifier.</param>
        /// <returns></returns>
        public IQueryable<CareHome> GetCareHomesBySAPId(List<string> careHomeSAPId)
        {            
            return All.Where(q => careHomeSAPId.Contains(q.SAPCareHomeNumber));           
        }

        /// <summary>
        /// Gets the care homes by load identifier.
        /// </summary>
        /// <param name="loadId">The load identifier.</param>
        /// <returns></returns>
        public IQueryable<CareHome> GetAllCareHomesBySapCareHomeId(List<string> sapCareHomeIdList)
        {
            return All.Where(q => sapCareHomeIdList.Contains(q.SAPCareHomeNumber));    
        }

        /// <summary>
        /// Gets the care homes by load identifier.
        /// </summary>
        /// <param name="LoadId">The load identifier.</param>
        /// <returns></returns>
        public IQueryable<CareHome> GetCareHomesByLoadId(long LoadId)
        {
            return All.Where(q => q.LoadId == LoadId);  
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CareHome Find(long id)
        {
            return _dbContext.CareHomes.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="careHome">The care home.</param>
        public void InsertOrUpdate(CareHome careHome)
        {
            if (careHome.CareHomeId == default(long))
            {
                // New entity
                _dbContext.CareHomes.Add(careHome);
            }
            else
            {
                // Existing entity                                  
                _dbContext.DataContext.Entry(careHome).State = EntityState.Modified;                
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var carehome = _dbContext.CareHomes.Find(id);
            _dbContext.CareHomes.Remove(carehome);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        /// <summary>
        /// Gets the added carehome entities.
        /// </summary>
        /// <returns></returns>
        public System.Collections.Generic.List<CareHome> GetAddedCarehomeEntities()
        {
            var carehomes = _dbContext.CareHomes.Local.ToList();
            return carehomes;
        }
    }
}