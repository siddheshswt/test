﻿using SCA.Indigo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Infrastructure.Repositories
{
    /// <summary>
    /// Report Builder Repository
    /// </summary>
    public interface IReportBuilderRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<ReportBuilderFormat> All { get; }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ReportBuilderFormat Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="reportBuilderFormat">The report builder format.</param>
        void InsertOrUpdate(ReportBuilderFormat reportBuilderFormat);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Report Builder Repository
    /// </summary>
    public class ReportBuilderRepository : IReportBuilderRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportBuilderRepository"/> class.
        /// </summary>
        public ReportBuilderRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }


        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IReportBuilderRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<ReportBuilderFormat> All
        {
            get { return _dbContext.ReportBuilderFormat; }
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ReportBuilderFormat Find(long id)
        {
            return _dbContext.ReportBuilderFormat.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="reportBuilderFormat">The report builder format.</param>
        public void InsertOrUpdate(ReportBuilderFormat reportBuilderFormat)
        {
            if (reportBuilderFormat.ReportBuilderId == default(long))
            {
                // New entity
                _dbContext.ReportBuilderFormat.Add(reportBuilderFormat);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.ReportBuilderFormats.Attach(reportBuilderFormat);
                _dbContext.DataContext.Entry(reportBuilderFormat).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var reportBuilderFormat = _dbContext.ReportBuilderFormat.Find(id);
            _dbContext.ReportBuilderFormat.Remove(reportBuilderFormat);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }   
    }
}
