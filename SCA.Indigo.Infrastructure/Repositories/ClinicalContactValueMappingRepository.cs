﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sachin
// Created          : 04-28-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="ClinicalContactValueMappingRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SCA.Indigo.Model;
    using System.Linq.Expressions;
    using System.Data.Entity;

    /// <summary>
    /// Interface Value Mapping
    /// </summary>
    public interface IClinicalContactValueMappingRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<ClinicalContactValueMapping> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<ClinicalContactValueMapping> AllIncluding(params Expression<Func<ClinicalContactValueMapping, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ClinicalContactValueMapping Find(long id);

        //IQueryable<ClinicalContactValueMapping> GetClinicalContactValueMappingByCustomerId(long customerId);

        /// <summary>
        /// Gets all clinical contact value mapping.
        /// </summary>
        /// <returns></returns>
        IQueryable<ClinicalContactValueMapping> GetAllClinicalContactValueMapping();

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactValueMapping">The clinical contact value mapping.</param>
        void InsertOrUpdate(ClinicalContactValueMapping clinicalContactValueMapping);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }
    
    /// <summary>
    /// Value Mapping Repository
    /// </summary>
    public class ClinicalContactValueMappingRepository : IClinicalContactValueMappingRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicalContactValueMappingRepository"/> class.
        /// </summary>
        public ClinicalContactValueMappingRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ClinicalContactValueMappingRepository"/> class.
        /// </summary>
        ~ClinicalContactValueMappingRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IClinicalContactValueMappingRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<ClinicalContactValueMapping> All
        {
            get { return _dbContext.ClinicalContactValueMappings; }
        }

        /// <summary>
        /// Gets all clinical contact value mapping.
        /// </summary>
        /// <returns></returns>
        public IQueryable<ClinicalContactValueMapping> GetAllClinicalContactValueMapping()
        {
           return All;
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<ClinicalContactValueMapping> AllIncluding(params Expression<Func<ClinicalContactValueMapping, object>>[] includeProperties)
        {
            IQueryable<ClinicalContactValueMapping> query = _dbContext.ClinicalContactValueMappings;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ClinicalContactValueMapping Find(long id)
        {
            return _dbContext.ClinicalContactValueMappings.Find(id);
        }

        //public IQueryable<ClinicalContactValueMapping> GetClinicalContactValueMappingByCustomerId(long customerId)
        //{
        //    return All.Where(u => u.CustomerId == customerId && u.IsRemoved == false);
        //}

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactValueMapping">The clinical contact value mapping.</param>
        public void InsertOrUpdate(ClinicalContactValueMapping clinicalContactValueMapping)
        {
            if (clinicalContactValueMapping.ClinicalContactValueMappingId == default(long))
            {              
                _dbContext.ClinicalContactValueMappings.Add(clinicalContactValueMapping);
            }
            else
            {             
                _dbContext.DataContext.ClinicalContactValueMappings.Attach(clinicalContactValueMapping);
                _dbContext.DataContext.Entry(clinicalContactValueMapping).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var clinicalContactValueMapping = _dbContext.ClinicalContactValueMappings.Find(id);
            _dbContext.ClinicalContactValueMappings.Remove(clinicalContactValueMapping);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
