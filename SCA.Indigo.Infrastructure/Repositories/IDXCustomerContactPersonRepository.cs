// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXCustomerContactPersonRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Customer Contact Person
    /// </summary>
    public interface IIDXCustomerContactPersonRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXCustomerContactPerson> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerContactPerson> AllIncluding(params Expression<Func<IDXCustomerContactPerson, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXCustomerContactPerson Find(long id);

        /// <summary>
        /// Gets all customer contact person.
        /// </summary>
        /// <returns></returns>
        IQueryable<IDXCustomerContactPerson> GetAllCustomerContactPerson();

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxCustomerContactPerson">The index customer contact person.</param>
        void InsertOrUpdate(IDXCustomerContactPerson idxCustomerContactPerson);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Customer Contact Person
    /// </summary>
    public class IDXCustomerContactPersonRepository : IIDXCustomerContactPersonRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXCustomerContactPersonRepository"/> class.
        /// </summary>
        public IDXCustomerContactPersonRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXCustomerContactPersonRepository"/> class.
        /// </summary>
        ~IDXCustomerContactPersonRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXCustomerContactPersonRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXCustomerContactPerson> All
        {
            get { return _dbContext.IDXCustomerContactPersons; }
        }

        /// <summary>
        /// Gets all customer contact person.
        /// </summary>
        /// <returns></returns>
        public IQueryable<IDXCustomerContactPerson> GetAllCustomerContactPerson()
        {
            return this.All;
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXCustomerContactPerson> AllIncluding(params Expression<Func<IDXCustomerContactPerson, object>>[] includeProperties)
        {
            IQueryable<IDXCustomerContactPerson> query = _dbContext.IDXCustomerContactPersons;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXCustomerContactPerson Find(long id)
        {
            return _dbContext.IDXCustomerContactPersons.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxCustomerContactPerson">The index customer contact person.</param>
        public void InsertOrUpdate(IDXCustomerContactPerson idxCustomerContactPerson)
        {
            if (idxCustomerContactPerson.ContactPersonId == default(long))
            {
                // New entity
                _dbContext.IDXCustomerContactPersons.Add(idxCustomerContactPerson);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxCustomerContactPerson).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxCustomerContactPerson = _dbContext.IDXCustomerContactPersons.Find(id);
            _dbContext.IDXCustomerContactPersons.Remove(idxCustomerContactPerson);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }
}