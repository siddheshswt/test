// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty 
// Created          : 02-12-2015
//
// Last Modified By : mamshett
// Last Modified On : 02-12-2015
// ***********************************************************************
// <copyright file="PatientTypeMatrixRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Patient Type Matrix Repository
    /// </summary>
    public interface IPatientTypeMatrixRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<PatientTypeMatrix> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<PatientTypeMatrix> AllIncluding(params Expression<Func<PatientTypeMatrix, object>>[] includeProperties);
     
        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        PatientTypeMatrix Find(long id);

        /// <summary>
        /// Gets the patient type matrix by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<PatientTypeMatrix> GetPatientTypeMatrixByCustomerId(long customerId);

        /// <summary>
        /// Gets the type of the matrix by patient.
        /// </summary>
        /// <param name="patientTypeIds">The patient type ids.</param>
        /// <returns></returns>
        IQueryable<PatientTypeMatrix> GetMatrixByPatientType(List<long> patientTypeIds);

        /// <summary>
        /// Gets the patient type by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
		List<PatientTypeMatrix> GetPatientTypeMatrixByCustomerId(List<long> customerIds);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="patientTypeMatrix">The patient type matrix.</param>
        void InsertOrUpdate(PatientTypeMatrix patientTypeMatrix);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Patient Type Matrix Repository
    /// </summary>
    public class PatientTypeMatrixRepository : IPatientTypeMatrixRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientTypeMatrixRepository"/> class.
        /// </summary>
        public PatientTypeMatrixRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PatientTypeMatrixRepository"/> class.
        /// </summary>
        ~PatientTypeMatrixRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPatientTypeMatrixRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }   

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<PatientTypeMatrix> All
        {
            get { return _dbContext.PatientTypeMatrix; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<PatientTypeMatrix> AllIncluding(params Expression<Func<PatientTypeMatrix, object>>[] includeProperties)
        {
            IQueryable<PatientTypeMatrix> query = _dbContext.PatientTypeMatrix;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public PatientTypeMatrix Find(long id)
        {
            return _dbContext.PatientTypeMatrix.Find(id);
        }

        /// <summary>
        /// Gets the patient type matrix by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<PatientTypeMatrix> GetPatientTypeMatrixByCustomerId(long customerId)
        {            
            return All.Where(u => u.CustomerId == customerId && u.IsRemove != true);
        }

        /// <summary>
        /// Gets the type of the matrix by patient.
        /// </summary>
        /// <param name="patientTypeIds">The patient type ids.</param>
        /// <returns></returns>
        public IQueryable<PatientTypeMatrix> GetMatrixByPatientType(List<long> patientTypeIds)
        {
            return this.All.Where(q => patientTypeIds.Contains((long)q.PatientType) && q.IsRemove != true);
        }

        /// <summary>
        /// Gets the patient type by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
		public List<PatientTypeMatrix> GetPatientTypeMatrixByCustomerId(List<long> customerIds)
        {
			return All.Where(q => customerIds.Contains((long)q.CustomerId) && q.IsRemove != true).OrderBy(x => x.PatientType).ToList();
        }
       
        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="patientTypeMatrix">The patient type matrix.</param>
        public void InsertOrUpdate(PatientTypeMatrix patientTypeMatrix)
        {
            if (patientTypeMatrix.PatientTypeMatrixID == default(long))
            {
                // New entity
                _dbContext.PatientTypeMatrix.Add(patientTypeMatrix);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.PatientTypeMatrices.Attach(patientTypeMatrix);
                _dbContext.DataContext.Entry(patientTypeMatrix).State = EntityState.Modified;
            }
        }       

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var patientTypeMatrix = _dbContext.PatientTypeMatrix.Find(id);
            _dbContext.PatientTypeMatrix.Remove(patientTypeMatrix);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }        
    }   
}