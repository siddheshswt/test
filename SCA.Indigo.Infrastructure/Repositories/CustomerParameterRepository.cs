// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-18-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="CustomerParameterRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;
    using System.Collections.Generic;

    /// <summary>
    /// Customer Parameter Interface
    /// </summary>
    public interface ICustomerParameterRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<CustomerParameter> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<CustomerParameter> AllIncluding(params Expression<Func<CustomerParameter, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CustomerParameter Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="customerParameter">The customer parameter.</param>
        void InsertOrUpdate(CustomerParameter customerParameter);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets all customer parameters.
        /// </summary>
        /// <returns></returns>
        IQueryable<CustomerParameter> GetAllCustomerParameters();

        /// <summary>
        /// Gets all customer parameters by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<CustomerParameter> GetAllCustomerParametersByCustomerId(long customerId);

        /// <summary>
        /// Gets all customer parameters by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<CustomerParameter> GetAllCustomerParametersByCustomerId(List<long> customerIds);
    }

    /// <summary>
    /// Customer Parameter Repository
    /// </summary>
    public class CustomerParameterRepository : ICustomerParameterRepository
    {
        /// <summary>
        /// The database context
        /// </summary>
        private static DBContext DbContext;
        /// <summary>
        /// The unit of work
        /// </summary>
        private UnitOfWork UnitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerParameterRepository"/> class.
        /// </summary>
        public CustomerParameterRepository()
        {
            DbContext = new DBContext();
            UnitOfWork = DbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CustomerParameterRepository"/> class.
        /// </summary>
        ~CustomerParameterRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork ICustomerParameterRepository.UnitOfWork
        {
            get { return UnitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<CustomerParameter> All
        {
            get { return DbContext.CustomerParameters; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<CustomerParameter> AllIncluding(params Expression<Func<CustomerParameter, object>>[] includeProperties)
        {
            IQueryable<CustomerParameter> query = DbContext.CustomerParameters;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerParameter Find(long id)
        {
            return DbContext.CustomerParameters.Find(id);
        }

        /// <summary>
        /// Gets all customer parameters.
        /// </summary>
        /// <returns></returns>
        public IQueryable<CustomerParameter> GetAllCustomerParameters()
        {
            return All;
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="customerParameter">The customer parameter.</param>
        public void InsertOrUpdate(CustomerParameter customerParameter)
        {
            if (customerParameter.CustomerID == default(long))
            {
                // New entity
                DbContext.CustomerParameters.Add(customerParameter);
            }
            else
            {
                // Existing entity
                DbContext.DataContext.Entry(customerParameter).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var customerParameter = DbContext.CustomerParameters.Find(id);
            DbContext.CustomerParameters.Remove(customerParameter);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            UnitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected static void DisposeCore()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        }

        /// <summary>
        /// Gets all customer parameters by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<CustomerParameter> GetAllCustomerParametersByCustomerId(long customerId)
        {
            return All.Where(q => q.CustomerID == customerId);
        }

        /// <summary>
        /// Gets all customer parameters by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<CustomerParameter> GetAllCustomerParametersByCustomerId(List<long> customerIds)
        {
            return All.Where(q => customerIds.Contains(q.CustomerID));
        } 
    }
}