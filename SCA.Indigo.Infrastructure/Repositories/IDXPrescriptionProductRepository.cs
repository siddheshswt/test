// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXPrescriptionProductRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    using DataEntity = System.Data.Entity;

    /// <summary>
    /// IIDX Prescription Product Repository
    /// </summary>
    public interface IIDXPrescriptionProductRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXPrescriptionProduct> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> AllIncluding(params Expression<Func<IDXPrescriptionProduct, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXPrescriptionProduct Find(long id);

        /// <summary>
        /// Gets the product identifier list by prescription identifier.
        /// </summary>
        /// <param name="prescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> GetProductIdListByPrescriptionId(long prescriptionId);

        /// <summary>
        /// Gets the product identifier list by prescription identifier.
        /// </summary>
        /// <param name="prescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> GetProductIdListByPrescriptionId(List<long> prescriptionId);

        /// <summary>
        /// Gets the products by patient identifier.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> GetProductsByPatientId(long patientId);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxPrescriptionProduct">The index prescription product.</param>
        void InsertOrUpdate(IDXPrescriptionProduct idxPrescriptionProduct);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the prescription product by identifier.
        /// </summary>
        /// <param name="IDXPrescriptionProductId">The index prescription product identifier.</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> GetPrescriptionProductById(long IDXPrescriptionProductId);

        /// <summary>
        /// Checks the product exist.
        /// </summary>
        /// <param name="ProductId">The product identifier.</param>
        /// <param name="PrescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> CheckProductExist(long? ProductId, long PrescriptionId);
    }

    /// <summary>
    /// IIDX Prescription Product Repository
    /// </summary>
    public class IDXPrescriptionProductRepository : IIDXPrescriptionProductRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXPrescriptionProductRepository"/> class.
        /// </summary>
        public IDXPrescriptionProductRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXPrescriptionProductRepository"/> class.
        /// </summary>
        ~IDXPrescriptionProductRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        public UnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXPrescriptionProduct> All
        {
            get { return _dbContext.IDXPrescriptionProducts; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> AllIncluding(params Expression<Func<IDXPrescriptionProduct, object>>[] includeProperties)
        {
            IQueryable<IDXPrescriptionProduct> query = _dbContext.IDXPrescriptionProducts;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXPrescriptionProduct Find(long id)
        {
            return _dbContext.IDXPrescriptionProducts.Find(id);
        }

        /// <summary>
        /// Gets the product identifier list by prescription identifier.
        /// </summary>
        /// <param name="prescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> GetProductIdListByPrescriptionId(long prescriptionId)
        {
            return All.Where(q => q.PrescriptionId == prescriptionId);
        }

        /// <summary>
        /// Gets the product identifier list by prescription identifier.
        /// </summary>
        /// <param name="prescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> GetProductIdListByPrescriptionId(List<long> prescriptionId)
        {
            return All.Where(q => prescriptionId.Contains((long)q.PrescriptionId));
        }

        /// <summary>
        /// Gets the prescription product by identifier.
        /// </summary>
        /// <param name="IDXPrescriptionProductId">The index prescription product identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> GetPrescriptionProductById(long IDXPrescriptionProductId)
        {
            return All.Where(p => p.IDXPrescriptionProductId == IDXPrescriptionProductId);
        }

        /// <summary>
        /// Checks the product exist.
        /// </summary>
        /// <param name="ProductId">The product identifier.</param>
        /// <param name="PrescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> CheckProductExist(long? ProductId, long PrescriptionId)
        {
            return All.Where(p => p.PrescriptionId == PrescriptionId && p.ProductId == ProductId && p.IsRemoved != true);
        }

        /// <summary>
        /// Gets the products by patient identifier.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> GetProductsByPatientId(long patientId)
        {
            return All.Where(q => q.Prescription.PatientId == patientId);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxPrescriptionProduct">The index prescription product.</param>
        public void InsertOrUpdate(IDXPrescriptionProduct idxPrescriptionProduct)
        {
            if (idxPrescriptionProduct.IDXPrescriptionProductId == default(long))
            {
                _dbContext.IDXPrescriptionProducts.Add(idxPrescriptionProduct);
            }
            else
            { 
                _dbContext.DataContext.Entry(idxPrescriptionProduct).State = DataEntity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxprescriptionproduct = _dbContext.IDXPrescriptionProducts.Find(id);
            _dbContext.IDXPrescriptionProducts.Remove(idxprescriptionproduct);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }   
}