// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 03-25-2015
//
// Last Modified By : Mamatha Shetty
// Last Modified On : 03-25-2015
// ***********************************************************************
// <copyright file="AuditLogRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SCA.Indigo.Model;

    /// <summary>
    /// AuditLog Repository
    /// </summary>
    public class AuditLogRepository : IAuditLogRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IAuditLogRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditLogRepository"/> class.
        /// </summary>
        public AuditLogRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<AuditLog> All
        {
            get { return _dbContext.AuditLogs; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<AuditLog> AllIncluding(params Expression<Func<AuditLog, object>>[] includeProperties)
        {
            IQueryable<AuditLog> query = _dbContext.AuditLogs;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        /// <summary>
        /// Gets the audit log by audit log identifier.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public IQueryable<AuditLog> GetAuditLogByAuditLogId(List<long> ids)
        {
            
            return All.Where(q => ids.Contains(q.AuditLogId));
            
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public AuditLog Find(long id)
        {
            return _dbContext.AuditLogs.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="auditLog">The audit log.</param>
        public void InsertOrUpdate(AuditLog auditLog)
        {
            if (auditLog.AuditLogId == default(long))
            {
                // New entity
                _dbContext.AuditLogs.Add(auditLog);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.AuditLogs.Attach(auditLog);
                _dbContext.DataContext.Entry(auditLog).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var auditLog = _dbContext.AuditLogs.Find(id);
            _dbContext.AuditLogs.Remove(auditLog);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="AuditLogRepository"/> class.
        /// </summary>
        ~AuditLogRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }        
    }

    /// <summary>
    /// Interface AuditLogRepository
    /// </summary>
    public interface IAuditLogRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<AuditLog> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<AuditLog> AllIncluding(params Expression<Func<AuditLog, object>>[] includeProperties);

        /// <summary>
        /// Gets the audit log by audit log identifier.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        IQueryable<AuditLog> GetAuditLogByAuditLogId(List<long> ids);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        AuditLog Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        void InsertOrUpdate(AuditLog user);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();        
    }
}