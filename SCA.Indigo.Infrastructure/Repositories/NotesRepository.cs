// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Arvind Nishad
// Created          : 29-04-2015

// ***********************************************************************
// <copyright file="NotesRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;
    using System.Collections.Generic;

    /// <summary>
    /// Notes Repository
    /// </summary>
    public interface INotesRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Note> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Note> AllIncluding(params Expression<Func<Note, object>>[] includeProperties);

        /// <summary>
        /// Finds the list of specified identifier.
        /// </summary>
        /// <param name="id">List of identifier.</param>
        /// <returns></returns>
        IQueryable<Note> FindAll(List<long> id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="Notes">The notes.</param>
        void InsertOrUpdate(Note Notes);

        /// <summary>
        /// Gets the notes by order identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        List<Note> GetNotesByOrderId(long orderId);

        /// <summary>
        /// Gets the notes by carehome identifier.
        /// </summary>
        /// <param name="carehomeId">The carehome identifier.</param>
        /// <returns></returns>
        List<Note> GetNotesByCarehomeId(long carehomeId);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();       
    }

    /// <summary>
    /// Notes Repository
    /// </summary>
    public class NotesRepository : INotesRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The unit of work
        /// </summary>
        private UnitOfWork UnitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesRepository"/> class.
        /// </summary>
        public NotesRepository()
        {
            _dbContext = new DBContext();
            UnitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="NotesRepository"/> class.
        /// </summary>
        ~NotesRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork INotesRepository.UnitOfWork
        {
            get { return UnitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Note> All
        {
            get { return _dbContext.Notes; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Note> AllIncluding(params Expression<Func<Note, object>>[] includeProperties)
        {
            IQueryable<Note> query = _dbContext.Notes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the list of specified identifier.
        /// </summary>
        /// <param name="id">List of identifier.</param>
        /// <returns></returns>
        public IQueryable<Note> FindAll(List<long> id)
        {
            return All.Where(q => id.Contains(q.NoteId));            
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="notes">The notes.</param>
        public void InsertOrUpdate(Note notes)
        {
            if (notes.NoteId == default(long))
            {
                // New entity
                _dbContext.Notes.Add(notes);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(notes).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Gets the notes by order identifier.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        public List<Note> GetNotesByOrderId(long orderId) 
        {          
			List<Note>  notesList = All.Where(q => q.NoteTypeId == orderId).ToList();
            return notesList;
        }

        /// <summary>
        /// Gets the notes by carehome identifier.
        /// </summary>
        /// <param name="carehomeId">The carehome identifier.</param>
        /// <returns></returns>
        public List<Note> GetNotesByCarehomeId(long carehomeId)
        {
            List<Note> notesList = All.Where(q => q.NoteTypeId == carehomeId).ToList();
            return notesList;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var note = _dbContext.Notes.Find(id);
            _dbContext.Notes.Remove(note);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }     
    }   
}