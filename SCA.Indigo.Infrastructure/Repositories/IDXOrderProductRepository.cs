// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXOrderProductRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Order Product
    /// </summary>
    public class IDXOrderProductRepository : IIDXOrderProductRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXOrderProductRepository"/> class.
        /// </summary>
        public IDXOrderProductRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXOrderProductRepository"/> class.
        /// </summary>
        ~IDXOrderProductRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXOrderProductRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXOrderProduct> All
        {
            get { return _dbContext.IDXOrderProducts; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXOrderProduct> AllIncluding(params Expression<Func<IDXOrderProduct, object>>[] includeProperties)
        {
            IQueryable<IDXOrderProduct> query = _dbContext.IDXOrderProducts;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets order products by order id's
        /// </summary>
        /// <param name="orderIds"></param>
        /// <returns></returns>
        public IQueryable<IDXOrderProduct> GetOrderProductsByOrderIds (List<long> orderIds)
        {
            return All.Where(q => orderIds.Contains((long)q.OrderId)); 
        }

        /// <summary>
        /// Gets All the Order Products in the OrderId
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <returns>IQueryable of IDXOrderProduct </returns>
        public IQueryable<IDXOrderProduct> GetOrderProductsByOrderId(long orderId)
        {
            return All.Where(q=>q.OrderId == orderId);
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXOrderProduct Find(long id)
        {
            return _dbContext.IDXOrderProducts.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxOrderProduct">The index order product.</param>
        public void InsertOrUpdate(IDXOrderProduct idxOrderProduct)
        {
            if (idxOrderProduct.OrderProductId == default(long))
            {
                // New entity
                _dbContext.IDXOrderProducts.Add(idxOrderProduct);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxOrderProduct).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxOrderProduct = _dbContext.IDXOrderProducts.Find(id);
            _dbContext.IDXOrderProducts.Remove(idxOrderProduct);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }

    /// <summary>
    /// Order Product
    /// </summary>
    public interface IIDXOrderProductRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXOrderProduct> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXOrderProduct> AllIncluding(params Expression<Func<IDXOrderProduct, object>>[] includeProperties);

        /// <summary>
        /// Get Order Products By OrderIds
        /// </summary>
        /// <param name="orderIds">List of Order Ids</param>
        /// <returns></returns>
        IQueryable<IDXOrderProduct> GetOrderProductsByOrderIds(List<long> orderIds);

        /// <summary>
        /// Get Order Products By OrderId
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns>Iqueryable of IDXOrderProduct </returns>
        IQueryable<IDXOrderProduct> GetOrderProductsByOrderId(long orderId);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXOrderProduct Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxOrderProduct">The index order product.</param>
        void InsertOrUpdate(IDXOrderProduct idxOrderProduct);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }
}