// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-02-2015
// ***********************************************************************
// <copyright file="PatientRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{    
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Patient Repository
    /// </summary>
    public interface IPatientRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Patient> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Patient> AllIncluding(params Expression<Func<Patient, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Patient Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="patient">The patient.</param>
        void InsertOrUpdate(Patient patient);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the minimum frequency.
        /// </summary>
        /// <param name="patientIdList">The patient identifier list.</param>
        /// <returns></returns>
        long GetMinimumFrequency(List<long> patientIdList);

        /// <summary>
        /// Gets the patients by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<Patient> GetPatientsByCustomer(long customerId);

        /// <summary>
        /// Gets the care home patients.
        /// </summary>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <returns></returns>
        IQueryable<Patient> GetCareHomePatients(long careHomeId);

        /// <summary>
        /// Gets the patients by customer.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns></returns>
        IQueryable<Patient> GetPatientsByCustomer(List<long> customerIds);

        /// <summary>
        /// Gets the care home patients.
        /// </summary>
        /// <param name="careHomeIds">The care home ids.</param>
        /// <returns></returns>
        IQueryable<Patient> GetCareHomePatients(List<long> careHomeIds);

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<Patient> GetPatient(long patientId);

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="loadIds">load Ids</param>
        /// <returns>Patient</returns>
        IQueryable<Patient> GetPatientByLoadId(List<long> loadIds);

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<Patient> GetPatient(List<long> patientId);

        /// <summary>
        /// Determines whether [is reason code mapped to patient] [the specified customer identifier].
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="reasonCodeId">The reason code identifier.</param>
        /// <returns></returns>
        bool IsReasonCodeMappedToPatient(long customerId, long reasonCodeId);

        /// <summary>
        /// Gets the patients by customer and NDD.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="NDD">The NDD.</param>
        /// <returns></returns>
        IQueryable<Patient> GetPatientsByCustomerAndNDD(long customerId, DateTime NDD);

        /// <summary>
        /// Gets the patients by sap identifier.
        /// </summary>
        /// <param name="patientSAPId">The patient sap identifier.</param>
        /// <returns></returns>
        IQueryable<Patient> GetPatientsBySAPId(List<string> patientSAPId);


		/// <summary>
		/// Gets the patients ND
		/// </summary>
		/// <param name="patientId">The patient Id.</param>
		/// <returns></returns>
		DateTime? GetPatientsNDD(long patientId);
    }

    /// <summary>
    /// Patient Repository
    /// </summary>
    public class PatientRepository : IPatientRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientRepository"/> class.
        /// </summary>
        public PatientRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PatientRepository"/> class.
        /// </summary>
        ~PatientRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPatientRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Patient> All
        {
            get { return _dbContext.Patients; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Patient> AllIncluding(params Expression<Func<Patient, object>>[] includeProperties)
        {
            IQueryable<Patient> query = _dbContext.Patients;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Patient Find(long id)
        {
            return _dbContext.Patients.Find(id);
        }

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetPatient(long patientId)
        {
            return GetPatient(new List<long>() { patientId });
        }

        /// <summary>
        /// Gets the patients by customer and NDD.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="NDD">The NDD.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetPatientsByCustomerAndNDD(long customerId, DateTime NDD)
        {
            return All.Where(q => q.CustomerId == customerId && q.NextDeliveryDate <= NDD);
        }

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetPatient(List<long> patientId)
        {
            return All.Where(q => patientId.Contains(q.PatientId));
        }

        /// <summary>
        /// Gets the patient.
        /// </summary>
        /// <param name="patientId">load Ids</param>
        /// <returns>Patient</returns>
        public IQueryable<Patient> GetPatientByLoadId(List<long> loadIds)
        {
            return All.Where(q => loadIds.Contains(Convert.ToInt64(q.LoadId, CultureInfo.InvariantCulture)));
        }


        /// <summary>
        /// Determines whether [is reason code mapped to patient] [the specified customer identifier].
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="reasonCodeId">The reason code identifier.</param>
        /// <returns></returns>
        public bool IsReasonCodeMappedToPatient(long customerId, long reasonCodeId)
        {
            return All.Where(q => q.CustomerId == customerId && q.ReasonCodeID == reasonCodeId).Any();
        }

        /// <summary>
        /// Gets the patients by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetPatientsByCustomer(long customerId)
        {
            if (customerId == 0)
            {
                return All;
            }
            else
            {
                return All.Where(q => q.CustomerId == customerId);
            }
        }

        /// <summary>
        /// Gets the patients by customer.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetPatientsByCustomer(List<long> customerIds)
        {
            return All.Where(q => customerIds.Contains((long)q.CustomerId));
        }

        /// <summary>
        /// Gets the care home patients.
        /// </summary>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetCareHomePatients(long careHomeId)
        {
            return All.Where(q => q.CareHomeId == careHomeId);
        }

        /// <summary>
        /// Gets the care home patients.
        /// </summary>
        /// <param name="careHomeIds">The care home ids.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetCareHomePatients(List<long> careHomeIds)
        {
            return All.Where(q => careHomeIds.Contains((long)q.CareHomeId));
        }

        /// <summary>
        /// Gets the patients by sap identifier.
        /// </summary>
        /// <param name="patientSAPId">The patient sap identifier.</param>
        /// <returns></returns>
        public IQueryable<Patient> GetPatientsBySAPId(List<string> patientSAPId)
        {
            return All.Where(q => patientSAPId.Contains(q.SAPPatientNumber));
        }

        /// <summary>
        /// Return the min Patient Frequency from the list of patients
        /// </summary>
        /// <param name="patientIdList">The patient identifier list.</param>
        /// <returns>
        /// Minimum Frequency among the Prescription Products of patient.
        /// </returns>
        public long GetMinimumFrequency(List<long> patientIdList)
        {
            List<Patient> patients = this.All.Where(q => patientIdList.Contains(q.PatientId)).ToList();
            return patients.Min(d => Convert.ToInt64(d.DeliveryFrequency, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="patient">The patient.</param>
        public void InsertOrUpdate(Patient patient)
        {
            if (patient.PatientId == default(long))
            {                
                _dbContext.Patients.Add(patient);
            }
            else
            {             
                _dbContext.DataContext.Entry(patient).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var patient = _dbContext.Patients.Find(id);
            _dbContext.Patients.Remove(patient);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

		/// <summary>
		/// Return Patient NDD
		/// </summary>
		/// <param name="patientId"></param>
		/// <returns></returns>
		public DateTime? GetPatientsNDD(long patientId)
		{
			var nextDeliveryDate = this.All.Where(q=>q.PatientId == patientId).Select(r=>r.NextDeliveryDate).FirstOrDefault();			
			return nextDeliveryDate;
		}
	}
}