﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Saurabh Mayekar
// Created          : 13-09-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="IDXUserCarehomeRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    public interface IIDXUserCareHomeRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXUserCarehome> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXUserCarehome> AllIncluding(params Expression<Func<IDXUserCarehome, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXUserCarehome Find(long id);

        /// <summary>
        /// Gets the IDXUserCarehome by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IQueryable<IDXUserCarehome> FindByUserId(string userId);

        /// <summary>
        /// Gets the IDXUserCarehome by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IDXUserCarehome FindByUserIdCareHomeId(string userId,string careHomeId);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        void InsertOrUpdate(IDXUserCarehome userActionMonitor);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Users Repository
    /// </summary>
    public class IDXUserCareHomeRepository : IIDXUserCareHomeRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;

        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXUserCarehome"/> class.
        /// </summary>
        public IDXUserCareHomeRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXUserCarehome"/> class.
        /// </summary>
        ~IDXUserCareHomeRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXUserCareHomeRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXUserCarehome> All
        {
            get { return _dbContext.IDXUserCarehome; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXUserCarehome> AllIncluding(params Expression<Func<IDXUserCarehome, object>>[] includeProperties)
        {
            IQueryable<IDXUserCarehome> query = _dbContext.IDXUserCarehome;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXUserCarehome Find(long id)
        {
            return _dbContext.IDXUserCarehome.Find(id);
        }

        /// <summary>
        /// Gets the IDXUserCarehome by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXUserCarehome> FindByUserId(string userId)
        {
            return All.Where(u => u.UserId.ToString() == userId);
        }

        /// <summary>
        /// Gets the IDXUserCarehome by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public IDXUserCarehome FindByUserIdCareHomeId(string userId, string carehomeId)
        {
            return All.Where(u => u.UserId.ToString() == userId && u.CarehomeId.ToString() == carehomeId).FirstOrDefault();
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        public void InsertOrUpdate(IDXUserCarehome idxUserCarehome)
        {
            if (idxUserCarehome.IDXUserCarehomeId == default(int))
            {
                // New entity
                _dbContext.IDXUserCarehome.Add(idxUserCarehome);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.IDXUserCarehomes.Attach(idxUserCarehome);
                _dbContext.DataContext.Entry(idxUserCarehome).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var user = _dbContext.IDXUserCarehome.Find(id);
            _dbContext.IDXUserCarehome.Remove(user);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
