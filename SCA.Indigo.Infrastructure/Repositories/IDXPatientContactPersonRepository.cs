// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Siddharth Dilpak
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXPatientContactPersonRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// IIDX Patient Contact Person Repository
    /// </summary>
    public interface IIDXPatientContactPersonRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXPatientContactPerson> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXPatientContactPerson> AllIncluding(params Expression<Func<IDXPatientContactPerson, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXPatientContactPerson Find(long id);

        /// <summary>
        /// Gets all patient contact person.
        /// </summary>
        /// <returns></returns>
        IQueryable<IDXPatientContactPerson> GetAllPatientContactPerson();

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxPatientContactPerson">The index patient contact person.</param>
        void InsertOrUpdate(IDXPatientContactPerson idxPatientContactPerson);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// IIDX Patient Contact Person Repository
    /// </summary>
    public class IDXPatientContactPersonRepository : IIDXPatientContactPersonRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXPatientContactPersonRepository"/> class.
        /// </summary>
        public IDXPatientContactPersonRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXPatientContactPersonRepository"/> class.
        /// </summary>
        ~IDXPatientContactPersonRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXPatientContactPersonRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXPatientContactPerson> All
        {
            get { return _dbContext.IDXPatientContactPersons; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXPatientContactPerson> AllIncluding(params Expression<Func<IDXPatientContactPerson, object>>[] includeProperties)
        {
            IQueryable<IDXPatientContactPerson> query = _dbContext.IDXPatientContactPersons;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets all patient contact person.
        /// </summary>
        /// <returns></returns>
        public IQueryable<IDXPatientContactPerson> GetAllPatientContactPerson()
        {
            return this.All;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXPatientContactPerson Find(long id)
        {
            return _dbContext.IDXPatientContactPersons.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxPatientContactPerson">The index patient contact person.</param>
        public void InsertOrUpdate(IDXPatientContactPerson idxPatientContactPerson)
        {
            if (idxPatientContactPerson.PateintContactPersonId == default(long))
            {
                // New entity
                _dbContext.IDXPatientContactPersons.Add(idxPatientContactPerson);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxPatientContactPerson).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxPatientContactPerson = _dbContext.IDXPatientContactPersons.Find(id);
            _dbContext.IDXPatientContactPersons.Remove(idxPatientContactPerson);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}