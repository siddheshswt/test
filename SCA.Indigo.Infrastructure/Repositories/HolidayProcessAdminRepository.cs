﻿
// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : sphapale
// Created          : 25-Mar-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="HolidayProcessAdminRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using SCA.Indigo.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Holiday Process Repository
    /// </summary>
    public class HolidayProcessAdminRepository : IHolidayProcessAdminRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IHolidayProcessAdminRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HolidayProcessAdminRepository"/> class.
        /// </summary>
        public HolidayProcessAdminRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<HolidayProcessAdmin> All
        {
            get { return _dbContext.HolidayProcessAdmins; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<HolidayProcessAdmin> AllIncluding(params Expression<Func<HolidayProcessAdmin, object>>[] includeProperties)
        {
            IQueryable<HolidayProcessAdmin> query = _dbContext.HolidayProcessAdmins;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public HolidayProcessAdmin Find(long id)
        {
            return _dbContext.HolidayProcessAdmins.Find(id);
        }


        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="HolidayProcessAdmin">The holiday process admin.</param>
        public void InsertOrUpdate(HolidayProcessAdmin HolidayProcessAdmin)
        {
            if (HolidayProcessAdmin.HolidayProcessID == default(long))
            {
                // New entity
                _dbContext.HolidayProcessAdmins.Add(HolidayProcessAdmin);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.HolidayProcessAdmins.Attach(HolidayProcessAdmin);
                _dbContext.DataContext.Entry(HolidayProcessAdmin).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var customer = _dbContext.HolidayProcessAdmins.Find(id);
            _dbContext.HolidayProcessAdmins.Remove(customer);
        }

        /// <summary>
        /// Gets the detail by identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<HolidayProcessAdmin> GetDetailById(string customerId)
        {
            return All.Where(u => u.CustomerId.ToString() == customerId);
        }


        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="HolidayProcessAdminRepository"/> class.
        /// </summary>
        ~HolidayProcessAdminRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }

    /// <summary>
    /// Holiday Process Interface
    /// </summary>
    public interface IHolidayProcessAdminRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<HolidayProcessAdmin> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<HolidayProcessAdmin> AllIncluding(params Expression<Func<HolidayProcessAdmin, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        HolidayProcessAdmin Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="holidayProcessAdmin">The holiday process admin.</param>
        void InsertOrUpdate(HolidayProcessAdmin holidayProcessAdmin);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Gets the detail by identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<HolidayProcessAdmin> GetDetailById(string customerId);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

    }


}