﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Siddharth Dilpak
// Created          : 23-03-2015
//
// Last Modified By : Siddharth Dilpak
// Last Modified On : 23-03-2015
// ***********************************************************************
// <copyright file="OrderDisclaimerRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Order Disclaimer 
    /// </summary>
    public class OrderDisclaimerRepository : IOrderDisclaimerRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IOrderDisclaimerRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderDisclaimerRepository"/> class.
        /// </summary>
        public OrderDisclaimerRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<OrderDisclaimer> All
        {
            get { return _dbContext.OrderDisclaimers; }
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public OrderDisclaimer Find(long id)
        {
            return _dbContext.OrderDisclaimers.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="orderDisclaimer">The order disclaimer.</param>
        public void InsertOrUpdate(OrderDisclaimer orderDisclaimer)
        {
            if (orderDisclaimer.OrderDisclaimerId == default(long))
            {
                // New entity
                _dbContext.OrderDisclaimers.Add(orderDisclaimer);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.OrderDisclaimers.Attach(orderDisclaimer);
                _dbContext.DataContext.Entry(orderDisclaimer).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var orderDisclaimer = _dbContext.OrderDisclaimers.Find(id);
            _dbContext.OrderDisclaimers.Remove(orderDisclaimer);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="OrderDisclaimerRepository"/> class.
        /// </summary>
        ~OrderDisclaimerRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }
            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }

    /// <summary>
    /// Order Disclaimer 
    /// </summary>
    public interface IOrderDisclaimerRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<OrderDisclaimer> All { get; }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        OrderDisclaimer Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="orderDisclaimer">The order disclaimer.</param>
        void InsertOrUpdate(OrderDisclaimer orderDisclaimer);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

    }
}