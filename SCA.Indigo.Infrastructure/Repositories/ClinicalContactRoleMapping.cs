﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SCA.Indigo.Model;

namespace SCA.Indigo.Infrastructure.Repositories
{
    /// <summary>
    /// Clinical Contact Role Mapping Repository
    /// </summary>
    public class ClinicalContactRoleMappingRepository : IClinicalContactRoleMappingRepository
    {
        /// <summary>
        /// The database context
        /// </summary>
        private static DBContext DbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicalContactRoleMappingRepository"/> class.
        /// </summary>
        public ClinicalContactRoleMappingRepository()
        {
            DbContext = new DBContext();
            _unitOfWork = DbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ClinicalContactRoleMappingRepository"/> class.
        /// </summary>
        ~ClinicalContactRoleMappingRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<ClinicalContactRoleMapping> All
        {
            get { return DbContext.ClinicalContactRoleMappings; }
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IClinicalContactRoleMappingRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<ClinicalContactRoleMapping> AllIncluding(params Expression<Func<ClinicalContactRoleMapping, object>>[] includeProperties)
        {
            IQueryable<ClinicalContactRoleMapping> query = DbContext.ClinicalContactRoleMappings;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ClinicalContactRoleMapping Find(long id)
        {
            return DbContext.ClinicalContactRoleMappings.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactRoleMapping">The clinical contact role mapping.</param>
        public void InsertOrUpdate(ClinicalContactRoleMapping clinicalContactRoleMapping)
        {
            if (clinicalContactRoleMapping.ClinicalContactRoleMappingId == default(long))
            {
                // New entity
                DbContext.ClinicalContactRoleMappings.Add(clinicalContactRoleMapping);
            }
            else
            {
                // Existing entity
                DbContext.DataContext.Entry(clinicalContactRoleMapping).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var clinicalContactRoleMapping = DbContext.ClinicalContactRoleMappings.Find(id);
            DbContext.ClinicalContactRoleMappings.Remove(clinicalContactRoleMapping);
        }

        /// <summary>
        /// Gets the customers clinical contact role mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<ClinicalContactRoleMapping> GetCustomersClinicalContactRoleMapping(long customerId)
        {
            if (customerId == 0)
            {
                return All;
            }
            else
            {
                return All.Where(q => q.CustomerId == customerId);
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected static void DisposeCore()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        }

        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <param name="parentId">The parent identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
       public IQueryable<ClinicalContactRoleMapping> GetParent(long parentId , long customerId)
        {
            if (parentId == 0)
            {
                return All;
            }
            else
            {
                return All.Where(q => q.ChildListId == parentId && q.CustomerId == customerId && q.ParentListId == 0);
            }
        }
    }

    /// <summary>
    /// Interface Clinical Contact Role Mapping Repository
    /// </summary>
    public interface IClinicalContactRoleMappingRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<ClinicalContactRoleMapping> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<ClinicalContactRoleMapping> AllIncluding(params Expression<Func<ClinicalContactRoleMapping, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ClinicalContactRoleMapping Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactRoleMapping">The clinical contact role mapping.</param>
        void InsertOrUpdate(ClinicalContactRoleMapping clinicalContactRoleMapping);

        /// <summary>
        /// Gets the customers clinical contact role mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<ClinicalContactRoleMapping> GetCustomersClinicalContactRoleMapping(long customerId);

        /// <summary>
        /// Gets the parent.
        /// </summary>
        /// <param name="parentId">The parent identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<ClinicalContactRoleMapping> GetParent(long parentId, long customerId);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }
}
