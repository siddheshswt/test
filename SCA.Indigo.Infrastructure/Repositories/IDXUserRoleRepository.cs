// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXUserRoleRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// IIDX User Role Repository 
    /// </summary>
    public interface IIDXUserRoleRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXUserRole> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXUserRole> AllIncluding(params Expression<Func<IDXUserRole, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXUserRole Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxUserRole">The index user role.</param>
        void InsertOrUpdate(IDXUserRole idxUserRole);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the user role by role identifier.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        IQueryable<IDXUserRole> GetUserRoleByRoleId(long roleId);

        /// <summary>
        /// Gets the user role by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IQueryable<IDXUserRole> GetUserRoleByUserId(Guid userId);
    }

    /// <summary>
    /// IIDX User Role Repository 
    /// </summary>
    public class IDXUserRoleRepository : IIDXUserRoleRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;


        /// <summary>
        /// Initializes a new instance of the <see cref="IDXUserRoleRepository"/> class.
        /// </summary>
        public IDXUserRoleRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXUserRoleRepository"/> class.
        /// </summary>
        ~IDXUserRoleRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXUserRoleRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXUserRole> All
        {
            get { return _dbContext.IDXUserRoles; }
        }

        /// <summary>
        /// Gets the user role by role identifier.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXUserRole> GetUserRoleByRoleId(long roleId)
        {
            return _dbContext.IDXUserRoles.Where(d => d.RoleID == roleId);
        }

        /// <summary>
        /// Gets the user role by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXUserRole> GetUserRoleByUserId(Guid userId)
        {
            return _dbContext.IDXUserRoles.Where(d => d.UserID == userId);
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXUserRole> AllIncluding(params Expression<Func<IDXUserRole, object>>[] includeProperties)
        {
            IQueryable<IDXUserRole> query = _dbContext.IDXUserRoles;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXUserRole Find(long id)
        {
            return _dbContext.IDXUserRoles.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxuserrole">The idxuserrole.</param>
        public void InsertOrUpdate(IDXUserRole idxuserrole)
        {
            if (idxuserrole.IDXUserRoleID == default(long))
            {
                // New entity
                _dbContext.IDXUserRoles.Add(idxuserrole);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxuserrole).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxuserrole = _dbContext.IDXUserRoles.Find(id);
            _dbContext.IDXUserRoles.Remove(idxuserrole);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }
}