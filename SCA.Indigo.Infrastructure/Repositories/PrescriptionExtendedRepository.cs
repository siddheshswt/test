﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sagar Yerva
// Created          : 02-26-2015
//
// Last Modified By : Sagar yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="PrescriptionExtendedRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************


namespace SCA.Indigo.Infrastructure.Repositories
{    
    using System;
    using System.Data.Entity;
    using System.Linq;

    using SCA.Indigo.Model;

    /// <summary>
    /// Prescription Extended Repository
    /// </summary>
    public interface IPrescriptionExtendedRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<PrescriptionExtended> All { get; }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        PrescriptionExtended Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="comment">The comment.</param>
        void InsertOrUpdate(PrescriptionExtended comment);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the nurse detailsby prescription identifier.
        /// </summary>
        /// <param name="prescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        PrescriptionExtended GetNurseDetailsbyPrescriptionId(long prescriptionId);
    }

    /// <summary>
    /// Prescription Extended Repository
    /// </summary>
    public class PrescriptionExtendedRepository : IPrescriptionExtendedRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrescriptionExtendedRepository"/> class.
        /// </summary>
        public PrescriptionExtendedRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PrescriptionExtendedRepository"/> class.
        /// </summary>
        ~PrescriptionExtendedRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPrescriptionExtendedRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<PrescriptionExtended> All
        {
            get { return _dbContext.ExtendedPrescriptions; }
        }

        // Implement logic
        /// <summary>
        /// Gets the nurse detailsby prescription identifier.
        /// </summary>
        /// <param name="prescriptionId">The prescription identifier.</param>
        /// <returns></returns>
        public PrescriptionExtended GetNurseDetailsbyPrescriptionId(long prescriptionId)
        {
            return _dbContext.ExtendedPrescriptions.Where(q => q.PrescriptionId == prescriptionId).OrderByDescending(d => d.ModifiedDate).FirstOrDefault();
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public PrescriptionExtended Find(long id)
        {
            return _dbContext.ExtendedPrescriptions.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="comment">The comment.</param>
        public void InsertOrUpdate(PrescriptionExtended comment)
        {
            if (comment.PrescriptionExtendedId == default(long))
            {
                // New entity
                _dbContext.ExtendedPrescriptions.Add(comment);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.PrescriptionExtendeds.Attach(comment);
                _dbContext.DataContext.Entry(comment).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var prescriptionComment = _dbContext.ExtendedPrescriptions.Find(id);
            _dbContext.ExtendedPrescriptions.Remove(prescriptionComment);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
