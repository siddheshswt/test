// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sagar Yerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXPatientMedicalStaffRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// IIDX Patient Medical Staff Repository
    /// </summary>
    public interface IIDXPatientMedicalStaffRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXPatientMedicalStaff> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXPatientMedicalStaff> AllIncluding(params Expression<Func<IDXPatientMedicalStaff, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXPatientMedicalStaff Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxpatientmedicalstaff">The idxpatientmedicalstaff.</param>
        void InsertOrUpdate(IDXPatientMedicalStaff idxpatientmedicalstaff);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
        /// <summary>
        /// Gets all patient medical staff.
        /// </summary>
        /// <returns></returns>
        IQueryable<IDXPatientMedicalStaff> GetAllPatientMedicalStaff();

        /// <summary>
        /// Gets the medical staff information.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <returns></returns>
        IQueryable<IDXPatientMedicalStaff> GetMedicalStaffInformation(long patientId, int listTypeId);
    }

    /// <summary>
    /// IIDX Patient Medical Staff Repository
    /// </summary>
    public class IDXPatientMedicalStaffRepository : IIDXPatientMedicalStaffRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXPatientMedicalStaffRepository"/> class.
        /// </summary>
        public IDXPatientMedicalStaffRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXPatientMedicalStaffRepository"/> class.
        /// </summary>
        ~IDXPatientMedicalStaffRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXPatientMedicalStaffRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all patient medical staff.
        /// </summary>
        /// <returns></returns>
        public IQueryable<IDXPatientMedicalStaff> GetAllPatientMedicalStaff()
        {
            return this.All;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXPatientMedicalStaff> All
        {
            get { return _dbContext.IDXPatientMedicalStaffs; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXPatientMedicalStaff> AllIncluding(params Expression<Func<IDXPatientMedicalStaff, object>>[] includeProperties)
        {
            IQueryable<IDXPatientMedicalStaff> query = _dbContext.IDXPatientMedicalStaffs;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXPatientMedicalStaff Find(long id)
        {
            return _dbContext.IDXPatientMedicalStaffs.Find(id);
        }

        /// <summary>
        /// Gets the medical staff information.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXPatientMedicalStaff> GetMedicalStaffInformation(long patientId, int listTypeId)
        {
            return All.Where(idx => idx.PatientId == patientId); 
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxpatientmedicalstaff">The idxpatientmedicalstaff.</param>
        public void InsertOrUpdate(IDXPatientMedicalStaff idxpatientmedicalstaff)
        {
            if (idxpatientmedicalstaff.IDXPatientMedicalStaffId == default(long))
            {
                // New entity
                _dbContext.IDXPatientMedicalStaffs.Add(idxpatientmedicalstaff);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxpatientmedicalstaff).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxpatientmedicalstaff = _dbContext.IDXPatientMedicalStaffs.Find(id);
            _dbContext.IDXPatientMedicalStaffs.Remove(idxpatientmedicalstaff);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    } 
}