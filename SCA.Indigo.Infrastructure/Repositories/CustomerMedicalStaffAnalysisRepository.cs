// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="CustomerMedicalStaffAnalysisRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Medical Staff Interface
    /// </summary>
    public interface ICustomerMedicalStaffAnalysisRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<CustomerMedicalStaffAnalysis> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<CustomerMedicalStaffAnalysis> AllIncluding(params Expression<Func<CustomerMedicalStaffAnalysis, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CustomerMedicalStaffAnalysis Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="customerMedicalStaffAnalysis">The customer medical staff analysis.</param>
        void InsertOrUpdate(CustomerMedicalStaffAnalysis customerMedicalStaffAnalysis);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the medical staff analysis by list identifier.
        /// </summary>
        /// <param name="listId">The list identifier.</param>
        /// <returns></returns>
        IQueryable<CustomerMedicalStaffAnalysis> GetMedicalStaffAnalysisByListId(long listId);
    }

    /// <summary>
    /// Medical Staff Analysis Repository
    /// </summary>
    public class CustomerMedicalStaffAnalysisRepository : ICustomerMedicalStaffAnalysisRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerMedicalStaffAnalysisRepository"/> class.
        /// </summary>
        public CustomerMedicalStaffAnalysisRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CustomerMedicalStaffAnalysisRepository"/> class.
        /// </summary>
        ~CustomerMedicalStaffAnalysisRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
         UnitOfWork ICustomerMedicalStaffAnalysisRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

         /// <summary>
         /// Gets all.
         /// </summary>
         /// <value>
         /// All.
         /// </value>
        public IQueryable<CustomerMedicalStaffAnalysis> All
        {
            get { return _dbContext.CustomerMedicalStaffAnalysis; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<CustomerMedicalStaffAnalysis> AllIncluding(params Expression<Func<CustomerMedicalStaffAnalysis, object>>[] includeProperties)
        {
            IQueryable<CustomerMedicalStaffAnalysis> query = _dbContext.CustomerMedicalStaffAnalysis;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets the medical staff analysis by list identifier.
        /// </summary>
        /// <param name="listId">The list identifier.</param>
        /// <returns></returns>
        public IQueryable<CustomerMedicalStaffAnalysis> GetMedicalStaffAnalysisByListId(long listId)
        {
            return All.Where(q => q.ListId == listId);
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerMedicalStaffAnalysis Find(long id)
        {
            return _dbContext.CustomerMedicalStaffAnalysis.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="customerMedicalStaffAnalysis">The customer medical staff analysis.</param>
        public void InsertOrUpdate(CustomerMedicalStaffAnalysis customerMedicalStaffAnalysis)
        {
            if (customerMedicalStaffAnalysis.CustomerMedicalStaffAnalysisId == default(long))
            {
                // New entity
                _dbContext.CustomerMedicalStaffAnalysis.Add(customerMedicalStaffAnalysis);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.CustomerMedicalStaffAnalysis.Attach(customerMedicalStaffAnalysis);
                _dbContext.DataContext.Entry(customerMedicalStaffAnalysis).State = EntityState.Modified;               
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var customermedicalstaffanalysis = _dbContext.CustomerMedicalStaffAnalysis.Find(id);
            _dbContext.CustomerMedicalStaffAnalysis.Remove(customermedicalstaffanalysis);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }      
}