// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="CustomerRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Customer Interface 
    /// </summary>
    public interface ICustomerRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Customer> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Customer> AllIncluding(params Expression<Func<Customer, object>>[] includeProperties);

        /// <summary>
        /// Gets the customer by identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        Customer GetCustomerById(long customerId);

        /// <summary>
        /// Gets the customer by sap id.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Customer GetCustomerBySAPId(string customerSapId);

        /// <summary>
        /// Gets the customers for order creation.
        /// </summary>
        /// <returns></returns>
        IQueryable<Customer> GetCustomersForOrderCreation();

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Customer Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void InsertOrUpdate(Customer customer);

        /// <summary>
        /// Gets Country Id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int GetCountryId(long id);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns></returns>
        IQueryable<Customer> GetAllCustomer();

        /// <summary>
        /// Finds and returns the Customer with given SAP Id No need to appnd Zero's else returns NULL
        /// </summary>
        /// <param name="customerSAPId">Customer SAP Id</param>
        /// <returns>Customer</returns>
        Customer FindBySapId(string customerSAPId);     
    }

    /// <summary>
    /// Customer Repository
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        public CustomerRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        ~CustomerRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork ICustomerRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Customer> All
        {
            get { return _dbContext.Customers; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Customer> AllIncluding(params Expression<Func<Customer, object>>[] includeProperties)
        {
            IQueryable<Customer> query = _dbContext.Customers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }
        
        /// <summary>
        /// Get all customer
        /// </summary>
        /// <returns></returns>
        public IQueryable<Customer> GetAllCustomer()
        {
            return this.All;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Customer Find(long id)
        {
            return _dbContext.Customers.Find(id);
        }

        /// <summary>
        /// Gets the customer by identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public Customer GetCustomerById(long customerId)
        {
            var customers = All.Where(q => q.CustomerId == customerId);
            if (customers.Any())
            {
                return customers.First();
            }
            return null;
        }

        /// <summary>
        /// Get customer by sap id.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public Customer GetCustomerBySAPId(string customerSapId)
        {
            var customers = All.Where(q => q.SAPCustomerNumber == customerSapId);
            if (customers.Any())
            {
                return customers.First();
            }
            return null;
        }

        /// <summary>
        /// Gets the customers for order creation.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Customer> GetCustomersForOrderCreation()
        {
            var customers = All.Where(q => q.CustomerParameters.Where(cp => cp.OrderCreationAllowed == true).Any());
            return customers;
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="customer">The customer.</param>
        public void InsertOrUpdate(Customer customer)
        {
            if (customer.CustomerId == default(long))
            {
                // New entity
                _dbContext.Customers.Add(customer);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(customer).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetCountryId(long id)
        {
            var countryId = All.Where(q => q.CustomerId == id).Select(q => q.CountryId).FirstOrDefault();
            if (countryId != null && countryId != 0)
            {
                return countryId.Value;
            }
            return 1;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var customer = _dbContext.Customers.Find(id);
            _dbContext.Customers.Remove(customer);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        /// <summary>
        /// Finds and returns the Customer with given SAP Id No need to appnd Zero's else returns NULL
        /// </summary>
        /// <param name="customerSAPId"></param>
        /// <returns>Customer</returns>
        public Customer FindBySapId(string customerSAPId)
        {
            var customer = All.Where(q => q.SAPCustomerNumber.Contains(customerSAPId)).ToList();
            if(customer != null && customer.Any())
            {
                return customer.First();
            }
            return null;
        }
    }  
}