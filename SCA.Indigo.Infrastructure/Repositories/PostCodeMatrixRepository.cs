﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sagar Yerva
// Created          : 02-13-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="PostCodeRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Infrastructure.Repositories
{    
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// Post Code Matrix Repository
    /// </summary>
    public interface IPostCodeMatrixRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<PostcodeMatrix> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<PostcodeMatrix> AllIncluding(params Expression<Func<PostcodeMatrix, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        PostcodeMatrix Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="postcode">The postcode.</param>
        void InsertOrUpdate(PostcodeMatrix postcode);

        /// <summary>
        /// Gets the post code by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<PostcodeMatrix> GetPostCodeByCustomerId(long customerId);

        /// <summary>
        /// Gets the post code by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="round">The round.</param>
        /// <param name="roundId">The round identifier.</param>
        /// <param name="postcode">The postcode.</param>
        /// <returns></returns>
        IQueryable<PostcodeMatrix> GetPostCodeByCustomerId(long customerId, string round, string roundId, string postcode);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Post Code Matrix Repository
    /// </summary>
    public class PostCodeMatrixRepository : IPostCodeMatrixRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="PostCodeMatrixRepository"/> class.
        /// </summary>
        public PostCodeMatrixRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PostCodeMatrixRepository"/> class.
        /// </summary>
        ~PostCodeMatrixRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IPostCodeMatrixRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<PostcodeMatrix> All
        {
            get { return _dbContext.PostcodeMatrix; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<PostcodeMatrix> AllIncluding(params Expression<Func<PostcodeMatrix, object>>[] includeProperties)
        {
            IQueryable<PostcodeMatrix> query = _dbContext.PostcodeMatrix;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets the post code by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<PostcodeMatrix> GetPostCodeByCustomerId(long customerId)
        {
            return this.All.Where(q => q.CustomerID == customerId);
        }

        /// <summary>
        /// Gets the post code by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="round">The round.</param>
        /// <param name="roundId">The round identifier.</param>
        /// <param name="postcode">The postcode.</param>
        /// <returns></returns>
        public IQueryable<PostcodeMatrix> GetPostCodeByCustomerId(long customerId, string round, string roundId, string postcode)
        {
            return this.All.Where(q => q.CustomerID == customerId && q.Round == round && q.RoundID == roundId && q.Postcode == postcode && q.IsActive == true);
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public PostcodeMatrix Find(long id)
        {
            return _dbContext.PostcodeMatrix.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="postcode">The postcode.</param>
        public void InsertOrUpdate(PostcodeMatrix postcode)
        {
            if (postcode.PostcodeMatrixID == default(long))
            {
                // New entity
                _dbContext.PostcodeMatrix.Add(postcode);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(postcode).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var postcode = _dbContext.PostcodeMatrix.Find(id);
            _dbContext.PostcodeMatrix.Remove(postcode);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
