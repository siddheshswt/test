// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="IDXPatientOrderRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    public interface IIDXPatientOrderRepository : IDisposable
    {
        UnitOfWork UnitOfWork { get; }

        IQueryable<IDXPatientOrder> All { get; }

        IQueryable<IDXPatientOrder> AllIncluding(params Expression<Func<IDXPatientOrder, object>>[] includeProperties);

        IDXPatientOrder Find(long id);

        void InsertOrUpdate(IDXPatientOrder idxPatientOrder);

        void Delete(long id);

        void Save();

        IQueryable<IDXPatientOrder> GetOrders(long linkId, long linkType);

        IQueryable<IDXPatientOrder> GetOrders(List<long> linkIdList, long linkType);
    }

    public class IDXPatientOrderRepository : IIDXPatientOrderRepository
    {
        private static DBContext _dbContext;
        private bool _isDisposed;
        private UnitOfWork _unitOfWork;

        public IDXPatientOrderRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        ~IDXPatientOrderRepository()
        {
            Dispose(false);
        }

        UnitOfWork IIDXPatientOrderRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }
   
        public IQueryable<IDXPatientOrder> All
        {
            get { return _dbContext.IDXPatientOrders; }
        }

        public IQueryable<IDXPatientOrder> AllIncluding(params Expression<Func<IDXPatientOrder, object>>[] includeProperties)
        {
            IQueryable<IDXPatientOrder> query = _dbContext.IDXPatientOrders;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public IDXPatientOrder Find(long id)
        {
            return _dbContext.IDXPatientOrders.Find(id);
        }

        public IQueryable<IDXPatientOrder> GetOrders(long linkId, long linkType)
        {
            return GetOrders(new List<long>() { linkId }, linkType);
        }

        public IQueryable<IDXPatientOrder> GetOrders(List<long> linkIdList, long linkType)
        {
            return All.Where(idx => linkIdList.Contains((long)idx.PatientID) && idx.OrderLinkTypeId == linkType).OrderByDescending(idx => idx.Order.OrderDate);
        }

        public void InsertOrUpdate(IDXPatientOrder idxPatientOrder)
        {
            if (idxPatientOrder.IDXPatientOrderID == default(long))
            {
                // New entity
                _dbContext.IDXPatientOrders.Add(idxPatientOrder);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxPatientOrder).State = System.Data.Entity.EntityState.Modified;
            }
        }

        public void Delete(long id)
        {
            var idxpatientorder = _dbContext.IDXPatientOrders.Find(id);
            _dbContext.IDXPatientOrders.Remove(idxpatientorder);
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        protected static void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }   
}