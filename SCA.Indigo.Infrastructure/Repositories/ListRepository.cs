// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="ListRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// List Repository
    /// </summary>
    public interface IListRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<List> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<List> AllIncluding(params Expression<Func<List, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        List Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="list">The list.</param>
        void InsertOrUpdate(List list);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="listId">The list identifier.</param>
        /// <returns></returns>
        IQueryable<List> GetById(long listId);

        /// <summary>
        /// Gets the by type identifier.
        /// </summary>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <returns></returns>
        IQueryable<List> GetByTypeId(long listTypeId);
    }

    /// <summary>
    /// List Repository
    /// </summary>
    public class ListRepository : IListRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListRepository"/> class.
        /// </summary>
        public ListRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ListRepository"/> class.
        /// </summary>
        ~ListRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IListRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<List> All
        {
            get { return _dbContext.Lists; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<List> AllIncluding(params Expression<Func<List, object>>[] includeProperties)
        {
            IQueryable<List> query = _dbContext.Lists;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public List Find(long id)
        {
            return _dbContext.Lists.Find(id);
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="listId">The list identifier.</param>
        /// <returns></returns>
        public IQueryable<List> GetById(long listId)
        {
            return All.Where(q => q.ListId == listId);
        }

        /// <summary>
        /// Gets the by type identifier.
        /// </summary>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <returns></returns>
        public IQueryable<List> GetByTypeId(long listTypeId)
        {
            return All.Where(q => q.ListTypeId == listTypeId);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="list">The list.</param>
        public void InsertOrUpdate(List list)
        {
            if (list.ListId == default(long))
            {
                // New entity
                _dbContext.Lists.Add(list);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(list).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var list = _dbContext.Lists.Find(id);
            _dbContext.Lists.Remove(list);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }  
}