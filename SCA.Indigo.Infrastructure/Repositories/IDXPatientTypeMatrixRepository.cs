// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXPatientTypeMatrixRepository.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the IDXPatientTypeMatrixRepository file.</summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// IIDX Patient Type Matrix Repository
    /// </summary>
    public interface IIDXPatientTypeMatrixRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXPatientTypeMatrix> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXPatientTypeMatrix> AllIncluding(params Expression<Func<IDXPatientTypeMatrix, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXPatientTypeMatrix Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        void InsertOrUpdate(IDXPatientTypeMatrix user);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// IIDX Patient Type Matrix Repository
    /// </summary>
    public class IDXPatientTypeMatrixRepository : IIDXPatientTypeMatrixRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXPatientTypeMatrixRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXPatientTypeMatrixRepository"/> class.
        /// </summary>
        public IDXPatientTypeMatrixRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXPatientTypeMatrixRepository"/> class.
        /// </summary>
        ~IDXPatientTypeMatrixRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXPatientTypeMatrix> All
        {
            get { return _dbContext.IDXPatientTypeMatrix; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXPatientTypeMatrix> AllIncluding(params Expression<Func<IDXPatientTypeMatrix, object>>[] includeProperties)
        {
            IQueryable<IDXPatientTypeMatrix> query = _dbContext.IDXPatientTypeMatrix;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXPatientTypeMatrix Find(long id)
        {
            return _dbContext.IDXPatientTypeMatrix.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxPatientTypeMatrix">The index patient type matrix.</param>
        public void InsertOrUpdate(IDXPatientTypeMatrix idxPatientTypeMatrix)
        {
            if (idxPatientTypeMatrix.IDXPatientTypeMatrixId == default(long))
            {
                // New entity
                _dbContext.IDXPatientTypeMatrix.Add(idxPatientTypeMatrix);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.IDXPatientTypeMatrices.Attach(idxPatientTypeMatrix);
                _dbContext.DataContext.Entry(idxPatientTypeMatrix).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxPatientTypeMatrix = _dbContext.IDXPatientTypeMatrix.Find(id);
            _dbContext.IDXPatientTypeMatrix.Remove(idxPatientTypeMatrix);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }        
    }
}