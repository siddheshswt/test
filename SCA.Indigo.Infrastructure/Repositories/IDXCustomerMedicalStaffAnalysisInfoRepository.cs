﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 02-05-2015
// ***********************************************************************
// <copyright file="IDXCustomerMedicalStaffAnalysisInfoRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using SCA.Indigo.Model;

    /// <summary>
    /// IIDX Customer MedicalStaff Analysis Info Repository
    /// </summary>
    public interface IIDXCustomerMedicalStaffAnalysisInfoRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXCustomerMedicalStaffAnalysisInfo> All { get; }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxStaff">The index staff.</param>
        void InsertOrUpdate(IDXCustomerMedicalStaffAnalysisInfo idxStaff);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Gets the list identifier by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerMedicalStaffAnalysisInfo> GetListIdByCustomerId(long customerId);

        /// <summary>
        /// Gets the customer medical staff analysis information by list identifier.
        /// </summary>
        /// <param name="listId">The list identifier.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerMedicalStaffAnalysisInfo> GetCustomerMedicalStaffAnalysisInfoByListID(long listId);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// IIDX Customer MedicalStaff Analysis Info Repository
    /// </summary>
   public class IDXCustomerMedicalStaffAnalysisInfoRepository : IIDXCustomerMedicalStaffAnalysisInfoRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXCustomerMedicalStaffAnalysisInfoRepository"/> class.
        /// </summary>
        public IDXCustomerMedicalStaffAnalysisInfoRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXCustomerMedicalStaffAnalysisInfoRepository"/> class.
        /// </summary>
        ~IDXCustomerMedicalStaffAnalysisInfoRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
         UnitOfWork IIDXCustomerMedicalStaffAnalysisInfoRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

         /// <summary>
         /// Gets all.
         /// </summary>
         /// <value>
         /// All.
         /// </value>
        public IQueryable<IDXCustomerMedicalStaffAnalysisInfo> All
        {
            get { return _dbContext.IdxCustomerMedicalStaffAnalysisInfo; }
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxStaff">The index staff.</param>
        public void InsertOrUpdate(IDXCustomerMedicalStaffAnalysisInfo idxStaff)
        {
            if (idxStaff.IDXCustomerMedicalStaffAnalysisInfoID == default(long))
            {
                // New entity
                _dbContext.IdxCustomerMedicalStaffAnalysisInfo.Add(idxStaff);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.IDXCustomerMedicalStaffAnalysisInfoes.Attach(idxStaff);
                _dbContext.DataContext.Entry(idxStaff).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
             var idxStaff = _dbContext.IdxCustomerMedicalStaffAnalysisInfo.Find(id);
            _dbContext.IdxCustomerMedicalStaffAnalysisInfo.Remove(idxStaff);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Gets the list identifier by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
      public IQueryable<IDXCustomerMedicalStaffAnalysisInfo> GetListIdByCustomerId(long customerId)
        {
            return All.Where(q => q.CustomerID == customerId && q.IsRemoved == false);
        }

      /// <summary>
      /// Gets the customer medical staff analysis information by list identifier.
      /// </summary>
      /// <param name="listId">The list identifier.</param>
      /// <returns></returns>
      public IQueryable<IDXCustomerMedicalStaffAnalysisInfo> GetCustomerMedicalStaffAnalysisInfoByListID(long listId) 
      {
          return All.Where(q => q.MedicalAnalysisListID == listId);
      }

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }        
    }
}
