﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Saurabh Mayekar
// Created          : 29-04-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="UserActionMonitorRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    /// <summary>
    /// User Action Monitor Repository
    /// </summary>
    public interface IUserActionMonitorRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<UserActionMonitor> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<UserActionMonitor> AllIncluding(params Expression<Func<UserActionMonitor, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        UserActionMonitor Find(long id);

        /// <summary>
        /// Gets the UserActionMonitor by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        UserActionMonitor FindByUserId(string userId);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        void InsertOrUpdate(UserActionMonitor userActionMonitor);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Users Repository
    /// </summary>
    public class UserActionMonitorRepository : IUserActionMonitorRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;

        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserActionMonitorRepository"/> class.
        /// </summary>
        public UserActionMonitorRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="UserActionMonitorRepository"/> class.
        /// </summary>
        ~UserActionMonitorRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IUserActionMonitorRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<UserActionMonitor> All
        {
            get { return _dbContext.UserActionMonitor; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<UserActionMonitor> AllIncluding(params Expression<Func<UserActionMonitor, object>>[] includeProperties)
        {
            IQueryable<UserActionMonitor> query = _dbContext.UserActionMonitor;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public UserActionMonitor Find(long id)
        {
            return _dbContext.UserActionMonitor.Find(id);
        }

        /// <summary>
        /// Gets the UserActionMonitor by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public UserActionMonitor FindByUserId(string userId)
        {
            return All.Where(u => u.UserId.ToString() == userId).FirstOrDefault();
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="user">The user.</param>
        public void InsertOrUpdate(UserActionMonitor userActionMonitor)
        {
            if (userActionMonitor.ActionMonitorId == default(int))
            {
                // New entity
                _dbContext.UserActionMonitor.Add(userActionMonitor);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.UserActionMonitors.Attach(userActionMonitor);           
                _dbContext.DataContext.Entry(userActionMonitor).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var user = _dbContext.UserActionMonitor.Find(id);
            _dbContext.UserActionMonitor.Remove(user);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}