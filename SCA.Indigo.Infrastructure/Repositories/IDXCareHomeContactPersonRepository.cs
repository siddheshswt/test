// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXCareHomeContactPersonRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Carehome Contact IDX
    /// </summary>
    public interface IIDXCareHomeContactPersonRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXCareHomeContactPerson> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXCareHomeContactPerson> AllIncluding(params Expression<Func<IDXCareHomeContactPerson, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXCareHomeContactPerson Find(long id);

        /// <summary>
        /// Gets all care home contact person.
        /// </summary>
        /// <returns></returns>
        IQueryable<IDXCareHomeContactPerson> GetAllCareHomeContactPerson();

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxCareHomeContactPerson">The index care home contact person.</param>
        void InsertOrUpdate(IDXCareHomeContactPerson idxCareHomeContactPerson);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Carehome Contact IDX Repository
    /// </summary>
    public class IDXCareHomeContactPersonRepository : IIDXCareHomeContactPersonRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXCareHomeContactPersonRepository"/> class.
        /// </summary>
        public IDXCareHomeContactPersonRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXCareHomeContactPersonRepository"/> class.
        /// </summary>
        ~IDXCareHomeContactPersonRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXCareHomeContactPersonRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXCareHomeContactPerson> All
        {
            get { return _dbContext.IDXCareHomeContactPersons; }
        }

        /// <summary>
        /// Gets all care home contact person.
        /// </summary>
        /// <returns></returns>
        public IQueryable<IDXCareHomeContactPerson> GetAllCareHomeContactPerson()
        {
            return this.All;
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXCareHomeContactPerson> AllIncluding(params Expression<Func<IDXCareHomeContactPerson, object>>[] includeProperties)
        {
            IQueryable<IDXCareHomeContactPerson> query = _dbContext.IDXCareHomeContactPersons;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXCareHomeContactPerson Find(long id)
        {
            return _dbContext.IDXCareHomeContactPersons.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxCareHomeContactPerson">The index care home contact person.</param>
        public void InsertOrUpdate(IDXCareHomeContactPerson idxCareHomeContactPerson)
        {
            if (idxCareHomeContactPerson.IDXContactPersonId == default(long))
            {
                // New entity
                _dbContext.IDXCareHomeContactPersons.Add(idxCareHomeContactPerson);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxCareHomeContactPerson).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxCareHomeContactPerson = _dbContext.IDXCareHomeContactPersons.Find(id);
            _dbContext.IDXCareHomeContactPersons.Remove(idxCareHomeContactPerson);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }   
}