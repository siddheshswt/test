// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="OrderRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;
    using System.Collections.Generic;

    /// <summary>
    /// Order Repository
    /// </summary>
    public interface IOrderRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Order> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Order> AllIncluding(params Expression<Func<Order, object>>[] includeProperties);

        /// <summary>
        /// Gets the carehome orders.
        /// </summary>
        /// <param name="carehomeID">The carehome identifier.</param>
        /// <returns></returns>
        IQueryable<Order> GetCarehomeOrders(long carehomeID);
       
        /// <summary>
        /// Gets the patient orders.
        /// </summary>
        /// <param name="patientID">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<Order> GetPatientOrders(long patientID);

        /// <summary>
        /// Gets the added order entities.
        /// </summary>
        /// <returns></returns>
        System.Collections.Generic.List<Order> GetAddedOrderEntities();

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Order Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="order">The order.</param>
        void InsertOrUpdate(Order order);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    ///  Order Repository
    /// </summary>
    public class OrderRepository : IOrderRepository
    {
        /// <summary>
        /// The database context
        /// </summary>
        private DBContext DbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        public OrderRepository()
        {
            DbContext = new DBContext();
            _unitOfWork = DbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Gets the added order entities.
        /// </summary>
        /// <returns></returns>
        public System.Collections.Generic.List<Order> GetAddedOrderEntities()
        {
            var orders = DbContext.Orders.Local.ToList();
            return orders;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        ~OrderRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IOrderRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Order> All
        {
            get { return DbContext.Orders; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Order> AllIncluding(params Expression<Func<Order, object>>[] includeProperties)
        {
            IQueryable<Order> query = DbContext.Orders;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query; 
        }

        /// <summary>
        /// Gets the carehome orders.
        /// </summary>
        /// <param name="carehomeID">The carehome identifier.</param>
        /// <returns></returns>
        public IQueryable<Order> GetCarehomeOrders(long carehomeID)
        {
            return this.All.Where(q => q.CareHomeId == carehomeID);
        }

        /// <summary>
        /// Gets the patient orders.
        /// </summary>
        /// <param name="patientID">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<Order> GetPatientOrders(long patientID)
        {
            return this.All.Where(q => q.PatientId == patientID);
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Order Find(long id)
        {
            return DbContext.Orders.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="order">The order.</param>
        public void InsertOrUpdate(Order order)
        {
            if (order.OrderId == default(long))
            {                
                // New entity
                DbContext.Orders.Add(order);
            }
            else
            {
                // Existing entity
                DbContext.DataContext.Orders.Attach(order);
                DbContext.DataContext.Entry(order).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var order = DbContext.Orders.Find(id);
            DbContext.Orders.Remove(order);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        }
    }
}