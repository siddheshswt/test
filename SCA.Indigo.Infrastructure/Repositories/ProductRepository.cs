// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="ProductRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;
using System.Collections.Generic;

    /// <summary>
    /// Product Repository
    /// </summary>
    public interface IProductRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Product> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Product> AllIncluding(params Expression<Func<Product, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Product Find(long id);

        /// <summary>
        /// Gets the products by product ids.
        /// </summary>
        /// <param name="productIds">The product ids.</param>
        /// <returns></returns>
        List<Product> GetProductsByProductIds(List<long?> productIds);

        /// <summary>
        /// Gets the products by product ids.
        /// </summary>
        /// <param name="productIds">The product ids.</param>
        /// <returns></returns>
        IQueryable<Product> GetProductsByBaseMaterials(List<string> BaseMaterials);

        /// <summary>
        /// Gets the products by product ids.
        /// </summary>
        /// <param name="productIds">The product ids.</param>
        /// <returns></returns>
        IQueryable<Product> GetProductsBySAPProductIds(List<string> SAPProductIds);

        /// <summary>
        /// Gets the product by product SAP Id 
        /// </summary>
        /// <param name="SAPProductId"></param>
        /// <returns></returns>
        Product GetProductBySAPProductId(string SAPProductId);

        /// <summary>
        /// Gets the product by BaseMaterial
        /// </summary>
        /// <param name="SAPProductId"></param>
        /// <returns></returns>
        Product GetProductByBaseMaterial (string baseMaterial);    

        /// <summary>
        /// Searches the product.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        IQueryable<Product> SearchProduct(string searchText);

        /// <summary>
        /// Searches the sample product.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        IQueryable<SampleProduct> SearchSampleProduct(string searchText);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="product">The product.</param>
        void InsertOrUpdate(Product product);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        #region SampleProducts

        /// <summary>
        /// Gets all sample products.
        /// </summary>
        /// <value>
        /// All sample products.
        /// </value>
        IQueryable<SampleProduct> AllSampleProducts { get; }

        #endregion
    }

    /// <summary>
    /// Product Repository
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        public ProductRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        ~ProductRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IProductRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Product> All
        {
            get { return _dbContext.Products; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Product> AllIncluding(params Expression<Func<Product, object>>[] includeProperties)
        {
            IQueryable<Product> query = _dbContext.Products;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Product Find(long id)
        {
            return _dbContext.Products.Find(id);
        }

        /// <summary>
        /// Searches the product.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        public IQueryable<Product> SearchProduct(string searchText)
        {
            /// Do not make ToLower to ToLower, as throwing error.
			searchText = searchText.ToLower();
			return All.Where(q => q.SAPProductID.ToString().ToLower().Contains(searchText) || q.DescriptionUI.ToLower().Contains(searchText));
        }

        /// <summary>
        /// Searches the sample product.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        public IQueryable<SampleProduct> SearchSampleProduct(string searchText)
       {
           /// Do not make ToLower to ToLower, as throwing error.
		   searchText = searchText.ToLower();
			return AllSampleProducts.Where(q => q.HelixProductID.ToString().ToLower().Contains(searchText) || q.ProductDescription.ToLower().Contains(searchText));
       }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="product">The product.</param>
        public void InsertOrUpdate(Product product)
        {
            if (product.ProductId == default(long))
            {
                // New entity
                _dbContext.Products.Add(product);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(product).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var product = _dbContext.Products.Find(id);
            _dbContext.Products.Remove(product);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the products by SAP Product Ids.
        /// </summary>
        /// <param name="SAPProductIds"></param>
        /// <returns></returns>
        public IQueryable<Product> GetProductsByBaseMaterials(List<string> BaseMaterials)
        {
            return this.All.Where(q => BaseMaterials.Contains(q.BaseMaterial));
        }

        /// <summary>
        /// Gets the products by product ids.
        /// </summary>
        /// <param name="productIds">The product ids.</param>
        /// <returns></returns>
        public List<Product> GetProductsByProductIds(List<long?> productIds) 
        {
            List<Product> products = new List<Product>();
            products = (from product in _dbContext.Products
                       where productIds.Contains(product.ProductId)
                       select product).ToList();

            return products;
        }

        /// <summary>
        /// Gets the products by SAP Product Ids.
        /// </summary>
        /// <param name="SAPProductIds"></param>
        /// <returns></returns>
        public IQueryable<Product> GetProductsBySAPProductIds(List<string> SAPProductIds)
        {
            return this.All.Where(q => SAPProductIds.Contains(q.SAPProductID));
        }

        /// <summary>
        /// Gets the product by product SAP Id 
        /// </summary>
        /// <param name="SAPProductId"></param>
        /// <returns></returns>
        public Product GetProductBySAPProductId(string SAPProductId)
        {
             if(!string.IsNullOrEmpty(SAPProductId))
             {
                 return All.Where(q => q.SAPProductID == SAPProductId).FirstOrDefault();
             }
             return null;
        }

        /// <summary>
        /// Gets the product by BaseMaterial
        /// </summary>
        /// <param name="SAPProductId"></param>
        /// <returns></returns>
        public Product GetProductByBaseMaterial(string baseMaterial)
        {
           var products = All.Where(q => q.BaseMaterial == baseMaterial);
            if(products.Any())
            {
                return products.FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        #region Sample Products

        /// <summary>
        /// Gets all sample products.
        /// </summary>
        /// <value>
        /// All sample products.
        /// </value>
        public IQueryable<SampleProduct> AllSampleProducts
        {
            get { return _dbContext.SampleProducts; }
        }

        #endregion
        
    }
}