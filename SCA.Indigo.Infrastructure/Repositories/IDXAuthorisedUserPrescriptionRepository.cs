﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Avinash Deshpande
// Created          : 03-07-2015
// ***********************************************************************
// <copyright file="IDXUserCustomerRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    using DataEntity = System.Data.Entity;

    /// <summary>
    /// IIDX Authorised User Prescription Repository
    /// </summary>
    public interface IIDXAuthorisedUserPrescriptionRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXAuthorisedUserPrescription> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXAuthorisedUserPrescription> AllIncluding(params Expression<Func<IDXAuthorisedUserPrescription, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXAuthorisedUserPrescription Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxUserCustomer">The index user customer.</param>
        void InsertOrUpdate(IDXAuthorisedUserPrescription idxUserCustomer);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    public class IDXAuthorisedUserPrescriptionRepository : IIDXAuthorisedUserPrescriptionRepository
    {

        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXUserCustomerRepository"/> class.
        /// </summary>
        public IDXAuthorisedUserPrescriptionRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXUserCustomerRepository"/> class.
        /// </summary>
        ~IDXAuthorisedUserPrescriptionRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXAuthorisedUserPrescriptionRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.+
        /// </value>
        public IQueryable<IDXAuthorisedUserPrescription> All
        {
            get { return _dbContext.IDXAuthorisedUserPrescriptions; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXAuthorisedUserPrescription> AllIncluding(params Expression<Func<IDXAuthorisedUserPrescription, object>>[] includeProperties)
        {
            IQueryable<IDXAuthorisedUserPrescription> query = _dbContext.IDXAuthorisedUserPrescriptions;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXAuthorisedUserPrescription Find(long id)
        {
            return _dbContext.IDXAuthorisedUserPrescriptions.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxUserCustomer">The index user customer.</param>
        public void InsertOrUpdate(IDXAuthorisedUserPrescription IDXAuthorisedUserPrescription)
        {
            if (IDXAuthorisedUserPrescription.IDXAuthorisedUserPrescriptionID == default(long))
            {
                // New entity
                _dbContext.IDXAuthorisedUserPrescriptions.Add(IDXAuthorisedUserPrescription);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(IDXAuthorisedUserPrescription).State = DataEntity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var iDXAuthorisedUserPrescription = _dbContext.IDXAuthorisedUserPrescriptions.Find(id);
            _dbContext.IDXAuthorisedUserPrescriptions.Remove(iDXAuthorisedUserPrescription);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
