// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="AddressRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Interface AddressRepository
    /// </summary>
    public interface IAddressRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<Address> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<Address> AllIncluding(params Expression<Func<Address, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Address Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="address">The address.</param>
        void InsertOrUpdate(Address address);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Address Repository
    /// </summary>
    public class AddressRepository : IAddressRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressRepository"/> class.
        /// </summary>
        public AddressRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="AddressRepository"/> class.
        /// </summary>
        ~AddressRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IAddressRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<Address> All
        {
            get { return _dbContext.Addresses; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<Address> AllIncluding(params Expression<Func<Address, object>>[] includeProperties)
        {
            IQueryable<Address> query = _dbContext.Addresses;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Address Find(long id)
        {
            return _dbContext.Addresses.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="address">The address.</param>
        public void InsertOrUpdate(Address address)
        {
            if (address.AddressId == default(long))
            {
                // New entity
                _dbContext.Addresses.Add(address);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(address).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var address = _dbContext.Addresses.Find(id);
            _dbContext.Addresses.Remove(address);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }
}