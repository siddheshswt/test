﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Jagdish
// Created          : 14 July 2015
// ***********************************************************************
// <copyright file="MassDataLogRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using SCA.Indigo.Model;
    using System;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// MassDataLog Repository Interface
    /// </summary>
    public interface IMassDataLogRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<MassDataLog> All { get; }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="massDataLog">The mass data log.</param>
        void InsertOrUpdate(MassDataLog massDataLog);
    }

    /// <summary>
    /// MassDataLog Repository Class
    /// </summary>
    public class MassDataLogRepository : IMassDataLogRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;

        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="MassDataLogRepository"/> class.
        /// </summary>
        public MassDataLogRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="MassDataLogRepository"/> class.
        /// </summary>
        ~MassDataLogRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IMassDataLogRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<MassDataLog> All
        {
            get { return _dbContext.MassDataLogs; }
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="massDataLog">The mass data log.</param>
        public void InsertOrUpdate(MassDataLog massDataLog)
        {
            if (massDataLog.LogId == default(long))
            {
                // New entity
                _dbContext.MassDataLogs.Add(massDataLog);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(massDataLog).State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }    
}
