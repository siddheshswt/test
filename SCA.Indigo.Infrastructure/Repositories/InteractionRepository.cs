﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Gaurav Lad
// Created          : 02-12-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="InteractionRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;
using System.Collections.Generic;

    /// <summary>
    /// Interface IInteractionRepository
    /// </summary>
    public interface IInteractionRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// Gives all interaction rows.
        /// </value>
        IQueryable<Interaction> All { get; }

        /// <summary>
        /// Gets the interaction by order.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>
        /// Return interaction record based on order identifier
        /// </returns>
        Interaction GetInteractionByOrder(long orderId);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Interaction Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="interaction">The interaction.</param>
        void InsertOrUpdate(Interaction interaction);

        /// <summary>
        /// Gets the description from transproxy.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        /// <param name="languageid">The languageid.</param>
        /// <returns></returns>
        string GetDescriptionFromTransproxy(long translationId, long languageid);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Class InteractionRepository.
    /// </summary>
    public class InteractionRepository : IInteractionRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;

        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionRepository" /> class.
        /// </summary>
        public InteractionRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="InteractionRepository" /> class.
        /// </summary>
        ~InteractionRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IInteractionRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// gets all rows of interaction.
        /// </value>
        public IQueryable<Interaction> All
        {
            get { return _dbContext.Interactions; }
        }

        /// <summary>
        /// All the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns>
        /// All the properties related to interaction.
        /// </returns>
        public IQueryable<Interaction> AllIncluding(params Expression<Func<Interaction, object>>[] includeProperties)
        {
            IQueryable<Interaction> query = _dbContext.Interactions;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Interaction.
        /// </returns>
        public Interaction Find(long id)
        {
            return _dbContext.Interactions.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="interaction">The interaction.</param>
        public void InsertOrUpdate(Interaction interaction)
        {
            if (interaction.InteractionId == default(long))
            {
                // New entity
                _dbContext.Interactions.Add(interaction);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Interactions.Attach(interaction);
                _dbContext.DataContext.Entry(interaction).State = EntityState.Modified;                
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var interaction = _dbContext.Interactions.Find(id);
            _dbContext.Interactions.Remove(interaction);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            this._unitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!this._isDisposed && disposing)
            {
                DisposeCore();
            }

            this._isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        /// <summary>
        /// Gets the interaction by order.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>
        /// Return interaction record based on order identifier
        /// </returns>
        public Interaction GetInteractionByOrder(long orderId) 
        {
            Interaction existingrecord = _dbContext.Interactions.Where(a=>a.OrderId == orderId).Select(a=>a).FirstOrDefault();
            return existingrecord;
        }

        /// <summary>
        /// Gets the description from transproxy.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns></returns>
        public string GetDescriptionFromTransproxy(long translationId, long languageId) 
        {
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    var description = context.GetInteractionDescription(translationId, languageId).ToList().FirstOrDefault();
                    return description;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}