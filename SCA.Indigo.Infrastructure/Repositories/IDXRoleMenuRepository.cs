// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXRoleMenuRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// IIDX Role Menu Repository
    /// </summary>
    public interface IIDXRoleMenuRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXRoleMenu> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXRoleMenu> AllIncluding(params Expression<Func<IDXRoleMenu, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXRoleMenu Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxRoleMenu">The index role menu.</param>
        void InsertOrUpdate(IDXRoleMenu idxRoleMenu);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets all role menu.
        /// </summary>
        /// <returns></returns>
        IQueryable<IDXRoleMenu> GetAllRoleMenu();
    }

    /// <summary>
    /// IIDX Role Menu Repository
    /// </summary>
    public class IDXRoleMenuRepository : IIDXRoleMenuRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXRoleMenuRepository"/> class.
        /// </summary>
        public IDXRoleMenuRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXRoleMenuRepository"/> class.
        /// </summary>
        ~IDXRoleMenuRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXRoleMenuRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXRoleMenu> All
        {
            get { return _dbContext.IDXRoleMenus; }
        }

        /// <summary>
        /// Gets all role menu.
        /// </summary>
        /// <returns></returns>
        public  IQueryable<IDXRoleMenu> GetAllRoleMenu()
        {
            return this.All;
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXRoleMenu> AllIncluding(params Expression<Func<IDXRoleMenu, object>>[] includeProperties)
        {
            IQueryable<IDXRoleMenu> query = _dbContext.IDXRoleMenus;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXRoleMenu Find(long id)
        {
            return _dbContext.IDXRoleMenus.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxRoleMenu">The index role menu.</param>
        public void InsertOrUpdate(IDXRoleMenu idxRoleMenu)
        {
            if (idxRoleMenu.IDXRoleMenuID == default(long))
            {
                // New entity
                _dbContext.IDXRoleMenus.Add(idxRoleMenu);
            }
            else
            {
                _dbContext.DataContext.IDXRoleMenus.Attach(idxRoleMenu);
                // Existing entity
                _dbContext.DataContext.Entry(idxRoleMenu).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxrolemenu = _dbContext.IDXRoleMenus.Find(id);
            _dbContext.IDXRoleMenus.Remove(idxrolemenu);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }
}