﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Basanta Sahoo
// Created          : 12-05-2015
// ***********************************************************************
// <copyright file="ClinicalContactAnalysisMasterRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Interface Hierarchy
    /// </summary>
    public interface IClinicalContactAnalysisMasterRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<ClinicalContactAnalysisMaster> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<ClinicalContactAnalysisMaster> AllIncluding(params Expression<Func<ClinicalContactAnalysisMaster, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ClinicalContactAnalysisMaster Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactAnalysisMaster">The clinical contact analysis master.</param>
        void InsertOrUpdate(ClinicalContactAnalysisMaster clinicalContactAnalysisMaster);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();

        /// <summary>
        /// Gets all clinical contact analysis master.
        /// </summary>
        /// <returns></returns>
        IQueryable<ClinicalContactAnalysisMaster> GetAllClinicalContactAnalysisMaster();

        /// <summary>
        /// Gets all clinical contact analysis master by type identifier.
        /// </summary>
        /// <param name="typeid">The typeid.</param>
        /// <returns></returns>
        IQueryable<ClinicalContactAnalysisMaster> GetAllClinicalContactAnalysisMasterByTypeId(long typeid);
    }

    /// <summary>
    /// Clinical Contact Analysis Master Repository
    /// </summary>
    public class ClinicalContactAnalysisMasterRepository : IClinicalContactAnalysisMasterRepository
    {
        /// <summary>
        /// The database context
        /// </summary>
        private static DBContext DbContext;
        /// <summary>
        /// The unit of work
        /// </summary>
        private UnitOfWork UnitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClinicalContactAnalysisMasterRepository"/> class.
        /// </summary>
        public ClinicalContactAnalysisMasterRepository()
        {
            DbContext = new DBContext();
            UnitOfWork = DbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ClinicalContactAnalysisMasterRepository"/> class.
        /// </summary>
        ~ClinicalContactAnalysisMasterRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IClinicalContactAnalysisMasterRepository.UnitOfWork
        {
            get { return UnitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<ClinicalContactAnalysisMaster> All
        {
            get { return DbContext.ClinicalContactAnalysisMasters; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<ClinicalContactAnalysisMaster> AllIncluding(params Expression<Func<ClinicalContactAnalysisMaster, object>>[] includeProperties)
        {
            IQueryable<ClinicalContactAnalysisMaster> query = DbContext.ClinicalContactAnalysisMasters;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ClinicalContactAnalysisMaster Find(long id)
        {
            return DbContext.ClinicalContactAnalysisMasters.Find(id);
        }

        /// <summary>
        /// Gets all clinical contact analysis master.
        /// </summary>
        /// <returns></returns>
        public IQueryable<ClinicalContactAnalysisMaster> GetAllClinicalContactAnalysisMaster()
        {
            return All;
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactAnalysisMaster">The clinical contact analysis master.</param>
        public void InsertOrUpdate(ClinicalContactAnalysisMaster clinicalContactAnalysisMaster)
        {
            if (clinicalContactAnalysisMaster.ClinicalContactAnalysisMasterId == default(long))
            {
                // New entity
                DbContext.ClinicalContactAnalysisMasters.Add(clinicalContactAnalysisMaster);
            }
            else
            {
                // Existing entity
                DbContext.DataContext.Entry(clinicalContactAnalysisMaster).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var clinicalContactAnalysisMaster = DbContext.ClinicalContactAnalysisMasters.Find(id);
            DbContext.ClinicalContactAnalysisMasters.Remove(clinicalContactAnalysisMaster);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            UnitOfWork.Commit();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected static void DisposeCore()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        }

        /// <summary>
        /// Gets all clinical contact analysis master by type identifier.
        /// </summary>
        /// <param name="typeId">The type identifier.</param>
        /// <returns></returns>
        public IQueryable<ClinicalContactAnalysisMaster> GetAllClinicalContactAnalysisMasterByTypeId(long typeId)
        {
            return All.Where(q => q.TypeId == typeId);
        }
    }
}