﻿using SCA.Indigo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Infrastructure.Repositories
{
    /// <summary>
    /// Interface AttachmentRepository
    /// </summary>
    public interface IAttachmentRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// Gives all interaction rows.
        /// </value>
        IQueryable<Attachment> All { get; }

        /// <summary>
        /// Gets the lists all.
        /// </summary>
        /// <value>
        /// The lists all.
        /// </value>
        IQueryable<List> ListsAll { get; }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="attachments">The attachments.</param>
        void InsertOrUpdate(List<Attachment> attachments);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="attachments">The attachments.</param>
        void Delete(List<Attachment> attachments);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }


    /// <summary>
    /// Attachment Repository
    /// </summary>
    public class AttachmentRepository : IAttachmentRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractionRepository" /> class.
        /// </summary>
        public AttachmentRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="InteractionRepository" /> class.
        /// </summary>
        ~AttachmentRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        public UnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// Gives all interaction rows.
        /// </value>
        public IQueryable<Attachment> All
        {
            get { return _dbContext.Attachments; }
        }

        /// <summary>
        /// Gets the lists all.
        /// </summary>
        /// <value>
        /// The lists all.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public IQueryable<List> ListsAll
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="attachments"></param>
        public void InsertOrUpdate(List<Attachment> attachments)
        {
            // New entity
            _dbContext.Attachments.AddRange(attachments);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="attachments"></param>
        public void Delete(List<Attachment> attachments)
        {
            _dbContext.Attachments.RemoveRange(attachments);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            this._unitOfWork.Commit();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!this._isDisposed && disposing)
            {
                DisposeCore();
            }

            this._isDisposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
