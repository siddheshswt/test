﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SCA.Indigo.Model;

namespace SCA.Indigo.Infrastructure.Repositories
{
    /// <summary>
    /// Value Master
    /// </summary>
    public class ClinicalValueMasterRepository:IClinicalValueMasterRepository
    {
        /// <summary>
        /// The database context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="CareHomeRepository"/> class.
        /// </summary>
        public ClinicalValueMasterRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<ClinicalValueMaster> All
        {
            get { return _dbContext.ClinicalValueMasters; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<ClinicalValueMaster> AllIncluding(params Expression<Func<ClinicalValueMaster, object>>[] includeProperties)
        {
            IQueryable<ClinicalValueMaster> query = _dbContext.ClinicalValueMasters;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IClinicalValueMasterRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ClinicalValueMaster Find(long id)
        {
            return _dbContext.ClinicalValueMasters.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="ClinicalValueMaster">The clinical value master.</param>
        public void InsertOrUpdate(ClinicalValueMaster ClinicalValueMaster)
        {
            if (ClinicalValueMaster.ClinicalValueMasterId == default(long))
            {
                // New entity
                _dbContext.ClinicalValueMasters.Add(ClinicalValueMaster);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(ClinicalValueMaster).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var clinicalValueMaster = _dbContext.ClinicalValueMasters.Find(id);
            _dbContext.ClinicalValueMasters.Remove(clinicalValueMaster);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }

    /// <summary>
    /// Interface Value Master
    /// </summary>
    public interface IClinicalValueMasterRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<ClinicalValueMaster> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<ClinicalValueMaster> AllIncluding(params Expression<Func<ClinicalValueMaster, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ClinicalValueMaster Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="clinicalContactRoleMapping">The clinical contact role mapping.</param>
        void InsertOrUpdate(ClinicalValueMaster clinicalContactRoleMapping);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }
}
