// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXCustomerProductRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{    
    using System;
    using System.Data.Entity;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;
	using System.Collections.Generic;

    /// <summary>
    /// IIDX Customer Product Repository
    /// </summary>
    public interface IIDXCustomerProductRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXCustomerProduct> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerProduct> AllIncluding(params Expression<Func<IDXCustomerProduct, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXCustomerProduct Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxCustomerProduct">The index customer product.</param>
        void InsertOrUpdate(IDXCustomerProduct idxCustomerProduct);

        /// <summary>
        /// Gets the customer products.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerProduct> GetCustomerProducts(long customerId);

		/// <summary>
		/// Gets the customer products by Product Id.
		/// </summary>
		/// <param name="productIds">List of Product Ids</param>
		/// <returns></returns>
		IQueryable<IDXCustomerProduct> GetCustomerProductsByProductId(List<long> productIds);

		/// <summary>
		/// Gets the customer products by Product Id.
		/// </summary>
		/// <param name="productIds">List of Product Ids</param>
		/// <returns></returns>
		IQueryable<IDXCustomerProduct> GetCustomerProductsByIdxId(List<long> IdxIds);

        /// <summary>
        /// Searches the product.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        IQueryable<IDXCustomerProduct> SearchProduct(string searchText, long customerId);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// IIDX Customer Product Repository
    /// </summary>
    public class IDXCustomerProductRepository : IIDXCustomerProductRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXCustomerProductRepository"/> class.
        /// </summary>
        public IDXCustomerProductRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXCustomerProductRepository"/> class.
        /// </summary>
        ~IDXCustomerProductRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXCustomerProductRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXCustomerProduct> All
        {
            get { return _dbContext.IDXCustomerProducts; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXCustomerProduct> AllIncluding(params Expression<Func<IDXCustomerProduct, object>>[] includeProperties)
        {
            IQueryable<IDXCustomerProduct> query = _dbContext.IDXCustomerProducts;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXCustomerProduct Find(long id)
        {
            return _dbContext.IDXCustomerProducts.Find(id);
        }

        /// <summary>
        /// Gets the customer products.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXCustomerProduct> GetCustomerProducts(long customerId)
        {
            return this.All.Where(q => q.CustomerId == customerId && q.IsAvailable);
        }

        /// <summary>
        /// Searches the product.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        public IQueryable<IDXCustomerProduct> SearchProduct(string searchText, long customerId)
        {
            return All.Where(q => (q.Product.SAPProductID.ToLower().Contains(searchText) || q.Product.DescriptionUI.ToLower().Contains(searchText)) &&  q.CustomerId == customerId && q.IsAvailable );
        }

        /// <summary>
        /// Gets the customer products by Product Id.
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
		public IQueryable<IDXCustomerProduct> GetCustomerProductsByProductId(List<long> productIds)
		{
			return All.Where(q => productIds.Contains((long)q.ProductId) && q.IsAvailable);
		}

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxCustomerProduct">The index customer product.</param>
        public void InsertOrUpdate(IDXCustomerProduct idxCustomerProduct)
        {
            if (idxCustomerProduct.IDXId == default(long))
            {
                // New entity
                _dbContext.IDXCustomerProducts.Add(idxCustomerProduct);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxCustomerProduct).State = EntityState.Modified;
            }
        }

		/// <summary>
		/// return all the available products of customer
		/// </summary>
		/// <param name="idxCustomerProduct">The index customer product.</param>
		public IQueryable<IDXCustomerProduct> GetCustomerProducts(int customerId)
		{
			return All.Where(q => q.CustomerId == customerId && q.IsAvailable);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="IdxIds"></param>
		/// <returns></returns>
		public IQueryable<IDXCustomerProduct> GetCustomerProductsByIdxId(List<long> IdxIds)
		{
			return All.Where(q => IdxIds.Contains(q.IDXId));
		}

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxCustomerProduct = _dbContext.IDXCustomerProducts.Find(id);
            _dbContext.IDXCustomerProducts.Remove(idxCustomerProduct);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

	}
}