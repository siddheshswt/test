// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Siddharth Dilpak
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="ContactPersonRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using SCA.Indigo.Model;

    /// <summary>
    /// Interface Contact Person
    /// </summary>
    public interface IContactPersonRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<ContactPerson> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<ContactPerson> AllIncluding(params Expression<Func<ContactPerson, object>>[] includeProperties);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ContactPerson Find(long id);

        /// <summary>
        /// Gets all contact person.
        /// </summary>
        /// <returns></returns>
        IQueryable<ContactPerson> GetAllContactPerson();

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="contactPerson">The contact person.</param>
        void InsertOrUpdate(ContactPerson contactPerson);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Deletes the personal information.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeletePersonalInfo(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// Contact Person Repository
    /// </summary>
    public class ContactPersonRepository : IContactPersonRepository
    {
        /// <summary>
        /// The database context
        /// </summary>
        private static DBContext DbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactPersonRepository"/> class.
        /// </summary>
        public ContactPersonRepository()
        {
            DbContext = new DBContext();
            _unitOfWork = DbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ContactPersonRepository"/> class.
        /// </summary>
        ~ContactPersonRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<ContactPerson> All
        {
            get { return DbContext.ContactPersons; }
        }

        /// <summary>
        /// Gets all contact person.
        /// </summary>
        /// <returns></returns>
        public IQueryable<ContactPerson> GetAllContactPerson()
        {
            return this.All;
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IContactPersonRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<ContactPerson> AllIncluding(params Expression<Func<ContactPerson, object>>[] includeProperties)
        {
            IQueryable<ContactPerson> query = DbContext.ContactPersons;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ContactPerson Find(long id)
        {
            return DbContext.ContactPersons.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="contactPerson">The contact person.</param>
        public void InsertOrUpdate(ContactPerson contactPerson)
        {
            if (contactPerson.ContactPersonId == default(long))
            {
                // New entity
                DbContext.ContactPersons.Add(contactPerson);
            }
            else
            {
                // Existing entity
                DbContext.DataContext.Entry(contactPerson).State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var contactperson = DbContext.ContactPersons.Find(id);
            DbContext.ContactPersons.Remove(contactperson);
        }

        /// <summary>
        /// Deletes the personal information.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeletePersonalInfo(long id)
        {
            var personalInformation = DbContext.PersonalInformations.Find(id);
            DbContext.PersonalInformations.Remove(personalInformation);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected static void DisposeCore()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        } 
    } 
}