// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Sagar Yerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="IDXUserCustomerRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    using SCA.Indigo.Model;

    using DataEntity = System.Data.Entity;

    /// <summary>
    /// IIDX User Customer Repository
    /// </summary>
    public interface IIDXUserCustomerRepository : IDisposable
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        IQueryable<IDXUserCustomer> All { get; }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<IDXUserCustomer> AllIncluding(params Expression<Func<IDXUserCustomer, object>>[] includeProperties);

        /// <summary>
        /// Gets the customers by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IQueryable<IDXUserCustomer> GetCustomersByUserId(Guid userId);

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IDXUserCustomer Find(long id);

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxUserCustomer">The index user customer.</param>
        void InsertOrUpdate(IDXUserCustomer idxUserCustomer);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(long id);

        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }

    /// <summary>
    /// IIDX User Customer Repository
    /// </summary>
    public class IDXUserCustomerRepository : IIDXUserCustomerRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="IDXUserCustomerRepository"/> class.
        /// </summary>
        public IDXUserCustomerRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="IDXUserCustomerRepository"/> class.
        /// </summary>
        ~IDXUserCustomerRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        UnitOfWork IIDXUserCustomerRepository.UnitOfWork
        {
            get { return _unitOfWork; }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <value>
        /// All.
        /// </value>
        public IQueryable<IDXUserCustomer> All
        {
            get { return _dbContext.IDXUserCustomers; }
        }

        /// <summary>
        /// Alls the including.
        /// </summary>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<IDXUserCustomer> AllIncluding(params Expression<Func<IDXUserCustomer, object>>[] includeProperties)
        {
            IQueryable<IDXUserCustomer> query = _dbContext.IDXUserCustomers;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <summary>
        /// Gets the customers by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXUserCustomer> GetCustomersByUserId(Guid userId)
        {
            return All.Where(c => c.UserId == userId);
        }

        /// <summary>
        /// Checks the exist customer and user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public IQueryable<IDXUserCustomer> CheckExistCustomerAndUserId(Guid userId, long customerId)
        {
            return All.Where(c => c.UserId == userId && c.CustomerId == customerId);
        }

        /// <summary>
        /// Finds the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IDXUserCustomer Find(long id)
        {
            return _dbContext.IDXUserCustomers.Find(id);
        }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="idxUserCustomer">The index user customer.</param>
        public void InsertOrUpdate(IDXUserCustomer idxUserCustomer)
        {
            if (idxUserCustomer.IDXUserCustomerId == default(long))
            {
                // New entity
                _dbContext.IDXUserCustomers.Add(idxUserCustomer);
            }
            else
            {
                // Existing entity
                _dbContext.DataContext.Entry(idxUserCustomer).State = DataEntity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(long id)
        {
            var idxusercustomer = _dbContext.IDXUserCustomers.Find(id);
            _dbContext.IDXUserCustomers.Remove(idxusercustomer);
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        } 
    }
}