﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="PrescriptionCustomRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories.CustomRepositories
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Linq.Expressions;
	using System.Text;
	using System.Threading.Tasks;

	using SCA.Indigo.Model;
	using System.Data.Entity;
	using System.Data.SqlClient;
    using System.Collections;
    using System.Data;

	/// <summary>
	/// Interface PatientCustomRepository
	/// </summary>
	public interface IGoogleSearchRepository : IDisposable
	{


		/// <summary>
		/// Gets the unit of work.
		/// </summary>
		/// <value>
		/// The unit of work.
		/// </value>
		UnitOfWork UnitOfWork { get; }

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <value>
		/// All.
		/// </value>
		IQueryable<Google> All { get; }

		/// <summary>
		/// Insert list of records to Google table
		/// </summary>
		/// <param name="google"></param>
		void Insert(List<Google> google);

		/// <summary>
        /// Insert or update a record
		/// </summary>
		/// <param name="google"></param>		
		void InsertOrUpdate(Google google);

		/// <summary>
		/// Insert list of records to Google table
		/// </summary>
		/// <param name="google"></param>
		bool TruncateGoogleTable();

		/// <summary>
		/// Create Full Text on Fields Column of Google Table
		/// </summary>
		/// <returns></returns>
		bool CreateFullTextIndexGoogleTable();


		/// <summary>
		/// Gets the patient prescriptions.
		/// </summary>
		/// <param name="patientId">The patient identifier.</param>
		/// <param name="showRemoved">if set to <c>true</c> [show removed].</param>
		/// <returns></returns>
        List<GoogleSearchIndigoID_Result> GetGoogleSearchIds(string searchText, string customerIds, string carehomeId, string status, string patientType, int offSet, int rowCount, string sortColumn, string sortOrder);

        /// <summary>
        /// Get the patient details for search id
        /// </summary>
        /// <param name="searchId"></param>
        /// <param name="offSet"></param>
        /// <param name="rowCount"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        List<GoogleSearchIndigoID_Result> GetGoogleSearchForPatientOrder(string searchId, string paddedSearchId, string customerIds, string carehomeId, int offSet, int rowCount, string sortColumn, string sortOrder);


        /// <summary>
        /// Get the order details for search id
        /// </summary>
        /// <param name="searchId"></param>
        /// <param name="offSet"></param>
        /// <param name="rowCount"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        List<GoogleSearchIndigoID_Result> GetGoogleSearchForCarehomeOrder(string searchId, string paddedSearchId, string customerIds, string carehomeId, int offSet, int rowCount, string sortColumn, string sortOrder);

		List<GoogleSearch_Result> GetGoogleSearchForPatient(List<long> lstIds);

        /// <summary>
        /// GetPatientId
        /// </summary>
        /// <param name="searchId"></param>
        /// <param name="paddedSearchId"></param>
        /// <param name="customerIds"></param>
        /// <param name="carehomeId"></param>
        /// <returns></returns>
        List<GoogleSearchIndigoID_Result> GetPatientId(string searchId, string paddedSearchId, string customerIds, string carehomeId);

        /// <summary>
        /// GetCarehomeId
        /// </summary>
        /// <param name="searchId"></param>
        /// <param name="paddedSearchId"></param>
        /// <param name="customerIds"></param>
        /// <param name="carehomeId"></param>
        /// <returns></returns>
        List<GoogleSearchIndigoID_Result> GetCarehomeId(string searchId, string paddedSearchId, string customerIds, string carehomeId);
        

        /// <summary>
        /// GetGoogleSearchForPatientCarehomeId
        /// </summary>
        /// <param name="carehomeSearchIds"></param>
        /// <param name="patientSearchIds"></param>
        /// <param name="customerIds"></param>
        /// <param name="carehomeId"></param>
        /// <param name="offSet"></param>
        /// <param name="rowCount"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        List<GoogleSearchIndigoID_Result> GetGoogleSearchForPatientCarehomeId(string carehomeSearchIds, string patientSearchIds, string customerIds, string carehomeId, int offSet, int rowCount, string sortColumn, string sortOrder);
       
		List<GoogleSearch_Result> GetGoogleSearchForCarehome(List<long> lstIds);

		List<GoogleSearch_Result> GetGoogleSearchForCustomer(List<long> lstIds);

        List<GoogleSearchIndex_Result> GetPatientsForIndexing(string indigoId);
        List<GoogleSearchIndex_Result> GetCarehomesForIndexing(string indigoId);
        List<GoogleSearchIndex_Result> GetCustomersForIndexing(string indigoId);
       
        /// <summary>
        /// GetGoogleSearchForIndigoIDObjectType
        /// </summary>
        /// <param name="indigoId"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        List<Google> GetGoogleSearchForIndigoIDObjectType(long indigoId, long objectType);
	}

	/// <summary>
	/// Patient CustomRepository 
	/// </summary>
	public class GoogleSearchRepository : IGoogleSearchRepository
	{
		/// <summary>
		/// The _DB context
		/// </summary>
		private DBContext _dbContext;

		/// <summary>
		/// The _is disposed
		/// </summary>
		private bool _isDisposed;

		/// <summary>
		/// The _unit of work
		/// </summary>
		private UnitOfWork _unitOfWork;

		/// <summary>
		/// Gets the unit of work.
		/// </summary>
		/// <value>
		/// The unit of work.
		/// </value>
		UnitOfWork IGoogleSearchRepository.UnitOfWork
		{
			get { return _unitOfWork; }
		}
		/// <summary>
		/// Gets all.
		/// </summary>
		/// <value>
		/// All.
		/// </value>
		public IQueryable<Google> All
		{
			get { return _dbContext.Google; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GoogleSearchRepository"/> class.
		/// </summary>
		public GoogleSearchRepository()
		{
			_dbContext = new DBContext();
			_unitOfWork = _dbContext.GetUnitOfWork();
		}

		/// <summary>
        /// Inserts the or update.
		/// </summary>
		/// <param name="google"></param>        
		public void InsertOrUpdate(Google google)
		{
            if (google.GoogleId == default(long))
            {
				// New entity
				_dbContext.Google.Add(google);
			}
			else
			{
				_dbContext.DataContext.Googles.Attach(google);
				_dbContext.DataContext.Entry(google).State = EntityState.Modified;
			}
		}

		///// <summary>
		/// Inserts the or update.
		/// </summary>
		/// <param name="order">The order.</param>
		public void Insert(List<Google> google)
		{
			_dbContext.Google.AddRange(google);
		}

		/// <summary>
		/// Truncate the google table
		/// </summary>		
		public bool TruncateGoogleTable()
		{
			try
			{
				// TODO : Discuss-Index creation not allowed through code
				//	string strSQL = "DROP FULLTEXT INDEX ON Google;";
				//	_dbContext.DataContext.Database.ExecuteSqlCommand(strSQL);

				string strSQL = "TRUNCATE table Google; ";
				var result = _dbContext.DataContext.Database.ExecuteSqlCommand(strSQL);
			}
			catch (SqlException ex)
			{
				return false;
			}
			catch (Exception ex)
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Add full text index on Google Table
		/// </summary>		
		public bool CreateFullTextIndexGoogleTable()
		{
			try
			{
				string strSQL = "CREATE FULLTEXT INDEX ON Google(Fields) KEY INDEX PK;";
				var result = _dbContext.DataContext.Database.ExecuteSqlCommand(strSQL);
			}
			catch (SqlException ex)
			{
				return false;
			}
			catch (Exception ex)
			{
				return false;
			}
			return true;
		}



		/// <summary>
		/// Gives the Indigo Id list from Customer, Carehome and Patient for all records matching the search text
		/// </summary>
		/// <param name="searchText"></param>
		/// <returns></returns>
        public List<GoogleSearchIndigoID_Result> GetGoogleSearchIds(string searchText, string customerIds, string carehomeId, string status, string patientType, int offSet, int rowCount, string sortColumn, string sortOrder)
		{
            StringBuilder strSQL = new StringBuilder();

            ArrayList arrParam = new ArrayList();

            strSQL.Append("SELECT IndigoId, ObjectType as IdType, COUNT(*) OVER() TotalRows FROM Google WHERE ");

            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL.Append(" CONTAINS(Fields, " + AddSQLParameter("searchText", searchText, SqlDbType.NVarChar, ref arrParam) + ") AND ");
            }

            if (!string.IsNullOrEmpty(customerIds))
            {
                strSQL.Append(" CustomerId IN (");
                strSQL.Append(FormatQueryForInClause(customerIds, "customerId" ,ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(carehomeId))
            {
                strSQL.Append(" CareHomeId IN (");
                strSQL.Append(FormatQueryForInClause(carehomeId, "careHomeId", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(status))
            {
                strSQL.Append(" Status IN (");
                strSQL.Append(FormatQueryForInClause(status, "status", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(patientType))
            {
                strSQL.Append(" PatientTypeId IN (");
                strSQL.Append(FormatQueryForInClause(patientType, "patientType", ref arrParam));
                strSQL.Append(") AND ");
            }

            strSQL = new StringBuilder(strSQL.ToString().Substring(0, strSQL.Length - 4));

            strSQL.Append(" ORDER BY " + sortColumn + " " + sortOrder);
            strSQL.Append(" OFFSET " + offSet + " ROWS ");
            strSQL.Append(" FETCH NEXT " + rowCount + " ROWS ONLY ");

            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndigoID_Result>(strSQL.ToString(), arrParam.ToArray()).ToList();
			return result;
		}

        public string FormatQueryForInClause(string input, string parameterName, ref ArrayList arrParam)
        {
            var arrFormatter = arrParam;
            SqlParameter param = null;
            var count = 0;
            var paramName = string.Empty;
            string strText = string.Empty;

            input.Split(',').ToList().ForEach(q =>
                {
                    paramName = "@" + parameterName + "" + count++;

                    strText += paramName + ",";

                    param = new SqlParameter(paramName, SqlDbType.BigInt);
                    param.Value = q;

                    arrFormatter.Add(param);
                });

            strText = strText.Remove(strText.Length - 1, 1);

            return strText;
        }

        private string AddSQLParameter(string parameterName, string value, SqlDbType DBType, ref ArrayList arrFormatter)
        {
            var paramName = "@" + parameterName;

            SqlParameter param = new SqlParameter(paramName, DBType);
            param.Value = value;
            arrFormatter.Add(param);

            return paramName;
        }

		public List<GoogleSearch_Result> GetGoogleSearchForPatient(List<long> lstIds)
		{
			var strPatientId = string.Join(",", lstIds.ToArray());
			string strSQL = @";WITH Pres AS
                              (
                                    SELECT COUNT(1) NoOfProduct, MAX(PatientId) PatientId
                                    FROM Prescription pres
                                    INNER JOIN IDXPrescriptionProduct prod
                                    ON  prod.PrescriptionId = pres.PrescriptionId
                                    WHERE prod.IsRemoved = 0
                                    GROUP BY prod.PrescriptionId
                              ),
                              Ord AS
                              (
                                    SELECT COUNT(1) NoOfActiveOrders, PatientId
                                    FROM Orders o
                                    WHERE (OrderStatus = 10047 OR OrderStatus = 10098)
                                    AND   OrderType != 10100
                                    GROUP BY PatientId
                               )
                               SELECT p.PatientId,
                                      LSTTitle.DefaultText TitleId,
                                      pinfoP.FirstName + ' ' + pinfoP.LastName PatientName,
                                      pinfoP.FirstName FirstName,
                                      pinfoP.LastName LastName,
                                      pinfoP.TelephoneNumber,
                                      a.HouseNumber,
                                      a.HouseName,
                                      a.AddressLine1,
                                      a.AddressLine2,
                                      a.City, 
                                      a.County,
                                      a.Country,
                                      a.PostCode,
                                      p.DateofBirth,
                                      LstPType.DefaultText PatientType,
                                      LstStatus.DefaultText PatientStatus, 
                                      p.NextDeliveryDate DeliveryDate,
                                      ISNULL(pres.NoOfProduct, 0) NoOfProduct,
                                      cast(0 as bit) HasActiveOrder,
                                      0 OrderActivated,
                                      ISNULL(Ord.NoOfActiveOrders, 0) NoOfActiveOrders,
                                      p.CustomerId,
                                      ISNULL(p.CareHomeId, 0) CareHomeId,
                                      pinfoC.FirstName + ' ' + ISNULL(pinfoC.LastName, '') CustomerName,
                                      pinfoCH.FirstName + ' ' + ISNULL(pinfoCH.LastName, '') CarehomeName,
                                      c.SAPCustomerNumber,
                                      p.SAPPatientNumber,
                                      10113 ObjectType
                              FROM  Patient p
                              INNER JOIN PersonalInformation pinfoP
                              ON  pinfoP.PersonalInformationId = p.PersonalInformationId
                              LEFT JOIN CareHome ch
                              ON  ch.CareHomeId = p.CareHomeId
                              LEFT JOIN PersonalInformation pinfoCH
                              ON  pinfoCH.PersonalInformationId = ch.PersonalInformationId
                              INNER JOIN Customer c
                              ON  p.CustomerId = c.CustomerId
                              INNER JOIN PersonalInformation pinfoC
                              ON  pinfoC.PersonalInformationId = c.PersonalInformationId
                              INNER JOIN Address a
                              ON  pinfoP.AddressId = a.AddressId
                              LEFT JOIN pres
                              ON  p.PatientId = pres.PatientId
                              INNER JOIN List LstPType
                              ON  p.PatientType = LstPType.ListId
                              INNER JOIN List LstStatus
                              ON  p.PatientStatus = LstStatus.ListId
                              INNER JOIN List LSTTitle
                              ON  pinfoP.TitleId = LSTTitle.ListId
                              LEFT JOIN Ord
                              ON  p.PatientId = Ord.PatientId
                              WHERE p.PatientId IN (" + strPatientId + ")";

			var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearch_Result>(strSQL).ToList();
			return result;
		}

		public List<GoogleSearch_Result> GetGoogleSearchForCarehome(List<long> lstIds)
		{
			var strCareHomeId = string.Join(",", lstIds.ToArray());
			string strSQL = @";WITH Ord AS
                              (
                                    SELECT COUNT(1) NoOfActiveOrders, CareHomeId
                                    FROM Orders o
                                    WHERE (o.OrderStatus = 10047 OR o.OrderStatus = 10098) 
                                    GROUP BY CareHomeId
                              )
                              SELECT NULL AS PatientId,
                                     '' TitleId,
                                     pinfoCH.FirstName + ' ' + ISNULL(pinfoCH.LastName,'') PatientName,
                                     pinfoCH.FirstName,
                                     pinfoCH.LastName,
                                     pinfoCH.TelephoneNumber,
                                     a.HouseNumber,
                                     a.HouseName,
                                     ISNULL(a.AddressLine1,'') AddressLine1,
                                     a.AddressLine2,
                                     a.City,
                                     a.County,
                                     a.Country,
                                     ISNULL(a.PostCode,'') PostCode,
                                     NULL as DateofBirth,
                                     'CareHome' AS PatientType,
                                     LstStatus.DefaultText PatientStatus,
                                     CH.NextDeliveryDate as DeliveryDate,
                                     1 NoOfProduct,
                                     cast(0 as bit) AS HasActiveOrder,
                                     0 OrderActivated,
                                     ISNULL(o.NoOfActiveOrders, 0) NoOfActiveOrders,
                                     ch.CustomerId,
                                     ch.CareHomeId,
                                     pinfoC.FirstName + ' ' + ISNULL(pinfoC.LastName,'') AS CustomerName,
                                     pinfoCH.FirstName + ' ' + ISNULL(pinfoCH.LastName,'') AS CareHomeName,
                                     ISNULL(C.SAPCustomerNumber, '') AS SAPCustomerNumber,
                                     ISNULL(CH.SAPCareHomeNumber, '') As SAPPatientNumber,
                                     10114 ObjectType
                              FROM CareHome as CH
                              INNER JOIN PersonalInformation pinfoCH
                              ON  CH.PersonalInformationId = pinfoCH.PersonalInformationId
                              INNER JOIN Address  a
                              ON  a.AddressId = pinfoCH.AddressId
                              INNER JOIN List LstStatus 
                              ON  CH.CareHomeStatus = LstStatus.ListId
                              INNER JOIN Customer C
                              ON  C.CustomerId = CH.CustomerId
                              INNER JOIN PersonalInformation as pinfoC
                              ON  C.PersonalInformationId = pinfoC.PersonalInformationId
                              LEFT JOIN Ord o
                              ON  o.CareHomeId = CH.CareHomeId
                              WHERE CH.CareHomeId IN (" + strCareHomeId + ")";

			var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearch_Result>(strSQL).ToList();
			return result;
		}

		public List<GoogleSearch_Result> GetGoogleSearchForCustomer(List<long> lstIds)
		{
			var strCustomerId = string.Join(",", lstIds.ToArray());
			string strSQL = @"SELECT NULL PatientId,
                                     '' TitleId,
                                     pinfoC.FirstName + ' ' + ISNULL(pinfoC.LastName,'') PatientName,
                                     pinfoC.FirstName,
                                     pinfoC.LastName,
                                     pinfoC.TelephoneNumber,
                                     a.HouseNumber,
                                     a.HouseName,
                                     ISNULL(a.AddressLine1,'') AddressLine1,
                                     a.AddressLine2,
                                     a.City,
                                     a.County,
                                     a.Country,
                                     ISNULL(a.PostCode, '') PostCode,
                                     NULL DateofBirth,
                                     NULL PatientType,
                                     LstStatus.DefaultText PatientStatus,
                                     NULL DeliveryDate,
                                     0 NoOfProduct,
                                     CAST(0 AS BIT) HasActiveOrder,
                                     0 OrderActivated,
                                     0 NoOfActiveOrders,
                                     c.CustomerId,
                                     NULL CareHomeId,
                                     pinfoC.FirstName + ' ' + ISNULL(pinfoC.LastName,'') CustomerName,
                                     '' CareHomeName,
                                     c.SAPCustomerNumber SAPCustomerNumber,
                                     NULL SAPPatientNumber,
                                     10115 ObjectType
                              FROM Customer c
                              INNER JOIN PersonalInformation pinfoC
                              ON  c.PersonalInformationId = pinfoC.PersonalInformationId
                              INNER JOIN Address a
                              ON  a.AddressId = pinfoC.AddressId
                              LEFT JOIN List LstStatus
                              ON  c.Customer_Status = LstStatus.ListId
                              WHERE c.CustomerId  IN (" + strCustomerId + ")";

			var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearch_Result>(strSQL).ToList();
			return result;
		}


		/// <summary>
		/// Get the details of patients for Indexing
		/// </summary>
		/// <returns></returns>		
        public List<GoogleSearchIndex_Result> GetPatientsForIndexing(string indigoId)
		{
            ArrayList arrParam = new ArrayList();
            string strSQL = @";WITH Pres AS
                              (
                                  SELECT COUNT(1) NoOfProduct, MAX(PatientId) PatientId
                                  FROM Prescription pres
                                  INNER JOIN IDXPrescriptionProduct prod
                                  ON  prod.PrescriptionId = pres.PrescriptionId
                                  WHERE prod.IsRemoved = 0
                                  GROUP BY prod.PrescriptionId
                              )
                              SELECT p.PatientId as IndigoId, 10113 IdType,                                      
                                     ISNULL(pinfo.FirstName,'') + ' ' + ISNULL(pinfo.LastName,'') + ' ' + 
                                     ISNULL(a.PostCode, '') + ' ' +
                                     ISNULL(ca.PostCode, '') + ' ' +
                                     ISNULL(pinfoC.FirstName,'') + ' ' + ISNULL(pinfoC.LastName,'') + ' ' +
                                     CONVERT(VARCHAR(10), p.DateofBirth, 103) + ' ' +                                      
                                     ISNULL(pinfo.TelephoneNumber, '') as Fields,
                                     p.CustomerId CustomerId, p.CareHomeId CareHomeId, p.PatientStatus Status, p.PatientType PatientTypeID,
                                     l.DefaultText PatientType,
                                     ISNULL(pinfo.FirstName,'') FirstName, ISNULL(pinfo.LastName,'') LastName,
                                     ISNULL(pinfoC.FirstName,'') + ' ' + ISNULL(pinfoC.LastName,'') CareHomeName,
                                     a.PostCode Postcode,
                                     p.DateofBirth DateofBirth,
                                     CASE WHEN pres.NoOfProduct > 0 THEN p.NextDeliveryDate ELSE NULL END DeliveryDate
                               FROM   Patient p 
                               INNER JOIN PersonalInformation pinfo
                               ON p.PersonalInformationId = pinfo.PersonalInformationId
                               INNER JOIN Address a
                               ON pinfo.AddressId = a.AddressId
                               LEFT JOIN Address ca
                               ON pinfo.CorrespondenceAddressId = ca.AddressId
                               LEFT JOIN CareHome c
                               ON p.CareHomeId = c.CareHomeId
                               LEFT JOIN PersonalInformation pinfoC
                               ON c.PersonalInformationId = pinfoC.PersonalInformationId
                               LEFT JOIN List l
							   ON p.PatientType = l.ListId
                               LEFT JOIN pres
                               ON  p.PatientId = pres.PatientId";
            
            if (!string.IsNullOrEmpty(indigoId))
            {
                strSQL = strSQL + " WHERE p.PatientId =" + AddSQLParameter("indigoId", indigoId, SqlDbType.NVarChar, ref arrParam) + "";

            }


			var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndex_Result>(strSQL, arrParam.ToArray()).ToList();
			return result;
		}



		/// <summary>
		/// Get the details of Carehomes for Indexing
		/// </summary>
		/// <returns></returns>
        public List<GoogleSearchIndex_Result> GetCarehomesForIndexing(string indigoId)
		{
            ArrayList arrParam = new ArrayList();
			string strSQL = @" SELECT c.CareHomeId as IndigoId, 10114 as IdType,                                      
                                      ISNULL(a.PostCode, '') + ' ' + 
                                      ISNULL(p.FirstName, '') + ' ' + ISNULL(p.LastName, '') + ' ' +                                       
                                      ISNULL(p.TelephoneNumber, '') as Fields,
                                      c.CustomerId CustomerId, c.CareHomeId CareHomeId, c.CareHomeStatus Status, NULL PatientTypeID,
                                      'Carehome' PatientType,
                                      ISNULL(p.FirstName,'') FirstName, ISNULL(p.FirstName,'') LastName,
                                      ISNULL(p.FirstName,'') + ' ' + ISNULL(p.LastName,'') CareHomeName,
                                      a.PostCode Postcode,
                                      NULL DateofBirth,
                                      c.NextDeliveryDate DeliveryDate
                               FROM   CareHome c
                               INNER JOIN PersonalInformation p
                               ON   c.PersonalInformationId = p.PersonalInformationId
                               INNER JOIN Address a
                               ON   p.AddressId = a.AddressId";
            if (!string.IsNullOrEmpty(indigoId))
            {
                strSQL = strSQL + " WHERE C.CareHomeId =" + AddSQLParameter("indigoId", indigoId, SqlDbType.NVarChar, ref arrParam) + "";

            }
            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndex_Result>(strSQL, arrParam.ToArray()).ToList();
			return result;
		}

        /// <summary>
        /// GetPatientId
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="paddedSearchText"></param>
        /// <param name="customerIds"></param>
        /// <param name="carehomeId"></param>
        /// <returns></returns>
        public List<GoogleSearchIndigoID_Result> GetPatientId(string searchText, string paddedSearchText, string customerIds, string carehomeId)
        {
            StringBuilder strSQL = new StringBuilder();

            ArrayList arrParam = new ArrayList();

            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL.Append("SELECT pat.PatientId AS IndigoId, 10113 AS IdType, 1 as TotalRows FROM Patient pat WHERE (pat.PatientId = " + AddSQLParameter("patientId", searchText, SqlDbType.BigInt, ref arrParam));
                strSQL.Append(" OR pat.SAPPatientNumber = " + AddSQLParameter("patientNumber", searchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR pat.SAPPatientNumber = " + AddSQLParameter("sapPatientNumber", paddedSearchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR pat.NHSId = " + AddSQLParameter("nhsId", searchText, SqlDbType.NVarChar, ref arrParam) + ") AND ");
            }

            if (!string.IsNullOrEmpty(customerIds))
            {
                strSQL.Append(" pat.CustomerId IN (");
                strSQL.Append(FormatQueryForInClause(customerIds, "customerId", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(carehomeId))
            {
                strSQL.Append(" pat.CareHomeId IN (");
                strSQL.Append(FormatQueryForInClause(carehomeId, "careHomeId", ref arrParam));
                strSQL.Append(") AND ");
            }

            strSQL = new StringBuilder(strSQL.ToString().Substring(0, strSQL.Length - 4));

            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndigoID_Result>(strSQL.ToString(), arrParam.ToArray()).ToList();
            return result;
        }

        /// <summary>
        /// GetCarehomeId
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="paddedSearchText"></param>
        /// <param name="customerIds"></param>
        /// <param name="carehomeId"></param>
        /// <returns></returns>
        public List<GoogleSearchIndigoID_Result> GetCarehomeId(string searchText, string paddedSearchText, string customerIds, string carehomeId)
        {
            StringBuilder strSQL = new StringBuilder();

            ArrayList arrParam = new ArrayList();

            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL.Append("SELECT ca.CareHomeId AS IndigoId, 10114 AS IdType, 1 as TotalRows FROM Carehome ca WHERE (ca.CareHomeId = " + AddSQLParameter("careHomeId", searchText, SqlDbType.BigInt, ref arrParam));
                strSQL.Append(" OR ca.SAPCareHomeNumber = " + AddSQLParameter("careHomeNumber", searchText, SqlDbType.NVarChar, ref arrParam) + "");
                strSQL.Append(" OR ca.SAPCareHomeNumber = " + AddSQLParameter("sapCareHomeNumber", paddedSearchText, SqlDbType.NVarChar, ref arrParam) + ") AND ");
            }

            if (!string.IsNullOrEmpty(customerIds))
            {
                strSQL.Append(" ca.CustomerId IN (");
                strSQL.Append(FormatQueryForInClause(customerIds, "customerId", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(carehomeId))
            {
                strSQL.Append(" ca.CareHomeId IN (");
                strSQL.Append(FormatQueryForInClause(carehomeId, "careHomeId", ref arrParam));
                strSQL.Append(") AND ");
            }

            strSQL = new StringBuilder(strSQL.ToString().Substring(0, strSQL.Length - 4));

            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndigoID_Result>(strSQL.ToString(), arrParam.ToArray()).ToList();
            return result;
        }
        
        /// <summary>
        /// GetGoogleSearchForPatientCarehomeId
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="customerIds"></param>
        /// <param name="carehomeId"></param>
        /// <param name="offSet"></param>
        /// <param name="rowCount"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<GoogleSearchIndigoID_Result> GetGoogleSearchForPatientCarehomeId(string carehomeSearchIds, string patientSearchIds, string customerIds, string carehomeId, int offSet, int rowCount, string sortColumn, string sortOrder)
        {

            StringBuilder strSQL = new StringBuilder();

            ArrayList arrParam = new ArrayList();
            
            strSQL.Append("SELECT IndigoId, ObjectType as IdType , COUNT(*) OVER() TotalRows FROM Google gle WHERE (gle.CareHomeId IN (");
            strSQL.Append(FormatQueryForInClause(carehomeSearchIds, "careHomeId", ref arrParam));                      
            strSQL.Append(") OR ( gle.IndigoId IN  (");
            strSQL.Append(FormatQueryForInClause(patientSearchIds, "patientId", ref arrParam));                       
            strSQL.Append(") AND ObjectType = '10113')) AND ");
            
            if (!string.IsNullOrEmpty(customerIds))
            {
                strSQL.Append(" gle.CustomerId IN (");
                strSQL.Append(FormatQueryForInClause(customerIds, "customerId", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(carehomeId))
            {
                strSQL.Append(" gle.CareHomeId IN (");
                strSQL.Append(FormatQueryForInClause(carehomeId, "careHomeId", ref arrParam));
                strSQL.Append(") AND ");
            }

            strSQL = new StringBuilder(strSQL.ToString().Substring(0, strSQL.Length - 4));

            strSQL.Append(" ORDER BY " + sortColumn + " " + sortOrder);
            strSQL.Append(" OFFSET " + offSet + " ROWS ");
            strSQL.Append(" FETCH NEXT " + rowCount + " ROWS ONLY ");

            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndigoID_Result>(strSQL.ToString(), arrParam.ToArray()).ToList();
            return result;
        }   

        /// <summary>
        /// Gives the Indigo Id list for Patient for all records matching the exact search ID
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public List<GoogleSearchIndigoID_Result> GetGoogleSearchForPatientOrder(string searchText, string paddedSearchText, string customerIds, string carehomeId,   int offSet, int rowCount, string sortColumn, string sortOrder)
        {

            StringBuilder strSQL = new StringBuilder();

            ArrayList arrParam = new ArrayList();

            strSQL.Append("WITH OrderData AS (");

            strSQL.Append("SELECT ord.PatientId AS IndigoId, 10113 AS IdType , 1 AS TotalRows FROM Orders ord INNER JOIN IDXOrderProduct idxop ON ord.OrderId = idxop.OrderId " +
                         " INNER JOIN Patient pat ON pat.PatientId = ord.PatientId AND ");

            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL.Append(" (ord.SAPOrderNo = " + AddSQLParameter("sapOrderNo", searchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR ord.SAPOrderNo = " + AddSQLParameter("orderNo", paddedSearchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR idxop.SAPDeliveryNo = " + AddSQLParameter("sapDeliveryNo", searchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR idxop.SAPDeliveryNo = " + AddSQLParameter("deliveryNo", paddedSearchText, SqlDbType.NVarChar, ref arrParam) + ") AND ");
            }

            if (!string.IsNullOrEmpty(customerIds))
            {
                strSQL.Append(" pat.CustomerId IN (");
                strSQL.Append(FormatQueryForInClause(customerIds, "customerId", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(carehomeId))
            {
                strSQL.Append(" pat.CareHomeId IN (");
                strSQL.Append(FormatQueryForInClause(carehomeId, "careHomeId", ref arrParam));
                strSQL.Append(") AND ");
            }

            strSQL = new StringBuilder(strSQL.ToString().Substring(0, strSQL.Length - 4));

            strSQL.Append(" AND ord.PatientId IS NOT NULL ");
            strSQL.Append(" ORDER BY " + sortColumn + " " + sortOrder);
            strSQL.Append(" OFFSET " + offSet + " ROWS ");
            strSQL.Append(" FETCH NEXT " + rowCount + " ROWS ONLY) ");
            strSQL.Append(" SELECT TOP 1 IndigoId, IdType, TotalRows FROM OrderData");
            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndigoID_Result>(strSQL.ToString(), arrParam.ToArray()).ToList();
            return result;
        }


        /// <summary>
        /// Gives the Indigo Id list for carehome for all records matching the exact search ID
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public List<GoogleSearchIndigoID_Result> GetGoogleSearchForCarehomeOrder(string searchText, string paddedSearchText, string customerIds, string carehomeId, int offSet, int rowCount, string sortColumn, string sortOrder)
        {           
            StringBuilder strSQL = new StringBuilder();

            ArrayList arrParam = new ArrayList();

            strSQL.Append("WITH OrderData AS (");

            strSQL.Append("SELECT ord.CarehomeId AS IndigoId, 10114 AS IdType , 1 AS TotalRows FROM Orders ord INNER JOIN IDXOrderProduct idxop ON ord.OrderId = idxop.OrderId " +
                         " INNER JOIN Carehome ca ON ca.CarehomeId = ord.CarehomeId AND ");

            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL.Append(" (ord.SAPOrderNo = " + AddSQLParameter("sapOrderNo", searchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR ord.SAPOrderNo = " + AddSQLParameter("orderNo", paddedSearchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR idxop.SAPDeliveryNo = " + AddSQLParameter("sapdeliveryNo", searchText, SqlDbType.NVarChar, ref arrParam));
                strSQL.Append(" OR idxop.SAPDeliveryNo = " + AddSQLParameter("deliveryNo", paddedSearchText, SqlDbType.NVarChar, ref arrParam) + " ) AND ");
            }

            if (!string.IsNullOrEmpty(customerIds))
            {
                strSQL.Append(" ca.CustomerId IN (");
                strSQL.Append(FormatQueryForInClause(customerIds, "customerId", ref arrParam));
                strSQL.Append(") AND ");
            }

            if (!string.IsNullOrEmpty(carehomeId))
            {
                strSQL.Append(" ca.CareHomeId IN (");
                strSQL.Append(FormatQueryForInClause(carehomeId, "carehomeId", ref arrParam));
                strSQL.Append(") AND ");
            }

            strSQL = new StringBuilder(strSQL.ToString().Substring(0, strSQL.Length - 4));
            strSQL.Append(" AND ord.PatientId IS NULL ");
            strSQL.Append(" ORDER BY " + sortColumn + " " + sortOrder);
            strSQL.Append(" OFFSET " + offSet + " ROWS ");
            strSQL.Append(" FETCH NEXT " + rowCount + " ROWS ONLY) ");
            strSQL.Append(" SELECT TOP 1 IndigoId, IdType, TotalRows FROM OrderData");
            var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndigoID_Result>(strSQL.ToString(), arrParam.ToArray()).ToList();
            return result;
        }

		/// <summary>
		/// Get Customers for Indexing
		/// </summary>
		/// <returns></returns>
        public List<GoogleSearchIndex_Result> GetCustomersForIndexing(string indigoId)
		{
            ArrayList arrParam = new ArrayList();
			string strSQL = @" SELECT c.CustomerId as IndigoId, 10115 as  IdType,                                       
                                      ISNULL(a.PostCode, '') + ' ' + 
                                      ISNULL(p.FirstName, '') + ' ' + ISNULL(p.LastName, '') + ' ' + 
                                      ISNULL(p.TelephoneNumber, '') as Fields,
                                      c.CustomerId CustomerId, NULL CarehomeId, ISNULL(c.Customer_Status,'') Status, NULL PatientTypeID,
                                      NULL PatientType,
                                      ISNULL(p.FirstName,'') FirstName, ISNULL(p.FirstName,'') LastName,
                                      NULL CareHomeName,
                                      a.PostCode Postcode,
                                      NULL DateofBirth,
                                      NULL DeliveryDate
                                FROM   Customer c
                                INNER JOIN PersonalInformation p
                                ON   c.PersonalInformationId = p.PersonalInformationId
                                INNER JOIN Address a
                                ON   p.AddressId = a.AddressId";
            if(!string.IsNullOrEmpty(indigoId))
            {
                strSQL = strSQL + " WHERE c.CustomerId =" + AddSQLParameter("indigoId", indigoId, SqlDbType.NVarChar, ref arrParam) + "";

            }
			var result = _dbContext.DataContext.Database.SqlQuery<GoogleSearchIndex_Result>(strSQL, arrParam.ToArray()).ToList();
			return result;
		}

        public List<Google> GetGoogleSearchForIndigoIDObjectType(long indigoId,long objectType)
        {
            string strSQL = @"SELECT *
                              FROM  Google
                              WHERE IndigoId=" + indigoId  + " AND ObjectType=" + objectType;

            var result = _dbContext.DataContext.Database.SqlQuery<Google>(strSQL).ToList();
            return result;
        }

        
		/// <summary>
		/// Finalizes an instance of the <see cref="PatientCustomRepository"/> class.
		/// </summary>
		~GoogleSearchRepository()
		{
			Dispose(false);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged and - optionally - managed resources.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		private void Dispose(bool disposing)
		{
			if (!_isDisposed && disposing)
			{
				DisposeCore();
			}

			_isDisposed = true;
		}

		/// <summary>
		/// Disposes the core.
		/// </summary>
		protected void DisposeCore()
		{
			if (_dbContext != null)
			{
				_dbContext.Dispose();
			}
		}
	}
}
