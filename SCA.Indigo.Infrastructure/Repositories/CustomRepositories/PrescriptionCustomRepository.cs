﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Infrastructure
// Author           : Mamatha Shetty
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="PrescriptionCustomRepository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Infrastructure.Repositories.CustomRepositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    using SCA.Indigo.Model;

    /// <summary>
    /// Interface PatientCustomRepository
    /// </summary>
    public interface IPatientCustomRepository : IDisposable
    {
        /// <summary>
        /// Gets the patient prescriptions.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="showRemoved">if set to <c>true</c> [show removed].</param>
        /// <returns></returns>
        IQueryable<IDXPrescriptionProduct> GetPatientPrescriptions(long patientId, bool showRemoved);

        /// <summary>
        /// Gets the patient contact information.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        IQueryable<PersonalInformation> GetPatientContactInformation(long patientId);
    }

    /// <summary>
    /// Patient CustomRepository 
    /// </summary>
    public class PatientCustomRepository : IPatientCustomRepository
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private DBContext _dbContext;
        /// <summary>
        /// The _unit of work
        /// </summary>
        private UnitOfWork _unitOfWork;
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientCustomRepository"/> class.
        /// </summary>
        public PatientCustomRepository()
        {
            _dbContext = new DBContext();
            _unitOfWork = _dbContext.GetUnitOfWork();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="PatientCustomRepository"/> class.
        /// </summary>
        ~PatientCustomRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets all patients.
        /// </summary>
        /// <value>
        /// All patients.
        /// </value>
        public IQueryable<Patient> AllPatients
        {
            get { return _dbContext.Patients; }
        }

        /// <summary>
        /// Gets all prescriptions products.
        /// </summary>
        /// <value>
        /// All prescriptions products.
        /// </value>
        public IQueryable<IDXPrescriptionProduct> AllPrescriptionsProducts
        {
            get { return _dbContext.IDXPrescriptionProducts; }
        }

        /// <summary>
        /// Gets all prescriptions.
        /// </summary>
        /// <value>
        /// All prescriptions.
        /// </value>
        public IQueryable<Prescription> AllPrescriptions
        {
            get { return _dbContext.Prescriptions; }
        }

        /// <summary>
        /// Gets all personal informations.
        /// </summary>
        /// <value>
        /// All personal informations.
        /// </value>
        public IQueryable<PersonalInformation> AllPersonalInformations
        {
            get { return _dbContext.PersonalInformations; }
        }

        /// <summary>
        /// Gets the patient prescriptions.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="showRemoved">if set to <c>true</c> [show removed].</param>
        /// <returns></returns>
        public IQueryable<IDXPrescriptionProduct> GetPatientPrescriptions(long patientId, bool showRemoved)
        {
            var prescriptions = AllPrescriptions.Where(p => p.PatientId == patientId);

            if (showRemoved)
            {
                return AllPrescriptionsProducts.Where(idx => prescriptions.Where(pt => pt.PrescriptionId == idx.PrescriptionId).Any());
            }

            return AllPrescriptionsProducts.Where(idx => prescriptions.Where(pt => pt.PrescriptionId == idx.PrescriptionId).Any()
                                                                            && (idx.IsRemoved == false || idx.IsRemoved == null));
        }

        /// <summary>
        /// Gets the patient contact information.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns></returns>
        public IQueryable<PersonalInformation> GetPatientContactInformation(long patientId)
        {
            var patients = AllPatients.Where(p => p.PatientId == patientId);
            return AllPersonalInformations.Where(pi => patients.Where(p => p.PersonalInformationId == pi.PersonalInformationId).Any());
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
