﻿// ***********************************************************************
// Assembly         : OrderScheduler
// Author           : Sagar Yerva
// Created          : 02-26-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace OrderScheduler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Startup class for order
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Application starter
        /// </summary>
        /// <param name="args">string[] args</param>
        public static void Main(string[] args)
        {
            var objServiceConsumer = new ServiceConsumer();
            objServiceConsumer.SendOrderToSAPPI();
        }
    }
}
