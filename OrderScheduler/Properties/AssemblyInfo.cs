﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind
// Created          : 16-06-2015
// ***********************************************************************
// <copyright file="AssemblyInfo.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Assembly class for order</summary>
// ***********************************************************************
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OrderScheduler")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Capgemini")]
[assembly: AssemblyProduct("OrderScheduler")]
[assembly: AssemblyCopyright("Copyright © Capgemini 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("255b8231-6e77-46c3-bcb8-7990977df723")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
