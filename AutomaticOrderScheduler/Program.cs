﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind
// Created          : 16-06-2015
// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Startup class for clean master</summary>
// ***********************************************************************
namespace AutomaticOrderScheduler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;   

    /// <summary>
    /// Startup class for Automatic OrderScheduler
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Application starter
        /// </summary>
        /// <param name="args">string[] args</param>
        public static void Main(string[] args)
        {
            var objServiceConsumer = new ServiceConsumer();
            if (args.Length > 0)
            {
                var orderType = args[0];
                if (orderType.ToUpper() == "NONCAREHOME")
                {
                    objServiceConsumer.CreateNonCareHomeOrders();
                }
                else if (orderType.ToUpper() == "CAREHOME")
                {
                    objServiceConsumer.CreateCareHomeOrders();
                }
            }
        }
    }
}
