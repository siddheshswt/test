﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind
// Created          : 16-06-2015
// ***********************************************************************
// <copyright file="ServiceConsumer.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Service Consumer class for automatic order</summary>
// ***********************************************************************
namespace AutomaticOrderScheduler
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;

    /// <summary>
    /// Service Consumer class
    /// </summary>
    public class ServiceConsumer
    {
        /// <summary>
        /// Initializes a new instance of the ServiceConsumer
        /// </summary>
        public ServiceConsumer()
        {
            solutionPath = ConfigurationManager.AppSettings["BasePath"].ToString();
            this.BaseUrl = ConfigurationManager.AppSettings["BaseUrl"].ToString();
        }

        #region Class level variables

        /// <summary>
        /// Gets or sets HttpClient
        /// </summary>
        private static HttpClient ObjHttpClient { get; set; }

        /// <summary>
        /// Gets or sets RequestMessage
        /// </summary>
        private string RequestMessage { get; set; }

        /// <summary>
        /// Gets or sets RequestUrl
        /// </summary>
        private string RequestUrl { get; set; }

        /// <summary>
        /// Gets or sets BaseUrl
        /// </summary>
        private string BaseUrl { get; set; }

        /// <summary>
        /// Gets or sets SolutionPath
        /// </summary>
        private static string solutionPath;

        #endregion

        /// <summary>
        /// Create CareHome Orders
        /// </summary>
        public void CreateCareHomeOrders()
        {
            this.RequestUrl = "Order/GenerateAutomaticCareHomeOrders";
            this.GetHttpResponse("GenerateAutomaticCareHomeOrders", "Carehome");
        }

        /// <summary>
        /// Create Non CareHome Orders
        /// </summary>
        public void CreateNonCareHomeOrders()
        {
            this.RequestUrl = "Order/GenerateAutomaticCommunityOrders";
            this.GetHttpResponse("GenerateAutomaticCommunityOrders", "NonCarehome");
        }

        /// <summary>
        /// returns Http Response(Calling method-Get)
        /// </summary>
        /// <param name="batchHeader">batch Header</param>
        /// <param name="orderType">order Type</param>
        public void GetHttpResponse(string batchHeader, string orderType)
        {
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                var objHttpClient = new HttpClient();
                //// Set Time out 5 minut=300000 milisecond
                objHttpClient.Timeout = TimeSpan.FromMinutes(5);
                objHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = objHttpClient.GetAsync(new Uri(this.BaseUrl + "/" + this.RequestUrl)).Result;
                endTime = DateTime.Now;

                stringBuilder.Append(response.RequestMessage.ToString() + "\n");
                stringBuilder.Append(response.ToString());
            }
            catch (InvalidOperationException e)
            {
                stringBuilder.Append(e.Message + " \n");
                stringBuilder.Append(e.StackTrace + " \n");
            }
            catch (Exception e)
            {
                stringBuilder.Append(e.Message + " \n");
                stringBuilder.Append(e.StackTrace + " \n");
            }

            LogToFile(startTime, endTime, batchHeader, stringBuilder.ToString(), orderType, "TaskLog");
        }

        /// <summary>
        /// Log message to a txt file
        /// </summary>
        /// <param name="startTime">start Time</param>
        /// <param name="endTime">end Time</param>
        /// <param name="batchHeader">batch Header</param>
        /// <param name="logMessage">log Message</param>
        /// <param name="orderType">order Type</param>
        /// <param name="logFileName">log FileName</param>
        private static void LogToFile(DateTime startTime, DateTime endTime, string batchHeader, string logMessage, string orderType, string logFileName)
        {
            // create a writer and open the file           
            string strLogFilePath = Path.Combine(solutionPath + orderType, "LogFile\\" + logFileName + ".txt");

            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(string.Empty);
                textWriter.WriteLine("+++++++++++  " + batchHeader + " Batch Started at " + startTime + "  +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++ Batch End at " + endTime + "        +++++++++++++++");
            }
        }
    }
}
