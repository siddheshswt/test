﻿GRANT CONNECT TO [SCAOwner];


GO
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO [SCAOwner];


GO
GRANT REFERENCES
    ON CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification] TO [SCAOwner];

