﻿


-- =============================================

-- Author:		Siddehsh Sawant

--Modified By : Avinash

-- Modified date: 28-04-2015

-- Description:	Added new column for HasOrderActivated



-- ==========================================================================================

--	Modifeid By			Modified on		Reason

--	Avinash				01-JUNE-2015	Made patinetId = 0 when single/double click on customer or carehome
--	Avinash				15-July-2015	Added SAPCarehomeNumber
--	Avinash				20-July-2015	Added OrderActivated Condition For Carehome
--	Avinash				30-July-2015	modified condition for OrderActivated and hasOrderactivated
-- Jitendra K			05-August-2015	Added new column PatientSearch
-- Saurabh              28-June-2016    Included TelephoneNo In Google Search
-- Saurabh              22-July-2016    Included Corrspondence PostCode In Google Search
-- Saurabh              02-Aug-2016     Added Isnull for TelephoneNumber Column
-- Saurabh              12-Aug-2016     Patient NDD Is Null When No Active Prescription
-- Siddhesh             19-Sep-2016     Comment SAPDeliveryNo
-- Saurabh              20-Sep-2016     Added join with IDXOrderProduct table for SAPDeliveryNo 
-- Jitendra K           27-Sep-2016     Performance tune changes
-- Siddhesh             14-Oct-2016     Search by carehome name, order activated tick should be for CH Patient Order (201), 
--																									CH OneOff Standard (205), 
--																									CH OneOff Emergency (206), 
--																									CH OneOff FOC (207), 
--																									CH Product Order (208), 
--																									Automatic Order (401)
-- ==========================================================================================

CREATE VIEW [dbo].[PatientOrderSearchResult]

AS
Select *,CAST(PatientName AS VARCHAR)+CAST(PostCode AS VARCHAR)+CAST(RTRIM(CareHomeName) AS VARCHAR)+ISNULL(CONVERT(VARCHAR(10), DateofBirth,103),'')+CAST(ISNULL(TelephoneNumber,'') AS VARCHAR(20))+CAST(ISNULL(CPostCode,'') AS VARCHAR(50)) As PatientSearch from 
--Select *,PatientName+PostCode+RTRIM(CareHomeName)+ISNULL(CONVERT(VARCHAR(10), DateofBirth,103),'')+ISNULL(TelephoneNumber,'')+ISNULL(CPostCode,'') As PatientSearch from 
(
SELECT 

0 as PatientId,

C.CustomerId as CustomerId,

 LSTTitle.DefaultText AS TitleId,

             P.FirstName + ' ' +ISNULL(P.LastName,'') AS PatientName,

                                ISNULL(P.FirstName,'') as FirstName,

                                ISNULL(P.LastName, '') AS LastName,

								P.TelephoneNumber,

                                AD.HouseNumber,

                                AD.HouseName,

                                 ISNULL(AD.AddressLine1,'') as AddressLine1,

                                AD.AddressLine2,

                                AD.City,

                                AD.County,

                                AD.Country,

                                ISNULL(AD.PostCode, '') as PostCode,

								NULL as DateofBirth,

								NULL AS PatientType,

                                LstStatus.DefaultText AS PatientStatus,

                                C.CustomerId AS 'PatientCustomer',                                

								NULL as DeliveryDate,

								NULL as SAPOrderNo,

								NULL as SAPDeliveryNo,

                                0 AS OrderActivated,

								cast(0 as bit) AS HasActiveOrder,

                                P.FirstName + ' ' + ISNULL(P.LastName,'') AS CustomerName,

								ISNULL('', '') AS CareHomeName,

								C.SAPCustomerNumber AS SAPCustomerNumber,

								NULL As SAPPatientNumber,

								0 as PatientTypeId,

								LstStatus.ListId as PatientStatusId,

								0 as CareHomeId,

								10115 as objectType,
								NULL CPostCode

                                

from Customer as C(nolock) 

LEFT OUTER JOIN PersonalInformation as P(nolock) ON C.PersonalInformationId = P.PersonalInformationId

 LEFT OUTER JOIN Address AS AD(NOLOCK) 

                                ON AD.AddressId = P.AddressId

                LEFT OUTER JOIN List AS LstStatus(NOLOCK) 

                                ON C.Customer_Status = LstStatus.ListId

                LEFT OUTER JOIN List AS LSTTitle(NOLOCK)

                                ON P.TitleId = LSTTitle.ListId



								UNION 







								SELECT 

0 as PatientId,

CH.CustomerId as CustomerId,

 LSTTitle.DefaultText AS TitleId,

             P.FirstName + ' ' +ISNULL(P.LastName,'') AS PatientName,

                                 ISNULL(P.FirstName,'') as FirstName,

               ISNULL(P.LastName, '') AS LastName,

								P.TelephoneNumber,

                                AD.HouseNumber,

                                AD.HouseName,

                                 ISNULL(AD.AddressLine1,'') as AddressLine1,

                                AD.AddressLine2,

         AD.City,

                                AD.County,

                                AD.Country,

 ISNULL(AD.PostCode, '') as PostCode,

								NULL as DateofBirth,

								'CareHome' AS PatientType,

                                LstStatus.DefaultText AS PatientStatus,

                                CH.CustomerId AS 'PatientCustomer',                                

								CH.NextDeliveryDate as DeliveryDate,

								NULL as SAPOrderNo,

								NULL as SAPDeliveryNo,

                   	(SELECT CASE WHEN COUNT(*) > 0

									THEN 1

									ELSE 0

									END                                             

									FROM Orders O (NOLOCK)                                                                 

									WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND									

									O.CareHomeId = CH.CareHomeId AND O.OrderCreationType IN (201, 205, 206, 207, 208, 401)) AS OrderActivated,

								cast(0 as bit) AS HasActiveOrder,

                                Pcust.FirstName + ' ' + ISNULL(Pcust.LastName,'') AS CustomerName,

								P.FirstName + ' ' + ISNULL(P.LastName,'') AS CareHomeName,

								ISNULL(C.SAPCustomerNumber, '') AS SAPCustomerNumber,

								ISNULL(CH.SAPCareHomeNumber, '') As SAPPatientNumber,

								0 as PatientTypeId,

								LstStatus.ListId as PatientStatusId,

								CH.CareHomeId as CareHomeId,

								10114 as objectType,
								NULL CPostCode
                                

from CareHome as CH(nolock) 

LEFT OUTER JOIN PersonalInformation as P(nolock) ON CH.PersonalInformationId = P.PersonalInformationId

 LEFT OUTER JOIN Address AS AD(NOLOCK) 

                                ON AD.AddressId = P.AddressId

                    LEFT OUTER JOIN List AS LstStatus(NOLOCK) 

                                ON CH.CareHomeStatus = LstStatus.ListId

				LEFT OUTER JOIN List AS LstType(NOLOCK) 

                                ON CH.CareHomeType = LstType.ListId

                LEFT OUTER JOIN List AS LSTTitle(NOLOCK)

                                ON P.TitleId = LSTTitle.ListId

				LEFT OUTER JOIN Customer as C ON C.CustomerId = CH.CustomerId

				 LEFT OUTER JOIN PersonalInformation as Pcust(nolock) ON C.PersonalInformationId = Pcust.PersonalInformationId



								UNION



SELECT        PT.PatientId, PT.CustomerId, ISNULL(LSTTitle.DefaultText, '') AS TitleId, ISNULL(P.FirstName + ' ' + P.LastName, '') AS PatientName, ISNULL(P.FirstName, ' ') 

                         AS FirstName, ISNULL(P.LastName, ' ') AS LastName, ISNULL(P.TelephoneNumber, ' ') AS TelephoneNumber, ISNULL(AD.HouseNumber, ' ') AS HouseNumber, 

                         ISNULL(AD.HouseName, ' ') AS HouseName, ISNULL(AD.AddressLine1, '') AS AddressLine1, ISNULL(AD.AddressLine2, '') AS AddressLine2, ISNULL(AD.City, '') 

                         AS City, ISNULL(AD.County, '') AS County, ISNULL(AD.Country, '') AS Country, ISNULL(AD.PostCode, ' ') AS PostCode, PT.DateofBirth, ISNULL(LstPType.DefaultText, '') 

                         AS PatientType, ISNULL(LstStatus.DefaultText, '') AS PatientStatus, PT.CustomerId AS PatientCustomer,
						 CASE WHEN PRESCPRO.PrescriptionId IS NOT NULL THEN PT.NextDeliveryDate ELSE NULL END as DeliveryDate,

                         ISNULL(ORD.SAPOrderNo, '') AS SAPOrderNo, ISNULL(IOP.SAPDeliveryNo, '') AS SAPDeliveryNo,

                             (SELECT        CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END AS Expr1

                               FROM            dbo.Orders AS O WITH (NOLOCK)

                               WHERE        (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND (PatientId = PT.PatientId)) AS OrderActivated,

							   (SELECT CASE WHEN COUNT(*) > 0 THEN cast(1 as bit) ELSE cast(0 as bit)

                                    END                                             

									FROM Orders O (NOLOCK)                                

									WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND

										  O.PatientId = PT.PatientId) AS HasActiveOrder,

							    ISNULL(PC.FirstName, ' ') + ' ' + ISNULL(PC.LastName, 

                         ' ') AS CustomerName, ISNULL(PCH.FirstName, ' ') + ' ' + ISNULL(PCH.LastName, ' ') AS CareHomeName, CS.SAPCustomerNumber, PT.SAPPatientNumber, 

                         LstPType.ListId AS PatientTypeId, PT.PatientStatus AS PatientStatusId, PT.CareHomeId,10113 as objectType, ISNULL(CAD.PostCode, ' ') CPostCode

FROM            dbo.Patient AS PT WITH (NOLOCK) LEFT OUTER JOIN

                         dbo.PersonalInformation AS P WITH (NOLOCK) ON PT.PersonalInformationId = P.PersonalInformationId LEFT OUTER JOIN

     dbo.Address AS AD WITH (NOLOCK) ON AD.AddressId = P.AddressId LEFT OUTER JOIN
	 dbo.Address AS CAD WITH (NOLOCK) ON CAD.AddressId = P.CorrespondenceAddressId LEFT OUTER JOIN

                         dbo.List AS LstPType WITH (NOLOCK) ON PT.PatientType = LstPType.ListId LEFT OUTER JOIN

                         dbo.List AS LstStatus WITH (NOLOCK) ON PT.PatientStatus = LstStatus.ListId LEFT OUTER JOIN

                         dbo.List AS LSTTitle WITH (NOLOCK) ON P.TitleId = LSTTitle.ListId LEFT OUTER JOIN

                         dbo.Orders AS ORD WITH (nolock) ON PT.PatientId = ORD.PatientId AND ORD.OrderStatus = 10047 LEFT OUTER JOIN

						 dbo.IDXOrderProduct AS IOP WITH (nolock) ON IOP.OrderId = ORD.OrderId LEFT OUTER JOIN

                         dbo.Customer AS CS WITH (nolock) ON CS.CustomerId = PT.CustomerId LEFT OUTER JOIN

                         dbo.PersonalInformation AS PC WITH (nolock) ON PC.PersonalInformationId = CS.PersonalInformationId LEFT OUTER JOIN

                         dbo.CareHome AS CH WITH (nolock) ON CH.CareHomeId = PT.CareHomeId LEFT OUTER JOIN

                         dbo.PersonalInformation AS PCH WITH (nolock) ON PCH.PersonalInformationId = CH.PersonalInformationId
						 LEFT JOIN dbo.Prescription PRESC(NOLOCK) ON PT.PatientId = PRESC.PatientId
                         LEFT JOIN (SELECT PrescriptionId FROM dbo.IDXPrescriptionProduct(NOLOCK) WHERE IsRemoved = 0 GROUP BY PrescriptionId) PRESCPRO
                         ON PRESC.PrescriptionId = PRESCPRO.PrescriptionId

						 ) AS A



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[7] 4[11] 2[60] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PT"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 269
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "P"
            Begin Extent = 
               Top = 6
               Left = 307
               Bottom = 135
               Right = 514
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AD"
            Begin Extent = 
               Top = 6
               Left = 552
               Bottom = 135
               Right = 722
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LstPType"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LstStatus"
            Begin Extent = 
               Top = 138
               Left = 246
               Bottom = 267
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LSTTitle"
            Begin Extent = 
               Top = 138
               Left = 454
               Bottom = 267
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ORD"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 399
               Right = 244
            End
            DisplayFlags = 280
            TopC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PatientOrderSearchResult';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'olumn = 0
         End
         Begin Table = "CS"
            Begin Extent = 
               Top = 270
               Left = 282
               Bottom = 399
               Right = 489
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PC"
            Begin Extent = 
               Top = 270
               Left = 527
               Bottom = 399
               Right = 734
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CH"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 531
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PCH"
            Begin Extent = 
               Top = 402
               Left = 287
               Bottom = 531
               Right = 494
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 26
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1320
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PatientOrderSearchResult';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PatientOrderSearchResult';

