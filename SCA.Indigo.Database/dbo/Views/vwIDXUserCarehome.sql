﻿-- =============================================
-- Author:		Saurabh Mayekar
-- Create date: 15-09-2016
-- Description:	Get care home With mapped Users
-- =============================================

--		Modified Date		Modified By			Purpose


-- SELECT * FROM vwIDXUserCarehome

CREATE VIEW vwIDXUserCarehome
AS
WITH cte AS
(SELECT CarehomeId, STUFF((SELECT ',' + CAST(UserId AS VARCHAR(100)) [text()]
         FROM IDXUserCarehome (NOLOCK)
         WHERE CarehomeId = i.CarehomeId
         FOR XML PATH(''), TYPE)
        .value('.','VARCHAR(MAX)'),1,1,'') CarehomeUsers
FROM  IDXUserCarehome i (NOLOCK)
GROUP BY CarehomeId
)
SELECT CareHome.CareHomeId,cte.CarehomeUsers 
FROM CareHome (NOLOCK)
LEFT JOIN cte 
ON cte.CarehomeId = CareHome.CareHomeId