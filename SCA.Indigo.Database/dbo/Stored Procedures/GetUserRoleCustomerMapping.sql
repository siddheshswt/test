﻿-- ==========================================================================================
--	Modifeid By			Modified on		Reason
--  Saurabh             19/09/2016      Added condition And Users.IsCarehomeUser = 0 for non carehome users

CREATE PROC [dbo].[GetUserRoleCustomerMapping]

(

	 @UserName nvarchar(100),

	 @SapCustomerId nvarchar(100)

)

AS 

BEGIN





IF @UserName = 'NULL'

BEGIN

SET @UserName=''

END



IF @SapCustomerId = 'NULL'

BEGIN

SET @SapCustomerId=''

END



--IF(LEN(@SapCustomerId) > 0 and LEN(@SapCustomerId) < 10)

--BEGIN

--       set @SapCustomerId = replicate('0', 10 - len(@SapCustomerId)) + cast (@SapCustomerId as varchar)

--END





SELECT IDXUserRole.IDXUserRoleId, Users.UserId,Users.UserName, IDXUserRole.RoleID, Customer.CustomerId, Customer.SAPCustomerNumber

	FROM USERS  Users WITH (NOLOCK)

	LEFT OUTER JOIN IDXUserRole IDXUserRole WITH (NOLOCK)

	ON IDXUserRole.UserID=Users.UserId

	LEFT OUTER JOIN IDXUserCustomer  IDXUserCustomer WITH (NOLOCK)

	ON Users.UserId=IDXUserCustomer.UserId

	LEFT OUTER JOIN Customer Customer WITH (NOLOCK)

	ON Customer.CustomerId=IDXUserCustomer.CustomerId

	

WHERE 

        (

                        CASE

                                        WHEN  @UserName = '' THEN 1

                                        WHEN  Users.UserName like  '%'+ @UserName+'%' THEN 1

                                        ELSE 0

                        END = 1

        )

              and

        (

                        CASE

                                        WHEN  @SapCustomerId = '' THEN 1

                                        WHEN  Customer.SAPCustomerNumber like '%' +  @SapCustomerId + '%' THEN 1

                                        ELSE 0

                        END = 1

        )

		AND Users.IsActive = 1
		AND Users.IsCarehomeUser = 0

		UNION



SELECT IDXUserRole.IDXUserRoleId, Users.UserId,Users.UserName, IDXUserRole.RoleID, Customer.CustomerId, Customer.SAPCustomerNumber

	FROM USERS  Users WITH (NOLOCK)

	INNER JOIN IDXUserRole IDXUserRole WITH (NOLOCK)

	ON IDXUserRole.UserID=Users.UserId

	INNER JOIN Customer Customer WITH (NOLOCK)

	ON Customer.CustomerId IN (select CustomerId from Customer)

	where Users.UserName='SCAAdmin'

	AND Users.IsActive = 1
	AND Users.IsCarehomeUser = 0
	AND 

        (

                        CASE

                                        WHEN  @UserName = '' THEN 1

                                        WHEN  Users.UserName like  '%'+ @UserName+'%' THEN 1

                                        ELSE 0

                        END = 1

        )

              and

        (

                        CASE

                                        WHEN  @SapCustomerId = '' THEN 1

                                        WHEN  Customer.SAPCustomerNumber like '%' +  @SapCustomerId + '%' THEN 1

                                        ELSE 0

                        END = 1

        )





END

