﻿/*
Created By:		Siddhesh Sawant
Created Date:	14 Apr 2015
Purpose: Get the note details from order, patient and prescription table 

	Modified By		Modified Date	Purpose	
1.

*/

Create proc [dbo].[GetNoteDetailsForPatient]
@PatientId bigint
AS
BEGIN
WITH NoteDetails(PatientId,Note,CreatedBy,CreatedDate)
AS
(
SELECT	preNotes.PatientId, 
		CAST(preNotes.Note AS VARCHAR(MAX)), 
		preNotes.CreatedBy, 
		preNotes.CreatedDate FROM  
		PrescriptionNotes preNotes (NOLOCK) WHERE preNotes.PatientId = @PatientId
UNION ALL
SELECT  patNotes.PatientId, 
		CAST(patNotes.Note AS VARCHAR(MAX)), 
		patNotes.CreatedBy, 
		patNotes.CreatedDate FROM PatientNotes patNotes (NOLOCK) WHERE patNotes.PatientId = @PatientId
UNION ALL
SELECT  ord.PatientId, 
		CAST(ordNotes.Note AS VARCHAR(MAX)),
		ordNotes.CreatedBy,
		ordNotes.CreatedDate FROM OrderNotes ordNotes (NOLOCK) INNER JOIN Orders ord (NOLOCK) 
		ON ordNotes.OrderId = ord.OrderId WHERE ord.PatientId in (@PatientId)
)
SELECT PatientId,Note,CreatedBy,CreatedDate FROM NoteDetails ORDER BY CreatedDate DESC
END
