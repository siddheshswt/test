﻿

/******************************
** File:    AddCategory_usp
** Name:	AddCategory_usp
** Desc:	Procedure Returns search results as per input parameter given
** Auth:	Avinash
** Date:	11/11/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
** 2	02/07/2015		Avinash		Removed extra columns.
*******************************/

CREATE PROCEDURE [dbo].[GetSearchResults_usp_BK]
	-- Add the parameters for the stored procedure here
	@SearchText varchar(50),
	@UserId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @SearchResult table
(
	Result varchar(50)
)
 

DECLARE @maxCount BIGINT,
        @count BIGINT

		SET @maxCount = 20;        /*change to input more*/
SET @count = 0;

IF CHARINDEX('SCA Administrator',(select R.UserType+' '+R.RoleName from Role as R(nolock) INNER JOIN IDXUserRole as UR(nolock) ON R.RoleId = UR.RoleID where UR.UserID=@UserId)) > 0 
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
		on P.PersonalInformationId = PT.PersonalInformationId 
		where P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId 
		where P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId 
		where P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END
IF @count < @maxCount
BEGIN
	insert into @SearchResult 
	select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
	on P.PersonalInformationId = PT.PersonalInformationId 
	where P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)


	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId 
		where P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId 
		where P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 CONVERT(varchar(12), DateofBirth,103) from Patient  (nolock)
		where CONVERT(varchar(12), DateofBirth,103) like '%'+ @SearchText+'%' and DateofBirth NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN Customer as Cust (nolock)
		ON Cust.CustomerId = P.PersonalInformationId 
		where AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN CareHome as CH (nolock)
		ON CH.PersonalInformationId = P.PersonalInformationId 
		where AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN Patient as PT (nolock)
		ON PT.PersonalInformationId = P.PersonalInformationId 
		where AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

END
ELSE
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
		on P.PersonalInformationId = PT.PersonalInformationId where PT.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId where Cust.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId where CH.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END
IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
		on P.PersonalInformationId = PT.PersonalInformationId where PT.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END
IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId where Cust.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId where CH.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 CONVERT(varchar(12), DateofBirth,103) from Patient (nolock) where CustomerId 
		IN (select CustomerId from IDXUserCustomer where UserId=@userId) 
		and CONVERT(varchar(12), DateofBirth,103) like '%'+ @SearchText+'%' and DateofBirth NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN Customer as Cust (nolock)
		ON Cust.CustomerId = P.PersonalInformationId 
		where Cust.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@userId) 
		and AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN CareHome as CH (nolock)
		ON CH.PersonalInformationId = P.PersonalInformationId 
		where CH.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@userId)
		and AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN Patient as PT (nolock)
		ON PT.PersonalInformationId = P.PersonalInformationId 
		where PT.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@userId) 
		and AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

END


select top 20 Result from @SearchResult where Result is not NULL


    
END



