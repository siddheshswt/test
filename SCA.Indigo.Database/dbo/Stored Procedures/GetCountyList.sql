﻿


/******************************
** File:    GetCountyList
** Name:	GetCountyList
** Desc:	Gets list of Counties whose status is 1
** Auth:	Arvind
** Date:	29/01/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1	19/03/2015		Sagar		Added NOLOCK
**	2	15/06/2015		Sagar		Added Document Comment		
*******************************/

-- ===============================================================================
-- exec dbo.GetCountyList

CREATE Proc [dbo].[GetCountyList]

as begin
			select CountyId,County,CountyCode,Status from countyList (NOLOCK) where [status]='1'				
end



