﻿

------------------------------------------------------------------------------------
/*
Stored Procedure Name : [WriteSCAActionLog_usp]
Author : Mamatha Shetty
Desc: Stored procedure to log the User Actions and Exceptions, called from enterprise library
*/
------------------------------------------------------------------------------------
/******************************
********************************
** Change History
********************************
** PR	Date			Author		Description	
** 1	04-10-2016		Siddhesh	Added trnasaction and try catch block	
		
*******************************/

/****** Object:  Stored Procedure dbo.[WriteSCAActionLog_usp]    Script Date: 10/1/2004 3:16:36 PM ******/

CREATE PROCEDURE [dbo].[WriteSCAActionLog_usp]
(
	@EventID INT, 
	@Priority INT, 
	@Severity NVARCHAR(32), 
	@Title NVARCHAR(256), 
	@Timestamp DATETIME,
	@MachineName NVARCHAR(32), 
	@AppDomainName NVARCHAR(512),
	@ProcessID NVARCHAR(256),
	@ProcessName NVARCHAR(512),
	@ThreadName NVARCHAR(512),
	@Win32ThreadId NVARCHAR(128),
	@Message NVARCHAR(max),
	@FormattedMessage NTEXT,
	@LogId INT OUTPUT
)
AS 

----------------------------------------------------------------------------------------------------
BEGIN
	DECLARE @Result VARCHAR(200)
	DECLARE @ResultMessage VARCHAR(200)
	DECLARE @CurrentDate DATETIME = GETDATE()
	DECLARE @ModifiedBy VARCHAR(100) = CAST(NEWID() AS VARCHAR(100))
	BEGIN TRY
		BEGIN TRANSACTION
			IF(@Title = 'UserAction')
			BEGIN	
				DECLARE @ExtendedProperties NVARCHAR(max), @ActionName NVARCHAR(max),@Controller NVARCHAR(max), @UserId NVARCHAR(40) ;
				SET @ExtendedProperties =  (SELECT TOP 1 LTRIM(RTRIM(KeyValue)) from dbo.KeyValuePairs(@FormattedMessage, ':',',') WHERE KeyName = 'ExtendedProperties');

				SELECT 
				@ActionName = (SELECT TOP 1 KeyValue FROM KeyValuePairs(@ExtendedProperties,'-', '|') WHERE KeyName = 'ActionName'),--Will always get single row
				@Controller = (SELECT TOP 1 KeyValue FROM dbo.KeyValuePairs(@ExtendedProperties,'-', '|') WHERE KeyName = 'Controller'),--Will always get single row
				@UserId = (SELECT TOP 1 KeyValue FROM dbo.KeyValuePairs(@ExtendedProperties,'-', '|') WHERE KeyName = 'UserID')--Will always get single row	

				INSERT INTO UserActionLog(
					UserId,
					[Priority],		
					[Action],
					Controller,
					[Timestamp],
					MachineName,		
					ProcessID,		
					[Message],
					modifiedBy,
					ModifiedDate
		
				)
				VALUES (
					@UserId, 			
					@Priority, 		
					@ActionName,
					@Controller,
					@Timestamp,			
					@MachineName,
					@ProcessID,		
					@FormattedMessage,				
					@UserId,			
					GETDATE()
					)

				SET @LogID = @@IDENTITY
				--RETURN @LogID
			END
			ELSE
			BEGIN

				SET @ExtendedProperties =  (SELECT TOP 1 LTRIM(RTRIM(KeyValue)) from dbo.KeyValuePairs(SUBSTRING(@FormattedMessage, CHARINDEX('ExtendedProperties', @FormattedMessage), 2147483647), ':',',') WHERE KeyName = 'ExtendedProperties');
				SET @UserId = (SELECT TOP 1 KeyValue FROM dbo.KeyValuePairs(@ExtendedProperties,'-', '|') WHERE KeyName = 'UserID')--Will always get single row

				INSERT INTO ExceptionLog(		
							UserId,
							ErrorMessage,
							TimeStamp,
							modifiedBy,
							ModifiedDate		
						)
						VALUES (			
							@UserId, 
							@Message,
							@TimeStamp,		
							@UserId,
							GETDATE()
							)	

				SET @LogID = @@IDENTITY
				--RETURN @LogID	
			END			
		COMMIT
		RETURN @LogID
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: WriteSCAActionLog_usp' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @ModifiedBy
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage

	END CATCH
END
---------------------------------------------------------------------------



