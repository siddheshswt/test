﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/******************************
** File:    GetListDetailsOnListId_usp
** Name:	GetListDetailsOnListId_usp
** Desc:	Stored Procedure to get the lists based on listtypeId, ListId and languageId
** Auth:	Jagdish
** Date:	10/04/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
** 2	24/12/2015		Saurabh		Added "In" clause instead "=" in INNER JOIN IDXInteractiontypeSubType
*******************************/

-- =============================================
--	exec GetListDetailsOnListId_usp '10022', '20104', 1
	
CREATE PROCEDURE [dbo].[GetListDetailsOnListId_usp] 
	@listTypeId varchar(100),
	@listId varchar(100),
	@languageId INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
		lst.ListTypeId	
		,	lst.ListId
		,	lst.ListIndex
		,CASE WHEN t.TranslationType IS NULL
		THEN tp.Text
		ELSE  t.TranslationType 
		END DisplayText
	FROM List  lst (NOLOCK)
		INNER JOIN ListType lstType (NOLOCK) ON lst.ListTypeId = lstType.ListTypeId
		INNER JOIN TranslationProxy tp (NOLOCK) ON tp.TransProxyID = lst.TransProxyId
		LEFT OUTER JOIN Translation t (NOLOCK) ON t.TransProxyID = tp.TransProxyID AND t.LanguageID = CONVERT(VARCHAR(10),languageId)
		INNER JOIN IDXInteractiontypeSubType ts on lst.ListId = ts.InteractionsubtypeId and ts.InteractiontypeId in (SELECT Value FROM SplitToTable(@listId))
	WHERE
	(@listTypeId ='-1' or lst.ListTypeId in (SELECT Value FROM SplitToTable(@listTypeId)))
	order by lst.ListTypeId, lst.ListIndex
END





