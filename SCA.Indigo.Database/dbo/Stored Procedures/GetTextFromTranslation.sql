﻿-- =============================================
-- Author:		Siddhesh Sawant
-- Create date: 27 Sep 2016
-- Description:	Used to fetch the translation type based on language id 
-- =============================================
CREATE PROCEDURE [dbo].[GetTextFromTranslation]
	
	@Id BIGINT,
	@LanguageId BIGINT
AS
BEGIN

	SELECT dbo.GetTextFromTranslationByListId(@Id,@LanguageId) AS [TranslationText]
	
END



