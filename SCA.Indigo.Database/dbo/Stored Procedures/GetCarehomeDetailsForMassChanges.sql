﻿/******************************
** File:    GetCarehomeDetailsForMassChanges
** Name:	GetCarehomeDetailsForMassChanges
** Desc:	Adds a new category in List
** Auth:	Get Care home Details For Mass Changes
** Date:	12/06/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
*******************************/
--============================================================================										

-- exec [GetCarehomeDetailsForMassChanges] 9, '2,3,4,8', 0
-- exec [GetCarehomeDetailsForMassChanges] 9,'',10110
-- exec [GetCarehomeDetailsForMassChanges] 9,'2,3,4,8',10110
-- exec [GetCarehomeDetailsForMassChanges] 9

CREATE PROCEDURE [dbo].[GetCarehomeDetailsForMassChanges]
(
	-- Add the parameters for the stored procedure here
	@CustomerId bigint ,
	@careHomeId varchar(max) = null,
	@OrderType bigint = 0,
	@LanguageId bigint = 1,
	@CarehomeStatus varchar(100) = null
) 	
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		
		SELECT 
			c.LoadId,
			cust.SAPCustomerNumber AS CustomerId, 
			c.CareHomeId,
			c.SAPCareHomeNumber,
			pinfo.FirstName AS CareHomeName,
			
			--CareHome Address Details
			addr.HouseName,
			addr.AddressLine1,
			addr.AddressLine2,
			addr.City AS TownOrCity,
			addr.County,
			addr.Country,
			addr.PostCode,

			--Carehome Personal Information details
			pinfo.TelephoneNumber,
			pinfo.MobileNumber,
			pinfo.Email,
			tcs.Description AS CareHomeStatus,
			CONVERT(VARCHAR, c.NextDeliveryDate, 103) AS NextDeliveryDate,
			c.Round,
			c.RoundId,
			c.DeliveryFrequency,
			tct.Description AS CareHomeType,
			tot.Description AS OrderType,
			c.PurchaseOrderNo,
			CASE c.IsSentToSAP WHEN 1 THEN 'Y' ELSE 'N' END AS IsSentToSAP,
			c.BillTo,
			c.Remarks
		FROM CareHome c WITH(NOLOCK) 
			INNER JOIN Customer cust WITH(NOLOCK) ON c.CustomerId = cust.CustomerId
			INNER JOIN PersonalInformation pinfo WITH(NOLOCK)  on c.PersonalInformationId=pinfo.PersonalInformationId
			INNER JOIN Address addr WITH(NOLOCK)  on pinfo.AddressId =addr.AddressId
			INNER JOIN List lcs WITH(NOLOCK) ON c.CareHomeStatus = lcs.ListId
			INNER JOIN Translation tcs WITH(NOLOCK) ON lcs.TransProxyId = tcs.TransProxyID AND tcs.LanguageID = @LanguageId
			LEFT JOIN List lct WITH(NOLOCK) ON c.CareHomeType = lct.ListId
			LEFT JOIN Translation tct WITH(NOLOCK) ON lct.TransProxyId = tct.TransProxyID AND tct.LanguageID = @LanguageId	
			INNER JOIN List lot	WITH(NOLOCK) ON c.OrderType = lot.ListId
			INNER JOIN Translation tot WITH(NOLOCK) ON lot.TransProxyId = tot.TransProxyID AND tot.LanguageID = @LanguageId
		WHERE c.CustomerId = @CustomerId 
		AND ( @careHomeId IS NULL OR @careHomeId = '' OR c.SAPCareHomeNumber IN (select Value from dbo.SplitToTable(@careHomeId))) 
		AND ( @OrderType = 0 OR @OrderType IS NULL OR c.OrderType = @OrderType )
		AND ( @CarehomeStatus is null OR @CarehomeStatus = '' OR c.CareHomeStatus IN (select Value from dbo.SplitToTable(@CarehomeStatus)))
		ORDER BY c.CareHomeId
		
END







