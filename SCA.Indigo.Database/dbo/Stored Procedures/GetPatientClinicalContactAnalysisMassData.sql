﻿

-- =============================================
-- Author:		<Avinash>
-- Create date: <22 - 06- 2015>
-- Description:	<Returns Patients Medical staff and Analysis Destails>
-- EXEC [GetPatientClinicalContactAnalysisMassData] '242,241',9,1
-- =============================================
CREATE PROCEDURE [dbo].[GetPatientClinicalContactAnalysisMassData]
	@PatinetId VARCHAR(100),
	@CustomerId BIGINT,
	@TypeId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	IF @PatinetId = 'NULL'
		BEGIN
		SET @PatinetId=''
		END
	
    
	SELECT	IDXCustomer.CustomerID CustomerId,
			PT.SAPPatientNumber AS SAPPatientNumber,
			C.SAPCustomerNumber AS SAPCustomerNumber,
			IDXPatient.PatientId PatientId,
			MasterInfo.ClinicalContactAnalysisMasterId AS ClinicalContactTypeId,
			TransProxy.Text AS ClincialContactType,
			IDXCustomer.ContactName AS ClincialContactValue,
			CAST(IDXCustomer.IDXCustomerMedicalStaffAnalysisInfoID AS VARCHAR(100)) AS ClinicalContactValueTypeId,
			IDXPatient.Remarks AS Remarks

FROM IDXPatientMedicalStaff AS IDXPatient (NOLOCK)

INNER JOIN IDXCustomerMedicalStaffAnalysisInfo AS IDXCustomer (NOLOCK) ON IDXCustomer.IDXCustomerMedicalStaffAnalysisInfoID = IDXPatient.CustomerMedicalStaffId
INNER JOIN ClinicalContactAnalysisMaster AS MasterInfo (NOLOCK) ON MasterInfo.ClinicalContactAnalysisMasterId = IDXCustomer.MedicalAnalysisListID
INNER JOIN TranslationProxy AS TransProxy (NOLOCK) ON MasterInfo.TransProxyId = TransProxy.TransProxyID
INNER JOIN Patient AS PT (NOLOCK) ON PT.PatientId = IDXPatient.PatientId
INNER JOIN Customer AS C (NOLOCK) ON IDXCustomer.CustomerID = C.CustomerId
WHERE	
		(
			CASE
				WHEN  @PatinetId = '' THEN 1
				WHEN PT.SAPPatientNumber IN (select Value from dbo.SplitToTable(@PatinetId))  THEN 1
			ELSE 0
			END = 1
		)
		AND IDXCustomer.CustomerID = @CustomerId
		AND MasterInfo.TypeId=@TypeId

END


