﻿/*
===============================================================================
 Author      :	 Saurabh Mayekar
 Create date :   23-12-2015
 Description :	 Gets Interaction Report
===============================================================================

===============================================================================
		Modified Date		Modified By			Purpose
	1.	13-01-2016			Prafull Mohite		Changed Inner Join to Left join
	2.  15-01-2016          Saurabh Mayekar     Taken CareHomeId from Patient table In case It is NULL in Interaction table
	3.  20-01-2016          Saurabh Mayekar     Time part added to CreatedTo & ModifiedTo Date
	4.  22-01-2016          Saurabh Mayekar     Added zero padding for SAP Id columns
	5.  29-01-2016			Siddhesh Sawant		Added default sorting condition first customer then patient (last name) then created date desc
	6.  03-02-2016			Prafull Mohite		Modified default sorting condition when IsExport true 
	7.  03-10-2016          Saurabh Mayekar     Added Left Join vwIDXUserCarehome for Carehome Users
===============================================================================
P
EXEC GetInteractionReport 1,100,1,'9,23,26', '','', '20102,20103,20104', '', '', '', NULL, NULL, null, null
CH
EXEC GetInteractionReport 2,50,1,'11', '0000863506,0001418507','0000858494,0000858497', '', '', '', '', NULL, NULL, NULL, NULL

EXEC GetInteractionReport 2,50,1,NULL, '0001360697,0001452990,0001547472,0001547495',NULL, NULL, NULL, NULL, NULL, '27-Jul-2015', NULL, NULL, NULL
EXEC GetInteractionReport 2,50,1,'32', NULL ,NULL, NULL, NULL, NULL, NULL, '27-Jul-2015', '27-Jul-2015','27-Jul-2015', '27-Jul-2015'
EXEC GetInteractionReport 2,50,1,'23', NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
EXEC GetInteractionReport 2,100,0,'9,11,32', '', '', '', '', '', '', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '', '', '', '', '', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '', '', '', '', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '20102', '', '', '', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '20102', '20127', '', '', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '20102', '20127', '20100', '', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '20102', '20127', '20100', 'carol', NULL, NULL, NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '20102', '20127', '20100', '', NULL, '23-Dec-2015', NULL, NULL
EXEC GetInteractionReport 1,100,0,'9,11,32', '10978,7989,45206', '159,52', '20102', '20127', '20100', '','27-Jul-2015' ,'23-Dec-2015', NULL, NULL

EXEC GetInteractionReport 2,50,1,'23', NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PatientName', 'DESC', '5D9EE643-274D-40F8-9387-009292BDC75F'
EXEC GetInteractionReport 2,50,1,'23', NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PatientName', 'DESC', 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'

*/
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



CREATE PROC [dbo].[GetInteractionReport]
(
    @pageNum               BIGINT = 0,
    @recordCount           BIGINT = 0,
	@IsExport              BIT    = 1,
    @CustomerId            VARCHAR(4000),
	@PatientId             VARCHAR(4000),
	@CareHomeId            VARCHAR(4000),
	@InterActionTypeId     VARCHAR(4000),
	@InteractionSubTypeId  VARCHAR(4000),
	@StatusId              VARCHAR(4000),
	@CreatedBy             VARCHAR(4000),
	@CreatedDtFrom         DATETIME,
	@CreatedDtTo           DATETIME,
	@ModifiedDtFrom        DATETIME,
	@ModifiedDtTo          DATETIME,
	@SortColumn			   VARCHAR(50) = 'PatientName',	
	@SortOrder			   VARCHAR(10) = 'DESC',
	@UserId                VARCHAR(100)
)
AS
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.

SET NOCOUNT ON;
SET FMTONLY OFF; --Uncomment this, whenever input or output columns are changed and require EF Update

IF @SortColumn IS NULL SET @SortColumn = '' 
IF @SortOrder IS NULL SET @SortOrder = ''

SET @SortOrder = (SELECT UPPER(@SortOrder))

DECLARE @start BIGINT = 0,
        @end   BIGINT = 0,
        @TotalRecords BIGINT = 0

SET @start = ((@recordCount * @pageNum) - @recordCount) + 1
SET @end   = (@start-1) + @recordCount


IF @CreatedDtTo IS NOT NULL
    SET @CreatedDtTo = DATEADD(ms,998,DATEADD(SS,59,DATEADD(mi,59,DATEADD(HH,23,@CreatedDtTo))))

If @ModifiedDtTo IS NOT NULL
    SET @ModifiedDtTo = DATEADD(ms,998,DATEADD(SS,59,DATEADD(mi,59,DATEADD(HH,23,@ModifiedDtTo))))

DECLARE @CarehomeUserId VARCHAR(100) = ''
SELECT @CarehomeUserId = CAST(ISNULL(UserId,'') AS VARCHAR(100)) FROM Users WHERE IsCarehomeUser = 1 AND UserId = @UserId

SELECT @TotalRecords [TotalRecords], 
		--ROW_NUMBER() OVER(
		--ORDER BY interaction.ModifiedDate DESC
		--) [RowNumber], 
		ROW_NUMBER() OVER(
		ORDER BY 				
		CASE
		WHEN @SortOrder <> 'ASC' THEN ''
	    WHEN @SortColumn = 'CustomerName' THEN pinfoCustomer.FirstName
	    END ASC
		,CASE
        WHEN @SortOrder <> 'DESC' THEN ''
        WHEN @SortColumn = 'CustomerName' THEN pinfoCustomer.FirstName
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'CustomerId' THEN CAST(customer.SAPCustomerNumber AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'CustomerId' THEN CAST(customer.SAPCustomerNumber AS BIGINT)
        END DESC	
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'PatientName' THEN pinfoPatient.LastName
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'PatientName' THEN pinfoCustomer.FirstName
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'PatientId' THEN CAST(patient.SAPPatientNumber AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'PatientId' THEN CAST(patient.SAPPatientNumber AS BIGINT)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'CareHomeName' THEN pinfoCareHome.FirstName
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'CareHomeName' THEN pinfoCareHome.FirstName
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'CareHomeId' THEN CAST(carehome.SAPCareHomeNumber AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'CareHomeId' THEN CAST(carehome.SAPCareHomeNumber AS BIGINT)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'InteractionType' THEN listInteractionType.DefaultText
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'InteractionType' THEN listInteractionType.DefaultText
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'SubType' THEN listInteractionSubType.DefaultText
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'SubType' THEN listInteractionSubType.DefaultText
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'StatusId' THEN listStatusId.DefaultText
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'StatusId' THEN listStatusId.DefaultText
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'CreatedOn' THEN CAST(interaction.CreatedDate AS DATE)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'CreatedOn' THEN CAST(interaction.CreatedDate AS DATE)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'CreatedBy' THEN usersCreatedBy.UserName
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'CreatedBy' THEN usersCreatedBy.UserName
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'ModifiedOn' THEN CAST(interaction.ModifiedDate AS DATE)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'ModifiedOn' THEN CAST(interaction.ModifiedDate AS DATE)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'ModifiedBy' THEN usersModifiedBy.UserName
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'ModifiedBy' THEN usersModifiedBy.UserName
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'Description' THEN interaction.Description
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'Description' THEN interaction.Description
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'Alert' THEN CAST(interaction.DisplayAsAlert AS BIT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'Alert' THEN CAST(interaction.DisplayAsAlert AS BIT)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'AssignedTo' THEN usersAssignTo.UserName
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'AssignedTo' THEN usersAssignTo.UserName
        END DESC
		,CASE		
		WHEN @SortOrder <> '' THEN ''		
        WHEN @SortColumn = '' THEN pinfoCustomer.FirstName END DESC, pinfoPatient.LastName DESC, CAST(interaction.CreatedDate AS DATE) DESC				
		) [RowNumber], 
	   pinfoCustomer.FirstName AS CustomerFirstName,		
       ISNULL(pinfoCustomer.FirstName,'') + ' ' + ISNULL(pinfoCustomer.LastName,'') [CustomerName], 
	   customer.SAPCustomerNumber [CustomerId], 
	   pinfoPatient.FirstName AS PatientFirstName,
	   pinfoPatient.LastName AS PatientLastName,
	   ISNULL(pinfoPatient.FirstName,'') + ' ' + ISNULL(pinfoPatient.LastName,'') [PatientName], 
	   patient.SAPPatientNumber [PatientId], 
	   ISNULL(pinfoCareHome.FirstName,'') [CareHomeName], 
	   carehome.SAPCareHomeNumber [CareHomeId], 
	   listInteractionType.DefaultText [InteractionType], 
	   listInteractionSubType.DefaultText [SubType], 
	   listStatusId.DefaultText [StatusId], 
	   CONVERT(VARCHAR(10),interaction.CreatedDate,103) [CreatedOn],
	   usersCreatedBy.UserName [CreatedBy], 
	   CONVERT(VARCHAR(10),interaction.ModifiedDate,103) [ModifiedOn],
	   usersModifiedBy.UserName [ModifiedBy], 
	   interaction.Description [Description], 
	   CASE WHEN interaction.DisplayAsAlert = 1 THEN 'YES' ELSE 'NO' END [Alert], 
	   usersAssignTo.UserName [AssignedTo]
INTO   ##tmpRecords
FROM        Interaction(NOLOCK)         [interaction]
INNER JOIN  Customer(NOLOCK)            [customer]
ON   interaction.CustomerId = customer.CustomerId
INNER JOIN  PersonalInformation(NOLOCK) [pinfoCustomer]
ON   pinfoCustomer.PersonalInformationId = customer.PersonalInformationId
LEFT JOIN  Patient(NOLOCK)             [patient]
ON   patient.PatientId = interaction.PatientId
LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehomeP
ON  idxUserCarehomeP.CarehomeId = patient.CareHomeId
LEFT JOIN  PersonalInformation(NOLOCK) [pinfoPatient]
ON   patient.PersonalInformationId = pinfoPatient.PersonalInformationId
LEFT JOIN  CareHome(NOLOCK)            [carehome]
ON   carehome.CareHomeId = ISNULL(interaction.CarehomeId, patient.CareHomeId)
LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehomeC
ON  idxUserCarehomeC.CarehomeId = carehome.CareHomeId
LEFT JOIN  PersonalInformation(NOLOCK) [pinfoCareHome]
ON   carehome.PersonalInformationId = pinfoCareHome.PersonalInformationId
INNER JOIN  List(NOLOCK)                [listInteractionType]
ON   listInteractionType.ListTypeId = '10021'
AND  listInteractionType.ListId = interaction.InteractionTypeId
INNER JOIN  List(NOLOCK)                [listInteractionSubType]
ON   listInteractionSubType.ListTypeId = '10022'
AND  listInteractionSubType.ListId = interaction.InteractionSubTypeId
INNER JOIN  List(NOLOCK)                [listStatusId]
ON   listStatusId.ListTypeId = '10020'
AND  listStatusId.ListId = interaction.StatusId
LEFT JOIN  users(NOLOCK)               [usersCreatedBy]
ON   usersCreatedBy.UserId = interaction.CreatedBy
LEFT JOIN  users(NOLOCK)               [usersModifiedBy]
ON   usersModifiedBy.UserId = interaction.ModifiedBy
LEFT JOIN  users(NOLOCK)               [usersAssignTo]
ON   usersAssignTo.UserId = interaction.AssignToUserId
WHERE CASE WHEN ISNULL(@CustomerId,'') = '' THEN '%,' + CAST(ISNULL(interaction.CustomerId,'') AS VARCHAR) + ',%' 
                                            ELSE ',' + @CustomerId + ',' END                 LIKE '%,' + CAST(ISNULL(interaction.CustomerId,'') AS VARCHAR) + ',%'
AND   CASE WHEN ISNULL(@PatientId,'') = '' THEN '%,' + RIGHT('0000000000' + CAST(ISNULL(patient.SAPPatientNumber,'') AS VARCHAR),10) + ',%' 
                                            ELSE ',' + @PatientId + ',' END                 LIKE '%,' + RIGHT('0000000000' + CAST(ISNULL(patient.SAPPatientNumber,'') AS VARCHAR),10) + ',%'
AND   CASE WHEN ISNULL(@CareHomeId,'') = '' THEN '%,' + RIGHT('0000000000' + CAST(ISNULL(carehome.SAPCareHomeNumber,'') AS VARCHAR),10) + ',%' 
                                            ELSE ',' + @CareHomeId + ',' END                 LIKE '%,' + RIGHT('0000000000' + CAST(ISNULL(carehome.SAPCareHomeNumber,'') AS VARCHAR),10) + ',%'
AND   CASE WHEN ISNULL(@InterActionTypeId,'') = '' THEN '%,' + CAST(ISNULL(interaction.InteractionTypeId,'') AS VARCHAR) + ',%' 
                                            ELSE ',' + @InterActionTypeId + ',' END                 LIKE '%,' + CAST(ISNULL(interaction.InteractionTypeId,'') AS VARCHAR) + ',%'
AND   CASE WHEN ISNULL(@InteractionSubTypeId,'') = '' THEN '%,' + CAST(ISNULL(interaction.InteractionSubTypeId,'') AS VARCHAR) + ',%' 
                ELSE ',' + @InteractionSubTypeId + ',' END                 LIKE '%,' + CAST(ISNULL(interaction.InteractionSubTypeId,'') AS VARCHAR) + ',%'
AND   CASE WHEN ISNULL(@StatusId,'') = '' THEN '%,' + CAST(ISNULL(interaction.StatusId,'') AS VARCHAR) + ',%' 
                                            ELSE ',' + @StatusId + ',' END                 LIKE '%,' + CAST(ISNULL(interaction.StatusId,'') AS VARCHAR) + ',%'
AND   CASE WHEN ISNULL(@CreatedBy,'') = '' THEN '%,' + CAST(interaction.CreatedBy AS VARCHAR(100)) + ',%' 
                                            ELSE ',' + @CreatedBy + ',' END                 LIKE '%,' + CAST(interaction.CreatedBy AS VARCHAR(100)) + ',%'
AND  ((@CreatedDtFrom IS NOT NULL AND @CreatedDtTo IS NOT NULL AND interaction.CreatedDate >= @CreatedDtFrom AND interaction.CreatedDate <= @CreatedDtTo )
     OR
     (@CreatedDtFrom IS NOT NULL AND @CreatedDtTo IS NULL AND interaction.CreatedDate >= @CreatedDtFrom  )
     OR
     (@CreatedDtFrom IS NULL AND @CreatedDtTo IS NOT NULL AND interaction.CreatedDate <= @CreatedDtTo )
	 OR
	 (@CreatedDtFrom IS NULL AND @CreatedDtTo IS NULL AND 1 = 1))
AND  ((@ModifiedDtFrom IS NOT NULL AND @ModifiedDtTo IS NOT NULL AND interaction.ModifiedDate >= @ModifiedDtFrom AND interaction.CreatedDate <= @ModifiedDtTo )
     OR
     (@ModifiedDtFrom IS NOT NULL AND @ModifiedDtTo IS NULL AND interaction.ModifiedDate >= @ModifiedDtFrom  )
     OR
     (@ModifiedDtFrom IS NULL AND @ModifiedDtTo IS NOT NULL AND interaction.ModifiedDate <= @ModifiedDtTo )
	 OR
	 (@ModifiedDtFrom IS NULL AND @ModifiedDtTo IS NULL AND 1 = 1))
AND  (ISNULL(idxUserCarehomeP.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
      OR  ISNULL(idxUserCarehomeC.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%')

SELECT @TotalRecords = COUNT(*) FROM ##tmpRecords

UPDATE ##tmpRecords
SET    TotalRecords = @TotalRecords


IF (@IsExport = 1) 
       SELECT * FROM ##tmpRecords order by RowNumber --CustomerFirstName desc, PatientLastName desc, CreatedOn desc
 ELSE 	
     SELECT * FROM ##tmpRecords 
	    WHERE RowNumber BETWEEN @start and @end 

DROP TABLE ##tmpRecords

END