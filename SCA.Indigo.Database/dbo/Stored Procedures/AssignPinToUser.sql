﻿


--***********************************************************************************************
--Store Procedure Name	:	AssignPinToUser
--Created Date			:	19 Mar 2015
--Purpose				:	To update authorisation pin for user
--Created By			:	Siddhesh Sawant [siddhesh.a.sawant@capgemini.com]	

--EXEC UpdateValidFromDateForCareHome <SCA USER NAME FOR APPLICATION>, <4 DIGIT PIN>
--EXEC AssignPinToUser 'siddhesh','5678'
--***********************************************************************************************

CREATE PROCEDURE [dbo].[AssignPinToUser]  
 @Input_UserName varchar(30), @Input_Pin int
AS
BEGIN
BEGIN TRY 

BEGIN TRAN --OPEN TRANSACTION
DECLARE @UserID uniqueidentifier

SET @UserID = (SELECT UserId FROM Users (NOLOCK) WHERE UserName =  @Input_UserName)


UPDATE Users 
		SET AuthorizationPIN = @Input_Pin WHERE UserId = @UserID
					
COMMIT TRAN  -- COMMIT TRANSACTION
END TRY
BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE ASSIGNING CUSTOMER TO USER'

END CATCH
END

