﻿


CREATE PROCEDURE [dbo].[XSCAResetUser] 
as
BEGIN
declare
 @password nvarchar(max),
 @userid uniqueidentifier
set @password = 'd5ST8JurCwKMvffbRsRE+KjcppNrSSY6gF+jci1pQFQ=';
set @userid = (select userid from users where username = 'ScaAdmin');

Set nocount off

declare @userrestet table
(
userid uniqueidentifier,
useraction nvarchar(100),
username nvarchar(100),
PersonalInformationId bigint
)

insert @userrestet 
select 
	usr.userid,
	per.FirstName,
	per.LastName,
	per.PersonalInformationId
 from ContactPerson CP
inner join PersonalInformation per on per.PersonalInformationId = cp.PersonalInformationId
inner join Users usr on usr.UserName = per.LastName
where cp.CustomerId is not null
  and per.FirstName in ('Unlock','Reset')

update usrunlock
set usrunlock.IsAccountLocked	= 0,
	usrunlock.NoOfAttempts		= 0,
	usrunlock.ModifiedBy		= @userid,
	usrunlock.ModifiedDate		= Getdate()
from users usrunlock
inner join @userrestet res on res.userid = usrunlock.UserId
where useraction = 'Unlock'

update usrreset
set usrreset.IsAccountLocked	= 0,
	usrreset.NoOfAttempts		= 0,
	usrreset.IsNewUser			= 1,
	usrreset.Password			= @password,
	usrreset.ModifiedBy			= @userid,
	usrreset.ModifiedDate		= Getdate()
from users usrreset
inner join @userrestet res on res.userid = usrreset.UserId
where useraction = 'Reset'

Insert into PasswordHistories (userid,password,ModifiedBy,ModifiedDate)
select
 userid,
 @password,
 @userid,
 getdate()
from @userrestet
where useraction = 'Reset'

Update per
set lastname = null
from PersonalInformation per
inner join @userrestet usr on usr.PersonalInformationId = per.PersonalInformationId


End