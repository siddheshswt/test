﻿
--***********************************************************************************************
--Store Procedure Name	:	AssignCustomerToUser
--Created Date			:	19 Mar 2015
--Purpose				:	To assign customers to user
--Created By			:	Siddhesh Sawant [siddhesh.a.sawant@capgemini.com]	

--EXEC UpdateValidFromDateForCareHome <SAP CUSTOMER NUMBER WITH PRECEEDING ZERO>, <SCA USER NAME FOR APPLICATION>
--EXEC AssignCustomerToUser '0000015441', 'Siddhesh'
--***********************************************************************************************
CREATE PROCEDURE [dbo].[AssignCustomerToUser]  
@Input_SAPCustomerNumber varchar(20), @Input_UserName varchar(30)
AS
BEGIN
BEGIN TRY 

BEGIN TRAN --OPEN TRANSACTION
DECLARE @UserID uniqueidentifier, @CustomerID bigint

SET @UserID = (SELECT UserId FROM Users (NOLOCK) WHERE UserName =  @Input_UserName)
SET @CustomerID = (SELECT CustomerId from Customer (NOLOCK) WHERE SAPCustomerNumber = @Input_SAPCustomerNumber)

IF(NOT EXISTS(SELECT UserId FROM IDXUserCustomer (NOLOCK) WHERE UserId = @UserID AND CustomerId  = @CustomerID))
BEGIN
	INSERT INTO IDXUserCustomer (UserId, CustomerId,ModifiedBy,ModifiedDate)
			VALUES (@UserID,@CustomerID,'abe3ff99-cb70-48a6-bcc1-4d4c9b80a153',GETDATE())
END
		
			
COMMIT TRAN  -- COMMIT TRANSACTION
END TRY
BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE ASSIGNING CUSTOMER TO USER'

END CATCH
END

