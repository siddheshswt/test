﻿

-- =============================================
-- Author:		Arvind Nishad
-- Create date: 24-03-2015
-- Description:	Update Patient Status IsSentToSAP =True once patient is sent to sap after modification
-- =============================================
--exec UpdateSAPSentPatientStatus @PatientId ='1,2' , @UserId= 'ECDBCD8F-145A-411A-9FBD-98CC5D2F0786'
CREATE PROCEDURE [dbo].[UpdatePatientSentToSAPStatus] (
	@FrontendId VARCHAR(max)
	,-- It will be a comma separated Patient Id
	@RecordType VARCHAR(50)
	,@UserId VARCHAR(100)
	)
AS
BEGIN
	DECLARE @Result INT = 0
	DECLARE @ResultMessage VARCHAR(200)
	DECLARE @CurrentDate DATETIME = getdate()

	BEGIN TRY
		BEGIN TRANSACTION;

		-- If IsSentToSAP=True then don't send patient to SAP
		-- If IsSentToSAP=false then send patient to SAP
		IF @RecordType = 'PA'
		BEGIN
			UPDATE Patient
			SET IsSentToSAP = 1
				,ModifiedDate = @CurrentDate
			WHERE PatientId IN (
					SELECT val
					FROM dbo.Split(@FrontendId, ',')
					)

			SELECT @Result = 1
				,@ResultMessage = 'Patient ''IsSentToSAP'' Status updated'
		END
		ELSE
		BEGIN
			UPDATE CareHome
			SET IsSentToSAP = 1
				,ModifiedDate = @CurrentDate
			WHERE CareHomeId IN (
					SELECT val
					FROM dbo.Split(@FrontendId, ',')
					)


					SELECT @Result = 1
			,@ResultMessage = 'Carehome ''IsSentToSAP'' Status updated'
		END

		

		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		-- Insert exception in exception log		
		EXEC SaveExceptionLog @UserID = @UserId
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage
	END CATCH;

	SELECT @Result Result
		,@ResultMessage ResultMessage
END
