﻿

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------






/******************************

** File:    GetPatientDetails_usp

** Name:	GetPatientDetails_usp

** Desc:	Get Patient Details

** Auth:	Arvind

** Date:	20/02/2015



********************************

** Change History

********************************

** PR	Date			Author		Description	

** --	--------		-------		-----------------------------------

**	1	07-May-2015		Arvind		Added Correspondence Address column

**	2	12-May-2015		Arvind		Added CommunicationPreference and MarketingPreference column

**	3	13-May-2015		Arvind		Added IsDataProtected column

**	4	15-May-2015		Arvind		Change the select logic for CorrespondenceCareHomeName column name

**	5	15/06/2015		Sagar		Added Document Comment	

**  6   26/06/2015      Siddharth	Changed join for Communication Format from PreferenceTypeId To PatientId 	

**  6   07/12/2015      Siddhesh 	Added RemovedStoppedDateTime column in select statement

**  7   22/07/2016      Prafull 	Added Title column in select statement

*******************************/



--==============================================

--exec GetPatientDetails_usp @PatientId=116897 



CREATE PROCEDURE [dbo].[GetPatientDetails_usp] (

	--

	@PatientId bigint

	)

AS

BEGIN



SELECT	patient.PatientId PatientId

		,patient.SAPPatientNumber SAPPatientNumber

		,patient.DeliveryDate DeliveryDate

		,patient.NextDeliveryDate NextDeliveryDate

		,patient.PatientType PatientType

		,patient.PatientStatus PatientStatus

		,patient.ReasonCodeId

		,patient.CommunicationFormat CommunicationFormat		

		--CAST(10033 as bigint) CommunicationFormat,

		,patient.AssessmentDate AssessmentDate

		,patient.NextAssessmentDate NextAssessmentDate

		,ISNULL(patient.RoundId,'') RoundId

		,patient.DateofBirth DateofBirth

		,convert(INT, DATEDIFF(d, patient.DateofBirth, getdate()) / 365.25) Age

		,patient.NHSId NHSId

		,patient.LocalId LocalId

		,patient.DeliveryFrequency DeliveryFrequency

		,patient.CareHomeId CareHomeId

		,patient.ADP1

		,patient.ADP2

		--,'CareHomeName' CareHomeName

		,pInfoCareHome.FirstName AS CareHomeName

		,addr.HouseNumber HouseNumber

		,addr.HouseName HouseName

		,addr.AddressLine1 AddressLine1

		,addr.AddressLine2 AddressLine2

		,addr.City City

		,addr.PostCode PostCode

		,addr.County County

		,addr.Country Country

		,pInfo.TitleId TitleId

		,pInfo.FirstName FirstName

		,pInfo.LastName LastName

		,pInfo.Gender Gender

		,pinfo.TelephoneNumber TelephoneNumber

		,pinfo.MobileNumber MobileNumber

		,pinfo.Email Email		

		--pInfo.CreatedById CreatedBy,

		,CreatedUser.UserName as CreatedByName

		,pInfo.CreatedDateTime CreatedDate

		, cs.SAPCustomerNumber

		,CHome.NextDeliveryDate CareHomeNextDeliveryDate

		,pInfoCustomer.FirstName as CustomerFirstName

		,pInfoCustomer.LastName as CustomerLastName

		,cs.CustomerId as CustomerID

		,isnull(patient.Round,'') as PatientRound

		,CHome.CareHomeStatus CareHomeStatus

		,ISNULL(CHome.SAPCareHomeNumber ,'') AS CareHomeSAPNumber

		--Set Correspondence Address 		 

		,case when patient.CareHomeId>0 and pInfo.AddressId=pInfo.CorrespondenceAddressId then pInfoCareHome.FirstName 

		else CorrespondenceAddress.HouseName end as CorrespondenceCareHomeName

		,CorrespondenceAddress.AddressLine1 CorrespondenceAddress1

		,CorrespondenceAddress.AddressLine2 CorrespondenceAddress2

		,CorrespondenceAddress.City CorrespondenceTownOrCity

		,CorrespondenceAddress.PostCode CorrespondencePostCode

		,CorrespondenceAddress.County CorrespondenceCounty

		,CorrespondenceAddress.Country CorrespondenceCountry

		,case when pInfo.AddressId=pInfo.CorrespondenceAddressId then cast (1 as bit) else cast (0 as bit) end as CorrespondenceSameAsDelivery		

		--PreferenceType=20145 Communication Preference

		--PreferenceType=20146 Marketing Preference

		,(SELECT STUFF((SELECT ','+cast(idxPreference.Preference as varchar(50)) FROM [IDXPreference] idxPreference 

			WHERE PreferenceType=20145 and  idxPreference.PatientId = @PatientId FOR XML PATH('')), 1, 1, '')) as CommunicationPreferences

		,(SELECT STUFF((SELECT ','+cast(idxPreference.Preference as varchar(50)) FROM [IDXPreference] idxPreference 

		    WHERE PreferenceType=20146 and  idxPreference.PatientId = @PatientId FOR XML PATH('')), 1, 1, '')) as MarketingPreferences

		,patient.IsDataProtected

		,CASE WHEN patient.RemovedStoppedDateTime = NULL THEN '' ELSE patient.RemovedStoppedDateTime END AS RemovedStoppedDateTime

		,lst.DefaultText Title

	FROM Patient patient WITH (NOLOCK)

	LEFT OUTER JOIN PersonalInformation pInfo (NOLOCK) ON patient.PersonalInformationId = pInfo.PersonalInformationId	

	LEFT OUTER JOIN CareHome CHome (NOLOCK) ON CHome.CareHomeId = patient.CareHomeId

	LEFT OUTER JOIN PersonalInformation pInfoCareHome (NOLOCK) ON CHome.PersonalInformationId = pInfoCareHome.PersonalInformationId	

	LEFT OUTER JOIN Address addr (NOLOCK) ON addr.AddressId = pInfo.AddressId

	LEFT OUTER JOIN Address CorrespondenceAddress (NOLOCK) ON CorrespondenceAddress.AddressId=pInfo.CorrespondenceAddressId

	LEFT OUTER JOIN Users users (NOLOCK) ON users.UserId = patient.ModifiedBy

	LEFT OUTER JOIN ContactPerson Contact (NOLOCK) ON Contact.ContactPersonId = users.PersonId

	--LEFT OUTER JOIN PersonalInformation pInfo2 ON pInfo2.PersonalInformationId = Contact.PersonalInformationId

	LEFT OUTER JOIN Users as CreatedUser (NOLOCK) on CreatedUser.UserId = pInfo.CreatedById

	Left outer join Customer AS cs (NOLOCK) on cs.CustomerId = patient.CustomerId

	LEFT OUTER JOIN PersonalInformation pInfoCustomer (NOLOCK) ON cs.PersonalInformationId = pInfoCustomer.PersonalInformationId

	LEFT OUTER JOIN List lst (NOLOCK) ON lst.ListId = pInfo.TitleId

	

	WHERE patient.PatientId = @PatientId



END




