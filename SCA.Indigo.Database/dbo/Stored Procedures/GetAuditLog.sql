﻿-- =====================================================================
-- Author:		Sagar Yerva
-- Create date: 08-05-2015
-- Description:	Gets Audit Log Report for Patient/Customer/CareHome
-- ======================================================================
 -- ===============================================================================
 --		Modified Date		Modified By			Purpose
--	1.	11-May-2015			Sagar Yerva			Removed UserId hard-coding
--  2.  12-May-2015			Siddhesh Sawant		Added logic for custom paging
--  3.	18-May-2015			Sagar Yerva			Logic changed for gettitng total records in paging
--  4.  19-May-2015			Sagar Yerva			Date parameter added
--	5.	20-may-2015			Sagar Yerva			Audit Log for User Implemented
--	6.	02-June-2015		Sagar Yerva			Date Format changed for old value & new value
--	7.	21-Jul-2016		    Saurabh Mayekar	    Added condition for @PatientId for @AuditLogTypeId = 1
--  8.  02-Aug-2016         Saurabh Mayekar     Added Case for PatientId for @AuditLogTypeId = 1
--  9.  07-Sep-2016         Saurabh Mayekar     Increase size of OldValue & NewValue to MAX for #FinalData temp table
 -- ===============================================================================
 --exec GetAuditLog 1,100,  1,  9234,11,0,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','2016-03-06 19:41:00.000','2016-09-06 12:00:00.000','Patient',1,'00000000-0000-0000-0000-000000000000'
 --exec GetAuditLog 1,10,   2,  0,9,5,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','2015-05-05 19:41:00.000','2015-05-19 12:00:00.000','carehome',0,'00000000-0000-0000-0000-000000000000'
 --exec GetAuditLog 1,100,  3,  0,11,0,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','2014-01-20 19:41:00.000','2015-06-10 12:00:00.000','customer',1,'00000000-0000-0000-0000-000000000000'
 --exec GetAuditLog 1,100,  5,  0,10,0,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','2015-06-01 10:41:00.000','2015-06-08 23:40:00.000','users',1,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'

CREATE PROCEDURE [dbo].[GetAuditLog] -- Add the parameters for the stored procedure here
		@pageNum bigint = 0,
		@recordCount bigint = 0,
		@AuditLogTypeId bigint =NULL,
		@PatientId bigint=NULL,
		@CustomerId bigint=NULL,
		@CareHomeId bigint=NULL,
		@UserId uniqueidentifier=NULL,
		@FromDate datetime=NULL,
		@ToDate datetime=NULL,
		@TableName varchar(MAX)=NULL,
		@IsExport bit=1,
		@SearchedUserId uniqueidentifier = NULL 
			
AS BEGIN 

			
-- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.

SET NOCOUNT ON;
SET FMTONLY OFF; --Uncomment this, whenever input or output columns are changed and require EF Update

 -- Insert statements for procedure here
 DECLARE @start bigint = 0,
         @end bigint = 0

SET @start = ((@recordCount * @pageNum) - @recordCount) + 1
SET @end = (@start-1) + @recordCount 

DECLARE @TotalRecords BIGINT 
DECLARE @tmpFromDate date;
DECLARE @tmpToDate date;


SET @tmpFromDate = CONVERT(date,@FromDate);

SET @tmpToDate = CONVERT (date,@ToDate);

 print @tmpFromDate;


CREATE TABLE #FinalData
	(
		TotalRecords bigint ,
		RowNumber bigint ,
		PrimaryKey bigint ,
		AuditLogId bigint not null,
		AuditTransId bigint not null,
		TableName varchar(100) ,
		ColumnName nvarchar(100) ,
		[Action] varchar(100) ,
		OldValue varchar(MAX) ,
		NewValue varchar(MAX) ,
		ModifiedBy uniqueidentifier not null,
		UserName varchar(100) ,
		ModifiedDate datetime not null,
		Name varchar(100),
		Remarks varchar(max),
		
	);

DECLARE @AuditTransactions table(AuditTransId bigint) 
IF @AuditLogTypeId = 1 BEGIN

INSERT INTO @AuditTransactions
SELECT DISTINCT AuditTransId
FROM AuditLog (NOLOCK)
WHERE TableName = @TableName
AND PrimaryKey = @PatientId;

 WITH RESULT AS
  ( SELECT ROW_NUMBER() OVER(
                             ORDER BY al.AuditLogId DESC) AS RowNumber,
           al.PrimaryKey,
           AuditLogId,
           AuditTransId,
           TableName,
           ColumnName,
           Action,
           CASE
               WHEN ISDATE (OldValue) = 1 THEN CONVERT(nvarchar,
                   CONVERT(DATETIME,
                     OldValue),103)
               ELSE dbo.GlobalizedValueText(ColumnName, 1, OldValue)
           END AS OldValue,
           CASE
               WHEN ISDATE (NewValue) = 1 THEN CONVERT(nvarchar,
                                                       CONVERT(DATETIME,
                                                               NewValue),103)
               ELSE dbo.GlobalizedValueText(ColumnName, 1, NewValue)
           END AS NewValue,
           al.ModifiedBy,
           users.UserName,
           al.ModifiedDate,
           personalinformation.FirstName + ' ' + ISNULL(personalinformation.LastName,'') AS Name
		   ,ISNULL(al.Remarks,'') AS Remarks
   FROM AuditLog al (NOLOCK)
   INNER JOIN Patient patient (NOLOCK) ON patient.PatientId = @PatientId
   INNER JOIN Users users (NOLOCK) ON users.UserId = al.ModifiedBy
   INNER JOIN PersonalInformation personalinformation (NOLOCK) ON personalinformation.PersonalInformationId = patient.PersonalInformationId
   WHERE AuditTransId IN
       (SELECT AuditTransId
        FROM @AuditTransactions)
	 AND CASE WHEN al.TableName = 'Patient' THEN al.PrimaryKey ELSE @PatientId END = @PatientId
	 AND al.ColumnName not in ('ModifiedBy','ModifiedDate')
     AND Convert(date,al.ModifiedDate) >= @tmpFromDate
     AND Convert(date,al.ModifiedDate) <= @tmpToDate)	 
INSERT INTO #FinalData(TotalRecords,RowNumber,PrimaryKey,AuditLogId,AuditTransId,TableName,ColumnName,[Action],OldValue,NewValue,ModifiedBy,UserName,ModifiedDate,Name,Remarks)

SELECT
  (SELECT COUNT(*)
   FROM RESULT) AS TotalRecords,
       r.*
FROM RESULT r
--IF(@IsExport = 1) BEGIN
--SELECT *
--FROM #FinalData
-- END
--  ELSE
--   BEGIN
--SELECT *
--FROM #FinalData where RowNumber BETWEEN @start and @end
-- END
  END

IF @AuditLogTypeId = 2 BEGIN
INSERT INTO @AuditTransactions
SELECT DISTINCT AuditTransId
FROM AuditLog (NOLOCK)
WHERE TableName = @TableName
  AND PrimaryKey = @CareHomeId;

 WITH RESULT AS
  (SELECT ROW_NUMBER() OVER(
                            ORDER BY al.AuditLogId DESC) AS RowNumber,
          al.PrimaryKey,
          AuditLogId,
          AuditTransId,
          TableName,
          ColumnName,		   
          Action,
          CASE
              WHEN ISDATE (OldValue) = 1 THEN CONVERT(nvarchar,
                                                      CONVERT(DATETIME,
                                                              OldValue),103)
              ELSE dbo.GlobalizedValueText(ColumnName, 1, OldValue)
          END AS OldValue,
          CASE
              WHEN ISDATE (NewValue) = 1 THEN CONVERT(nvarchar,
                                                      CONVERT(DATETIME,
                                                              NewValue),103)
              ELSE dbo.GlobalizedValueText(ColumnName, 1, NewValue)
          END AS NewValue,
          al.ModifiedBy,
          users.UserName,
          al.ModifiedDate,
          personalinformation.FirstName + ' ' + ISNULL(personalinformation.LastName,'') AS Name
		  ,ISNULL(al.Remarks,'') AS Remarks
   FROM AuditLog al (NOLOCK)
   INNER JOIN CareHome carehome (NOLOCK) ON carehome.CareHomeId =@CareHomeId
   INNER JOIN Users users (NOLOCK) ON users.UserId = al.ModifiedBy
   INNER JOIN PersonalInformation personalinformation (NOLOCK) ON personalinformation.PersonalInformationId = carehome.PersonalInformationId
   WHERE AuditTransId IN
       (SELECT AuditTransId
        FROM @AuditTransactions)
	AND al.ColumnName not in ('ModifiedBy','ModifiedDate')
     AND Convert(date,al.ModifiedDate) >= @tmpFromDate
     AND Convert(date,al.ModifiedDate) <= @tmpToDate)
INSERT INTO #FinalData(TotalRecords,RowNumber,PrimaryKey,AuditLogId,AuditTransId,TableName,ColumnName,[Action],OldValue,NewValue,ModifiedBy,UserName,ModifiedDate,Name,Remarks)

SELECT
  (SELECT COUNT(*)
   FROM RESULT) AS TotalRecords,
 r.*
FROM RESULT r
-- IF(@IsExport = 1) BEGIN
--SELECT *
--FROM #FinalData
-- END ELSE BEGIN
--SELECT *
--FROM #FinalData where RowNumber BETWEEN @start and @end
-- END 
 END

IF @AuditLogTypeId = 3 BEGIN
INSERT INTO @AuditTransactions
SELECT DISTINCT AuditTransId
FROM AuditLog (NOLOCK)
WHERE TableName = @TableName
  AND PrimaryKey = @CustomerId;

 WITH RESULT AS
  (SELECT ROW_NUMBER() OVER(
                            ORDER BY al.AuditLogId DESC) AS RowNumber,
          al.PrimaryKey,
          AuditLogId,
          AuditTransId,
          TableName,
          ColumnName,		   		   
          Action,
          CASE
              WHEN ISDATE (OldValue) = 1 THEN CONVERT(nvarchar,
                                                      CONVERT(DATETIME,
                                                              OldValue),103)
              ELSE dbo.GlobalizedValueText(ColumnName, 1, OldValue)
          END AS OldValue,
          CASE
              WHEN ISDATE (NewValue) = 1 THEN CONVERT(nvarchar,
                                                      CONVERT(DATETIME,
                                                              NewValue),103)
              ELSE dbo.GlobalizedValueText(ColumnName, 1, NewValue)
          END AS NewValue,
          al.ModifiedBy,
          users.UserName,
          al.ModifiedDate,
          personalinformation.FirstName + ' ' + ISNULL(personalinformation.LastName,'') AS Name
		  ,ISNULL(al.Remarks,'') AS Remarks
   FROM AuditLog al (NOLOCK)
   INNER JOIN Customer customer (NOLOCK) ON customer.CustomerId = @CustomerId
   INNER JOIN Users users (NOLOCK) ON users.UserId = al.ModifiedBy
   INNER JOIN PersonalInformation personalinformation (NOLOCK) ON personalinformation.PersonalInformationId = customer.PersonalInformationId
   WHERE AuditTransId IN
       (SELECT AuditTransId
        FROM @AuditTransactions)
	AND al.ColumnName not in ('ModifiedBy','ModifiedDate')
     AND Convert(date,al.ModifiedDate) >= @tmpFromDate
     AND Convert(date,al.ModifiedDate) <= @tmpToDate )
INSERT INTO #FinalData(TotalRecords,RowNumber,PrimaryKey,AuditLogId,AuditTransId,TableName,ColumnName,[Action],OldValue,NewValue,ModifiedBy,UserName,ModifiedDate,Name,Remarks)

SELECT
  (SELECT COUNT(*)
   FROM RESULT) AS TotalRecords,
       r.*
FROM RESULT r
  END

IF @AuditLogTypeId = 5 BEGIN
INSERT INTO @AuditTransactions
SELECT DISTINCT AuditTransId
FROM AuditLog (NOLOCK)
WHERE ModifiedBy = @SearchedUserId;

 WITH RESULT AS
  (SELECT ROW_NUMBER() OVER(
                            ORDER BY al.ModifiedDate DESC) AS RowNumber,
          al.PrimaryKey,
          AuditLogId,
          AuditTransId,
          TableName,
          ColumnName,		   
          Action,
          CASE
              WHEN ISDATE (OldValue) = 1 THEN CONVERT(nvarchar,
                                                      CONVERT(DATETIME,
                                                              OldValue),103)
              ELSE dbo.GlobalizedValueText(ColumnName, 1, OldValue)
          END AS OldValue,
          CASE
              WHEN ISDATE (NewValue) = 1 THEN CONVERT(nvarchar,
                                                      CONVERT(DATETIME,
                                                              NewValue),103)
              ELSE dbo.GlobalizedValueText(ColumnName, 1, NewValue)
          END AS NewValue,
          al.ModifiedBy,
          users.UserName,
          al.ModifiedDate,
          users.UserName AS Name
		  ,ISNULL(al.Remarks,'') AS Remarks
   FROM AuditLog al (NOLOCK)
   INNER JOIN Users users (NOLOCK) ON users.UserId = al.ModifiedBy
   INNER JOIN PersonalInformation personalinformation (NOLOCK) ON personalinformation.PersonalInformationId = users.PersonId
   WHERE AuditTransId IN
       (SELECT AuditTransId
        FROM @AuditTransactions)
	 AND al.ColumnName not in ('ModifiedBy','ModifiedDate')
     AND Convert(date,al.ModifiedDate) >= @tmpFromDate
   AND Convert(date,al.ModifiedDate) <= @tmpToDate )
INSERT INTO #FinalData(TotalRecords,RowNumber,PrimaryKey,AuditLogId,AuditTransId,TableName,ColumnName,[Action],OldValue,NewValue,ModifiedBy,UserName,ModifiedDate,Name,Remarks)

SELECT
  (SELECT COUNT(*)
   FROM RESULT) AS TotalRecords,
       r.*
FROM RESULT r 
	END 
	IF(@IsExport = 1) BEGIN
SELECT	f.TotalRecords
		,f.RowNumber
		,f.PrimaryKey
		,f.AuditLogId
		,f.AuditTransId
		,f.TableName
		,f.ColumnName
		,f.[Action]
		,f.OldValue
		,f.NewValue
		,f.ModifiedBy
		,f.UserName
		,f.ModifiedDate
		,f.Name
		,f.Remarks
FROM #FinalData f
 END ELSE BEGIN
SELECT	f.TotalRecords
		,f.RowNumber
		,f.PrimaryKey
		,f.AuditLogId
		,f.AuditTransId
		,f.TableName
		,f.ColumnName
		,f.[Action]
		,f.OldValue
		,f.NewValue
		,f.ModifiedBy
		,f.UserName
		,f.ModifiedDate
		,f.Name
		,f.Remarks
FROM	#FinalData f
where	RowNumber BETWEEN @start and @end
		END 	
 END