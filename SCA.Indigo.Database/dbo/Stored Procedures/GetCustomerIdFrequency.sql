﻿

-- =============================================
-- Author:		Arvind Nishad
-- Create date: 11-03-2015
-- Description:	Get CustomerId based on OrderId
-- =============================================

-- ===============================================================================

--	Modified Date			Modified By			Purpose
--  1.	 19-Mar-2015			Sagar Yerva		Added NOLOCK
--  2.   08-Apr-2015			Siddhesh Sawant		Take date from DerivedNDD column
--  3.   09-Apr-2015			Siddhesh Sawant		Added carehomeid and carehomefrequncy columns
--  3.   10-Apr-2015			Siddhesh Sawant		Added Round and RoundId columns
--  4.   16-Apr-2015			Siddhesh Sawant		Added postcode colums
--  5.   05-May-2015			Siddhesh Sawant		Added patientId column
--  6.   25-Nov-2015			Siddhesh Sawant		Added OrderType column
--  7.   28-Apr-2016			Siddhesh Sawant		Added OrderCreationType column
-- ===============================================================================
 -- exec GetCustomerIdFrequency 30804

CREATE PROCEDURE [dbo].[GetCustomerIdFrequency] 
(
@OrderId bigint
)
as begin
    DECLARE @PatientId bigint
	DECLARE @CustomerId varchar(50)=''
	DECLARE @PatientFrequency int=0
	DECLARE @SAPDeliveryDate datetime	
	DECLARE @CarehomeId bigint
	DECLARE @CarehomeFrequency bigint 
	DECLARE @Round varchar(50)
	DECLARE @RoundId varchar(50)
	DECLARE @Postcode varchar(50)
	DECLARE @PatientRound varchar(50)
	DECLARE @PatientRoundId varchar(50)
	DECLARE @CarehomeRound varchar(50)
	DECLARE @CarehomeRoundId varchar(50)
	DECLARE @PatientPostcode varchar(50)
	DECLARE @CarehomePostcode varchar(50)
	DECLARE @OrderType BIGINT = 0
	DECLARE @OrderCreationType BIGINT = 0	

		SELECT
		@PatientId=patient.PatientId
		,@CustomerId=patient.CustomerId
		,@PatientFrequency=patient.DeliveryFrequency
		,@SAPDeliveryDate=orders.DerivedNDD
		,@CarehomeId = orders.carehomeid
		,@CarehomeFrequency =  (select DeliveryFrequency from CareHome (nolock) where CareHomeId =  orders.carehomeid)
		,@PatientRound = patient.Round
		,@PatientRoundId = patient.RoundId
		,@CarehomeRound = (select Round from CareHome (nolock) where CareHomeId =  orders.carehomeid)
		,@CarehomeRoundId = (select RoundId from CareHome (nolock) where CareHomeId =  orders.carehomeid)
		,@PatientPostcode = (select adr.postcode from Address adr (nolock) where adr.AddressId = (select pif.AddressId from PersonalInformation pif (nolock)
							where pif.PersonalInformationId =(select pt.PersonalInformationId from Patient pt (nolock) where pt.PatientId = patient.PatientId)))
		,@CarehomePostcode = (select adr.postcode from Address adr (nolock) where adr.AddressId = (select pif.AddressId from PersonalInformation pif (nolock)
							where pif.PersonalInformationId =(select ch.PersonalInformationId from CareHome ch (nolock) where ch.CareHomeId = orders.carehomeid)))
		,@OrderType= orders.OrderType
		,@OrderCreationType = orders.OrderCreationType		
		--@SAPDeliveryDate=orders.SAPOrderDeliveryDate 		
		FROM Patient patient (NOLOCK) INNER JOIN Orders orders (NOLOCK) ON patient.PatientId=orders.PatientId
		WHERE orders.OrderId= @OrderId

		IF(@CarehomeId is not null)
		BEGIN
			SET @Round = @CarehomeRound
			SET @RoundId = @CarehomeRoundId
			SET @Postcode = @CarehomePostcode
		END
		ELSE
		BEGIN
			SET @Round = @PatientRound
			SET @RoundId = @PatientRoundId
			SET @Postcode = @PatientPostcode
		END

		SELECT 
				@PatientId  PatientId
				,@CustomerId CustomerId
				,@PatientFrequency PatientFrequency
				,@SAPDeliveryDate SAPDeliveryDate
				,@CarehomeId CarehomeId
				,@CarehomeFrequency CarehomeFrequency
				,@Round [Round]
				,@RoundId RoundId
				,@Postcode Postcode
				,@OrderType OrderType
				,@OrderCreationType OrderCreationType				

end

