﻿
/******************************
** File:    AuditLogCriteriaSearch
** Name:	AuditLogCriteriaSearch
** Desc:	Search for Customer/CareHome/Patient/User
** Auth:	Sagar
** Date:	13/05/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 	1	13/05/2015		Sagar 		Implemented search for Customer
**  2	14/05/2015		Sagar 		Implemented search for CareHome & Patient												
**	3	20/05/2015		Sagar 		Implemented search for Users
**	4	15/06/2015		Sagar		Added Document comment	
*******************************/

-- ==========================================================================================
--exec   [dbo].[AuditLogCriteriaSearch] 'n','Customer','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','0' -- for Customer
--exec   [dbo].[AuditLogCriteriaSearch] 'c','CareHome','8EFB92BA-8D37-4197-922A-F00B734DD771','11' -- for CareHome
--exec   [dbo].[AuditLogCriteriaSearch] 'tas','patient','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','10' -- for patient
--exec   [dbo].[AuditLogCriteriaSearch] 'sca','users','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','0' -- for users
--exec   [dbo].[AuditLogCriteriaSearch] 'tena','product','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','9' -- for product

CREATE PROCEDURE [dbo].[AuditLogCriteriaSearch]

-- Add the parameters for the stored procedure here
				@SearchText varchar(MAX),
				@SearchTable varchar(MAX),
				@UserId uniqueidentifier,
				@CustomerId bigint
				AS BEGIN

 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.

SET NOCOUNT ON;

declare
				@Role varchar(max),
				@RoleType varchar(max);

select  @Role = role.RoleName, @RoleType = role.UserType from IDXUserRole idxUR
inner join Role role on role.RoleId = idxUR.RoleID
 where idxUR.UserID = @UserId

 -- Insert statements for procedure here
 IF @SearchTable = 'Customer' BEGIN
SELECT DISTINCT cust.CustomerId AS Id
				, ISNULL (cust.SAPCustomerNumber,'') AS SAPId
                ,personal.FirstName AS Name
                ,personal.FirstName + ' (' + ISNULL(cust.SAPCustomerNumber,'') + ')' AS SearchResult
				,idxuser.UserId AS UserId
FROM Customer cust
INNER JOIN PersonalInformation personal ON personal.PersonalInformationId = cust.PersonalInformationId
LEFT OUTER JOIN IDXUserCustomer idxuser ON (idxuser.CustomerId = cust.CustomerId and idxuser.UserId = @UserId)
WHERE 
(cust.SAPCustomerNumber LIKE '%' + @SearchText + '%'
       OR personal.FirstName LIKE '%' + @SearchText + '%')
AND
( CASE		
                                                WHEN  @Role = 'Administrator' and @RoleType = 'SCA' THEN 1
                                                WHEN idxuser.UserId = @UserId THEN 1		
                                                ELSE 0		
                                END = 1)
	   END

 IF @SearchTable = 'Patient' BEGIN
  SELECT DISTINCT p.PatientId AS Id,
                  ISNULL(p.SAPPatientNumber,'') AS SAPId,
                  personal.FirstName + ' ' + ISNULL(personal.LastName ,'') AS Name,
                  personal.FirstName + ' ' + ISNULL(personal.LastName ,'') + ' (' + ISNULL(p.SAPPatientNumber,'') + ')' AS SearchResult,
				  idxuser.UserId AS UserId
  FROM Patient  p
  INNER JOIN IDXUserCustomer idxuser ON idxuser.UserId = @UserId
  inner join Customer cust on cust.CustomerId = p.CustomerId
  INNER JOIN PersonalInformation personal ON personal.PersonalInformationId = p.PersonalInformationId WHERE idxuser.UserId = @UserId
  AND (p.SAPPatientNumber LIKE '%' + @SearchText + '%'
       OR personal.FirstName LIKE '%' + @SearchText + '%' OR personal.LastName like '%' + @SearchText + '%') AND p.CustomerId = @CustomerId 
	   --AND p.CustomerId IN (idxuser.CustomerId)	  
 END

IF @SearchTable = 'CareHome' BEGIN
  SELECT DISTINCT care.CareHomeId AS Id,
                  ISNULL (care.SAPCareHomeNumber,'') AS SAPId,
                  personal.FirstName AS Name,
                  personal.FirstName + ' (' + ISNULL(care.SAPCareHomeNumber,'') + ')' AS SearchResult,
				  idxuser.UserId AS UserId
  FROM CareHome care
  INNER JOIN IDXUserCustomer idxuser ON idxuser.UserId = @UserId
  INNER JOIN PersonalInformation personal ON personal.PersonalInformationId = care.PersonalInformationId WHERE idxuser.UserId = @UserId
  AND (care.SAPCareHomeNumber LIKE '%' + @SearchText + '%'
       OR personal.FirstName LIKE '%' + @SearchText + '%')AND care.CustomerId = @CustomerId 
	   --and  care.CustomerId in (idxuser.CustomerId)
END

IF @SearchTable = 'Users' BEGIN
  SELECT		 CAST (0 AS bigint) AS Id
				,'' AS SAPId				
				,u.UserName AS Name
				,u.UserName as SearchResult
				,u.UserId AS UserId
	FROM Users u WHERE u.UserName LIKE '%'+ @SearchText +  '%' 
END

IF @SearchTable = 'product' BEGIN
  SELECT DISTINCT idxprod.ProductId AS Id,
                  ISNULL(prod.SAPProductID,'') AS SAPId,
                  prod.DescriptionUI AS Name,
                  prod.DescriptionUI AS SearchResult,
				  @UserId AS UserId
  FROM IDXCustomerProduct  idxprod
  INNER JOIN Product prod ON prod.ProductId = idxprod.ProductId  
  AND (prod.DescriptionUI LIKE '%' + @SearchText + '%'
       OR prod.SAPProductID LIKE '%' + @SearchText + '%' ) and idxprod.CustomerId = @CustomerId  
 END

 END


