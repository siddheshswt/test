﻿

/******************************
** File:    GetMenuDetails
** Name:	GetMenuDetails
** Desc:	Gets Menu and submenu list with isDisplay flag
** Auth:	
** Date:	

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
*******************************/


CREATE PROCEDURE [dbo].[GetMenuDetails]
	-- Add the parameters for the stored procedure here
	@LanguageId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT 
MAINMENU.MenuId,
ISNULL(SUBMENU.MenuId,MAINMENU.MenuId)  SubMenuID,
       CASE WHEN trlMainMenu.TranslationType IS NULL
              THEN 
                           trpMainMenu.[Text]
              ELSE 
                           trlMainMenu.TranslationType
              END MenuName,
       CASE WHEN trlSubMenu.TranslationType IS NULL
              THEN
                     trpSubMenu.[Text]
              ELSE
                     trlSubMenu.TranslationType
              END SubMenuName
FROM Menu MAINMENU WITH (NOLOCK)
LEFT JOIN MENU SUBMENU WITH (NOLOCK)
ON  MAINMENU.MenuId=SUBMENU.ParentMenuID 
LEFT JOIN TranslationProxy trpMainMenu ON MAINMENU.TranslationProxyId = trpMainMenu.TransProxyID
LEFT JOIN TranslationProxy trpSubMenu ON SUBMENU.TranslationProxyId = trpSubMenu.TransProxyID
LEFT JOIN Translation trlMainMenu ON trpMainMenu.TransProxyID = trlMainMenu.TransProxyID
LEFT JOIN Translation trlSubMenu ON trpSubMenu.TransProxyID = trlSubMenu.TransProxyID
WHERE MAINMENU.ParentMenuID=0 AND trlMainMenu.LanguageID = @LanguageId

END


