﻿-- =============================================

-- Author:		Arvind Nishad

-- Create date: 2015-03-17

-- Description:	Get Pending Authorisation

-- =============================================

--=====================================================================

--		Modified Date			Modified By			Purpose

--	1.	 18-Mar-2015			Mamatha Shetty		Avoid duplication of rows/records.	

--	2.   19-Mar-2015			Sagar Yerva			Added NOLOCK

--	3.   24-Feb-2016			Prafull Mohite		Added two variable @waitingforApproval and @patientStatusStopped

--	3.   03-Aug-2016			Prafull Mohite		Added order by clause

--========================================================================

-- GetPendingAuthorizations 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'

-- GetPendingAuthorizations '5D9EE643-274D-40F8-9387-009292BDC75F'

CREATE PROCEDURE [dbo].[GetPendingAuthorizations_test]

	-- Add the parameters for the stored procedure here

	@userName varchar(100) =null

	

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	DECLARE @CarehomeUserId VARCHAR(100) = ''
	SELECT @CarehomeUserId = CAST(ISNULL(UserId,'') AS VARCHAR(100)) FROM Users WHERE IsCarehomeUser = 1 AND UserId = @userName



	DECLARE @waitingforApproval BIGINT = 10070,

            @patientStatusStopped   BIGINT = 10021



    -- Insert statements for procedure here

	select 

	customer.CustomerId,

	patient.PatientId,

	patient.SAPPatientNumber,

	patient.DateofBirth,

	patient.PatientStatus,

	customer.SAPCustomerNumber,

	PatientPersonalInfo.FirstName PatientFirstName,

	PatientPersonalInfo.LastName PatientLastName,

	customerPersonalInformation.FirstName CustomerFirstName,

	customerPersonalInformation.LastName CustomerLastName,

	PatientAddress.AddressLine1,

	PatientAddress.AddressLine2,

	case when (select analysis.Text from CustomerMedicalStaffAnalysis analysis (nolock) where ListId=23 and CustomerMedicalStaffAnalysisId in(

	select CustomerMedicalStaffId  from IDXPatientMedicalStaff (nolock) where PatientId=patient.PatientId)) is not null then 

	(select analysis.Text from CustomerMedicalStaffAnalysis analysis(nolock) where ListId=23 and CustomerMedicalStaffAnalysisId in(

	select  CustomerMedicalStaffId from IDXPatientMedicalStaff (nolock) where PatientId=patient.PatientId))

	else '' end as Locality	



	from Patient patient (nolock)

	inner join PersonalInformation PatientPersonalInfo (nolock) on PatientPersonalInfo.PersonalInformationId=patient.PersonalInformationId

	inner join Address PatientAddress (nolock) on PatientPersonalInfo.AddressId=PatientAddress.AddressId

	inner join Customer customer(nolock)  on customer.CustomerId=patient.CustomerId

	inner join PersonalInformation customerPersonalInformation (nolock) on customerPersonalInformation.PersonalInformationId=customer.PersonalInformationId

	inner join Prescription prescription (nolock) on prescription.PatientId=patient.PatientId 

						and (select COUNT(*) from IDXPrescriptionProduct idx (nolock) where idx.PrescriptionId = prescription.PrescriptionId  and idx.IsRemoved=0 

																				and (idx.PrescriptionAuthorizationApprovalFlag=1 or idx.ProductApprovalFlag=1)

							) > 0
    LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
	ON  idxUserCarehome.CarehomeId = patient.CareHomeId



	--inner join Prescription prescription on prescription.PatientId=patient.PatientId

	--inner join IDXPrescriptionProduct idxPrescriptionProduct on idxPrescriptionProduct.PrescriptionId=prescription.PrescriptionId

	--and idxPrescriptionProduct.IsRemoved=0 and 

	--(idxPrescriptionProduct.PrescriptionAuthorizationApprovalFlag=1 or idxPrescriptionProduct.ProductApprovalFlag=1)

		

	where patient.ReasonCodeID=@waitingforApproval and patient.PatientStatus=@patientStatusStopped and ( @userName is null or patient.CustomerId in 

	(select CustomerId from IDXUserCustomer (nolock) where UserId=@userName) )
	AND ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
	order by CustomerFirstName



END





