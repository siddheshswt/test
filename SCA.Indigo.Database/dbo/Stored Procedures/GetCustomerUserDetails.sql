﻿
-- =============================================
-- Author:		<Avinash,Deshpande>
-- Create date: <9-Apr-2015>
-- Description:	<Get User details which are assigned to customer>
-- =============================================
CREATE PROCEDURE [dbo].[GetCustomerUserDetails]
	-- Add the parameters for the stored procedure here
	@CustomerId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select p.FirstName,p.LastName,p.JobTitle,p.Email,p.TelephoneNumber,p.MobileNumber,U.AuthorizationPIN, U.UserName , UC.UserId , UC.IDXAuthorisedUserPrescriptionId
from Users as U(nolock) INNER JOIN PersonalInformation as p(nolock) ON p.PersonalInformationId = U.PersonId 
INNER JOIN IDXAuthorisedUserPrescription as UC(nolock) ON UC.UserId = U.UserId where UC.CustomerId=@CustomerId AND ISNULL(UC.IsRemoved,0)=0
END

