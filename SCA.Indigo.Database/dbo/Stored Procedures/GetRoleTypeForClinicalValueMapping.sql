﻿-- =============================================

-- Author:		
-- Create date: 
-- Description:	Get role type for clinical value mapping

-- =============================================
-- =============================================
--	     Modified Date			Modified By			Purpose

--	1.  05-10-2016				Siddhesh Sawant		Added try catch block


-- =============================================


CREATE PROCEDURE [dbo].[GetRoleTypeForClinicalValueMapping] 
	@customerId BIGINT,
	@languageId INT	
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
		SELECT 
			  TypeId	
			, clinicalcontactanalysismasterid
			, [Index]
			, CASE WHEN t.TranslationType IS NULL
			  THEN tp.Text
			  ELSE  t.TranslationType 
		END ContactName
		FROM ClinicalContactAnalysisMaster (NOLOCK) clinicalAnalysis
		INNER JOIN TranslationProxy tp (NOLOCK) ON tp.TransProxyID = clinicalAnalysis.TransProxyId
		LEFT OUTER JOIN Translation t (NOLOCK) ON t.TransProxyID = tp.TransProxyID
		AND t.LanguageID = CONVERT(VARCHAR(10),@languageId)
		WHERE clinicalcontactanalysismasterid in 
		(
			SELECT ChildListId FROM ClinicalContactRoleMapping (NOLOCK) WHERE CustomerId = @CustomerId and TypeId = 1
		)
		ORDER BY ContactName
	END TRY
	BEGIN CATCH
	END CATCH
END


