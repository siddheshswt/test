﻿-- =============================================
-- Author:		<Saurabh>
-- Create date: <24 Aug 2016>
/*
GetCareHomeOnCustomerId '23', 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
GetCareHomeOnCustomerId '23','5D9EE643-274D-40F8-9387-009292BDC75F'

PR	Date			Author				Description	
1   03/10/2016      Saurabh Mayekar     Added Left Join on vwIDXUserCarehome for carehome users
*/
-- =============================================
CREATE PROCEDURE [dbo].[GetCareHomeOnCustomerId]
@CustomerId VARCHAR(1000),
@UserId     VARCHAR(100)
AS
BEGIN

	DECLARE @CarehomeUserId VARCHAR(100) = ''
	SELECT @CarehomeUserId = CAST(ISNULL(UserId,'') AS VARCHAR(100)) FROM Users WHERE IsCarehomeUser = 1 AND UserId = @UserId

	SELECT (ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'') + ' (' +  ISNULL(SUBSTRING(C.SAPCareHomeNumber, PATINDEX('%[^0 ]%', C.SAPCareHomeNumber + ' '), LEN(C.SAPCareHomeNumber)),'') +')' ) AS CareHomeName,
           C.CareHomeId [CareHomeId]
	FROM      CareHome C
	LEFT JOIN vwIDXUserCarehome idxUserCarehome
	ON  idxUserCarehome.CarehomeId = C.CareHomeId
	LEFT JOIN PersonalInformation P
	ON C.PersonalInformationId = P.PersonalInformationId
	WHERE ',' + @CustomerId + ',' LIKE '%,' + CAST(C.CustomerId AS varchar) + ',%'
	AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
	ORDER BY P.FirstName, P.LastName

END