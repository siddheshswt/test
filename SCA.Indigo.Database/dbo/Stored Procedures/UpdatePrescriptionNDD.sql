﻿

-- =============================================
-- Author:		Arvind Nishad
-- Create date: 10-03-2015
-- Description:	Update NDD in IDXPrescriptionProduct table
-- =============================================

--		Modified Date		Modified By			Purpose
--  1	04-10-2016			Siddhesh Sawant		Added trnasaction and try catch block


CREATE PROCEDURE [dbo].[UpdatePrescriptionNDD] 
(
@PrescriptionId BIGINT=NULL,
@NDD DATETIME
)
AS BEGIN
	DECLARE @Result VARCHAR(200)
	DECLARE @ResultMessage VARCHAR(200)
	DECLARE @CurrentDate DATETIME = GETDATE()
	DECLARE @ModifiedBy VARCHAR(100) = CAST(NEWID() AS VARCHAR(100))
	BEGIN TRY
		BEGIN TRANSACTION
			--UPDATE NDD
					UPDATE IDXPrescriptionProduct 
					SET
					NextDeliveryDate=@Ndd 
					WHERE IDXPrescriptionProductId  =@PrescriptionId

			SELECT @@ROWCOUNT Result

			/*
			--Added conditions to update DQ flags after order is generated
			--UPDATE DQ FLag 
			 DECLARE @OrderStatus BIGINT = 10105
			 DECLARE @FlagDQ1 BIT, @FlagDQ2 BIT, @FlagDQ3 BIT, @FlagDQ4 BIT, @OrderGenerated BIGINT = 10048
			 IF(@OrderStatus = @OrderGenerated)			 
			 BEGIN
				SELECT 
				 @FlagDQ1 = FlagDQ1
				,@FlagDQ2 = FlagDQ2
				,@FlagDQ3 = FlagDQ3
				,@FlagDQ4 = FlagDQ4 FROM IDXPrescriptionProduct WHERE IDXPrescriptionProductId =  @PrescriptionId

				IF (@FlagDQ1 = 1)
				BEGIN
				UPDATE IDXPrescriptionProduct SET 
					 FlagDQ1 = 0
					,FlagDQ2 = 1
					,FlagDQ3 = 0
					,FlagDQ4 = 0 WHERE IDXPrescriptionProductId =  @PrescriptionId
				END
				ELSE IF (@FlagDQ2 = 1)
				BEGIN
				UPDATE IDXPrescriptionProduct SET 
					 FlagDQ1 = 0
					,FlagDQ2 = 0
					,FlagDQ3 = 1
					,FlagDQ4 = 0 WHERE IDXPrescriptionProductId =  @PrescriptionId
				END
				ELSE IF (@FlagDQ3 = 1)
				BEGIN
				UPDATE IDXPrescriptionProduct SET 
					 FlagDQ1 = 0
					,FlagDQ2 = 0
					,FlagDQ3 = 0
					,FlagDQ4 = 1 WHERE IDXPrescriptionProductId =  @PrescriptionId
				END
				ELSE IF (@FlagDQ4 = 1)
				BEGIN
				UPDATE IDXPrescriptionProduct SET 
					 FlagDQ1 = 1
					,FlagDQ2 = 0
					,FlagDQ3 = 0
					,FlagDQ4 = 0 WHERE IDXPrescriptionProductId =  @PrescriptionId
				END
			END
			*/
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: UpdatePrescriptionNDD' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @ModifiedBy
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage
	END CATCH
END

