﻿
-- =============================================
-- Author:		Sachin Phapale
-- Create date: 2015-04-30
-- Description:	Get clinical values by customer id
-- =============================================
--exec GetClinicalContactsValueMapping 9,7
CREATE PROCEDURE [dbo].[GetClinicalContactsValueMapping]
	-- Add the parameters for the stored procedure here	
	 @CustomerId BIGINT
	,@ChildListId BIGINT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF(@ChildListId > 0)
BEGIN
	SELECT 
			 C.ClinicalContactRoleMappingId
			,C.ClinicalContactValueMappingId
			,dbo.GetClinicalContactValueName(C.ChildValueId) as ChildValue
			,dbo.GetClinicalContactValueName(C.ParentValueId) as ParentValue
			,C.ChildListId
			,R.HierarchyListId
			,C.IsRemoved
			,R.ParentListId
			,c.ParentValueId
			,c.ChildValueId
	FROM ClinicalContactValueMapping C WITH (NOLOCK)
	INNER JOIN ClinicalContactRoleMapping R (NOLOCK) 
	ON C.ClinicalContactRoleMappingId = R.ClinicalContactRoleMappingId 
	WHERE C.ChildListId = @ChildListId AND C.IsRemoved = 0 AND R.CustomerId = @CustomerId
END
ELSE
BEGIN
	SELECT 
			 C.ClinicalContactRoleMappingId
			,C.ClinicalContactValueMappingId
			,dbo.GetClinicalContactValueName(C.ChildValueId) as ChildValue
			,dbo.GetClinicalContactValueName(C.ParentValueId) as ParentValue
			,C.ChildListId
			,R.HierarchyListId
			,C.IsRemoved
			,R.ParentListId
			,c.ParentValueId
			,c.ChildValueId
	FROM ClinicalContactValueMapping C WITH (NOLOCK)
	INNER JOIN ClinicalContactRoleMapping R (NOLOCK)  
	ON C.ClinicalContactRoleMappingId = R.ClinicalContactRoleMappingId
	WHERE C.IsRemoved = 0 AND R.CustomerId = @CustomerId
END	
END
