﻿

CREATE PROCEDURE [dbo].[XSCACreateProcareOrders] 
@Input_Deliverydate date
AS
BEGIN
BEGIN TRY
              
BEGIN TRAN

declare @rundate		date = @Input_Deliverydate

declare @patientid		bigint
declare @homeid			bigint
declare @NDD			Date

Declare @orderid		bigint

declare @prescriptionid	bigint
declare @ProductId		bigint
declare @quantity		bigint
declare @nextqtyflag	bigint

--set @rundate = '2015-06-12'

	   declare cursor_CreateOrderHeader Cursor
       LOCAL SCROLL STATIC 
       FOR 
			Select distinct pat.PatientId,pat.CareHomeId,pat.NextDeliveryDate
			  from Patient (nolock) pat
			       inner join Prescription (nolock) pre on pre.PatientId = pat.PatientId
				   inner join IDXPrescriptionProduct (nolock) pro on pro.PrescriptionId = pre.PrescriptionId
			 where pat.PatientType      in (10010,10019)
			   and pat.NextDeliveryDate = @rundate
			   and pat.PatientStatus = 10020
 			   and pro.NextDeliveryDate = @rundate
 			   and pro.ValidFromDate <= @rundate
 			   and pro.ValidToDate >= @rundate
 			   and pro.Status = 'Active'
 			   and pat.PatientId not in (Select pat.PatientId
										   from Patient (nolock) pat
											    inner join Prescription (nolock) pre on pre.PatientId = pat.PatientId
											    inner join orders (nolock) ord on ord.PatientId = pat.patientid
										  where pat.PatientType      in (10010,10019)
											and pat.NextDeliveryDate     = @rundate
											and ord.SAPOrderDeliveryDate = pat.NextDeliveryDate)

		OPEN cursor_CreateOrderHeader
        FETCH NEXT FROM cursor_CreateOrderHeader
         INTO @patientid,@homeid,@NDD

		      WHILE @@FETCH_STATUS = 0
              BEGIN 
			  INSERT INTO Orders
			             (OrderType
           			     ,SAPOrderNo
						 --,SAPDeliveryNo
						 ,IsRushFlag
						 ,OrderStatus
						 --,InvoiceNo
						 ,OrderDate
						 ,SAPOrderDeliveryDate
 						 ,ExcludeServiceCharge
						 ,ModifiedBy
						 ,ModifiedDate
						 ,SAPMessage
						 ,HelixOrderNo
						 ,PatientId
						 ,CareHomeId
						 ,SamplePatientId
						 ,DerivedNDD)
			      VALUES (10053
						 ,null
						 --,null
						 ,0
						 ,10047
						 --,null
						 ,getdate()
						 ,@ndd
						 ,0
						 ,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
						 ,getdate()
						 ,null
						 ,null
						 ,@PatientId
						 ,@HomeId
						 ,null
						 ,@ndd)

				Set @orderid = SCOPE_IDENTITY()

	   declare cursor_CreateOrderline Cursor
       LOCAL SCROLL STATIC 
       FOR 
			Select pre.PrescriptionId
				  ,pro.ProductId
				  ,case when pro.FlagDQ1 = 1 then pro.Del1
						when pro.FlagDQ2 = 1 then pro.Del2
						when pro.FlagDQ3 = 1 then pro.Del3
						when pro.FlagDQ2 = 1 then pro.Del4
				   else 0
					end quantity
				  ,case when pro.FlagDQ1 = 1 then 2
						when pro.FlagDQ2 = 1 then 3
						when pro.FlagDQ3 = 1 then 4
						when pro.FlagDQ2 = 1 then 1
				   else 0
				   end nextqtyflag
			  from Prescription pre
				   inner join IDXPrescriptionProduct pro on pro.PrescriptionId = pre.PrescriptionId
			 where pro.NextDeliveryDate = @rundate
 			   and pro.ValidFromDate   <= @rundate
 			   and pro.ValidToDate     >= @rundate
 			   and pro.Status			= 'Active'
 			   and pre.PatientId		= @patientid
				
		OPEN cursor_CreateOrderline
        FETCH NEXT FROM cursor_CreateOrderline
         INTO @prescriptionid,@ProductId,@quantity,@nextqtyflag

		      WHILE @@FETCH_STATUS = 0
              BEGIN 
				INSERT INTO IDXOrderProduct
							(OrderId
							,ProductId
							,Quantity
							,IsFOC
							,ModifiedBy
							,ModifiedDate
							,IsRemoved
							,ProductDescription
							,SampleProductId)
					VALUEs  (@OrderId
							,@ProductId
							,@quantity
							,0
							,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
							,GETDATE()
							,0
							,null
							,null)

				Update IDXPrescriptionProduct
				   set FlagDQ1 = Case when @nextqtyflag = 1 then 1 else 0 end 
				      ,FlagDQ2 = Case when @nextqtyflag = 2 then 1 else 0 end 
					  ,FlagDQ3 = Case when @nextqtyflag = 3 then 1 else 0 end 
					  ,FlagDQ4 = Case when @nextqtyflag = 4 then 1 else 0 end 
				  where PrescriptionId = @prescriptionid
				    and ProductId		= @ProductId

        FETCH NEXT FROM cursor_CreateOrderline
         INTO @prescriptionid,@ProductId,@quantity,@nextqtyflag
		      END

       CLOSE cursor_CreateOrderline -- close the cursor
       DEALLOCATE cursor_CreateOrderline

	     FETCH NEXT FROM cursor_CreateOrderHeader
         INTO @patientid,@homeid,@NDD

		 End
       CLOSE cursor_CreateOrderHeader -- close the cursor
       DEALLOCATE cursor_CreateOrderHeader

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'Error Creating Procare Orders'

END CATCH
END


