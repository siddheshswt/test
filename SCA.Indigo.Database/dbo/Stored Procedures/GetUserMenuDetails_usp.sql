﻿

-- =============================================
-- Author:		Mukesh Shrimali
-- Create date: 2014-11-20
-- Modified By: Basanta on 13-03-2015. Checked the ViewOnly and FullRigts property to dosplay Menu
-- Description:	stored procedure to get menu list as per role

-- ===============================================================================

--		Modified Date			Modified By			Purpose
--		28-04-2015.				Arvind Nishad		Added ParentMenuIndex,DisplayMenuIndex to show Menu position based on index rather hardcoded in controller 
-- ===============================================================================
--		22-05-2015.				Basanta				Added IsDisplay Comlumn 
--      15-07-2015				Gaurav Lad			Fetching data from Translation table
-- ===============================================================================
--Sample execution 
/*	
  declare @UserName varchar(100) = 'SCAADMIN'			
  exec GetUserMenuDetails_usp @UserName
*/
-- =============================================
 
CREATE PROCEDURE [dbo].[GetUserMenuDetails_usp] 
		-- Add the parameters for the stored procedure here
	@UserName varchar(100),
	@LanguageId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT 
	irm.MenuID
	,irm.FullRights
	,irm.ViewOnly
	,me.ParentMenuID
	,CASE WHEN trl.TranslationType IS NULL
		THEN trp.[Text]
		ELSE trl.TranslationType 
		END MenuName
	,me.ControllerName
	,me.ActionName 
	,me.DisplayIndex
	,me.ParentMenuIndex
	,case when (select count(ParentMenuID) from Menu where ParentMenuID=me.MenuId )>0 then  cast( 1 as bit)
		else cast( 0 as bit) end HasSubMenu
	,me.IsDisplay

		FROM IDXRoleMenu irm inner join Menu me ON irm.MenuID=me.MenuId
		Inner Join TranslationProxy trp ON trp.TransProxyID = me.TranslationProxyId
		Inner Join Translation trl ON trp.TransProxyID = trl.TransProxyID
		WHERE irm.RoleID=
			(	SELECT RoleID 
				FROM Users ur inner join IDXUserRole iur ON iur.UserID=ur.UserId 
				WHERE ur.UserName= @UserName
			) 
		AND (irm.ViewOnly =1 OR irm.FullRights =1)
		AND trl.LanguageID = @LanguageId		
ORDER BY me.ParentMenuIndex ,  me.DisplayIndex 
END




