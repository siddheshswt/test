﻿-- =============================================
-- Author:		Gaurav Lad
-- Create date: 7th May 2015
-- Description:	Fetch the exact address of the document 
-- =============================================
Create PROCEDURE [dbo].[GetAttachmentByAttachmentId]
	-- Add the parameters for the stored procedure here
	@AttachmentId VARCHAR(MAX)
AS
BEGIN	
	SET NOCOUNT ON;
	
	SELECT AttachmentLocation AS AttachmentLocation FROM Attachment WHERE AttachmentId IN (@AttachmentId)

END

