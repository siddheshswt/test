﻿/******************************
** File:    GetAlertForChangesOnHomePage
** Name:	GetAlertForChangesOnHomePage
** Desc:	Used to Retrive the data for Home Page Alert
** Auth:	Saurabh
** Date:	05/05/2016

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
*******************************/

-- =============================================================
-- Sample: exec GetAlertForChangesOnHomePage 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'

CREATE PROC [dbo].[GetAlertForChangesOnHomePage]
(
      @UserId  VARCHAR(100)
)
AS
BEGIN

     SELECT 'I' 'NotificationType', intr.Description 'Description'
	 FROM       Interaction(NOLOCK)       intr
	 INNER JOIN UserActionMonitor(NOLOCK) uam
	 ON     intr.AssignToUserId = uam.UserId
	 WHERE  DisplayAsAlert = 1
	 AND    AssignToUserId = @UserId
	 AND    ISNULL(intr.ModifiedDate,intr.CreatedDate) > uam.LastAccessHome
	 ORDER BY ModifiedDate, CreatedDate DESC

END