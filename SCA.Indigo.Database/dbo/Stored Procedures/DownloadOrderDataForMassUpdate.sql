﻿

/******************************
** File:    DownloadOrderDataForMassUpdate
** Name:	DownloadOrderDataForMassUpdate
** Desc:	Download the Order Data of all the patients who are under provided Customer and Selected Criteria
** Auth:	Sagar
** Date:	19/06/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1	30/06/2015		Sagar		Added CAST on Some Fields where EF gives Nullable
**	2	20/07/2015		Sagar		Added SAPCustomerId and SAPCareHomeId in Output Parameters

*******************************/
--'1150901,150983,150985,150901'
-- ==========================================================================================

--   exec   [dbo].[DownloadOrderDataForMassUpdate] 9,'','','','','10010','','', '','','10105' 
--   exec   [dbo].[DownloadOrderDataForMassUpdate] 9,'','','905112','','','','', '','','' 
--   exec   [dbo].[DownloadOrderDataForMassUpdate] 9,'','','','130986','','','', '','','' 



CREATE PROCEDURE [dbo].[DownloadOrderDataForMassUpdate]
	-- Add the parameters for the stored procedure here
	 @CustomerId BIGINT
	,@ProductSAPId VARCHAR(2000)
	,@PatientIds VARCHAR(2000)
	,@CareHomeIds VARCHAR(2000)
	,@OrderIds VARCHAR(2000)
	,@PatientTypes VARCHAR(2000)
	,@CareHomeOrderTypes VARCHAR(2000)
	,@OrderTypes VARCHAR(2000)
	,@PatientStatus VARCHAR(2000)
	,@CareHomeStatus VARCHAR(2000)
	,@OrderStatus VARCHAR(2000)	
AS
BEGIN
	DECLARE @status varchar(1000) = ''

	IF @OrderStatus = ''
		OR @OrderStatus IS NULL
	BEGIN
		SET @status = '10047,10105'
	END
	ELSE
	BEGIN
		SET @status = @OrderStatus
	END	

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here


		SELECT ISNULL(p.CustomerId, 0) AS CustomerId
			,ISNULL(cust.SAPCustomerNumber, '') AS SAPCustomerId
			,p.SAPPatientNumber AS SAPPatientId
			,ISNULL(p.PatientId, 0) AS PatientId
			,p.CareHomeId AS CareHomeId
			,ISNULL(care.SAPCareHomeNumber,'') AS SAPCareHomeId
			,ISNULL(o.OrderType, 0) AS OrderType
			,ISNULL(o.OrderId, 0) AS OrderId
			,prod.SAPProductID AS SAPProductId
			,prod.BaseMaterial AS BaseMaterial
			,idxprod.Quantity AS Quantity
			,ISNULL(o.DerivedNDD, o.SAPOrderDeliveryDate) AS Deliverydate
			,ISNULL(o.OrderStatus, 0) AS OrderStatus
			,custpersonal.FirstName AS CustomerName
			,patpersonal.FirstName + ISNULL(patpersonal.LastName, '') AS PatientName
			,ISNULL(prod.ProductId, 0) AS ProductId
			,ISNULL(carePersonal.FirstName, '') AS CareHomeName
		FROM IDXOrderProduct(NOLOCK) idxprod
		LEFT JOIN Orders(NOLOCK) o ON o.OrderId = idxprod.OrderId
		LEFT JOIN Patient(NOLOCK) p ON o.PatientId = p.PatientId
		LEFT JOIN CareHome(NOLOCK) care ON care.CareHomeId = p.CareHomeId
		LEFT JOIN Customer(NOLOCK) cust ON cust.CustomerId = @CustomerId
		LEFT JOIN Product(NOLOCK) prod ON idxprod.ProductId = prod.ProductId
		LEFT JOIN PersonalInformation(NOLOCK) custpersonal ON custpersonal.PersonalInformationId = cust.PersonalInformationId
		LEFT JOIN PersonalInformation(NOLOCK) patpersonal ON p.PersonalInformationId = patpersonal.PersonalInformationId
		LEFT JOIN PersonalInformation(NOLOCK) carepersonal ON care.PersonalInformationId = carepersonal.PersonalInformationId
		WHERE p.CustomerId = @CustomerId
			AND (
				@PatientIds = ''
				OR p.SAPPatientNumber IN (
					SELECT REPLICATE('0', 10 - LEN(Value)) + Value
					FROM dbo.SplitToTable(@PatientIds)
					)
				)
			AND (
				@CareHomeIds = ''
				OR care.SAPCareHomeNumber IN (
					SELECT REPLICATE('0', 10 - LEN(Value)) + Value
					FROM dbo.SplitToTable(@CareHomeIds)
					)
				)
			AND (
				@ProductSAPId = ''
				OR prod.SAPProductID IN (
					SELECT REPLICATE('0', 18 - LEN(Value)) + Value
					FROM dbo.SplitToTable(@ProductSAPId)
					)
				)
			AND (
				@CareHomeOrderTypes = ''
				OR care.OrderType IN (
					SELECT Value
					FROM dbo.SplitToTable(@CareHomeOrderTypes)
					)
				)
			AND (
				@CareHomeStatus = ''
				OR care.CareHomeStatus IN (
					SELECT Value
					FROM dbo.SplitToTable(@CareHomeStatus)
					)
				)
			AND (
				@OrderTypes = ''
				OR o.OrderType IN (
					SELECT Value
					FROM dbo.SplitToTable(@OrderTypes)
					)
				)
				AND (
				@PatientTypes = ''
				OR p.PatientType  IN (
					SELECT Value
					FROM dbo.SplitToTable(@PatientTypes)
					)
				)
				AND (
				@PatientStatus = ''
				OR p.PatientStatus  IN (
					SELECT Value
					FROM dbo.SplitToTable(@PatientStatus)
					)
				)
				
			AND (
				 o.OrderStatus IN (
					SELECT Value
					FROM dbo.SplitToTable(@status)
					)
				)
			AND (
				@OrderIds = ''
				OR o.SAPOrderNo IN (
					SELECT REPLICATE('0', 10 - LEN(Value)) + Value
					FROM dbo.SplitToTable(@OrderIds)
					)
				)
				ORDER BY o.PatientId,o.OrderId
	END
	

