﻿



/******************************
** File:    GetCustomerClinicalAnalysisMassData
** Name:	GetCustomerClinicalAnalysisMassData
** Desc:	Get Customer Clinical Analysis MassData
** Auth:	NA
** Date:	22 - 06 - 2015

********************************
** exec GetCustomerClinicalAnalysisMassData 9 , 1 
** exec GetCustomerClinicalAnalysisMassData 9 , 2
*/
CREATE PROC [dbo].[GetCustomerClinicalAnalysisMassData]
(
	@CustomerId BIGINT,
	@TypeId INT
)
AS
BEGIN
	SET NOCOUNT ON;


SELECT	INFO.CustomerID AS CustomerId,
		Customer.SAPCustomerNumber AS SAPCustomerNumber,
		M.ClinicalContactAnalysisMasterId AS ClinicalContactTypeId, 
		T.Text AS ClincialContactType, 
		INFO.ContactName AS ClincialContactValue, 
		CASE 
		WHEN ISNULL(INFO.ContactName,'') = '' THEN NULL
		ELSE CAST(INFO.IDXCustomerMedicalStaffAnalysisInfoID AS VARCHAR(10))
		END AS ClinicalContactValueTypeId,
		INFO.Remarks AS Remarks
	FROM IDXCustomerMedicalStaffAnalysisInfo INFO (NOLOCK)
	INNER JOIN ClinicalContactAnalysisMaster M (NOLOCK) 
	ON INFO.MedicalAnalysisListID = M.ClinicalContactAnalysisMasterId
	INNER JOIN TranslationProxy T (NOLOCK) ON M.TransProxyId = T.TransProxyId
	INNER JOIN Customer CUSTOMER ON CUSTOMER.CustomerId = INFO.CustomerID
	WHERE CUSTOMER.CustomerID = @CustomerId AND M.TypeId = @TypeId 
ORDER BY T.TEXT

SET NOCOUNT OFF;
END





