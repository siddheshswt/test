﻿


CREATE PROCEDURE [dbo].[XSCAUpdatePatientNDD] 
as
BEGIN
BEGIN TRY
              
BEGIN TRAN --OPEN TRANSACTION

	   DECLARE @Patientid				 bigint
	   DECLARE @patfreq					 bigint
	   DECLARE @patndd					 date
	   DECLARE @ldd						 date
	   DECLARE @NDD						 date
	   
       DECLARE cursor_updateNDDLDDforPatient CURSOR -- Declare cursor
       LOCAL SCROLL STATIC 
       FOR                          
				select pat.PatientId
				      ,pat.DeliveryFrequency patfreq
					  ,pat.NextDeliveryDate patndd
					  ,max(SAPOrderDeliveryDate) LDD
					  ,max((SAPOrderDeliveryDate) + (pat.DeliveryFrequency*7)) NDD
				from patient pat
				    inner join Orders ord on ord.PatientId = pat.PatientId
					inner join IDXOrderProduct opro on opro.OrderId = ord.OrderId
					inner join List list on list.ListId = ord.OrderStatus
				where ord.OrderType = 10053
				  and ord.OrderStatus = 10048
				group by pat.PatientId
				        ,pat.DeliveryFrequency
				        ,pat.NextDeliveryDate
				having max(SAPOrderDeliveryDate) + (pat.DeliveryFrequency*7) != pat.NextDeliveryDate
                     
              OPEN cursor_updateNDDLDDforPatient
              FETCH NEXT FROM cursor_updateNDDLDDforPatient
              INTO @patientid,@patfreq,@patndd,@ldd,@NDD

              WHILE @@FETCH_STATUS = 0
              BEGIN                
                   
                     
					 UPDATE Patient 
					 SET DeliveryDate = @ldd
					    ,NextDeliveryDate = @NDD
					  WHERE patientid = @patientid
                     

                     FETCH NEXT FROM cursor_updateNDDLDDforPatient
                     INTO @patientid,@patfreq,@patndd,@ldd,@NDD  
              END

       CLOSE cursor_updateNDDLDDforPatient -- close the cursor
       DEALLOCATE cursor_updateNDDLDDforPatient -- Deallocate the cursor 

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE UPDATING Patient NDD LDD'

END CATCH
END




