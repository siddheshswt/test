﻿
/******************************
** File:    GetCareHomeReportData
** Name:	GetCareHomeReportData
** Desc:	Get Carehome Report Patient Details
** Auth:	Siddhesh
** Date:	09Dec2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1. 28Dec2015		Siddhesh   Added IsExport flag
**	2. 28Dec2015		Vikas      Added Paging and Export
**	3. 30Dec2015		Siddhesh   Added sorting logic
**  4. 31Dec2015        Vikas      Changed the sorting column for Product
**  5. 04Jan2016        Siddhesh   Removed flag variable logic
*******************************/

--===========================================================================
--exec GetCareHomeReportData 40,0,1,10
--exec GetCareHomeReportData 40,1,1,10

CREATE PROCEDURE [dbo].[GetCareHomeReportData] 
	-- Add the parameters for the stored procedure here
	@CareHomeId BIGINT = 0,
	@IsExport BIT = 0,
	@pageNum BIGINT = 0,
	@recordCount BIGINT = 0,
	@SortColumn	VARCHAR(50) = NULL,
	@SortOrder	VARCHAR(10) = 'DESC'		
AS
BEGIN
SET NOCOUNT ON;
IF 1=0 BEGIN
    SET FMTONLY OFF
END	

SET @SortOrder = (SELECT UPPER(@SortOrder))

 DECLARE @start bigint = 0,
         @end bigint = 0

SET @start = ((@recordCount * @pageNum) - @recordCount) + 1
SET @end = (@start-1) + @recordCount 

DECLARE @TotalRecords BIGINT 
--temporary table to pass details of patient and product		
CREATE TABLE #CarehomeReport 
(
 TotalRecords BIGINT,
 ProductCount VARCHAR(10),
 PatientName VARCHAR(100),
 PatientSAPId VARCHAR(20), 
 DOB DATE,
 DateOfBirth VARCHAR(30),
 PatientStatus VARCHAR(3),
 LastAssess DATE,
 LastAssessDate VARCHAR(30),
 NextAssess DATE,
 NextAssessDate VARCHAR(30),
 SAPProductId VARCHAR(20),
 ProductDescription VARCHAR(100),
 AssPPD VARCHAR(10),
 ActPPD VARCHAR(10),
 Frequency VARCHAR(10),
 NextDD DATE,
 NDD VARCHAR(30),
 Comment VARCHAR(10),
 PatientId BIGINT)



DECLARE 
		@PId BIGINT,
		@PName VARCHAR(100),
        @PSAPId VARCHAR(20), 
		@DOB DATE,		
		@PDOB VARCHAR(30), 
		@PStatus VARCHAR(3), 
		@LastAssess DATE,
		@PLastAsses VARCHAR(30), 
		@NextAssess DATE,
		@PNextAssess VARCHAR(30),
		@ProductId VARCHAR(20),
		@ProductDescription VARCHAR(100),
		@AssPPD VARCHAR(10),
		@ActPPD VARCHAR(10),
		@Frequency VARCHAR(10),
		@NextDD DATE,
		@NDD VARCHAR(30),
		@RowCount INT,
		@ProductCount INT,
		@Flag INT = 0
    

--1st cursor to get all patient details
DECLARE PatientData CURSOR
FOR 
SELECT 
         patient.PatientId
		,pInfo.FirstName +' '+ pInfo.LastName 		
		,CAST(CAST (patient.SAPPatientNumber AS BIGINT) AS VARCHAR(20)) 
		,patient.DateofBirth				
		,CONVERT(VARCHAR(10),patient.DateofBirth,103) AS DateofBirth
		,CASE WHEN patient.PatientStatus = 10020 THEN 'ACT' ELSE 'STO' END 
		,patient.AssessmentDate
		,CONVERT(VARCHAR(10),patient.AssessmentDate,103) AS AssessmentDate
		,patient.NextAssessmentDate
		,CONVERT(VARCHAR(10),patient.NextAssessmentDate,103) AS NextAssessmentDate								
	FROM Patient patient (NOLOCK)	
	INNER JOIN PersonalInformation pInfo (NOLOCK) ON patient.PersonalInformationId = pInfo.PersonalInformationId		
	INNER JOIN CareHome CHome (NOLOCK) ON CHome.CareHomeId = patient.CareHomeId	
	INNER join List patientType (NOLOCK)  on patientType.ListId=patient.PatientType	
	WHERE CHome.CareHomeId = @CareHomeId
	AND patient.PatientStatus in (10020, 10021)
	order by pInfo.LastName

OPEN PatientData		

FETCH NEXT FROM PatientData
 INTO @PId, @PName, @PSAPId, @DOB, @PDOB, @PStatus, @LastAssess, @PLastAsses, @NextAssess, @PNextAssess

WHILE @@FETCH_STATUS = 0
BEGIN

SET @ProductCount = (SELECT COUNT(*) FROM IDXPrescriptionProduct ( NOLOCK) WHERE PrescriptionId = (SELECT PrescriptionId FROM Prescription (NOLOCK) WHERE PatientId = @PId) AND Status = 'Active')
--2nd cursor to get prescription product of patient 
 DECLARE ProductData CURSOR
 FOR
 SELECT 	
		 prod.BaseMaterial
		,prod.DescriptionUI
		,idxPrescriptionProduct.AssessedPadsPerDay
	    ,idxPrescriptionProduct.ActPPD		
		,idxPrescriptionProduct.Frequency
		,idxPrescriptionProduct.NextDeliveryDate
		,CONVERT(VARCHAR(10),idxPrescriptionProduct.NextDeliveryDate,103) AS NextDeliveryDate
	FROM 
	Patient patient (NOLOCK)		
	INNER JOIN CareHome CHome (NOLOCK) ON CHome.CareHomeId = patient.CareHomeId			
	INNER JOIN Prescription prescription  (NOLOCK) ON prescription.PatientId=Patient.PatientID
	INNER JOIN IDXPrescriptionProduct idxPrescriptionProduct  (NOLOCK) ON prescription.PrescriptionId=idxPrescriptionProduct.PrescriptionId	
	AND idxPrescriptionProduct.Status='Active'
	LEFT OUTER JOIN Product prod ON prod.ProductId=idxPrescriptionProduct.ProductId
	WHERE CHome.CareHomeId = @CareHomeId
	AND patient.PatientId = @PId

SET @Flag = 0

OPEN ProductData 

FETCH NEXT FROM ProductData
 INTO @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NextDD, @NDD


WHILE @@FETCH_STATUS = 0
BEGIN
IF(@IsExport = 0)
BEGIN		
		INSERT INTO #CarehomeReport VALUES
		(0,@ProductCount, @PName, @PSAPId, @DOB, @PDOB, @PStatus, @LastAssess, @PLastAsses, @NextAssess, @PNextAssess, @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NextDD, @NDD, '',@PId)
END
ELSE
BEGIN
	INSERT INTO #CarehomeReport VALUES
		(0,@ProductCount, @PName, @PSAPId, @DOB, @PDOB, @PStatus, @LastAssess, @PLastAsses, @NextAssess, @PNextAssess, @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NextDD, @NDD, '',@PId)

	--SET @Flag = @Flag + 1
	--IF(@Flag = 1)
	--BEGIN
	--	INSERT INTO #CarehomeReport VALUES
	--	(0,@ProductCount, @PName, @PSAPId, @DOB, @PDOB, @PStatus, @LastAssess, @PLastAsses, @NextAssess, @PNextAssess, @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NextDD, @NDD, '')
	--END
	--ELSE
	--BEGIN		
	--	INSERT INTO #CarehomeReport VALUES
	--	(0,'', '', '', '', '', '', '', '', '', '', @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, '', @NDD, '')
	--END	
END
	
FETCH NEXT FROM ProductData
INTO @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NextDD, @NDD
END



CLOSE ProductData
DEALLOCATE ProductData		

FETCH NEXT FROM PatientData
INTO @PId, @PName, @PSAPId, @DOB, @PDOB, @PStatus, @LastAssess, @PLastAsses, @NextAssess, @PNextAssess

END

CLOSE PatientData
DEALLOCATE PatientData		

SELECT @TotalRecords = COUNT(*) FROM #CarehomeReport

UPDATE #CarehomeReport
SET TotalRecords = @TotalRecords	


IF @IsExport = 1
BEGIN
	SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS RowNumber 
			,TotalRecords
			,ProductCount
			,PatientName
			,PatientSAPId
			,DateOfBirth
			,PatientStatus
			,LastAssessDate
			,NextAssessDate
			,SAPProductId
			,ProductDescription
			,AssPPD
			,ActPPD
			,Frequency
			,NDD
			,Comment
			,PatientId 
	FROM #CarehomeReport
END
ELSE
BEGIN    
 WITH carehomeReport_CTE as (
	SELECT ROW_NUMBER() OVER(
	--ORDER BY (SELECT NULL)
	ORDER BY 				
		CASE
		WHEN @SortOrder <> 'ASC' THEN ''
	    WHEN @SortColumn = 'PatientName' THEN PatientName
	    END ASC
		,CASE
        WHEN @SortOrder <> 'DESC' THEN ''
        WHEN @SortColumn = 'PatientName' THEN PatientName
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'PatientSAPId' THEN CAST(PatientSAPId AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'PatientSAPId' THEN CAST(PatientSAPId AS BIGINT)
        END DESC	
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'DateOfBirth' THEN CAST (DOB AS DATE)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'DateOfBirth' THEN CAST (DOB AS DATE)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'PatientStatus' THEN PatientStatus
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'PatientStatus' THEN PatientStatus
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'LastAssessDate' THEN CAST(LastAssess AS DATE)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'LastAssessDate' THEN CAST(LastAssess AS DATE)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'NextAssessDate' THEN CAST(NextAssess AS DATE)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'NextAssessDate' THEN CAST(NextAssess AS DATE)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'SAPProductId' THEN CAST(SAPProductId AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'SAPProductId' THEN CAST(SAPProductId AS BIGINT)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'ProductDescription' THEN ProductDescription
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'ProductDescription' THEN ProductDescription
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'AssPPD' THEN CAST(AssPPD AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'AssPPD' THEN CAST(AssPPD AS BIGINT)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'ActPPD' THEN CAST(ActPPD AS FLOAT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'ActPPD' THEN CAST(ActPPD AS FLOAT)
        END DESC
		,CASE
        WHEN @SortOrder <> 'ASC' THEN ''
        WHEN @SortColumn = 'Frequency' THEN CAST(Frequency AS BIGINT)
        END ASC
		,CASE		
        WHEN @SortOrder <> 'DESC' THEN ''		
        WHEN @SortColumn = 'Frequency' THEN CAST(Frequency AS BIGINT)
        END DESC		
		,CASE		
        WHEN @SortOrder <> 'ASC' THEN ''		
        WHEN @SortColumn = 'NDD' THEN CAST(NextDD AS DATE)
        END ASC
		,CASE
        WHEN @SortOrder <> 'DESC' THEN ''
        WHEN @SortColumn = 'NDD' THEN CAST(NextDD AS DATE)
        END DESC			
	) AS RowNumber
			,TotalRecords
			,ProductCount
			,PatientName
			,PatientSAPId
			,DateOfBirth
			,PatientStatus
			,LastAssessDate
			,NextAssessDate
			,SAPProductId
			,ProductDescription
			,AssPPD
			,ActPPD
			,Frequency
			,NDD
			,Comment
			,PatientId
	FROM #CarehomeReport
	)
	SELECT * FROM carehomeReport_CTE WHERE	RowNumber BETWEEN @start AND @end  

END

DROP TABLE #CarehomeReport
				
END

