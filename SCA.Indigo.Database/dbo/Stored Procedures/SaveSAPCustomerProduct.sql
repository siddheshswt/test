﻿
/******************************
** File:    SaveSAPCustomerProduct
** Name:	SaveSAPCustomerProduct
** Desc:	Save Product for Cusomer
** Auth:	Arvind
** Date:	12/12/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1   07/04/2015		Siddhesh	Update sales unit as PKT for CON	
**	2	15/06/2015		Sagar		Added Document Comment		
**	3	26/06/2015		Arvind		Changed logic for existing product from Material(SAPProductId) to BaseMaterial
**	4	29/07/2015		Arvind		Update if ProductId exists in both table else insert(previously every time IDXCustomerProduct was inserted) 
*******************************/
-- ==============================================================================================
-- Execute commond 
/*
DECLARE	@return_value int,
		@ProductId varchar(50)

EXEC	[dbo].[SaveSAPCustomerProduct]
		@CustomerId = 27,
		@SAPProductId = N'94926500',
		@SAPProductName = N'ID Expert Form Plus (6x21)',
		@SalesUnit = 'CON',
		@ApprovalFlag = 0,
		@BaseMaterial = N'949265',
		@NetValue =0.29,
		@Vat = 0.06,
		@PackSize = N'21',
		@ModifiedBy = N'ECDBCD8F-145A-411A-9FBD-98CC5D2F0786',
		@ModifiedDate = N'2015-07-22 16:48:25.820',
		@ProductId = @ProductId OUTPUT

*/
CREATE PROCEDURE [dbo].[SaveSAPCustomerProduct] (
	-- Add the parameters for the stored procedure here
	@CustomerId BIGINT
	,@SAPProductId NVARCHAR(50)
	,@SAPProductName VARCHAR(200)
	,@SalesUnit VARCHAR(50) = NULL
	,@ApprovalFlag BIT = NULL
	,@BaseMaterial NVARCHAR(50)
	,@NetValue DECIMAL(18, 0)
	,@Vat DECIMAL(18, 0)
	,@PackSize NVARCHAR(50)
	,@ModifiedBy VARCHAR(100)
	,@ModifiedDate DATETIME = NULL
	,@ProductId VARCHAR(50) OUTPUT
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION;

		-- Insert statements for procedure here
		--DECLARE @ProductId BIGINT
		DECLARE @IdxCustomerProductId BIGINT
		DECLARE @ProductResult BIGINT
		DECLARE @IDXCustomerProductResult BIGINT
		DECLARE @CommondType INT ---- 1=Insert and 2=Update
		DECLARE @Result VARCHAR(200)
		DECLARE @ResultMessage VARCHAR(200)
		DECLARE @CurrentDate DATETIME = getdate()

		SELECT @ProductId = ProductId
		FROM Product
		WHERE BaseMaterial = @BaseMaterial

		--if not exists(select 1 from Product where SAPProductId=@SAPProductId)	
		IF @ProductId IS NULL
			OR @ProductId < 1
		BEGIN
			SET @CommondType = 1

			-- Insert into Product table
			INSERT INTO Product (
				DescriptionUI
				,SalesUnit
				--,ApprovalFlag
				,SAPProductId
				,BaseMaterial
				,NetValue
				,Vat
				,PackSize
				,ModifiedBy
				,ModifiedDate
				)
			VALUES (
				@SAPProductName
				--,@SalesUnit
				,CASE @SalesUnit
					WHEN 'CON'
						THEN 'PKT'
					ELSE @SalesUnit
					END
				--,@ApprovalFlag
				,@SAPProductId
				,@BaseMaterial
				,@NetValue
				,@Vat
				,@PackSize
				,@ModifiedBy
				,@ModifiedDate
				)

			SELECT @ProductId = SCOPE_IDENTITY()
		END
		ELSE
		BEGIN
			SET @CommondType = 2

			UPDATE Product
			SET DescriptionUI = @SAPProductName
				--,SalesUnit = @SalesUnit
				,SalesUnit = CASE @SalesUnit
					WHEN 'CON'
						THEN 'PKT'
					ELSE @SalesUnit
					END
				--,ApprovalFlag = @ApprovalFlag
				,SAPProductId = @SAPProductId
				,BaseMaterial = @BaseMaterial
				,NetValue = @NetValue
				,Vat = @Vat
				,PackSize = @PackSize
				,ModifiedBy = @ModifiedBy
				,ModifiedDate = Getdate()
			WHERE ProductId = @ProductId

			SELECT @ProductResult = @@ROWCOUNT
		END

		-- Insert into IDXCustomerProduct table 
		IF @ProductId > 0
		BEGIN
			IF EXISTS (
					SELECT IDXId
					FROM IDXCustomerProduct
					WHERE CustomerId = @CustomerId
						AND ProductId = @ProductId
					)
			BEGIN
				--Update IDXCustomerProduct if Product is already exists
				UPDATE IDXCustomerProduct
				SET ModifiedBy = @ModifiedBy
					,ModifiedDate = Getdate()
				WHERE ProductId = @ProductId
					AND CustomerId = @CustomerId

				SELECT @IDXCustomerProductResult = @@ROWCOUNT
			END
			ELSE
			BEGIN
				INSERT INTO IDXCustomerProduct (
					ProductId
					,CustomerId
					,ModifiedBy
					,ModifiedDate
					)
				VALUES (
					@ProductId
					,@CustomerId
					,@ModifiedBy
					,GETDATE()
					)

				SELECT @IDXCustomerProductResult = SCOPE_IDENTITY()
			END
		END

		IF @CommondType = 1
			AND @ProductId > 0
			AND @IDXCustomerProductResult > 0
		BEGIN
			SET @Result = 'Data inserted successfully'
		END
		ELSE IF @CommondType = 2
			AND @ProductResult > 0
			AND @IDXCustomerProductResult > 0
		BEGIN
			SET @Result = 'Data updated successfully'
		END
		ELSE
		BEGIN
			SET @ProductId = - 1
			SET @Result = 'Failed to save data'
		END

		SELECT @ProductId ProductId
			,@Result Result
			,@CommondType CommondType

		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: SaveSAPCustomerProduct' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @ModifiedBy
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage

		SELECT @ProductId ProductId
			,@Result Result
			,@CommondType CommondType
	END CATCH;
END
