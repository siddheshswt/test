﻿

-- =============================================

-- Author:		Arvind Nishad

-- Create date: 2015-04-29

-- Description:	Get Note List

-- =============================================

--exec GetNoteList 20132,6326 

-- =============================================



--	     Modified Date			Modified By			Purpose

--	1.  06-May-2015				Siddhesh Sawant		Display notes for Patient,Order,Prescription and Carehome
--  2.  28-Aug-2015				Siddhesh Sawant		added note.NoteType = 20133 condition for order notes
--  3.  02-May-2016				Prafull Mohite		added note.DisplayAsAlert into notes result 
--  4.  06-July-2016			Siddhesh Sawant		Added sample order note id 20261 

-- =============================================

/*

20132	Patient Note

20133	Order Note

20134	Prescription Note

20135	CareHome Note

20261	Sample Order Note
*/

CREATE PROCEDURE [dbo].[GetNoteList]

	-- Add the parameters for the stored procedure here

	@NoteType BIGINT 

	,@NoteTypeId BIGINT

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	--SELECT NoteId

	--	,NoteType

	--	,NoteTypeId

	--	,NoteText

	--	,users.UserName CreatedBy

	--	,CreatedDate

	--FROM Notes note WITH (NOLOCK)

	--inner join Users users on users.ModifiedBy=note.CreatedBy

	--WHERE NoteType = @NoteType

	--	AND NoteTypeId = @NoteTypeId

	--	order by CreatedDate desc

		

	IF(@NoteType = 20135)

	BEGIN		

			SELECT   note.NoteId,

					 note.NoteType,

					 note.NoteTypeId,

					 CAST(note.NoteText AS NVARCHAR(MAX)) AS NoteText, 		 

					 (SELECT UserName FROM Users U (NOLOCK) WHERE U.UserId = note.CreatedBy) AS CreatedBy, 		 

					 note.CreatedDate,

					 note.DisplayAsAlert FROM 

					 Notes note (NOLOCK) WHERE note.NoteType = @NoteType AND note.NoteTypeId = @NoteTypeId ORDER BY CreatedDate DESC					

	END

	ELSE

	BEGIN			

			WITH NoteDetails(NoteId,NoteType,NoteTypeId,NoteText,CreatedBy,CreatedDate,DisplayAsAlert)

			AS

			(			

			SELECT   note.NoteId,

					 note.NoteType,

					 note.NoteTypeId,

					 CAST(note.NoteText AS NVARCHAR(MAX)) AS NoteText, 		 

					 (SELECT UserName FROM Users U (NOLOCK) WHERE U.UserId = note.CreatedBy) AS CreatedBy, 		 

					 note.CreatedDate,

					 note.DisplayAsAlert FROM 

					 Notes note (NOLOCK) WHERE note.NoteType in (20132,20134,20261) AND note.NoteTypeId = @NoteTypeId		



			UNION ALL

						

			SELECT  note.NoteId,

					20133 AS NoteType,

					note.NoteTypeId, 

					CAST(note.NoteText AS NVARCHAR(MAX)) AS NoteText, 	

					(SELECT UserName FROM Users U (NOLOCK) WHERE U.UserId = note.CreatedBy) AS CreatedBy, 		

					note.CreatedDate,

					note.DisplayAsAlert FROM Notes note (NOLOCK) INNER JOIN Orders ord (NOLOCK) 

					ON note.NoteTypeId = ord.OrderId WHERE ord.PatientId in (@NoteTypeId) and note.NoteType = 20133			

			)


			SELECT NoteId,NoteType,NoteTypeId,NoteText,CreatedBy,CreatedDate,DisplayAsAlert FROM NoteDetails ORDER BY CreatedDate DESC						

	END		

END
