﻿-- =============================================

-- Author:		
-- Create date: 
-- Description:	Get key value pair

-- =============================================
-- =============================================
--	     Modified Date			Modified By			Purpose

--	1.  04-10-2016				Siddhesh Sawant		Added transaction and try catch block
--	2.  05-10-2016				Siddhesh Sawant		Added comments for CAST



-- =============================================
CREATE proc [dbo].[SaveCustomerParameter]
(
@CustomerId BigInt
)
AS BEGIN
	
	DECLARE @Result VARCHAR(200)
	DECLARE @ResultMessage VARCHAR(200)
	DECLARE @CurrentDate DATETIME = GETDATE()
	DECLARE @ModifiedBy VARCHAR(100) = CAST(NEWID() AS VARCHAR(100))

	BEGIN TRY
		BEGIN TRANSACTION
			
			INSERT INTO [dbo].[CustomerParameter]
           (
           [CustomerID]
           ,[OrderCreationAllowed]
           ,[OrderApproval]
           ,[OrderLeadTime]
           ,[OrderCutOffTime]
           ,[CheckAge]
           ,[IsNHSIDMandatory]
           ,[IsADPMandatory]
           ,[IsTelephoneMandatory]
           ,[IsNextAssessmentDateMandatory]
           ,[MaxPadPerDay]
           ,[NextAssessmentDate]
          
           ,[AllowIncreaseOrderQty]
           ,[IsCustomerApprovalReq]
          
           ,[Age]
           ,[IsOrderAllowed]
           ,[ModifiedBy]
           ,[ModifiedDate])
     VALUES
           (
		  
		   @CustomerId,
		   1,
		   1,
		   7,
		   '2014-11-10 15:00:00.000',
		   1,
		   1,
		   1,
		   1,
		   1,
		   10,
		   0,
		
		 
		   1,
		   1,
		   
		   null,
		   1,
		   'A4ACF75F-2E97-41CE-910F-484A8710FAFD',
		   Getdate()
		   )

		COMMIT
	END TRY
	BEGIN CATCH
	ROLLBACK TRANSACTION

	SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

	SET @ResultMessage = 'sp: SaveCustomerParameter' +' '+ ISNULL(@ResultMessage,'')

	EXEC SaveExceptionLog @UserID = @ModifiedBy
		,@TimeStamp = @CurrentDate
		,@ErrorMessage = @ResultMessage

	END CATCH
END

