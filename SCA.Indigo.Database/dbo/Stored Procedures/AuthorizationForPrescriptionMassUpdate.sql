﻿
/******************************
** File:    AuthorizationForPrescriptionMassUpdate
** Name:	AuthorizationForPrescriptionMassUpdate
** Desc:	Authorize prescriptions after Mass Update
** Auth:	Sagar/Mamatha
** Date:	02/07/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** 1	04-10-2016		Siddhesh	Added trnasaction and try catch block	-----------------------------------
		
*******************************/

 -- ===============================================================================
 --exec AuthorizationForPrescriptionMassUpdate 142,142

CREATE PROCEDURE [dbo].[AuthorizationForPrescriptionMassUpdate] -- Add the parameters for the stored procedure here
		@PrescriptionId BIGINT = 0,
		@PatientId BIGINT = 0
		
			
AS BEGIN 

	BEGIN TRY		
-- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.

			SET NOCOUNT ON;
			DECLARE @Result VARCHAR(200)
			DECLARE @ResultMessage VARCHAR(200)
			DECLARE @CurrentDate DATETIME = getdate()
			DECLARE @ModifiedBy VARCHAR(100) =  CAST(NEWID() AS VARCHAR(100))
			DECLARE @isApprovalRequired BIT,
			@MaxPPD DECIMAL(18,2), 
			@isSentToApproval BIT = 0, 
			@customerId BIGINT
			BEGIN TRANSACTION
			IF (SELECT count(*) FROM Prescription WHERE PrescriptionId =  @PrescriptionId and IsMassChange = 1) > 0
			BEGIN

					SELECT TOP 1 @customerId = cp.CustomerID, 
								 @MaxPPD = cp.MaxPadPerDay, 
								 @isApprovalRequired= cp.IsCustomerApprovalReq 
								 FROM 
								 CustomerParameter cp 
								 WHERE cp.customerId = (SELECT CustomerID FROM Patient WHERE PatientId = @PatientId)

					IF (SELECT SUM(ActPPD) FROM IDXPrescriptionProduct WHERE IsRemoved <> 1 
																		AND Status = 'Active' 
																		AND PrescriptionId = @PrescriptionId) > @MaxPPD OR @isApprovalRequired = 1
					BEGIN
						UPDATE IDXPrescriptionProduct 
								SET PrescriptionAuthorizationApprovalFlag = 1 
								WHERE IsRemoved <> 1 
								AND Status = 'Active' 
								AND PrescriptionId = @PrescriptionId
						SET @isSentToApproval = 1
					END

					--begin tran

					 UPDATE IDXPrescriptionProduct SET PrescriptionAuthorizationApprovalFlag = 1
						WHERE IDXPrescriptionProductId IN (
							SELECT pp.IDXPrescriptionProductId FROM IDXPrescriptionProduct   pp
							INNER JOIN IDXCustomerProduct cp ON cp.ProductId = pp.ProductId AND cp.CustomerId = @customerId
							WHERE pp.IsRemoved <> 1 AND pp.Status = 'Active' AND pp.PrescriptionId = @PrescriptionId
							AND cp.IsApporvalRequired = 1
					)

					IF(@@ROWCOUNT > 0)
					BEGIN
						set @isSentToApproval = 1
					END

					--rollback


					IF @isSentToApproval = 1
					UPDATE Patient SET PatientStatus = 10021 , ReasonCodeID = 10070
					WHERE PatientId = @PatientId

					UPDATE Prescription SET IsMassChange = 0 WHERE PrescriptionId = @PrescriptionId
					
			END
			COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		EXEC SaveExceptionLog @UserID = @ModifiedBy
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage		
				
	END CATCH
END



