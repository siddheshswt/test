﻿/*
===============================================================================
 Author      :	 Saurabh Mayekar
 Create date :   28-12-2015
 Description :	 Gets Users For Interaction Report
===============================================================================

===============================================================================
		Modified Date		Modified By			Purpose
	1.	
===============================================================================
EXEC GetUsers

*/

--EXEC GetInteractionReportUsers '9'

CREATE PROC [dbo].[GetInteractionReportUsers]
(
	@CustomerId		VARCHAR(4000)
)
AS
BEGIN

SELECT DISTINCT UserName , UserId
FROM       Users(NOLOCK) users   
INNER JOIN Interaction(NOLOCK) inetraction
ON  users.UserId = inetraction.CreatedBy 
WHERE inetraction.CustomerId in (SELECT Value FROM SplitToTable(ISNULL(@CustomerId,'')))
ORDER BY UserName

END