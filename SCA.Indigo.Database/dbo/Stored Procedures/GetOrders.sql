﻿

-- =============================================
-- Author:		<Ansari Atif>
-- Create date: <12-09-2014>
-- Description:	<Procedure to Display Orders w.r.t Orders in Popup e.g exec GetOrders_usp 1,1>
-- Parameters:	<@OrderType=1 for Residential Activated Orders,@OrderType=1 for Community Order>
-- =============================================

-- ======================================================================
--		Modified Date			Modified By			Purpose
--	1.   19-Mar-2015			Sagar Yerva			Added NOLOCK
-- =======================================================================

--exec GetOrders 1


CREATE PROCEDURE [dbo].[GetOrders]
(
-- Add the parameters for the stored procedure here
@PatientId as bigint
)
As
Begin
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

--Getting Residential Activated  

SELECT        Patient.SAPPatientNumber PatientId, (PersonalInformation.FirstName+' '+PersonalInformation.LastName) PatientName, List.DefaultText PatientType,
              --Product.ProductDisplayId ProductId,
			  --Orders.Quantity, 
			  Product.SalesUnit,Patient.DeliveryDate, 
              Orders.OrderStatus,Patient.DeliveryFrequency DeliveryFrequency
			 -- Product.Description Description
FROM          IDXOrderProduct INNER JOIN
              Orders (NOLOCK) ON IDXOrderProduct.OrderId = Orders.OrderId INNER JOIN
              Product (NOLOCK) ON IDXOrderProduct.ProductId = Product.ProductId INNER JOIN
              IDXPatientOrder (NOLOCK) ON Orders.OrderId = IDXPatientOrder.OrderID INNER JOIN
              Patient (NOLOCK) INNER JOIN
              PersonalInformation (NOLOCK) ON Patient.PersonalInformationId = PersonalInformation.PersonalInformationId ON IDXPatientOrder.PatientID = Patient.PatientId INNER JOIN
              List (NOLOCK) ON Patient.PatientType = List.ListId WHERE Patient.PatientId=@PatientId
END




