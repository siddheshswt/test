﻿

-- =============================================
-- Author:		Arvind Nishad
-- Create date: 24-03-2015
-- Description:	Insert into ExceptionLog
-- =============================================
/******************************
********************************
** Change History
********************************
** PR	Date			Author		Description	
** 1	04-10-2016		Siddhesh	Added trnasaction and try catch block	
		
*******************************/
/*
Declare @CurrentDate datetime=getdate()
exec SaveExceptionLog @UserID='ECDBCD8F-145A-411A-9FBD-98CC5D2F0786' ,@TimeStamp=@CurrentDate, @ErrorMessage='SAP Exception test'
select * from ExceptionLog
*/
CREATE PROCEDURE [dbo].[SaveExceptionLog] (
	@UserID varchar(100),
	@TimeStamp datetime,
	@ErrorMessage varchar(8000)
	)
AS
BEGIN
	DECLARE @Result VARCHAR(200)
	DECLARE @ResultMessage VARCHAR(200)
	DECLARE @CurrentDate DATETIME = GETDATE()	
	BEGIN TRY
		BEGIN TRANSACTION
			INSERT INTO ExceptionLog
			(
			UserID,
			ErrorMessage,
			TimeStamp,
			ModifiedBy,
			ModifiedDate
			)
			values
			(
			@UserID,
			@ErrorMessage,
			GetDate(),
			@UserID,
			Getdate()
			)
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: SaveExceptionLog' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @UserID
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage

	END CATCH
END

