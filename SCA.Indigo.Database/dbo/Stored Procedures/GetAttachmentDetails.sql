﻿-- =============================================
-- Author:		Gaurav Lad
-- Create date: 5th May 2015
-- Description:	Fetch the Attachment Details based on TypeId and Attachment Type
-- =============================================
CREATE PROCEDURE [dbo].[GetAttachmentDetails]
	-- Add the parameters for the stored procedure here
	@TypeId  BIGINT,
	@AttachmentType  BIGINT
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT U.UserName AS CreatedBy ,A.AttachmentId AS AttachmentId ,A.FileName AS FileName, A.CreatedDate AS AttachmentDate, A.AttachmentLocation As AttachmentLocation from Attachment A INNER JOIN Users U
	ON a.CreatedBy = U.UserId
	WHERE A.TypeId = @TypeId AND A.AttachmentType = @AttachmentType

END

