﻿
/******************************
** File:    GetCareHomePatientList
** Name:	GetCareHomePatientList
** Desc:	Get Carehome Patient List
** Auth:	Arvind
** Date:	13/04/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1   11/06/2015		Arvind		Added Customer Name
**	2	15/06/2015		Sagar		Added Document Comment		
**	3   16/06/2015		Arvind		changed  idxPrescriptionProduct.DeliveryDate to patient.NextDeliveryDate
**	4   10/11/2016		Siddhesh	changed column name DQ1 to 4 To Del1 to 4 in idxPrescriptionProduct table
*******************************/

--===========================================================================
--exec GetCareHomePatientList 46

CREATE PROCEDURE [dbo].[GetCareHomePatientList] 
	-- Add the parameters for the stored procedure here
	@CareHomeId BigInt = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
SELECT patient.PatientId PatientId
		,customer.SAPCustomerNumber
		,patient.SAPPatientNumber SAPPatientNumber
		,patientStatus.DefaultText PatientStatus
		,patient.PatientStatus PatientStatusCode
		,pInfo.FirstName FirstName
		,pInfo.LastName LastName
		,idxPrescriptionProduct.NextDeliveryDate DeliveryDate		
		--,patient.NextDeliveryDate DeliveryDate		
		,patientType.Description PatientType
		,patient.DeliveryFrequency DeliveryFrequency		
		,prod.BaseMaterial SAPProductID
		,prod.DescriptionUI ProductName
		,prod.BaseMaterial BaseMaterial
		,IDXPrescriptionProduct.Frequency
		,case when IDXPrescriptionProduct.FlagDQ1=1 then IDXPrescriptionProduct.Del1 
		when IDXPrescriptionProduct.FlagDQ2=1 then IDXPrescriptionProduct.Del2 
		when IDXPrescriptionProduct.FlagDQ3=1 then IDXPrescriptionProduct.Del3 
		when IDXPrescriptionProduct.FlagDQ4=1 then IDXPrescriptionProduct.Del4 
		end as Quantity ,
		customerPersonalInfo.FirstName CustomerName
	FROM Patient patient WITH (NOLOCK)	
	LEFT OUTER JOIN PersonalInformation pInfo (NOLOCK) ON patient.PersonalInformationId = pInfo.PersonalInformationId	
	inner join Customer customer WITH (NOLOCK)	on customer.CustomerId=patient.CustomerId
	inner join PersonalInformation customerPersonalInfo WITH (NOLOCK) on customerPersonalInfo.PersonalInformationId=customer.PersonalInformationId
	inner JOIN CareHome CHome (NOLOCK) ON CHome.CareHomeId = patient.CareHomeId	
	inner join List patientType with(nolock)  on patientType.ListId=patient.PatientType
	inner join List patientStatus  with(nolock)  on patientStatus.ListId=patient.PatientStatus	
	left outer join Prescription prescription  with(nolock) on prescription.PatientId=Patient.PatientID
	left outer join IDXPrescriptionProduct idxPrescriptionProduct  with(nolock) on prescription.PrescriptionId=idxPrescriptionProduct.PrescriptionId	
	and idxPrescriptionProduct.Status='Active'
	left outer join Product prod on prod.ProductId=idxPrescriptionProduct.ProductId
	WHERE CHome.CareHomeId = @CareHomeId
	order by pInfo.LastName			
							
END
