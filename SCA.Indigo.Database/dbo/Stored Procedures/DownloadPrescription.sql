﻿/******************************
** File:    DownloadPrescription
** Name:	DownloadPrescription
** Desc:	Download the Prescription data of all the patients who are under provided Customer and Product
** Auth:	Sagar
** Date:	19/06/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1	20/07/2015		Sagar		Added CustomerId and SAPCustomerId in Output 
**	2   10/11/2016		Siddhesh	changed column name DQ1 to 4 To Del1 to 4 in idxPrescriptionProduct table
*******************************/


-- ==========================================================================================
--exec   [dbo].[DownloadPrescription] 9,160

CREATE PROCEDURE [dbo].[DownloadPrescription] 
		-- Add the parameters for the stored procedure here
	@CustomerId bigint, 
	@ProductId bigint 

	AS BEGIN 
	
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.

SET NOCOUNT ON;

 -- Insert statements for procedure here

SELECT p.PatientId  AS PatientInternalId,
	   p.SAPPatientNumber AS SAPPatientNumber,
	   cust.SAPCustomerNumber AS SAPCustomerId,
	   cust.CustomerId AS CustomerId,
       prod.SAPProductID AS SAPProductID,
	   prod.BaseMaterial AS BaseMaterial,
       prod.DescriptionUI AS DescriptionUI,
       idxprod.Frequency AS Frequency,
       idxprod.DeliveryDate AS DeliveryDate,
       idxprod.NextDeliveryDate AS NextDeliveryDate,
       idxprod.ValidFromDate AS ValidFromDate,
       idxprod.ValidToDate AS ValidToDate,
       idxprod.[Status] AS [Status],
       idxprod.AssessedPadsPerDay AS AssessedPadsPerDay,
       idxprod.ActPPD AS ActPPD,
       idxprod.IsOverridenFlag AS IsOverridenFlag,
       idxprod.Del1 AS DQ1,
       idxprod.Del2 AS DQ2,
       idxprod.Del3 AS DQ3,
       idxprod.Del4 AS DQ4,
       idxprod.FlagDQ1 AS FlagDQ1,
       idxprod.FlagDQ2 AS FlagDQ2,
       idxprod.FlagDQ3 AS FlagDQ3,
       idxprod.FlagDQ4 AS FlagDQ4,
	   idxprod.IDXPrescriptionProductId AS IDXPrescriptionProductId,
	   idxprod.PrescriptionId AS PrescriptionId
	   
FROM IDXPrescriptionProduct (nolock) idxprod
INNER JOIN Prescription (nolock) presc ON idxprod.PrescriptionId = presc.PrescriptionId
INNER JOIN Patient (nolock) p ON p.PatientId = presc.PatientId
INNER JOIN Product (nolock) prod ON idxprod.ProductId = prod.ProductId
INNER JOIN Customer (NOLOCK) cust ON p.CustomerId = cust.CustomerId
WHERE p.CustomerId = @CustomerId
  AND (idxprod.IsRemoved  = 0 OR idxprod.IsRemoved is null)
  AND
		(
				CASE
								WHEN  @ProductId = 0 THEN 1
								WHEN idxprod.ProductId = @ProductId THEN 1
								ELSE 0
				END = 1
		)  
	END 
