﻿-- =============================================
-- Author:		<Jagdish>
-- Create date: <28 May 2015>
-- Sample : GetProductsOnCustomerId '9,10'
-- =============================================
CREATE PROCEDURE [dbo].[GetProductsOnCustomerId]
@CustomerId VARCHAR(1000)
AS
BEGIN

	SELECT p.ProductId, p.DescriptionUI + ' ' +'('  + 	ISNULL(SUBSTRING(p.SAPProductID, PATINDEX('%[^0 ]%', p.SAPProductID + ' '), LEN(p.SAPProductID)),'') + ')' AS ProductName
	FROM Product p(nolock)
		INNER JOIN IDXCustomerProduct cp(nolock) ON p.ProductId = cp.ProductId 
	WHERE ',' + @CustomerId + ',' LIKE '%,' + CAST(cp.CustomerId AS varchar) + ',%'

END






