﻿

/******************************
** File:    GetSAPOrderItemDetails
** Name:	GetSAPOrderItemDetails
** Desc:	Get SAP Order Item Details
** Auth:	Arvind
** Date:	29/12/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1   19/03/2015		Sagar		Added NOLOCK
--  2   09/04/2015		Siddhesh	Calculate NDD for products before sending to SAP
--  3   20/05/2015		Siddhesh	procedure will check for nonselfcare patient's cancelled order to 
--									consider product quantity = 0
**	4	15/06/2015		Sagar		Added Document Comment		
**	5	15/06/2015		Siddhesh	Added frequency case for ZHDH order type	
--  6	21/07/2016		Siddhesh	Added ACTUAL PPD and Assessed PPD		
--  7	23/11/2016		Siddhesh	Added DerivedNDD column for product ndd calculation
--  8	25/11/2016		Siddhesh	Added condition to exclude one off standard orders from NDD calculation
*******************************/

-- ==============================================================================================
-- exec [GetSAPOrderItemDetails] 2453

CREATE Proc [dbo].[GetSAPOrderItemDetails]
@OrderId BIGINT

AS BEGIN
DECLARE @OrderStatus bigint = 0
DECLARE @IsNonselfcare bit = 0
DECLARE @ProductNotInPrescription VARCHAR(5) = '-1'
DECLARE @Empty VARCHAR(10)= ''
DECLARE @CommunityOneOffStandard BIGINT = 102
DECLARE @CarehomePatientOneOffStandard BIGINT = 202
DECLARE @CarehomeOneOffStandard BIGINT = 205

SELECT @OrderStatus= ord.OrderStatus,
		@IsNonselfcare = 1
FROM  orders ord INNER JOIN Patient pt ON ord.patientId = pt.PatientId 
WHERE ord.orderid = @OrderId
AND pt.PatientType in (10010,10012,10014,10016,10018) and ord.OrderStatus = 10105

print @OrderStatus
print @IsNonselfcare
SELECT 	
		idxPrescriptionProduct.IDXPrescriptionProductId,
		product.BaseMaterial,
		idxorder.Quantity,
		CASE WHEN product.SalesUnit='PKT' 
				THEN 'CON' 
			 ELSE product.SalesUnit 
		END SalesUnit,
						--case when ord.ArrangedDate is null then convert(varchar, ord.SAPOrderDeliveryDate , 112)
						--else  convert(varchar, ord.ArrangedDate , 112) end as DeliveryDate,
							
		CONVERT(VARCHAR, ord.SAPOrderDeliveryDate , 112) AS DeliveryDate,	

		ord.SAPOrderDeliveryDate,
	    ord.DerivedNDD,						

		CAST(idxorder.IsFOC AS VARCHAR(50)) ITEMFOC,	

						--(select min(DeliveryFrequency) from Patient where PatientId in (ord.PatientID)) Frequency,
						--case when ord.OrderType=10053 then '' else convert(varchar, ord.SAPOrderDeliveryDate  , 112) 	 end as NDD-- This for order other than ZHDP

		CASE WHEN IDXPrescriptionProduct.Frequency IS NULL 
				THEN @ProductNotInPrescription 
			 ELSE CAST(IDXPrescriptionProduct.Frequency AS VARCHAR(10))
		END Frequency,
		CAST(idxPrescriptionProduct.ActPPD AS VARCHAR(10)) as ACPPD,
		CAST(IDXPrescriptionProduct.AssessedPadsPerDay AS VARCHAR(10)) as ASPPD, 
		--CASE WHEN ord.OrderType=10053 AND IDXPrescriptionProduct.Frequency IS NOT NULL  
		--		THEN @Empty												-- order is ZHDP and product in prescription
		--	 WHEN ord.OrderType=10053 AND IDXPrescriptionProduct.Frequency IS NULL  
		--		THEN CONVERT(VARCHAR, ord.DerivedNDD, 112)	-- order is ZHDP and product not in prescription 
  --          WHEN ord.OrderType=20158 AND IDXPrescriptionProduct.Frequency IS NOT NULL  
		--		THEN @Empty												-- This is not possible. Frequency will be always null. order is ZHDH and product in prescription
		--	 WHEN ord.OrderType=20158 AND IDXPrescriptionProduct.Frequency IS NULL  
		--		THEN CONVERT(VARCHAR, ord.DerivedNDD, 112)	-- order is ZHDH and product not in prescription 
		--	ELSE CONVERT(VARCHAR, ord.DerivedNDD  , 112) 	 
		--END AS NDD		
														-- This for order other than ZHDP

		CASE WHEN ord.OrderType=10053 AND IDXPrescriptionProduct.Frequency IS NOT NULL AND ord.OrderCreationType NOT IN(@CommunityOneOffStandard, @CarehomePatientOneOffStandard) 
				THEN @Empty												-- order is ZHDP but not one off standard and product in prescription
			 WHEN ord.OrderType=10053 AND IDXPrescriptionProduct.Frequency IS NULL  
				THEN CONVERT(VARCHAR, ord.DerivedNDD, 112)	-- order is ZHDP and product not in prescription 
            WHEN ord.OrderType=20158 AND IDXPrescriptionProduct.Frequency IS NOT NULL AND ord.OrderCreationType NOT IN(@CarehomeOneOffStandard)  
				THEN @Empty												-- This is not possible. Frequency will be always null. order is ZHDH and product in prescription
			 WHEN ord.OrderType=20158 AND IDXPrescriptionProduct.Frequency IS NULL  
				THEN CONVERT(VARCHAR, ord.DerivedNDD, 112)	-- order is ZHDH and product not in prescription 
			ELSE 
				CONVERT(VARCHAR, ord.DerivedNDD  , 112) 	-- This for order other than ZHDP & ZHDP (community, carehome patien one off standard) & ZHDH (carehome one off standard) 
		END AS NDD														
	 
FROM IDXOrderProduct idxorder WITH(NOLOCK)
INNER JOIN Product product on idxorder.ProductId=product.ProductId
INNER JOIN Orders ord on ord.OrderId=idxorder.OrderId
LEFT OUTER JOIN Prescription prescription on prescription.PatientId=ord.PatientID
LEFT OUTER JOIN IDXPrescriptionProduct idxPrescriptionProduct on prescription.PrescriptionId=idxPrescriptionProduct.PrescriptionId
AND	idxorder.ProductId = idxPrescriptionProduct.ProductId
AND idxPrescriptionProduct.Status='Active'
--WHERE idxorder.OrderID=@OrderId AND idxorder.Quantity>0  AND idxorder.IsRemoved = 0 
WHERE idxorder.OrderID=@OrderId AND (
(@OrderStatus = 10105 and @IsNonselfcare = 1 and idxorder.Quantity = 0)
OR (idxorder.Quantity > 0 AND idxorder.IsRemoved = 0 ))

END



