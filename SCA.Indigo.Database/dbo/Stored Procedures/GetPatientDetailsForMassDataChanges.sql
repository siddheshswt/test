﻿/******************************
** File:    GetPatientDetailsForMassDataChanges
** Name:	GetPatientDetailsForMassDataChanges
** Desc:	Get Patient's Details For Mass Data Changes
** Auth:	Arvind Nishad
** Date:	01/07/2015

********************************
** Change History
********************************
** PR	Date			Author			Description	
** --	--------		-------			-----------------------------------
** 1	02/07/2015		Arvind Nishad	Get Patient List for Mass Upload
*****************************************************************************/

--============================================================================
-- exec GetPatientDetailsForMassDataChanges '9', 10010, 0, '','1'
-- exec GetPatientDetailsForMassDataChanges '9', '0','','0001150901,150983','1'
-- exec GetPatientDetailsForMassDataChanges '9', '0','0','','1'

CREATE PROC [dbo].[GetPatientDetailsForMassDataChanges]
	@CustomerId BIGINT,
	@PatientType VARCHAR(max) = NULL,
	@PatientStatus VARCHAR(max) = NULL,
	@PatientIds VARCHAR(max) = NULL,
	@LanguageId BIGINT=NULL
AS
BEGIN
--set @LanguageId=1
			SELECT 
				cast(patient.LoadId as varchar(50)) LoadId
				,cast(patient.CustomerId as varchar(50)) CustomerId
				,customer.SAPCustomerNumber SapCustomerId
				,cast(patient.PatientId as varchar(50)) PatientId
				,patient.SAPPatientNumber AS PatientSAPID				 
				,patientPersonalInfo.FirstName 
				,patientPersonalInfo.LastName 
				,grnderLanguage.TranslationType AS Gender 				 
				,TitleLanguage.TranslationType AS Title 				
				,CONVERT(VARCHAR, patient.DateofBirth , 103) DateofBirth 		 
				,cast(patient.CareHomeId  as varchar(50)) CareHomeId 				
				,patientAddress.AddressLine1 AS AddressLine1 
				,patientAddress.AddressLine2 AS AddressLine2 
				,patientAddress.City AS City 
				,patientAddress.County AS County 
				,patientAddress.Country AS Country
				,patientAddress.HouseName AS HouseName 
				,patientAddress.HouseNumber AS HouseNumber 
				,patientAddress.PostCode AS PostCode 

				,deliveryAddress.AddressLine1 AS DeliveryAddressLine1 
				,deliveryAddress.AddressLine2 AS DeliveryAddressLine2 
				,deliveryAddress.City AS DeliveryCity 
				,deliveryAddress.County AS DeliveryCounty 
				,deliveryAddress.Country AS DeliveryCountry
				,deliveryAddress.HouseName AS DeliveryHouseName 
				,deliveryAddress.HouseNumber AS DeliveryHouseNumber 
				,deliveryAddress.PostCode AS DeliveryPostCode 				

				,patientPersonalInfo.TelephoneNumber
				,patientPersonalInfo.MobileNumber
				,patientPersonalInfo.Email
				,case when patient.IsDataProtected =0 then 'N' else 'Y' end as IsDataProtected
				,patient.DeliveryFrequency
				,patient.Round
				,patient.RoundId
				,patient.ADP1
				,patient.ADP2
				,patientStatusLanguage.Description AS PatientStatus 						
				,reasonCodeLanguage.Description AS ReasonCode 				
				,patientTypeLanguage.Description AS PatientType 			
				,patient.NHSId 
				,patient.LocalId
				,CONVERT(VARCHAR, patient.AssessmentDate , 103) AssessmentDate 
				,CONVERT(VARCHAR, patient.NextAssessmentDate , 103) NextAssessmentDate 
				,CONVERT(VARCHAR, patient.DeliveryDate , 103) DeliveryDate 
				,CONVERT(VARCHAR, patient.NextDeliveryDate , 103) NextDeliveryDate 				
				,'Y' IsSentToSAP -- Send always to sap when ever there is change in patient details				
				,cast(patient.BillTo as varchar(50)) BillTo
				,patient.Remarks

				FROM Patient patient(NOLOCK)  	
					inner join Customer customer(NOLOCK) on customer.CustomerId=patient.CustomerId
					Inner JOIN PersonalInformation patientPersonalInfo(NOLOCK) ON patient.PersonalInformationId = patientPersonalInfo.PersonalInformationId  
					Inner JOIN List genderList(NOLOCK) ON patientPersonalInfo.Gender = genderList.ListId
					LEFT JOIN Translation grnderLanguage(NOLOCK) ON grnderLanguage.TransProxyId = genderList.TransProxyID AND grnderLanguage.LanguageID =@LanguageId
					LEFT JOIN List TitleIdList (NOLOCK) ON patientPersonalInfo.TitleId = TitleIdList.ListId
					LEFT JOIN Translation TitleLanguage(NOLOCK) ON TitleIdList.TransProxyId = TitleLanguage.TransProxyID AND TitleLanguage.LanguageID =@LanguageId
					LEFT JOIN List patientType(NOLOCK) ON patient.PatientType = patientType.ListId
					LEFT JOIN Translation patientTypeLanguage(NOLOCK) ON patientType.TransProxyId = patientTypeLanguage.TransProxyID AND patientTypeLanguage.LanguageID =@LanguageId
					LEFT JOIN List patientStatus(NOLOCK) ON patient.PatientStatus = patientStatus.ListId
					LEFT JOIN Translation patientStatusLanguage(NOLOCK) ON patientStatus.TransProxyId = patientStatusLanguage.TransProxyID AND patientStatusLanguage.LanguageID =@LanguageId
					LEFT OUTER JOIN List reasonCodeList(NOLOCK) ON patient.ReasonCodeID = reasonCodeList.ListId
					LEFT OUTER JOIN Translation reasonCodeLanguage(NOLOCK) ON reasonCodeList.TransProxyId = reasonCodeLanguage.TransProxyID AND reasonCodeLanguage.LanguageID =@LanguageId
					LEFT JOIN [Address] patientAddress(NOLOCK) ON patientPersonalInfo.AddressId = patientAddress.AddressId  					
					LEFT JOIN [Address] deliveryAddress(NOLOCK) ON patientPersonalInfo.CorrespondenceAddressId =deliveryAddress.AddressId  					
				WHERE patient.CustomerId = @CustomerId  
				AND
				(@PatientType='' OR @PatientType IS NULL OR patient.PatientType IN (select Value from dbo.SplitToTable(@PatientType)))	
				AND
				(@PatientStatus = '' OR @PatientStatus IS NULL OR patient.PatientStatus IN (select Value from dbo.SplitToTable(@PatientStatus)))	
				AND
				(@PatientIds ='' OR @PatientIds IS NULL OR patient.SAPPatientNumber IN (select Value from dbo.SplitToTable(@PatientIds)))	
		
END




