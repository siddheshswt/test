﻿

CREATE PROCEDURE [dbo].[XSCAUpdatePatientNullNdd] 
as
BEGIN
BEGIN TRY
              
BEGIN TRAN --OPEN TRANSACTION

	   DECLARE @Patientid		 bigint
	   DECLARE @NDD				 Date

	   
       DECLARE cursor_updatePatientNullNdd CURSOR -- Declare cursor
       LOCAL SCROLL STATIC 
       FOR                          
			select pat.patientid,MIN(pro.NextDeliveryDate) NND
			from patient pat
				inner join PersonalInformation per with (nolock) on per.PersonalInformationId = pat.PersonalInformationId 
				inner join prescription pre with (nolock) on pre.patientid = pat.patientid
				left outer join idxprescriptionproduct pro with (nolock) on pro.prescriptionid = pre.prescriptionid
			where pat.NextDeliveryDate is null and pro.Status = 'Active' and pro.NextDeliveryDate is not null
			group by pat.patientid
                     
              OPEN cursor_updatePatientNullNdd
              FETCH NEXT FROM cursor_updatePatientNullNdd
              INTO @Patientid,@NDD

              WHILE @@FETCH_STATUS = 0
              BEGIN                                 
					 UPDATE Patient 
					    SET NextDeliveryDate  = @NDD
					  WHERE PatientId		  = @Patientid
                     

                     FETCH NEXT FROM cursor_updatePatientNullNdd
                     INTO @Patientid,@NDD  
              END

       CLOSE cursor_updatePatientNullNdd -- close the cursor
       DEALLOCATE cursor_updatePatientNullNdd -- Deallocate the cursor 

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE UPDATING Patient Null NDD'

END CATCH
END




