﻿-- =============================================
-- Author:		<Jagdish>
-- Create date: <22 June 2015>
-- Description:	<Return ContactPerson table data>
-- Sample : GetCommunicationFormatForMassDataChanges 9,2,'',1 
-- Sample : GetCommunicationFormatForMassDataChanges '9',3 
-- =============================================
CREATE PROCEDURE [dbo].[GetCommunicationFormatForMassDataChanges]
	@customerId BIGINT,
	@type BIGINT,	
	@sapIds VARCHAR(max) = NULL,
	@languageId bigint = 1
AS
BEGIN
	SET NOCOUNT ON;
	-- Type >> 2 >> Patient, 3 >> Carehome
	
	--declare @type varchar(10)
	--declare @customerId BIGINT
	--declare @sapIds VARCHAR(max) = NULL	
	--SET @customerId = 9
	--SET @type = 2
	--SET @sapIds  = '000000030330,0001150901,000000150983,000000150985'
	--set @type = 3
	--set @sapIds ='0000893572,000123456789,0000858352'

	declare @commonId varchar(5)
	set @commonId = ''

  IF(@type = 2)
	BEGIN
		IF (isnull(@sapIds,'') = '' OR LEN(@sapIds) = 0)
		BEGIN		
			SELECT 
			isnull(t.SAPCustomerNumber, t1.SAPCustomerNumber) as CustomerId
			, isnull(t.PatientId, t1.PatientId) as IndigoPatientId
			, @commonId as IndigoCarehomeId
			, t.CommunicationPreference as CommunicationPreference
			, t1.MarkettingPreference as MarkettingPreference
			, ISNULL(t.Remarks,'') + '|' + ISNULL(t1.Remarks,'') AS Remarks
			from  ((
					SELECT 
							c.SAPCustomerNumber
							,p.PatientId                      
							,tr.TranslationType as CommunicationPreference
							,ip.Remarks
					FROM IDXPreference ip  (NOLOCK)           
					INNER JOIN Patient P (NOLOCK) ON ip.PatientId = p.PatientId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on p.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20145  and p.CustomerId = @customerId) as t 
			FULL OUTER JOIN (               
					SELECT 
							c.SAPCustomerNumber
							,p.PatientId 
							,tr.TranslationType as MarkettingPreference                          
							,ip.Remarks
					FROM IDXPreference ip  (NOLOCK)           
					INNER JOIN Patient P (NOLOCK) ON ip.PatientId = p.PatientId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on p.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20146 and p.CustomerId = @customerId) as t1 on t.PatientId = t1.PatientId) 
					WHERE t1.PatientId in ( SELECT PatientId FROM Patient (NOLOCK) WHERE CustomerId = @customerId )				
		END
		ELSE
		BEGIN		
			SELECT 
			isnull(t.SAPCustomerNumber, t1.SAPCustomerNumber) as CustomerId
			, isnull(t.PatientId, t1.PatientId) as IndigoPatientId   
			, @commonId as IndigoCarehomeId
			, t.CommunicationPreference as CommunicationPreference
			, t1.MarkettingPreference as MarkettingPreference
			, ISNULL(t.Remarks,'') + '|' + ISNULL(t1.Remarks,'') AS Remarks
			from  ((
					SELECT 
							 c.SAPCustomerNumber
							,p.PatientId                      
							,tr.TranslationType as CommunicationPreference
							,ip.Remarks
					FROM IDXPreference ip  (NOLOCK)           
					INNER JOIN Patient P (NOLOCK) ON ip.PatientId = p.PatientId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on p.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20145  and p.CustomerId = @customerId) as t 
			FULL OUTER JOIN (               
					SELECT 
							 c.SAPCustomerNumber
							,p.PatientId 
							,tr.TranslationType as MarkettingPreference   
							,ip.Remarks							                       
					FROM IDXPreference ip  (NOLOCK)           
					INNER JOIN Patient P (NOLOCK) ON ip.PatientId = p.PatientId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on p.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20146 and p.CustomerId = @customerId) as t1 on t.PatientId = t1.PatientId) 
			WHERE t1.PatientId in ( SELECT PatientId FROM Patient (NOLOCK) WHERE CustomerId = @customerId AND SAPPatientNumber 
								IN ( SELECT Value FROM dbo.SplitToTable(@sapIds))
							)			
		END
	END	
 ELSE IF(@type = 3)
	BEGIN
		IF (isnull(@sapIds,'') = '' OR LEN(@sapIds) = 0)
		BEGIN		
			SELECT 
              isnull(t.SAPCustomerNumber, t1.SAPCustomerNumber) as CustomerId
			  , @commonId as IndigoPatientId
              , isnull(t.CarehomeId, t1.CarehomeId) as IndigoCarehomeId    
              , t.CommunicationPreference as CommunicationPreference
              , t1.MarkettingPreference as MarkettingPreference
			  , ISNULL(t.Remarks,'') + '|' + ISNULL(t1.Remarks,'') AS Remarks
			from  ((
					SELECT 
                             c.SAPCustomerNumber
                            ,ch.CarehomeId 
							,tr.TranslationType as CommunicationPreference
							,ip.Remarks
					FROM IDXPreference ip (NOLOCK)            
					INNER JOIN Carehome ch (NOLOCK) ON ip.CarehomeId = ch.CarehomeId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on ch.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20145  and ch.CustomerId = @customerId) as t 
			FULL OUTER JOIN (               
					SELECT 
                             c.SAPCustomerNumber
                            ,ch.CarehomeId 
							,tr.TranslationType as MarkettingPreference         
							,ip.Remarks							                 
					FROM IDXPreference ip (NOLOCK)            
					INNER JOIN Carehome ch (NOLOCK) ON ip.CarehomeId = ch.CarehomeId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on ch.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20146 and ch.CustomerId = @customerId) as t1 on t.CareHomeId = t1.CareHomeId) 	
					WHERE t1.CarehomeId in (SELECT CarehomeId FROM Carehome (NOLOCK) WHERE CustomerId = @customerId)			
		END
		ELSE
		BEGIN		
			SELECT 
              isnull(t.SAPCustomerNumber, t1.SAPCustomerNumber) as CustomerId
			  , @commonId as IndigoPatientId
              , isnull(t.CarehomeId, t1.CarehomeId) as IndigoCarehomeId   
              , t.CommunicationPreference as CommunicationPreference
              , t1.MarkettingPreference as MarkettingPreference
			  , ISNULL(t.Remarks,'') + '|' + ISNULL(t1.Remarks,'') AS Remarks
			from  ((
					SELECT 
                             c.SAPCustomerNumber
                            ,ch.CarehomeId 
							,tr.TranslationType as CommunicationPreference
							,ip.Remarks
					FROM IDXPreference ip (NOLOCK)             
					INNER JOIN Carehome ch (NOLOCK) ON ip.CarehomeId = ch.CarehomeId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on ch.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20145  and ch.CustomerId = @customerId) as t 
			FULL OUTER JOIN (               
					SELECT 
                             c.SAPCustomerNumber
                            ,ch.CarehomeId 
							,tr.TranslationType as MarkettingPreference                          
							,ip.Remarks
					FROM IDXPreference ip (NOLOCK)            
					INNER JOIN Carehome ch (NOLOCK) ON ip.CarehomeId = ch.CarehomeId
					LEFT JOIN List lst (NOLOCK) oN IP.Preference = LST.ListId
					LEFT JOIN Translation tr (NOLOCK) on lst.TransProxyId = tr.TransProxyID and tr.LanguageID = @languageId
					LEFT JOIN Customer c (NOLOCK) on ch.CustomerId = c.CustomerId 
					WHERE ip.PreferenceType = 20146 and ch.CustomerId = @customerId) as t1 on t.CareHomeId = t1.CareHomeId) 	
					WHERE t1.CarehomeId in ( SELECT CarehomeId FROM Carehome (NOLOCK) WHERE CustomerId = @customerId AND SAPCareHomeNumber 
									IN ( SELECT Value FROM dbo.SplitToTable(@sapIds))
								)	
		END
	END
END
