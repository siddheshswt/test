﻿-- =============================================
-- Author:		BASANTA SAHOO
-- Create date: 2015-05-11
-- Description:	stored procedure to get the Clinical Contact Master Data based on listtype and languageId

--Sample execution 
/*			declare @listTypeId varchar(100) = '1,2,3,4,5,6,7,8,9,10,15,17',
			@languageId INT = 1	;
	exec GetClinicalContactAnalysisMaster @listTypeId, @languageId
*/
-- =============================================

CREATE PROCEDURE [dbo].[GetClinicalContactAnalysisMaster] 
	@listTypeId varchar(100),
	@languageId INT	
AS
BEGIN
	SET NOCOUNT ON;
SELECT 
		clinicalMaster.TypeId	
	,	clinicalMaster.clinicalcontactanalysismasterid
	,	clinicalMaster.[Index]
	,CASE WHEN t.TranslationType IS NULL
		  THEN tp.Text
		  ELSE  t.TranslationType 
	END ContactName
FROM ClinicalContactAnalysisMaster  clinicalMaster (NOLOCK)
INNER JOIN TranslationProxy tp (NOLOCK) ON tp.TransProxyID = clinicalMaster.TransProxyId
LEFT OUTER JOIN Translation t (NOLOCK) ON t.TransProxyID = tp.TransProxyID
AND t.LanguageID = CONVERT(VARCHAR(10),@languageId)
 WHERE
 (@listTypeId ='-1' or clinicalMaster.TypeId in (SELECT * FROM SplitToTable(@listTypeId)))

ORDER BY clinicalMaster.TypeId, clinicalMaster.[index]

END

