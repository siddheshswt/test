﻿
/******************************
** File:    GetReportData_usp
** Name:	GetReportData_usp
** Desc:	Get Report Data
** Auth:	Gaurav/Jagdish
** Date:	04 May 2015

********************************
** Change History
********************************
** PR	Date			Author				Description	
** --	--------		-------				-----------------------------------
**	1	08Dec2015      Siddhesh Sawant		Added RemovedStoppedDateTime column in select statement
**	2	08Dec2015      Siddhesh Sawant		Added ActualDeliveryDate,Plant,BillingDate,Currency,BillingQuantity,BillingUnit,NetValue,VatValue column from order table in select statement
**	3	19Dec2015      Siddhesh Sawant		Added Invoice number range and invoice/billing date range in criteria
**	4	19Dec2015      Siddhesh Sawant		casting to date for delivery date, order date and billing date to compare date values
**  5   13Jan2016	   Siddhesh Sawant		Removed ActualDeliveryDate column from Orders table
*******************************/
-- =============================================

/*
exec GetReportData_usp 
@CustomerId = 11,
@PatientIdName = '',
@CareHomeId = '',
@PatientDeilveryFromDate = '',
@PatientDeliveryToDate = '',
@OrderFromDate = '',
@OrderToDate = '',
@InvoiceFromDate = '',
@InvoiceToDate = '',
@InvoiceNumber = '',
@InvoiceNumberTo = '',
@PatientType = '',
@PatientStatus  = '',
@UserDataOnly = 0,
@CustomerColumns = 'SapCustomerNumber,CustomerName',
@PatientColumns = 'SAPPatientNumber,RemovedStoppedDateTime',
@PrescriptionColumns = '',
@ClinicalContactsColumns = '',
@CareHomeColumns = '',
@OrdersColumns = '',
@UserColumns = '',
@ProductIds = '',
@LanguageId= 1,
@PageNbr = 1,
@PageSize = 10,
@SortColumn  = '',
@SortDirc  = '',
@IsDownload = 0
*/
-- =============================================
CREATE PROCEDURE [dbo].[GetReportData_usp_22012016]
@CustomerId VARCHAR(MAX) = NULL,
@PatientIdName VARCHAR(400) = NULL,
@CareHomeId VARCHAR(MAX) = NULL,
@PatientDeilveryFromDate VARCHAR(20) = NULL,
@PatientDeliveryToDate VARCHAR(20) = NULL,
@OrderFromDate VARCHAR(20) = NULL,
@OrderToDate VARCHAR(20) = NULL,
@InvoiceFromDate VARCHAR(20) = NULL,
@InvoiceToDate VARCHAR(20) = NULL,
@InvoiceNumber NVARCHAR(100) = NULL,
@InvoiceNumberTo NVARCHAR(100) = NULL,
@PatientType VARCHAR(500) = NULL,
@PatientStatus VARCHAR(200) = NULL,
@UserDataOnly BIT = 0,
@CustomerColumns VARCHAR(2000) = NULL,
@PatientColumns VARCHAR(2000) = NULL,
@PrescriptionColumns VARCHAR(2000) = NULL,
@ClinicalContactsColumns VARCHAR(2000) = NULL,
@CareHomeColumns VARCHAR(2000) = NULL,
@OrdersColumns VARCHAR(2000) = NULL,
@UserColumns VARCHAR(2000) = NULL,
@ProductIds VARCHAR(2000) = NULL,
@LanguageId VARCHAR(3) = 1,
@PageNbr INT = 1,
@PageSize INT = 500,
@SortColumn VARCHAR(100) = null,
@SortDirc VARCHAR(10) = null,
@IsDownload bit = 0
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @pivotRequired bit = 0;
	DECLARE @Query VARCHAR(MAX) = ''
	DECLARE @QueryFrom VARCHAR(MAX) = ''
	DECLARE @Columns VARCHAR(MAX) = ''
	DECLARE @AllColumns VARCHAR(MAX) = ''
	DECLARE @FirstRec  INT,
			@LastRec   INT

	SET @FirstRec  = ( @PageNbr - 1 ) * @PageSize
    SET @LastRec   = ( @PageNbr * @PageSize + 1 ) 	


	IF(@UserDataOnly = 0)
		BEGIN		
			-- Create temporary table only if all data is not required. Only specified range is required to return.
			IF(@IsDownload = 0)
				BEGIN
					CREATE TABLE #ReportBuilderData
					(
						RowNumber INT IDENTITY(1,1), SapCustomerNumber NVARCHAR(100), CustomerName NVARCHAR(200), CustomerStartDate DATETIME, CustomerStatus NVARCHAR(100)
						, CustomerUserName NVARCHAR(100), CustomerAuthorizationPIN INT, CustomerAuthorizedUserFirstName NVARCHAR(200), CustomerAuthorizedUserLastName NVARCHAR(200)
						, CustomerAuthorizedUserJobTitle NVARCHAR(200), CustomerAuthorizedUserTelephoneNumber NVARCHAR(40), CustomerAuthorizedUserMobileNumber NVARCHAR(40)
						, CustomerAuthorizedUserEmail NVARCHAR(100), SAPPatientNumber NVARCHAR(128), PatientGender NVARCHAR(100), PatientTitle NVARCHAR(100), PatientFirstName NVARCHAR(200)
						, PatientLastName NVARCHAR(200), PatientDateofBirth DATETIME, PatientType NVARCHAR(100), PatientStatus NVARCHAR(100), PatientReasonCode NVARCHAR(100)
						, PatientDeliveryFrequency VARCHAR(2), PatientDeliveryAddressLine1 NVARCHAR(160), PatientDeliveryAddressLine2 NVARCHAR(160), PatientDeliveryCity NVARCHAR(128)
						, PatientDeliveryCountry NVARCHAR(200), PatientDeliveryHouseName NVARCHAR(64), PatientDeliveryHouseNumber NVARCHAR(128), PatientDeliveryPostCode NVARCHAR(20)
						, PatientAddressAddressLine1 NVARCHAR(160), PatientAddressAddressLine2 NVARCHAR(160), PatientAddressCity NVARCHAR(128), PatientAddressCountry NVARCHAR(200)
						, PatientAddressHouseName NVARCHAR(64), PatientAddressHouseNumber NVARCHAR(128), PatientAddressPostCode NVARCHAR(20), PatientAssessmentDate DATETIME
						, PatientNextAssessmentDate DATETIME, PatientTelephoneNumber NVARCHAR(40), PatientMobileNumber NVARCHAR(40), PatientEmail NVARCHAR(100)
						, PatientCommunicationFormat NVARCHAR(100), PatientDeliveryDate DATETIME, PatientNextDeliveryDate DATETIME, PatientRoundId VARCHAR(10), PatientAdpLtc NVARCHAR(32)
						, PatientAdpNc NVARCHAR(32), PatientCreatedDate DATETIME, PatientCreatedBy NVARCHAR(100), PatientNhsId NVARCHAR(32), PatientLocalId NVARCHAR(32)
						, PatientSAPCareHomeNumber NVARCHAR(50), PatientCareHomeName NVARCHAR(200), IsDataProtected NVARCHAR(5)
						, RemovedStoppedDateTime DATETIME
						, PrescriptionSapProductId NVARCHAR(50)
						, PrescriptionProductDescription NVARCHAR(200), PrescriptionFrequency BIGINT, PrescriptionAssessedPadsPerDay BIGINT, PrescriptionActualPadsPerDay FLOAT
						, PrescriptionDQ1 BIGINT, PrescriptionDQ2 BIGINT, PrescriptionDQ3 BIGINT, PrescriptionDQ4 BIGINT, PrescriptionValidFromDate DATETIME, PrescriptionValidToDate DATETIME
						, PrescriptionStatus NVARCHAR(32), PrescriptionDeliveryDate DATETIME, PrescriptionNextDeliveryDate DATETIME, ClinicalContactRoleName NVARCHAR(100)
						, ClinicalContactRoleId BIGINT, ClinicalContactRoleValue NVARCHAR(200), ClinicalContactModifiedBy NVARCHAR(100), ClinicalContactModifiedDate DATETIME
						, CareHomeId BIGINT, SAPCareHomeNumber NVARCHAR(50), CareHomeName NVARCHAR(200), CareHomeDeliveryFrequency VARCHAR(2), CareHomeRoundId VARCHAR(4) 
						, CareHomeAddressLine1 NVARCHAR(160), CareHomeAddressLine2 NVARCHAR(160), CareHomeCity NVARCHAR(128), CareHomeCountry NVARCHAR(200), CareHomeHouseName NVARCHAR(64)
						, CareHomeHouseNumber NVARCHAR(128), CareHomePostCode NVARCHAR(20), CareHomeStatus NVARCHAR(100), CareHomeType NVARCHAR(100), CarehomeNextDeliveryDate DATETIME
						, CarehomePurchaseOrderNo BIGINT, OrderId bigint, SAPOrderNo NVARCHAR(100), OrderSAPDeliveryNo NVARCHAR(200), OrderType NVARCHAR(100), OrderDate DATETIME
						, SAPOrderDeliveryDate DATETIME, OrderSAPProductID BIGINT, OrderProductDescription NVARCHAR(200), OrderQuantity BIGINT, OrderInvoiceNo NVARCHAR(100)
						
						--, ActualDeliveryDate DATETIME
						, Plant VARCHAR(20), BillingDate DATETIME, Currency VARCHAR(20), BillingQuantity DECIMAL(18,3), BillingUnit VARCHAR(20), NetValue DECIMAL(18,2), VatValue DECIMAL(18,2)
						
						, UserType NVARCHAR(40), UserRoleName NVARCHAR(64), UserId NVARCHAR(50), UserName NVARCHAR(100), UserSapCustomerNo NVARCHAR(100), UserCustomerName NVARCHAR(200)
						, ContinenceAdvisor NVARCHAR(200), Nurse NVARCHAR(200), NurseBase NVARCHAR(200), HealthVisitor NVARCHAR(200), HC NVARCHAR(200), Doctor NVARCHAR(200), GPPractice NVARCHAR(200)
						, CaseloadHolder NVARCHAR(200), PONumber NVARCHAR(200), TypeofIncontinence NVARCHAR(200),  MedicalCondition NVARCHAR(200), School NVARCHAR(200), Ethnicity NVARCHAR(200)
						, Locality NVARCHAR(200), Sector NVARCHAR(200), HealthCentre NVARCHAR(200), BudgetCentre NVARCHAR(200)
					)
				END
			-- Below if conditions checks whether pivot fields exists in patient columns or not.
			IF(LEN(@PatientColumns) > 1 AND (CHARINDEX('ContinenceAdvisor', @PatientColumns) != 0 OR CHARINDEX('Nurse', @PatientColumns) != 0 OR CHARINDEX('NurseBase', @PatientColumns) != 0
												OR CHARINDEX('HealthVisitor', @PatientColumns) != 0 OR CHARINDEX('HC', @PatientColumns) != 0 OR CHARINDEX('Doctor', @PatientColumns) != 0
												OR CHARINDEX('GPPractice', @PatientColumns) != 0 OR CHARINDEX('CaseloadHolder', @PatientColumns) != 0 OR CHARINDEX('PONumber', @PatientColumns) != 0 
												OR CHARINDEX('TypeofIncontinence', @PatientColumns) != 0 OR CHARINDEX('MedicalCondition', @PatientColumns) != 0 OR CHARINDEX('School', @PatientColumns) != 0
												OR CHARINDEX('Ethnicity', @PatientColumns) != 0 OR CHARINDEX('Locality', @PatientColumns) != 0 OR CHARINDEX('Sector', @PatientColumns) != 0
												OR CHARINDEX('HealthCentre', @PatientColumns) != 0 OR CHARINDEX('BudgetCentre', @PatientColumns) != 0 ))
			BEGIN
				SET @pivotRequired = 1;
			END

			IF(LEN(@CustomerColumns) > 1)	
				BEGIN
					SET @Columns = @Columns + ',' + @CustomerColumns
				end

			IF(LEN(@PatientColumns) > 1)	
				BEGIN
					SET @Columns = @Columns + ',' + @PatientColumns
				end

			IF(LEN(@PrescriptionColumns) > 1)	
				BEGIN
					SET @Columns = @Columns +  ',' + @PrescriptionColumns
				end

			IF(LEN(@ClinicalContactsColumns) > 1)	
				BEGIN
					SET @Columns = @Columns +  ',' + @ClinicalContactsColumns
				end

			IF(LEN(@CareHomeColumns) > 1)	
				BEGIN
					SET @Columns = @Columns +  ',' + @CareHomeColumns
				end

			IF(LEN(@OrdersColumns) > 1)	
				BEGIN
					SET @Columns = @Columns +  ',' + @OrdersColumns
				end

			IF(LEN(@UserColumns) > 1)	
				BEGIN
					SET @Columns = @Columns +  ',' + @UserColumns
				end

			-- Remove first comma from @Columns
			SET @Columns = SUBSTRING(@Columns, 2, LEN(@Columns))

			IF(@IsDownload = 0)
				BEGIN
					SET @Query = @Query + ' INSERT INTO #ReportBuilderData (' + @Columns + ') '
				END

			SET @Query = @Query +
			' SELECT * FROM (
					SELECT DISTINCT ' + @Columns

			SET @Query = @Query + ' FROM ( '
			
			IF(@pivotRequired = 1)	
				BEGIN				
					SET  @Query =  @Query +  
					' SELECT T1.* , T1.[Continence Advisor] AS ContinenceAdvisor, T1.[Nurse Base] AS NurseBase, T1.[Health Visitor] AS [HealthVisitor], T1.[GP Practice] AS [GPPractice]
						, T1.[Caseload Holder] AS [CaseloadHolder], T1.[PO Number] AS [PONumber], T1.[Type of Incontinence] AS [TypeofIncontinence], T1.[Medical Condition] AS [MedicalCondition]
						, T1.[Health Centre] AS [HealthCentre], T1.[Budget Centre] AS [BudgetCentre]
					FROM ( '
				END

			SET  @Query =  @Query + ' SELECT '
		
			IF(LEN(@CustomerColumns) > 1)	
				BEGIN
					--Customer Columns
					IF(CHARINDEX('SapCustomerNumber', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', c.SapCustomerNumber '
						END

					IF(CHARINDEX('CustomerName', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.FirstName AS CustomerName '
						END

					IF(CHARINDEX('CustomerStartDate', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', c.CustomerStartDate '
						END

					IF(CHARINDEX('CustomerStatus', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tcs.TranslationType AS CustomerStatus '
						END

					IF(CHARINDEX('CustomerAuthorizationPIN', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', u.AuthorizationPIN AS CustomerAuthorizationPIN '
						END

					IF(CHARINDEX('CustomerAuthorizedUserFirstName', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.FirstName AS CustomerAuthorizedUserFirstName '
						END

					IF(CHARINDEX('CustomerAuthorizedUserLastName', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.LastName AS CustomerAuthorizedUserLastName '
						END

					IF(CHARINDEX('CustomerAuthorizedUserJobTitle', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', CAST(pic.JobTitle AS NVARCHAR(200)) AS CustomerAuthorizedUserJobTitle '
						END

					IF(CHARINDEX('CustomerAuthorizedUserTelephoneNumber', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.TelephoneNumber AS CustomerAuthorizedUserTelephoneNumber '
						END

					IF(CHARINDEX('CustomerAuthorizedUserMobileNumber', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.MobileNumber AS CustomerAuthorizedUserMobileNumber '
						END

					IF(CHARINDEX('CustomerAuthorizedUserEmail', @CustomerColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.Email AS CustomerAuthorizedUserEmail '
						END
				END

			IF(LEN(@PatientColumns) > 1)	
				BEGIN
					--Patient Columns
					IF(CHARINDEX('SAPPatientNumber', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.SAPPatientNumber '
						END

					IF(CHARINDEX('PatientGender', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tpg.TranslationType AS PatientGender '
						END

					IF(CHARINDEX('PatientTitle', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tpti.TranslationType AS PatientTitle '
						END

					IF(CHARINDEX('PatientFirstName', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.FirstName AS PatientFirstName '
						END

					IF(CHARINDEX('PatientLastName', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.LastName AS PatientLastName '
						END

					IF(CHARINDEX('PatientDateofBirth', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.DateofBirth AS PatientDateofBirth '
						END

					IF(CHARINDEX('PatientType', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tpty.TranslationType AS PatientType '
						END

					IF(CHARINDEX('PatientStatus', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tps.TranslationType AS PatientStatus '
						END

					IF(CHARINDEX('PatientReasonCode', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tpr.TranslationType AS PatientReasonCode '
						END

					IF(CHARINDEX('PatientDeliveryFrequency', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.DeliveryFrequency AS PatientDeliveryFrequency '
						END

					IF(CHARINDEX('PatientDeliveryAddressLine1', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.AddressLine1 AS PatientDeliveryAddressLine1 '
						END

					IF(CHARINDEX('PatientDeliveryAddressLine2', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.AddressLine2 AS PatientDeliveryAddressLine2 '
						END

					IF(CHARINDEX('PatientDeliveryCity', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.City AS PatientDeliveryCity '
						END

					IF(CHARINDEX('PatientDeliveryCountry', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.Country AS PatientDeliveryCountry '
						END

					IF(CHARINDEX('PatientDeliveryHouseName', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.HouseName AS PatientDeliveryHouseName '
						END

					IF(CHARINDEX('PatientDeliveryHouseNumber', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.HouseNumber AS PatientDeliveryHouseNumber '
						END

					IF(CHARINDEX('PatientDeliveryPostCode', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', apd.PostCode AS PatientDeliveryPostCode '
						END

					IF(CHARINDEX('PatientAddressAddressLine1', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.AddressLine1 AS PatientAddressAddressLine1 '
						END

					IF(CHARINDEX('PatientAddressAddressLine2', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.AddressLine2 AS PatientAddressAddressLine2 '
						END

					IF(CHARINDEX('PatientAddressCity', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.City AS PatientAddressCity '
						END

					IF(CHARINDEX('PatientAddressCountry', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.Country AS PatientAddressCountry '
						END

					IF(CHARINDEX('PatientAddressHouseName', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.HouseName AS PatientAddressHouseName '
						END

					IF(CHARINDEX('PatientAddressHouseNumber', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.HouseNumber AS PatientAddressHouseNumber '
						END

					IF(CHARINDEX('PatientAddressPostCode', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ap.PostCode AS PatientAddressPostCode '
						END

					IF(CHARINDEX('PatientAssessmentDate', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.AssessmentDate AS PatientAssessmentDate '
						END

					IF(CHARINDEX('PatientNextAssessmentDate', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.NextAssessmentDate AS PatientNextAssessmentDate '
						END

					IF(CHARINDEX('PatientTelephoneNumber', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.TelephoneNumber AS PatientTelephoneNumber '
						END

					IF(CHARINDEX('PatientMobileNumber', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.MobileNumber AS PatientMobileNumber '
						END

					IF(CHARINDEX('PatientEmail', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.Email AS PatientEmail '
						END

					IF(CHARINDEX('PatientCommunicationFormat', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tcf.TranslationType AS PatientCommunicationFormat '
						END

					IF(CHARINDEX('PatientDeliveryDate', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.DeliveryDate AS PatientDeliveryDate '
						END

					IF(CHARINDEX('PatientNextDeliveryDate', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.NextDeliveryDate AS PatientNextDeliveryDate '
						END

					IF(CHARINDEX('PatientRoundId', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.RoundId AS PatientRoundId '
						END

					IF(CHARINDEX('PatientAdpLtc', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', CAST(p.ADP1 AS NVARCHAR(32)) AS PatientAdpLtc  '
						END

					IF(CHARINDEX('PatientAdpNc', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', CAST(p.ADP2 AS NVARCHAR(32)) AS PatientAdpNc '
						END

					IF(CHARINDEX('PatientCreatedDate', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.CreatedDateTime AS PatientCreatedDate '
						END

					IF(CHARINDEX('PatientCreatedBy', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pip.CreatedById AS PatientCreatedBy '
						END

					IF(CHARINDEX('PatientNhsId', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.NhsId AS PatientNhsId '
						END

					IF(CHARINDEX('PatientLocalId', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.LocalId AS PatientLocalId '
						END

					IF(CHARINDEX('IsDataProtected', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', CASE p.IsDataProtected WHEN 1 THEN ''Yes'' ELSE ''No'' END AS IsDataProtected '
						END

					IF(CHARINDEX('RemovedStoppedDateTime', @PatientColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', p.RemovedStoppedDateTime AS RemovedStoppedDateTime '
						END
				END

			IF(@pivotRequired = 1)	
				BEGIN				
					SET @AllColumns = @AllColumns +
					', clinical.ContactName AS PatientContactName, info.ContactName as PatientContactNameValue '
				END

			IF(LEN(@PrescriptionColumns) > 1)	
				BEGIN
					--Table Prescription
					IF(CHARINDEX('PrescriptionSapProductId', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pro.SapProductId AS PrescriptionSapProductId '
						END 

					IF(CHARINDEX('PrescriptionProductDescription', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pro.DescriptionUI AS PrescriptionProductDescription '
						END

					IF(CHARINDEX('PrescriptionFrequency', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.Frequency AS PrescriptionFrequency '
						END

					IF(CHARINDEX('PrescriptionAssessedPadsPerDay', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.AssessedPadsPerDay AS PrescriptionAssessedPadsPerDay '
						END

					IF(CHARINDEX('PrescriptionActualPadsPerDay', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.ActPPD AS PrescriptionActualPadsPerDay '
						END

					IF(CHARINDEX('PrescriptionDQ1', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.DQ1 AS PrescriptionDQ1 '
						END

					IF(CHARINDEX('PrescriptionDQ2', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.DQ2 AS PrescriptionDQ2 '
						END

					IF(CHARINDEX('PrescriptionDQ3', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.DQ3 AS PrescriptionDQ3 '
						END

					IF(CHARINDEX('PrescriptionDQ4', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.DQ4 AS PrescriptionDQ4 '
						END

					IF(CHARINDEX('PrescriptionValidFromDate', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.ValidFromDate AS PrescriptionValidFromDate '
						END

					IF(CHARINDEX('PrescriptionValidToDate', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.ValidToDate AS PrescriptionValidToDate '
						END

					IF(CHARINDEX('PrescriptionStatus', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.Status AS PrescriptionStatus '
						END

					IF(CHARINDEX('PrescriptionDeliveryDate', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.DeliveryDate AS PrescriptionDeliveryDate '
						END

					IF(CHARINDEX('PrescriptionNextDeliveryDate', @PrescriptionColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pp.NextDeliveryDate AS PrescriptionNextDeliveryDate '
						END
				END
			
			IF(LEN(@ClinicalContactsColumns) > 1)	
				BEGIN
					--Table Clinical Contacts
					IF(CHARINDEX('ClinicalContactRoleName', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tcc.TranslationType AS ClinicalContactRoleName '
						END
					
					IF(CHARINDEX('ClinicalContactRoleId', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', a.ClinicalContactAnalysisMasterId AS ClinicalContactRoleId '
						END

					IF(CHARINDEX('ClinicalContactRoleValue', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', vm.ContactName AS ClinicalContactRoleValue '
						END

					IF(CHARINDEX('ClinicalContactModifiedBy', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', v.ModifiedBy AS ClinicalContactModifiedBy '
						END

					IF(CHARINDEX('ClinicalContactModifiedDate', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', v.ModifiedDate AS ClinicalContactModifiedDate'
						END
				END

			IF(LEN(@CareHomeColumns) > 1)	
				BEGIN
					--Table Carehome
					IF(CHARINDEX('CareHomeId', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ch.CareHomeId '
						END

					IF(CHARINDEX('SAPCareHomeNumber', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ch.SAPCareHomeNumber '
						END

					IF(CHARINDEX('CareHomeName', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pich.FirstName AS CareHomeName '
						END

					IF(CHARINDEX('CareHomeDeliveryFrequency', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ch.DeliveryFrequency  AS CareHomeDeliveryFrequency '
						END

					IF(CHARINDEX('CareHomeRoundId', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ch.RoundId AS CareHomeRoundId '
						END

					IF(CHARINDEX('CareHomeAddressLine1', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ',ach.AddressLine1 AS CareHomeAddressLine1 '
						END

					IF(CHARINDEX('CareHomeAddressLine2', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ach.AddressLine2 AS CareHomeAddressLine2 '
						END

					IF(CHARINDEX('CareHomeCity', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ach.City AS CareHomeCity '
						END

					IF(CHARINDEX('CareHomeCountry', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ach.Country AS CareHomeCountry '
						END

					IF(CHARINDEX('CareHomeHouseName', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ',ach.HouseName AS CareHomeHouseName '
						END

					IF(CHARINDEX('CareHomeHouseNumber', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ach.HouseNumber AS CareHomeHouseNumber '
						END

					IF(CHARINDEX('CareHomePostCode', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ach.PostCode AS CareHomePostCode '
						END

					IF(CHARINDEX('CareHomeStatus', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tchs.TranslationType AS CareHomeStatus '
						END

					IF(CHARINDEX('CareHomeType', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ',tcht.TranslationType AS CareHomeType '
						END

					IF(CHARINDEX('CarehomeNextDeliveryDate', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ch.NextDeliveryDate AS CarehomeNextDeliveryDate '
						END

					IF(CHARINDEX('CarehomePurchaseOrderNo', @CareHomeColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ch.PurchaseOrderNo AS CarehomePurchaseOrderNo '
						END
				END

			IF(LEN(@OrdersColumns) > 1)	
				BEGIN
					--Table Order
					IF(CHARINDEX('OrderId', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.OrderId '
						END

					IF(CHARINDEX('SAPOrderNo', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.SAPOrderNo '
						END

					IF(CHARINDEX('OrderSAPDeliveryNo', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.SAPDeliveryNo AS OrderSAPDeliveryNo '
						END

					IF(CHARINDEX('OrderType', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', tot.Description AS OrderType '
						END

					IF(CHARINDEX('OrderDate', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.OrderDate AS OrderDate '
						END

					IF(CHARINDEX('SAPOrderDeliveryDate', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.SAPOrderDeliveryDate AS SAPOrderDeliveryDate '
						END

					IF(CHARINDEX('OrderSAPProductID', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ISNULL(proo.SAPProductID, sp.HelixProductID) AS OrderSAPProductID '
						END

					IF(CHARINDEX('OrderProductDescription', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', ISNULL(proo.DescriptionUI, sp.ProductDescription) AS OrderProductDescription '
						END

					IF(CHARINDEX('OrderQuantity', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', op.Quantity AS OrderQuantity '
						END

					IF(CHARINDEX('OrderInvoiceNo', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.InvoiceNo AS OrderInvoiceNo '
						END

					--IF(CHARINDEX('ActualDeliveryDate', @OrdersColumns) <> 0)
					--	BEGIN
					--		SET @AllColumns = @AllColumns + ', o.ActualDeliveryDate AS ActualDeliveryDate '
					--	END

					IF(CHARINDEX('Plant', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.Plant AS Plant '
						END

					IF(CHARINDEX('BillingDate', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.BillingDate AS BillingDate '
						END

					IF(CHARINDEX('Currency', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.Currency AS Currency '
						END

					IF(CHARINDEX('BillingQuantity', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.BillingQuantity AS BillingQuantity '
						END

					IF(CHARINDEX('BillingUnit', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.BillingUnit AS BillingUnit '
						END

					IF(CHARINDEX('NetValue', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.NetValue AS NetValue '
						END

					IF(CHARINDEX('VatValue', @OrdersColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', o.VatValue AS VatValue '
						END
				END

			IF(LEN(@UserColumns) > 1)	
				BEGIN
					--Table User Maintenance
					IF(CHARINDEX('UserType', @UserColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ',r.UserType '
						END

					IF(CHARINDEX('UserRoleName', @UserColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', r.RoleName AS UserRoleName '
						END

					IF(CHARINDEX('UserId', @UserColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', u.UserId '
						END

					IF(CHARINDEX('UserName', @UserColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', u.UserName '
						END

					IF(CHARINDEX('UserSapCustomerNo', @UserColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', c.SAPCustomerNumber AS UserSapCustomerNo '
						END

					IF(CHARINDEX('UserCustomerName', @UserColumns) <> 0)
						BEGIN
							SET @AllColumns = @AllColumns + ', pic.FirstName AS UserCustomerName '
						END
				END
			
			-- Remove first comma
			SET @AllColumns = SUBSTRING(@AllColumns, 2, LEN(@AllColumns))
						
			SET @QueryFrom = @QueryFrom + ' FROM Customer c(nolock) '

			IF(LEN(@CustomerColumns) > 1 OR LEN(@UserColumns) > 1)	
				BEGIN
					SET @QueryFrom = @QueryFrom + 
					' LEFT JOIN PersonalInformation pic(nolock) ON c.PersonalInformationId = pic.PersonalInformationId '

					IF(CHARINDEX('UserId', @UserColumns) <> 0 OR CHARINDEX('UserName', @UserColumns) <> 0 OR CHARINDEX('UserType', @UserColumns) <> 0 
						OR CHARINDEX('UserRoleName', @UserColumns) <> 0 OR CHARINDEX('CustomerUserName', @CustomerColumns) <> 0 OR CHARINDEX('CustomerAuthorizationPIN', @CustomerColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN IDXUserCustomer as uc(nolock) ON c.CustomerId = uc.CustomerId 
							LEFT JOIN Users as u(nolock) ON uc.UserId = u.UserId '
						END
				END

			IF(LEN(@CustomerColumns) > 1)	
				BEGIN
					IF(CHARINDEX('CustomerStatus', @CustomerColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lcs(nolock) ON c.Customer_Status = lcs.ListId
							LEFT JOIN Translation tcs(nolock) ON lcs.TransProxyId = tcs.TransProxyID AND tcs.LanguageID =' + @LanguageId + ' '
						END
				END

			IF(LEN(@PatientColumns) > 1 OR @PatientIdName <> '' OR @PatientDeilveryFromDate <> '' OR @PatientDeliveryToDate <> '' OR @PatientType <> '' OR @PatientStatus <> '' or LEN(@OrdersColumns) > 1 OR LEN(@PrescriptionColumns) > 1)	
				BEGIN
					SET @QueryFrom = @QueryFrom + 
					' LEFT JOIN Patient p(nolock) ON c.CustomerId = p.CustomerId 
					LEFT JOIN PersonalInformation pip(nolock) ON p.PersonalInformationId = pip.PersonalInformationId '
				END
				
			IF(LEN(@PatientColumns) > 1)	
				BEGIN
					IF(CHARINDEX('PatientGender', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lpg(nolock) ON pip.Gender = lpg.ListId
							LEFT JOIN Translation tpg(nolock) ON lpg.TransProxyId = tpg.TransProxyID AND tpg.LanguageID =' + @LanguageId + ' '
						END

					IF(CHARINDEX('PatientTitle', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lpti(nolock) ON pip.TitleId = lpti.ListId
							LEFT JOIN Translation tpti(nolock) ON lpti.TransProxyId = tpti.TransProxyID AND tpti.LanguageID =' + @LanguageId + ' '
						END

					IF(CHARINDEX('PatientType', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lpty(nolock) ON p.PatientType = lpty.ListId
							LEFT JOIN Translation tpty(nolock) ON lpty.TransProxyId = tpty.TransProxyID AND tpty.LanguageID =' + @LanguageId + ' '
						END

					IF(CHARINDEX('PatientStatus', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lps(nolock) ON p.PatientStatus = lps.ListId
							LEFT JOIN Translation tps(nolock) ON lps.TransProxyId = tps.TransProxyID AND tps.LanguageID =' + @LanguageId +  ' '
						END

					IF(CHARINDEX('PatientReasonCode', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lpr(nolock) ON p.ReasonCodeID = lpr.ListId
							LEFT JOIN Translation tpr(nolock) ON lpr.TransProxyId = tpr.TransProxyID AND tpr.LanguageID =' + @LanguageId +  ' '

						END

					IF(CHARINDEX('PatientDeliveryAddressLine1', @PatientColumns) <> 0 OR CHARINDEX('PatientDeliveryAddressLine2', @PatientColumns) <> 0 OR CHARINDEX('PatientDeliveryCity', @PatientColumns) <> 0
					   OR CHARINDEX('PatientDeliveryCountry', @PatientColumns) <> 0 OR CHARINDEX('PatientDeliveryHouseName', @PatientColumns) <> 0 OR CHARINDEX('PatientDeliveryHouseNumber', @PatientColumns) <> 0
					   OR CHARINDEX('PatientDeliveryPostCode', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN [Address] apd(nolock) ON pip.CorrespondenceAddressId = apd.AddressId '
						END

					IF(CHARINDEX('PatientAddressAddressLine1', @PatientColumns) <> 0 OR CHARINDEX('PatientAddressAddressLine2', @PatientColumns) <> 0 OR CHARINDEX('PatientAddressCity', @PatientColumns) <> 0
						OR CHARINDEX('PatientAddressCountry', @PatientColumns) <> 0 OR CHARINDEX('PatientAddressHouseName', @PatientColumns) <> 0 OR CHARINDEX('PatientAddressHouseNumber', @PatientColumns) <> 0
						OR CHARINDEX('PatientAddressPostCode', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN [Address] ap(nolock) ON pip.AddressId = ap.AddressId '
						END

					IF(CHARINDEX('PatientCommunicationFormat', @PatientColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lcf(nolock) ON p.CommunicationFormat = lcf.ListId
							LEFT JOIN Translation tcf(nolock) ON lcf.TransProxyId = tcf.TransProxyID AND tcf.LanguageID =' + @LanguageId +  ' '
						END
				END

			IF(@pivotRequired = 1)	
				BEGIN
					SET @QueryFrom = @QueryFrom + 
					' LEFT JOIN IDXPatientMedicalStaff staff(nolock) ON p.Patientid = staff.Patientid
					LEFT JOIN [IDXCustomerMedicalStaffAnalysisInfo] info(nolock) ON staff.CustomerMedicalStaffId= info.IDXCustomerMedicalStaffAnalysisInfoID and c.CustomerId = info.CustomerID
					LEFT JOIN ClinicalContactAnalysisMaster clinical(nolock) ON info.MedicalAnalysisListID = clinical.ClinicalContactAnalysisMasterId '
				END

			IF(LEN(@PrescriptionColumns) > 1)	
				BEGIN
					SET @QueryFrom = @QueryFrom + 
					' LEFT JOIN Prescription pr(nolock) ON p.PatientId = pr.PatientId
					LEFT JOIN IDXPrescriptionProduct pp(nolock) ON pr.PrescriptionId = pp.PrescriptionId AND pp.IsRemoved = 0 '

					IF(CHARINDEX('PrescriptionSapProductId', @PrescriptionColumns) <> 0 OR CHARINDEX('PrescriptionProductDescription', @PrescriptionColumns) <> 0  OR @ProductIds != '')
						BEGIN
							SET @QueryFrom = @QueryFrom + ' LEFT JOIN Product pro(nolock) ON pp.ProductId = pro.ProductId '
						END
				END
			
			IF(LEN(@PatientColumns) > 1)
				BEGIN
					SET @QueryFrom = @QueryFrom + '
					LEFT JOIN CareHome ch(nolock) ON c.CustomerId = ch.CustomerId AND p.CareHomeId = ch.CareHomeId '
				END

			IF((LEN(@CareHomeColumns) > 1 OR @CareHomeId <> '' OR LEN(@OrdersColumns) > 1 OR @OrderFromDate <> '' OR @OrderToDate <> '' OR @InvoiceFromDate <> '' OR @InvoiceToDate <> '' OR @InvoiceNumber <> '' OR @InvoiceNumberTo <> '') AND LEN(@PatientColumns) = 0)	
				BEGIN
					SET @QueryFrom = @QueryFrom + '
					LEFT JOIN CareHome ch(nolock) ON c.CustomerId = ch.CustomerId '
				END

			IF(LEN(@CareHomeColumns) > 1)	
				BEGIN
					IF(CHARINDEX('CareHomeName', @CareHomeColumns) <> 0 OR CHARINDEX('CareHomeAddressLine1', @CareHomeColumns) <> 0 OR CHARINDEX('CareHomeAddressLine2', @CareHomeColumns) <> 0
						OR CHARINDEX('CareHomeCity', @CareHomeColumns) <> 0 OR CHARINDEX('CareHomeCountry', @CareHomeColumns) <> 0 OR CHARINDEX('CareHomeHouseName', @CareHomeColumns) <> 0
						OR CHARINDEX('CareHomeHouseNumber', @CareHomeColumns) <> 0 OR CHARINDEX('CareHomePostCode', @CareHomeColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN PersonalInformation pich(nolock) ON ch.PersonalInformationId = pich.PersonalInformationId
							LEFT JOIN [Address] ach(nolock) ON pich.AddressId = ach.AddressId '
						END

					IF(CHARINDEX('CareHomeType', @CareHomeColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lcht(nolock) ON ch.CareHomeType = lcht.ListId
							LEFT JOIN Translation tcht(nolock) ON lcht.TransProxyId = tcht.TransProxyID AND tcht.LanguageID =' + @LanguageId + ' ' 
						END

					IF(CHARINDEX('CareHomeStatus', @CareHomeColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lchs(nolock) ON ch.CareHomeStatus = lchs.ListId
							LEFT JOIN Translation tchs(nolock) ON lchs.TransProxyId = tchs.TransProxyID  AND tchs.LanguageID =' + @LanguageId + ' '
						END
				END

			IF(LEN(@OrdersColumns) > 1 OR @OrderFromDate <> '' OR @OrderToDate <> '' OR @InvoiceFromDate <> '' OR @InvoiceToDate <> '' OR @InvoiceNumber <> '' OR @InvoiceNumberTo <> '')	
				BEGIN
					SET @QueryFrom = @QueryFrom + 
					' LEFT JOIN Orders o(nolock) ON isnull(o.PatientId, o.CareHomeId) =
						CASE 
							WHEN o.PatientId is not null
								THEN  p.PatientId
							ELSE ch.CareHomeId	
						END	'
				END

			IF(LEN(@OrdersColumns) > 1)	
				BEGIN
					IF(CHARINDEX('OrderType', @OrdersColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN List lot(nolock) ON o.OrderType = lot.ListId
							LEFT JOIN Translation tot(nolock) ON lot.TransProxyId = tot.TransProxyID AND tot.LanguageID =' + @LanguageId + ' '
						END

					IF(CHARINDEX('OrderSAPProductID', @OrdersColumns) <> 0 OR CHARINDEX('OrderProductDescription', @OrdersColumns) <> 0 OR CHARINDEX('OrderQuantity', @OrdersColumns) <> 0 OR @ProductIds != '')
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN IDXOrderProduct op(nolock) ON o.OrderId = op.OrderId
							LEFT JOIN Product proo(nolock) ON op.ProductId = proo.ProductId 
							LEFT JOIN SampleProduct sp on op.SampleProductId = sp.SampleProductId '
						END
				END

			IF(LEN(@UserColumns) > 1)	
				BEGIN
					IF(CHARINDEX('UserType', @UserColumns) <> 0 OR CHARINDEX('UserRoleName', @UserColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + 
							' LEFT JOIN IDXUserRole ur(nolock) ON u.UserId = ur.UserID
							LEFT JOIN [Role] r(nolock) ON ur.RoleID = r.RoleId '
						END
				END

			IF(LEN(@ClinicalContactsColumns) > 1)	
				BEGIN
					SET @QueryFrom = @QueryFrom + ' LEFT JOIN ClinicalContactRoleMapping rm(nolock) ON c.CustomerId = rm.CustomerId '
					
					IF(CHARINDEX('ClinicalContactModifiedBy', @ClinicalContactsColumns) <> 0 OR CHARINDEX('ClinicalContactModifiedDate', @ClinicalContactsColumns) <> 0 OR CHARINDEX('ClinicalContactRoleValue', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + ' LEFT JOIN ClinicalContactValueMapping v(nolock) ON rm.ClinicalContactRoleMappingId = v.ClinicalContactRoleMappingId '
						END

					IF(CHARINDEX('ClinicalContactRoleValue', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + ' LEFT JOIN ClinicalValueMaster vm(nolock) ON v.ChildValueId = vm.ClinicalValueMasterId '
						END
					
					IF(CHARINDEX('ClinicalContactRoleId', @ClinicalContactsColumns) <> 0 OR CHARINDEX('ClinicalContactRoleName', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + ' LEFT JOIN ClinicalContactAnalysisMaster a(nolock) ON rm.ChildListId = a.ClinicalContactAnalysisMasterId '
						END
					IF(CHARINDEX('ClinicalContactRoleName', @ClinicalContactsColumns) <> 0)
						BEGIN
							SET @QueryFrom = @QueryFrom + ' LEFT JOIN Translation tcc(nolock) ON a.TransProxyID = tcc.TransProxyID AND tcc.LanguageID =' + @LanguageId + ' '
						END
				END

			IF(@ProductIds != '' AND LEN(@PrescriptionColumns) = 0 AND LEN(@OrdersColumns) = 0)
				BEGIN
					SET @QueryFrom = @QueryFrom + 
					' INNER JOIN IDXCustomerProduct cp(nolock) ON c.CustomerId = cp.CustomerId '
				END

			SET  @QueryFrom =  @QueryFrom + ' WHERE 1 = 1'

			IF(@CustomerId <> '' AND @CustomerId IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND c.CustomerId in ('+@CustomerId + ') '
				END 
			ELSE
				print 'a'

			IF(@PatientIdName <> '' AND @PatientIdName IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND (p.SAPPatientNumber = '''+@PatientIdName +''' OR pip.FirstName + '' '' + pip.LastName LIKE ''%' +@PatientIdName+'%''' + ') '
				END 

			IF(@CareHomeId <> '' AND @CareHomeId IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND ch.CareHomeId in ('+@CareHomeId + ') '
				END 

			IF(@PatientDeilveryFromDate <> '' AND @PatientDeilveryFromDate IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND CAST(p.DeliveryDate AS DATE) >= CAST('''+ @PatientDeilveryFromDate + ''' AS DATE)'
				END 

			IF(@PatientDeliveryToDate <> '' AND @PatientDeliveryToDate IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND '+ 'CAST(p.DeliveryDate AS DATE) < CAST(DATEADD(day,1,CAST('''+ @PatientDeliveryToDate + ''' AS DATE)) AS DATE)'
				END 

			IF(@OrderFromDate <> '' AND @OrderFromDate IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND CAST(o.OrderDate AS DATE) >= CAST('''+ @OrderFromDate + ''' AS DATE)'
				END 

			IF(@OrderToDate <> '' AND @OrderToDate IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND '+ 'CAST(o.OrderDate AS DATE) < CAST(DATEADD(day,1,CAST('''+ @OrderToDate + ''' AS DATE)) AS DATE)'
				END 

			IF(@InvoiceFromDate <> '' AND @InvoiceFromDate IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND CAST(o.BillingDate AS DATE) >= CAST('''+ @InvoiceFromDate + ''' AS DATE)'					
				END 

			IF(@InvoiceToDate <> '' AND @InvoiceToDate IS NOT NULL)
				BEGIN 
					SET  @QueryFrom =  @QueryFrom + ' AND '+ 'CAST(o.BillingDate AS DATE) <  CAST(DATEADD(day,1,CAST('''+@InvoiceToDate + ''' AS DATE)) AS DATE)' 										
				END 

			IF(@InvoiceNumber <> '' AND @InvoiceNumber IS NOT NULL)
				BEGIN			
					SET  @QueryFrom =  @QueryFrom + ' AND CAST(o.InvoiceNo AS BIGINT)  >=  CAST('''+ @InvoiceNumber  + ''' AS BIGINT)'
				END

			IF(@InvoiceNumberTo <> '' AND @InvoiceNumberTo IS NOT NULL)
			BEGIN
				SET  @QueryFrom =  @QueryFrom + ' AND CAST(o.InvoiceNo AS BIGINT)  <= CAST('''+ @InvoiceNumberTo  + ''' AS BIGINT)'
			END

			IF(@PatientType <> '' AND @PatientType IS NOT NULL)
				BEGIN
					SET  @QueryFrom =  @QueryFrom + ' AND p.PatientType IN ('+ @PatientType + ')'
				END

			IF(@PatientStatus <> '' AND @PatientStatus IS NOT NULL)
				BEGIN
					SET  @QueryFrom =  @QueryFrom + ' AND p.PatientStatus IN ('+ @PatientStatus + ')'
				END

			IF(@ProductIds != '' AND LEN(@PrescriptionColumns) = 0 AND LEN(@OrdersColumns) = 0)
				BEGIN
					SET @QueryFrom = @QueryFrom + ' AND cp.ProductId IN (' + @ProductIds + ')'
				END
			ELSE IF(@ProductIds != '' AND LEN(@PrescriptionColumns) <> 0)
				BEGIN
					SET @QueryFrom = @QueryFrom + ' AND pro.ProductId IN (' + @ProductIds + ')'
				END
			ELSE IF(@ProductIds != '' AND LEN(@OrdersColumns) <> 0)
				BEGIN
					SET @QueryFrom = @QueryFrom + ' AND proo.ProductId IN (' + @ProductIds + ')'
				END

			SET  @QueryFrom =  @QueryFrom + ' ) AS T '
			
			IF(@pivotRequired = 0)
				BEGIN
					SET  @QueryFrom =  @QueryFrom + ') AS T1'
				END

			IF(@pivotRequired = 1)	
				BEGIN				
					SET  @QueryFrom =  @QueryFrom + 
					' PIVOT
						(
							max(PatientContactNameValue)
							FOR PatientContactName IN ([Continence Advisor], [Nurse], [Nurse Base], [Health Visitor], [HC]
													 , [Doctor], [GP Practice], [Caseload Holder], [PO Number]
													 , [Type of Incontinence], [Medical Condition], [School], [Ethnicity], [Locality]
													 , [Sector], [Health Centre], [Budget Centre])
						) AS T1 ) AS T2 ) AS T3'
				END

			IF(@SortColumn != '' AND @SortDirc != '')
				BEGIN
					SET  @QueryFrom =  @QueryFrom + ' ORDER BY ' + @SortColumn + ' ' + @SortDirc
				END
				 
			--PRINT @Query
			--PRINT @AllColumns
			--print @QueryFrom
			EXEC (@Query + @AllColumns + @QueryFrom)

			IF(@IsDownload = 0)
				BEGIN
					SET @Query = ''
					SET @Query = 'SELECT ' + @Columns + ' FROM #ReportBuilderData WHERE RowNumber > ' + CAST(@FirstRec AS VARCHAR) + ' AND RowNumber < ' + CAST(@LastRec AS VARCHAR)
					EXEC (@Query)

					SELECT ISNULL(MAX(RowNumber), 0) AS TotalRows  FROM #ReportBuilderData
					DROP TABLE #ReportBuilderData
				END
		END
	ELSE
		BEGIN		
			IF(@IsDownload = 0)
				BEGIN
					CREATE TABLE #ReportBuilderData1
					(
						RowNumber INT IDENTITY(1,1), UserType NVARCHAR(40), UserId NVARCHAR(50), UserName NVARCHAR(100), UserSapCustomerNo NVARCHAR(100), UserCustomerName NVARCHAR(200), UserRoleName NVARCHAR(64)
					)

					SET @Query = 'INSERT INTO #ReportBuilderData1 (' + @UserColumns + ') '
				END
			
			SET @Query = @Query + ' SELECT DISTINCT ' + @UserColumns + ' FROM (
			SELECT 
				r.UserType
				,u.UserId
				,u.UserName
				,c.SAPCustomerNumber AS UserSapCustomerNo
				,p.FirstName AS UserCustomerName 
				,r.RoleName AS UserRoleName
			FROM Users u(nolock)
				LEFT JOIN IDXUserRole ur(nolock) ON u.UserId = ur.UserID
				LEFT JOIN [Role] r(nolock) ON ur.RoleID = r.RoleId
				LEFT JOIN IDXUserCustomer as uc(nolock) ON u.UserId = uc.UserId 
				LEFT JOIN Customer c(nolock) ON uc.CustomerId = c.CustomerId
				LEFT JOIN PersonalInformation p(nolock) ON c.PersonalInformationId = p.PersonalInformationId '

			IF(@CustomerId <> '')
				BEGIN
					SET @Query = @Query + ' WHERE C.CustomerId in ('+ @CustomerId + ') '
				END
			
			SET @Query = @Query + ' ) AS T'

			IF(@SortColumn != '' AND @SortDirc != '')
				BEGIN
					SET  @Query =  @Query + ' ORDER BY ' + @SortColumn + ' ' + @SortDirc
				END
			
			EXEC (@Query)

			IF(@IsDownload = 0)
				BEGIN
					SET @Query = ''
					SET @Query = 'SELECT ' + @UserColumns + ' FROM #ReportBuilderData1 WHERE RowNumber > ' + CAST(@FirstRec AS VARCHAR) + ' AND RowNumber < ' + CAST(@LastRec AS VARCHAR)

					--PRINT @Query
					EXEC (@Query)
					SELECT ISNULL(MAX(RowNumber), 0) AS TotalRows FROM #ReportBuilderData1
					DROP TABLE #ReportBuilderData1
				END
		END
END










