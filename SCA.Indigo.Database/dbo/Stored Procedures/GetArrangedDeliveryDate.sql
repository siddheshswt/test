﻿

-- =============================================
-- Author:		Arvind Nishad
-- Create date: 27-03-2015
-- Description:	Get Arranged Delivery date
-- =============================================
--exec [GetArrangedDeliveryDate] @CustomerId=9, @DerivedDate='2015-05-26' ,@OrderCreationTime='15:45'
--select * from HolidayProcessAdmin
CREATE Proc [dbo].[GetArrangedDeliveryDate] 
(
@CustomerId bigint,
@DerivedDate datetime,
@OrderCreationTime varchar(50)
)
as begin

select 
CustomerId,
NDD,
ArrangedNDD,
StartTime,
EndTime
from HolidayProcessAdmin with (nolock) 
where CustomerId=@CustomerId and  NDD=@DerivedDate
and  @OrderCreationTime between CONVERT(VARCHAR(5),StartTime,108) and CONVERT(VARCHAR(5),EndTime,108)
End




