﻿/******************************
** File:    DeleteIDXCustomerProduct
** Name:	DeleteIDXCustomerProduct
** Desc:	Deletes a Product assigned to the customer
** Auth:	Arvind
** Date:	17/12/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	17/12/2014		Arvind		Created SP
** 2	15/06/2015		Sagar		Added Document commnet
** 3	29/07/2014		Arvind		Delete all the product which are not assigned to customer
** 4	04/10/2016		Siddhesh	Added transaction and try catch block
*******************************/
CREATE PROCEDURE [dbo].[DeleteIDXCustomerProduct]
	-- Add the parameters for the stored procedure here
	@CustomerId BIGINT,
	@ProductId VARCHAR(MAX)
AS
BEGIN
	BEGIN TRY
		DECLARE @Result VARCHAR(200)
		DECLARE @ResultMessage VARCHAR(200)
		DECLARE @CurrentDate DATETIME = GETDATE()
		DECLARE @ModifiedBy VARCHAR(100) = CAST(NEWID() AS VARCHAR(100))

		BEGIN TRANSACTION
			-- Insert statements for procedure here
			DELETE IDXCustomerProduct 
			WHERE 
			CustomerId=@CustomerId
			AND ProductId NOT IN (SELECT val FROM dbo.Split(@ProductId, ','))
			SELECT @@ROWCOUNT			

		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: DeleteIDXCustomerProduct' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @ModifiedBy
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage

		SELECT 0 
	END CATCH
END



