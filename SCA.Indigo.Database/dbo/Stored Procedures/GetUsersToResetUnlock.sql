﻿/*
===============================================================================
 Author      :	 Saurabh Mayekar
 Create date :   11-01-2016
 Description :	 Gets Users
===============================================================================

===============================================================================
		Modified Date		Modified By			Purpose
	1.	18-01-2016          Saurabh Mayekar     Include Row Count in O/P
===============================================================================

EXEC GetUsersToResetUnlock 2,100,''
*/
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



CREATE PROC [dbo].[GetUsersToResetUnlock]
(
    @pageNum               BIGINT = 0,
    @recordCount           BIGINT = 0,
    @searchText            VARCHAR(500) = '',
	@SortColumn			   VARCHAR(50) = 'UserName',
	@SortOrder			   VARCHAR(10) = 'DESC'		
)
AS
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.

SET NOCOUNT ON;
SET FMTONLY OFF; --Uncomment this, whenever input or output columns are changed and require EF Update

SET @SortOrder = (SELECT UPPER(@SortOrder))
PRINT @SortOrder
PRINT @SortColumn
DECLARE @start BIGINT = 0,
        @end   BIGINT = 0,
        @TotalRecords BIGINT = 0

SET @start = ((@recordCount * @pageNum) - @recordCount) + 1
SET @end   = (@start-1) + @recordCount 

SELECT  @TotalRecords [TotalRecords], 
        ROW_NUMBER() OVER(
		ORDER BY 
		CASE
		WHEN @SortOrder <> 'ASC' THEN ''
	    WHEN @SortColumn = 'UserName' THEN UserName
	    END ASC,
		CASE
        WHEN @SortOrder <> 'DESC' THEN ''
        WHEN @SortColumn = 'UserName' THEN UserName
        END DESC,
		CASE
		WHEN @SortOrder <> 'ASC' THEN ''
	    WHEN @SortColumn = 'IsAccountLocked' THEN IsAccountLocked
	    END ASC,
		CASE
        WHEN @SortOrder <> 'DESC' THEN ''
        WHEN @SortColumn = 'IsAccountLocked' THEN IsAccountLocked
        END DESC
	    ) [RowNumber], UserId, UserName, '' [Name], '' [EmailId], '' [PinCode], CASE WHEN IsAccountLocked = 1 THEN 'Y' ELSE 'N' END [IsAccountLocked], 
       CASE WHEN IsAccountLocked = 1 THEN 'Unlock' ELSE 'Lock' END [Locked], 'Reset' [Reset]
INTO  #UserResult
FROM  Users(NOLOCK)
WHERE LOWER(UserName) LIKE LOWER('%' + @searchText + '%')

SELECT @TotalRecords = COUNT(*) FROM #UserResult

UPDATE #UserResult
SET    TotalRecords = @TotalRecords

SELECT * FROM #UserResult
WHERE RowNumber BETWEEN @start and @end

END
