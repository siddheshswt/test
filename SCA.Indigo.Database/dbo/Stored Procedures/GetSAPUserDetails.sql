﻿


-- =============================================
-- Author:		Arvind Nishad
-- Create date: 29-12-2014
-- Description:	Get SAP User Details
-- =============================================

-- =============================================
--		Modified Date			Modified By			Purpose
--	1.   19-Mar-2015			Sagar Yerva			Added NOLOCK
-- =============================================

--select * from Users where username='SAPUser'


CREATE Procedure [dbo].[GetSAPUserDetails]
as begin
select 
--cast( UserId as varchar(100)) UserId,
UserId,
UserName,
ValidFromDate,
ValidToDate
from Users WITH (NOLOCK) where  username='SAPUser'
end


