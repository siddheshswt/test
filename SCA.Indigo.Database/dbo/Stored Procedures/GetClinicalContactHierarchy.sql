﻿-- =============================================
-- Author:		Avinash
-- Create date: 11-05-2015
-- Description:	Get Clinical Contact Hierarchy with Linkage
-- =============================================
-- exec [GetClinicalContactHierarchy] @CustomerId =9 

CREATE PROCEDURE [dbo].[GetClinicalContactHierarchy]
       @CustomerId BIGINT
AS
BEGIN

       SET NOCOUNT ON;

 WITH Hierarchy(ChildListId, Generation, ParentListId, HierarchyListId)
AS
(
    SELECT ChildListId, 0, ParentListId, HierarchyListId
        FROM ClinicalContactRoleMapping AS FirtGeneration WITH(NOLOCK)
        WHERE ParentListId = 0  AND CustomerId = @CustomerId 
    UNION ALL
    SELECT NextGeneration.ChildListId,  Parent.Generation + 1, Parent.ChildListId, NextGeneration.HierarchyListId
        FROM ClinicalContactRoleMapping AS NextGeneration WITH(NOLOCK)
        INNER JOIN Hierarchy AS Parent 
              ON NextGeneration.ParentListId = Parent.ChildListId
              WHERE  NextGeneration.CustomerId = @CustomerId
)

SELECT  Hierarchy.ChildListId, Hierarchy.ParentListId, Hierarchy.HierarchyListId, ROW_NUMBER() OVER ( PARTITION BY Hierarchy.HierarchyListId ORDER BY Hierarchy.Generation DESC ) AS RowNum
        ,CASE WHEN ISNULL(Relate.ParentListId,'') = '' THEN 1 ELSE 0 END IsNode
    FROM Hierarchy 
	LEFT JOIN Hierarchy AS Relate
	ON   Hierarchy.HierarchyListId = Relate.HierarchyListId
	AND  Hierarchy.ChildListId = Relate.ParentListId
    ORDER BY Hierarchy.HierarchyListId ASC, Hierarchy.Generation DESC
    OPTION(MAXRECURSION 32767)
END