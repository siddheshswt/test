﻿-- =============================================
-- Author:		Gaurav Lad
-- Create date: 29th May 2015
-- Description:	Used to fetch the Interaction description values 
-- =============================================
CREATE PROCEDURE [dbo].[GetInteractionDescription]
	
	@TranslationId BIGINT,
	@LanguageId BIGINT
AS
BEGIN

	SELECT traprox.Text FROM TranslationProxy traprox INNER JOIN Translation trans
	on traprox.TransProxyID = trans.TransProxyID
	where trans.TranslationId = @TranslationId and trans.LanguageID = @LanguageId ;	
	
END



