﻿
/******************************
** File:    GetCareHomeReportDetailsList
** Name:	GetCareHomeReportDetailsList
** Desc:	Get Carehome Report Patient Details List
** Auth:	Siddhesh
** Date:	09Dec2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	
**	
**	
*******************************/

--===========================================================================
--exec GetCareHomeReportDetailsList 25

CREATE PROCEDURE [dbo].[GetCareHomeReportDetailsList] 
	-- Add the parameters for the stored procedure here
	@CareHomeId BigInt = 0
	
AS
BEGIN
SET NOCOUNT ON;
IF 1=0 BEGIN
    SET FMTONLY OFF
END	
--temporary table to pass details of patient and product		
CREATE TABLE #CarehomeReport 
(ProductCount VARCHAR(10),
 PatientName VARCHAR(100),
 PatientSAPId VARCHAR(20),
 DateOfBirth VARCHAR(10),
 PatientStatus VARCHAR(3),
 LastAssessDate VARCHAR(10),
 NextAssessDate VARCHAR(10),
 SAPProductId VARCHAR(20),
 ProductDescription VARCHAR(100),
 AssPPD VARCHAR(10),
 ActPPD VARCHAR(10),
 Frequency VARCHAR(10),
 NDD VARCHAR(10),
 Comment VARCHAR(10))



DECLARE 
		@PId BIGINT,
		@PName VARCHAR(100),
        @PSAPId VARCHAR(20), 
		@PDOB VARCHAR(10), 
		@PStatus VARCHAR(3), 
		@PLastAsses VARCHAR(10), 
		@PNextAssess VARCHAR(10),
		@ProductId VARCHAR(20),
		@ProductDescription VARCHAR(100),
		@AssPPD VARCHAR(10),
		@ActPPD VARCHAR(10),
		@Frequency VARCHAR(10),
		@NDD VARCHAR(10),
		@RowCount INT,
		@ProductCount INT,
		@Flag INT = 0
    

--1st cursor to get all patient details
DECLARE PatientData CURSOR
FOR 
SELECT 
         patient.PatientId
		,pInfo.FirstName +' '+ pInfo.LastName 		
		,CAST(CAST (patient.SAPPatientNumber AS BIGINT) AS VARCHAR(20)) 		
		,CONVERT(VARCHAR(10), patient.DateofBirth, 105) 
		,CASE WHEN patient.PatientStatus = 10020 THEN 'ACT' ELSE 'STO' END 
		,CONVERT(VARCHAR(10), patient.AssessmentDate, 105) 
		,CONVERT(VARCHAR(10), patient.NextAssessmentDate, 105) 									
	FROM Patient patient (NOLOCK)	
	INNER JOIN PersonalInformation pInfo (NOLOCK) ON patient.PersonalInformationId = pInfo.PersonalInformationId		
	INNER JOIN CareHome CHome (NOLOCK) ON CHome.CareHomeId = patient.CareHomeId	
	INNER join List patientType (NOLOCK)  on patientType.ListId=patient.PatientType	
	WHERE CHome.CareHomeId = @CareHomeId
	AND patient.PatientStatus in (10020, 10021)
	order by pInfo.LastName

OPEN PatientData		

FETCH NEXT FROM PatientData
 INTO @PId, @PName, @PSAPId, @PDOB, @PStatus, @PLastAsses, @PNextAssess

WHILE @@FETCH_STATUS = 0
BEGIN

SET @ProductCount = (SELECT COUNT(*) FROM IDXPrescriptionProduct ( NOLOCK) WHERE PrescriptionId = (SELECT PrescriptionId FROM Prescription (NOLOCK) WHERE PatientId = @PId) AND Status = 'Active')
--2nd cursor to get prescription product of patient 
 DECLARE ProductData CURSOR
 FOR
 SELECT 	
		 prod.BaseMaterial
		,prod.DescriptionUI
		,idxPrescriptionProduct.AssessedPadsPerDay
	    ,idxPrescriptionProduct.ActPPD		
		,idxPrescriptionProduct.Frequency
		,CONVERT(VARCHAR(10), idxPrescriptionProduct.NextDeliveryDate, 105) 						
	FROM 
	Patient patient (NOLOCK)		
	INNER JOIN CareHome CHome (NOLOCK) ON CHome.CareHomeId = patient.CareHomeId			
	INNER JOIN Prescription prescription  (NOLOCK) ON prescription.PatientId=Patient.PatientID
	INNER JOIN IDXPrescriptionProduct idxPrescriptionProduct  (NOLOCK) ON prescription.PrescriptionId=idxPrescriptionProduct.PrescriptionId	
	AND idxPrescriptionProduct.Status='Active'
	LEFT OUTER JOIN Product prod ON prod.ProductId=idxPrescriptionProduct.ProductId
	WHERE CHome.CareHomeId = @CareHomeId
	AND patient.PatientId = @PId

SET @Flag = 0

OPEN ProductData 

FETCH NEXT FROM ProductData
 INTO @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NDD


WHILE @@FETCH_STATUS = 0
BEGIN
	SET @Flag = @Flag + 1
	IF(@Flag = 1)
	BEGIN
		INSERT INTO #CarehomeReport VALUES
		(@ProductCount, @PName, @PSAPId, @PDOB, @PStatus, @PLastAsses, @PNextAssess, @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NDD, '')
	END
	ELSE
	BEGIN
		INSERT INTO #CarehomeReport VALUES
		('', '', '', '', '', '', '', @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NDD, '')
	END	
FETCH NEXT FROM ProductData
INTO @ProductId, @ProductDescription, @AssPPD, @ActPPD, @Frequency, @NDD
END



CLOSE ProductData
DEALLOCATE ProductData		

FETCH NEXT FROM PatientData
INTO @PId, @PName, @PSAPId, @PDOB, @PStatus, @PLastAsses, @PNextAssess

END

CLOSE PatientData
DEALLOCATE PatientData		

SELECT ProductCount, PatientName, PatientSAPId, DateOfBirth, PatientStatus, LastAssessDate, NextAssessDate, SAPProductId, ProductDescription, AssPPD, ActPPD, Frequency, NDD, Comment FROM #CarehomeReport

DROP TABLE #CarehomeReport
				
END

