﻿
/******************************
** File:    GetPostCodeMatrixForMassDataChanges
** Name:	GetPostCodeMatrixForMassDataChanges
** Desc:	Adds a new category in List
** Auth:	Get Post code matrix For Mass Changes
** Date:	12/06/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	19/06/2015		Gaurav		Added Document Comment		
** 2	25/06/2015		Gaurav		Modified the I/P paramerter Customer Id to SAPCustomerNumber 
*******************************/
--============================================================================										

CREATE PROCEDURE [dbo].[GetPostCodeMatrixForMassDataChanges]
(
	-- Add the parameters for the stored procedure here
	@CustomerId VARCHAR(2000) 
) 	
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		SELECT 
				--CT.SAPCustomerNumber AS SAPCustomerNumber,
				CT.SAPCustomerNumber AS CustomerId,
				PostcodeMatrixID,
				Monday,
				Tuesday,
				Wednesday,
				Thursday,
				Friday,
				RoundID,
				Round,
				Postcode,
				IsActive
		FROM 
				PostcodeMatrix PT (NOLOCK)  INNER JOIN Customer CT 
				ON CT.CustomerId = PT.CustomerID
		WHERE 
				CT.CustomerId IN (SELECT Value
					FROM dbo.SplitToTable(@CustomerId))
END



