﻿











/******************************



** File:    GetListDetails_usp



** Name:	GetListDetails_usp



** Desc:	Stored Procedure to get the lists based on listtype and languageId



** Auth:	Mamatha



** Date:	20/11/2014







********************************



** Change History



********************************



** PR	Date			Author		Description	



** --	--------		-------		-----------------------------------



**	1	24/03/2015		Arvind		Changed Join to incorporate multilingual List



**	2   20/04/2015		Siddhesh	changed languageId to @languageId



**	3	15/06/2015		Sagar		Added Document Comment		



**	4	25/06/2015		Arvind		Added ListCode column


**	5	04/10/2016		Siddhesh	Added WHERE CLAUSE


*******************************/







--===============================================================================



/*			declare @listTypeId varchar(100) = '1,2,3,4,5,6,7,8,9,10,15,17',



			@languageId INT = 1	;



	exec GetListDetails_usp '3', 1



*/







CREATE PROCEDURE [dbo].[GetListDetails_usp] 



	@listTypeId varchar(100),



	@languageId INT	



AS



BEGIN



	-- SET NOCOUNT ON added to prevent extra result sets from



	-- interfering with SELECT statements.



	SET NOCOUNT ON;

DECLARE @Prospect BIGINT = 10101

SELECT 



		lst.ListTypeId	



	,	lst.ListId



	,	lst.ListIndex



	,CASE WHEN t.TranslationType IS NULL



	THEN tp.Text



	ELSE  t.TranslationType 



	END DisplayText



	,lst.Description ListCode



FROM List  lst (NOLOCK)



INNER JOIN ListType lstType (NOLOCK) ON lst.ListTypeId = lstType.ListTypeId



INNER JOIN TranslationProxy tp (NOLOCK) ON tp.TransProxyID = lst.TransProxyId



LEFT OUTER JOIN Translation t (NOLOCK) ON t.TransProxyID = tp.TransProxyID



AND t.LanguageID = CONVERT(VARCHAR(10),@languageId)





 WHERE



 (@listTypeId ='-1' or lst.ListTypeId in (SELECT Value FROM SplitToTable(@listTypeId)))


 AND lst.ListId != @Prospect




order by lst.ListTypeId, lst.ListIndex







END












