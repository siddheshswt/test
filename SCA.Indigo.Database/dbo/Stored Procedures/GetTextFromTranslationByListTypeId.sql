﻿-- =============================================
-- Author:		Saurabh Mayekar
-- Create date: 28 Sep 2016
-- Description:	Used to fetch the translation type based on language id for ListTypeId
-- =============================================

-- EXEC GetTextFromTranslationByListTypeId 3,1
CREATE PROCEDURE [dbo].[GetTextFromTranslationByListTypeId]
	@Id BIGINT,
	@LanguageId BIGINT
AS
BEGIN

	SELECT l.ListId, t.TranslationType
	FROM ListType(NOLOCK) lt
	INNER JOIN List(NOLOCK) l
	ON lt.ListTypeId = l.ListTypeId
	INNER JOIN TranslationProxy(NOLOCK) tp
	ON tp.TransProxyID = l.TransProxyId
	INNER JOIN Translation(NOLOCK) t
	ON tp.TransProxyID = t.TransProxyID
	WHERE lt.ListTypeId = @Id
	AND   t.LanguageID = @LanguageId
	
END