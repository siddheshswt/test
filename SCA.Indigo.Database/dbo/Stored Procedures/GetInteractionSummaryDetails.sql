﻿/******************************

** File:    GetInteractionSummaryDetails

** Name:	GetInteractionSummaryDetails

** Desc:	Used to Retrive the data for Interaction Summary

** Auth:	Gaurav

** Date:	14/04/2015



********************************

** Change History

********************************

** PR	Date			Author		Description	

** --	--------		-------		-----------------------------------

** 1	15/06/2015		Sagar		Added Document Comment		

** 2	22/7/2015		Gaurav		Added (NOLOCK) 

** 3    16/06/2015		Siddhesh	Changed the sorting order to interaction.CreatedDate desc, interaction.ModifiedDate desc

** 4    08/07/2016		Vikas		Added column Resolved, ResolvedBy, ResolvedDate, ClosedBy, ClosedDate

** 5    13/07/2016      Saurabh     Join changed for ResolvedBy, ClosedBy
** 6    02/09/2016      Saurabh     Added IsInteractionViewed Column in Select
** 7    10/03/2016      Saurabh     Added IsBtnEnabled Column added

*******************************/
-- =============================================================
-- Sample: exec GetInteractionSummaryDetails 0,0,0,0,'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
-- Sample: exec GetInteractionSummaryDetails 0,23,0,173732,'00000000-0000-0000-0000-000000000000'

/*
exec GetInteractionSummaryDetails 0,23,0,183861,'00000000-0000-0000-0000-000000000000','5D9EE643-274D-40F8-9387-009292BDC75F'
exec GetInteractionSummaryDetails 0,23,0,173732,'00000000-0000-0000-0000-000000000000','5D9EE643-274D-40F8-9387-009292BDC75F'

exec GetInteractionSummaryDetails 0,23,0,183861, '00000000-0000-0000-0000-000000000000','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
exec GetInteractionSummaryDetails 0,23,0,173732, '00000000-0000-0000-0000-000000000000','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
*/

CREATE PROCEDURE [dbo].[GetInteractionSummaryDetails]



	-- Add the parameters for the stored procedure here



	@CustomerId bigint,



	@PatientId bigint,



	@CarehomeId bigint,



	@InteractionId bigint,



	@AssignedTo uniqueidentifier ,
	@UserId VARCHAR(100) = NULL



AS



BEGIN



	-- SET NOCOUNT ON added to prevent extra result sets from



	-- interfering with SELECT statements.



	SET NOCOUNT ON;

	DECLARE @CarehomeUserId VARCHAR(100) = ''
	SELECT @CarehomeUserId = CAST(ISNULL(UserId,'') AS VARCHAR(100)) FROM Users WHERE IsCarehomeUser = 1 AND UserId = @UserId




	IF(@AssignedTo !=  '00000000-0000-0000-0000-000000000000')



	BEGIN



		Select



				interaction.InteractionId,



				interaction.InteractionTypeId,



				interaction.InteractionSubTypeId,



				interaction.StatusId,



				StatusList.DefaultText AS StatusDefaultText,



				interaction.CustomerId,



				interaction.PatientId,



				interaction.CarehomeId as CareHomeId,



				interaction.AssignToUserId,



				interaction.Description,



				interaction.DisplayAsAlert,



				interaction.Resolved,
				u3.UserName AS ResolvedBy,
				interaction.ResolvedDate,
				u2.UserName AS ClosedBy,
				interaction.ClosedDate,



				patinet.SAPPatientNumber AS SAPPatientNumber,



				interactiontype.DefaultText AS InteractionCategory,



				ineractionSubTypeId.DefaultText AS InteractionSubTypeCategory,



				customer.SAPCustomerNumber AS SAPCustomerNumber,



				customer.Transport_Company AS LogisticProviderName,



				Carehome.SAPCareHomeNumber AS SAPCarehomeNumber,



				customerPersonalInformation.FirstName + ' '+ ISNULL(customerPersonalInformation.LastName,'') AS CustomerName,



				carehomePersonalInformation.FirstName + ' '+ ISNULL(carehomePersonalInformation.LastName,'') AS CareHomeName,



				PaitientPersonalInformation.FirstName +' '+ISNULL(PaitientPersonalInformation.LastName,'') AS PatientName,				



				u.UserName AS CreatedBy,



				interaction.CreatedDate AS CreatedDate,



				u1.UserName AS ModifiedBy,



				interaction.ModifiedDate AS ModifiedDate,
				interaction.IsInteractionViewed,
				1 IsBtnEnabled		



				from Interaction AS interaction (NOLOCK)



				inner join List AS interactiontype (NOLOCK)



					on interaction.InteractionTypeId = interactiontype.ListId



				inner join List AS ineractionSubTypeId (NOLOCK)



					on interaction.InteractionSubTypeId = ineractionSubTypeId.ListId



				inner join List AS StatusList (NOLOCK)



					on interaction.StatusId = StatusList.ListId



				inner join Customer AS customer (NOLOCK)



					on interaction.CustomerId = customer.CustomerId



				left outer join Users as u (NOLOCK)



					on interaction.CreatedBy = u.UserId



				left outer join Users as u1 (NOLOCK)



					on interaction.ModifiedBy = u1.UserId
				left outer join Users as u2 (NOLOCK)
					on interaction.ClosedBy = u2.UserId
				left outer join Users as u3 (NOLOCK)
					on interaction.ResolvedBy = u3.UserId



				LEFT join Patient as patinet (NOLOCK)



					on interaction.PatientId = patinet.PatientId



				LEFT join Carehome AS Carehome (NOLOCK)



					on interaction.CareHomeId = Carehome.CareHomeId



				LEFT join PersonalInformation AS customerPersonalInformation (NOLOCK)



					on customer.PersonalInformationId = customerPersonalInformation.PersonalInformationId



				LEFT join PersonalInformation AS carehomePersonalInformation (NOLOCK)



					on Carehome.PersonalInformationId = carehomePersonalInformation.PersonalInformationId



				Left join PersonalInformation AS PaitientPersonalInformation (NOLOCK)



					on patinet.PersonalInformationId = PaitientPersonalInformation.PersonalInformationId



				WHERE INTERACTION.AssignToUserId = @AssignedTo



					AND interaction.DisplayAsAlert = 1



				--ORDER BY interaction.ModifiedDate desc , interaction.CreatedDate desc 



				ORDER BY interaction.CreatedDate desc, interaction.ModifiedDate desc



		END



	ELSE IF(@InteractionId != 0)



		BEGIN







		Select



				interaction.InteractionId,



				interaction.InteractionTypeId,



				interaction.InteractionSubTypeId,



				interaction.StatusId,



				StatusList.DefaultText AS StatusDefaultText,



				interaction.CustomerId,



				interaction.PatientId,



				interaction.CarehomeId as CareHomeId,



				interaction.AssignToUserId,



				interaction.Description,



				interaction.DisplayAsAlert,



				interaction.Resolved,
				u3.UserName AS ResolvedBy,
				interaction.ResolvedDate,
				u2.UserName AS ClosedBy,



				interaction.ClosedDate,



				patinet.SAPPatientNumber AS SAPPatientNumber,



				interactiontype.DefaultText AS InteractionCategory,



				ineractionSubTypeId.DefaultText AS InteractionSubTypeCategory,



				customer.SAPCustomerNumber AS SAPCustomerNumber,



				customer.Transport_Company AS LogisticProviderName,



				Carehome.SAPCareHomeNumber AS SAPCarehomeNumber,



				customerPersonalInformation.FirstName + ' '+ ISNULL(customerPersonalInformation.LastName,'') AS CustomerName,



				carehomePersonalInformation.FirstName + ' '+ ISNULL(carehomePersonalInformation.LastName,'') AS CareHomeName,



				PaitientPersonalInformation.FirstName +' '+ISNULL(PaitientPersonalInformation.LastName,'') AS PatientName,				



				u.UserName AS CreatedBy,



				interaction.CreatedDate AS CreatedDate,



				u1.UserName AS ModifiedBy,



				interaction.ModifiedDate AS ModifiedDate,
				interaction.IsInteractionViewed,
				CASE WHEN ISNULL(idxUserCarehomeC.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%' OR ISNULL(idxUserCarehomeP.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
				     THEN 1
					 ELSE 0
			    END IsBtnEnabled
				from Interaction AS interaction (NOLOCK)



				inner join List AS interactiontype (NOLOCK)



					on interaction.InteractionTypeId = interactiontype.ListId



				inner join List AS ineractionSubTypeId (NOLOCK)



					on interaction.InteractionSubTypeId = ineractionSubTypeId.ListId



				inner join List AS StatusList (NOLOCK)



					on interaction.StatusId = StatusList.ListId



				inner join Customer AS customer (NOLOCK)



					on interaction.CustomerId = customer.CustomerId



				left outer join Users as u (NOLOCK)



					on interaction.CreatedBy = u.UserId



				left outer join Users as u1 (NOLOCK)



					on interaction.ModifiedBy = u1.UserId

				left outer join Users as u2 (NOLOCK)
					on interaction.ClosedBy = u2.UserId
				left outer join Users as u3 (NOLOCK)
					on interaction.ResolvedBy = u3.UserId



				LEFT join Patient as patinet (NOLOCK)
				ON interaction.PatientId = patinet.PatientId
				LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehomeP
				ON  idxUserCarehomeP.CarehomeId = patinet.CareHomeId



				LEFT join Carehome AS Carehome (NOLOCK)
				ON  interaction.CareHomeId = Carehome.CareHomeId
				LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehomeC
				ON  idxUserCarehomeC.CarehomeId = Carehome.CareHomeId



				LEFT join PersonalInformation AS customerPersonalInformation (NOLOCK)



					on customer.PersonalInformationId = customerPersonalInformation.PersonalInformationId



				LEFT join PersonalInformation AS carehomePersonalInformation (NOLOCK)



					on Carehome.PersonalInformationId = carehomePersonalInformation.PersonalInformationId



				Left join PersonalInformation AS PaitientPersonalInformation (NOLOCK)



					on patinet.PersonalInformationId = PaitientPersonalInformation.PersonalInformationId



		WHERE INTERACTION.InteractionId = @InteractionId



		--ORDER BY interaction.ModifiedDate desc , interaction.CreatedDate desc 



		ORDER BY interaction.CreatedDate desc, interaction.ModifiedDate desc







		END



	



	ELSE IF(@PatientId != 0)



		BEGIN







		Select



				interaction.InteractionId,



				interaction.InteractionTypeId,



				interaction.InteractionSubTypeId,



				interaction.StatusId,



				StatusList.DefaultText AS StatusDefaultText,



				interaction.CustomerId,



				interaction.PatientId,



				interaction.CarehomeId as CareHomeId,



				interaction.AssignToUserId,



				interaction.Description,



				interaction.DisplayAsAlert,



				interaction.Resolved,



				u3.UserName AS ResolvedBy,
				interaction.ResolvedDate,
				u2.UserName AS ClosedBy,
				interaction.ClosedDate,



				patinet.SAPPatientNumber AS SAPPatientNumber,



				interactiontype.DefaultText AS InteractionCategory,



				ineractionSubTypeId.DefaultText AS InteractionSubTypeCategory,



				customer.SAPCustomerNumber AS SAPCustomerNumber,



				customer.Transport_Company AS LogisticProviderName,



				Carehome.SAPCareHomeNumber AS SAPCarehomeNumber,



				customerPersonalInformation.FirstName + ' '+ ISNULL(customerPersonalInformation.LastName,'') AS CustomerName,



				carehomePersonalInformation.FirstName + ' '+ ISNULL(carehomePersonalInformation.LastName,'') AS CareHomeName,



				PaitientPersonalInformation.FirstName +' '+ISNULL(PaitientPersonalInformation.LastName,'') AS PatientName,				



				u.UserName AS CreatedBy,



				interaction.CreatedDate AS CreatedDate,



				u1.UserName AS ModifiedBy,



				interaction.ModifiedDate AS ModifiedDate,
				interaction.IsInteractionViewed,
				1 IsBtnEnabled
				from Interaction AS interaction (NOLOCK)



				inner join List AS interactiontype (NOLOCK)



					on interaction.InteractionTypeId = interactiontype.ListId



				inner join List AS ineractionSubTypeId (NOLOCK)



					on interaction.InteractionSubTypeId = ineractionSubTypeId.ListId



				inner join List AS StatusList (NOLOCK)



					on interaction.StatusId = StatusList.ListId



				inner join Customer AS customer (NOLOCK)



					on interaction.CustomerId = customer.CustomerId



				left outer join Users as u (NOLOCK)



					on interaction.CreatedBy = u.UserId



				left outer join Users as u1 (NOLOCK)



					on interaction.ModifiedBy = u1.UserId
				left outer join Users as u2 (NOLOCK)
					on interaction.ClosedBy = u2.UserId
				left outer join Users as u3 (NOLOCK)
					on interaction.ResolvedBy = u3.UserId



				LEFT join Patient as patinet (NOLOCK)



					on interaction.PatientId = patinet.PatientId



				LEFT join Carehome AS Carehome (NOLOCK)



					on interaction.CareHomeId = Carehome.CareHomeId



				LEFT join PersonalInformation AS customerPersonalInformation (NOLOCK)



					on customer.PersonalInformationId = customerPersonalInformation.PersonalInformationId



				LEFT join PersonalInformation AS carehomePersonalInformation (NOLOCK)



					on Carehome.PersonalInformationId = carehomePersonalInformation.PersonalInformationId



				Left join PersonalInformation AS PaitientPersonalInformation (NOLOCK)



					on patinet.PersonalInformationId = PaitientPersonalInformation.PersonalInformationId



		WHERE interaction.CustomerId = @CustomerId and interaction.PatientId = @PatientId



		--ORDER BY interaction.ModifiedDate desc , interaction.CreatedDate desc 



		ORDER BY interaction.CreatedDate desc, interaction.ModifiedDate desc



	



		END



	else



		BEGIN



	



	Select



				interaction.InteractionId,



				interaction.InteractionTypeId,



				interaction.InteractionSubTypeId,



				interaction.StatusId,



				StatusList.DefaultText AS StatusDefaultText,



				interaction.CustomerId,



				interaction.PatientId,



				interaction.CarehomeId as CareHomeId,



				interaction.AssignToUserId,



				interaction.Description,



				interaction.DisplayAsAlert,



				interaction.Resolved,
				u3.UserName AS ResolvedBy,
				interaction.ResolvedDate,
				u2.UserName AS ClosedBy,



				interaction.ClosedDate,



				patinet.SAPPatientNumber AS SAPPatientNumber,



				interactiontype.DefaultText AS InteractionCategory,



				ineractionSubTypeId.DefaultText AS InteractionSubTypeCategory,



				customer.SAPCustomerNumber AS SAPCustomerNumber,



				customer.Transport_Company AS LogisticProviderName,



				Carehome.SAPCareHomeNumber AS SAPCarehomeNumber,



				customerPersonalInformation.FirstName + ' '+ ISNULL(customerPersonalInformation.LastName,'') AS CustomerName,



				carehomePersonalInformation.FirstName + ' '+ ISNULL(carehomePersonalInformation.LastName,'') AS CareHomeName,



				PaitientPersonalInformation.FirstName +' '+ISNULL(PaitientPersonalInformation.LastName,'') AS PatientName,				



				u.UserName AS CreatedBy,



				interaction.CreatedDate AS CreatedDate,



				u1.UserName AS ModifiedBy,



				interaction.ModifiedDate AS ModifiedDate,
				interaction.IsInteractionViewed,
				1 IsBtnEnabled
				from Interaction AS interaction (NOLOCK)



				inner join List AS interactiontype  (NOLOCK)



					on interaction.InteractionTypeId = interactiontype.ListId



				inner join List AS ineractionSubTypeId (NOLOCK)



					on interaction.InteractionSubTypeId = ineractionSubTypeId.ListId



				inner join List AS StatusList (NOLOCK)



					on interaction.StatusId = StatusList.ListId



				inner join Customer AS customer (NOLOCK)



					on interaction.CustomerId = customer.CustomerId



				left outer join Users as u (NOLOCK)



					on interaction.CreatedBy = u.UserId



				left outer join Users as u1 (NOLOCK)



					on interaction.ModifiedBy = u1.UserId
				left outer join Users as u2 (NOLOCK)
					on interaction.ClosedBy = u2.UserId
				left outer join Users as u3 (NOLOCK)
					on interaction.ResolvedBy = u3.UserId



				LEFT join Patient as patinet (NOLOCK)



					on interaction.PatientId = patinet.PatientId



				LEFT join Carehome AS Carehome (NOLOCK)



					on interaction.CareHomeId = Carehome.CareHomeId



				LEFT join PersonalInformation AS customerPersonalInformation (NOLOCK)



					on customer.PersonalInformationId = customerPersonalInformation.PersonalInformationId



				LEFT join PersonalInformation AS carehomePersonalInformation (NOLOCK)



					on Carehome.PersonalInformationId = carehomePersonalInformation.PersonalInformationId



				Left join PersonalInformation AS PaitientPersonalInformation (NOLOCK)



					on patinet.PersonalInformationId = PaitientPersonalInformation.PersonalInformationId



		where interaction.CustomerId = @CustomerId and interaction.CarehomeId = @CarehomeId



		--ORDER BY interaction.ModifiedDate desc , interaction.CreatedDate desc 



		ORDER BY interaction.CreatedDate desc, interaction.ModifiedDate desc



		END    



END