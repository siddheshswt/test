﻿

CREATE PROCEDURE [dbo].[XSCAUpdateprescriptionNDD] 
as
BEGIN
BEGIN TRY
              
BEGIN TRAN --OPEN TRANSACTION

	   DECLARE @IDXPrescriptionProductId bigint
	   DECLARE @presfreq				 bigint
	   DECLARE @presndd					 date
	   DECLARE @ldd						 date
	   DECLARE @NDD						 date
	   
       DECLARE cursor_updateNDDLDDforPrescription CURSOR -- Declare cursor
       LOCAL SCROLL STATIC 
       FOR                          
			select pro.IDXPrescriptionProductId
				  ,pro.Frequency presfreq
				  ,pro.NextDeliveryDate presndd
				  ,max(SAPOrderDeliveryDate) ldd
				  ,max((SAPOrderDeliveryDate) + (pro.Frequency*7)) NDD
			from Prescription pre
				inner join IDXPrescriptionProduct pro on pro.PrescriptionId = pre.PrescriptionId
				inner join Orders ord on ord.PatientId = pre.PatientId
				inner join IDXOrderProduct opro on opro.OrderId = ord.OrderId
				inner join List list on list.ListId = ord.OrderStatus
			where pro.ProductId = opro.ProductId
			  and ord.OrderType = 10053
			  and ord.OrderStatus = 10048
			group by pro.IDXPrescriptionProductId
				   ,pro.Frequency
				   ,pro.NextDeliveryDate
			having max(SAPOrderDeliveryDate) + (pro.Frequency*7) != pro.NextDeliveryDate
                     
              OPEN cursor_updateNDDLDDforPrescription
              FETCH NEXT FROM cursor_updateNDDLDDforPrescription
              INTO @IDXPrescriptionProductId,@presfreq,@presndd,@ldd,@NDD

              WHILE @@FETCH_STATUS = 0
              BEGIN                
                   
                     
					 UPDATE IDXPrescriptionProduct  
					 SET DeliveryDate = @ldd
					    ,NextDeliveryDate = @NDD
					  WHERE IDXPrescriptionProductId = @IDXPrescriptionProductId
                     

                     FETCH NEXT FROM cursor_updateNDDLDDforPrescription
                     INTO @IDXPrescriptionProductId,@presfreq,@presndd,@ldd,@NDD  
              END

       CLOSE cursor_updateNDDLDDforPrescription -- close the cursor
       DEALLOCATE cursor_updateNDDLDDforPrescription -- Deallocate the cursor 

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE UPDATING Prescription NDD LDD'

END CATCH
END



