﻿




-- =============================================
-- Author:		Arvind Nishad
-- Create date: 06-01-2015
-- Description:	Update Patient with SAP PatinetNumber
-- =============================================
-- exec [UpdateSAPPatientId] @PatientId =8 ,@SAPPatientNumber='SAP1515'

CREATE Proc [dbo].[UpdateSAPPatientId]
(
@PatientId bigint,
@SAPPatientNumber varchar(50),
@CustomerType varchar(50)
)
as begin

BEGIN TRY
			BEGIN TRANSACTION;	
Declare @Result int =0 
Declare @ResultMessage varchar(200)
-- Changed 20/3/2015 DMF not in QA 
--Declare @ModifiedBy varchar(100)
--select @ModifiedBy= UserId from Users where UserName='SAPUser'
if @CustomerType='PA'
begin
		
		Update Patient set SAPPatientNumber=@SAPPatientNumber
		-- Changed 20/3/2015 DMF not in QA 
		--ModifiedBy= @ModifiedBy,
		---ModifiedDate=GETDATE()

		where PatientId=@PatientId
		set @ResultMessage='Patient SAPId Updated'		
		select @Result=@@ROWCOUNT
end
else if @CustomerType='HO'
begin	
		Update CareHome set SAPCareHomeNumber=@SAPPatientNumber
		-- Changed 20/3/2015 DMF not in QA 
		--ModifiedBy= @ModifiedBy,
		--ModifiedDate=GETDATE()		
		where CareHomeId=@PatientId
		set @ResultMessage='Carehome SAPId Updated'
		select @Result=@@ROWCOUNT		
end


COMMIT TRANSACTION;
	END TRY

		BEGIN CATCH
			ROLLBACK TRANSACTION;
			select @Result=-1, @ResultMessage=ERROR_MESSAGE()			
		END CATCH;	

		Select @Result Result, @ResultMessage ResultMessage
end





