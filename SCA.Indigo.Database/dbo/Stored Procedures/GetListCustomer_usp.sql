﻿
-- =============================================
-- Author:		
-- Create date: <27-11-2014>
-- Description:	<Procedure to Display ID and Name w.r.t Customer in Popup e.g exec GetListCustomer '1'>
-- =============================================

--exec GetListCustomer_usp '1'


CREATE PROCEDURE [dbo].[GetListCustomer_usp]
(
-- Add the parameters for the stored procedure here
@ListId as int
)
As
Begin
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SELECT 0 AS ListId,'' as Text,0 as ListTypeId ,0 as CustomerMedicalStaffAnalysisId,'' AS StaffId
Union
SELECT        CustomerMedicalStaff.ListId ListId, Cast(CustomerMedicalStaff.Text as nvarchar) Text, List.ListTypeId ListTypeId,CustomerMedicalStaff.CustomerMedicalStaffAnalysisId CustomerMedicalStaffAnalysisId,cast(CustomerMedicalStaff.StaffId as nvarchar) StaffId
FROM          IDXPatientMedicalStaff PatientMedicalStaff (nolock) INNER JOIN
              CustomerMedicalStaffAnalysis  CustomerMedicalStaff(nolock) ON PatientMedicalStaff.CustomerMedicalStaffId = CustomerMedicalStaff.CustomerMedicalStaffAnalysisId INNER JOIN
              List (NOLOCK) ON CustomerMedicalStaff.ListId = List.ListId WHERE CustomerMedicalStaff.ListId=@ListId 
END




