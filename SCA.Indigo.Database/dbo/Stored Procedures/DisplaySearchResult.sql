﻿/******************************
** File:    DisplaySearchResult
** Name:	DisplaySearchResult
** Desc:	Search for Customer/CareHome/Patient/PatientType/Order
** Auth:	Avinash
** Date:	05/05/2015

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
--	1	05/06/2015		Avinash		Implemented paging
--	2	10/06/2015		Avinash		Modified NULL check condition
--	3	07/07/2015		Avinash		Modified DOB search after Optimization
--	4	16/07/2015		Sagar		Added Header
--  5   03/08/2015      Jitendra K	Changed parallel queries to use single thread
--	6	06/08/2015		Jitendra K	Removed Case statements from CTE table
--  7   10/02/2016      Saurabh     Dynamic sort order column
--	8	09/05/2016		Siddhesh	Increased lenght from address line 1 and address line to in #searchresult table from varchar(50) to varchar(100)
--  9   28/06/2016      Saurabh     Included TelephoneNo In Google Search
--  10  22/07/2016      Saurabh     Included Corrspondence PostCode In Google Search
--  11  25/07/2016      Saurabh     House Name field Increaed to 100
--  12  19/09/2016      Saurabh     Added left join vwIDXUserCarehome for CarehomeUsers
--  13  19/09/2016      Siddhesh    Comment SAPDeliveryNo 
--  14  20/09/2016      Saurabh     Added join with IDXOrderProduct table for SAPDeliveryNo 
--  15  21/09/2016      Saurabh     Increased Query1 from @Query1  VARCHAR(4000) to Query1 VARCHAR(MAX)
--  12  21/09/2016		Jitendra K	Performance tuning enhancements
--  13  26/09/2016      Saurabh     Removed ObjectType Condition to not include Customers
*******************************/

-- ==========================================================================================

--exec DisplaySearchResult 'HU14 3HG'
--exec DisplaySearchResult 'RUA,Fernando,DE,SCA,INDIGO,SS'
--EXEC DisplaySearchResult 1,100,'01/03/1983', 'NULL', 'NULL', -1, 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153','LastName','ASC'

CREATE PROCEDURE [dbo].[DisplaySearchResult] (
				@pageNum bigint ,
				@recordCount bigint ,
				@inputStr VARCHAR(100),
                @CustomerId varchar(100),
                @PatientStatus varchar(100),  
                @PatientType bigint=-1, @UserId uniqueidentifier,
				@SortCoulmn VARCHAR(100),
				@SortOrder VARCHAR(10)
				)
AS 
BEGIN
SET NOCOUNT ON
--variable declaration for Paging
	 DECLARE @start bigint = 0,
         @end bigint = 0

		DECLARE @CarehomeUserId VARCHAR(100) = ''
		DECLARE @QueryCarehomeUser VARCHAR(1000) = ''

		SELECT @CarehomeUserId = ISNULL(CAST(UserId AS VARCHAR(100)), '') FROM Users WHERE IsCarehomeUser = 1 AND UserId = @UserId

		IF @CarehomeUserId <> ''
			SET @QueryCarehomeUser = ' AND ISNULL(idxUserCarehome.CarehomeUsers, '''') LIKE ''%' + @CarehomeUserId + '%'' '

SET @start = ((@recordCount * @pageNum) - @recordCount) + 1
SET @end = (@start-1) + @recordCount 

DECLARE @TotalRecords BIGINT 
--end of paging variable

DECLARE @sql varchar(max)
DECLARE @ValueStr  VARCHAR(100)
DECLARE @InitialCondition VARCHAR(max)
DECLARE @searchCondition VARCHAR(500)
DECLARE @Count  INT, @Loop INT = 1, @totalSum INT
DECLARE @Query1  VARCHAR(MAX),@Query2  VARCHAR(1000),@Query3  VARCHAR(1000),
		@Query4  VARCHAR(1000), @Query5  VARCHAR(1000), @Query6  VARCHAR(1000),
		@Query  VARCHAR(4000),@Combination VARCHAR(1000)

		IF @CustomerId = 'NULL'
		BEGIN
			SET @CustomerId=''
		END
	
		IF @PatientStatus = 'NULL'
		BEGIN
			SET @PatientStatus= ''
		END
		IF @CustomerId=''
			SET @Query2='  1=1 '
		ELSE
			SET @Query2 = '  CustomerID IN ( ' + convert(VARCHAR(500),@CustomerId) + ') '
			
		IF @PatientStatus = ''
			SET @Query3=' AND 1=1 '
		ELSE
			SET @Query3 = ' AND PatientStatusID IN ( ' + convert(VARCHAR(500),@PatientStatus) + ') '
			
		IF @PatientType = -1
			Set @Query4 = ' AND 1=1'
		ELSE
			 --Set @Query4 = ' AND PatientTypeId = ' + convert(VARCHAR(500),@PatientType )
			 Set @Query4 = ' AND PatientTypeId IN ( ' + convert(VARCHAR(500),@PatientType) + ') '
			 
	IF LEN(@inputStr)= 0
	IF CHARINDEX('SCA Administrator',(SELECT R.UserType+' '+R.RoleName FROM Role AS R(NOLOCK) INNER JOIN IDXUserRole AS UR(NOLOCK) ON R.RoleId = UR.RoleID WHERE UR.UserID=@UserId)) > 0 
		BEGIN
			
				SET @Sql = 'WITH CTEPatientDetails AS
				(
				
						select ROW_NUMBER() OVER(ORDER BY ' + @SortCoulmn + ' ' + @SortOrder + ') as RowNumber , * from 
						(
							SELECT DISTINCT PatientId, CustomerId, TitleId,PatientName,FirstName,LastName,TelephoneNumber, 
						HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
						DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,
						SAPOrderNo,SAPDeliveryNo,OrderActivated,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
						PatientTypeId, PatientStatusId,CareHomeId,objectType,HasActiveOrder
						FROM dbo.PatientOrderSearchResult
						WHERE ' + @Query2 +  @Query3 + @Query4 + 
						' )AS T) 											

						SELECT *,(select COUNT(*) from CTEPatientDetails) as TotalRecords FROM CTEPatientDetails where RowNumber BETWEEN ' + cast(@start AS varchar)+ ' and ' + cast(@end as varchar)				
				EXEC (@sql)
			
				
		END
	ELSE
		BEGIN

				SET @Sql = 'WITH CTEPatientDetails AS
				(
						select ROW_NUMBER() OVER(ORDER BY ' + @SortCoulmn + ' ' + @SortOrder + ') as RowNumber , * from 
						(
							SELECT DISTINCT PatientId, CustomerId, TitleId,PatientName,FirstName,LastName,TelephoneNumber, 
						HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
						DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,
						SAPOrderNo,SAPDeliveryNo,OrderActivated,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
						PatientTypeId, PatientStatusId,PatientOrderSearchResult.CareHomeId [CareHomeId],objectType,HasActiveOrder
						FROM dbo.PatientOrderSearchResult
						LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
						ON  idxUserCarehome.CarehomeId = PatientOrderSearchResult.CareHomeId
						WHERE ' + @Query2 +  @Query3 + @Query4 + '
						and(CustomerId IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId=' + '''' + CAST(@userId AS VARCHAR(150)) + '''' + ')) ' +
						@QueryCarehomeUser + '
						)AS T) 
	
						SELECT *,(select COUNT(*) from CTEPatientDetails) as TotalRecords FROM CTEPatientDetails where RowNumber BETWEEN ' + cast(@start AS varchar)+ ' and ' + cast(@end as varchar)
		EXEC (@sql)
		
		END

	ELSE

	BEGIN
-- PatientType Convert
--Temporary table to capture all the words separately
	CREATE TABLE #tmpvalues      
(   
	intIndex INT IDENTITY(1,1),
	intProc INT,
	subStr VARCHAR(100)
)
--Temporary table to store all the possible combinations
	CREATE TABLE #tmpCombinations
(   
	colText1 VARCHAR(50),
	colText2 VARCHAR(50),   
	colText3 VARCHAR(50),
	colText4 VARCHAR(50),
	colText5 VARCHAR(50),
	colText6 VARCHAR(50),
	colText7 VARCHAR(50)
)  
--get the sub-strings(words) from input statement into a temp table
WHILE LEN(@inputStr) > 0
BEGIN
    SET @ValueStr = '%' + LEFT(@inputStr, ISNULL(NULLIF(CHARINDEX(',', @inputStr) - 1, -1),
                       LEN(@inputStr)))
    SET @inputStr = SUBSTRING(@inputStr,ISNULL(NULLIF(CHARINDEX(',', @inputStr), 0),
                       LEN(@inputStr)) + 1, LEN(@inputStr))    
    INSERT INTO #tmpvalues VALUES (@Loop, @ValueStr )
    SET @Loop=@Loop+1	
	IF(@Loop = 8)-- COMBINATION OF 6 COMMA SEPARATED WORDS ONLY
	 BREAK;
END

	DECLARE @loopCount int
	SET @loopCount = (SELECT COUNT(1) FROM #tmpvalues)
	if(@loopCount<7)
BEGIN
	WHILE @loopCount<7
	BEGIN
	 SET @loopCount = @loopCount+1
	 INSERT INTO #tmpvalues VALUES (@loopCount, '' )
	END
END

-- Jitendra 9/21 new cursor introduced Start
	Set @InitialCondition = ' AND ('
	DECLARE @MyCursor CURSOR
		SET @MyCursor = CURSOR FAST_FORWARD
		FOR
		SELECT distinct SubStr  FROM   #tmpvalues where subStr <> ''
			OPEN @MyCursor
				FETCH NEXT FROM @MyCursor
		INTO @searchCondition
		
	WHILE @@FETCH_STATUS = 0	
	BEGIN
	
		SET @InitialCondition= @InitialCondition + ' PatientSearch Like ' + '''' + @searchCondition  + '%' + '''' + ' OR '
	FETCH NEXT FROM @MyCursor
	INTO @searchCondition
	END
	CLOSE @MyCursor
	DEALLOCATE @MyCursor 
	
	
	IF SUBSTRING(@InitialCondition,LEN(@InitialCondition)-1,LEN(@InitialCondition)) ='OR '
 		SET @InitialCondition= SUBSTRING(@InitialCondition,1,LEN(@InitialCondition)-2) + ')'
	IF @InitialCondition = ' AND ('
		SET @InitialCondition = ' '
-- Jitendra 9/21 new cursor introduced End

	SELECT @Count = MAX(intINDEX) FROM #tmpvalues
	SET @Loop=1
	--Set an integer values for each words
--This will be used to filter the combinations in which any two words are repating
	DECLARE @tempIntAdd INT--Addition factor
	SET @tempIntAdd =@Loop*@Count
	WHILE @Loop <= (@Count-1)
BEGIN
    DECLARE @tempIntProc INT
    SELECT @tempIntProc = intProc from #tmpvalues where intIndex=@Loop
    UPDATE #tmpvalues SET intProc=@tempIntProc+ @tempIntAdd WHERE intIndex = @Loop+1
    SET @Loop = @Loop+1
    SET @tempIntAdd=@tempIntAdd*2
END
--
	SET @Loop=1
	SET @Query1='INSERT INTO #tmpCombinations SELECT '
	SET @Query2='ALL_COMBINATIONS FROM'
	SET @Query3=' '
	SET @Query4=' WHERE'
	SET @Query5='('
	SET @Query6=')'
-- Generate the dynamic query to get permutations and combination of individual words
	WHILE @Loop <= @Count
BEGIN
	SELECT @ValueStr = subStr FROM #tmpvalues WHERE intIndex=@Loop	
	SET @Query1 = @Query1 + 'T' + CAST(@Loop AS VARCHAR) + '.subStr '
	if(@Loop<@Count)
	 SET @Query1=@Query1 + ', ' 
    SET @Query3= @Query3 + '#tmpvalues ' + 'T' + CAST(@Loop AS VARCHAR)
    if(@Loop<@Count)
	 SET @Query3=@Query3+ ', '
    SET @Query5=@Query5 + 'T'+ CAST(@Loop AS VARCHAR) + '.intProc'
    if(@Loop<@Count)
	 SET @Query5=@Query5+ ' + '
    SET @Loop = @Loop+1
END
	SELECT @totalSum = SUM(intProc) from #tmpvalues
--Create final query
	SET @Query = @Query1+@Query2+@Query3+@Query4+@Query5+@Query6  + ' =' + CAST(@totalSum AS VARCHAR)
--Execute the dynamic Query

	EXECUTE(@Query)

	--SELECT * FROM #tmpCombinations --R

	DECLARE @searchText1 varchar(50), @searchText2 varchar(50), @searchText3 varchar(50), @searchText4 varchar(50), @searchText5 varchar(50) 

	--DECLARE @searchResult TABLE (
	CREATE TABLE #searchResult (
	PatientId bigint,CustomerId bigint,TitleId varchar(50),PatientName varchar(100),FirstName varchar(50),LastName varchar(50),TelephoneNumber varchar(50), 
	HouseNumber varchar(50),HouseName varchar(100),AddressLine1 varchar(100),AddressLine2 varchar(100),City varchar(50),County varchar(50),Country varchar(50), 
	PostCode varchar(50), DateofBirth Datetime,PatientType varchar(50),PatientStatus varchar(50),PatientCustomer bigint,CareHomeName varchar(50),
	DeliveryDate Datetime,SAPOrderNo varchar(50),SAPDeliveryNo varchar(50),OrderActivated int,CustomerName varchar(100),
	SAPPatientNumber VARCHAR(100),SAPCustomerNumber VARCHAR(100), PatientTypeId bigint, PatientStatusId bigint, CareHomeId bigint , objectType int , HasActiveOrder bit)

--------------------------------------------------------

--Jitendra K 9/21/2016 introduced #Step_1 table
	CREATE TABLE #Step_1 (
	PatientId bigint,CustomerId bigint,TitleId varchar(50),PatientName varchar(100),FirstName varchar(50),LastName varchar(50),TelephoneNumber varchar(50), 
	HouseNumber varchar(50),HouseName varchar(100),AddressLine1 varchar(100),AddressLine2 varchar(100),City varchar(50),County varchar(50),Country varchar(50), 
	PostCode varchar(50), DateofBirth Datetime,PatientType varchar(50),PatientStatus varchar(50),PatientCustomer bigint,CareHomeName varchar(50),
	DeliveryDate Datetime,SAPOrderNo varchar(50),SAPDeliveryNo varchar(50),OrderActivated int,CustomerName varchar(100),
	SAPPatientNumber VARCHAR(100),SAPCustomerNumber VARCHAR(100), PatientTypeId bigint, PatientStatusId bigint, CareHomeId bigint , objectType int , HasActiveOrder bit, PatientSearch Varchar(600))
--------------------------------------------------------

SET @Query1= ' AND ('
	--DECLARE @MyCursor CURSOR
		SET @MyCursor = CURSOR FAST_FORWARD
		FOR
		SELECT distinct colText1+ colText2+colText3+ colText4+ colText5 + colText6 + colText7  FROM   #tmpCombinations
			OPEN @MyCursor
				FETCH NEXT FROM @MyCursor
		INTO @searchText1

	WHILE @@FETCH_STATUS = 0	
	BEGIN
	
		SET @Query1= @Query1 + ' PatientSearch Like ' + '''' + @searchText1  + '%' + '''' + ' OR '
	FETCH NEXT FROM @MyCursor
	INTO @searchText1
	END
	CLOSE @MyCursor
	DEALLOCATE @MyCursor 
	IF SUBSTRING(@Query1,LEN(@Query1)-1,LEN(@Query1)) ='OR '
 		SET @Query1= SUBSTRING(@Query1,1,LEN(@Query1)-2) + ')'

	IF CHARINDEX('SCA Administrator',(select R.UserType+' '+R.RoleName from Role as R(nolock) INNER JOIN IDXUserRole as UR(nolock) ON R.RoleId = UR.RoleID where UR.UserID=@UserId)) > 0 
	BEGIN

		SET @sql=' select PatientId, CustomerId, TitleId,PatientName,FirstName,LastName,TelephoneNumber, HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
		DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,SAPOrderNo,SAPDeliveryNo,OrderActivated,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
		PatientTypeId, PatientStatusId,CareHomeId,objectType,HasActiveOrder, PatientSearch
		from dbo.PatientOrderSearchResult
		WHERE 1=1 '
-- Jitendra K 9/21 Performance tuning code started
		SET @sql = @sql + @InitialCondition
		
		
		INSERT INTO #Step_1
		Execute (@sql)
		
		CREATE INDEX IX ON #Step_1(PatientSearch)
		SET @sql=' SELECT PatientId, CustomerId, TitleId,PatientName,FirstName,LastName,TelephoneNumber, HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
		DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,SAPOrderNo,SAPDeliveryNo,OrderActivated,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
		PatientTypeId, PatientStatusId,CareHomeId,objectType,HasActiveOrder
		 FROM #Step_1 WHERE 1=1 '
-- Jitendra K 9/21 Performance tuning code Ended

		IF LEN(@Query1) > 0
			SET @sql=@sql + @Query1
		 SET @sql = @sql + ' option (maxdop  1)'
		 INSERT INTO #searchResult
		 Execute (@sql)
		 
	END
	ELSE
	BEGIN
	
		SET @sql=' select PatientId, CustomerId,TitleId,PatientName,FirstName,LastName,TelephoneNumber, HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
		DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,SAPOrderNo,SAPDeliveryNo,OrderActivated ,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
		PatientTypeId, PatientStatusId,PatientOrderSearchResult.CareHomeId [CareHomeId],objectType,HasActiveOrder,PatientSearch
		from dbo.PatientOrderSearchResult
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = PatientOrderSearchResult.CareHomeId
		where 1=1 and(PatientCustomer IN (select CustomerId from IDXUserCustomer where UserId=''' + Cast(@userId as varchar(150))  + '''))' + 
		@QueryCarehomeUser
-- Jitendra K 9/21 Performance tuning code started
		SET @sql = @sql + @InitialCondition
		
		
		INSERT INTO #Step_1
		Execute (@sql)
		
		CREATE INDEX IX_1 ON #Step_1(PatientSearch)
		SET @sql=' SELECT PatientId, CustomerId, TitleId,PatientName,FirstName,LastName,TelephoneNumber, HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
		DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,SAPOrderNo,SAPDeliveryNo,OrderActivated,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
		PatientTypeId, PatientStatusId,CareHomeId,objectType,HasActiveOrder
		 FROM #Step_1 WHERE 1=1 '
-- Jitendra K 9/21 Performance tuning code Ended	
		IF LEN(@Query1) > 0
			SET @sql=@sql + @Query1
		 SET @sql = @sql + ' option (maxdop  1)'
		 
		 INSERT INTO #searchResult			  
		 Execute (@sql)
		 
	END
	Create Index IX ON #searchResult(CustomerId,PatientStatusId,PatientTypeId)
	BEGIN
	IF @PatientStatus = 'NULL'
		BEGIN
			SET @PatientStatus= ''
		END
		IF @CustomerId=''
			SET @Query2='  1=1 '
		ELSE
			SET @Query2 = '  CustomerID IN ( ' + convert(VARCHAR(500),@CustomerId) + ') '

		IF @PatientStatus = ''
			SET @Query3=' AND 1=1 '
		ELSE
			SET @Query3 = ' AND PatientStatusID IN ( ' + convert(VARCHAR(500),@PatientStatus) + ') '
		IF @PatientType = -1
			Set @Query4 = ' AND 1=1'
		ELSE
			 --Set @Query4 = ' AND PatientTypeId = ' +@PatientType 
			 Set @Query4 = ' AND PatientTypeId IN ( ' + convert(VARCHAR(500),@PatientType) + ') '	

--			 SELECT * FROM #searchResult

	SET @sql = 'WITH CTEPatientDetails AS
	(
			select ROW_NUMBER() OVER(ORDER BY ' + @SortCoulmn + ' ' + @SortOrder + ') as RowNumber , * from 
			(
				SELECT DISTINCT PatientId, CustomerId, TitleId,PatientName,FirstName,LastName,TelephoneNumber, 
			HouseNumber,HouseName,AddressLine1,AddressLine2,City,County,Country, PostCode,
			DateofBirth,PatientType,PatientStatus,PatientCustomer,CareHomeName,DeliveryDate,
			SAPOrderNo,SAPDeliveryNo,OrderActivated,CustomerName,SAPPatientNumber,SAPCustomerNumber, 
			PatientTypeId, PatientStatusId,CareHomeId,objectType,HasActiveOrder
			FROM #searchResult
			WHERE ' + @Query2 +  @Query3 + @Query4 + '
			)AS T) 
	
			SELECT *, (select COUNT(1) from CTEPatientDetails) as TotalRecords FROM CTEPatientDetails where RowNumber BETWEEN ' + cast(@start AS varchar)+ ' and ' + cast(@end as varchar)
			
		EXEC (@sql)
		

		
	END
	
	--Drop temporary tables
	DROP TABLE #tmpCombinations
	--DROP TABLE #tmpCombinations1
	DROP TABLE #Step_1
	DROP TABLE #tmpvalues
	
	
	DROP TABLE #searchResult
	
	
  END
END