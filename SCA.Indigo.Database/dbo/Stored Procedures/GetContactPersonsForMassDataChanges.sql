﻿-- =============================================
-- Author:		<Jagdish>
-- Create date: <22 June 2015>
-- Description:	<Return ContactPerson table data>
-- Sample : GetContactPersonsForMassDataChanges '9',1 
-- Sample : GetContactPersonsForMassDataChanges '9',1 

-- Sample : GetContactPersonsForMassDataChanges '9', 2, '000000030330,0001150901,000000150983,000000150985'
-- Sample : GetContactPersonsForMassDataChanges '9', 2, ''

-- Sample : GetContactPersonsForMassDataChanges '9', 2, '0000030330'
-- Sample : GetContactPersonsForMassDataChanges '9', 3, '0000893572,000123456789,0000858352'
-- Sample : GetContactPersonsForMassDataChanges '9', 3, '0000893572,000123456789,0000858352'
-- Sample : GetContactPersonsForMassDataChanges '9', 3, ''
-- =============================================
CREATE PROCEDURE [dbo].[GetContactPersonsForMassDataChanges]
	@customerId varchar(20),
	@type BIGINT,	
	@sapIds VARCHAR(max) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	-- Type >> 1 >> Customer, 2 >> Patient, 3 >> Carehome
	
	--declare @type varchar(10)
	--declare @sapCustomerNumberId BIGINT
	--declare @sapIds VARCHAR(max) = NULL	
	--SET @customerId = 0000857002
	--SET @type = 'p'
	--SET @sapIds  = '000000030330,0001150901,000000150983,000000150985'
	--set @type = 'CH'
	--set @sapIds ='0000893572,000123456789,0000858352'

	IF(@type = 1)
	BEGIN
		SELECT 
			 ISNULL(cp.ContactPersonId,0) AS ContactPersonId
			, Convert(varchar(50),c.SAPCustomerNumber) AS CustomerId
			,Convert(varchar(50),cp.PatientId) AS PatientId
			,Convert(varchar(50),cp.CarehomeId) AS CarehomeId			
			,pinfo.FirstName
			,pinfo.LastName
			,pinfo.JobTitle
			,pinfo.TelephoneNumber
			,pinfo.MobileNumber
			,pinfo.Email
			,cp.Remarks			
		FROM ContactPerson cp
		INNER JOIN PersonalInformation pinfo (NOLOCK) ON cp.PersonalInformationId = pinfo.PersonalInformationId
		INNER JOIN Customer c (NOLOCK) ON cp.CustomerId = c.CustomerId
		WHERE c.CustomerId = @customerId
	END
	ELSE IF(@type = 2)
	BEGIN
		IF (isnull(@sapIds,'') = '' OR LEN(@sapIds) = 0)
		BEGIN
			SELECT 
				 ISNULL(cp.ContactPersonId,0) AS ContactPersonId
				,Convert(varchar(50),cp.CustomerId) AS CustomerId
				,Convert(varchar(50),cp.PatientId) AS PatientId
				,Convert(varchar(50),cp.CarehomeId) AS CarehomeId
				,pinfo.FirstName
				,pinfo.LastName
				,pinfo.JobTitle
				,pinfo.TelephoneNumber
				,pinfo.MobileNumber
				,pinfo.Email	
				,cp.Remarks		 
			FROM ContactPerson cp 
			INNER JOIN PersonalInformation pinfo (NOLOCK) ON cp.PersonalInformationId = pinfo.PersonalInformationId
			WHERE PatientId in (SELECT PatientId FROM Patient (NOLOCK) WHERE CustomerId = @customerId)
		END
		ELSE
		BEGIN
			SELECT 
				 ISNULL(cp.ContactPersonId,0) AS ContactPersonId
				,Convert(varchar(50),cp.CustomerId) AS CustomerId
				,Convert(varchar(50),cp.PatientId) AS PatientId
				,Convert(varchar(50),cp.CarehomeId) AS CarehomeId
				,pinfo.FirstName
				,pinfo.LastName
				,pinfo.JobTitle
				,pinfo.TelephoneNumber
				,pinfo.MobileNumber
				,pinfo.Email
				,cp.Remarks		
			FROM ContactPerson cp 
			INNER JOIN PersonalInformation pinfo (NOLOCK) ON cp.PersonalInformationId = pinfo.PersonalInformationId
			WHERE PatientId in (	SELECT PatientId FROM Patient (NOLOCK) WHERE CustomerId = @customerId AND SAPPatientNumber 
									IN ( SELECT Value FROM dbo.SplitToTable(@sapIds))
								)
		END
	END
	ELSE IF(@type = 3)
	BEGIN
		IF (isnull(@sapIds,'') = '' OR LEN(@sapIds) = 0)
		BEGIN
			SELECT 
				 ISNULL(cp.ContactPersonId,0) AS ContactPersonId
				,Convert(varchar(50),cp.CustomerId) AS CustomerId
				,Convert(varchar(50),cp.PatientId) AS PatientId
				,Convert(varchar(50),cp.CarehomeId) AS CarehomeId
				,pinfo.FirstName
				,pinfo.LastName
				,pinfo.JobTitle
				,pinfo.TelephoneNumber
				,pinfo.MobileNumber
				,pinfo.Email
				,cp.Remarks						
			FROM ContactPerson cp 
			INNER JOIN PersonalInformation pinfo (NOLOCK) ON cp.PersonalInformationId = pinfo.PersonalInformationId
			WHERE CarehomeId in (SELECT CarehomeId FROM Carehome (NOLOCK) WHERE CustomerId = @customerId)
		END
		ELSE
		BEGIN
			SELECT 
				 ISNULL(cp.ContactPersonId,0) AS ContactPersonId
				,Convert(varchar(50),cp.CustomerId) AS CustomerId
				,Convert(varchar(50),cp.PatientId) AS PatientId
				,Convert(varchar(50),cp.CarehomeId) AS CarehomeId
				,pinfo.FirstName
				,pinfo.LastName
				,pinfo.JobTitle
				,pinfo.TelephoneNumber
				,pinfo.MobileNumber
				,pinfo.Email
				,cp.Remarks						
			FROM ContactPerson cp 
			INNER JOIN PersonalInformation pinfo (NOLOCK) ON cp.PersonalInformationId = pinfo.PersonalInformationId
			WHERE CarehomeId in (	SELECT CarehomeId FROM Carehome (NOLOCK) WHERE CustomerId = @customerId AND SAPCareHomeNumber 
									IN ( SELECT Value FROM dbo.SplitToTable(@sapIds))
								)
		END
	END
END
