﻿-- =============================================
-- Author:		Arvind
-- Create date: 2015-08-13
-- Description:	Save user login details
-- =============================================
/******************************
********************************
** Change History
********************************
** PR	Date			Author		Description	
** 1	04-10-2016		Siddhesh	Added trnasaction and try catch block	
		
*******************************/
CREATE PROCEDURE [dbo].[SaveLoginUserDetails] 
	-- Add the parameters for the stored procedure here
	@userId varchar(100),
	@LoginDate datetime
AS
BEGIN
	
	DECLARE @Result VARCHAR(200)
	DECLARE @ResultMessage VARCHAR(200)
	DECLARE @CurrentDate DATETIME = GETDATE()
		
	BEGIN TRY
		BEGIN TRANSACTION
			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- Insert statements for procedure here
			INSERT INTO [dbo].[LoginUserDetails] 
			(		
				UserId,
				LoginDate
			)
			VALUES
			(
				@UserId,
				@LoginDate
			)
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: SaveLoginUserDetails' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @userId
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage

	END CATCH
END
