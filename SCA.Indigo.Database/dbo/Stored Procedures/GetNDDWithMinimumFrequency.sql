﻿/******************************
** File:    GetNDDWithMinimumFrequency
** Name:	GetNDDWithMinimumFrequency
** Desc:	Get NDD With Minimum Frequency
** Auth:	Siddhesh
** Date:	12/09/2016

********************************
** Change History
********************************
** PR	Date			Author			Description	
** --	--------		-------			-----------------------------------
    1  23-09-2016	  Siddhesh Sawant   Added isnull condition 
		
*******************************/

-- ==============================================================================================
--exec [GetNDDWithMinimumFrequency] 115192

CREATE PROCEDURE [dbo].[GetNDDWithMinimumFrequency]
@OrderId BIGINT
AS
BEGIN
	BEGIN TRY
		DECLARE @PatientId BIGINT = 0
		DECLARE @PrescriptionId BIGINT = 0
		DECLARE @MinFrequency BIGINT = 0
		DECLARE @Status VARCHAR(6)= 'ACTIVE' 
		DECLARE @Result VARCHAR(200)
		DECLARE @ResultMessage VARCHAR(200)
		DECLARE @CurrentDate DATETIME = getdate()

		SET @PatientId = (SELECT PatientId FROM Orders (NOLOCK) WHERE OrderId = @OrderId)

		SET @PrescriptionId = (SELECT PrescriptionId FROM Prescription (NOLOCK) WHERE PatientId = @PatientId)

		SET @MinFrequency = (SELECT MIN(Frequency) FROM IDXPrescriptionProduct (NOLOCK) WHERE PrescriptionId = @PrescriptionId  AND IsRemoved = 0 AND Status =  @Status)

		SELECT ISNULL(MIN(NextDeliveryDate),GETDATE()) FROM IDXPrescriptionProduct (NOLOCK) WHERE PrescriptionId = @PrescriptionId AND Frequency = @MinFrequency AND IsRemoved = 0 AND Status =  @Status

	END TRY

	BEGIN CATCH
		

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		EXEC SaveExceptionLog @UserID = 'ECDBCD8F-145A-411A-9FBD-98CC5D2F0786'
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage

		SELECT GETDATE()
		
	END CATCH;

	
END

