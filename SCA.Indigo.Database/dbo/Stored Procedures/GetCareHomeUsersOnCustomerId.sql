﻿-- =============================================
-- Author:		<Saurabh>
-- Create date: <12 Sep 2016>
-- Sample : GetCareHomeUsersOnCustomerId '23,30,31,9,11,26,27,32'
-- =============================================
CREATE PROCEDURE [dbo].[GetCareHomeUsersOnCustomerId]
@CustomerId VARCHAR(1000)
AS
BEGIN

	SELECT DISTINCT UserName, users.UserId 
	FROM Users users (NOLOCK)
	INNER JOIN IDXUserCustomer usercustomer (NOLOCK)
	ON usercustomer.UserId = users.UserId
	WHERE ',' + @CustomerId + ',' LIKE '%,' + CAST(usercustomer.CustomerId AS varchar) + ',%'
	AND   users.IsCarehomeUser = 1
	ORDER BY UserName

END