﻿

/******************************
** File:    GetPatientCustomerCareHomeStatus
** Name:	GetPatientCustomerCareHomeStatus
** Desc:	Return patient,carehome,customer status
** Auth:	Avinash
** Date:	29/05/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
*******************************/

CREATE PROCEDURE [dbo].[GetPatientCustomerCareHomeStatus] 
	-- Add the parameters for the stored procedure here
	@StatusIds varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT proxy.Text,
  STUFF((SELECT distinct ',' + CONVERT(VARCHAR(MAX), p1.[ListId])
         FROM LIST p1 WITH (NOLOCK)
              INNER JOIN TranslationProxy proxy1 WITH (NOLOCK)
               ON P1.TransProxyId = proxy1.TransProxyID
         WHERE proxy.Text = proxy1.Text
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)')
        ,1,1,'') ListId
FROM LIST p WITH (NOLOCK)
INNER JOIN TranslationProxy proxy WITH (NOLOCK)
ON P.TransProxyId = proxy.TransProxyID
WHERE ListTypeId  IN ( select Value from dbo.SplitToTable( @StatusIds)  ) AND
 ListId <> 20123
;

END



