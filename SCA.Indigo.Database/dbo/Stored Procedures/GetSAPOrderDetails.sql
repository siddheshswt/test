﻿/******************************
** File:    GetSAPOrderDetails
** Name:	GetSAPOrderDetails
** Desc:	Get Customer Patient Details
** Auth:	Arvind
** Date:	29/12/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1   19/03/2015		Sagar		Added NOLOCK
**	2   27/03/2015		Arvind		Added case condition for order type 'ZHDF'
**  3   10/04/2015		Siddhesh	Logic to select round and round id if customer pateint or carehome patient
**  4   16/04/2015		Siddhesh	Logic to select postcode if customer pateint or carehome patient
**  5   05/05/2015		Siddhesh	Added patientId column
**  6   15/05/2015		Siddhesh	Added PONumber column
**  7	15/06/2015		Sagar		Added Document Comment		
**  8	23/06/2015		Siddhesh	Added union for carehome product and one-off order
**  9	22/07/2015		Dean F	    Added condition of time '18:00:00'
**  10	25/11/2015		Siddhesh	Added carehome front end id column (CHFrontEndId)
**  11	13/01/2017		Siddhesh	Added @CountryId
*******************************/
--================================================================
-- exec GetSAPOrderDetails

CREATE Proc [dbo].[GetSAPOrderDetails] 

as begin
Declare @OrderDateCondition datetime, @ZHDF varchar(4) = 'ZHDF', @CountryId INT = 1

if convert(time,getdate()) >= '18:00:00' begin
select @OrderDateCondition = dbo.FN_DateAddBusinessDay(Getdate(),4, @CountryId)-- changed from 4 to 5 for BH
end
else
begin
select @OrderDateCondition = dbo.FN_DateAddBusinessDay(Getdate(),3, @CountryId) -- changed from 3 to 4 for BH
end 

--select @OrderDateCondition 
 



select 
patient.PatientId,
case when ord.ExcludeServiceCharge=1 and 
(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where OrderId=ord.OrderId)=(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where IsFOC=1 and OrderId=ord.OrderId)
 then @ZHDF ELSE OrderTypeList.DefaultText END OrderType,
--OrderTypeList.DefaultText OrderType,
ord.OrderId,
customer.CustomerId,
customer.SAPCustomerNumber,
customer.SalesOrg,
customer.DistrChanel,
customer.Division,
patient.SAPPatientNumber SAPId,
case when ord.CareHomeId is null then 0 else carehome.CareHomeId end as CHFrontEndId,
case when ord.CareHomeId is null then 0 else carehome.SAPCareHomeNumber end as CareHomeId,

case when ord.ExcludeServiceCharge=1 then 'X' else '' end NOSERVCHARGE,
case when 
(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where OrderId=ord.OrderId)=(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where IsFOC=1 and OrderId=ord.OrderId)
 then 'X' else '' end ORDERFOC,
'' ASAINDICATOR,
case when ord.CareHomeId is null then (select Email from PersonalInformation WITH (NOLOCK) where PersonalInformationId=patient.PersonalInformationId)
else (select Email from PersonalInformation WITH (NOLOCK) where PersonalInformationId=carehome.PersonalInformationId) end EmailTo,

--BillTo - If order Type PA then PatientId BillTo if OrderType =HO then Carehome BillTo
case when ord.CareHomeId is null then cast(patient.BillTo as varchar(50))
else cast(carehome.BillTo as varchar(50)) end BillTo,
case when ord.CareHomeId is null then patient.Round else carehome.Round end [Round],
case when ord.CareHomeId is null then patient.RoundId else carehome.RoundId end RoundId,

case when ord.CareHomeId is null then 
	(select adr.postcode from Address adr (nolock) where adr.AddressId = (select pif.AddressId from PersonalInformation pif (nolock)
		where pif.PersonalInformationId =(select pt.PersonalInformationId from Patient pt (nolock) where pt.PatientId = ord.PatientId)))
else
(select adr.postcode from Address adr (nolock) where adr.AddressId = (select pif.AddressId from PersonalInformation pif (nolock)
		where pif.PersonalInformationId =(select ch.PersonalInformationId from CareHome ch (nolock) where ch.CareHomeId = ord.CareHomeId)))
		end Postcode,

case when ord.CareHomeId is null then 	
 ISNULL
		((select cast(idx.ContactName as varchar(50)) from IDXCustomerMedicalStaffAnalysisInfo idx (nolock) where 
					idx.MedicalAnalysisListID = 27 and idx.IsFieldType = 0 
					and idx.IDXCustomerMedicalStaffAnalysisInfoID in 
					(select idxMS.CustomerMedicalStaffId from IDXPatientMedicalStaff idxMS (nolock) 
					where idxMS.PatientId = ord.PatientId)
		),'0')
else
	(select isnull(cast(ch.PurchaseOrderNo as varchar(50)),'0')  from CareHome ch (nolock) where ch.CareHomeId = ord.CareHomeId)
end PONumber


--'857304' BillTo 
from Orders ord with(nolock)
inner join List OrderTypeList WITH (NOLOCK) on ord.OrderType=OrderTypeList.ListId
inner join Patient patient WITH (NOLOCK) on patient.PatientId=ord.PatientID	
left outer join CareHome carehome WITH (NOLOCK) on ord.CareHomeId=carehome.CareHomeId and carehome.SAPCareHomeNumber is not null
inner join Customer customer WITH (NOLOCK) on customer.CustomerId=patient.CustomerId
where ord.SAPOrderNo is null 
and
patient.SAPPatientNumber is not null
and
(ord.SAPOrderDeliveryDate<=@OrderDateCondition or ord.IsRushFlag=1)
and ord.OrderStatus=10047 -- send only those orders who were genarated
and ord.OrderType<>10100 -- Don't send Sample order to SAP


UNION

--GET CAREHOME PRODUCT LEVEL AND ONE OFF ORDERS

select 
'0' as PatientId,
case when ord.ExcludeServiceCharge=1 and 
(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where OrderId=ord.OrderId)=(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where IsFOC=1 and OrderId=ord.OrderId)
 then @ZHDF ELSE OrderTypeList.DefaultText END OrderType,
--OrderTypeList.DefaultText OrderType,
ord.OrderId,
customer.CustomerId,
customer.SAPCustomerNumber,
customer.SalesOrg,
customer.DistrChanel,
customer.Division,
carehome.SAPCareHomeNumber SAPId,
case when ord.CareHomeId is null then 0 else carehome.CareHomeId end as CHFrontEndId,
case when ord.CareHomeId is null then 0 else carehome.SAPCareHomeNumber end as CareHomeId,

case when ord.ExcludeServiceCharge=1 then 'X' else '' end NOSERVCHARGE,
case when 
(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where OrderId=ord.OrderId)=(select COUNT(OrderId) from [dbo].[IDXOrderProduct] WITH (NOLOCK) where IsFOC=1 and OrderId=ord.OrderId)
 then 'X' else '' end ORDERFOC,
'' ASAINDICATOR,
case when ord.CareHomeId is null then null
else (select Email from PersonalInformation WITH (NOLOCK) where PersonalInformationId=carehome.PersonalInformationId) end EmailTo,

--BillTo - If order Type PA then PatientId BillTo if OrderType =HO then Carehome BillTo
case when ord.CareHomeId is null then cast('' as varchar(50))
else cast(carehome.BillTo as varchar(50)) end BillTo,
case when ord.CareHomeId is null then '' else carehome.Round end [Round],
case when ord.CareHomeId is null then '' else carehome.RoundId end RoundId,

case when ord.CareHomeId is null then 
	(select adr.postcode from Address adr (nolock) where adr.AddressId = (select pif.AddressId from PersonalInformation pif (nolock)
		where pif.PersonalInformationId =(select pt.PersonalInformationId from Patient pt (nolock) where pt.PatientId = ord.PatientId)))
else
(select adr.postcode from Address adr (nolock) where adr.AddressId = (select pif.AddressId from PersonalInformation pif (nolock)
		where pif.PersonalInformationId =(select ch.PersonalInformationId from CareHome ch (nolock) where ch.CareHomeId = ord.CareHomeId)))
		end Postcode,

case when ord.CareHomeId is null then 	
 ISNULL
		((select cast(idx.ContactName as varchar(50)) from IDXCustomerMedicalStaffAnalysisInfo idx (nolock) where 
					idx.MedicalAnalysisListID = 27 and idx.IsFieldType = 0 
					and idx.IDXCustomerMedicalStaffAnalysisInfoID in 
					(select idxMS.CustomerMedicalStaffId from IDXPatientMedicalStaff idxMS (nolock) 
					where idxMS.PatientId = ord.PatientId)
		),'0')
else
	(select isnull(cast(ch.PurchaseOrderNo as varchar(50)),'0')  from CareHome ch (nolock) where ch.CareHomeId = ord.CareHomeId)
end PONumber


--'857304' BillTo 
from Orders ord with(nolock)
inner join List OrderTypeList WITH (NOLOCK) on ord.OrderType=OrderTypeList.ListId
--inner join Patient patient WITH (NOLOCK) on patient.PatientId=ord.PatientID	
left outer join CareHome carehome WITH (NOLOCK) on ord.CareHomeId=carehome.CareHomeId and carehome.SAPCareHomeNumber is not null
inner join Customer customer WITH (NOLOCK) on customer.CustomerId=carehome.CustomerId
where ord.SAPOrderNo is null 
and
ord.OrderType = 20158
and
(ord.SAPOrderDeliveryDate<=@OrderDateCondition or ord.IsRushFlag=1)
and ord.OrderStatus=10047 -- send only those orders who were genarated
and ord.OrderType<>10100 
end