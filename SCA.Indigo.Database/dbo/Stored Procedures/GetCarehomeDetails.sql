﻿-- =============================================
-- Author:		Arvind Nishad
-- Create date: 08-12-2014
-- Description:	Get care home details based on CustomerId parameter
-- =============================================

--		Modified Date		Modified By			Purpose
--	1.  14-04-2015			Arvind Nishad		Added Additional Column since it will be used for carehome screen also
--	2.  27-05-2015			Sagar Yerva			Added Additional Column 'SAPCustomerNumber' in output, this will be shown in Audit Log when it is accessed from CareHome maintenance screen
--	3.  05-06-2015			Arvind Nishad		Added Column CustomerName
--	3.  27-06-2015			Siddhesh Sawant		Commented left ouer join and uncommented inner join
--	4.  29-06-2015			Siddhesh Sawant		Changes to show created by and created on instead modified by and modified date
--	5.	29-06-2015			Siddharth Dilpak	Added CommunicationPreferences comma separated
--	6.	21/07/2015			Sagar				Executed SP for DataTypeChange in CareHome table ( PurchaseOrderNo )
--  7.  15/09/2016          Saurabh             Added Left join for vwIDXUserCarehome View	

-- exec [GetCarehomeDetails] 23,0, 'B31AC98F-1B54-49EA-9938-091D8421821E'
-- exec [GetCarehomeDetails] 23,0, 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
-- exec [GetCarehomeDetails] 0,10, 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'


CREATE PROCEDURE [dbo].[GetCarehomeDetails]
(
-- Add the parameters for the stored procedure here
@CustomerId bigint ,
@careHomeId bigint =null,
@UserId VARCHAR(100) = NULL
) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CarehomeUser VARCHAR(100) = ''

	SELECT @CarehomeUser = CAST(ISNULL(UserId,'') AS VARCHAR(100)) FROM Users WHERE IsCarehomeUser = 1 AND UserId = @UserId
    -- Insert statements for procedure here	
select 
--CareHome Details
carehome.CareHomeId,
carehome.SAPCareHomeNumber,
carehome.CustomerId,
carehome.PersonalInformationId,
carehome.DeliveryFrequency,
isnull(carehome.RoundId,'') as RoundId,
isnull(careHome.Round,'') as CareHomeRound,
carehome.NextDeliveryDate,
careHome.BillTo,
pinfo.CreatedDateTime as ModifiedDate,
--careHome.ModifiedDate,
careHome.CareHomeType,
careHome.OrderType,
careHome.PurchaseOrderNo,
careHome.CareHomeStatus,
careHome.CommunicationFormat,
--PreferenceType=20145 Communication Preference
(SELECT STUFF((SELECT ','+cast(idxPreference.Preference as varchar(50)) FROM [IDXPreference] idxPreference 
WHERE PreferenceType=20145 and  idxPreference.CarehomeId = @careHomeId FOR XML PATH('')), 1, 1, '')) as CommunicationPreferences,

--Carehome Personal Information details
pinfo.FirstName CareHomeName,
pinfo.TelephoneNumber,
pinfo.MobileNumber,
pinfo.Email,
pinfo.FaxNumber,
addr.AddressId,

--CareHome Address Details
addr.HouseNumber,
addr.HouseName,
addr.AddressLine1,
addr.AddressLine2,
addr.City,
CountyList.County as County,
addr.County as CountyCode,
addr.Country,
addr.PostCode,
users.UserName,

ISNULL(cust.SAPCustomerNumber,'') AS CustomerSAPNumber,
pcustomer.FirstName CustomerName
 from CareHome carehome with(nolock) 
inner join PersonalInformation pinfo with(nolock)  on carehome.PersonalInformationId=pinfo.PersonalInformationId
inner join Address addr with(nolock)  on pinfo.AddressId =addr.AddressId
INNER JOIN Customer cust with(nolock) on cust.CustomerId = carehome.CustomerId
inner join PersonalInformation pcustomer with(nolock) on pcustomer.PersonalInformationId=cust.PersonalInformationId
LEFT JOIN CountyList with(nolock) ON addr.County = CountyList.CountyCode
--left outer join Users users on users.ModifiedBy=carehome.ModifiedBy
--- Added and tested in prod list of homes now correct and who modified was correct
 --inner join Users users on users.userid=carehome.ModifiedBy
 inner join Users users on users.userid=pinfo.CreatedById
 LEFT JOIN vwIDXUserCarehome idxUserCarehome
 ON  idxUserCarehome.CarehomeId = carehome.CareHomeId
where 1=1
and 
-- If the Status is Removed(10109), no patients can be tagged and no orders can be created
(@CustomerId = 0 or carehome.CustomerId= @CustomerId and carehome.CareHomeStatus<>10109)
and 
(@careHomeId =0 or carehome.CareHomeId=@careHomeId)
AND ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUser + '%'

END