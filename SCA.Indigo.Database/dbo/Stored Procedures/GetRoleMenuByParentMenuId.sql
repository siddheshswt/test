﻿

-- =============================================
-- Author:		Basanta
-- Create date: 2015-06-01
-- Description:	Get RoleMenu By Parent Menu Id
-- =============================================
-- Modified		ModifiedBy		Reason
-- -------		---------		------
-- 01-06-2015	Basanta Sahoo	Added IsDisaply check
--exec GetRoleMenuByParentMenuId 24,5

CREATE PROCEDURE [dbo].[GetRoleMenuByParentMenuId]
(
 @parentMenuId bigint, @roleId bigint
)
AS
BEGIN

SELECT @parentMenuId ParentMenuId, *
FROM IDXROLEMENU NOLOCK
WHERE MENUID IN (SELECT MENUID FROM MENU NOLOCK WHERE PARENTMENUID=@parentMenuId AND IsDisplay = 1)AND ROLEID=@roleId
AND( FullRights =1
OR VIEWONLY =1)

END


