﻿


CREATE PROCEDURE [dbo].[XSCAUpdateCarehomeNDD] 
as
BEGIN
BEGIN TRY
              
BEGIN TRAN --OPEN TRANSACTION

	   DECLARE @Carehomeid				 bigint
	   DECLARE @carfreq					 bigint
	   DECLARE @carndd					 date
	   DECLARE @ldd						 date
	   DECLARE @NDD						 date
	   
       DECLARE cursor_updateNDDLDDforCarehome CURSOR -- Declare cursor
       LOCAL SCROLL STATIC 
       FOR                          
				select car.CareHomeId
					  ,car.DeliveryFrequency carfreq
					  ,car.NextDeliveryDate carndd
					  ,max(SAPOrderDeliveryDate) LDD
					  ,max((SAPOrderDeliveryDate) + (car.DeliveryFrequency*7)) NDD
				from carehome car
					inner join Orders ord on ord.Carehomeid = car.CareHomeId
				where ord.OrderType = 10053
				  and ord.OrderStatus = 10048
				group by car.CareHomeId
						,car.DeliveryFrequency
						,car.NextDeliveryDate
				having max(SAPOrderDeliveryDate) + (car.DeliveryFrequency*7) != car.NextDeliveryDate
                     
              OPEN cursor_updateNDDLDDforCarehome
              FETCH NEXT FROM cursor_updateNDDLDDforCarehome
              INTO @carehomeid,@carfreq,@carndd,@ldd,@NDD

              WHILE @@FETCH_STATUS = 0
              BEGIN                                 
					 UPDATE Carehome 
					    SET NextDeliveryDate = @NDD
					  WHERE Carehomeid = @Carehomeid
                     

                     FETCH NEXT FROM cursor_updateNDDLDDforCarehome
                     INTO @carehomeid,@carfreq,@carndd,@ldd,@NDD  
              END

       CLOSE cursor_updateNDDLDDforCarehome -- close the cursor
       DEALLOCATE cursor_updateNDDLDDforCarehome -- Deallocate the cursor 

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE UPDATING Carehome NDD LDD'

END CATCH
END




