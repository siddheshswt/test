﻿-- =============================================
-- Author:		Avinash Deshpande/Siddhesh Sawant/Arvind Nishad
-- Modified date: 28-04-2015
-- Description:	Added new column for HasOrderActivated
-- =============================================

-- ==========================================================================================
--	Modifeid By			Modified on		Reason
--	Avinash				01-JUNE-2015	Made patinetId = 0 when single/double click on customer or carehome
--	Avinash				05-JUNE-2015	Implemented paging
--	Avinash				10-JUNE-2015	Modified NULL check condition.
--	Avinash				11-JUNE-2015	Modified SAPOrderNo condition.
--	Siddhesh Sawant		30-JUNE-2015	Exclude sample order type 10110 from order activated count
--	Avinash				15-July-2015	Added SAPCarehomeNumber
--	Avinash				20-July-2015	Added OrderActivated Condition For Carehome
--	Avinash				30-July-2015	modified condition for OrderActivated and hasOrderactivated
--	Jitendra			12-Aug-2015		Tuned SP by writting dynamic queries.
--  Siddhesh			13-Aug-2015		Added search condition for SAPDeliveryNumber
--  Siddhesh			10-Dec-2015		Added search condition for NHS number
--  Siddhesh			20-Jan-2016		R3 UAT Defect. Changed ' + @searchID + ' to ' + '''' + @searchID + ''''+ ' and  ' + @SapID + ' to ' + '''' + @SapID + ''''+ '
--  Saurabh             10/02/2016      Dynamic sort order column
--  Saurabh             28/06/2016      Included TelephoneNo In Google Search
--  Siddhesh			29-Jun-2016		Added search condition for patient Id
--  Saurabh             12-Aug-2016     Patient NDD Is Null When No Active Prescription
--  Siddhesh            25-Aug-2016     Changed order number check from ' + @orderIdCheck + ' to '''' + @orderIdCheck + ''''
--  Siddhesh			01-Sep-2016		Added @newSql varchar (max) to avoid @sql exceeding to its maximum limit. COSMOS ID- 1651742
-- ==========================================================================================

--EXEC DisplayPatientDetails_usp 1,100,'NULL','NULL','NULL',-1,'NULL','16223','12a368a5-ccd4-4938-899f-fd34cae62d15', 'LastName', 'ASC'
--EXEC DisplayPatientDetails_usp 1,100,'PA11 3TN,00000 000000,Ashgrove House,Mitchell','NULL','NULL',-1,'NULL','','ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153', 'LastName', 'ASC'
CREATE PROCEDURE [dbo].[DisplayPatientDetails_usp_JK]
                (-- Add the parameters for the stored procedure here
				@pageNum bigint ,--= 1,
				@recordCount bigint,-- = 100,
                @SearchText VARCHAR(50),--='PA11 3TN,00000 000000,Ashgrove House,Mitchell',
                @CustomerId VARCHAR(100),--='NULL',
                @PatientStatus VARCHAR(100),--='NULL',  
                @PatientType BIGINT,--=-1,
                @OrderNo VARCHAR(50),--='NULL',
				@searchID VARCHAR(100),--='',
				--@TotalRecords INT OUTPUT,
                @UserId uniqueidentifier,--='ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153',
				@SortCoulmn VARCHAR(100) = 'LastName',
				@SortOrder  VARCHAR(10)  = 'ASC'
                )
AS
BEGIN
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
 SET NOCOUNT ON;
 DECLARE @SQL VARCHAR(max)
 DECLARE @newSQL VARCHAR(max)
 DECLARE @Query1 VARCHAR(500), @Query2 VARCHAR(500), @Query3 VARCHAR(500), @Query4 VARCHAR(500), @Query5 VARCHAR(500), @Query6 VARCHAR(500)
 DECLARE @start bigint = 0,
         @end bigint = 0

SET @start = ((@recordCount * @pageNum) - @recordCount) + 1
SET @end = (@start-1) + @recordCount 

IF ISNULL(@SortCoulmn,'') = ''
	SET @SortCoulmn = 'LastName'

IF ISNULL(@SortOrder,'') = ''
	SET @SortOrder = 'ASC'

IF CHARINDEX('SCA Administrator',(SELECT R.UserType+' '+R.RoleName FROM Role AS R(NOLOCK) INNER JOIN IDXUserRole AS UR(NOLOCK) ON R.RoleId = UR.RoleID WHERE UR.UserID=@UserId)) > 0 
	
	BEGIN

		DECLARE @orderIdCheck VARCHAR(50)='Value'
		DECLARE @SapID VARCHAR(50)=@searchID
		IF @searchID = 'NULL'
			BEGIN
				SET @searchID=''
			END

		IF(LEN(@searchID) > 0 and LEN(@searchID) < 10)
		BEGIN
			SET @searchID = replicate('0', 10 - len(@searchID)) + cast (@searchID AS VARCHAR)
		END

		IF @OrderNo = 'NULL'
			BEGIN
				SET @orderIdCheck=''
			END
		ELSE
			BEGIN
				SET @orderIdCheck = replicate('0', 10 - len(@OrderNo)) + cast (@OrderNo AS VARCHAR)
			END
		IF @SearchID =''
		BEGIN
			SET @Query1 = ' 1=1 '
			SET @Query3 = ' AND 1=1 '
			SET @Query4 = ' 1=1 '
		END
		ELSE
		BEGIN		
			--SET @Query1 = '((C.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' ) OR (  C.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' )) '
			--SET @Query3 =' AND ((CS.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @searchID + ''''+ ' OR PT.NHSId = ' + ''''+ @searchID  + ''''+') OR (CS.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @SapID + ''''+ ' OR PT.NHSId = ' + '''' + @SapID + ''''+ ')) '						
			--SET @Query4 = '((CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' ) OR (  CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ' )) '
			
			SET @Query1 = '((C.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' ) OR (  C.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' )) '
			SET @Query3 =' AND ((CS.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @searchID + ''''+ '  OR PT.PatientId = ' + '''' + @searchID + ''''+ ' OR PT.NHSId = 
' + ''''+ @searchID  + ''''+') OR (CS.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @SapID + ''''+ ' OR PT.PatientId = ' + '''' + @SapID + ''''+ ' OR PT.NHSId = ' 
+ '''' + @SapID + ''''+ ')) '						
			SET @Query4 = '((CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' ) OR (  CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ' )) '

		END
		IF @orderIdCheck = ''
		BEGIN
			SET @Query2 = ' 1=1 '
			SET @Query5 = ' '
			SET @Query6 = ' '
		END
		ELSE
		BEGIN		
			
			--SET @Query2 = '((ORD.SAPOrderNo = ' + @orderIdCheck + ' ) OR (  ORD.SAPDeliveryNo = ' + @orderIdCheck + ' )) '
			--SET @Query5 = ' and ' + @orderIdCheck + ' = ' + '''' + ''''
			--SET @Query6 = ' and ' + @orderIdCheck + '<> '+'''' + '''' 

			SET @Query2 = '((ORD.SAPOrderNo = ' + '''' + @orderIdCheck + ''''+ ') OR (  ORD.SAPDeliveryNo =' + '''' + @orderIdCheck + ''''+ ' )) '
			SET @Query5 = ' and ' + '''' + @orderIdCheck + ''''+ ' = ' + '''' + ''''
			SET @Query6 = ' and ' + '''' + @orderIdCheck + ''''+ '<> '+'''' + '''' 
		END
											 
		

	IF( @OrderNo <> 'NULL' OR @searchID <> '')
		BEGIN		
			SET @sql = 	'WITH CTEPatientDetails AS
				(
					SELECT ROW_NUMBER() OVER(ORDER BY ' + @SortCoulmn + ' ' + @SortOrder + ') as RowNumber,
					 * from 
					(
						select
								0 AS PatientId,
								LSTTitle.DefaultText AS TitleId,
								P.FirstName +' + '''' + ' ' + ''''  + '+ISNULL(P.LastName,'+ '''' + ''''+ ') AS PatientName,
								P.FirstName,
								P.LastName,
								P.TelephoneNumber,
								AD.HouseNumber,
								AD.HouseName,
								ISNULL(AD.AddressLine1,' + '''' + '''' + ') as AddressLine1,
								AD.AddressLine2,
								AD.City,
								AD.County,
								AD.Country,
								ISNULL(AD.PostCode, ' + '''' + '''' + ') as PostCode,
								NULL as DateofBirth,
								NULL AS PatientType,
								LstStatus.DefaultText AS PatientStatus,
								C.CustomerId AS ' + '''' + 'PatientCustomer' + '''' + ',                                
								NULL as DeliveryDate,
								NULL as CareHomeId,
								0 AS OrderActivated,
								cast(0 as bit) AS HasActiveOrder,
								P.FirstName+' + '''' + ' ' + ''''  + '+ISNULL(P.LastName,' + '''' + '' + '''' + ') AS CustomerName,' +
								'''' + '''' + ' AS CareHomeName,
								C.SAPCustomerNumber AS SAPCustomerNumber,
								NULL As SAPPatientNumber,
								10115 as objectType               
                                
						from Customer as C(nolock) 
						LEFT OUTER JOIN PersonalInformation as P(nolock) ON C.PersonalInformationId = P.PersonalInformationId '+ @Query5 + '
						LEFT OUTER JOIN Address AS AD(NOLOCK) ON AD.AddressId = P.AddressId '+ @Query5 + '
						LEFT OUTER JOIN List AS LstStatus(NOLOCK) ON C.Customer_Status = LstStatus.ListId '+ @Query5 + '
						LEFT OUTER JOIN List AS LSTTitle(NOLOCK) ON P.TitleId = LSTTitle.ListId '+ @Query5 + '

						WHERE 
						' + @Query1 + '

					UNION
					
						
						SELECT
								0 AS PatientId,
								LSTTitle.DefaultText AS TitleId,
								P.FirstName+' + '''' +' ' + '''' +' +ISNULL(P.LastName,' + '''' + '''' + ') AS PatientName,
								P.FirstName,
								P.LastName,
								P.TelephoneNumber,
								AD.HouseNumber,
								AD.HouseName,
								ISNULL(AD.AddressLine1,' + '''' + '''' + ') as AddressLine1,
								AD.AddressLine2,
								AD.City,
								AD.County,
								AD.Country,
								ISNULL(AD.PostCode,' + '''' + '''' + ') as PostCode,
								NULL as DateofBirth,
								' + '''' + 'CareHome' + '''' + ' AS PatientType,
								LstStatus.DefaultText AS PatientStatus,
								CH.CustomerId AS ' + '''' + 'PatientCustomer' + '''' +',                                
								CH.NextDeliveryDate as DeliveryDate,
								CH.CareHomeId as CareHomeId,
								(SELECT CASE WHEN COUNT(*) > 0
									THEN 1
									ELSE 0
									END                                             
									FROM Orders O (NOLOCK)                                                                 
									WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND
									O.IsRushFlag = 0 AND
									O.CareHomeId = CH.CareHomeId) AS OrderActivated,
								cast(0 as bit) AS HasActiveOrder,
								Pcust.FirstName + ' + '''' + ' ' + '''' + '+ ISNULL(Pcust.LastName,' + '''' + '' + '''' + ') AS CustomerName,
								P.FirstName+' + '''' +' ' + '''' +' +ISNULL(P.LastName,' + '''' + '''' + ') AS CareHomeName,
								ISNULL(C.SAPCustomerNumber,' + '''' + '''' + ') AS SAPCustomerNumber,
								ISNULL(CH.SAPCareHomeNumber,' + '''' + '''' + ') As SAPPatientNumber,
								10114 as objectType               
                                
						from CareHome as CH(nolock) 
						LEFT OUTER JOIN PersonalInformation as P(nolock) ON CH.PersonalInformationId = P.PersonalInformationId
						LEFT OUTER JOIN Address AS AD(NOLOCK) ON AD.AddressId = P.AddressId
						LEFT OUTER JOIN List AS LstStatus(NOLOCK) ON CH.CareHomeStatus = LstStatus.ListId
						LEFT OUTER JOIN List AS LstType(NOLOCK) ON CH.CareHomeType = LstType.ListId
						LEFT OUTER JOIN List AS LSTTitle(NOLOCK) ON P.TitleId = LSTTitle.ListId
						LEFT OUTER JOIN Customer as C ON C.CustomerId = CH.CustomerId
						LEFT OUTER JOIN PersonalInformation as Pcust(nolock) ON C.PersonalInformationId = Pcust.PersonalInformationId
						LEFT OUTER JOIN Orders AS ORD(NOLOCK) ON CH.CareHomeId = ORD.CareHomeId ' + @Query6 + '
						WHERE ' + @Query4 + ' AND ' + @Query2 + '

					UNION'
					SET @newSQL ='
						SELECT 
								PT.PatientId,
								LSTTitle.DefaultText AS TitleId,
								P.FirstName+' + '''' +' ' + '''' +' +ISNULL(P.LastName,' + '''' + '''' + ') AS PatientName,
								P.FirstName,
								P.LastName,
								P.TelephoneNumber,
								AD.HouseNumber,
								AD.HouseName,
								ISNULL(AD.AddressLine1,' + '''' + '' + '''' + ') as AddressLine1,
								AD.AddressLine2,
								AD.City,
								AD.County,
								AD.Country,
								ISNULL(AD.PostCode, ' + '''' + '' + '''' + ') as PostCode,
								PT.DateofBirth,
								LstPType.DefaultText AS PatientType,
								LstStatus.DefaultText AS PatientStatus,
								PT.CustomerId AS ' + '''' + 'PatientCustomer' + '''' + ',                                
								CASE WHEN PRESCPRO.PrescriptionId IS NOT NULL THEN PT.NextDeliveryDate ELSE NULL END as DeliveryDate,
								PT.CareHomeId,
								(SELECT CASE WHEN COUNT(*) > 0
								THEN 1
								ELSE 0
								END                                             
								FROM Orders O (NOLOCK)                 
								WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND
								O.OrderType != 10100 AND
								O.IsRushFlag = 0 AND
								O.PatientId = PT.PatientId) AS OrderActivated,
								(SELECT CASE WHEN COUNT(*) > 0
								THEN cast (1 as bit)
								ELSE cast (0 as bit)
								END                                             
								FROM Orders O (NOLOCK)                                                                 
								WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND
								O.OrderType != 10100 AND
								O.PatientId = PT.PatientId) AS HasActiveOrder,
								PC.FirstName+' + '''' +' ' + '''' +' +ISNULL(PC.LastName,' + '''' + '''' + ') AS CustomerName,
								PCH.FirstName+' + '''' +' ' + '''' +' +ISNULL(PCH.LastName,' + '''' + '''' + ') AS CareHomeName,
								CS.SAPCustomerNumber AS SAPCustomerNumber,
								PT.SAPPatientNumber As SAPPatientNumber,
								10113 as objectType                  
						FROM Patient AS PT(NOLOCK) 
						LEFT OUTER JOIN PersonalInformation AS P(NOLOCK) ON PT.PersonalInformationId = P.PersonalInformationId
						LEFT OUTER JOIN Address AS AD(NOLOCK) ON AD.AddressId = P.AddressId
						LEFT OUTER JOIN List AS LstPType(NOLOCK) ON PT.PatientType = LstPType.ListId
						LEFT OUTER JOIN List AS LstStatus(NOLOCK) ON PT.PatientStatus = LstStatus.ListId
						LEFT OUTER JOIN List AS LSTTitle(NOLOCK) ON P.TitleId = LSTTitle.ListId
						LEFT OUTER JOIN Customer AS CS(NOLOCK) ON CS.CustomerId = PT.CustomerId
						LEFT OUTER JOIN PersonalInformation AS PC(NOLOCK) ON PC.PersonalInformationId = CS.PersonalInformationId
						LEFT OUTER JOIN CareHome AS CH(NOLOCK) ON CH.CareHomeId = PT.CareHomeId
						LEFT OUTER JOIN PersonalInformation AS PCH(NOLOCK) ON PCH.PersonalInformationId = CH.PersonalInformationId
						LEFT OUTER JOIN Orders AS ORD(NOLOCK) ON PT.PatientId = ORD.PatientId 
						LEFT OUTER JOIN Prescription PRESC(NOLOCK) ON PT.PatientId = PRESC.PatientId
                        LEFT OUTER JOIN (SELECT PrescriptionId FROM IDXPrescriptionProduct(NOLOCK) WHERE IsRemoved = 0 GROUP BY PrescriptionId) PRESCPRO
                        ON PRESC.PrescriptionId = PRESCPRO.PrescriptionId ' + @Query6 + '

						WHERE ' + @Query2 + @Query3 + '
						
				) AS T ) --end of with


				SELECT *,(select COUNT(1) from CTEPatientDetails) as TotalRecords FROM CTEPatientDetails where RowNumber BETWEEN ' + cast(@start As Varchar(10)) + ' and ' + cast(@end AS Varchar(10)) + ' and FirstName IS NOT NULL'
					print 	@sql+' '+@newSQL		
				EXEC( @sql+' '+@newSQL)
								
		END
		ELSE
			BEGIN
				IF(@SearchText = 'NULL')
                SET @SearchText = ''
		
		EXEC DisplaySearchResult_JK @pageNum,@recordCount, @SearchText, @CustomerId, @PatientStatus, @PatientType, @UserId, @SortCoulmn, @SortOrder
		--select getdate()
		--PRINT @pageNum
		--PRINT @recordCount
		--PRINT @SearchText
		--PRINT @CustomerId
		--PRINT @PatientStatus
		--PRINT @PatientType
		--PRINT @UserId
		--PRINT @SortCoulmn
		--PRINT @SortOrder
			END
	END

ELSE
	BEGIN	
			SET @orderIdCheck ='Value'
			SET @SapID =@searchID
			IF @OrderNo = 'NULL'
			BEGIN
				SET @orderIdCheck=''
			END
			ELSE
			BEGIN
				SET @orderIdCheck = replicate('0', 10 - len(@OrderNo)) + cast (@OrderNo AS VARCHAR)
			END

			IF @searchID = 'NULL'
			BEGIN
				SET @searchID=''
			END

			IF(LEN(@searchID) > 0 and LEN(@searchID) < 10)
				BEGIN
					SET @searchID = replicate('0', 10 - len(@searchID)) + cast (@searchID AS VARCHAR)
				END
		IF @SearchID =''
		BEGIN
			SET @Query1 = ' 1=1 '
			SET @Query3 = ' AND 1=1 '
			SET @Query4 = ' 1=1 '
		END
		ELSE
		BEGIN		
			--SET @Query1 = '((C.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' ) OR (  C.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' )) '
			--SET @Query3 =' AND ((CS.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @searchID + ''''+ ' OR PT.NHSId = ' + '''' + @searchID + ''''+ ') OR (CS.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @SapID + ''''+ ' OR PT.NHSId = ' + '''' + @SapID + ''''+ ')) '
			--SET @Query4 = '((CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' ) OR (  CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ')) '
						
			SET @Query1 = '((C.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' ) OR (  C.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' )) '
			SET @Query3 =' AND ((CS.SAPCustomerNumber = ' + '''' + @searchID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @searchID + ''''+ ' OR PT.PatientId = ' + '''' + @searchID + ''''+ ' OR PT.NHSId = ' + '''' + @searchID + ''''+ ') OR (CS.SAPCustomerNumber = ' + '''' + @SapID + ''''+ ' OR CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ' OR PT.SAPPatientNumber = ' + '''' + @SapID + ''''+ ' OR PT.PatientId = ' + '''' + @SapID + ''''+ ' OR PT.NHSId = ' 
+ '''' + @SapID + ''''+ ')) '
			SET @Query4 = '((CH.SAPCareHomeNumber = ' + '''' + @searchID + ''''+ ' ) OR (  CH.SAPCareHomeNumber = ' + '''' + @SapID + ''''+ ')) '

		END
		
		IF @orderIdCheck = ''
		BEGIN
			SET @Query2 = ' 1=1 '
			SET @Query5 = ' '
			SET @Query6 = ' '
		END
		ELSE
		BEGIN
			
			--SET @Query2 = '((ORD.SAPOrderNo = ' + @orderIdCheck + ' ) OR (  ORD.SAPDeliveryNo = ' + @orderIdCheck + ' )) '
			--SET @Query5 = ' and ' + @orderIdCheck + ' = ' + '''' + ''''
			--SET @Query6 = ' and ' + @orderIdCheck + '<> '+'''' + '''' 
			SET @Query2 = '((ORD.SAPOrderNo = ' + '''' + @orderIdCheck + ''''+ ') OR (  ORD.SAPDeliveryNo =' + '''' + @orderIdCheck + ''''+ ' )) '
			SET @Query5 = ' and ' + '''' + @orderIdCheck + ''''+ ' = ' + '''' + ''''
			SET @Query6 = ' and ' + '''' + @orderIdCheck + ''''+ '<> '+'''' + '''' 
		END
		
		IF( @OrderNo <> 'NULL' OR @searchID <> '')
		BEGIN
			
				Set @sql = 'WITH CTEPatientDetails AS
				(
					SELECT ROW_NUMBER() OVER(ORDER BY ' + @SortCoulmn + ' ' + @SortOrder + ') as RowNumber,
					 * from 
					(
						select
								0 AS PatientId,
								LSTTitle.DefaultText AS TitleId,
								P.FirstName +' + '''' + ' ' + ''''  + '+ISNULL(P.LastName,'+ '''' + ''''+ ') AS PatientName,
								P.FirstName,
								P.LastName,
								P.TelephoneNumber,
								AD.HouseNumber,
								AD.HouseName,
								ISNULL(AD.AddressLine1,' + '''' + '''' + ') as AddressLine1,
								AD.AddressLine2,
								AD.City,
								AD.County,
								AD.Country,
								ISNULL(AD.PostCode, ' + '''' + '''' + ') as PostCode,
								NULL as DateofBirth,
								NULL AS PatientType,
								LstStatus.DefaultText AS PatientStatus,
								C.CustomerId AS ' + '''' + 'PatientCustomer' + '''' + ',                                
								NULL as DeliveryDate,
								NULL as CareHomeId,
								0 AS OrderActivated,
								cast(0 as bit) AS HasActiveOrder,
								P.FirstName+' + '''' + ' ' + ''''  + '+ISNULL(P.LastName,' + '''' + '' + '''' + ') AS CustomerName,' +
								'''' + '''' + ' AS CareHomeName,
								C.SAPCustomerNumber AS SAPCustomerNumber,
								NULL As SAPPatientNumber,
								10115 as objectType               
						from Customer as C(nolock) 
						LEFT OUTER JOIN PersonalInformation as P(nolock) ON C.PersonalInformationId = P.PersonalInformationId '+ @Query5 + '
						LEFT OUTER JOIN Address AS AD(NOLOCK) ON AD.AddressId = P.AddressId '+ @Query5 + '
						LEFT OUTER JOIN List AS LstStatus(NOLOCK) ON C.Customer_Status = LstStatus.ListId '+ @Query5 + '
						LEFT OUTER JOIN List AS LSTTitle(NOLOCK) ON P.TitleId = LSTTitle.ListId '+ @Query5 + '
						WHERE 
						' + @Query1 + ' and(C.CustomerId IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId=' + '''' + CAST(@userId AS VARCHAR(150)) + '''' + '))
					UNION
						SELECT
								0 AS PatientId,
								LSTTitle.DefaultText AS TitleId,
								P.FirstName+' + '''' +' ' + '''' +' +ISNULL(P.LastName,' + '''' + '''' + ') AS PatientName,
								P.FirstName,
								P.LastName,
								P.TelephoneNumber,
								AD.HouseNumber,
								AD.HouseName,
								ISNULL(AD.AddressLine1,' + '''' + '''' + ') as AddressLine1,
								AD.AddressLine2,
								AD.City,
								AD.County,
								AD.Country,
								ISNULL(AD.PostCode,' + '''' + '''' + ') as PostCode,
								NULL as DateofBirth,
								' + '''' + 'CareHome' + '''' + ' AS PatientType,
								LstStatus.DefaultText AS PatientStatus,
								CH.CustomerId AS ' + '''' + 'PatientCustomer' + '''' +',                                
								CH.NextDeliveryDate as DeliveryDate,
								CH.CareHomeId as CareHomeId,
								(SELECT CASE WHEN COUNT(*) > 0
									THEN 1
									ELSE 0
									END                                             
									FROM Orders O (NOLOCK)                                                                 
									WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND
									O.IsRushFlag = 0 AND
									O.CareHomeId = CH.CareHomeId) AS OrderActivated,
								cast(0 as bit) AS HasActiveOrder,
								Pcust.FirstName + ' + '''' + ' ' + '''' + '+ ISNULL(Pcust.LastName,' + '''' + '' + '''' + ') AS CustomerName,
								P.FirstName+' + '''' +' ' + '''' +' +ISNULL(P.LastName,' + '''' + '''' + ') AS CareHomeName,
								ISNULL(C.SAPCustomerNumber,' + '''' + '''' + ') AS SAPCustomerNumber,
								ISNULL(CH.SAPCareHomeNumber,' + '''' + '''' + ') As SAPPatientNumber,
								10114 as objectType               
                                
						from CareHome as CH(nolock) 
						LEFT OUTER JOIN PersonalInformation as P(nolock) ON CH.PersonalInformationId = P.PersonalInformationId
						LEFT OUTER JOIN Address AS AD(NOLOCK) ON AD.AddressId = P.AddressId
						LEFT OUTER JOIN List AS LstStatus(NOLOCK) ON CH.CareHomeStatus = LstStatus.ListId
						LEFT OUTER JOIN List AS LstType(NOLOCK) ON CH.CareHomeType = LstType.ListId
						LEFT OUTER JOIN List AS LSTTitle(NOLOCK) ON P.TitleId = LSTTitle.ListId
						LEFT OUTER JOIN Customer as C ON C.CustomerId = CH.CustomerId
						LEFT OUTER JOIN PersonalInformation as Pcust(nolock) ON C.PersonalInformationId = Pcust.PersonalInformationId
						LEFT OUTER JOIN Orders AS ORD(NOLOCK) ON CH.CareHomeId = ORD.CareHomeId ' + @Query6 + '
						WHERE ' + @Query4 + ' AND ' + @Query2 + 'and(CH.CustomerId IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId= ' + '''' + CAST(@userId AS VARCHAR(150)) + '''' + '))
					UNION'
					SET @newSQL =	'SELECT 
								PT.PatientId,
								LSTTitle.DefaultText AS TitleId,
								P.FirstName+' + '''' +' ' + '''' +' +ISNULL(P.LastName,' + '''' + '''' + ') AS PatientName,
								P.FirstName,
								P.LastName,
								P.TelephoneNumber,
								AD.HouseNumber,
								AD.HouseName,
								ISNULL(AD.AddressLine1,' + '''' + '' + '''' + ') as AddressLine1,
								AD.AddressLine2,
								AD.City,
								AD.County,
								AD.Country,
								ISNULL(AD.PostCode, ' + '''' + '' + '''' + ') as PostCode,
								PT.DateofBirth,
								LstPType.DefaultText AS PatientType,
								LstStatus.DefaultText AS PatientStatus,
								PT.CustomerId AS ' + '''' + 'PatientCustomer' + '''' + ',                                
								CASE WHEN PRESCPRO.PrescriptionId IS NOT NULL THEN PT.NextDeliveryDate ELSE NULL END as DeliveryDate,
								PT.CareHomeId,
								(SELECT CASE WHEN COUNT(*) > 0
								THEN 1
								ELSE 0
								END                                             
								FROM Orders O (NOLOCK)                                                                 
								WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND
								O.OrderType != 10100 AND
								O.IsRushFlag = 0 AND
								O.PatientId = PT.PatientId) AS OrderActivated,
								(SELECT CASE WHEN COUNT(*) > 0
								THEN cast (1 as bit)
								ELSE cast (0 as bit)
								END                           
								FROM Orders O (NOLOCK)                                                                 
								WHERE (O.OrderStatus = 10047 OR O.OrderStatus = 10098) AND
								O.OrderType != 10100 AND
								O.PatientId = PT.PatientId) AS HasActiveOrder,
								PC.FirstName+' + '''' +' ' + '''' +' +ISNULL(PC.LastName,' + '''' + '''' + ') AS CustomerName,
								PCH.FirstName+' + '''' +' ' + '''' +' +ISNULL(PCH.LastName,' + '''' + '''' + ') AS CareHomeName,
								CS.SAPCustomerNumber AS SAPCustomerNumber,
								PT.SAPPatientNumber As SAPPatientNumber,
								10113 as objectType                  
						FROM Patient AS PT(NOLOCK) 
						LEFT OUTER JOIN PersonalInformation AS P(NOLOCK) ON PT.PersonalInformationId = P.PersonalInformationId
						LEFT OUTER JOIN Address AS AD(NOLOCK) ON AD.AddressId = P.AddressId
						LEFT OUTER JOIN List AS LstPType(NOLOCK) ON PT.PatientType = LstPType.ListId
						LEFT OUTER JOIN List AS LstStatus(NOLOCK) ON PT.PatientStatus = LstStatus.ListId
						LEFT OUTER JOIN List AS LSTTitle(NOLOCK) ON P.TitleId = LSTTitle.ListId
						LEFT OUTER JOIN Customer AS CS(NOLOCK) ON CS.CustomerId = PT.CustomerId
						LEFT OUTER JOIN PersonalInformation AS PC(NOLOCK) ON PC.PersonalInformationId = CS.PersonalInformationId
						LEFT OUTER JOIN CareHome AS CH(NOLOCK) ON CH.CareHomeId = PT.CareHomeId
						LEFT OUTER JOIN PersonalInformation AS PCH(NOLOCK) ON PCH.PersonalInformationId = CH.PersonalInformationId
						LEFT OUTER JOIN Orders AS ORD(NOLOCK) ON PT.PatientId = ORD.PatientId 
						LEFT OUTER JOIN Prescription PRESC(NOLOCK) ON PT.PatientId = PRESC.PatientId
                        LEFT OUTER JOIN (SELECT PrescriptionId FROM IDXPrescriptionProduct(NOLOCK) WHERE IsRemoved = 0 GROUP BY PrescriptionId) PRESCPRO
                        ON PRESC.PrescriptionId = PRESCPRO.PrescriptionId ' + @Query6 + '
						WHERE ' + @Query2 + @Query3 + ' and(PT.CustomerId IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId=' + '''' + CAST(@userId AS VARCHAR(150)) + '''' + '))						
						
				) AS T )
				SELECT *,(select COUNT(1) from CTEPatientDetails) as TotalRecords FROM CTEPatientDetails where RowNumber BETWEEN ' + cast(@start As Varchar(10)) + ' and ' + cast(@end AS Varchar(10)) + ' and FirstName IS NOT NULL'
			 
				 EXEC(@sql+' '+@newSQL)
								
		END
		ELSE
		BEGIN
			IF(@SearchText = 'NULL')
			SET @SearchText = ''

     	EXEC DisplaySearchResult_JK @pageNum,@recordCount, @SearchText, @CustomerId, @PatientStatus, @PatientType, @UserId, @SortCoulmn, @SortOrder
END
    END
END