﻿-- =============================================
-- Author:	Siddhesh Sawant
-- Create date: 15-04-2015
-- Description:	Get postcode matrix for patient
-- =============================================

-- ===============================================================================

--	Modified Date			Modified By				Purpose
--  1.	 16-Apr-2015			Siddhesh Sawant			Added postcode column
--  2.   22-May-2015			Siddhesh Sawant			check for null condition for postcode value
--  3.   08-Jul-2015			Siddhesh Sawant			Added condition for non carehome patient not saved
--  3.   
-- ===============================================================================
 -- exec GetPostCodeMatrixForPatient '141','9','0'
 -- exec GetPostCodeMatrixForPatient '297','9','0'  --EMPTY POSTCODE
 -- exec GetPostCodeMatrixForPatient '0','11','0'  -- new non carehome patient not saved
CREATE PROC [dbo].[GetPostCodeMatrixForPatient]
@PatientId BIGINT, @CustomerId BIGINT, @CareHomeId BIGINT
AS 
BEGIN
DECLARE @Round VARCHAR(50) = 0
DECLARE @RoundId VARCHAR(50) = 0
DECLARE @Postcode VARCHAR(50) = 0
DECLARE @DefaultRound VARCHAR(50) = 'GBHD000090'
DECLARE @DefaultRoundId VARCHAR(50) = 'XXXX'

-- for new non carehome patient, not saved
IF(@CareHomeId = 0 AND @PatientId = 0)	
	BEGIN 	
	IF EXISTS(SELECT TOP 1 CustomerID,PostcodeMatrixID,Monday,Tuesday,Wednesday,Thursday,Friday,RoundID,Round,Postcode FROM PostcodeMatrix (NOLOCK) 
						WHERE CustomerID = @CustomerId 
						AND Round = @DefaultRound 
						AND RoundID = @DefaultRoundId 
						AND Postcode = ''
						AND IsActive = 1)
		BEGIN 		
			SELECT TOP 1 CustomerID,PostcodeMatrixID,Monday,Tuesday,Wednesday,Thursday,Friday,RoundID,Round,Postcode FROM PostcodeMatrix (NOLOCK) 
						WHERE CustomerID = @CustomerId 
						AND Round = @DefaultRound 
						AND RoundID = @DefaultRoundId 
						AND Postcode = ''
						AND IsActive = 1
		END
	ELSE
		BEGIN			
		SELECT  @CustomerId  AS CustomerID, 
						CAST(0 AS BIGINT) AS  PostcodeMatrixID,
						CAST(1 AS BIT) AS Monday,
						CAST(1 AS BIT)  AS Tuesday,
						CAST(1 AS BIT)  AS Wednesday,
						CAST(1 AS BIT)  AS Thursday,
						CAST(1 AS BIT)  AS Friday,
						@DefaultRoundId AS RoundID ,
						@DefaultRound AS Round,
						'' AS Postcode 					
		END
	END	
ELSE 
	BEGIN
		-- for new or existing carehome patient
		IF(@CareHomeId != 0)
			BEGIN	
				SELECT @Round = Round, @RoundId = RoundId FROM Carehome (NOLOCK) WHERE CareHomeId = @CareHomeId		
				(SELECT @Postcode = ISNULL(adr.postcode,'') FROM Address adr (NOLOCK) WHERE adr.AddressId = 
					(SELECT pif.AddressId FROM PersonalInformation pif (NOLOCK)
					WHERE pif.PersonalInformationId =(SELECT ch.PersonalInformationId from CareHome ch (nolock)  
					WHERE ch.CareHomeId = @CareHomeId)))	
			END
		-- for existing non carehome patient
		ELSE IF(@PatientId != 0)
			BEGIN	
				SELECT @Round = Round, @RoundId = RoundId FROM Patient (NOLOCK) WHERE PatientId = @PatientId
				(SELECT @Postcode = ISNULL(adr.postcode,'') FROM Address adr (NOLOCK) WHERE adr.AddressId = 
					(SELECT pif.AddressId FROM PersonalInformation pif (NOLOCK)
					WHERE pif.PersonalInformationId =(SELECT pt.PersonalInformationId FROM Patient pt (NOLOCK) 
					WHERE pt.PatientId = @PatientId)))
			END

		--CHECK WITH PATIENT'S POSTCODE
			IF EXISTS(SELECT CustomerID,PostcodeMatrixID,Monday,Tuesday,Wednesday,Thursday,Friday,RoundID,Round,Postcode FROM PostcodeMatrix (NOLOCK) 
						WHERE CustomerID = @CustomerId 
						AND Round = @Round 
						AND RoundID = @RoundId 
						AND Postcode = @Postcode
						AND IsActive = 1)
			BEGIN								
					SELECT CustomerID,PostcodeMatrixID,Monday,Tuesday,Wednesday,Thursday,Friday,RoundID,Round,Postcode FROM PostcodeMatrix (NOLOCK) 
					WHERE CustomerID = @CustomerId 
					AND Round = @Round 
					AND RoundID = @RoundId 
					AND Postcode = @Postcode
					AND IsActive = 1
			END
			--CHECK WITHOUT POSTCODE
			ELSE IF EXISTS(SELECT TOP 1 CustomerID,PostcodeMatrixID,Monday,Tuesday,Wednesday,Thursday,Friday,RoundID,Round,Postcode FROM PostcodeMatrix (NOLOCK) 
							WHERE CustomerID = @CustomerId 
							AND Round = @Round 
							AND RoundID = @RoundId 	
							--AND Postcode = ''		
							AND IsActive = 1)
			BEGIN		
						
					SELECT TOP 1 CustomerID,PostcodeMatrixID,Monday,Tuesday,Wednesday,Thursday,Friday,RoundID,Round,Postcode FROM PostcodeMatrix (NOLOCK) 
					WHERE CustomerID = @CustomerId 
					AND Round = @Round 
					AND RoundID = @RoundId	
					--AND Postcode = ''				
					AND IsActive = 1
			END
			ELSE			
			-- PROVIDE DEFAULT POST CODE									
				SELECT  @CustomerId  AS CustomerID, 
						CAST(0 AS BIGINT) AS  PostcodeMatrixID,
						CAST(1 AS BIT) AS Monday,
						CAST(1 AS BIT)  AS Tuesday,
						CAST(1 AS BIT)  AS Wednesday,
						CAST(1 AS BIT)  AS Thursday,
						CAST(1 AS BIT)  AS Friday,
						@RoundId AS RoundID ,
						@Round AS Round,
						@Postcode AS Postcode 	
	END
END
