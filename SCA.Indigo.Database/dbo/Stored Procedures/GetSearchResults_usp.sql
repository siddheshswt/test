﻿/******************************
** File:    AddCategory_usp
** Name:	AddCategory_usp
** Desc:	Procedure Returns search results as per input parameter given
** Auth:	Avinash
** Date:	11/11/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
** 2	02/07/2015		Avinash		Removed extra columns.
** 3    28/06/2016      Saurabh     Included TelephoneNo In Google Search
** 4    22/07/2016      Saurabh     Included Corrspondence PostCode In Google Search
** 5    19/09/2016      Saurabh     Filter for Carehome users
** 6    26/09/2016      Saurabh     Customer Removed for carehome users
*******************************/
--GetSearchResults_usp '0000', '5D9EE643-274D-40F8-9387-009292BDC75F'

CREATE PROCEDURE [dbo].[GetSearchResults_usp]
	-- Add the parameters for the stored procedure here
	@SearchText varchar(50),
	@UserId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @SearchResult table
(
	Result varchar(50)
)
 

DECLARE @maxCount BIGINT,
        @count BIGINT

		SET @maxCount = 20;        /*change to input more*/
SET @count = 0;

IF CHARINDEX('SCA Administrator',(select R.UserType+' '+R.RoleName from Role as R(nolock) INNER JOIN IDXUserRole as UR(nolock) ON R.RoleId = UR.RoleID where UR.UserID=@UserId)) > 0 
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
		on P.PersonalInformationId = PT.PersonalInformationId 
		where P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId 
		where P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId 
		where P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END
IF @count < @maxCount
BEGIN
	insert into @SearchResult 
	select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
	on P.PersonalInformationId = PT.PersonalInformationId 
	where P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)


	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId 
		where P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId 
		where P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 CONVERT(varchar(12), DateofBirth,103) from Patient  (nolock)
		where CONVERT(varchar(12), DateofBirth,103) like '%'+ @SearchText+'%' and DateofBirth NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN Customer as Cust (nolock)
		ON Cust.CustomerId = P.PersonalInformationId 
		where AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN CareHome as CH (nolock)
		ON CH.PersonalInformationId = P.PersonalInformationId 
		where AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId OR P.CorrespondenceAddressId = AD.AddressId
		INNER JOIN Patient as PT (nolock)
		ON PT.PersonalInformationId = P.PersonalInformationId 
		where AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
    INSERT INTO @SearchResult
         SELECT DISTINCT TOP 20 pinfo.TelephoneNumber
         FROM   PersonalInformation pinfo(NOLOCK)
         LEFT JOIN Patient patient(NOLOCK)
         ON pinfo.PersonalInformationId = patient.PersonalInformationId
         LEFT JOIN CareHome carehome(NOLOCK)
         ON pinfo.PersonalInformationId = carehome.PersonalInformationId
         LEFT JOIN Customer customer(NOLOCK)
         ON pinfo.PersonalInformationId = customer.PersonalInformationId
         WHERE ISNULL(pinfo.TelephoneNumber,'') LIKE '%' + @SearchText + '%'
         AND   (patient.PatientId IS NOT NULL OR carehome.CareHomeId IS NOT NULL OR customer.CustomerId IS NOT NULL)

	set @count = (select COUNT(*) from @SearchResult )
END


END
ELSE
BEGIN

	DECLARE @CarehomeUserId VARCHAR(100) = ''
	SELECT @CarehomeUserId = CAST(ISNULL(UserId,'') AS VARCHAR(100)) FROM Users WHERE IsCarehomeUser = 1 AND UserId = @UserId

	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
		on P.PersonalInformationId = PT.PersonalInformationId
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = PT.CareHomeId
		where PT.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )

IF @count < @maxCount AND LEN(@CarehomeUserId) = 0
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId where Cust.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		and P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.LastName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId 
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = CH.CareHomeId
		where CH.CustomerId	IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and P.LastName like '%'+ @SearchText+'%' and P.LastName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END
IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Patient as PT(nolock)
		on P.PersonalInformationId = PT.PersonalInformationId 
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = PT.CareHomeId
		where PT.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END
IF @count < @maxCount AND LEN(@CarehomeUserId) = 0
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN Customer as Cust(nolock)
		on P.PersonalInformationId = Cust.PersonalInformationId where Cust.CustomerId
		IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		and P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 P.FirstName from PersonalInformation AS P(nolock) INNER JOIN CareHome as CH(nolock)
		on P.PersonalInformationId = CH.PersonalInformationId 
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = CH.CareHomeId
		where CH.CustomerId	IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and P.FirstName like '%'+ @SearchText+'%' and P.FirstName NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 CONVERT(varchar(12), DateofBirth,103) from Patient (nolock)
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = Patient.CareHomeId
		where CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@UserId) 
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and CONVERT(varchar(12), DateofBirth,103) like '%'+ @SearchText+'%' and DateofBirth NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount AND LEN(@CarehomeUserId) = 0
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN Customer as Cust (nolock)
		ON Cust.CustomerId = P.PersonalInformationId 
		where Cust.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@UserId) 
		and AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END


IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId INNER JOIN CareHome as CH (nolock)
		ON CH.PersonalInformationId = P.PersonalInformationId 
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = CH.CareHomeId
		where CH.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
	insert into @SearchResult 
		select distinct top 20 AD.PostCode from Address as AD (nolock) INNER JOIN PersonalInformation as P (nolock)
		ON P.AddressId = AD.AddressId OR P.CorrespondenceAddressId = AD.AddressId
		INNER JOIN Patient as PT (nolock)
		ON PT.PersonalInformationId = P.PersonalInformationId 
		LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		ON  idxUserCarehome.CarehomeId = PT.CareHomeId
		where PT.CustomerId IN (select CustomerId from IDXUserCustomer where UserId=@UserId)
		AND   ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		and AD.PostCode like '%'+ @SearchText+'%' and PostCode NOT IN (select * from @SearchResult)

	set @count = (select COUNT(*) from @SearchResult )
END

IF @count < @maxCount
BEGIN
    INSERT INTO @SearchResult
         SELECT DISTINCT TOP 20 pinfo.TelephoneNumber
         FROM   PersonalInformation pinfo(NOLOCK)
         LEFT JOIN Patient patient(NOLOCK)
         ON pinfo.PersonalInformationId = patient.PersonalInformationId
		 LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehome
		 ON  idxUserCarehome.CarehomeId = patient.CareHomeId
         LEFT JOIN CareHome carehome(NOLOCK)
         ON pinfo.PersonalInformationId = carehome.PersonalInformationId
		 LEFT OUTER JOIN vwIDXUserCarehome idxUserCarehomeC
		 ON  idxUserCarehomeC.CarehomeId = carehome.CareHomeId
         LEFT JOIN Customer customer(NOLOCK)
         ON pinfo.PersonalInformationId = customer.PersonalInformationId
         WHERE (patient.CustomerId  IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId=@UserId)
		 OR     carehome.CustomerId IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId=@UserId)
		 OR     customer.CustomerId IN (SELECT CustomerId FROM IDXUserCustomer WHERE UserId=@UserId))
		 AND   (ISNULL(idxUserCarehome.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%'
		 OR     ISNULL(idxUserCarehomeC.CarehomeUsers, '') LIKE '%' + @CarehomeUserId + '%')
		 AND   ISNULL(pinfo.TelephoneNumber,'') LIKE '%' + @SearchText + '%'
         AND   (patient.PatientId IS NOT NULL OR carehome.CareHomeId IS NOT NULL OR customer.CustomerId IS NOT NULL)

	set @count = (select COUNT(*) from @SearchResult )
END

END

select top 20 Result from @SearchResult where Result is not NULL
    
END

