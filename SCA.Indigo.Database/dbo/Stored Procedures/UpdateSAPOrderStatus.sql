﻿

-- =============================================
-- Author:		Arvind Nishad
-- Create date: 29-01-2015
-- Description:	Update Order Status to 'Send To SAP' after sending to SAP PI
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSAPOrderStatus] (
	@OrderID varchar(8000)-- It will be a comma separated Order Id
	)
AS
BEGIN
	DECLARE @Result INT = 0
	DECLARE @ResultMessage VARCHAR(200)
	
	DECLARE @OrderStatus BIGINT=10098 
	-- 10098 order status =	Sent To SAP

	BEGIN TRY
		BEGIN TRANSACTION;

		Update Orders set OrderStatus=@OrderStatus,
		ModifiedDate=GetDate()
		where OrderId in(SELECT val FROM dbo.Split(@OrderID, ','))



		SELECT @Result =  1
			,@ResultMessage = 'Order Status updated'
	
		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()
	END CATCH;

	SELECT @Result Result
		,@ResultMessage ResultMessage
END

