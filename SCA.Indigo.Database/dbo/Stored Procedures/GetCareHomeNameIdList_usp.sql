﻿-- =============================================
-- Author: Jagdish Karanday
-- Create date: 5 May 2015
-- Description: Procedure to fetch carehome names and id
-- Sample: EXEC GetCareHomeNameIdList_usp '2D6CB566-504F-404F-9E21-22EABE4078B0'
--		   EXEC GetCareHomeNameIdList_usp 'ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153'
-- =============================================

CREATE PROCEDURE [dbo].[GetCareHomeNameIdList_usp]
@UserId uniqueIdentifier
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;

IF CHARINDEX('SCA Administrator',(SELECT R.UserType+' '+R.RoleName FROM Role AS R(NOLOCK) INNER JOIN IDXUserRole AS UR(NOLOCK) ON R.RoleId = UR.RoleID WHERE UR.UserID=@UserId)) > 0 
	BEGIN
		SELECT (ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'') + ' (' +  ISNULL(SUBSTRING(C.SAPCareHomeNumber, PATINDEX('%[^0 ]%', C.SAPCareHomeNumber + ' '), LEN(C.SAPCareHomeNumber)),'') +')' ) AS CareHomeName
			,C.CareHomeId 
		FROM CareHome AS C (NOLOCK) 
			INNER JOIN PersonalInformation AS P (NOLOCK) ON C.PersonalInformationId = P.PersonalInformationId 
		ORDER BY CareHomeName
	END
Else
	BEGIN
		SELECT (ISNULL(P.FirstName,'')+' '+ISNULL(P.LastName,'') + ' (' +  ISNULL(SUBSTRING(C.SAPCareHomeNumber, PATINDEX('%[^0 ]%', C.SAPCareHomeNumber + ' '), LEN(C.SAPCareHomeNumber)),'') +')' ) AS CareHomeName
			,C.CareHomeId 
		FROM CareHome AS C (NOLOCK) 
			INNER JOIN PersonalInformation AS P (NOLOCK) ON C.PersonalInformationId = P.PersonalInformationId
			INNER JOIN IDXUserCustomer AS UC(nolock) ON UC.CustomerId = C.CustomerId
		WHERE UC.UserId =@UserId  
		ORDER BY CareHomeName
	END
END





