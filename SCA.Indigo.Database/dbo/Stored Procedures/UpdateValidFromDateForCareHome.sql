﻿
CREATE PROCEDURE [dbo].[UpdateValidFromDateForCareHome] 
@Input_SAPCarehomeNumber varchar(20),
@Input_ValidFromDate date 
AS
BEGIN
BEGIN TRY
              
BEGIN TRAN --OPEN TRANSACTION

       DECLARE @SAPCarehomeNumber varchar(20) = @Input_SAPCarehomeNumber
	   DECLARE @ValidFromDate date  = @Input_ValidFromDate
	   DECLARE @IDXPrescriptionProductId bigint
	   
	   
       DECLARE cursor_updateValidFromDateforCareHome CURSOR -- Declare cursor
       LOCAL SCROLL STATIC 
       FOR
              
             
       SELECT idx.IDXPrescriptionProductId from IDXPrescriptionProduct (nolock) idx INNER JOIN
	   Prescription (nolock) pre ON idx.PrescriptionId = pre.PrescriptionId INNER JOIN 
	   Patient (nolock) pt ON pre.PatientId = pt.PatientId INNER JOIN 
	   CareHome (nolock) ch ON pt.CareHomeId = ch.CareHomeId 
	   WHERE pt.CareHomeId IS NOT NULL 
	   AND ch.SAPCareHomeNumber = @SAPCarehomeNumber
                     
              OPEN cursor_updateValidFromDateforCareHome
              FETCH NEXT FROM cursor_updateValidFromDateforCareHome
              INTO @IDXPrescriptionProductId

              WHILE @@FETCH_STATUS = 0
              BEGIN                
                   
                     
					 UPDATE IDXPrescriptionProduct  
					 SET ValidFromDate = @ValidFromDate WHERE 
					 IDXPrescriptionProductId = @IDXPrescriptionProductId
                     

                     FETCH NEXT FROM cursor_updateValidFromDateforCareHome
                     INTO @IDXPrescriptionProductId  
              END

       CLOSE cursor_updateValidFromDateforCareHome -- close the cursor
       DEALLOCATE cursor_updateValidFromDateforCareHome -- Deallocate the cursor 

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE UPDATING VALID FROM DATE FOR CAREHOME'

END CATCH
END


