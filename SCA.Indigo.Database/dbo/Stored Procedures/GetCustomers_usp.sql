﻿

-- =============================================

-- Author: <Avinash Deshpande>

-- Create date: <20-11-2014>

-- Description: <Procedure to display customer names in Search Dropdown. e.g EXEC GetCustomers_usp >

-- =============================================

-- =============================================
--		Modified Date			Modified By			Purpose
--	1.   19-Mar-2015			Sagar Yerva			Added NOLOCK
--  2.   15-Apr-2016            Prafull Mohite      Added OrderBy clause
--	3.   14-July-2016			Siddhesh Sawant		added replace function for "(", ")" to replace with "[", "]"
-- =============================================

CREATE PROCEDURE [dbo].[GetCustomers_usp]

@UserId uniqueIdentifier


AS

BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from

-- interfering with SELECT statements.

SET NOCOUNT ON;

IF CHARINDEX('SCA Administrator',(select R.UserType+' '+R.RoleName from Role as R(nolock) INNER JOIN IDXUserRole as UR(nolock) ON R.RoleId = UR.RoleID where UR.UserID=@UserId)) > 0 
BEGIN

--select (P.FirstName+' '+ISNULL(P.LastName,'') + ' (' +  ISNULL(SUBSTRING(C.SAPCustomerNumber, PATINDEX('%[^0 ]%', C.SAPCustomerNumber + ' '), LEN(C.SAPCustomerNumber)),'') +')' ) as CustomerName,C.CustomerId from Customer as C (NOLOCK) INNER JOIN PersonalInformation as P (NOLOCK)

--ON C.PersonalInformationId = P.PersonalInformationId 


select (Replace(Replace(P.FirstName+' '+ISNULL(P.LastName,''),')',']'),'(','[') + ' (' +  ISNULL(SUBSTRING(C.SAPCustomerNumber, PATINDEX('%[^0 ]%', C.SAPCustomerNumber + ' '), LEN(C.SAPCustomerNumber)),'') +')' ) as CustomerName,C.CustomerId from Customer as C (NOLOCK) INNER JOIN PersonalInformation as P (NOLOCK)

ON C.PersonalInformationId = P.PersonalInformationId   order by P.FirstName

END
Else
Begin
 
select (P.FirstName+' '+ISNULL(P.LastName,'') + ' (' + ISNULL(SUBSTRING(C.SAPCustomerNumber, PATINDEX('%[^0 ]%', C.SAPCustomerNumber + ' '), LEN(C.SAPCustomerNumber)),'') +')' ) as CustomerName,C.CustomerId from Customer as C(nolock) INNER JOIN PersonalInformation as P(nolock)

ON C.PersonalInformationId = P.PersonalInformationId INNER JOIN IDXUserCustomer as UC(nolock) ON UC.CustomerId = C.CustomerId
where UC.UserId =@UserId   order by P.FirstName
End

END
