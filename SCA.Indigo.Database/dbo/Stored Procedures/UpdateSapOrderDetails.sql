﻿
-- =============================================
-- Author:		Arvind Nishad
-- Create date: 06-01-2015
-- Description:	Update Order with SAP OrderNo
-- =============================================
-- ===============================================================================

--		Modified Date			Modified By			Purpose
--  1.	 27-Mar-2015			Arvind Nishad		Logic with DerivedNDD column
--  2.	 10-Apr-2015			Siddhesh Sawant		Logic to update carehome ndd for carehome + commented logic of updating product ndd with patient NDD in order
--  3.   27-Apr-2015			Siddhesh Sawant		Added check for error code 'C' for cancelled order
--  4.   08-Dec-2015			Siddhesh Sawant		Removed else part of if condition "IF @Result > 0 AND @OrderType = 10053"
--  5.   08-Dec-2015			Siddhesh Sawant		Added @ACTDELDATE, @PLANT, @BILLINGDATE, @CURRENCY, @BILLINGQTY, @BILLINGUNIT, @NETVALUE, @VATVALUE
--  6.   13-Jan-2015			Siddhesh Sawant		Removed ActualDeliveryDate column from Orders table
--  7.   21-Jan-2015			Siddhesh Sawant		Update IDXOrderProduct table for @PLANT, @BILLINGDATE, @CURRENCY, @BILLINGQTY, @BILLINGUNIT, @NETVALUE, @VATVALUE
--  8.   12-Sep-2016			Siddhesh Sawant		Update IDXOrderProduct table for SAPActualDeliveryDate, SAPDeliveryNo, InvoiceNo
--  9.   12-Sep-2016			Siddhesh Sawant		Added @PODDATE, @DELIVERYQTY, @DELIVERYUNIT
--  10.  12-Sep-2016			Siddhesh Sawant		Added condition @OrderCreationType NOT IN (@CommunityOneOffStandard,@CarehomePatientOneOffStandard)
--  11.  12-Sep-2016			Siddhesh Sawant		Added @ORDERQTY
--  12.  16-Sep-2016			Siddhesh Sawant		Removed @ORDERQTY
--  13.  16-Sep-2016			Siddhesh Sawant		Added logic for new service charge line items
--  14.  27-Sep-2016			Siddhesh Sawant		quantity should be zero for order item >=900
--  15.  26-Oct-2016			Siddhesh Sawant		Added condition to ignore invalid POD, BILLING AND ACTUAL DELIVERY DATE
-- ===============================================================================
/*
EXEC	[dbo].[UpdateSapOrderDetails]
		@OrderID = 30762,
		@SAPOrderNo = N'123456',
		@DELIVERY = N'464646',		
		@INVOICE = N'123456',		
		@ErrorCode = N'S',
		@ErrorMessage = N'success',
		@PatientNDD = N'2015-04-20',
		@PatientNDD = N'2015-04-20',
		@IsCarehomeOrder = 1
*/
CREATE PROCEDURE [dbo].[UpdateSapOrderDetails] (
	@OrderID BIGINT
	,@SAPOrderNo VARCHAR(100)
	,@BASEMATERIAL VARCHAR(50) = NULL
	,@ORDERITEM VARCHAR(50) = NULL
	,@DELIVERY VARCHAR(50)
	,@DELIVERYITEM VARCHAR(50) = NULL
	,@INVOICE VARCHAR(100)
	,@INVOICEITEM VARCHAR(50) = NULL
	,@ErrorCode VARCHAR(500) = NULL	
	,@ErrorMessage VARCHAR(500) = NULL
	,@BASEMATERIALID VARCHAR(50) = NULL
	,@PatientNDD datetime=null -- NDD calculation is done on SAP interface
	,@CarehomeNDD datetime=null -- NDD calculation is done on SAP interface
	,@IsCarehomeOrder bit = 0
	,@ACTDELDATE VARCHAR(20) = NULL
    ,@PLANT VARCHAR(20) = NULL
    ,@BILLINGDATE VARCHAR(20) = NULL
    ,@CURRENCY VARCHAR(20) = NULL
    ,@BILLINGQTY VARCHAR(20) = NULL
    ,@BILLINGUNIT VARCHAR(20) = NULL
    ,@NETVALUE VARCHAR(20) = NULL
    ,@VATVALUE VARCHAR(20) = NULL
	,@PODDATE VARCHAR(20) = NULL
	,@DELIVERYQTY VARCHAR(20) = NULL
	,@DELIVERYUNIT VARCHAR(20) = NULL	
	)
AS
BEGIN
	DECLARE @Result INT = 0
	DECLARE @ResultMessage VARCHAR(200)	
	DECLARE @OrderType BIGINT = 0			
	DECLARE @OrderCreationType BIGINT = 0	
	DECLARE @OrderStatus BIGINT	
	DECLARE @CarehomeId BIGINT
	DECLARE @BaseMaterialCount INT = 0
	DECLARE @CommunityOneOffStandard BIGINT = 102
	DECLARE @CarehomePatientOneOffStandard BIGINT = 202
	DECLARE @SAPItemNumber BIGINT
	DECLARE @SAPUser UNIQUEIDENTIFIER
	DECLARE @ProductId BIGINT = 0
	DECLARE @OrderProductId BIGINT = 0
	DECLARE @CurrentDate DATETIME = GETDATE()
	BEGIN TRY
		BEGIN TRANSACTION;			

		IF (@ACTDELDATE = '')
		BEGIN
			SET @ACTDELDATE = NULL
		END
		ELSE
		BEGIN 
			IF(ISDATE(@ACTDELDATE) != 1)
			BEGIN
				SET @ACTDELDATE = NULL
			END
		END
		IF (@PLANT = '')
		BEGIN
			SET @PLANT = NULL
		END
		IF (@BILLINGDATE = '')
		BEGIN
			SET @BILLINGDATE = NULL
		END
		ELSE
		BEGIN 
			IF(ISDATE(@BILLINGDATE) != 1)
			BEGIN
				SET @BILLINGDATE = NULL
			END
		END
		IF (@CURRENCY = '')
		BEGIN
			SET @CURRENCY = NULL
		END
		IF (@BILLINGQTY = '')
		BEGIN
			SET @BILLINGQTY = NULL
		END
		IF (@BILLINGUNIT = '')
		BEGIN
			SET @BILLINGUNIT = NULL
		END
		IF (@NETVALUE = '')
		BEGIN
			SET @NETVALUE = NULL
		END
		IF (@VATVALUE = '')
		BEGIN
			SET @VATVALUE = NULL
		END
		IF (@PODDATE = '')
		BEGIN
			SET @PODDATE = NULL
		END
		ELSE
		BEGIN 
			IF(ISDATE(@PODDATE) != 1)
			BEGIN
				SET @PODDATE = NULL
			END
		END
		IF (@DELIVERYQTY = '')
		BEGIN
			SET @DELIVERYQTY = NULL
		END
		IF (@DELIVERYUNIT = '')
		BEGIN
			SET @DELIVERYUNIT = NULL
		END
		IF (@INVOICE = '')
		BEGIN
			SET @INVOICE = NULL
		END
		IF (@DELIVERY = '')
		BEGIN
			SET @DELIVERY = NULL
		END
			
			--Set Order Type
			--select @OrderType=OrderType from Orders where OrderId=@OrderID		
				
			SELECT	@OrderType=OrderType, 
					@OrderCreationType = OrderCreationType, 
					@CarehomeId = CareHomeId FROM Orders (NOLOCK) WHERE OrderId=@OrderID		


			SET @SAPUser = (SELECT UserId FROM Users WHERE UserName = 'SAPUser')
			SET @SAPItemNumber = (SELECT CAST(@ORDERITEM AS BIGINT))
			SET @SAPItemNumber = (SELECT ISNULL(@SAPItemNumber,0))
						
			-- Update Order 
			--10098=Send To SAP
			--10099=Failed
			--10048=Generated
			-- If Errorcode is E then set order status Failed(10099) else set Generated(10048) 
								
			IF @ErrorCode = 'E'
			BEGIN
				SET @OrderStatus = 10099
			END
			ELSE IF @ErrorCode = 'C'
			BEGIN
				SET @OrderStatus = 10105
			END
			ELSE
			BEGIN
				SET @OrderStatus = 10048
			END

	
			UPDATE Orders
			SET  
				SAPOrderNo = @SAPOrderNo
				--,InvoiceNo = @INVOICE
				,OrderStatus = @OrderStatus
				--,SAPDeliveryNo = @DELIVERY
				,SAPMessage = @ErrorMessage
				,ModifiedDate= GETDATE()				
			WHERE OrderId = @OrderID

			SELECT @Result = @@ROWCOUNT
			
			-- CR#8 Interface changes. Update IDXORDERPRODUCT TABLE
			IF(@Result > 0 AND @ErrorCode = 'S')
			BEGIN

			    -- CHECK FOR SINGLE PRODUCT ID
				SET @BaseMaterialCount = (SELECT Count(ProductId) FROM Product (NOLOCK) WHERE BaseMaterial = @BASEMATERIALID) --SHOULD BE SINGLE PRODUCT FOR THE BASEMATERIAL							
				IF (@BaseMaterialCount = 1)
				BEGIN
										
					SET @ProductId = (SELECT ProductId FROM Product (NOLOCK) WHERE BaseMaterial = @BASEMATERIALID)											

					-- CHECK FOR ORDER ITEM NUMBER AND INSERT/UPDATE
					IF(@SAPItemNumber>=900)
					BEGIN
						IF NOT EXISTS(SELECT OrderId FROM IDXOrderProduct (NOLOCK) WHERE OrderId = @OrderID AND OrderItem = @ORDERITEM)
						BEGIN									
							INSERT INTO IDXOrderProduct (OrderId, ProductId, Quantity,IsFOC,ModifiedBy,ModifiedDate, IsRemoved, OrderItem)
							VALUES (@OrderID, @ProductId, 0, 0, @SAPUser, GETDATE(), 0, @ORDERITEM)
						END			
					END	
					ELSE
					BEGIN
						UPDATE IDXOrderProduct SET OrderItem = @ORDERITEM WHERE ProductId = @ProductId AND OrderId = @OrderID AND OrderItem IS NULL
					END

					UPDATE IDXOrderProduct
					SET  Plant = @PLANT
						,BillingDate = CAST(@BILLINGDATE AS DATETIME)
						,Currency = @CURRENCY
						,BillingQuantity = CAST(@BILLINGQTY AS DECIMAL(18,3))
						,BillingUnit = @BILLINGUNIT
						,NetValue = CAST(@NETVALUE AS DECIMAL(18,2))
						,VatValue = CAST(@VATVALUE AS DECIMAL(18,2))
						,OrderItem = @ORDERITEM
						,ModifiedDate= GETDATE()												
					WHERE ProductId = @ProductId
					AND OrderId = @OrderID AND OrderItem = @ORDERITEM			

					IF(@ACTDELDATE IS NOT NULL AND @ErrorCode = 'S')
					BEGIN					
						UPDATE IDXOrderProduct 
						SET SAPActualDeliveryDate = CAST(@ACTDELDATE AS DATETIME)
						,SAPDeliveryNo = @DELIVERY
						,InvoiceNo = @INVOICE				
						,ModifiedDate= GETDATE()
						,PODDate = CAST(@PODDATE AS DATETIME)						
						,DeliveryQuantity = CAST(@DELIVERYQTY AS DECIMAL(18,3))
						,DeliveryUnit = @DELIVERYUNIT
						WHERE ProductId = @ProductId
						AND OrderId = @OrderID AND OrderItem = @ORDERITEM																	
					END					
			END				
			ELSE
			BEGIN
				SET @ProductId = -1  -- This means new item product is not available in PRODUCT master table.
			END

				
			END

			
			
			-- If order is updated and Order Type is PatientOrder then Update Patient Table Delevery Date(LDD)
			/*IF @ErrorCode = 'S'*/
			IF (@ErrorCode = 'S' OR @ErrorCode = 'C')
			BEGIN
					--10053 OrderType ZHDP & not one off standard 102 & 202
					IF (@Result > 0 AND @OrderType = 10053  AND @OrderCreationType NOT IN (@CommunityOneOffStandard,@CarehomePatientOneOffStandard))
						BEGIN
							--Update carehome NDD for carehome order
							IF(@IsCarehomeOrder = 1)
							BEGIN
								UPDATE CareHome SET NextDeliveryDate = @CarehomeNDD 
												WHERE CarehomeId = @CarehomeId
							End

							--Update Patient NDD 
							UPDATE Patient
							SET	 DeliveryDate = (SELECT DerivedNDD FROM Orders WHERE OrderId = @OrderID)
								,NextDeliveryDate=@PatientNDD
							WHERE PatientId in(SELECT PatientId FROM Orders WHERE OrderID=@OrderID)						
							SELECT @Result = @@ROWCOUNT ,@ResultMessage = 'Order details,Patient Delivery date and NDD Updated'
					END				
					ELSE 
						BEGIN													
							SELECT @Result = 1 ,@ResultMessage = 'Order is either rush, FOC or one off standard'
						END
			END
			COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH	
		ROLLBACK TRANSACTION;

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		SET @ResultMessage = 'sp: UpdateSapOrderDetails' +' '+ ISNULL(@ResultMessage,'')

		EXEC SaveExceptionLog @UserID = @SAPUser
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage
	END CATCH;

	SELECT @Result Result
		,@ResultMessage ResultMessage
END
