﻿



CREATE PROCEDURE [dbo].[XSCAUpdatePatientFreqZero] 
as
BEGIN
BEGIN TRY
              
BEGIN TRAN --OPEN TRANSACTION

	   DECLARE @Patientid				 bigint
	   DECLARE @Frequency				 bigint

	   
       DECLARE cursor_updatePatientFreqZero CURSOR -- Declare cursor
       LOCAL SCROLL STATIC 
       FOR                          
			select pat.patientid,min(pro.frequency) Frequency
			from patient pat
				inner join PersonalInformation per with (nolock) on per.PersonalInformationId = pat.PersonalInformationId 
				inner join prescription pre with (nolock) on pre.patientid = pat.patientid
				left outer join idxprescriptionproduct pro with (nolock) on pro.prescriptionid = pre.prescriptionid
			where pat.DeliveryFrequency = 0 and pro.frequency is not null
			group by pat.patientid
                     
              OPEN cursor_updatePatientFreqZero
              FETCH NEXT FROM cursor_updatePatientFreqZero
              INTO @Patientid,@Frequency

              WHILE @@FETCH_STATUS = 0
              BEGIN                                 
					 UPDATE Patient 
					    SET DeliveryFrequency = @Frequency
						   ,IsSentToSAP	  = 0
					  WHERE PatientId		  = @Patientid
                     

                     FETCH NEXT FROM cursor_updatePatientFreqZero
                     INTO @Patientid,@Frequency  
              END

       CLOSE cursor_updatePatientFreqZero -- close the cursor
       DEALLOCATE cursor_updatePatientFreqZero -- Deallocate the cursor 

COMMIT TRAN  -- COMMIT TRANSACTION

END TRY 

BEGIN CATCH


ROLLBACK TRAN -- ROLLBACK TRANSACTION
PRINT 'THERE WAS SOME ERROR WHILE UPDATING Patient Frequency Zero'

END CATCH
END





