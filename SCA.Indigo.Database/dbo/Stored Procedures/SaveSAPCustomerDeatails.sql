﻿

/******************************
** File:    SaveSAPCustomerDeatails
** Name:	SaveSAPCustomerDeatails
** Desc:	Save Customer Details
** Auth:	Arvind
** Date:	10/12/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
** 1	15/06/2015		Sagar		Added Document Comment		
** 2	17/07/2015		Arvind		Added Check for SAP customerId length
** 3	29/07/2015		Arvind		Removed  EXEC DeleteIDXCustomerProduct @CustomerId from procedure , Now it be called after all the product inserted
*******************************/

CREATE PROCEDURE [dbo].[SaveSAPCustomerDeatails] (
	-- Add the parameters for the stored procedure here
	--Parameter for Address table
	 @HouseNumber VARCHAR(16) = NULL
	,@AddressLine1 VARCHAR(128) = NULL
	,@AddressLine2 VARCHAR(128) = NULL
	,@City VARCHAR(128) = NULL
	,@County VARCHAR(64) = NULL
	,@Country VARCHAR(64) = NULL
	,@PostCode VARCHAR(20) = NULL
	,@HouseName VARCHAR(64) = NULL
	
	--Parameter for Personal information table
	,@FirstName NVARCHAR(100)
	,@TelephoneNumber VARCHAR(50) = NULL
	,@MobileNumber VARCHAR(50) = NULL
	,@Email NVARCHAR(50) = NULL
	,@FaxNumber NVARCHAR(50) = NULL
	
	--Parameter for customer table
	,@SAPCustomerNumber VARCHAR(50)
	,@SalesOrg VARCHAR(10) = NULL
	,@DistrChanel VARCHAR(10) = NULL
	,@Division VARCHAR(10) = NULL
	,@Haulier VARCHAR(10) = NULL
	,@ModifiedBy varchar(100)
	,@ModifiedDate DATETIME=null
	,@CustomerId varchar(50) out
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	--DECLARE @CustomerId BIGINT
	DECLARE @PersonalInformationId BIGINT
	DECLARE @AddressId BIGINT
	DECLARE @AddressResult INT
	DECLARE @PersonalInfoResult INT
	DECLARE @CustomerResult INT
	DECLARE @Result varchar(200)	
	DECLARE @CommondType INT---- 1=Insert and 2=Update	
	DECLARE @SapIdLength INT=10	

	BEGIN TRY
			BEGIN TRANSACTION;	

	--Set SAP customer with preceding zero if SAPCustomerId lenth is less than 10
	select @SAPCustomerNumber= replicate('0', @SapIdLength - len(@SAPCustomerNumber)) + cast (@SAPCustomerNumber AS VARCHAR)
	SET @ModifiedDate=GETDATE()
	--Set the CustomerID, AddressId and PersonalInformationId
	SELECT @CustomerId = CustomerId
			,@PersonalInformationId = PersonalInformationId
		FROM Customer
		WHERE SAPCustomerNumber = @SAPCustomerNumber

		SELECT @AddressId = AddressId
		FROM PersonalInformation
		WHERE PersonalInformationId = @PersonalInformationId

		
			

	IF NOT EXISTS (SELECT CustomerId FROM Customer WHERE SAPCustomerNumber = @SAPCustomerNumber)
	BEGIN
	set @CommondType=1
		-- Insert into Address table
		INSERT INTO Address (
			HouseNumber
			,AddressLine1
			,AddressLine2
			,City
			,County
			,Country
			,PostCode
			,HouseName
			,ModifiedBy
			,ModifiedDate
			)
		VALUES (
			@HouseNumber
			,@AddressLine1
			,@AddressLine2
			,@City
			,@County
			,@Country
			,@PostCode
			,@HouseName
			,@ModifiedBy
			,@ModifiedDate
			)

		SELECT @AddressId = SCOPE_IDENTITY()

		--select @AddressId
		-- Insert PersonalInformation table 
		INSERT INTO PersonalInformation (
			FirstName
			,AddressId
			,TelephoneNumber
			,MobileNumber
			,Email
			,FaxNumber
			,CreatedById
			,CreatedDateTime
			,ModifiedBy
			,ModifiedDate
			)
		VALUES (
			@FirstName
			,@AddressId
			,@TelephoneNumber
			,@MobileNumber
			,@Email
			,@FaxNumber
			,@ModifiedBy
			,@ModifiedDate
			,@ModifiedBy
			,@ModifiedDate
			)

		SELECT @PersonalInformationId = SCOPE_IDENTITY()

		--select @PersonalInformationId
		--Insert into customer table
		INSERT INTO Customer (
			PersonalInformationId
			,SAPCustomerNumber
			,SalesOrg
			,DistrChanel
			,Division
			,Haulier
			,ModifiedBy
			,ModifiedDate
			)
		VALUES (
			@PersonalInformationId
			,@SAPCustomerNumber
			,@SalesOrg
			,@DistrChanel
			,@Division
			,@Haulier
			,@ModifiedBy
			,@ModifiedDate
			)

		SELECT @CustomerId = SCOPE_IDENTITY()
		exec [dbo].[SaveCustomerParameter] @CustomerId
			
	END
			--If customer ID exists then Update The Address, PersonalInformation and Customer tables
	ELSE
	BEGIN	
	set @CommondType=2
		----------Address Update Start----------------------------------------------------------
		UPDATE [Address]
		SET HouseNumber = @HouseNumber
			,AddressLine1 = @AddressLine1
			,AddressLine2 = @AddressLine2
			,City = @City
			,County = @County
			,Country = @Country
			,PostCode = @PostCode
			,HouseName = @HouseName
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = GETDATE()
		WHERE Addressid = @AddressId

		SET @AddressResult = @@ROWCOUNT

		----------Address Update End--------------------------------------------------------------
		----------PersonalInformation Update Start------------------------------------------------
		UPDATE PersonalInformation
		SET FirstName = @FirstName
			,
			--AddressId=@AddressId,
			TelephoneNumber = @TelephoneNumber
			,MobileNumber = @MobileNumber
			,Email = @Email
			,FaxNumber = @FaxNumber
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = GETDATE()
		WHERE PersonalInformationId = @PersonalInformationId

		SET @PersonalInfoResult = @@ROWCOUNT

		----------PersonalInformation Update End-------------------------------------------------
		UPDATE Customer
		SET
			--PersonalInformationId=@PersonalInformationId,
			--SAPCustomerNumber=@SAPCustomerNumber,
			SalesOrg = @SalesOrg
			,DistrChanel = @DistrChanel
			,Division = @Division
			,Haulier = @Haulier
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = GETDATE()
		WHERE CustomerId = @CustomerId

		SET @CustomerResult = @@ROWCOUNT

		
	END			

		IF @CommondType = 1 AND @CustomerId > 0 
		BEGIN			
			set @Result='Data inserted successfully'
		END

		ELSE IF @CommondType = 2 AND @AddressResult > 0 AND @PersonalInfoResult > 0 AND @CustomerResult > 0			
		BEGIN			
			set @Result='Data updated successfully'
		END

		ELSE
		BEGIN
			set @CustomerId=-1
			set @Result='Failed to save data'
		END
				
		select @CustomerId CustomerId,  @Result Result, @CommondType CommondType
	COMMIT TRANSACTION;
	END TRY

		BEGIN CATCH
			ROLLBACK TRANSACTION;
			Return  @@error 
		END CATCH;	
END


