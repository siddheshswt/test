﻿


/******************************
** File:    GetSAPCustomerPatientList
** Name:	GetSAPCustomerPatientList
** Desc:	Get Customer Patient Details
** Auth:	Arvind
** Date:	29/12/2014

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1   22/05/2015		Siddhesh	Send Round associated with Patient from Patient/carehome table instead Default
**	2	15/06/2015		Sagar		Added Document Comment	
**	3	15/07/2015		Arvind		Changed HouseName column fetch condition, it should be always coming from address table even for carehome, beacause housename will be same as carehome name
**	4	03/11/2016		Siddhesh	Added Telephone number for escaper character function
**	5	16/01/2017		Siddhesh	removed escaper character function
*******************************/
-- =============================================
-- exec GetSAPCustomerPatientList ''

CREATE proc [dbo].[GetSAPCustomerPatientList]
(
@Filters varchar(250)
)
as begin

--Declare @DateCondition datetime
--set @DateCondition=DATEADD(Minute,-65,Getdate())
Exec [dbo].[XSCAResetUser]

select 
'PA' CustomerType, -- for Patient CustomerType is PA
patient.PatientId FrontEndId,
SAPCustomerNumber,
SalesOrg,
DistrChanel,
Division,
patient.SAPPatientNumber SAPId,
careHome.SAPCareHomeNumber as CareHomeId,
--patient.CareHomeId, 
listPatientStatus.Description PatientStatus, 
listPatientType.Description PatientType,
(TitleList.DefaultText) Title, 
 (patientinfo.FirstName+' '+patientinfo.LastName) Name,
 (addr.HouseName) CareHomeName,
 (patientinfo.FirstName)as FirstName,
 (patientinfo.LastName) LastName,
 (addr.HouseNumber) HouseNumber,
 (addr.AddressLine1) STREET,
 (addr.AddressLine2) AddressLine2,
''  AddressLine3,
(addr.City) City,
addr.PostCode,
 (addr.County) County,
'GB' Country,
 (patientinfo.TelephoneNumber) TelephoneNumber,
patient.DeliveryFrequency,
patient.RoundId,
--'GBHD000090' Round,
Patient.Round,
'' SEQUENCE, 
convert(varchar, patient.DeliveryDate , 112) LDDT,
convert(varchar, patient.NextDeliveryDate , 112) NDDT,
--'' NDDT,
(patient.ADP1) DELINST,
(patient.ADP2) ADP
from 
Customer customer with (nolock)
inner join Patient patient with (nolock) on patient.CustomerId=customer.CustomerId
left outer join CareHome carehome  with (nolock)  on patient.CareHomeId= carehome.CareHomeId
inner join List listPatientStatus  with (nolock) on listPatientStatus.ListId=patient.PatientStatus
inner join List listPatientType  with (nolock) on listPatientType.ListId=patient.PatientType
inner join PersonalInformation  patientinfo with (nolock) on patientinfo.PersonalInformationId=patient.PersonalInformationId
inner join List TitleList  with (nolock) on TitleList.ListId=patientinfo.TitleId
left outer join Address addr with (nolock) on patientinfo.AddressId=addr.AddressId
where 
patient.IsSentToSAP=0
--patient.ModifiedDate>=@DateCondition 
-- and Patient.PatientId=236
-- Select Statement for Carehome
union all
select 
'HO' CustomerType, -- for Carehome CustomerType is HO
carehome.CareHomeId FrontEndId,
SAPCustomerNumber,
SalesOrg,
DistrChanel,
Division,
CareHome.SAPCareHomeNumber SAPId, 
'' CareHomeId, 
''  PatientStatus, 
'' PatientType,
'' Title, 
 (addr.HouseName) Name,
'' CareHomeName,
 (CareHomeInfo.FirstName) FirstName,
 (CareHomeInfo.LastName) LastName,
 (addr.HouseNumber) HouseNumber,
 (addr.AddressLine1) STREET,
 (addr.AddressLine2) AddressLine2,
''  AddressLine3,
 (addr.City) City,
addr.PostCode,
 (addr.County) County,
'GB' Country,
 (CareHomeInfo.TelephoneNumber) TelephoneNumber,
carehome.DeliveryFrequency,
carehome.RoundId,
carehome.Round,
--'GBHD000090' Round,
'' SEQUENCE,
'' LDDT, 
convert(varchar, carehome.NextDeliveryDate , 112) NDDT,
'' DELINST,
'' ADP
from 
Customer customer with (nolock)
inner join CareHome carehome with (nolock)  on carehome.CustomerId = customer.CustomerId
inner join PersonalInformation CareHomeInfo with (nolock) on CareHomeInfo.PersonalInformationId=carehome.PersonalInformationId
left outer join Address addr with (nolock)  on CareHomeInfo .AddressId=addr.AddressId
where 
carehome.IsSentToSAP=0
--carehome.ModifiedDate>=@DateCondition 
--and carehome.carehomeId=273
end




