﻿CREATE TABLE [dbo].[IDXInteractiontypeSubType] (
    [IDXInteractiontypeSubTypeId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [InteractiontypeId]           BIGINT           NOT NULL,
    [InteractionsubtypeId]        BIGINT           NOT NULL,
    [ModifiedBy]                  UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]                DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXInteractiontypeSubType] PRIMARY KEY CLUSTERED ([IDXInteractiontypeSubTypeId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXInteractiontypeSubType_List] FOREIGN KEY ([InteractiontypeId]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_IDXInteractiontypeSubType_List1] FOREIGN KEY ([InteractionsubtypeId]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXInteractiontypeSubType_InteractionsubtypeId_FK]
    ON [dbo].[IDXInteractiontypeSubType]([InteractionsubtypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXInteractiontypeSubType_InteractiontypeId_FK]
    ON [dbo].[IDXInteractiontypeSubType]([InteractiontypeId] ASC);

