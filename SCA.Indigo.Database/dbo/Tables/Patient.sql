﻿CREATE TABLE [dbo].[Patient] (
    [PatientId]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [SAPPatientNumber]        NVARCHAR (64)    NULL,
    [PersonalInformationId]   BIGINT           NULL,
    [DateofBirth]             DATETIME         NULL,
    [PatientStatus]           BIGINT           NULL,
    [PatientType]             BIGINT           NULL,
    [LastModifiedBy]          BIGINT           NULL,
    [ContactPersonId]         BIGINT           NULL,
    [DeliveryInstructionText] TEXT             NULL,
    [PrescriptionId]          BIGINT           NOT NULL,
    [DeliveryFrequency]       VARCHAR (2)      NULL,
    [RoundId]                 VARCHAR (4)      NULL,
    [AlternateDeliveryPoint]  TEXT             NULL,
    [NHSId]                   NVARCHAR (16)    NULL,
    [LocalId]                 NVARCHAR (16)    NULL,
    [CustomerId]              BIGINT           NULL,
    [CarehomeAssignmentDate]  DATETIME         NULL,
    [AssessmentDate]          DATETIME         NULL,
    [NextAssessmentDate]      DATETIME         NULL,
    [DeliveryDate]            DATETIME         NULL,
    [CommunicationFormat]     BIGINT           NULL,
    [CareHomeId]              BIGINT           NULL,
    [ReasonCodeID]            BIGINT           NULL,
    [ADP1]                    TEXT             NULL,
    [ADP2]                    TEXT             NULL,
    [ModifiedBy]              UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]            DATETIME         NOT NULL,
    [Round]                   VARCHAR (10)     NULL,
    [NextDeliveryDate]        DATETIME         NULL,
    [BillTo]                  BIGINT           NULL,
    [IsSentToSAP]             BIT              DEFAULT ((0)) NULL,
    [IsDataProtected]         BIT              CONSTRAINT [DF_Patient_IsDataProtected] DEFAULT ((0)) NULL,
    [LoadId]                  BIGINT           NULL,
    [Remarks]                 NVARCHAR (MAX)   NULL,
    [RemovedStoppedDateTime]  DATETIME         NULL,
    CONSTRAINT [PK_Patients] PRIMARY KEY CLUSTERED ([PatientId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Patient_CareHome] FOREIGN KEY ([CareHomeId]) REFERENCES [dbo].[CareHome] ([CareHomeId]),
    CONSTRAINT [FK_Patient_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Patient_List] FOREIGN KEY ([ReasonCodeID]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Patient_PatientStatusList] FOREIGN KEY ([PatientStatus]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Patient_PatientTypeList] FOREIGN KEY ([PatientType]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Patient_PersonalInformation] FOREIGN KEY ([PersonalInformationId]) REFERENCES [dbo].[PersonalInformation] ([PersonalInformationId])
);


GO
CREATE NONCLUSTERED INDEX [ix_Patient]
    ON [dbo].[Patient]([PersonalInformationId] ASC, [PatientType] ASC, [PatientStatus] ASC, [PatientId] ASC, [CustomerId] ASC, [CareHomeId] ASC)
    INCLUDE([SAPPatientNumber], [DateofBirth], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CareHomeId]
    ON [dbo].[Patient]([CareHomeId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CareHomeId_FK]
    ON [dbo].[Patient]([CareHomeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CareHomeId_Search]
    ON [dbo].[Patient]([CareHomeId] ASC)
    INCLUDE([PatientId], [SAPPatientNumber], [PersonalInformationId], [PatientStatus], [PatientType], [DeliveryFrequency], [CustomerId]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CustomerId_CareHomeId_DeliveryDate]
    ON [dbo].[Patient]([CustomerId] ASC, [CareHomeId] ASC, [DeliveryDate] ASC)
    INCLUDE([PatientId], [PersonalInformationId], [DateofBirth], [PatientStatus], [DeliveryFrequency], [AssessmentDate], [NextAssessmentDate], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CustomerId_CareHomeId_DeliveryDate_Search]
    ON [dbo].[Patient]([CustomerId] ASC, [CareHomeId] ASC, [DeliveryDate] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CustomerId_DeliveryDate]
    ON [dbo].[Patient]([CustomerId] ASC, [DeliveryDate] ASC)
    INCLUDE([PatientId], [PersonalInformationId], [DateofBirth], [PatientStatus], [DeliveryFrequency], [AssessmentDate], [NextAssessmentDate], [CareHomeId], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CustomerId_DeliveryDate_CareHomeId]
    ON [dbo].[Patient]([CustomerId] ASC, [DeliveryDate] ASC, [CareHomeId] ASC)
    INCLUDE([PatientId], [PersonalInformationId], [DateofBirth], [PatientStatus], [DeliveryFrequency], [AssessmentDate], [NextAssessmentDate], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_CustomerId_FK]
    ON [dbo].[Patient]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_PatientStatus_FK]
    ON [dbo].[Patient]([PatientStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_PatientStatus_PatientType_CustomerId]
    ON [dbo].[Patient]([PatientStatus] ASC, [PatientType] ASC, [CustomerId] ASC)
    INCLUDE([PatientId], [SAPPatientNumber], [PersonalInformationId], [AssessmentDate], [NextAssessmentDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_PatientType_CustomerId]
    ON [dbo].[Patient]([PatientType] ASC, [CustomerId] ASC)
    INCLUDE([PatientId], [SAPPatientNumber], [PersonalInformationId], [AssessmentDate], [NextAssessmentDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_PatientType_FK]
    ON [dbo].[Patient]([PatientType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_PersonalInformationId_FK]
    ON [dbo].[Patient]([PersonalInformationId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_ReasonCodeID_FK]
    ON [dbo].[Patient]([ReasonCodeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Patient_Include]
    ON [dbo].[Patient]([CustomerId] ASC, [PatientType] ASC, [PatientStatus] ASC, [PatientId] ASC, [PersonalInformationId] ASC, [CareHomeId] ASC)
    INCLUDE([SAPPatientNumber], [DateofBirth], [NextDeliveryDate]);

