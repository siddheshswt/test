﻿CREATE TABLE [dbo].[IDXUserCustomer] (
    [IDXUserCustomerId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [CustomerId]        BIGINT           NOT NULL,
    [ModifiedBy]        UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]      DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXUserCustomer] PRIMARY KEY CLUSTERED ([IDXUserCustomerId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXUserCustomer_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_IDXUserCustomer_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXUserCustomer_CustomerId_FK]
    ON [dbo].[IDXUserCustomer]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXUserCustomer_UserId_FK]
    ON [dbo].[IDXUserCustomer]([UserId] ASC);

