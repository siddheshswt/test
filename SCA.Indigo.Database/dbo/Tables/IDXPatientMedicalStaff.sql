﻿CREATE TABLE [dbo].[IDXPatientMedicalStaff] (
    [IDXPatientMedicalStaffId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerMedicalStaffId]   BIGINT           NULL,
    [PatientId]                BIGINT           NULL,
    [ModifiedBy]               UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]             DATETIME         NOT NULL,
    [IsClinicalLinkage]        BIT              DEFAULT ((0)) NULL,
    [Remarks]                  NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_IDXPatientMedicalStaffs] PRIMARY KEY CLUSTERED ([IDXPatientMedicalStaffId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXPatientMedicalStaff_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientMedicalStaff_CustomerMedicalStaffId_PatientId]
    ON [dbo].[IDXPatientMedicalStaff]([CustomerMedicalStaffId] ASC, [PatientId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientMedicalStaff_PatientId]
    ON [dbo].[IDXPatientMedicalStaff]([PatientId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientMedicalStaff_PatientId_FK]
    ON [dbo].[IDXPatientMedicalStaff]([PatientId] ASC);


GO
-- =============================================
-- Author:		Saurabh Mayekar
-- Create date: 2016-05-13
-- Description:	Delete Entries Where PatientId Is Null
-- =============================================
-- ===============================================================================

--		Modified Date			Modified By			Purpose
-- 1.   13-05-2016				Siddhesh            Added  IF UPDATE (PatientId) condition and @PatientId, @IDXPatientMedicalStaffId variables
-- ===============================================================================
CREATE TRIGGER trgDeleteMedicalStaffDetails 
ON IDXPatientMedicalStaff
AFTER UPDATE
AS
BEGIN

DECLARE @Result        INT          = 0,
        @ResultMessage VARCHAR(100) = '',
		@CurrentDate   DATETIME     = GETDATE(),
		@UserID        VARCHAR(100) = '',
		@PatientId	   BIGINT = 0,
		@IDXPatientMedicalStaffId BIGINT = 0;

   SET NOCOUNT ON;
   BEGIN TRY
      BEGIN TRAN
	      SELECT TOP 1 @UserID = ModifiedBy FROM INSERTED

		  IF UPDATE (PatientId)
		  BEGIN
			  SELECT @PatientId = ISNULL(IDXPatientMedicalStaff.PatientId,0), 
					 @IDXPatientMedicalStaffId = IDXPatientMedicalStaff.IDXPatientMedicalStaffId 
			  FROM INSERTED IDXPatientMedicalStaff	
		  
			  IF (@PatientId = 0)
			  BEGIN 
				DELETE FROM IDXPatientMedicalStaff WHERE IDXPatientMedicalStaffId =  @IDXPatientMedicalStaffId				
			  END			  
		  END		  	 		 
		--   DELETE IDXPatientMedicalStaff
		  --FROM   IDXPatientMedicalStaff idxpms
		  --INNER JOIN INSERTED ins
		  --ON    idxpms.IDXPatientMedicalStaffId = ins.IDXPatientMedicalStaffId
		  --WHERE ISNULL(ins.PatientId,'') = ''
	  COMMIT
   END TRY
   BEGIN CATCH
      ROLLBACK

      SELECT @Result = @@ERROR, @ResultMessage = ERROR_MESSAGE()

      -- Insert exception in exception log		
      EXEC SaveExceptionLog @UserID = @UserID, @TimeStamp = @CurrentDate, @ErrorMessage = @ResultMessage
   END CATCH
   SET NOCOUNT OFF;
END