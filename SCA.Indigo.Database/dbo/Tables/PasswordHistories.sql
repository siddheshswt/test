﻿CREATE TABLE [dbo].[PasswordHistories] (
    [Id]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]       UNIQUEIDENTIFIER NULL,
    [Password]     NVARCHAR (MAX)   NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_PasswordHistories] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PasswordHistories_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PasswordHistories_UserId_FK]
    ON [dbo].[PasswordHistories]([UserId] ASC);

