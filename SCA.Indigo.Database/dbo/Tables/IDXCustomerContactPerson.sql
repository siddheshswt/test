﻿CREATE TABLE [dbo].[IDXCustomerContactPerson] (
    [IDXId]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]      BIGINT           NULL,
    [ContactPersonId] BIGINT           NULL,
    [ModifiedBy]      UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]    DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXCustomerContactPersons] PRIMARY KEY CLUSTERED ([IDXId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXCustomerContactPerson_ContactPerson] FOREIGN KEY ([ContactPersonId]) REFERENCES [dbo].[ContactPerson] ([ContactPersonId]),
    CONSTRAINT [FK_IDXCustomerContactPerson_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerContactPerson_ContactPersonId_FK]
    ON [dbo].[IDXCustomerContactPerson]([ContactPersonId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerContactPerson_CustomerId_FK]
    ON [dbo].[IDXCustomerContactPerson]([CustomerId] ASC);

