﻿CREATE TABLE [dbo].[DebugErrorLog] (
    [LogId]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [ErrorMessage] VARCHAR (MAX) NULL,
    [InputData]    VARCHAR (MAX) NULL,
    [TimeStamp]    DATETIME      NULL,
    CONSTRAINT [PK_DebugErrorLog] PRIMARY KEY CLUSTERED ([LogId] ASC) WITH (FILLFACTOR = 80)
);

