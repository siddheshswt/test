﻿CREATE TABLE [dbo].[CareHome] (
    [CareHomeId]            BIGINT           IDENTITY (1, 1) NOT NULL,
    [SAPCareHomeNumber]     VARCHAR (50)     NULL,
    [CustomerId]            BIGINT           NULL,
    [PersonalInformationId] BIGINT           NULL,
    [DeliveryFrequency]     VARCHAR (2)      NULL,
    [RoundId]               VARCHAR (4)      NULL,
    [NextDeliveryDate]      DATETIME         NULL,
    [ModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]          DATETIME         NOT NULL,
    [Round]                 VARCHAR (10)     NULL,
    [BillTo]                BIGINT           NULL,
    [IsSentToSAP]           BIT              DEFAULT ((0)) NULL,
    [CareHomeType]          BIGINT           NULL,
    [OrderType]             BIGINT           NULL,
    [PurchaseOrderNo]       VARCHAR (50)     NULL,
    [CareHomeStatus]        BIGINT           NULL,
    [CommunicationFormat]   BIGINT           NULL,
    [LoadId]                BIGINT           NULL,
    [Remarks]               NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_CareHomes] PRIMARY KEY CLUSTERED ([CareHomeId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Carehome_CareHomeStatus] FOREIGN KEY ([CareHomeStatus]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Carehome_CareHomeType] FOREIGN KEY ([CareHomeType]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Carehome_CommunicationFormat] FOREIGN KEY ([CommunicationFormat]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_CareHome_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Carehome_OrderType] FOREIGN KEY ([OrderType]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_CareHomePerson] FOREIGN KEY ([PersonalInformationId]) REFERENCES [dbo].[PersonalInformation] ([PersonalInformationId])
);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_CareHomeStatus_FK]
    ON [dbo].[CareHome]([CareHomeStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_CareHomeType_FK]
    ON [dbo].[CareHome]([CareHomeType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_CommunicationFormat_FK]
    ON [dbo].[CareHome]([CommunicationFormat] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_CustomerId_FK]
    ON [dbo].[CareHome]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_OrderType_FK]
    ON [dbo].[CareHome]([OrderType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_PersonalInformationId_FK]
    ON [dbo].[CareHome]([PersonalInformationId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_Search]
    ON [dbo].[CareHome]([CustomerId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_CareHome_CareHomeID_PersonalInformationId]
    ON [dbo].[CareHome]([CareHomeId] ASC, [PersonalInformationId] ASC);


GO
-- =============================================
-- Author:		Arvind Nishad
-- Create date: 2015-04-22
-- Description:	Update Patient Frequency
-- =============================================
-- ===============================================================================

--		Modified Date			Modified By			Purpose
--	1.	 14-May-2015			Siddhesh Sawant		Changed updateion logic to update if column value has changed and added round and round id columns
--	2.	 24-May-2016			Siddhesh Sawant		Added IsSentToSAP = 0 for patient

-- ===============================================================================
CREATE TRIGGER [dbo].[tgrUpdatePatientFrequency]
   ON  [dbo].[CareHome]
   AFTER UPDATE
AS 
BEGIN	

    -- Insert statements for trigger here
	Declare @CareHomeId bigint=0
	Declare @Frequency int=0
	DECLARE @Result INT = 0
	DECLARE @Round Varchar(10)
	DECLARE @RoundId Varchar(4)	
	DECLARE @UserId varchar(100)	
	Declare @ResultMessage varchar(100)
	DECLARE @CurrentDate DATETIME = getdate()
	select  @UserId=UserId from Users where  username='SAPUser'


	BEGIN TRY
		BEGIN TRANSACTION;
		BEGIN
			IF UPDATE (DeliveryFrequency) OR UPDATE(Round) OR UPDATE (RoundId) 
			BEGIN			
				SELECT 
						@CareHomeId=carehome.CareHomeId,
						@Frequency=carehome.DeliveryFrequency,
						@Round=carehome.Round,
						@RoundId = carehome.RoundId
						from inserted carehome
					
				UPDATE Patient SET 
				DeliveryFrequency = @Frequency,
				Round = @Round,
				RoundId = @RoundId,
				IsSentToSAP = 0,
				ModifiedDate = @CurrentDate,
				ModifiedBy=@UserId
			
				WHERE CareHomeId =@CareHomeId

				SELECT @Result = @@ROWCOUNT			
			END			

		END		

		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		SELECT @Result = - 1
			,@ResultMessage = ERROR_MESSAGE()

		-- Insert exception in exception log		
		EXEC SaveExceptionLog @UserID = @UserId
			,@TimeStamp = @CurrentDate
			,@ErrorMessage = @ResultMessage
	END CATCH;

END
