﻿CREATE TABLE [dbo].[PrescriptionNotes] (
    [PrescriptionNoteId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PatientId]          BIGINT           NULL,
    [Note]               NVARCHAR (MAX)   NULL,
    [CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME         NOT NULL,
    CONSTRAINT [PK_PrescriptionNotes] PRIMARY KEY CLUSTERED ([PrescriptionNoteId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PrescriptionNotes_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PrescriptionNotes_PatientId_FK]
    ON [dbo].[PrescriptionNotes]([PatientId] ASC);

