﻿CREATE TABLE [dbo].[TranslationProxy] (
    [TransProxyID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [Text]         NVARCHAR (50)    NOT NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_TranslationProxy] PRIMARY KEY CLUSTERED ([TransProxyID] ASC) WITH (FILLFACTOR = 80)
);

