﻿CREATE TABLE [dbo].[XSCANewPatients] (
    [frontendid]        BIGINT         NULL,
    [SapCustomerNumber] NVARCHAR (64)  NULL,
    [Trustname]         NVARCHAR (100) NULL,
    [patienttype]       NVARCHAR (3)   NULL,
    [SapPatientnumber]  NVARCHAR (64)  NULL,
    [Title]             NVARCHAR (20)  NULL,
    [Firstname]         NVARCHAR (100) NULL,
    [LastName]          NVARCHAR (100) NULL,
    [AddressLine1]      NVARCHAR (80)  NULL,
    [AddressLine2]      NVARCHAR (80)  NULL,
    [AddressLine3]      NVARCHAR (80)  NULL,
    [City]              NVARCHAR (64)  NULL,
    [County]            VARCHAR (100)  NULL,
    [PostCode]          NVARCHAR (10)  NULL,
    [Trusttelephone]    NVARCHAR (100) NULL,
    [Customertelephone] NVARCHAR (30)  NULL,
    [Route]             NVARCHAR (3)   NULL,
    [Frequency]         NVARCHAR (20)  NULL,
    [Firstdelivery]     NVARCHAR (10)  NULL,
    [PatientChanged]    NVARCHAR (10)  NULL,
    [PatientCreated]    DATETIME       NULL,
    [Extracted]         NVARCHAR (10)  NULL,
    [Inserteddate]      DATETIME       NOT NULL
);

