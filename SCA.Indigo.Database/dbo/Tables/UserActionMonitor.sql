﻿CREATE TABLE [dbo].[UserActionMonitor] (
    [ActionMonitorId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [LastAccessHome]  DATETIME         NOT NULL,
    CONSTRAINT [PK_ActionMonitorId] PRIMARY KEY CLUSTERED ([ActionMonitorId] ASC)
);

