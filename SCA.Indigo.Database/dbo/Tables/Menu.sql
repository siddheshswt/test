﻿CREATE TABLE [dbo].[Menu] (
    [MenuId]             BIGINT           IDENTITY (1, 1) NOT NULL,
    [MenuName]           NVARCHAR (32)    NULL,
    [ParentMenuID]       BIGINT           NULL,
    [ControllerName]     NVARCHAR (50)    NULL,
    [ActionName]         NVARCHAR (50)    NULL,
    [ModifiedBy]         UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]       DATETIME         NOT NULL,
    [IsDisplay]          BIT              DEFAULT ((0)) NULL,
    [ParentMenuIndex]    INT              NULL,
    [DisplayIndex]       INT              NULL,
    [TranslationProxyId] BIGINT           NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([MenuId] ASC) WITH (FILLFACTOR = 80),
    FOREIGN KEY ([TranslationProxyId]) REFERENCES [dbo].[TranslationProxy] ([TransProxyID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Menu_TranslationProxyId_FK]
    ON [dbo].[Menu]([TranslationProxyId] ASC);

