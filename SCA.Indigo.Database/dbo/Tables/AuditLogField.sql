﻿CREATE TABLE [dbo].[AuditLogField] (
    [AuditLogFieldId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [TableName]       VARCHAR (MAX) NOT NULL,
    [ColumnName]      VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_AuditLogField] PRIMARY KEY CLUSTERED ([AuditLogFieldId] ASC) WITH (FILLFACTOR = 80)
);

