﻿CREATE TABLE [dbo].[Interaction] (
    [InteractionId]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]           BIGINT           NOT NULL,
    [PatientId]            BIGINT           NULL,
    [CarehomeId]           BIGINT           NULL,
    [InteractionTypeId]    BIGINT           NOT NULL,
    [InteractionSubTypeId] BIGINT           NOT NULL,
    [StatusId]             BIGINT           NOT NULL,
    [Description]          NVARCHAR (MAX)   NULL,
    [DisplayAsAlert]       BIT              NULL,
    [AssignToUserId]       UNIQUEIDENTIFIER NULL,
    [ModifiedBy]           UNIQUEIDENTIFIER NULL,
    [ModifiedDate]         DATETIME         NULL,
    [CreatedDate]          DATETIME         NULL,
    [CreatedBy]            UNIQUEIDENTIFIER NULL,
    [OrderId]              BIGINT           NULL,
    [ResolvedDate]         DATETIME         NULL,
    [ResolvedBy]           UNIQUEIDENTIFIER NULL,
    [ClosedDate]           DATETIME         NULL,
    [ClosedBy]             UNIQUEIDENTIFIER NULL,
    [Resolved]             BIT              NULL,
    [IsInteractionViewed]  BIT              NULL,
    CONSTRAINT [PK_Interaction] PRIMARY KEY CLUSTERED ([InteractionId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Interaction_careHome] FOREIGN KEY ([CarehomeId]) REFERENCES [dbo].[CareHome] ([CareHomeId]),
    CONSTRAINT [FK_Interaction_customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Interaction_patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId]),
    CONSTRAINT [FK_interactionstatus_list] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_interactionsubtype_list] FOREIGN KEY ([InteractionSubTypeId]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_interactiontype_list] FOREIGN KEY ([InteractionTypeId]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Interaction_CarehomeId_FK]
    ON [dbo].[Interaction]([CarehomeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interaction_CustomerId_FK]
    ON [dbo].[Interaction]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interaction_InteractionSubTypeId_FK]
    ON [dbo].[Interaction]([InteractionSubTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interaction_InteractionTypeId_FK]
    ON [dbo].[Interaction]([InteractionTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interaction_PatientId_FK]
    ON [dbo].[Interaction]([PatientId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interaction_StatusId_FK]
    ON [dbo].[Interaction]([StatusId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX1_InteractionAABF5]
    ON [dbo].[Interaction]([DisplayAsAlert] ASC, [AssignToUserId] ASC)
    INCLUDE([InteractionId], [CustomerId], [PatientId], [CarehomeId], [InteractionTypeId], [InteractionSubTypeId], [StatusId], [Description], [ModifiedBy], [ModifiedDate], [CreatedDate], [CreatedBy], [ResolvedDate], [ResolvedBy], [ClosedDate], [ClosedBy], [Resolved]);

