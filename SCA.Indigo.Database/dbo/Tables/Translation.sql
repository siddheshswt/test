﻿CREATE TABLE [dbo].[Translation] (
    [TranslationId]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [TranslationType] NVARCHAR (MAX)   NULL,
    [LanguageID]      BIGINT           NOT NULL,
    [TransProxyID]    BIGINT           NOT NULL,
    [ModifiedBy]      UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]    DATETIME         NOT NULL,
    [Description]     NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Translations] PRIMARY KEY CLUSTERED ([TranslationId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Translation_TranslationProxy] FOREIGN KEY ([TransProxyID]) REFERENCES [dbo].[TranslationProxy] ([TransProxyID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Translation_TransProxyID_FK]
    ON [dbo].[Translation]([TransProxyID] ASC);

