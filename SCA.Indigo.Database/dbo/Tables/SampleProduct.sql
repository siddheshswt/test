﻿CREATE TABLE [dbo].[SampleProduct] (
    [SampleProductId]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [HelixProductID]      VARCHAR (500)    NULL,
    [ClientProductID]     VARCHAR (500)    NULL,
    [ProductDescription]  NVARCHAR (MAX)   NULL,
    [MaxQuantity]         BIGINT           NULL,
    [CostPrice]           DECIMAL (18)     NULL,
    [CostValue]           DECIMAL (18)     NULL,
    [UnitPriceBand1]      DECIMAL (18)     NULL,
    [UnitPriceBandValue1] DECIMAL (18)     NULL,
    [UnitPriceBand2]      DECIMAL (18)     NULL,
    [UnitPriceBandValue2] DECIMAL (18)     NULL,
    [UnitPriceBand3]      DECIMAL (18)     NULL,
    [UnitPriceBandValue3] DECIMAL (18)     NULL,
    [UnitPriceBand4]      DECIMAL (18)     NULL,
    [UnitPriceBandValue4] DECIMAL (18)     NULL,
    [UnitPriceBand5]      DECIMAL (18)     NULL,
    [UnitPriceBandValue5] DECIMAL (18)     NULL,
    [ModifiedBy]          UNIQUEIDENTIFIER NULL,
    [ModifiedDate]        DATETIME         NULL,
    CONSTRAINT [PK_SampleProduct] PRIMARY KEY CLUSTERED ([SampleProductId] ASC) WITH (FILLFACTOR = 80)
);

