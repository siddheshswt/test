﻿CREATE TABLE [dbo].[PrescriptionExtended] (
    [PrescriptionExtendedId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PrescriptionId]         BIGINT           NULL,
    [ModifiedBy]             UNIQUEIDENTIFIER NULL,
    [RoleId]                 BIGINT           NULL,
    [Name]                   VARCHAR (100)    NULL,
    [MobileNumber]           VARCHAR (50)     NULL,
    [Email]                  NVARCHAR (100)   NULL,
    [Comments]               NVARCHAR (MAX)   NULL,
    [ModifiedDate]           DATETIME         NULL,
    CONSTRAINT [PK_PrescriptionExtended] PRIMARY KEY CLUSTERED ([PrescriptionExtendedId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PrescriptionExtended_Prescription] FOREIGN KEY ([PrescriptionId]) REFERENCES [dbo].[Prescription] ([PrescriptionId]),
    CONSTRAINT [FK_PrescriptionExtended_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([RoleId]),
    CONSTRAINT [FK_PrescriptionExtended_Users] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PrescriptionExtended_ModifiedBy_FK]
    ON [dbo].[PrescriptionExtended]([ModifiedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PrescriptionExtended_PrescriptionId_FK]
    ON [dbo].[PrescriptionExtended]([PrescriptionId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PrescriptionExtended_RoleId_FK]
    ON [dbo].[PrescriptionExtended]([RoleId] ASC);

