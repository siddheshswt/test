﻿CREATE TABLE [dbo].[ExceptionLog] (
    [ExceptionID]  BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserID]       UNIQUEIDENTIFIER NULL,
    [ErrorMessage] TEXT             NULL,
    [TimeStamp]    DATETIME         NOT NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED ([ExceptionID] ASC) WITH (FILLFACTOR = 80)
);

