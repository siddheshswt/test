﻿CREATE TABLE [dbo].[IDXUserRole] (
    [IDXUserRoleID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserID]        UNIQUEIDENTIFIER NOT NULL,
    [RoleID]        BIGINT           NOT NULL,
    [ModifiedBy]    UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]  DATETIME         NOT NULL,
    [FullControl]   BIT              DEFAULT ((0)) NULL,
    [IsActive]      BIT              DEFAULT ((0)) NULL,
    CONSTRAINT [PK_IDXUserRole] PRIMARY KEY NONCLUSTERED ([IDXUserRoleID] ASC),
    CONSTRAINT [FK_IDXUserRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleId]),
    CONSTRAINT [FK_IDXUserRole_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE CLUSTERED INDEX [IX_IDXUserRole]
    ON [dbo].[IDXUserRole]([RoleID] ASC, [UserID] ASC) WITH (FILLFACTOR = 80);

