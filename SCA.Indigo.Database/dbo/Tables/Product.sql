﻿CREATE TABLE [dbo].[Product] (
    [ProductId]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [DescriptionUI] NVARCHAR (100)   NULL,
    [SalesUnit]     NVARCHAR (50)    NULL,
    [SAPProductID]  NVARCHAR (18)    NULL,
    [BaseMaterial]  NVARCHAR (18)    NULL,
    [NetValue]      DECIMAL (18)     NULL,
    [Vat]           DECIMAL (18)     NULL,
    [PackSize]      NVARCHAR (5)     NULL,
    [ModifiedBy]    UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]  DATETIME         NOT NULL,
    [SortOrder]     BIGINT           DEFAULT ((100)) NOT NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ProductId] ASC) WITH (FILLFACTOR = 80)
);

