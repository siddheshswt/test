﻿CREATE TABLE [dbo].[IDXUserCarehome] (
    [IDXUserCarehomeId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [CarehomeId]        BIGINT           NOT NULL,
    [ModifiedBy]        UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]      DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([IDXUserCarehomeId] ASC)
);

