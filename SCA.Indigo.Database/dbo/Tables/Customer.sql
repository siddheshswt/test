﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]            BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonalInformationId] BIGINT           NULL,
    [SAPCustomerNumber]     NVARCHAR (50)    NULL,
    [SalesOrg]              NVARCHAR (50)    NULL,
    [DistrChanel]           NVARCHAR (50)    NULL,
    [Division]              NVARCHAR (50)    NULL,
    [Haulier]               NVARCHAR (50)    NULL,
    [ModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]          DATETIME         NOT NULL,
    [Transport_Company]     VARCHAR (MAX)    NULL,
    [Customer_Status]       BIGINT           NULL,
    [CustomerStartDate]     DATETIME         NULL,
    [CountryId]             INT              NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([CustomerId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Customer_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryId]),
    CONSTRAINT [FK_Customer_CustomerStatusList] FOREIGN KEY ([Customer_Status]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Customer_PersonalInformation] FOREIGN KEY ([PersonalInformationId]) REFERENCES [dbo].[PersonalInformation] ([PersonalInformationId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Customer_CountryId_FK]
    ON [dbo].[Customer]([CountryId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Customer_Customer_Status_FK]
    ON [dbo].[Customer]([Customer_Status] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Customer_PersonalInformationId_FK]
    ON [dbo].[Customer]([PersonalInformationId] ASC);

