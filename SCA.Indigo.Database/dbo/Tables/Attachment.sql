﻿CREATE TABLE [dbo].[Attachment] (
    [AttachmentId]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [AttachmentType]     BIGINT           NOT NULL,
    [TypeId]             BIGINT           NOT NULL,
    [AttachmentLocation] VARCHAR (500)    NOT NULL,
    [FileName]           VARCHAR (500)    NOT NULL,
    [CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME         NOT NULL,
    CONSTRAINT [PK_Attachment] PRIMARY KEY CLUSTERED ([AttachmentId] ASC) WITH (FILLFACTOR = 80),
    FOREIGN KEY ([AttachmentType]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Attachment_AttachmentType_FK]
    ON [dbo].[Attachment]([AttachmentType] ASC);

