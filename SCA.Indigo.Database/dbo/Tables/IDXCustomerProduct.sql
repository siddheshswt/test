﻿CREATE TABLE [dbo].[IDXCustomerProduct] (
    [IDXId]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [ProductId]          BIGINT           NULL,
    [CustomerId]         BIGINT           NULL,
    [ModifiedBy]         UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]       DATETIME         NOT NULL,
    [IsApporvalRequired] BIT              DEFAULT ((0)) NULL,
    [IsAvailable]        BIT              DEFAULT ((1)) NOT NULL,
    [IsDisplayForSample] BIT              DEFAULT ((0)) NOT NULL,
    [IsOverridden]       BIT              DEFAULT ((0)) NOT NULL,
    [Quantity]           BIGINT           NULL,
    [Frequency]          BIGINT           NULL,
    CONSTRAINT [PK_IDXCustomerProducts] PRIMARY KEY CLUSTERED ([IDXId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXCustomerProduct_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_IDXCustomerProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerProduct_CustomerId_FK]
    ON [dbo].[IDXCustomerProduct]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerProduct_ProductId_FK]
    ON [dbo].[IDXCustomerProduct]([ProductId] ASC);

