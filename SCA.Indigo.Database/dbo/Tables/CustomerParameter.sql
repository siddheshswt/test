﻿CREATE TABLE [dbo].[CustomerParameter] (
    [CustomerParameterID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerID]                    BIGINT           NOT NULL,
    [OrderCreationAllowed]          BIT              NOT NULL,
    [OrderApproval]                 BIT              NOT NULL,
    [OrderLeadTime]                 INT              CONSTRAINT [DF_CustomerParameter_OrderLeadTime] DEFAULT ((0)) NOT NULL,
    [OrderCutOffTime]               TIME (7)         NULL,
    [CheckAge]                      BIT              NOT NULL,
    [IsNHSIDMandatory]              BIT              NOT NULL,
    [IsADPMandatory]                BIT              NOT NULL,
    [IsTelephoneMandatory]          BIT              NOT NULL,
    [IsNextAssessmentDateMandatory] BIT              NOT NULL,
    [MaxPadPerDay]                  DECIMAL (18, 2)  NULL,
    [NextAssessmentDate]            INT              NULL,
    [AllowIncreaseOrderQty]         BIT              CONSTRAINT [DF_CustomerParameter_AllowIncreaseOrderQty] DEFAULT ((1)) NOT NULL,
    [IsCustomerApprovalReq]         BIT              CONSTRAINT [DF_CustomerParameter_IsCustomerApprovalReq] DEFAULT ((1)) NOT NULL,
    [Age]                           INT              NULL,
    [IsOrderAllowed]                BIT              NOT NULL,
    [IsNDDAllow]                    BIT              CONSTRAINT [DF_CustomerParameter_IsNDDAllow] DEFAULT ((1)) NOT NULL,
    [ModifiedBy]                    UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]                  DATETIME         NOT NULL,
    [Round]                         VARCHAR (16)     NULL,
    [IsCollectionApplicable]        BIT              CONSTRAINT [DF_CustomerParameter_IsCollectionApplication] DEFAULT ((0)) NOT NULL,
    [IsClinicalContactWithLinkage]  BIT              DEFAULT ((0)) NULL,
    [SampleOrderProductType]        VARCHAR (20)     NULL,
    CONSTRAINT [PK_CustomerParameter] PRIMARY KEY CLUSTERED ([CustomerParameterID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CustomerParameter_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerId]),
    UNIQUE NONCLUSTERED ([CustomerID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_CustomerParameter_CustomerID_FK]
    ON [dbo].[CustomerParameter]([CustomerID] ASC);

