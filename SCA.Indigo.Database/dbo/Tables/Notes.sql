﻿CREATE TABLE [dbo].[Notes] (
    [NoteId]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [NoteType]       BIGINT           NOT NULL,
    [NoteTypeId]     BIGINT           NOT NULL,
    [NoteText]       NVARCHAR (MAX)   NULL,
    [CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]    DATETIME         NOT NULL,
    [DisplayAsAlert] BIT              DEFAULT ((0)) NOT NULL,
    [ModifiedBy]     UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]   DATETIME         NOT NULL,
    CONSTRAINT [PK_Notes] PRIMARY KEY CLUSTERED ([NoteId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Notes_NoteType] FOREIGN KEY ([NoteType]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Notes_NoteType_FK]
    ON [dbo].[Notes]([NoteType] ASC);

