﻿CREATE TABLE [dbo].[IDXPatientTypeMatrix] (
    [IDXPatientTypeMatrixId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PatientTypeMatrixId]    BIGINT           NOT NULL,
    [PatientTypeId]          BIGINT           NOT NULL,
    [ModifiedBy]             UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]           DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXPatientTypeMatrix] PRIMARY KEY CLUSTERED ([IDXPatientTypeMatrixId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXPatientTypeMatrix_List] FOREIGN KEY ([PatientTypeId]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_IDXPatientTypeMatrix_PatientTypeMatrix] FOREIGN KEY ([PatientTypeMatrixId]) REFERENCES [dbo].[PatientTypeMatrix] ([PatientTypeMatrixID])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientTypeMatrix_PatientTypeId_FK]
    ON [dbo].[IDXPatientTypeMatrix]([PatientTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientTypeMatrix_PatientTypeMatrixId_FK]
    ON [dbo].[IDXPatientTypeMatrix]([PatientTypeMatrixId] ASC);

