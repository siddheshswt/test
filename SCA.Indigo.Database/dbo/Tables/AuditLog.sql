﻿CREATE TABLE [dbo].[AuditLog] (
    [AuditLogId]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [TableName]      NVARCHAR (64)    NOT NULL,
    [AuditTransId]   BIGINT           NOT NULL,
    [PrimaryKey]     VARCHAR (50)     NULL,
    [PrimaryKeyType] NVARCHAR (64)    NOT NULL,
    [ColumnName]     NVARCHAR (64)    NOT NULL,
    [OldValue]       NVARCHAR (MAX)   NULL,
    [NewValue]       NVARCHAR (MAX)   NOT NULL,
    [Action]         VARCHAR (20)     NOT NULL,
    [ModifiedBy]     UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]   DATETIME         NOT NULL,
    [Remarks]        NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_AuditLogs] PRIMARY KEY CLUSTERED ([AuditLogId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_AuditLog_AuditLogTransaction] FOREIGN KEY ([AuditTransId]) REFERENCES [dbo].[AuditLogTransaction] ([AuditTransID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AuditLog_AuditTransId_FK]
    ON [dbo].[AuditLog]([AuditTransId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AuditLog_ColumnName_ModifiedDate]
    ON [dbo].[AuditLog]([ColumnName] ASC, [ModifiedDate] ASC)
    INCLUDE([AuditTransId], [ModifiedBy]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_AuditLog_TableName]
    ON [dbo].[AuditLog]([TableName] ASC)
    INCLUDE([AuditTransId], [PrimaryKey]) WITH (FILLFACTOR = 80);

