﻿CREATE TABLE [dbo].[ReportBuilderFormat] (
    [ReportBuilderId]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [ReportFormatName]        VARCHAR (1000)   NOT NULL,
    [CustomerColumn]          VARCHAR (MAX)    NULL,
    [PatientTypeMatrixColumn] VARCHAR (MAX)    NULL,
    [CustomerParameterColumn] VARCHAR (MAX)    NULL,
    [PatientColumn]           VARCHAR (MAX)    NULL,
    [CareHomeColumn]          VARCHAR (MAX)    NULL,
    [UserMaintainceColumn]    VARCHAR (MAX)    NULL,
    [ClinicalContactsColumn]  VARCHAR (MAX)    NULL,
    [PrescriptionColumn]      VARCHAR (MAX)    NULL,
    [CreatedBy]               UNIQUEIDENTIFIER NULL,
    [CreatedDate]             DATETIME         NULL,
    [ModifiedBy]              UNIQUEIDENTIFIER NULL,
    [ModifiedDate]            DATETIME         NULL,
    [OrderColumn]             VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_ReportBuilderFormat] PRIMARY KEY CLUSTERED ([ReportBuilderId] ASC) WITH (FILLFACTOR = 80)
);

