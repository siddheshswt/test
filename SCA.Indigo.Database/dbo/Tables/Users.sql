﻿CREATE TABLE [dbo].[Users] (
    [UserId]           UNIQUEIDENTIFIER NOT NULL,
    [PersonId]         BIGINT           NULL,
    [SecurityLevel]    BIGINT           NULL,
    [ValidFromDate]    DATETIME         NULL,
    [ValidToDate]      DATETIME         NULL,
    [UserName]         NVARCHAR (50)    NOT NULL,
    [Password]         NVARCHAR (MAX)   NULL,
    [IsAccountLocked]  BIT              NULL,
    [NoOfAttempts]     BIGINT           NULL,
    [IsNewUser]        BIT              CONSTRAINT [DF_User_IsNewUser] DEFAULT ((1)) NULL,
    [AuthorizationPIN] INT              NULL,
    [ModifiedBy]       UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]     DATETIME         NOT NULL,
    [GroupEmailId]     NVARCHAR (100)   NULL,
    [FirstName]        NVARCHAR (200)   NULL,
    [LastName]         NVARCHAR (200)   NULL,
    [Email]            NVARCHAR (100)   NULL,
    [MobileNumber]     NVARCHAR (40)    NULL,
    [IsCarehomeUser]   BIT              DEFAULT ((0)) NOT NULL,
    [IsActive]         BIT              DEFAULT ((1)) NOT NULL,
    [IsInternalUser]   BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_users_Personalinformation] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[PersonalInformation] ([PersonalInformationId]),
    CONSTRAINT [UK_User] UNIQUE NONCLUSTERED ([UserName] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Users_PersonId_FK]
    ON [dbo].[Users]([PersonId] ASC);

