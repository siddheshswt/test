﻿CREATE TABLE [dbo].[List] (
    [ListId]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [ListTypeId]   BIGINT           NOT NULL,
    [ListIndex]    INT              NOT NULL,
    [DefaultText]  NVARCHAR (32)    NOT NULL,
    [Description]  NVARCHAR (MAX)   NULL,
    [TransProxyId] BIGINT           NOT NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_Lists] PRIMARY KEY CLUSTERED ([ListId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_List_ListType] FOREIGN KEY ([ListTypeId]) REFERENCES [dbo].[ListType] ([ListTypeId]),
    CONSTRAINT [FK_List_TranslationProxy] FOREIGN KEY ([TransProxyId]) REFERENCES [dbo].[TranslationProxy] ([TransProxyID])
);


GO
CREATE NONCLUSTERED INDEX [IX_List_ListTypeId_FK]
    ON [dbo].[List]([ListTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_List_TransProxyId_FK]
    ON [dbo].[List]([TransProxyId] ASC);

