﻿CREATE TABLE [dbo].[ContactPerson] (
    [ContactPersonId]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonalInformationId] BIGINT           NULL,
    [ModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]          DATETIME         NOT NULL,
    [PatientId]             BIGINT           NULL,
    [CarehomeId]            BIGINT           NULL,
    [CustomerId]            BIGINT           NULL,
    [Remarks]               NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ContactPersons] PRIMARY KEY CLUSTERED ([ContactPersonId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ContactPerson_Carehome] FOREIGN KEY ([CarehomeId]) REFERENCES [dbo].[CareHome] ([CareHomeId]),
    CONSTRAINT [FK_ContactPerson_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_ContactPerson_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId]),
    CONSTRAINT [FK_ContactPerson_PersonalInformation] FOREIGN KEY ([PersonalInformationId]) REFERENCES [dbo].[PersonalInformation] ([PersonalInformationId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ContactPerson_CarehomeId_FK]
    ON [dbo].[ContactPerson]([CarehomeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ContactPerson_CustomerId_FK]
    ON [dbo].[ContactPerson]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ContactPerson_PatientId_FK]
    ON [dbo].[ContactPerson]([PatientId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ContactPerson_PersonalInformationId_FK]
    ON [dbo].[ContactPerson]([PersonalInformationId] ASC);

