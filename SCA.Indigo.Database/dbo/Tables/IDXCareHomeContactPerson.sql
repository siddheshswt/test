﻿CREATE TABLE [dbo].[IDXCareHomeContactPerson] (
    [IDXContactPersonId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CareHomeId]         BIGINT           NULL,
    [ContactPersonId]    BIGINT           NULL,
    [ModifiedBy]         UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]       DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXCareHomeContactPersons] PRIMARY KEY CLUSTERED ([IDXContactPersonId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXCareHomeContactPerson_CareHome] FOREIGN KEY ([CareHomeId]) REFERENCES [dbo].[CareHome] ([CareHomeId]),
    CONSTRAINT [FK_IDXCareHomeContactPerson_ContactPerson] FOREIGN KEY ([ContactPersonId]) REFERENCES [dbo].[ContactPerson] ([ContactPersonId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCareHomeContactPerson_CareHomeId_FK]
    ON [dbo].[IDXCareHomeContactPerson]([CareHomeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCareHomeContactPerson_ContactPersonId_FK]
    ON [dbo].[IDXCareHomeContactPerson]([ContactPersonId] ASC);

