﻿CREATE TABLE [dbo].[PostcodeMatrix] (
    [PostcodeMatrixID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [RoundID]          VARCHAR (50)     NOT NULL,
    [Monday]           BIT              NULL,
    [Tuesday]          BIT              NULL,
    [Wednesday]        BIT              NULL,
    [Thursday]         BIT              NULL,
    [Friday]           BIT              NULL,
    [CustomerID]       BIGINT           NULL,
    [Postcode]         VARCHAR (20)     NULL,
    [Round]            VARCHAR (20)     NULL,
    [IsActive]         BIT              DEFAULT ((1)) NOT NULL,
    [ModifiedBy]       UNIQUEIDENTIFIER DEFAULT ('ABE3FF99-CB70-48A6-BCC1-4D4C9B80A153') NOT NULL,
    [ModifiedDate]     DATETIME         DEFAULT (getdate()) NOT NULL,
    [Remarks]          NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_PostcodeMatrix] PRIMARY KEY CLUSTERED ([PostcodeMatrixID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PostCodeMatrix_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_PostCodeMatrix_User] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[Users] ([UserId]),
    CONSTRAINT [Unique_PostcodeMatrix] UNIQUE NONCLUSTERED ([Round] ASC, [RoundID] ASC, [Postcode] ASC, [CustomerID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_PostcodeMatrix_CustomerID_FK]
    ON [dbo].[PostcodeMatrix]([CustomerID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PostcodeMatrix_ModifiedBy_FK]
    ON [dbo].[PostcodeMatrix]([ModifiedBy] ASC);

