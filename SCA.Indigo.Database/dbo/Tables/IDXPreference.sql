﻿CREATE TABLE [dbo].[IDXPreference] (
    [IDXPreferenceId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PreferenceType]  BIGINT           NULL,
    [PatientId]       BIGINT           NULL,
    [Preference]      BIGINT           NULL,
    [ModifiedBy]      UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]    DATETIME         NOT NULL,
    [CarehomeId]      BIGINT           NULL,
    [Remarks]         NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_IDXPreferences_1] PRIMARY KEY CLUSTERED ([IDXPreferenceId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXPreference_Carehome] FOREIGN KEY ([CarehomeId]) REFERENCES [dbo].[CareHome] ([CareHomeId]),
    CONSTRAINT [FK_IDXPreference_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId]),
    CONSTRAINT [FK_IDXPreference_Preference] FOREIGN KEY ([Preference]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPreference_CarehomeId_FK]
    ON [dbo].[IDXPreference]([CarehomeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPreference_PatientId_FK]
    ON [dbo].[IDXPreference]([PatientId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPreference_Preference_FK]
    ON [dbo].[IDXPreference]([Preference] ASC);

