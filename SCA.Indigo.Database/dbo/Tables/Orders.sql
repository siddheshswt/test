﻿CREATE TABLE [dbo].[Orders] (
    [OrderId]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [OrderType]            BIGINT           NULL,
    [SAPOrderNo]           NVARCHAR (50)    NULL,
    [IsRushFlag]           BIT              NULL,
    [OrderStatus]          BIGINT           NULL,
    [OrderDate]            DATETIME         NULL,
    [SAPOrderDeliveryDate] DATETIME         NULL,
    [ExcludeServiceCharge] BIT              CONSTRAINT [DF_Orders_ExcludeServiceCharge] DEFAULT ((0)) NOT NULL,
    [ModifiedBy]           UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]         DATETIME         NOT NULL,
    [SAPMessage]           VARCHAR (500)    NULL,
    [HelixOrderNo]         NVARCHAR (100)   NULL,
    [PatientId]            BIGINT           NULL,
    [CareHomeId]           BIGINT           NULL,
    [SamplePatientId]      BIGINT           NULL,
    [DisclaimerId]         BIGINT           NULL,
    [DerivedNDD]           DATETIME         NULL,
    [EmailId]              VARCHAR (100)    NULL,
    [OrderCreationType]    BIGINT           NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([OrderId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_OrderList_OrderStatus] FOREIGN KEY ([OrderStatus]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_OrderList_OrderType] FOREIGN KEY ([OrderType]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Orders_CareHome] FOREIGN KEY ([CareHomeId]) REFERENCES [dbo].[CareHome] ([CareHomeId]),
    CONSTRAINT [FK_Orders_List_OrderCreationType] FOREIGN KEY ([OrderCreationType]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_Orders_OrderDisclaimer] FOREIGN KEY ([DisclaimerId]) REFERENCES [dbo].[OrderDisclaimer] ([OrderDisclaimerId]),
    CONSTRAINT [FK_Orders_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId])
);


GO
CREATE NONCLUSTERED INDEX [ix_Order]
    ON [dbo].[Orders]([PatientId] ASC, [OrderStatus] ASC, [IsRushFlag] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_CareHomeId]
    ON [dbo].[Orders]([CareHomeId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_CareHomeId_FK]
    ON [dbo].[Orders]([CareHomeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_DisclaimerId_FK]
    ON [dbo].[Orders]([DisclaimerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_OrderCreationType_FK]
    ON [dbo].[Orders]([OrderCreationType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_OrderStatus_FK]
    ON [dbo].[Orders]([OrderStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_OrderType_FK]
    ON [dbo].[Orders]([OrderType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_PatientId_FK]
    ON [dbo].[Orders]([PatientId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_PatientID_IsRushFlag]
    ON [dbo].[Orders]([PatientId] ASC, [IsRushFlag] ASC)
    INCLUDE([CareHomeId]) WHERE ([OrderStatus] IN ((10047), (10098))) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Orders_OIPO]
    ON [dbo].[Orders]([OrderStatus] ASC, [IsRushFlag] ASC, [PatientId] ASC, [OrderId] ASC);

