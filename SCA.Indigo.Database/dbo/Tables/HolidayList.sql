﻿CREATE TABLE [dbo].[HolidayList] (
    [HolidayID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [CountryID]        INT              NOT NULL,
    [HolidayDate]      DATETIME         NOT NULL,
    [Description]      VARCHAR (50)     NULL,
    [CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]      DATETIME         NOT NULL,
    [ModifiedBy]       UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDatetime] DATETIME         NOT NULL,
    CONSTRAINT [PK_HolidayList] PRIMARY KEY CLUSTERED ([HolidayID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_HolidayList_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryId])
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidayList_CountryID_FK]
    ON [dbo].[HolidayList]([CountryID] ASC);

