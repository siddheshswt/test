﻿CREATE TABLE [dbo].[ListType] (
    [ListTypeId]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Text]         NVARCHAR (MAX)   NULL,
    [Description]  NVARCHAR (MAX)   NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_ListTypes] PRIMARY KEY CLUSTERED ([ListTypeId] ASC) WITH (FILLFACTOR = 80)
);

