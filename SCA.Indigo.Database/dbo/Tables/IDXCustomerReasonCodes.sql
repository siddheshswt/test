﻿CREATE TABLE [dbo].[IDXCustomerReasonCodes] (
    [IDXCustomerReasonCodeId] INT              IDENTITY (1, 1) NOT NULL,
    [CustomerID]              BIGINT           NOT NULL,
    [ListID]                  BIGINT           NOT NULL,
    [IsRemoved]               BIT              DEFAULT ((0)) NOT NULL,
    [ModifiedBy]              UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]            DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXCustomerReasonCodes] PRIMARY KEY CLUSTERED ([IDXCustomerReasonCodeId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXCustomerReasonCodes_customerId] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_IDXCustomerReasonCodes_ListId] FOREIGN KEY ([ListID]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerReasonCodes_CustomerID_FK]
    ON [dbo].[IDXCustomerReasonCodes]([CustomerID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerReasonCodes_ListID_FK]
    ON [dbo].[IDXCustomerReasonCodes]([ListID] ASC);

