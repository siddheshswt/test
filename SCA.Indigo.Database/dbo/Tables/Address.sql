﻿CREATE TABLE [dbo].[Address] (
    [AddressId]    BIGINT           IDENTITY (1, 1) NOT NULL,
    [HouseNumber]  NVARCHAR (64)    NULL,
    [AddressLine1] NVARCHAR (80)    NULL,
    [AddressLine2] NVARCHAR (80)    NULL,
    [City]         NVARCHAR (64)    NULL,
    [County]       VARCHAR (100)    NULL,
    [Country]      NVARCHAR (32)    NULL,
    [PostCode]     NVARCHAR (10)    NULL,
    [HouseName]    NVARCHAR (100)   NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED ([AddressId] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [ix_Address]
    ON [dbo].[Address]([AddressId] ASC, [PostCode] ASC, [AddressLine1] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Address_Address_Search]
    ON [dbo].[Address]([AddressId] ASC)
    INCLUDE([HouseNumber], [HouseName], [AddressLine1], [AddressLine2], [City], [County], [Country], [PostCode]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [ix_Address_AddressLine1]
    ON [dbo].[Address]([AddressLine1] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Address_PostCode]
    ON [dbo].[Address]([PostCode] ASC)
    INCLUDE([AddressId]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Address_AddressId_Inc]
    ON [dbo].[Address]([AddressId] ASC)
    INCLUDE([PostCode]);


GO
-- =============================================
-- Author:		Saurabh Mayekar
-- Create date: 2016-06-22
-- Description:	Update PostCode
-- =============================================
-- ===============================================================================

--		Modified Date			Modified By			Purpose
--	1.	
--	

-- ===============================================================================

CREATE TRIGGER [dbo].[tgrUpdatePostCode]
   ON  [dbo].[Address]
   AFTER INSERT, UPDATE

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION;
		BEGIN
			IF UPDATE(PostCode)
			BEGIN

                   UPDATE Address
				   SET    PostCode = UPPER(addr.PostCode)
				   FROM   inserted ins
				   INNER JOIN Address addr
				   ON     ins.AddressId = addr.AddressId
 
			END			
		END		
		COMMIT TRANSACTION;
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;
	END CATCH;

END
