﻿CREATE TABLE [dbo].[IDXPatientContactPerson] (
    [PateintContactPersonId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PatientId]              BIGINT           NULL,
    [ContactPersonId]        BIGINT           NULL,
    [ModifiedBy]             UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]           DATETIME         NOT NULL,
    CONSTRAINT [PK_IDXPatientContactPersons] PRIMARY KEY CLUSTERED ([PateintContactPersonId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXPatientContactPerson_ContactPerson] FOREIGN KEY ([ContactPersonId]) REFERENCES [dbo].[ContactPerson] ([ContactPersonId]),
    CONSTRAINT [FK_IDXPatientContactPerson_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientContactPerson_ContactPersonId_FK]
    ON [dbo].[IDXPatientContactPerson]([ContactPersonId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPatientContactPerson_PatientId_FK]
    ON [dbo].[IDXPatientContactPerson]([PatientId] ASC);

