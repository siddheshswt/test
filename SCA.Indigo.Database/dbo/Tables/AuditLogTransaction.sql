﻿CREATE TABLE [dbo].[AuditLogTransaction] (
    [AuditTransID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_AuditLogTransaction] PRIMARY KEY CLUSTERED ([AuditTransID] ASC) WITH (FILLFACTOR = 80)
);

