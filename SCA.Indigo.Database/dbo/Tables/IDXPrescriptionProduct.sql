﻿CREATE TABLE [dbo].[IDXPrescriptionProduct] (
    [IDXPrescriptionProductId]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [PrescriptionId]                        BIGINT           NULL,
    [ProductId]                             BIGINT           NULL,
    [AssessedPadsPerDay]                    BIGINT           NULL,
    [IsOverridenFlag]                       BIT              NOT NULL,
    [ActualPadsPerDay]                      BIGINT           NULL,
    [Del1]                                  BIGINT           NULL,
    [Del2]                                  BIGINT           NULL,
    [Del3]                                  BIGINT           NULL,
    [Del4]                                  BIGINT           NULL,
    [Frequency]                             BIGINT           NULL,
    [Status]                                NVARCHAR (16)    NULL,
    [ValidFromDate]                         DATETIME         NULL,
    [ValidToDate]                           DATETIME         NULL,
    [FlagDQ1]                               BIT              NULL,
    [FlagDQ2]                               BIT              NULL,
    [FlagDQ3]                               BIT              NULL,
    [FlagDQ4]                               BIT              NULL,
    [IsRemoved]                             BIT              NULL,
    [Description]                           TEXT             NULL,
    [DeliveryDate]                          DATETIME         NULL,
    [PrescriptionAuthorizationApprovalFlag] BIT              NULL,
    [ApprovedOn]                            DATETIME         NULL,
    [ActPPD]                                FLOAT (53)       NULL,
    [NextDeliveryDate]                      DATETIME         NULL,
    [ModifiedBy]                            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]                          DATETIME         NOT NULL,
    [ProductApprovalFlag]                   BIT              DEFAULT ((0)) NOT NULL,
    [Remarks]                               NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_IDXPrescriptionProducts1] PRIMARY KEY CLUSTERED ([IDXPrescriptionProductId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXPrescriptionProduct_Prescription] FOREIGN KEY ([PrescriptionId]) REFERENCES [dbo].[Prescription] ([PrescriptionId]),
    CONSTRAINT [FK_IDXPrescriptionProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_IsRemoved]
    ON [dbo].[IDXPrescriptionProduct]([IsRemoved] ASC)
    INCLUDE([PrescriptionId], [ProductId], [AssessedPadsPerDay], [Frequency]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_IsRemoved_ProductId]
    ON [dbo].[IDXPrescriptionProduct]([IsRemoved] ASC, [ProductId] ASC)
    INCLUDE([PrescriptionId], [Frequency]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_FK]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_IsRemoved]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC, [IsRemoved] ASC)
    INCLUDE([ProductId], [AssessedPadsPerDay], [Frequency]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_IsRemoved_Search]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC, [IsRemoved] ASC)
    INCLUDE([ProductId], [AssessedPadsPerDay], [Frequency], [Status], [DeliveryDate], [ActPPD], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_IsRemoved_Search1]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC, [IsRemoved] ASC)
    INCLUDE([IDXPrescriptionProductId], [ProductId], [AssessedPadsPerDay], [IsOverridenFlag], [Del1], [Del2], [Del3], [Del4], [Frequency], [Status], [ValidFromDate], [ValidToDate], [FlagDQ1], [FlagDQ2], [FlagDQ3], [FlagDQ4], [DeliveryDate], [ActPPD], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_ProductId]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC, [ProductId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_ProductId_Status]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC, [ProductId] ASC, [Status] ASC)
    INCLUDE([IDXPrescriptionProductId], [Frequency]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_PrescriptionId_Status]
    ON [dbo].[IDXPrescriptionProduct]([PrescriptionId] ASC, [Status] ASC)
    INCLUDE([ProductId], [Del1], [Del2], [Del3], [Del4], [Frequency], [FlagDQ1], [FlagDQ2], [FlagDQ3], [FlagDQ4], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_ProductId_FK]
    ON [dbo].[IDXPrescriptionProduct]([ProductId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_Status]
    ON [dbo].[IDXPrescriptionProduct]([Status] ASC)
    INCLUDE([PrescriptionId], [ProductId], [Del1], [Del2], [Del3], [Del4], [Frequency], [FlagDQ1], [FlagDQ2], [FlagDQ3], [FlagDQ4], [NextDeliveryDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_IDXPrescriptionProduct_Status_Search]
    ON [dbo].[IDXPrescriptionProduct]([Status] ASC)
    INCLUDE([IDXPrescriptionProductId], [PrescriptionId], [ProductId], [Frequency]) WITH (FILLFACTOR = 80);

