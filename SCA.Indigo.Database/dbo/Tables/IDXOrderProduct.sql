﻿CREATE TABLE [dbo].[IDXOrderProduct] (
    [OrderProductId]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [OrderId]               BIGINT           NULL,
    [ProductId]             BIGINT           NULL,
    [Quantity]              BIGINT           NOT NULL,
    [IsFOC]                 BIT              CONSTRAINT [DF_IDXOrderProduct_IsFOC] DEFAULT ((0)) NOT NULL,
    [ModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]          DATETIME         NOT NULL,
    [IsRemoved]             BIT              CONSTRAINT [DF_IDXOrderProduct_IsRemoved] DEFAULT ((0)) NOT NULL,
    [ProductDescription]    VARCHAR (100)    NULL,
    [SampleProductId]       BIGINT           NULL,
    [Remarks]               NVARCHAR (MAX)   NULL,
    [Plant]                 VARCHAR (4)      NULL,
    [BillingDate]           DATETIME         NULL,
    [Currency]              VARCHAR (5)      NULL,
    [BillingQuantity]       DECIMAL (18, 3)  NULL,
    [BillingUnit]           VARCHAR (3)      NULL,
    [NetValue]              DECIMAL (18, 2)  NULL,
    [VatValue]              DECIMAL (18, 2)  NULL,
    [PODDate]               DATETIME         NULL,
    [DeliveryQuantity]      DECIMAL (18, 3)  NULL,
    [SAPDeliveryNo]         NVARCHAR (100)   NULL,
    [InvoiceNo]             NVARCHAR (50)    NULL,
    [SAPActualDeliveryDate] DATETIME         NULL,
    [DeliveryUnit]          VARCHAR (3)      NULL,
    [OrderItem]             VARCHAR (6)      NULL,
    CONSTRAINT [PK_IDXOrderProducts] PRIMARY KEY CLUSTERED ([OrderProductId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXOrderProduct_Order] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([OrderId]),
    CONSTRAINT [FK_IDXOrderProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXOrderProduct_OrderId_FK]
    ON [dbo].[IDXOrderProduct]([OrderId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXOrderProduct_ProductId_FK]
    ON [dbo].[IDXOrderProduct]([ProductId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXOrderProduct_SampleProductId_FK]
    ON [dbo].[IDXOrderProduct]([SampleProductId] ASC);

