﻿CREATE TABLE [dbo].[OrderDisclaimer] (
    [OrderDisclaimerId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [Name]              VARCHAR (32)     NOT NULL,
    [Position]          VARCHAR (32)     NOT NULL,
    [ModifiedBy]        UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]      DATETIME         NOT NULL,
    CONSTRAINT [PK_OrderDisclaimer] PRIMARY KEY CLUSTERED ([OrderDisclaimerId] ASC) WITH (FILLFACTOR = 80)
);

