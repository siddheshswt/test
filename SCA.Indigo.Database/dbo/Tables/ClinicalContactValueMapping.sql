﻿CREATE TABLE [dbo].[ClinicalContactValueMapping] (
    [ClinicalContactValueMappingId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ClinicalContactRoleMappingId]  BIGINT           NULL,
    [ChildValueId]                  BIGINT           NULL,
    [ParentValueId]                 BIGINT           NULL,
    [ChildListId]                   BIGINT           NULL,
    [IsRemoved]                     BIT              NULL,
    [ModifiedBy]                    UNIQUEIDENTIFIER NULL,
    [ModifiedDate]                  DATETIME         NULL,
    CONSTRAINT [PK_ClinicalContactValueMapping] PRIMARY KEY CLUSTERED ([ClinicalContactValueMappingId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ClinicalContactValueMapping_ClinicalContactRoleMapping] FOREIGN KEY ([ClinicalContactRoleMappingId]) REFERENCES [dbo].[ClinicalContactRoleMapping] ([ClinicalContactRoleMappingId]),
    CONSTRAINT [FK_ClinicalContactValueMapping_ClinicalValueMaster_Child] FOREIGN KEY ([ChildValueId]) REFERENCES [dbo].[ClinicalValueMaster] ([ClinicalValueMasterId]),
    CONSTRAINT [FK_ClinicalContactValueMapping_ClinicalValueMaster_Parent] FOREIGN KEY ([ParentValueId]) REFERENCES [dbo].[ClinicalValueMaster] ([ClinicalValueMasterId]),
    CONSTRAINT [FK_ClinicalContactValueMapping_Users] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactValueMapping_ChildValueId_FK]
    ON [dbo].[ClinicalContactValueMapping]([ChildValueId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactValueMapping_ClinicalContactRoleMappingId_FK]
    ON [dbo].[ClinicalContactValueMapping]([ClinicalContactRoleMappingId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactValueMapping_ModifiedBy_FK]
    ON [dbo].[ClinicalContactValueMapping]([ModifiedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactValueMapping_ParentValueId_FK]
    ON [dbo].[ClinicalContactValueMapping]([ParentValueId] ASC);

