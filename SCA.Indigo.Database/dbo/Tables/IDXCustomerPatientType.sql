﻿CREATE TABLE [dbo].[IDXCustomerPatientType] (
    [IDXCustomerPatientTypeID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerID]               BIGINT           NULL,
    [PatientTypeID]            BIGINT           NULL,
    [ModifiedBy]               UNIQUEIDENTIFIER NULL,
    [ModifiedDate]             DATETIME         NULL,
    [IsRemove]                 BIT              DEFAULT ((0)) NULL,
    CONSTRAINT [PK_IDXCustomerPatientType] PRIMARY KEY CLUSTERED ([IDXCustomerPatientTypeID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXCustomerPatientType_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_IDXCustomerPatientType_List] FOREIGN KEY ([PatientTypeID]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerPatientType_CustomerID_FK]
    ON [dbo].[IDXCustomerPatientType]([CustomerID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerPatientType_PatientTypeID_FK]
    ON [dbo].[IDXCustomerPatientType]([PatientTypeID] ASC);

