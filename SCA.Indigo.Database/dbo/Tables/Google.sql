﻿CREATE TABLE [dbo].[Google](
	[GoogleId] [bigint] IDENTITY(1,1) NOT NULL,
	[IndigoId] [bigint] NOT NULL,
	[ObjectType] [int] NOT NULL,
	[Fields] [varchar](max) NULL,
	[CustomerId] [bigint] NOT NULL,
	[Status] [bigint] NOT NULL,
	[PatientType] [nvarchar](64) NULL,
	[LastName] [nvarchar](400) NULL,
	[CareHomeName] [nvarchar](400) NULL,
	[Postcode] [nvarchar](20) NULL,
	[DateofBirth] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[PatientTypeID] [bigint] NULL,
	[CareHomeId] [bigint] NULL,
 CONSTRAINT [PK] PRIMARY KEY CLUSTERED 
(
	[GoogleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_KEY] UNIQUE NONCLUSTERED 
(
	[IndigoId] ASC,
	[ObjectType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Google]  WITH CHECK ADD  CONSTRAINT [CHECK_Val] CHECK  (([ObjectType]=(10115) OR [ObjectType]=(10114) OR [ObjectType]=(10113)))
GO

ALTER TABLE [dbo].[Google] CHECK CONSTRAINT [CHECK_Val]
GO

