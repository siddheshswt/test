﻿CREATE TABLE [dbo].[ClinicalValueMaster] (
    [ClinicalValueMasterId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ContactName]           NVARCHAR (MAX)   NULL,
    [ModifiedBy]            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]          DATETIME         NOT NULL,
    [IsRemoved]             BIT              DEFAULT ((0)) NULL,
    CONSTRAINT [PK_ClinicalValueMaster] PRIMARY KEY CLUSTERED ([ClinicalValueMasterId] ASC) WITH (FILLFACTOR = 80)
);

