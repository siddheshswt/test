﻿CREATE TABLE [dbo].[UserActionLog] (
    [LogID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserID]       UNIQUEIDENTIFIER NOT NULL,
    [Priority]     INT              NULL,
    [Action]       TEXT             NULL,
    [Controller]   NVARCHAR (50)    NULL,
    [TimeStamp]    DATETIME         NULL,
    [MachineName]  NVARCHAR (50)    NULL,
    [ProcessID]    NVARCHAR (50)    NULL,
    [Message]      TEXT             NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_UserActionLog] PRIMARY KEY CLUSTERED ([LogID] ASC) WITH (FILLFACTOR = 80)
);

