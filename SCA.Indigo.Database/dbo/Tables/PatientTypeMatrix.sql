﻿CREATE TABLE [dbo].[PatientTypeMatrix] (
    [PatientTypeMatrixID]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [PatientType]                        BIGINT           NULL,
    [Presc_Min_Freq]                     BIGINT           NULL,
    [Presc_Max_Freq]                     BIGINT           NULL,
    [IsAddFromPrescriptionAllowed]       BIT              CONSTRAINT [DF_PatientTypeMatrix_IsAddFromPrescriptionAllowed] DEFAULT ((1)) NOT NULL,
    [IsAddFromProductAllowed]            BIT              CONSTRAINT [DF_PatientTypeMatrix_IsAddFromProductAllowed] DEFAULT ((1)) NOT NULL,
    [AdvanceOrderActivationDays]         BIGINT           NULL,
    [ChangeOrderNddDays]                 BIGINT           NULL,
    [IsOneOffAddFromPrescriptionAllowed] BIT              CONSTRAINT [DF_PatientTypeMatrix_IsOneOffAddFromPrescriptionAllowed] DEFAULT ((1)) NOT NULL,
    [IsOneOffAddFromProductAllowed]      BIT              CONSTRAINT [DF_PatientTypeMatrix_IsOneOffAddFromProductAllowed] DEFAULT ((1)) NOT NULL,
    [IsPrescriptionApprovalRequired]     BIT              CONSTRAINT [DF_PatientTypeMatrix_IsPrescriptionApprovalRequired] DEFAULT ((0)) NULL,
    [AdultBillTo]                        BIGINT           NULL,
    [PaedBillTo]                         BIGINT           NULL,
    [CreatedBy]                          UNIQUEIDENTIFIER NULL,
    [CreatedDate]                        DATETIME         NULL,
    [ModifiedBy]                         UNIQUEIDENTIFIER NULL,
    [ModifiedDate]                       DATETIME         NULL,
    [IncrementFrequency]                 BIGINT           NULL,
    [CustomerId]                         BIGINT           NULL,
    [IsRemove]                           BIT              DEFAULT ((0)) NULL,
    [DefaultFrequency]                   BIGINT           NULL,
    CONSTRAINT [PK_PatientTypeMatrix] PRIMARY KEY CLUSTERED ([PatientTypeMatrixID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PatientTypeMatrix_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_PatientTypeMatrix_List] FOREIGN KEY ([PatientType]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PatientTypeMatrix_CustomerId_FK]
    ON [dbo].[PatientTypeMatrix]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PatientTypeMatrix_PatientType_FK]
    ON [dbo].[PatientTypeMatrix]([PatientType] ASC);

