﻿CREATE TABLE [dbo].[SamplePatient] (
    [SamplePatientId]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonalInformationId] BIGINT           NULL,
    [CreatedBy]             UNIQUEIDENTIFIER NULL,
    [CreatedDate]           DATETIME         NULL,
    [ModifiedBy]            UNIQUEIDENTIFIER NULL,
    [ModifiedDate]          DATETIME         NULL,
    [PatientType]           BIGINT           NULL,
    CONSTRAINT [PK_SamplePatient] PRIMARY KEY CLUSTERED ([SamplePatientId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SamplePatient_List] FOREIGN KEY ([PatientType]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_SamplePatient_PersonalInformationId] FOREIGN KEY ([PersonalInformationId]) REFERENCES [dbo].[PersonalInformation] ([PersonalInformationId])
);


GO
CREATE NONCLUSTERED INDEX [IX_SamplePatient_PatientType_FK]
    ON [dbo].[SamplePatient]([PatientType] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SamplePatient_PersonalInformationId_FK]
    ON [dbo].[SamplePatient]([PersonalInformationId] ASC);

