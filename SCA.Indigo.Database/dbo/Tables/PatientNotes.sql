﻿CREATE TABLE [dbo].[PatientNotes] (
    [PatientNoteId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PatientId]     BIGINT           NULL,
    [Note]          NVARCHAR (MAX)   NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]   DATETIME         NOT NULL,
    CONSTRAINT [PK_PatientNotes] PRIMARY KEY CLUSTERED ([PatientNoteId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PatientNotes_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PatientNotes_PatientId_FK]
    ON [dbo].[PatientNotes]([PatientId] ASC);

