﻿CREATE TABLE [dbo].[LoginUserDetails] (
    [LoginUserDetailId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [LoginDate]         DATETIME         CONSTRAINT [DF_LoginUserDetails_LoginDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LoginUserDetails] PRIMARY KEY CLUSTERED ([LoginUserDetailId] ASC),
    CONSTRAINT [FK_LoginUserDetails_LoginUserDetails] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
);

