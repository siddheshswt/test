﻿CREATE TABLE [dbo].[ClinicalContactAnalysisMaster] (
    [ClinicalContactAnalysisMasterId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [TypeId]                          BIGINT           NOT NULL,
    [Index]                           BIGINT           NOT NULL,
    [ContactName]                     NVARCHAR (100)   NULL,
    [TransProxyId]                    BIGINT           NOT NULL,
    [ModifiedBy]                      UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]                    DATETIME         NOT NULL,
    CONSTRAINT [PK_ClinicalContactAnalysisMaster] PRIMARY KEY CLUSTERED ([ClinicalContactAnalysisMasterId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ClinicalContactAnalysisMaster_TranslationProxy] FOREIGN KEY ([TransProxyId]) REFERENCES [dbo].[TranslationProxy] ([TransProxyID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactAnalysisMaster_TransProxyId_FK]
    ON [dbo].[ClinicalContactAnalysisMaster]([TransProxyId] ASC);

