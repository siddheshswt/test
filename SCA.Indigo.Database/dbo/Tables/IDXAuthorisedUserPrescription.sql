﻿CREATE TABLE [dbo].[IDXAuthorisedUserPrescription] (
    [IDXAuthorisedUserPrescriptionID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]                      BIGINT           NULL,
    [UserId]                          UNIQUEIDENTIFIER NULL,
    [IsRemoved]                       BIT              NULL,
    [ModifiedBy]                      UNIQUEIDENTIFIER NULL,
    [ModifiedDate]                    DATETIME         NULL,
    CONSTRAINT [PK_IDXPrescriptionAuthorisedUser] PRIMARY KEY CLUSTERED ([IDXAuthorisedUserPrescriptionID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXAuthorisedUserPrescription_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_IDXAuthorisedUserPrescription_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXAuthorisedUserPrescription_CustomerId_FK]
    ON [dbo].[IDXAuthorisedUserPrescription]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXAuthorisedUserPrescription_UserId_FK]
    ON [dbo].[IDXAuthorisedUserPrescription]([UserId] ASC);

