﻿CREATE TABLE [dbo].[MassDataLog] (
    [LogId]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [TemplateName] VARCHAR (200)    NOT NULL,
    [DataFileName] VARCHAR (200)    NOT NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME         NOT NULL,
    [IsInsert]     BIT              NOT NULL,
    CONSTRAINT [PK_MassDataLog] PRIMARY KEY CLUSTERED ([LogId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_MassDataLog_UploadedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_MassDataLog_CreatedBy_FK]
    ON [dbo].[MassDataLog]([CreatedBy] ASC);

