﻿CREATE TABLE [dbo].[Country] (
    [CountryId]   INT            NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [CountryCode] VARCHAR (20)   NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC) WITH (FILLFACTOR = 80)
);

