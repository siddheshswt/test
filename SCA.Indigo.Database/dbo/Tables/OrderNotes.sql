﻿CREATE TABLE [dbo].[OrderNotes] (
    [OrderNoteId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [OrderId]     BIGINT           NOT NULL,
    [Note]        NVARCHAR (MAX)   NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_OrderNotes] PRIMARY KEY CLUSTERED ([OrderNoteId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_OrderNotes_Orders] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([OrderId])
);


GO
CREATE NONCLUSTERED INDEX [IX_OrderNotes_OrderId_FK]
    ON [dbo].[OrderNotes]([OrderId] ASC);

