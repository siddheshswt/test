﻿CREATE TABLE [dbo].[CustomerMedicalStaffAnalysis] (
    [CustomerMedicalStaffAnalysisId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]                     BIGINT           NULL,
    [ListId]                         BIGINT           NULL,
    [Text]                           TEXT             NULL,
    [Description]                    TEXT             NULL,
    [StaffId]                        VARCHAR (50)     NULL,
    [ModifiedBy]                     UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]                   DATETIME         NOT NULL,
    CONSTRAINT [PK_CustomerMedicalStaffAnalysis] PRIMARY KEY CLUSTERED ([CustomerMedicalStaffAnalysisId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CustomerMedicalStaffAnalysis_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_CustomerMedicalStaffAnalysis_List] FOREIGN KEY ([ListId]) REFERENCES [dbo].[List] ([ListId])
);


GO
CREATE NONCLUSTERED INDEX [IX_CustomerMedicalStaffAnalysis_CustomerId]
    ON [dbo].[CustomerMedicalStaffAnalysis]([CustomerId] ASC)
    INCLUDE([CustomerMedicalStaffAnalysisId]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_CustomerMedicalStaffAnalysis_CustomerId_FK]
    ON [dbo].[CustomerMedicalStaffAnalysis]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CustomerMedicalStaffAnalysis_ListId_FK]
    ON [dbo].[CustomerMedicalStaffAnalysis]([ListId] ASC);

