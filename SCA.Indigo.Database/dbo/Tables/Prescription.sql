﻿CREATE TABLE [dbo].[Prescription] (
    [PrescriptionId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PatientId]      BIGINT           NOT NULL,
    [ModifiedBy]     UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]   DATETIME         NOT NULL,
    [IsMassChange]   BIT              NULL,
    CONSTRAINT [PK_Prescriptions] PRIMARY KEY CLUSTERED ([PrescriptionId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Prescription_Patient] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Patient] ([PatientId]),
    CONSTRAINT [UK_Prescription_PatientID] UNIQUE NONCLUSTERED ([PatientId] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Prescription_PatientId_FK]
    ON [dbo].[Prescription]([PatientId] ASC);


GO
-- =============================================
-- Author:           Siddhesh Sawant
-- Create date: 02-Jul-2015
-- Description:      Send Prescription for Authorization if required after mass update of prescription
-- =============================================
-- ===============================================================================

--            Modified Date              Modified By                Purpose
--     1.     

-- ===============================================================================
CREATE TRIGGER [dbo].[tgrCheckPrescriptionAuthorisation]
   ON  [dbo].[Prescription]
   AFTER UPDATE
AS 
BEGIN  

    -- Insert statements for trigger here
       Declare @PrescriptionId bigint=0
       Declare @PatientId bigint=0
       DECLARE @Result INT = 0
       DECLARE @ResultMessage varchar(100)
       DECLARE @UserId varchar(100)      
       DECLARE @CurrentDate DATETIME = getdate()
       select @UserId = UserId from Users where  username='System'

       BEGIN TRY
              BEGIN TRANSACTION;
              BEGIN
                     IF UPDATE (IsMassChange) 
                     BEGIN                
                           SELECT 
                                         @PrescriptionId=Prescription.PrescriptionId,
                                         @PatientId=Prescription.PatientId
                                         from inserted Prescription
                                  

                           -- call your stored procedure here
                           --========================================
						   exec AuthorizationForPrescriptionMassUpdate @PrescriptionId,@PatientId
                           --==========================================
                           
                           SELECT @Result = @@ROWCOUNT  						              
                     END                  

              END           

              COMMIT TRANSACTION;
       END TRY

       BEGIN CATCH
              ROLLBACK TRANSACTION;

              SELECT @Result = - 1
                     ,@ResultMessage = ERROR_MESSAGE()

              -- Insert exception in exception log            
              EXEC SaveExceptionLog @UserID = @UserId
                     ,@TimeStamp = @CurrentDate
                     ,@ErrorMessage = @ResultMessage
       END CATCH;

END
