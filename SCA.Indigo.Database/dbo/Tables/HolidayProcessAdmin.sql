﻿CREATE TABLE [dbo].[HolidayProcessAdmin] (
    [HolidayProcessID] INT              IDENTITY (1, 1) NOT NULL,
    [CustomerId]       BIGINT           NULL,
    [NDD]              DATETIME         NULL,
    [StartTime]        DATETIME         NULL,
    [EndTime]          DATETIME         NULL,
    [ArrangedNDD]      DATETIME         NULL,
    [ModifiedBy]       UNIQUEIDENTIFIER NULL,
    [ModifiedDate]     DATETIME         NULL,
    CONSTRAINT [PK_HolidayProcessAdmin] PRIMARY KEY CLUSTERED ([HolidayProcessID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_HolidayProcessAdmin_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
);


GO
CREATE NONCLUSTERED INDEX [IX_HolidayProcessAdmin_CustomerId_FK]
    ON [dbo].[HolidayProcessAdmin]([CustomerId] ASC);

