﻿CREATE TABLE [dbo].[PersonalInformation] (
    [PersonalInformationId]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [FirstName]               NVARCHAR (100)   NULL,
    [LastName]                NVARCHAR (100)   NULL,
    [TitleId]                 BIGINT           NULL,
    [Gender]                  BIGINT           NULL,
    [AddressId]               BIGINT           NULL,
    [TelephoneNumber]         NVARCHAR (20)    NULL,
    [MobileNumber]            NVARCHAR (20)    NULL,
    [Email]                   NVARCHAR (50)    NULL,
    [FaxNumber]               NVARCHAR (20)    NULL,
    [CreatedById]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDateTime]         DATETIME         NOT NULL,
    [JobTitle]                TEXT             NULL,
    [ModifiedBy]              UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]            DATETIME         NOT NULL,
    [CorrespondenceAddressId] BIGINT           NULL,
    CONSTRAINT [PK_PersonalInformations] PRIMARY KEY CLUSTERED ([PersonalInformationId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PersonalInformation_Address] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId])
);


GO
CREATE NONCLUSTERED INDEX [ix_PersonalInformation]
    ON [dbo].[PersonalInformation]([PersonalInformationId] ASC, [AddressId] ASC, [TitleId] ASC)
    INCLUDE([FirstName], [LastName], [TelephoneNumber]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_PersonalInformation_AddressId]
    ON [dbo].[PersonalInformation]([AddressId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_PersonalInformation_AddressId_FK]
    ON [dbo].[PersonalInformation]([AddressId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PersonalInformation_CorrespondenceAddressId]
    ON [dbo].[PersonalInformation]([CorrespondenceAddressId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [ix_PersonalInformation_fn]
    ON [dbo].[PersonalInformation]([PersonalInformationId] ASC, [FirstName] ASC, [LastName] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_personalInformation_FN_LN_TN]
    ON [dbo].[PersonalInformation]([FirstName] ASC, [LastName] ASC, [TelephoneNumber] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_PersonalInformation_Inc]
    ON [dbo].[PersonalInformation]([PersonalInformationId] ASC, [AddressId] ASC, [TitleId] ASC, [CorrespondenceAddressId] ASC, [FirstName] ASC)
    INCLUDE([LastName], [TelephoneNumber]);

