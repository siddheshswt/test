﻿CREATE TABLE [dbo].[Role] (
    [RoleId]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [RoleName]               NVARCHAR (32)    NULL,
    [UserType]               NVARCHAR (20)    NULL,
    [ModifiedBy]             UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]           DATETIME         NOT NULL,
    [IsPatientInfoEditable]  BIT              CONSTRAINT [DF_Role_IsPatientInfoEditable] DEFAULT ((1)) NOT NULL,
    [IsMedicalInfoEditable]  BIT              CONSTRAINT [DF_Role_IsMedicalInfoEditable] DEFAULT ((1)) NOT NULL,
    [IsPrescriptionEditable] BIT              CONSTRAINT [DF_Role_IsPrescriptionEditable] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([RoleId] ASC) WITH (FILLFACTOR = 80)
);

