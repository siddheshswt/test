﻿CREATE TABLE [dbo].[IDXRoleMenu] (
    [IDXRoleMenuID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [RoleID]        BIGINT           NOT NULL,
    [MenuID]        BIGINT           NOT NULL,
    [ModifiedBy]    UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]  DATETIME         NOT NULL,
    [ViewOnly]      BIT              DEFAULT ((0)) NULL,
    [FullRights]    BIT              DEFAULT ((0)) NULL,
    CONSTRAINT [PK_IDXRoleMenu] PRIMARY KEY NONCLUSTERED ([IDXRoleMenuID] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_IDXRoleMenu]
    ON [dbo].[IDXRoleMenu]([IDXRoleMenuID] ASC) WITH (FILLFACTOR = 80);

