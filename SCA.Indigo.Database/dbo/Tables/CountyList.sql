﻿CREATE TABLE [dbo].[CountyList] (
    [CountyId]   INT           IDENTITY (1, 1) NOT NULL,
    [County]     VARCHAR (100) NOT NULL,
    [CountyCode] VARCHAR (50)  NOT NULL,
    [Status]     BIT           CONSTRAINT [DF_CountyList_Status] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_CountyList] PRIMARY KEY CLUSTERED ([CountyId] ASC) WITH (FILLFACTOR = 80)
);

