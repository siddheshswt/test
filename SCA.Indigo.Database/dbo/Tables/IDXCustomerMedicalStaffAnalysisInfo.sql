﻿CREATE TABLE [dbo].[IDXCustomerMedicalStaffAnalysisInfo] (
    [IDXCustomerMedicalStaffAnalysisInfoID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerID]                            BIGINT           NULL,
    [MedicalAnalysisListID]                 BIGINT           NULL,
    [MedicalType]                           VARCHAR (50)     NULL,
    [IsRemoved]                             BIT              CONSTRAINT [DF_IDXCustomerMedicalStaffAnalysisInfo_IsRemoved] DEFAULT ((0)) NOT NULL,
    [ModifiedBy]                            UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]                          DATETIME         NOT NULL,
    [IsMandatory]                           BIT              DEFAULT ((0)) NULL,
    [ContactName]                           VARCHAR (MAX)    NULL,
    [MedicalStaffAnalysisId]                BIGINT           NULL,
    [IsFieldType]                           BIT              NULL,
    [Remarks]                               NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_IDXCustomerMedicalStaffAnalysisInfo] PRIMARY KEY CLUSTERED ([IDXCustomerMedicalStaffAnalysisInfoID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_IDXCustomerMedicalStaffAnalysisInfo_ClinicalContactAnalysisMaster] FOREIGN KEY ([MedicalAnalysisListID]) REFERENCES [dbo].[ClinicalContactAnalysisMaster] ([ClinicalContactAnalysisMasterId]),
    CONSTRAINT [FK_IDXCustomerMedicalStaffAnalysisInfo_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([CustomerId])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerMedicalStaffAnalysisInfo_CustomerID_FK]
    ON [dbo].[IDXCustomerMedicalStaffAnalysisInfo]([CustomerID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerMedicalStaffAnalysisInfo_MedicalAnalysisListID_FK]
    ON [dbo].[IDXCustomerMedicalStaffAnalysisInfo]([MedicalAnalysisListID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDXCustomerMedicalStaffAnalysisInfo_Search]
    ON [dbo].[IDXCustomerMedicalStaffAnalysisInfo]([CustomerID] ASC, [IsRemoved] ASC) WITH (FILLFACTOR = 80);

