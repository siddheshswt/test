﻿CREATE TABLE [dbo].[ClinicalContactRoleMapping] (
    [ClinicalContactRoleMappingId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]                   BIGINT           NULL,
    [HierarchyListId]              BIGINT           NULL,
    [ChildListId]                  BIGINT           NULL,
    [ParentListId]                 BIGINT           NULL,
    [ModifiedBy]                   UNIQUEIDENTIFIER NULL,
    [ModifiedDate]                 DATETIME         NULL,
    CONSTRAINT [PK_ClinicalContactRoleMapping] PRIMARY KEY CLUSTERED ([ClinicalContactRoleMappingId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ClinicalContactRoleMapping_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_ClinicalContactRoleMapping_List] FOREIGN KEY ([HierarchyListId]) REFERENCES [dbo].[List] ([ListId]),
    CONSTRAINT [FK_ClinicalContactRoleMapping_Users] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[Users] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactRoleMapping_CustomerId]
    ON [dbo].[ClinicalContactRoleMapping]([CustomerId] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactRoleMapping_CustomerId_FK]
    ON [dbo].[ClinicalContactRoleMapping]([CustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactRoleMapping_HierarchyListId_FK]
    ON [dbo].[ClinicalContactRoleMapping]([HierarchyListId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactRoleMapping_ModifiedBy_FK]
    ON [dbo].[ClinicalContactRoleMapping]([ModifiedBy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ClinicalContactRoleMapping_Search]
    ON [dbo].[ClinicalContactRoleMapping]([ClinicalContactRoleMappingId] ASC, [CustomerId] ASC) WITH (FILLFACTOR = 80);

