﻿-- =============================================

-- Author:		
-- Create date: 
-- Description:	trim values

-- =============================================
-- =============================================
--	     Modified Date			Modified By			Purpose

--	1.  05-10-2016				Siddhesh Sawant		Added comments for CAST


-- =============================================
CREATE FUNCTION [dbo].[TRIM](@string VARCHAR(MAX))
RETURNS VARCHAR(MAX)
BEGIN
RETURN LTRIM(RTRIM(@string))
END



