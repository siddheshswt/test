﻿-- =============================================
-- Author:		Arvind Nishad
-- Create date: 20-03-2015
-- Description:	REPLACE SPECIAL CHARACHTER FROM STRING
-- =============================================

Create FUNCTION [dbo].[FN_REMOVESPECIALCHARACTER] (  
 @INPUT_STRING varchar(1000))
RETURNS VARCHAR(1000)
AS 
BEGIN
 
--declare @testString varchar(100),
DECLARE @NEWSTRING VARCHAR(1000) 
-- set @teststring = '@san?poojari(darsh)'
 SET @NEWSTRING = @INPUT_STRING ;

 select @NEWSTRING=Replace(@NEWSTRING, '&', '&amp;') -- Replace & with &&amp;
 select @NEWSTRING=Replace(@NEWSTRING, '>', '&gt;') -- Replace > with &gt;
 select @NEWSTRING=Replace(@NEWSTRING, '<', '&lt;') -- Replace < with &lt; 
 select @NEWSTRING=Replace(@NEWSTRING, '''', '&apos;') -- Replace ' with &apos;
 select @NEWSTRING=Replace(@NEWSTRING, '"', '&quot;') -- Replace " with &quot;

return @NEWSTRING 
END


