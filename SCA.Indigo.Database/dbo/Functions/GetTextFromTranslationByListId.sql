﻿CREATE FUNCTION dbo.GetTextFromTranslationByListId(@ListId BIGINT, @LanguageId BIGINT)  
RETURNS VARCHAR(500)   
AS   
-- Returns the stock level for the product.  
BEGIN  
    DECLARE @translationType VARCHAR(500)='';      

	SELECT @translationType = trans.TranslationType FROM TranslationProxy traprox (NOLOCK) INNER JOIN Translation trans (NOLOCK)
	ON traprox.TransProxyID = trans.TransProxyID
	WHERE trans.TransProxyID = (SELECT TransProxyID FROM LIST (NOLOCK) WHERE ListId = @ListId) AND trans.LanguageID = @LanguageId ;

	IF (@translationType IS NULL)   
        SET @translationType = '';

    RETURN @translationType;  
END;  
