﻿/****** Object:  UserDefinedFunction [dbo].[GetClinicalContacts]    Script Date: 5/18/2015 2:43:55 PM ******/

CREATE FUNCTION [dbo].[GetClinicalContactValueName]
(
	@ValueId BIGINT
)
RETURNS VARCHAR(400)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ClinicalContacts varchar(400) 
	
	SELECT 
		@ClinicalContacts = ContactName
	FROM ClinicalValueMaster
	WHERE ClinicalValueMasterId = @ValueId AND ISNULL(IsRemoved, 0) = 0
	
	RETURN @ClinicalContacts

END

