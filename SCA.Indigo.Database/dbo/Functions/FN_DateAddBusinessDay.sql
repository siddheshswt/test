﻿/******************************
** File:    dbo.FN_DateAddBusinessDay
** Name:	dbo.FN_DateAddBusinessDay
** Desc:	
** Auth:	Arvind
** Date:	

********************************
** Change History
********************************
** PR	Date			Author		Description	
** --	--------		-------		-----------------------------------
**	1   13/01/2017		Siddhesh	Get the date excluding weekend and holidayList
**	
*******************************/
-- =============================================

CREATE FUNCTION dbo.FN_DateAddBusinessDay
(
    @InputDate DATETIME = NULL,
    @InputDays INT,
	@CountryId INT = 1
)
RETURNS DATETIME
AS
BEGIN
    -- Declare the return variable here
    DECLARE @OutputDateTime DATETIME = @InputDate
    DECLARE @date DATETIME
    DECLARE @days INT
	DECLARE @Count INT = 1
    -- Add the T-SQL statements to compute the return value here
    SET @days = @InputDays    
       
	
	WHILE(@Count <= @days)
	BEGIN
		SET @OutputDateTime = DATEADD(d, 1, @OutputDateTime)
		IF ((UPPER(DATENAME(dw, @OutputDateTime)) = 'SATURDAY') OR (UPPER(DATENAME(dw, @OutputDateTime)) = 'SUNDAY'))
		BEGIN 
			CONTINUE			
		END		
		ELSE IF(EXISTS(SELECT HolidayDate FROM HolidayList WHERE CAST(HolidayDate AS DATE) = CAST(@OutputDateTime AS DATE) AND CountryID = @CountryId))
		BEGIN 
			CONTINUE
		END
		SET @Count = @Count + 1
	END

	RETURN @OutputDateTime;
 
END
 



