﻿-- =============================================

-- Author:		
-- Create date: 
-- Description:	Get Globalized Value Text

-- =============================================
-- =============================================
--	     Modified Date			Modified By			Purpose

--	1.  05-10-2016				Siddhesh Sawant		Added comments for CAST


-- =============================================
CREATE FUNCTION [dbo].[GlobalizedValueText]
(
@ColumnName VarChar(128),
@Culture VarChar(10) = Null,
@ID VarChar(Max)
)
RETURNS VarChar(MAX)
AS
BEGIN
declare @ListColumns table(Columnname varchar(max))
insert into @ListColumns (Columnname) values ('PatientType');
insert into @ListColumns (Columnname) values ('PatientStatus');
insert into @ListColumns (Columnname) values ('CommunicationFormat');
insert into @ListColumns (Columnname) values ('CareHomeStatus');
insert into @ListColumns (Columnname) values ('CareHomeType');
insert into @ListColumns (Columnname) values ('Customer_Status');
insert into @ListColumns (Columnname) values ('MedicalAnalysisListID');
insert into @ListColumns (Columnname) values ('ListID');
insert into @ListColumns (Columnname) values ('PatientTypeId');
insert into @ListColumns (Columnname) values ('PreferenceType');
insert into @ListColumns (Columnname) values ('ContactType');
insert into @ListColumns (Columnname) values ('ReasonCodeID');
insert into @ListColumns (Columnname) values ('Preference');
insert into @ListColumns (Columnname) values ('OrderType');


Declare @Value VarChar(MAX)
Declare @ListId Bigint

if(@ColumnName in (select * from @ListColumns))
begin
Set @ListId = Convert(Bigint, @ID);

Select @Value = (Select T.TranslationType from TranslationProxy TP 
inner join List L On L.TransProxyId = TP.TransProxyID
inner join Translation T on T.TransProxyID = TP.TransProxyID
where t.LanguageID = 1 and ListId = @ID)

end
ELSE IF @ColumnName = 'ProductId'
BEGIN
SELECT  @Value= ISNULL(SAPProductID,' ') FROM PRODUCT WHERE ProductId = (CONVERT(bigint,@ID))
SET @Value = SUBSTRING(@Value, PATINDEX('%[^0]%', @Value+'.'), LEN(@Value))
SET @Value = (CASE WHEN LEN(@Value) > 2 THEN lEFT(@Value, LEN(@Value)-2) ELSE '' END)
END
else 
begin
set @Value = CONVERT(varchar(max),@ID);
end

-- Return the result of the function
RETURN(@Value)

END



