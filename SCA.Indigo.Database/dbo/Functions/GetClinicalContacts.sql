﻿-- =============================================
-- Author:		<Jagdish Karanday>
-- Create date: <14 May 2015>
-- Description:	<Returns Clinical Contacts>
-- =============================================
CREATE FUNCTION [dbo].[GetClinicalContacts]
(
	@PatientId BIGINT,
	@TypeId INT
)
RETURNS VARCHAR(400)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ClinicalContacts varchar(400) 
	
	SELECT 
		@ClinicalContacts = COALESCE(@ClinicalContacts + ', ', '') + proxy.Text + ': ' + info.ContactName
	FROM IDXPatientMedicalStaff staff
		INNER JOIN [IDXCustomerMedicalStaffAnalysisInfo] info ON staff.CustomerMedicalStaffId= info.IDXCustomerMedicalStaffAnalysisInfoID
		INNER JOIN ClinicalContactAnalysisMaster clinical ON info.MedicalAnalysisListID = clinical.ClinicalContactAnalysisMasterId
		INNER JOIN TranslationProxy proxy 
	ON PROXY.TransProxyID = CLINICAL.TransProxyId
	WHERE PatientId = @PatientId AND clinical.TypeId = @TypeId
	
	RETURN @ClinicalContacts

END


