﻿CREATE SERVICE [NotificationService]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[NotificationQueue]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

