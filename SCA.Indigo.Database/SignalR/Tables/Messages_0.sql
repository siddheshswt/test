﻿CREATE TABLE [SignalR].[Messages_0] (
    [PayloadId]  BIGINT          NOT NULL,
    [Payload]    VARBINARY (MAX) NOT NULL,
    [InsertedOn] DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([PayloadId] ASC)
);

