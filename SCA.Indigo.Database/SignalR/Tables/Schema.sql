﻿CREATE TABLE [SignalR].[Schema] (
    [SchemaVersion] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([SchemaVersion] ASC)
);

