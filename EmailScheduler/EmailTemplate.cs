﻿// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Email sending template for EmailScheduler</summary>
// ***********************************************************************

namespace EmailScheduler
{
    using System;

    /// <summary>
    /// Email Template
    /// </summary>
    public static class EmailTemplate
    {
        #region Online/Telephone Log
        /// <summary>
        /// Email Subject for Online/Telephone Log
        /// </summary>
        public const string OnlineTelephoneLogSubject = "Online/Telephone Orders Log";

        /// <summary>
        /// Email Body for Online/Telephone Log
        /// </summary>
        public const string OnlineTelephoneLogBody = "Dear Indigo User,\r\n\r\nPlease find the attached logs for Online\\Telephone Orders.\r\n\r\nKind Regards,\r\nIndigo Support Team";
        #endregion
    }
}
