﻿// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Email sending class for EmailScheduler</summary>
// ***********************************************************************

namespace EmailScheduler
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class TriggerEmail
    {
        /// <summary>
        /// String Builder Object For Log Writing.
        /// </summary>
        StringBuilder strBuilder = new StringBuilder();

        private string IPAddress;
        private string SCATaskScheduler;
        private string OnlineTelephoneOrderDirectory;
        private string ArchivalDirectory;
        private Int64 OnlineTelephoneOrderFileTimeInMinutes;
        private Int64 OnlineOrderFileNoOfDaysInArchival;

        /// <summary>
        /// Gets or sets SolutionPath
        /// </summary>
        private static string solutionPath = ConfigurationManager.AppSettings["BasePath"];

        /// <summary>
        /// Initializes a new instance of the TriggerEmail
        /// </summary>
        public TriggerEmail()
        {
            IPAddress = ConfigurationManager.AppSettings["IPAddress"];
            SCATaskScheduler = ConfigurationManager.AppSettings["ServerAddress"];
        }

        #region Online/Telephone Order Log Mail

        /// <summary>
        /// Initilizes Variable Required For Online/Telephone Order Log Email
        /// </summary>
        public void OnlineTelephoneOrderLogEmailInitlize()
        {
            var _OnlineTelephoneOrderDirectory = ConfigurationManager.AppSettings["OnlineTelephoneOrderLog"];
            var _ArchivalDirectory = ConfigurationManager.AppSettings["ArchivePath"];

            OnlineTelephoneOrderDirectory = @"\\" + Path.Combine(IPAddress, SCATaskScheduler, _OnlineTelephoneOrderDirectory);
            strBuilder.Append("0.1 Online/Telephone Order Log File Path. " + OnlineTelephoneOrderDirectory + Environment.NewLine);

            ArchivalDirectory = @"\\" + Path.Combine(IPAddress, SCATaskScheduler, _OnlineTelephoneOrderDirectory, _ArchivalDirectory);
            strBuilder.Append("0.2 Online/Telephone Order Log Archival File Path. " + ArchivalDirectory + Environment.NewLine);

            var OnlineTelephoneOrderFileTimeHours = Convert.ToInt64(ConfigurationManager.AppSettings["OnlineOrderFileTimeHours"], CultureInfo.InvariantCulture);
            OnlineTelephoneOrderFileTimeInMinutes = (OnlineTelephoneOrderFileTimeHours * 60) + 2;
            strBuilder.Append("0.3 Online/Telephone Order Log Time. " + OnlineTelephoneOrderFileTimeInMinutes + Environment.NewLine);
            
            OnlineOrderFileNoOfDaysInArchival = Convert.ToInt64(ConfigurationManager.AppSettings["OnlineOrderFileNoOfDaysInArchival"], CultureInfo.InvariantCulture);
            strBuilder.Append("0.4 No Of Days For Archival. " + OnlineOrderFileNoOfDaysInArchival + Environment.NewLine);
        }

        /// <summary>
        /// Sends Online\Telephone order log in Email
        /// </summary>
        /// <returns></returns>
        public bool SendOnlineTelephoneOrderLog()
        {
            var startTime = DateTime.Now;
            try
            {
                strBuilder.Append("1.1 Initilization Started." + Environment.NewLine);
                OnlineTelephoneOrderLogEmailInitlize();
                strBuilder.Append("1.2 Initilization Completed." + Environment.NewLine);

                strBuilder.Append("1.3 File Reading Started." + Environment.NewLine);
                var lstFile = ReadOnlineTelephoneOrderLogFiles();
                strBuilder.Append("1.4 File Reading Completed." + Environment.NewLine);

                if (lstFile.Count > 0)
                {
                    EmailModel objEmail = new EmailModel();

                    objEmail.FromEmailId = ConfigurationManager.AppSettings["EmailFrom"];
                    strBuilder.Append("1.5 From EmailId : " + objEmail.FromEmailId + Environment.NewLine);

                    var strTo = ConfigurationManager.AppSettings["OnlineOrderLogEmailTo"];
                    foreach (string str in strTo.Trim().Split(';'))
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            objEmail.ToEmailId.Add(str);
                            strBuilder.Append("1.6 To EmailId : " + str + Environment.NewLine);
                        }
                    }

                    var strCc = ConfigurationManager.AppSettings["OnlineOrderLogEmailCc"];
                    foreach (string str in strCc.Trim().Split(';'))
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            objEmail.CcEmailId.Add(str);
                            strBuilder.Append("1.7 Cc EmailId : " + str + Environment.NewLine);
                        }
                    }

                    var strBcc = ConfigurationManager.AppSettings["OnlineOrderLogEmailBcc"];
                    foreach (string str in strBcc.Trim().Split(';'))
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            objEmail.BccEmailId.Add(str);
                            strBuilder.Append("1.8 Bcc EmailId : " + str + Environment.NewLine);
                        }
                    }

                    foreach (var obj in lstFile)
                    {
                        objEmail.Attachment.Add(obj.FullName);
                        strBuilder.Append("1.9 Attachment FilePath : " + obj.FullName + Environment.NewLine);
                    }

                    objEmail.Subject = EmailTemplate.OnlineTelephoneLogSubject;
                    objEmail.IsBodyHtml = false;
                    objEmail.Body = EmailTemplate.OnlineTelephoneLogBody;

                    strBuilder.Append("1.10 Email Sending. Start Time : " + DateTime.Now + Environment.NewLine);
                    objEmail.Send();
                    strBuilder.Append("1.11 Email Sending Completed. End Time : " + DateTime.Now + Environment.NewLine);

                    strBuilder.Append("1.12 File Archival Started." + Environment.NewLine);
                    MoveLogFileToArchival(lstFile);
                    strBuilder.Append("1.13 File Archival Completed" + Environment.NewLine);
                }

                if (OnlineOrderFileNoOfDaysInArchival == 0)
                    strBuilder.Append("1.14 Archival Not Marked For Deletion." + Environment.NewLine);
                else
                {
                    strBuilder.Append("1.15 Delete File From Archival Started." + Environment.NewLine);
                    DeleteArchivalFile(ArchivalDirectory, OnlineOrderFileNoOfDaysInArchival);
                    strBuilder.Append("1.16 Delete File From Archival Completed" + Environment.NewLine);
                }
            }
            catch (ArgumentNullException ex)
            {
                strBuilder.Append("1.17 " + ex.Message + Environment.NewLine);
                strBuilder.Append("1.18 " + ex.StackTrace + Environment.NewLine);
            }
            catch (Exception ex)
            {
                strBuilder.Append("1.19 " + ex.Message + Environment.NewLine);
                strBuilder.Append("1.20 " + ex.StackTrace + Environment.NewLine);
            }
            LogToFile(startTime, "SendOnlineTelephoneOrderLog", strBuilder.ToString());
            return true;
        }

        /// <summary>
        /// Reads log file created in particular time interval
        /// </summary>
        /// <returns></returns>
        private List<FileInfo> ReadOnlineTelephoneOrderLogFiles()
        {
            List<FileInfo> lstFiles = new List<FileInfo>();

            DirectoryInfo objDirectory = new DirectoryInfo(OnlineTelephoneOrderDirectory);

            if (objDirectory.Exists)
            {
                lstFiles = objDirectory.GetFiles().Where(q => q.CreationTime > DateTime.Now.AddMinutes(-OnlineTelephoneOrderFileTimeInMinutes)).ToList();
                if (lstFiles.Count > 0)
                {
                    strBuilder.Append("2.1 Count of Online/Telephone Order Log Files : " + lstFiles.Count + Environment.NewLine);
                    strBuilder.Append("2.2 File Names : " + Environment.NewLine);
                    foreach (var file in lstFiles)
                        strBuilder.Append("2.3 " + file.Name + Environment.NewLine);
                }
                else
                    strBuilder.Append("2.4 Online/Telephone Order Log Files Not Ceated For This Batch." + Environment.NewLine);
            }
            else
            {
                strBuilder.Append("2.5 Online/Telephone Order Log Directory Does Not Exits Or Not Accessible." + Environment.NewLine);
            }
            return lstFiles;
        }

        /// <summary>
        /// Move Online\Telephone Logs To archival Folder
        /// </summary>
        /// <param name="lstFile"></param>
        /// <returns></returns>
        private void MoveLogFileToArchival(List<FileInfo> lstFile)
        {
            if (!Directory.Exists(ArchivalDirectory))
            {
                strBuilder.Append("3.1 Creating Archival Directory." + Environment.NewLine);
                Directory.CreateDirectory(ArchivalDirectory);
                strBuilder.Append("3.2 Archival Directory Created." + Environment.NewLine);
            }

            strBuilder.Append("3.3 File Moving To Archival." + Environment.NewLine);
            foreach (var file in lstFile)
            {
                file.MoveTo(ArchivalDirectory + "\\" + file.Name);
                strBuilder.Append("3.4 File Moved " + file.Name + Environment.NewLine);
            }
        }
        #endregion

        /// <summary>
        /// Delete's The Files From Specified Location.
        /// </summary>
        /// <param name="path">Directory</param>
        /// <param name="NoOfDays">No Of Days Previous File Delete</param>
        public void DeleteArchivalFile(string path, Int64 NoOfDays)
        {
            if (!Directory.Exists(path))
            {
                strBuilder.Append("4.1 Path Does Not Exist To Delete Files." + Environment.NewLine);
                return;
            }

            var lstFiles = new DirectoryInfo(path).GetFiles().Where(q => q.CreationTime < DateTime.Now.AddDays(-NoOfDays)).ToList();

            if (lstFiles.Count > 0)
            {
                strBuilder.Append("4.2 Count Of Files To Be Deleted. " + lstFiles.Count + Environment.NewLine);
                strBuilder.Append("4.3 File Deleting." + Environment.NewLine);
                foreach (var file in lstFiles)
                {
                    file.Delete();
                    strBuilder.Append("4.4 File Deleted. FileName : " + file.Name + Environment.NewLine);
                }
            }
            else
                strBuilder.Append("4.5 File Does Not Exist To Delete." + Environment.NewLine);
        }

        /// <summary>
        /// Log message to a txt file
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="moduleName"></param>
        /// <param name="logMessage"></param>
        /// <param name="logFileName"></param>
        private static void LogToFile(DateTime startTime, string moduleName, string logMessage, string logFileName = "TaskLog")
        {
            string strLogFilePath = Path.Combine(solutionPath, "LogFile\\");

            if (!Directory.Exists(strLogFilePath))
                Directory.CreateDirectory(strLogFilePath);

            strLogFilePath += logFileName + ".txt";

            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(string.Empty);
                textWriter.WriteLine("+++++++++++++++ " + moduleName + " Batch Started at " + startTime + " +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++++++ Batch End at " + DateTime.Now + " +++++++++++++++");
            }
        }
    }
}
