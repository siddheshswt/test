﻿// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Start point for Taskscheduler</summary>
// ***********************************************************************

namespace EmailScheduler
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Start Point for Email scheduler
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Application starter
        /// </summary>
        /// <param name="args">string[] args</param>
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Email Scheduler Started at " + DateTime.Now.ToString());
                TriggerEmail objEmail = new TriggerEmail();

                if (args.Length > 0)
                {
                    var runType = args[0];
                    if (runType.ToUpper(CultureInfo.InvariantCulture) == "OT")// OT: Online and Telephone log files
                    {
                        objEmail.SendOnlineTelephoneOrderLog();
                    }
                }                
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
    }
}
