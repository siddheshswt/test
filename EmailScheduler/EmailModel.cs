﻿// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Email sending class for EmailScheduler</summary>
// ***********************************************************************

namespace EmailScheduler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// Email Model for Sending EMails
    /// </summary>
    public class EmailModel
    {
        /// <summary>
        /// Initilizes the new Instance of EmailModel class
        /// </summary>
        public EmailModel()
        {
            ToEmailId = new List<string>();
            CcEmailId = new List<string>();
            BccEmailId = new List<string>();
            Attachment = new List<string>();
        }

        /// <summary>
        /// From Email Id
        /// </summary>
        public string FromEmailId { get; set; }

        /// <summary>
        /// To EmailId List
        /// </summary>
        public List<string> ToEmailId { get; set; }

        /// <summary>
        /// CC EmailId List
        /// </summary>
        public List<string> CcEmailId { get; set; }

        /// <summary>
        /// BCC EmailId List
        /// </summary>
        public List<string> BccEmailId { get; set; }

        /// <summary>
        /// Email Attachement : Specify file path
        /// </summary>
        public List<string> Attachment { get; set; }

        /// <summary>
        /// Subject of the Email
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Is Body HTML
        /// </summary>
        public bool IsBodyHtml { get; set; }

        /// <summary>
        /// Email Body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Validates the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>
        /// True or False
        /// </returns>
        private bool ValidateEmail(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                string regularExpression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                var regex = new Regex(regularExpression);
                if (regex.IsMatch(email))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Checks the Validity of the email
        /// </summary>
        /// <returns></returns>
        private bool IsValid()
        {
            if (!string.IsNullOrEmpty(FromEmailId))
            {
                if (!ValidateEmail(FromEmailId))
                    throw new ArgumentNullException("FromEmailId:" + FromEmailId, "Send mail failed from address invalid.");
            }
            else
                throw new ArgumentNullException("FromEmailId", "Send mail failed from address is null or empty.");

            if (ToEmailId.Count > 0)
                foreach (string str in ToEmailId)
                {
                    if (!ValidateEmail(str))
                    {
                        throw new ArgumentNullException("ToEmailId: " + str, "Send mail failed to address is invalid.");
                    }
                }
            else
                throw new ArgumentNullException("ToEmailId", "Send mail failed to address is null or empty.");

            foreach (string str in CcEmailId)
                if (!ValidateEmail(str))
                    throw new ArgumentNullException("CCEmailId: " + str, "Send mail failed to address is invalid.");

            foreach (string str in BccEmailId)
                if (!ValidateEmail(str))
                    throw new ArgumentNullException("BCCEmailId: " + str, "Send mail failed to address is invalid.");

            foreach(string str in Attachment)
                if (!File.Exists(str))
                    throw new ArgumentNullException("Attachment: " + str, "Send mail failed Attachement does not exist.");

            return true;
        }

        /// <summary>
        /// Sends Email Using SMTP server.
        /// </summary>
        /// <returns></returns>
        public bool Send()
        {
            SmtpClient smtpClient = null;
            MailMessage mail = null;
            try
            {
                if (IsValid())
                {
                    smtpClient = new SmtpClient();
                    if (smtpClient != null && smtpClient.DeliveryMethod == System.Net.Mail.SmtpDeliveryMethod.SpecifiedPickupDirectory)
                    {
                        var directoryPath = smtpClient.PickupDirectoryLocation;
                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);
                    }

                    mail = new MailMessage();

                    mail.From = new MailAddress(FromEmailId);

                    foreach (string str in ToEmailId)
                        mail.To.Add(str);

                    foreach (string str in CcEmailId)
                        mail.CC.Add(str);

                    foreach (string str in BccEmailId)
                        mail.Bcc.Add(str);

                    foreach (string str in Attachment)
                        mail.Attachments.Add(new System.Net.Mail.Attachment(str, MimeMapping.GetMimeMapping(str)));

                    mail.Subject = Subject;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.IsBodyHtml = IsBodyHtml;
                    mail.Body = Body;

                    smtpClient.Send(mail);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (smtpClient != null)
                    smtpClient.Dispose();

                if (mail != null)
                    mail.Dispose();
            }
            return true;
        }
    }
}