﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace IncrementalUpdateIndexScheduler
{
    class ServiceConsumer
    {
        #region Class level variables

        /// <summary>
        /// Gets or sets IPAddress
        /// </summary>
        private string IPAddress { get; set; }

        /// <summary>
        /// Gets or sets SCATaskScheduler
        /// </summary>
        private string SCATaskScheduler { get; set; }
        
        /// <summary>
        /// Gets or sets SolutionPath
        /// </summary>
        private string SolutionPath { get; set; }

       /// <summary>
        /// Gets or sets RequestUrl
       /// </summary>
        private string RequestUrl { get; set; }

        /// <summary>
        /// Gets or sets ObjHttpClient
        /// </summary>
        private HttpClient ObjHttpClient { get; set; }

        /// <summary>
        /// Gets or sets BaseUrl
        /// </summary>
        private string BaseUrl { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the ServiceConsumer       
        /// </summary>
        public ServiceConsumer()
        {
            SolutionPath = ConfigurationManager.AppSettings["BasePath"];
            BaseUrl = ConfigurationManager.AppSettings["BaseUrl"].ToString();
            //IPAddress = ConfigurationManager.AppSettings["IPAddress"];
            //SCATaskScheduler = ConfigurationManager.AppSettings["ServerAddress"];            
        }

        
        /// <summary>
        /// IncrementalUpdateSearchIndex
        /// </summary>
        public void IncrementalUpdateSearchIndex()
        {
            RequestUrl = "IncrementalUpdateSearchIndex/ReadXMLFilesFromFolder";
            GetHttpResponse("IncrementalUpdateSearchIndex", "IncrementalUpdate");
        }       

        /// <summary>
        /// returns Http Response(Calling method-Get)
        /// </summary>
        /// <param name="batchHeader">batch Header</param>
        /// <param name="orderType">order Type</param>
        public void GetHttpResponse(string batchHeader, string batchType)
        {
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                var objHttpClient = new HttpClient();
                //// Set Time out 5 minut=300000 milisecond
                objHttpClient.Timeout = TimeSpan.FromMinutes(40);
                objHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = objHttpClient.GetAsync(new Uri(this.BaseUrl + "/" + this.RequestUrl)).Result;
                endTime = DateTime.Now;

                stringBuilder.Append(response.RequestMessage.ToString() + "\n");
                stringBuilder.Append(response.ToString());
            }
            catch (InvalidOperationException e)
            {
                stringBuilder.Append(e.Message + " \n");
                stringBuilder.Append(e.StackTrace + " \n");
            }
            catch (Exception e)
            {
                stringBuilder.Append(e.Message + " \n");
                stringBuilder.Append(e.StackTrace + " \n");
            }

            LogToFile(startTime, endTime, batchHeader, batchType, stringBuilder.ToString(), "TaskLog");
        }

        /// <summary>
        /// Log message to a txt file
        /// </summary>
        /// <param name="startTime">start Time</param>
        /// <param name="endTime">end Time</param>
        /// <param name="batchHeader">batch Header</param>
        /// <param name="logMessage">log Message</param>
        /// <param name="orderType">order Type</param>
        /// <param name="logFileName">log FileName</param>
        private void LogToFile(DateTime startTime, DateTime endTime, string batchHeader, string batchType, string logMessage, string logFileName)
        {
            // create a writer and open the file           
            string strLogFilePath = Path.Combine(SolutionPath + batchType, "LogFile\\");

            if (!Directory.Exists(strLogFilePath))
                Directory.CreateDirectory(strLogFilePath);

            strLogFilePath += logFileName + ".txt";

            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(string.Empty);
                textWriter.WriteLine("+++++++++++  " + batchHeader + " Batch Started at " + startTime + "  +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++ Batch End at " + endTime + "        +++++++++++++++");
            }
        }
    }
}
