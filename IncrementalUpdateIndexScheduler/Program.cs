﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncrementalUpdateIndexScheduler
{
    class Program
    {
        static void Main()
        {
            try
            {
                if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
                    return;

                System.Diagnostics.Process[] objProcesses = System.Diagnostics.Process.GetProcessesByName("IndexBuilderScheduler");
                if (objProcesses.Length > 0)
                {
                    System.Diagnostics.Process.GetProcessesByName("IncrementalUpdateIndexScheduler")[0].Kill();                     
                }
                

                
                Console.WriteLine("IncrementalUpdateIndex Scheduler Started at " + DateTime.Now.ToString());
                ServiceConsumer objBuilder = new ServiceConsumer();

                objBuilder.IncrementalUpdateSearchIndex();

                       
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }
    }
}
