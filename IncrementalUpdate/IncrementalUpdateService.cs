﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using SCA.Indigo.BusinessCore;
using SCA.Indigo.BusinessCore.Interfaces;

namespace IncrementalUpdate
{
    public partial class IncrementalUpdateService : ServiceBase
    {
        /// <summary>
        /// Gets or sets FolderPath
        /// </summary>
        string FolderPath { get; set; }

        FileSystemWatcher fileWatcher = null;

        EventHandlerForFileWatcher _eventWatcher = null;
        SequenceFileProcess _objSequence = null;
        IIncrementalUpdateCore _objIncrementalUpdateCore = null;
        IBuildIndexCore _iBuildIndex = null;
        IFileCore _iFileCore = null;


      private  string _iPAddress = ConfigurationManager.AppSettings["IPAddress"];
      private string _scaTaskScheduler = ConfigurationManager.AppSettings["ServerAddress"];
      private  string _incrementalUpdateSearchIndexLog = ConfigurationManager.AppSettings["IncrementalUpdateSearchIndexLog"];
      private  string _archivalDirectoryLog = ConfigurationManager.AppSettings["ArchivePath"];
       

        public IncrementalUpdateService()
        {
            InitializeComponent();
        }

        /// <summary>
        /// DO NOT REMOVE: This Method is reuiqred for Debugging windows service
        /// </summary>
        /// <param name="args"></param>
        //internal void TestStartupAndStop(string[] args)
        //{
        //    this.OnStart(args);
        //    Console.ReadLine();
        //    this.OnStop();
        //    Thread.Sleep(50000000);
        //}

        protected override void OnStart(string[] args)
        {
            Program.Log("OnStart Started.");
            try
            {
                FolderPath = ConfigurationManager.AppSettings["BasePath"];
                Program.Log("Directory For File Watcher: " + FolderPath);

                _objIncrementalUpdateCore = new IncrementalUpdateCore(_iPAddress, _scaTaskScheduler, _incrementalUpdateSearchIndexLog, _archivalDirectoryLog);
                _iBuildIndex = new GoogleSearchCore();
                _iFileCore = new FileProcessCore();
                _objSequence = new SequenceFileProcess(_objIncrementalUpdateCore, _iBuildIndex, _iFileCore);

                Program.Log("File Watcher Started.");
                fileWatcher = new FileSystemWatcher(FolderPath, "*.xml");
                fileWatcher.NotifyFilter = NotifyFilters.LastWrite;
                _eventWatcher = new EventHandlerForFileWatcher(_objSequence);

                fileWatcher.Changed += new FileSystemEventHandler(_eventWatcher.FileCreated);
                fileWatcher.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                Program.Log("Excpetion In OnStart: " + ex.ToString());                
            }
            finally
            {
                Program.Log("Onstart Completed.");
            }
        }

        protected override void OnStop()
        {
            Program.Log("OnStop Started."); 
            try
            {
                //TODO:
            }
            catch (Exception ex)
            {
                Program.Log("Excpetion In OnStop: " + ex.ToString());
            }
            finally
            {
                Program.Log("OnStop Completed.");  
            }
        }
    }
}
