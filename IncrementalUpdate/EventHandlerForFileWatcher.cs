﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCA.Indigo.BusinessCore.Interfaces;
using System.Threading;

namespace IncrementalUpdate
{
    public class EventHandlerForFileWatcher
    {
        SequenceFileProcess objSequence = null;

        public EventHandlerForFileWatcher(SequenceFileProcess sequence)
        {
            this.objSequence = sequence;
        }

        public void FileCreated(object source, FileSystemEventArgs e)
        {
            try
            {
                Program.Log("File Captured: " + e.Name);
                objSequence.AddFileToQueue(e.FullPath);
            }
            catch (Exception ex)
            {
                Program.Log("Exception In FileCreated: " + ex.ToString());
            }

        }
    }
}
