﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IncrementalUpdate
{
    static class Program
    {
        private static string solutionPath = ConfigurationManager.AppSettings["BasePath"];
        private static string logPath = ConfigurationManager.AppSettings["WindowsServiceLog"];

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // DO NOT DELETE: This is reuired for Debugging the windows service
            //System.Diagnostics.Debugger.Break();
            //if (Environment.UserInteractive)
            //{
            //    IncrementalUpdateService service1 = new IncrementalUpdateService();
            //    service1.TestStartupAndStop(null);
            //}

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            {
                new IncrementalUpdateService() 
            };
            ServiceBase.Run(ServicesToRun);
        }

        public static void Log(string logMessage)
        {
            string strLogFilePath = Path.Combine(solutionPath, logPath);

            if (!Directory.Exists(strLogFilePath))
                Directory.CreateDirectory(strLogFilePath);

            strLogFilePath += "\\WindowsServiceLog.txt";

            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(DateTime.Now.ToString() + ": " + logMessage);
            }
        }
    }
}
