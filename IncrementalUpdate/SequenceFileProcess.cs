﻿using SCA.Indigo.BusinessCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IncrementalUpdate
{
    public class SequenceFileProcess
    {
        IIncrementalUpdateCore _objIncrementalUpdateCore = null;
        IBuildIndexCore _iBuildIndex = null;
        IFileCore _iFileCore = null;
        Thread thread = null;
        object locked = new object();

        public string UserId
        {
            get { return "00000000-0000-0000-0000-000000000000"; } //default user id. Considered as System User
        }

        public SequenceFileProcess(IIncrementalUpdateCore objIncrementalUpdateCorex, IBuildIndexCore iBuildIndex, IFileCore iFileCore)
        {
            this._objIncrementalUpdateCore = objIncrementalUpdateCorex;
            this._iBuildIndex = iBuildIndex;
            this._iFileCore = iFileCore;
        }

        public void AddFileToQueue(string FilePath)
        {
            try
            {
                thread = new Thread(() => StartFileProcess(FilePath));
                thread.Start();
            }
            catch (Exception ex)
            {
                Program.Log("Exception In AddFileToQueue: " + ex.ToString());
            }
        }

        public void StartFileProcess(string FilePath)
        {
            try
            {
                var fileName = Path.GetFileName(FilePath);
                lock (locked)
                {
                    if (!_objIncrementalUpdateCore.FileIsReady(FilePath, UserId))
                    {
                        Program.Log("EXCEPTION: XML file is open in another process. File Name: " + fileName);
                        return;
                    }

                    Program.Log("XML File Reading Started: " + fileName);
                    _objIncrementalUpdateCore.ReadXMLFilesFromFolder(_iBuildIndex, _iFileCore, fileName, UserId);
                    Program.Log("XML File Reading Completed: " + fileName);
                }
            }
            catch (Exception ex)
            {
                Program.Log("Exception In StartFileProcess: " + ex.ToString());
            }
        }
    }
}
