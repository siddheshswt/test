﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Model
// Author           : syerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="DatabaseFactory.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Model
{
    using System;

    public class DatabaseFactory : IDatabaseFactory
    {
        private bool _isDisposed;
        private SCAIndigoEntities _dataContext;

        public SCAIndigoEntities Get()
        {
            return _dataContext ?? (_dataContext = new SCAIndigoEntities());
        }
        
        ~DatabaseFactory()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        protected void DisposeCore()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
            }
        }
    }

    /// <summary>
    /// IDatabaseFactory
    /// </summary>
    public interface IDatabaseFactory : IDisposable
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        SCAIndigoEntities Get();
    }
}
