//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class ReportBuilderFormat
    {
        [Key]
        public long ReportBuilderId { get; set; }
        public string ReportFormatName { get; set; }
        public string CustomerColumn { get; set; }
        public string PatientTypeMatrixColumn { get; set; }
        public string CustomerParameterColumn { get; set; }
        public string PatientColumn { get; set; }
        public string CareHomeColumn { get; set; }
        public string UserMaintainceColumn { get; set; }
        public string ClinicalContactsColumn { get; set; }
        public string PrescriptionColumn { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string OrderColumn { get; set; }
    }
}
