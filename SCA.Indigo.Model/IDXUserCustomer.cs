//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class IDXUserCustomer
    {
        [Key]
        public long IDXUserCustomerId { get; set; }
        public System.Guid UserId { get; set; }
        public long CustomerId { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual Users User { get; set; }
    }
}
