//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public partial class GetCareHomePatientProducts_Result
    {
        public Nullable<long> IDXPrescriptionProductId { get; set; }
        public Nullable<long> PrescriptionId { get; set; }
        public Nullable<long> ProductId { get; set; }
        public string Description { get; set; }
        public Nullable<long> Quantity { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
    }
}
