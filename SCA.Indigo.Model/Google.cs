//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Google
    {
        [Key]
        public long GoogleId { get; set; }
        public long IndigoId { get; set; }
        public int ObjectType { get; set; }
        public string Fields { get; set; }
        public long CustomerId { get; set; }
        public long Status { get; set; }
        public string PatientType { get; set; }
        public string LastName { get; set; }
        public string CareHomeName { get; set; }
        public string Postcode { get; set; }
        public Nullable<System.DateTime> DateofBirth { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<long> PatientTypeID { get; set; }
        public Nullable<long> CareHomeId { get; set; }
        public string FirstName { get; set; }
    }
}
