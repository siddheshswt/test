//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Users
    {
        public Users()
        {
            this.ClinicalContactRoleMappings = new HashSet<ClinicalContactRoleMapping>();
            this.ClinicalContactValueMappings = new HashSet<ClinicalContactValueMapping>();
            this.IDXAuthorisedUserPrescriptions = new HashSet<IDXAuthorisedUserPrescription>();
            this.IDXUserCustomers = new HashSet<IDXUserCustomer>();
            this.MassDataLogs = new HashSet<MassDataLog>();
            this.PasswordHistories = new HashSet<PasswordHistory>();
            this.PostcodeMatrices = new HashSet<PostcodeMatrix>();
            this.PrescriptionExtendeds = new HashSet<PrescriptionExtended>();
            this.LoginUserDetails = new HashSet<LoginUserDetail>();
            this.IDXUserRoles = new HashSet<IDXUserRole>();
        }
    
        [Key]
        public System.Guid UserId { get; set; }
        public Nullable<long> PersonId { get; set; }
        public Nullable<long> SecurityLevel { get; set; }
        public Nullable<System.DateTime> ValidFromDate { get; set; }
        public Nullable<System.DateTime> ValidToDate { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsAccountLocked { get; set; }
        public Nullable<long> NoOfAttempts { get; set; }
        public Nullable<bool> IsNewUser { get; set; }
        public Nullable<int> AuthorizationPIN { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string GroupEmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public bool IsCarehomeUser { get; set; }
        public bool IsActive { get; set; }
        public bool IsInternalUser { get; set; }
    
        public virtual ICollection<ClinicalContactRoleMapping> ClinicalContactRoleMappings { get; set; }
        public virtual ICollection<ClinicalContactValueMapping> ClinicalContactValueMappings { get; set; }
        public virtual ICollection<IDXAuthorisedUserPrescription> IDXAuthorisedUserPrescriptions { get; set; }
        public virtual ICollection<IDXUserCustomer> IDXUserCustomers { get; set; }
        public virtual ICollection<MassDataLog> MassDataLogs { get; set; }
        public virtual ICollection<PasswordHistory> PasswordHistories { get; set; }
        public virtual PersonalInformation PersonalInformation { get; set; }
        public virtual ICollection<PostcodeMatrix> PostcodeMatrices { get; set; }
        public virtual ICollection<PrescriptionExtended> PrescriptionExtendeds { get; set; }
        public virtual ICollection<LoginUserDetail> LoginUserDetails { get; set; }
        public virtual ICollection<IDXUserRole> IDXUserRoles { get; set; }
    }
}
