//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class IDXCustomerProduct
    {
        [Key]
        public long IDXId { get; set; }
        public Nullable<long> ProductId { get; set; }
        public Nullable<long> CustomerId { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public Nullable<bool> IsApporvalRequired { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsDisplayForSample { get; set; }
        public bool IsOverridden { get; set; }
        public Nullable<long> Quantity { get; set; }
        public Nullable<long> Frequency { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
    }
}
