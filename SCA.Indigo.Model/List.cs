//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class List
    {
        public List()
        {
            this.CustomerMedicalStaffAnalysis = new HashSet<CustomerMedicalStaffAnalysis>();
            this.IDXPatientTypeMatrices = new HashSet<IDXPatientTypeMatrix>();
            this.Patients = new HashSet<Patient>();
            this.SamplePatients = new HashSet<SamplePatient>();
            this.Interactions = new HashSet<Interaction>();
            this.Interactions1 = new HashSet<Interaction>();
            this.Interactions2 = new HashSet<Interaction>();
            this.IDXInteractiontypeSubTypes = new HashSet<IDXInteractiontypeSubType>();
            this.IDXInteractiontypeSubTypes1 = new HashSet<IDXInteractiontypeSubType>();
            this.IDXCustomerPatientTypes = new HashSet<IDXCustomerPatientType>();
            this.IDXCustomerReasonCodes = new HashSet<IDXCustomerReasonCodes>();
            this.ClinicalContactRoleMappings = new HashSet<ClinicalContactRoleMapping>();
            this.Notes = new HashSet<Note>();
            this.Attachments = new HashSet<Attachment>();
            this.Patients1 = new HashSet<Patient>();
            this.Orders = new HashSet<Order>();
            this.Orders1 = new HashSet<Order>();
            this.PatientTypeMatrices = new HashSet<PatientTypeMatrix>();
            this.Customers = new HashSet<Customer>();
            this.Patients11 = new HashSet<Patient>();
            this.IDXPreferences = new HashSet<IDXPreference>();
            this.OrderCreationType = new HashSet<Order>();
            this.CareHomes = new HashSet<CareHome>();
            this.CareHomes1 = new HashSet<CareHome>();
            this.CareHomes2 = new HashSet<CareHome>();
            this.CareHomes3 = new HashSet<CareHome>();
        }
    
        [Key]
        public long ListId { get; set; }
        public long ListTypeId { get; set; }
        public int ListIndex { get; set; }
        public string DefaultText { get; set; }
        public string Description { get; set; }
        public long TransProxyId { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual ICollection<CustomerMedicalStaffAnalysis> CustomerMedicalStaffAnalysis { get; set; }
        public virtual ICollection<IDXPatientTypeMatrix> IDXPatientTypeMatrices { get; set; }
        public virtual ListType ListType { get; set; }
        public virtual TranslationProxy TranslationProxy { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
        public virtual ICollection<SamplePatient> SamplePatients { get; set; }
        public virtual ICollection<Interaction> Interactions { get; set; }
        public virtual ICollection<Interaction> Interactions1 { get; set; }
        public virtual ICollection<Interaction> Interactions2 { get; set; }
        public virtual ICollection<IDXInteractiontypeSubType> IDXInteractiontypeSubTypes { get; set; }
        public virtual ICollection<IDXInteractiontypeSubType> IDXInteractiontypeSubTypes1 { get; set; }
        public virtual ICollection<IDXCustomerPatientType> IDXCustomerPatientTypes { get; set; }
        public virtual ICollection<IDXCustomerReasonCodes> IDXCustomerReasonCodes { get; set; }
        public virtual ICollection<ClinicalContactRoleMapping> ClinicalContactRoleMappings { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<Patient> Patients1 { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Order> Orders1 { get; set; }
        public virtual ICollection<PatientTypeMatrix> PatientTypeMatrices { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Patient> Patients11 { get; set; }
        public virtual ICollection<IDXPreference> IDXPreferences { get; set; }
        public virtual ICollection<Order> OrderCreationType { get; set; }
        public virtual ICollection<CareHome> CareHomes { get; set; }
        public virtual ICollection<CareHome> CareHomes1 { get; set; }
        public virtual ICollection<CareHome> CareHomes2 { get; set; }
        public virtual ICollection<CareHome> CareHomes3 { get; set; }
    }
}
