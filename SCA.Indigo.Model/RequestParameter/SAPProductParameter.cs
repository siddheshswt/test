﻿//-----------------------------------------------------------------------
// <copyright file="SAPProductParameter.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Model.RequestParameter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Product Parameter
    /// </summary>
    public class SAPProductParameter
    {
        public long CustomerId { get; set; }

        public string ProductId { get; set; }

        public string SAPProductId { get; set; }

        public string Basematerial { get; set; }

        public string SAPProductName { get; set; }

        public decimal Netvalue { get; set; }

        public decimal VAT { get; set; }

        public string Salesunit { get; set; }

        public bool ApprovalFlag { get; set; }

        public string Packsize { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}
