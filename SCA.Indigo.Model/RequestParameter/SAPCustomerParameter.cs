﻿//-----------------------------------------------------------------------
// <copyright file="SAPCustomerParameter.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Model.RequestParameter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Customer Parameter
    /// </summary>
    public class SAPCustomerParameter
    {
        public string CustomerId { get; set; }

        public string CustomerNo { get; set; }

        public string SalseOrg { get; set; }

        public string Distrchannel { get; set; }

        public string Division { get; set; }

        public string Haulier { get; set; }

        public string CustomerName { get; set; }

        public string Street { get; set; }

        public string Postcode { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string Country { get; set; }

        public string HouseNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string PostCode { get; set; }

        public string HouseName { get; set; }

        public string TelephoneNumber { get; set; }

        public string MobileNumber { get; set; }

        public string Email { get; set; }

        public string FaxNumber { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }
    }
}
