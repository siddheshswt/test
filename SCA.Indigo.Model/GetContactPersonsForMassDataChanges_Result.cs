//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public partial class GetContactPersonsForMassDataChanges_Result
    {
        public string CustomerId { get; set; }
        public string PatientId { get; set; }
        public string CarehomeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string TelephoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public long ContactPersonId { get; set; }
        public string Remarks { get; set; }
    }
}
