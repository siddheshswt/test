//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class PersonalInformation
    {
        public PersonalInformation()
        {
            this.ContactPersons = new HashSet<ContactPerson>();
            this.Patients = new HashSet<Patient>();
            this.SamplePatients = new HashSet<SamplePatient>();
            this.Customers = new HashSet<Customer>();
            this.CareHomes = new HashSet<CareHome>();
            this.Users = new HashSet<Users>();
        }
    
        [Key]
        public long PersonalInformationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<long> TitleId { get; set; }
        public Nullable<long> Gender { get; set; }
        public Nullable<long> AddressId { get; set; }
        public string TelephoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string FaxNumber { get; set; }
        public System.Guid CreatedById { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string JobTitle { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public Nullable<long> CorrespondenceAddressId { get; set; }
    
        public virtual Address Address { get; set; }
        public virtual ICollection<ContactPerson> ContactPersons { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }
        public virtual ICollection<SamplePatient> SamplePatients { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<CareHome> CareHomes { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
