﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Model
// Author           : syerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="Repository.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Model
{
    using System.Data.Entity;

    public class Repository : DbContext
    {
        private readonly DatabaseFactory _databaseFactory;

        private SCAIndigoEntities _dataContext;

        public SCAIndigoEntities DataContext
        {
            get { return _dataContext ?? (_dataContext = _databaseFactory.Get()); }
        }

        private UnitOfWork _unitOfWork;

        private UnitOfWork UnitOfWork
        {
            get { return _unitOfWork ?? (_unitOfWork = new UnitOfWork(DataContext)); }
        }

        public Repository()
        {
            _databaseFactory = new DatabaseFactory();
        }

        public UnitOfWork GetUnitOfWork()
        {
            return UnitOfWork;
        }
    }  
}
