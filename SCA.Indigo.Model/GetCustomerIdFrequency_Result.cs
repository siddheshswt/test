//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public partial class GetCustomerIdFrequency_Result
    {
        public string CustomerId { get; set; }
        public Nullable<int> PatientFrequency { get; set; }
        public Nullable<System.DateTime> SAPDeliveryDate { get; set; }
        public Nullable<long> CarehomeId { get; set; }
        public Nullable<long> CarehomeFrequency { get; set; }
        public string Round { get; set; }
        public string RoundId { get; set; }
        public string Postcode { get; set; }
        public Nullable<long> PatientId { get; set; }
        public Nullable<long> OrderType { get; set; }
        public Nullable<long> OrderCreationType { get; set; }
    }
}
