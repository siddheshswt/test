﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Model
// Author           : syerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="DBHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Core.Objects;
    using System.Globalization;
    using System.Linq;

    using SCA.Indigo.Model.RequestParameter;
    using System.Data.Common;
	using System.Data;
    using System.Data.SqlClient;

    public class DBHelper
    {
        /// <summary>
        /// Get Authorised customers
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List (GetCustomers_usp_Result) .</returns>
        public List<GetCustomers_usp_Result> GetAuthoriseCustomers(Guid userId)
        {
            var result = new List<GetCustomers_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCustomers_usp(userId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Search Results
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List (GetSearchResults_usp_Result) .</returns>
        public List<GetSearchResults_usp_Result> GetSearchResults(string searchText, string userId)
        {
            var result = new List<GetSearchResults_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetSearchResults_usp(searchText, Guid.Parse(userId)).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Display Patient Details
        /// </summary>
        /// <param name="page">Page No</param>
        /// <param name="rowCount">No of Rows</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientStatus">The patient status.</param>
        /// <param name="patientType">Type of the patient.</param>
        /// <param name="orderNo">The order no.</param>
        /// <param name="searchID">Search Id</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="sortColumn">Sort Column</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>List (DisplayPatientDetails_usp_Result) .</returns>
        public List<DisplayPatientDetails_usp_Result> DisplayPatientDetails(long page, long rowCount, string searchText, string customerId, string patientStatus, 
                                                                            long patientType, string orderNo, string searchID, string userId, string sortColumn, string sortOrder)
        {
            var result = new List<DisplayPatientDetails_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.DisplayPatientDetails_usp(page, rowCount, searchText, customerId, patientStatus, patientType, orderNo, searchID,
                                                               Guid.Parse(userId), sortColumn, sortOrder).ToList();
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Get PatientDetails
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>List (GetPatientDetails_usp_Result).</returns>
        public List<GetPatientDetails_usp_Result> GetPatientDetails(long patientId)
        {
            var result = new List<GetPatientDetails_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPatientDetails_usp(patientId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get GetPostCodeMatrix For Patient
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>List (GetPatientDetails_usp_Result).</returns>
        public List<GetPostCodeMatrixForPatient_Result> GetPostCodeMatrixForPatient(long customerId, long patientId, long carehomeId)
        {
            var result = new List<GetPostCodeMatrixForPatient_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPostCodeMatrixForPatient(patientId, customerId, carehomeId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Care home details of a customer
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>List (GetCarehomeDetails_Result).</returns>
        public List<GetCarehomeDetails_Result> GetCarehomeDetails(long customerId, long careHomeId, string userId)
        {
            var result = new List<GetCarehomeDetails_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCarehomeDetails(customerId, careHomeId, userId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Care home details of a customer
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>List (GetCarehomeDetails_Result).</returns>
        public List<GetCareHomePatientList_Result> GetCarehomePatientDetails(long careHomeId)
        {
            var result = new List<GetCareHomePatientList_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCareHomePatientList(careHomeId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Care home report data
        /// </summary>
        /// <param name="careHomeId"></param>
        /// <returns></returns>
        public List<GetCareHomeReportData_Result> GetCarehomeReportData(long careHomeId, bool isExport, long page, long rows, string sidx, string sord)
        {
            var result = new List<GetCareHomeReportData_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCareHomeReportData(careHomeId, isExport, page, rows, sidx, sord).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// Get List Details
        /// </summary>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>List (GetListDetails_usp_Result)</returns>
        public List<GetListDetails_usp_Result> GetListDetails(string listTypeId, int languageId)
        {
            var result = new List<GetListDetails_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetListDetails_usp(listTypeId, languageId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get GetNoteDetailsForPatient Details
        /// </summary>
        /// <param name="patientId">patient id.</param>        
        /// <returns>List (GetNoteDetailsForPatient_Result)</returns>
        public List<GetNoteDetailsForPatient_Result> GetNoteDetailsForPatient(long patientId)
        {
            var result = new List<GetNoteDetailsForPatient_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetNoteDetailsForPatient(patientId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get List Details
        /// </summary>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <param name="listId">The list identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>List (GetListDetailsOnListId_usp_Result)</returns>
        public List<GetListDetailsOnListId_usp_Result> GetListDetails(string listTypeId, string listId, int languageId)
        {
            var result = new List<GetListDetailsOnListId_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetListDetailsOnListId_usp(listTypeId, listId, languageId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get List Details
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>List of Menu Id's assigned to the User's role</returns>
        public List<GetUserMenuDetails_usp_Result> GetUserMenuDetails(string userName, long languageId)
        {
            var result = new List<GetUserMenuDetails_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetUserMenuDetails_usp(userName, languageId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Save customer details along with Address and PersonalInformation
        /// </summary>
        /// <param name="parameter">instance of SaveSAPCustomerDeatailsParameter</param>
        /// <returns>long value.</returns>
        public long SaveSapCustomerDetails(SAPCustomerParameter parameter)
        {
            try
            {
                var customerId = new ObjectParameter("CustomerId", "0");
                using (var context = new SCAIndigoEntities())
                {
                    context.SaveSAPCustomerDeatails(
                       parameter.HouseNumber,
                       parameter.AddressLine1,
                       parameter.AddressLine2,
                       parameter.City,
                       parameter.County,
                       parameter.Country,
                       parameter.Postcode,
                       parameter.HouseName,
                       parameter.CustomerName,
                       parameter.TelephoneNumber,
                       parameter.MobileNumber,
                       parameter.Email,
                       parameter.FaxNumber,
                       parameter.CustomerNo,
                       parameter.SalseOrg,
                       parameter.Distrchannel,
                       parameter.Division,
                       parameter.Haulier,
                       parameter.ModifiedBy,
                       DateTime.Now,
                       customerId).SingleOrDefault();
                }
                return Convert.ToInt64(customerId.Value, CultureInfo.InvariantCulture);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Save product details and make entry in IDXCustomerProduct
        /// </summary>
        /// <param name="parameter">instance of SaveSAPCustomerProductParameter</param>
        /// <returns>returns newly added Product</returns>
        public SaveSAPCustomerProduct_Result SaveSapCustomerProduct(SAPProductParameter parameter)
        {
            try
            {
                var productId = new ObjectParameter("ProductId", "0");
                SaveSAPCustomerProduct_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.SaveSAPCustomerProduct(
                    parameter.CustomerId,
                    parameter.SAPProductId,
                    parameter.SAPProductName,
                    parameter.Salesunit,
                    parameter.ApprovalFlag,
                    parameter.Basematerial,
                    parameter.Netvalue,
                    parameter.VAT,
                    parameter.Packsize,
                    parameter.ModifiedBy,
                    parameter.ModifiedDate,
                    productId).SingleOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get SAP user Details
        /// </summary>
        /// <returns>Sap user's details</returns>
        public string GetSAPUserDetails()
        {
            string result = string.Empty;
            using (var context = new SCAIndigoEntities())
            {
                result = context.GetSAPUserDetails().SingleOrDefault().UserId.ToString();
            }
            return result.ToString().ToUpper(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get Order header information
        /// </summary>
        /// <returns>List of Sap Order's details.</returns>
        public List<GetSAPOrderDetails_Result> GetHeaderDetails()
        {
            try
            {
                List<GetSAPOrderDetails_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetSAPOrderDetails().ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Order Details based on OrderId
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>List (Get SAPOrderItemDetails_Result) </returns>
        public List<GetSAPOrderItemDetails_Result> GetOrderItemsDetails(long orderId)
        {
            try
            {
                List<GetSAPOrderItemDetails_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetSAPOrderItemDetails(orderId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Order Details based on OrderId
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="NDD">Next Delivery date to be set as Prescription's NDD </param>
        /// <returns>List of SAP Order Item Details </returns>
        public object UpdatePrescriptionNDD(long? PrescriptionId, DateTime NDD)
        {
            try
            {
                object result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.UpdatePrescriptionNDD(PrescriptionId, NDD);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get NDD with minimum frequency from the prescription
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>NDD </returns>
        public DateTime? GetNDDWithMinimumFrequency(long orderId)        
        {                                                   
            try
            {
                DateTime? result = DateTime.Now; 
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetNDDWithMinimumFrequency(orderId).FirstOrDefault();
                }
                return result.Value;
            }
            catch (Exception)
            {
                throw;
            }
        }
       

        /// <summary>
        /// Get All Customer Details with carehome and Patient
        /// </summary>
        /// <param name="filters">string filters</param>
        /// <returns>List of Customers</returns>
        public List<GetSAPCustomerPatientList_Result> GetAllCustomers(string filters)
        {
            try
            {
                List<GetSAPCustomerPatientList_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetSAPCustomerPatientList(filters).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Patient or Carehome SAP Id
        /// </summary>
        /// <param name="patientId">long patientId</param>
        /// <param name="sapPatientNumber">string sapPatientNumber</param>
        /// <param name ="customerType">Customer Type Id</param>
        /// <returns>Updated SAP PatientId.</returns>
        public UpdateSAPPatientId_Result CustomerResponse(long patientId, string sapPatientNumber, string customerType)
        {
            try
            {
                UpdateSAPPatientId_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.UpdateSAPPatientId(patientId, sapPatientNumber, customerType).FirstOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update OrderNo and Delivery Date
        /// </summary>
        /// <param name="orderId">long orderId</param>
        /// <param name="sapOrderNo">string sapOrderNo</param>
        /// <param name="sapDeliveryNo">tring sapDeliveryNo</param>
        /// <param name="sapInvoice">string sapInvoice</param>
        /// <param name="errorCode">string errorCode</param>
        /// <param name="errorMessage">string errorMessage</param>
        /// <param name="ndd">Next Delivery Date</param>
        /// <returns>Update SapOrder Details</returns>     
        public UpdateSapOrderDetails_Result OrderResponse(
            long orderId,
            string sapOrderNo,
            string sapDeliveryNo,
            string sapInvoice,
            string errorCode,
            string errorMessage,
            string baseMaterialId,
            DateTime? ndd,
            DateTime? carehomeNDD,
            bool isCarehomeOrder,
            string sapActualDelDate,
            string sapPlant,
            string sapBillingDate,
            string sapCurrency,
            string sapBillingQuantity,
            string sapBillingUnit,
            string sapNetValue,
            string sapVatValue,
            string sapPODDate,
            string sapDeliveryQuantity,
            string sapDeliveryUnit,
            string sapOrderItem
            )
        {
            try
            {
                UpdateSapOrderDetails_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.UpdateSapOrderDetails(
                        orderId,
                        sapOrderNo,
                        null,
                        sapOrderItem,
                        sapDeliveryNo,
                        null,
                        sapInvoice,
                        null,
                        errorCode,
                        errorMessage,
                        baseMaterialId,
                        ndd,
                        carehomeNDD,
                        isCarehomeOrder,
                        sapActualDelDate,
                        sapPlant,
                        sapBillingDate,
                        sapCurrency,
                        sapBillingQuantity,
                        sapBillingUnit,
                        sapNetValue,
                        sapVatValue,
                        sapPODDate,
                        sapDeliveryQuantity,
                        sapDeliveryUnit).FirstOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update order ststaus once order send to SAP
        /// </summary>
        /// <param name="OrderIds">OrderId by comma separated</param>
        /// <returns></returns>
        public UpdateSAPOrderStatus_Result UpdateSAPOrderStatus(string OrderIds)
        {
            try
            {
                UpdateSAPOrderStatus_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.UpdateSAPOrderStatus(OrderIds).FirstOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetCustomerId
        /// </summary>
        /// <param name="OrderIds"></param>
        /// <returns></returns>
        public GetCustomerIdFrequency_Result GetCustomerId(long OrderIds)
        {
            try
            {
                GetCustomerIdFrequency_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCustomerIdFrequency(OrderIds).FirstOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetCountyList
        /// </summary>
        /// <returns></returns>
        public List<GetCountyList_Result> GetCountyList()
        {
            try
            {
                List<GetCountyList_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCountyList().ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetPendingAuthorizations
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public List<GetPendingAuthorizations_Result> GetPendingAuthorizations(string userName)
        {
            try
            {
                List<GetPendingAuthorizations_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPendingAuthorizations(userName).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetMenuDetails
        /// </summary>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public List<GetMenuDetails_Result> GetMenuDetails(long languageId)
        {
            try
            {
                List<GetMenuDetails_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetMenuDetails(languageId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetUserRoleCustomerMapping
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="sapCustomerId"></param>
        /// <returns></returns>
        public List<GetUserRoleCustomerMapping_Result> GetUserRoleCustomerMapping(string userName, string sapCustomerId)
        {
            try
            {
                List<GetUserRoleCustomerMapping_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetUserRoleCustomerMapping(userName, sapCustomerId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Patient Status IsSentToSAP =True once patient is sent to sap after modification
        /// </summary>
        /// <param name="patientId">string patientId</param>
        /// <param name="recordType">string recordType</param>
        /// <returns></returns>
        public UpdatePatientSentToSAPStatus_Result UpdateSendToSAPFlag(string patientId, string recordType, string userId)
        {
            try
            {
                UpdatePatientSentToSAPStatus_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.UpdatePatientSentToSAPStatus(patientId, recordType, userId).FirstOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetArrangedDeliveryDate
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="DerivedDate"></param>
        /// <param name="OrderCreationTime"></param>
        /// <returns></returns>
        public GetArrangedDeliveryDate_Result GetArrangedDeliveryDate(long? CustomerId, DateTime DerivedDate, string OrderCreationTime)
        {
            try
            {
                GetArrangedDeliveryDate_Result result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetArrangedDeliveryDate(CustomerId, DerivedDate, OrderCreationTime).FirstOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetRoleMenuBy
        /// </summary>
        /// <param name="parentMenuId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<GetRoleMenuByParentMenuId_Result> GetRoleMenuBy(long parentMenuId, long roleId)
        {
            try
            {
                List<GetRoleMenuByParentMenuId_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetRoleMenuByParentMenuId(parentMenuId, roleId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// GetInteractionSummaryDetails
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientId"></param>
        /// <param name="carehomeId"></param>
        /// <param name="interactionId"></param>
        /// <param name="assignedTo"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<GetInteractionSummaryDetails_Result> GetInteractionSummaryDetails(long customerId, long patientId,
            long carehomeId, long interactionId, Guid? assignedTo, string userId = null)
        {
            try
            {
                List<GetInteractionSummaryDetails_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetInteractionSummaryDetails(customerId, patientId, carehomeId, interactionId,
                        assignedTo, userId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Notes Details based on NoteType and NoteTypeId
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>List (Get SAPOrderItemDetails_Result) </returns>
        /// 
        public List<GetNoteList_Result> GetNotes(long noteType, long noteTypeId)
        {
            try
            {
                List<GetNoteList_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetNoteList(noteType, noteTypeId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get careHome name id list
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List of careHome</returns>
        public List<GetCareHomeNameIdList_usp_Result> GetCareHomeNameIdList(Guid userId)
        {
            var result = new List<GetCareHomeNameIdList_usp_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCareHomeNameIdList_usp(userId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region AuditLog

        /// <summary>
        /// GetAuditLog
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="RecordsPerPage"></param>
        /// <param name="AuditLogTypeId"></param>
        /// <param name="PatientId"></param>
        /// <param name="CustomerId"></param>
        /// <param name="CareHomeId"></param>
        /// <param name="LoggedInUserId"></param>
        /// <param name="SearchedUserId"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="TableName"></param>
        /// <param name="IsExport"></param>
        /// <returns></returns>
        public List<GetAuditLog_Result> GetAuditLog(long PageNumber, long RecordsPerPage, long? AuditLogTypeId, long? PatientId, long? CustomerId, long? CareHomeId, Guid LoggedInUserId, Guid SearchedUserId, DateTime? FromDate, DateTime? ToDate, string TableName, bool? IsExport)
        {
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    var data = context.GetAuditLog(PageNumber, RecordsPerPage, AuditLogTypeId, PatientId, CustomerId, CareHomeId, LoggedInUserId, FromDate, ToDate, TableName, IsExport, SearchedUserId);
                    return data.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


    /// <summary>
   /// GetAuditLogSearchResult
    /// </summary>
    /// <param name="searchText"></param>
    /// <param name="searchTable"></param>
    /// <param name="userId"></param>
    /// <param name="customerId"></param>
    /// <returns></returns>
        public List<AuditLogCriteriaSearch_Result> GetAuditLogSearchResult(string searchText, string searchTable, Guid userId, long? customerId)
        {
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    var data = context.AuditLogCriteriaSearch(searchText, searchTable, userId, customerId);
                    return data.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        /// <summary>
        /// GetClinicalContactHierarchy
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<GetClinicalContactHierarchy_Result> GetClinicalContactHierarchy(long customerId)
        {
            var result = new List<GetClinicalContactHierarchy_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetClinicalContactHierarchy(customerId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the attachment details.
        /// </summary>
        /// <param name="typeId">The type identifier.</param>
        /// <param name="inputAttachmentType">Type of the input attachment.</param>
        /// <returns></returns>
        public List<GetAttachmentDetails_Result> GetAttachmentDetails(long typeId, long inputAttachmentType)
        {
            try
            {
                List<GetAttachmentDetails_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetAttachmentDetails(typeId, inputAttachmentType).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get Clinical Contact Analysis Master Data
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public List<GetClinicalContactAnalysisMaster_Result> GetClinicalContactAnalysisMaster(string listTypeId, int languageId)
        {
            var result = new List<GetClinicalContactAnalysisMaster_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetClinicalContactAnalysisMaster(listTypeId, languageId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Role Type For Clinical Value Mapping
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<GetRoleTypeForClinicalValueMapping_Result> GetRoleTypeForClinicalValueMapping(long customerId, int languageId)
        {
            var result = new List<GetRoleTypeForClinicalValueMapping_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetRoleTypeForClinicalValueMapping(customerId, languageId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get report data
        /// </summary>
        /// <param name="reportBuilderCriteria">Report Builder Criteria</param>
        /// <returns>report data</returns>
        public List<GetReportData_usp_Result> GetReportData(string connection, int timeout, string columns, string customerIds, string patientNameId, string careHomeIds, string patientDeliveryDateFrom
                                                          , string patientDeliveryDateTo, string orderDateFrom, string orderDateTo, string invoiceDateFrom, string invoiceDateTo
                                                          , string patientCreateDateFrom, string patientCreateDateTo
                                                          , string nextAssessmentDateFrom, string nextAssessmentDateTo 
                                                          , string invoiceNo, string invoiceNoTo
                                                          , string patientType, string patientStatus, bool userDataOnly, string customerColumns, string patientColumns
                                                          , string prescriptionColumns, string clinicalContactsColumns, string careHomeColumns, string orderColumns
                                                          , string userColumns, string productIds, string languageId, int pageNbr, int pageSize, string sortColumn, string sortDir
                                                          , bool isDownload, out int totalRows)
        {
            List<GetReportData_usp_Result> result = new List<GetReportData_usp_Result>();
            totalRows = 0;
            string[] columnsArray = columns.Split(',');
			DbCommand dbCommand = null;
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(connection);
                dbCommand = sqlConnection.CreateCommand();

                dbCommand.CommandText = "GetReportData_usp";
                dbCommand.CommandType = System.Data.CommandType.StoredProcedure;

                DbParameter paramCustomerId = dbCommand.CreateParameter();
                paramCustomerId.ParameterName = "@CustomerId";
                paramCustomerId.Value = customerIds == null ? string.Empty : customerIds;
                dbCommand.Parameters.Add(paramCustomerId);

                DbParameter paramPatientId = dbCommand.CreateParameter();
                paramPatientId.ParameterName = "@PatientIdName";
                paramPatientId.Value = patientNameId == null ? string.Empty : patientNameId;
                dbCommand.Parameters.Add(paramPatientId);

                DbParameter paramCareHomeId = dbCommand.CreateParameter();
                paramCareHomeId.ParameterName = "@CareHomeId";
                paramCareHomeId.Value = careHomeIds == null ? string.Empty : careHomeIds;
                dbCommand.Parameters.Add(paramCareHomeId);

                DbParameter paramDeliveryFrom = dbCommand.CreateParameter();
                paramDeliveryFrom.ParameterName = "@PatientDeilveryFromDate";
                paramDeliveryFrom.Value = patientDeliveryDateFrom == null ? string.Empty : patientDeliveryDateFrom;
                dbCommand.Parameters.Add(paramDeliveryFrom);

                DbParameter paramDeliveryTo = dbCommand.CreateParameter();
                paramDeliveryTo.ParameterName = "@PatientDeliveryToDate";
                paramDeliveryTo.Value = patientDeliveryDateTo == null ? string.Empty : patientDeliveryDateTo;
                dbCommand.Parameters.Add(paramDeliveryTo);

                DbParameter paramOrderFrom = dbCommand.CreateParameter();
                paramOrderFrom.ParameterName = "@OrderFromDate";
                paramOrderFrom.Value = orderDateFrom == null ? string.Empty : orderDateFrom;
                dbCommand.Parameters.Add(paramOrderFrom);

                DbParameter paramOrderTo = dbCommand.CreateParameter();
                paramOrderTo.ParameterName = "@OrderToDate";
                paramOrderTo.Value = orderDateTo == null ? string.Empty : orderDateTo;
                dbCommand.Parameters.Add(paramOrderTo);

                DbParameter paramInvoiceFrom = dbCommand.CreateParameter();
                paramInvoiceFrom.ParameterName = "@InvoiceFromDate";
                paramInvoiceFrom.Value = invoiceDateFrom == null ? string.Empty : invoiceDateFrom;
                dbCommand.Parameters.Add(paramInvoiceFrom);

                DbParameter paramInvoiceTo = dbCommand.CreateParameter();
                paramInvoiceTo.ParameterName = "@InvoiceToDate";
                paramInvoiceTo.Value = invoiceDateTo == null ? string.Empty : invoiceDateTo;
                dbCommand.Parameters.Add(paramInvoiceTo);

                DbParameter paramPatientCreateFrom = dbCommand.CreateParameter();
                paramPatientCreateFrom.ParameterName = "@PatientCreateFromDate";
                paramPatientCreateFrom.Value = patientCreateDateFrom == null ? string.Empty : patientCreateDateFrom;
                dbCommand.Parameters.Add(paramPatientCreateFrom);

                DbParameter paramPatientCreateTo = dbCommand.CreateParameter();
                paramPatientCreateTo.ParameterName = "@PatientCreateToDate";
                paramPatientCreateTo.Value = patientCreateDateTo == null ? string.Empty : patientCreateDateTo;
                dbCommand.Parameters.Add(paramPatientCreateTo);

                DbParameter paramNextAssessmentFrom = dbCommand.CreateParameter();
                paramNextAssessmentFrom.ParameterName = "@NextAssessmentFromDate";
                paramNextAssessmentFrom.Value = nextAssessmentDateFrom == null ? string.Empty : nextAssessmentDateFrom;
                dbCommand.Parameters.Add(paramNextAssessmentFrom);

                DbParameter paramNextAssessmentTo = dbCommand.CreateParameter();
                paramNextAssessmentTo.ParameterName = "@NextAssessmentToDate";
                paramNextAssessmentTo.Value = nextAssessmentDateTo == null ? string.Empty : nextAssessmentDateTo;
                dbCommand.Parameters.Add(paramNextAssessmentTo);

                DbParameter paramInvoiceNo = dbCommand.CreateParameter();
                paramInvoiceNo.ParameterName = "@InvoiceNumber";
                paramInvoiceNo.Value = invoiceNo == null ? string.Empty : invoiceNo;
                dbCommand.Parameters.Add(paramInvoiceNo);

                DbParameter paramInvoiceNoTo = dbCommand.CreateParameter();
                paramInvoiceNoTo.ParameterName = "@InvoiceNumberTo";
                paramInvoiceNoTo.Value = invoiceNoTo == null ? string.Empty : invoiceNoTo;
                dbCommand.Parameters.Add(paramInvoiceNoTo);

                DbParameter paramPatientType = dbCommand.CreateParameter();
                paramPatientType.ParameterName = "@PatientType";
                paramPatientType.Value = patientType == null ? string.Empty : patientType;
                dbCommand.Parameters.Add(paramPatientType);

                DbParameter paramPatientStatus = dbCommand.CreateParameter();
                paramPatientStatus.ParameterName = "@PatientStatus";
                paramPatientStatus.Value = patientStatus == null ? string.Empty : patientStatus;
                dbCommand.Parameters.Add(paramPatientStatus);

                DbParameter paramUserDataOnly = dbCommand.CreateParameter();
                paramUserDataOnly.ParameterName = "@UserDataOnly";
                paramUserDataOnly.Value = userDataOnly;
                dbCommand.Parameters.Add(paramUserDataOnly);

                DbParameter paramCustomerColums = dbCommand.CreateParameter();
                paramCustomerColums.ParameterName = "@CustomerColumns";
                paramCustomerColums.Value = customerColumns == null ? string.Empty : customerColumns;
                dbCommand.Parameters.Add(paramCustomerColums);

                DbParameter paramPatientColums = dbCommand.CreateParameter();
                paramPatientColums.ParameterName = "@PatientColumns";
                paramPatientColums.Value = patientColumns == null ? string.Empty : patientColumns;
                dbCommand.Parameters.Add(paramPatientColums);

                DbParameter paramPrescriptionColums = dbCommand.CreateParameter();
                paramPrescriptionColums.ParameterName = "@PrescriptionColumns";
                paramPrescriptionColums.Value = prescriptionColumns == null ? string.Empty : prescriptionColumns;
                dbCommand.Parameters.Add(paramPrescriptionColums);

                DbParameter paramClinicalColums = dbCommand.CreateParameter();
                paramClinicalColums.ParameterName = "@ClinicalContactsColumns";
                paramClinicalColums.Value = clinicalContactsColumns == null ? string.Empty : clinicalContactsColumns;
                dbCommand.Parameters.Add(paramClinicalColums);

                DbParameter paramCareHomeColums = dbCommand.CreateParameter();
                paramCareHomeColums.ParameterName = "@CareHomeColumns";
                paramCareHomeColums.Value = careHomeColumns == null ? string.Empty : careHomeColumns;
                dbCommand.Parameters.Add(paramCareHomeColums);

                DbParameter paramOrderColums = dbCommand.CreateParameter();
                paramOrderColums.ParameterName = "@OrdersColumns";
                paramOrderColums.Value = orderColumns == null ? string.Empty : orderColumns;
                dbCommand.Parameters.Add(paramOrderColums);

                DbParameter paramUserColums = dbCommand.CreateParameter();
                paramUserColums.ParameterName = "@UserColumns";
                paramUserColums.Value = userColumns == null ? string.Empty : userColumns;
                dbCommand.Parameters.Add(paramUserColums);

                DbParameter paramProductIds = dbCommand.CreateParameter();
                paramProductIds.ParameterName = "@ProductIds";
                paramProductIds.Value = productIds == null ? string.Empty : productIds;
                dbCommand.Parameters.Add(paramProductIds);

                DbParameter paramLanguageId = dbCommand.CreateParameter();
                paramLanguageId.ParameterName = "@LanguageId";
                paramLanguageId.Value = languageId == null ? string.Empty : languageId;
                dbCommand.Parameters.Add(paramLanguageId);

                DbParameter paramPageNbr = dbCommand.CreateParameter();
                paramPageNbr.ParameterName = "@PageNbr";
                paramPageNbr.Value = pageNbr == 0 ? 1 : pageNbr;
                dbCommand.Parameters.Add(paramPageNbr);

                DbParameter paramPageSize = dbCommand.CreateParameter();
                paramPageSize.ParameterName = "@PageSize";
                paramPageSize.Value = pageSize == 0 ? 10 : pageSize;
                dbCommand.Parameters.Add(paramPageSize);

                DbParameter paramSortColumn = dbCommand.CreateParameter();
                paramSortColumn.ParameterName = "@SortColumn";
                paramSortColumn.Value = sortColumn;
                dbCommand.Parameters.Add(paramSortColumn);

                DbParameter paramSortDir = dbCommand.CreateParameter();
                paramSortDir.ParameterName = "@SortDirc";
                paramSortDir.Value = sortDir;
                dbCommand.Parameters.Add(paramSortDir);

                DbParameter paramIsDownload = dbCommand.CreateParameter();
                paramIsDownload.ParameterName = "@IsDownload";
                paramIsDownload.Value = isDownload;
                dbCommand.Parameters.Add(paramIsDownload);
                dbCommand.CommandTimeout = timeout;
                sqlConnection.Open();
                DbDataReader reader = dbCommand.ExecuteReader();

                GetReportData_usp_Result data;
                while (reader.Read())
                {
                    data = new GetReportData_usp_Result();
                    for (int i = 0; i < columnsArray.Length; i++)
                    {
                        data[columnsArray[i]] = Convert.ToString(reader[columnsArray[i]], CultureInfo.InvariantCulture);
                    }

                    result.Add(data);
                }

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        totalRows = (int)reader[0];
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                throw;
            }
			finally
			{
				if(dbCommand != null && dbCommand.Connection.State != ConnectionState.Closed)
				{
					dbCommand.Connection.Close();
				}
			}
			
        }

        /// <summary>
        /// Get list of product name and id
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// /// <param name="userId">User identifier</param>
        /// <returns>Return list of products</returns>
        public List<GetProductsOnCustomerId_Result> GetProductsOnCustomerId(string customerId)
        {
            var result = new List<GetProductsOnCustomerId_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetProductsOnCustomerId(customerId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of carehome name and id
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// /// <param name="userId">User identifier</param>
        /// <returns>Return list of products</returns>
        public List<GetCareHomeOnCustomerId_Result> GetCareHomeOnCustomerId(string customerId, string userId)
        {
            var result = new List<GetCareHomeOnCustomerId_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCareHomeOnCustomerId(customerId, userId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// GetCareHomeUsersOnCustomerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<GetCareHomeUsersOnCustomerId_Result> GetCareHomeUsersOnCustomerId(string customerId)
        {
            var result = new List<GetCareHomeUsersOnCustomerId_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCareHomeUsersOnCustomerId(customerId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Clinical Contacts Value Mapping
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="roleType"></param>
        /// <returns>list object</returns>
        public List<GetClinicalContactsValueMapping_Result> GetClinicalContactsValueMapping(long customerId, long roleType)
        {
            var result = new List<GetClinicalContactsValueMapping_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetClinicalContactsValueMapping(customerId, roleType).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of Patient Customer CareHome Status
        /// </summary>
        /// <param name="userId">customerId</param>
        /// <returns>Return list of Status</returns>
        public List<GetPatientCustomerCareHomeStatus_Result> GetPatientCustomerCareHomeStatus(string statusIds)
        {
            var result = new List<GetPatientCustomerCareHomeStatus_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPatientCustomerCareHomeStatus(statusIds).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of Carehome Details For Mass Changes
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="orderType"></param>
        /// <param name="careHomeIds"></param>
        /// <returns>list of carehome details</returns>
        public List<GetCarehomeDetailsForMassChanges_Result> GetCarehomeDetailsForMassChanges(long customerId, long orderType, string careHomeIds, string careHomeStatus, long languageId)
        {
            var result = new List<GetCarehomeDetailsForMassChanges_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCarehomeDetailsForMassChanges(customerId, careHomeIds, orderType, languageId, careHomeStatus).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of patient Details For Mass Changes
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientType"></param>
        /// <param name="patientStatus"></param>
        /// <param name="patientIds"></param>
        /// <returns>list of details</returns>
        public List<GetPatientDetailsForMassDataChanges_Result> GetPatientDetailsForMassChanges(long customerId, string patientType, string patientStatus, string patientIds, long languageId)
        {
            var result = new List<GetPatientDetailsForMassDataChanges_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPatientDetailsForMassDataChanges(customerId, patientType, patientStatus, patientIds, languageId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// DownloadPrescription
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public List<DownloadPrescription_Result> DownloadPrescription(long customerId, long productId)
        {
            try
            {
                List<DownloadPrescription_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.DownloadPrescription(customerId, productId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Gets the post code matrix for mass data changes.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>list of post code matrix</returns>
        public List<GetPostCodeMatrixForMassDataChanges_Result> GetPostCodeMatrixForMassDataChanges(string customerId)
        {
            var result = new List<GetPostCodeMatrixForMassDataChanges_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPostCodeMatrixForMassDataChanges(customerId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// GetContactPersonsForMassDataChanges
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="typeId"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<GetContactPersonsForMassDataChanges_Result> GetContactPersonsForMassDataChanges(string customerId, long typeId, string ids)
        {
            var result = new List<GetContactPersonsForMassDataChanges_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetContactPersonsForMassDataChanges(customerId, typeId, ids).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the post code matrix for mass data changes.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>list of post code matrix</returns>
        public List<GetPatientClinicalContactAnalysisMassData_Result> GetPatientClinicalContactAnalysisInfo(string patientIds, string customerId, int typeId)
        {
            var result = new List<GetPatientClinicalContactAnalysisMassData_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetPatientClinicalContactAnalysisMassData(patientIds, Convert.ToInt64(customerId, CultureInfo.InvariantCulture), typeId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// CustomerClinicalAnalysisMassData
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        public List<GetCustomerClinicalAnalysisMassData_Result> CustomerClinicalAnalysisMassData(long customerId, int typeId)
        {
            try
            {
                List<GetCustomerClinicalAnalysisMassData_Result> result = null;
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCustomerClinicalAnalysisMassData(customerId, typeId).ToList();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Communication Format For Mass Data Changes
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="typeId"></param>
        /// <param name="ids"></param>
        /// <param name="languageId"></param>
        /// <returns>list of data</returns>
        public List<GetCommunicationFormatForMassDataChanges_Result> GetCommunicationFormatForMassDataChanges(long customerId, long typeId, string ids, int languageId)
        {
            var result = new List<GetCommunicationFormatForMassDataChanges_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCommunicationFormatForMassDataChanges(customerId, typeId, ids, languageId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// GetOrderDetailsForMassChanges
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ProductSAPId"></param>
        /// <param name="PatientIds"></param>
        /// <param name="CareHomeIds"></param>
        /// <param name="OrderIds"></param>
        /// <param name="PatientTypes"></param>
        /// <param name="CareHomeOrderTypes"></param>
        /// <param name="OrderTypes"></param>
        /// <param name="PatientStatus"></param>
        /// <param name="CareHomeStatus"></param>
        /// <param name="OrderStatus"></param>
        /// <param name="OrderDownloadFor"></param>
        /// <returns></returns>
        public List<DownloadOrderDataForMassUpdate_Result> GetOrderDetailsForMassChanges(long CustomerId, string ProductSAPId, string PatientIds, string CareHomeIds, string OrderIds, string PatientTypes, string CareHomeOrderTypes, string OrderTypes, string PatientStatus, string CareHomeStatus, string OrderStatus, int OrderDownloadFor)
        {

            var result = new List<DownloadOrderDataForMassUpdate_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.DownloadOrderDataForMassUpdate(CustomerId, ProductSAPId, PatientIds, CareHomeIds, OrderIds, PatientTypes, CareHomeOrderTypes, OrderTypes, PatientStatus, CareHomeStatus, OrderStatus).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// GetCustomerUserDetails
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<GetCustomerUserDetails_Result> GetCustomerUserDetails(long customerId)
        {
            var result = new List<GetCustomerUserDetails_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetCustomerUserDetails(customerId).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Delete product from IDXCustomer Product which are not assigned to customer
        /// </summary>
        /// <param name="productIds">string product Ids</param>
        /// <param name="customerId">long customer Id</param>        
        public void DeleteIDXCustomerProduct(string productIds, long customerId)
        {
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    context.DeleteIDXCustomerProduct(customerId, productIds);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Log User Login Details
        /// </summary>
        /// <param name="objLoginUserDetail">Login UserDetail</param>
        public void LogUserLoginDetails(LoginUserDetail objLoginUserDetail)
        {
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    context.SaveLoginUserDetails(objLoginUserDetail.UserId.ToString(), objLoginUserDetail.LoginDate);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Interaction Report
        /// <summary>
        /// Get Interaction Report
        /// </summary>
        /// <param name="pageNum">Page No</param>
        /// <param name="recordCount">Record Count</param>
        /// <param name="isExport">Is Exort</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="patientId">Patient Id</param>
        /// <param name="careHomeId">Care Home Id</param>
        /// <param name="interActionTypeId">InteractionType Id</param>
        /// <param name="interactionSubTypeId">Interaction Sub Type</param>
        /// <param name="statusId">Status Is</param>
        /// <param name="createdBy">Created By</param>
        /// <param name="createdDtFrom">Created Date From</param>
        /// <param name="createdDtTo">Created Date To</param>
        /// <param name="modifiedDtFrom">Modified Date From</param>
        /// <param name="modifiedDtTo">Modified Date To</param>
        /// <returns></returns>
        public List<GetInteractionReport_Result> GetInteractionReportSumDetails(Nullable<long> pageNum, Nullable<long> recordCount, Nullable<bool> isExport, string customerId, string patientId, string careHomeId, string interActionTypeId, string interactionSubTypeId, string statusId, string createdBy, Nullable<DateTime> createdDtFrom, Nullable<DateTime> createdDtTo, Nullable<DateTime> modifiedDtFrom, Nullable<DateTime> modifiedDtTo, string sortColumn, string sortby, string userId)
        {
            List<GetInteractionReport_Result> result = null;
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetInteractionReport(pageNum,
                                                          recordCount,
                                                          isExport,
                                                          customerId,
                                                          patientId,
                                                          careHomeId,
                                                          interActionTypeId,
                                                          interactionSubTypeId,
                                                          statusId,
                                                          createdBy,
                                                          createdDtFrom,
                                                          createdDtTo,
                                                          modifiedDtFrom,
                                                          modifiedDtTo,
                                                          sortColumn,
                                                          sortby,
                                                          userId).ToList();

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Users For Interaction Report
        /// </summary>
        /// <returns>List (GetInteractionReportUsers_Result) .</returns>
        public List<GetInteractionReportUsers_Result> GetInteractionReportUsers(string customerIds)
        {
            var result = new List<GetInteractionReportUsers_Result>();
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetInteractionReportUsers(customerIds).ToList();
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region Users Reset-Unlock Users
        /// <summary>
        /// Get Users For Lock-Reset
        /// </summary>
        /// <param name="pageNum">Page No</param>
        /// <param name="recordCount">Record Count</param>
        /// <param name="searchText">Search Text</param>
        /// <param name="sortColumn">Sort Column</param>
        /// <param name="sortby">Sort By</param>
        /// <returns></returns>
        public List<GetUsersToResetUnlock_Result> GetUserResetUnlockSummary(Nullable<long> pageNum, Nullable<long> recordCount, string searchText, string sortColumn, string sortby)
        {
            List<GetUsersToResetUnlock_Result> result = null;
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetUsersToResetUnlock(pageNum,
                                                            recordCount,
                                                            searchText,
                                                            sortColumn,
                                                            sortby).ToList();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Notification
        /// <summary>
        /// Check For Interaction Assigned For Notification
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<GetAlertForChangesOnHomePage_Result> CheckForNotification(string userId)
        {
            List<GetAlertForChangesOnHomePage_Result> result = null;
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetAlertForChangesOnHomePage(userId).ToList();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        /// <summary>
        /// Gets the translation based on ListTypeId
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public List<GetTextFromTranslationByListTypeId_Result> GetTextFromTranslationByListTypeId(long listTypeId, long languageId)
        {
            List<GetTextFromTranslationByListTypeId_Result> result = null;
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    result = context.GetTextFromTranslationByListTypeId(listTypeId, languageId).ToList();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
