﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Model
// Author           : mamshett
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// Last Modified By : mamshett
// Last Modified On : 03-30-2015
// ***********************************************************************
// <copyright file="UnitOfWork.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Model
{
    using System;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Data.Entity.Infrastructure;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Linq;

    public class UnitOfWork : IUnitOfWork
    {
        private SCAIndigoEntities _dataContext;

        public UnitOfWork(SCAIndigoEntities dataContext)
        {
            _dataContext = dataContext;
        }

        protected SCAIndigoEntities DataContext
        {
            get { return _dataContext; }
        }

        private void SaveAuditLog(System.Collections.Generic.List<AuditLog> auditLogs)
        {
            DBContext dbContext = null;
            try
            {
                dbContext = new DBContext();
                var auditlogFields = dbContext.AuditLogFields.ToList();

                if (auditLogs.Count > 0)
                {
                    AuditLogTransaction auditTran = new AuditLogTransaction() { ModifiedBy = auditLogs[0].ModifiedBy, ModifiedDate = auditLogs[0].ModifiedDate };
                    foreach (var item in auditLogs)
                    {
                        if (auditlogFields.Where(a => a.TableName.ToLower() == item.TableName.ToLower() && a.ColumnName.ToLower() == item.ColumnName.ToLower()).Any())
                        {
                            auditTran.AuditLogs.Add(item);
                        }
                    }
					dbContext.DataContext.AuditLogTransactions.Add(auditTran);
					dbContext.DataContext.SaveChangesAsync();
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName, validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            finally
            {
                if (dbContext != null)
                {
                    dbContext.Dispose();
                }
            }
        }

        private List<AuditLog> ConvertEntityToAuditLogModel(DbEntityEntry entity, EntityState entityState)
        {
            System.Collections.Generic.List<AuditLog> auditLogs = new System.Collections.Generic.List<AuditLog>();
            var entityType = ObjectContext.GetObjectType(entity.Entity.GetType());
            var entityName = entityType.Name;
            var primaryKeyType = string.Empty;
            var primarykeyValue = string.Empty;
            var modifiedBy = string.Empty;
            DateTime modifiedDate = new DateTime();
            var createdBy = string.Empty;
            DateTime createdDate = new DateTime();
            foreach (System.Reflection.PropertyInfo property in entityType.GetProperties())
            {
                foreach (var custom in property.CustomAttributes)
                {
                    if (custom.AttributeType.Name == "KeyAttribute")
                    {
                        primaryKeyType = property.Name;
                    }
                }
            }

            foreach (var propName in entity.CurrentValues.PropertyNames)
            {

                var property = entity.Property(propName);

                var curr = entity.CurrentValues[propName];
                var original = entity.OriginalValues[propName];

                if (propName.Equals(primaryKeyType))
                {
                    primarykeyValue = Convert.ToString(curr);
                }
                else if (propName.ToLower().Equals("modifiedby"))
                {
                    modifiedBy = Convert.ToString(curr);
                }
                else if (propName.ToLower().Equals("modifieddate"))
                {
                    modifiedDate = Convert.ToDateTime(curr);
                }
                else if (propName.ToLower().Equals("createdby"))
                {
                    createdBy = Convert.ToString(curr);
                }
                else if (propName.ToLower().Equals("createddate"))
                {
                    createdDate = Convert.ToDateTime(curr);
                }

                if (entityState == EntityState.Modified && Convert.ToString(curr) != Convert.ToString(original))
                {                   
                    var log = new AuditLog()
                    {
                        Action = entityState.ToString(),
                        PrimaryKeyType = primaryKeyType,
                        PrimaryKey = primarykeyValue,
                        TableName = entityName,
                        ColumnName = propName,
                        OldValue = Convert.ToString(original),
                        NewValue = Convert.ToString(curr)
                    };
                    auditLogs.Add(log);
                }
                else if (entityState == System.Data.Entity.EntityState.Added)
                {
                    var addedValue = Convert.ToString(curr);
                    if (!string.IsNullOrEmpty(addedValue))
                    {
                        var log = new AuditLog()
                        {
                            Action = entityState.ToString(),
                            PrimaryKeyType = primaryKeyType,
                            PrimaryKey = primarykeyValue,
                            TableName = entityName,
                            ColumnName = propName,
                            OldValue = string.Empty,
                            NewValue = Convert.ToString(curr)
                        };
                        auditLogs.Add(log);
                    }
                }
            }
            auditLogs.ForEach(a =>
            {
                a.ModifiedBy = Guid.Parse(string.IsNullOrEmpty(modifiedBy) ? (string.IsNullOrEmpty(createdBy) ? default(Guid).ToString() : createdBy) : modifiedBy);
                a.ModifiedDate = modifiedDate == null || modifiedDate == default(DateTime) ? (createdDate == default(DateTime) ? DateTime.Now : createdDate) : modifiedDate;
            });

            return auditLogs;
        }

		public void Commit()
		{
			Commit(string.Empty);
		}

        public void Commit(string remark)
        {
            try
            {
                System.Collections.Generic.List<AuditLog> auditLogs = new System.Collections.Generic.List<AuditLog>();                                           
                            
                var changedEntries = new List<DbEntityEntry>();
                var addedEntries = new List<DbEntityEntry>();                
                if (DataContext.ChangeTracker.HasChanges())                
                {
                    
                    var entries = DataContext.ChangeTracker.Entries();
                    foreach (var entry in entries)
                    {
                        if (entry.State == System.Data.Entity.EntityState.Modified)
                        {                                                     
                            changedEntries.Add(entry);
                        }
                        else if(entry.State == System.Data.Entity.EntityState.Added)
                        {                            
                            addedEntries.Add(entry);
                        }
                    }                    
                }

                foreach (var entity in changedEntries)
                {
                    auditLogs.AddRange(ConvertEntityToAuditLogModel(entity, EntityState.Modified));
                }

                DataContext.SaveChanges();
                
                foreach (var entity in addedEntries)
                {
                    auditLogs.AddRange(ConvertEntityToAuditLogModel(entity, EntityState.Added));
                }
                if (auditLogs.Count > 0)
                {
                    if(!string.IsNullOrEmpty(remark))
						auditLogs.ForEach(a => a.Remarks = remark);
                    SaveAuditLog(auditLogs);
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName, validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
        }
    }

    /// <summary>
    /// IUnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Commit
        /// </summary>
        /// <param name="remark"></param>
        void Commit(string remark);

        /// <summary>
        /// Commit
        /// </summary>
		void Commit();
    }
}
