//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCA.Indigo.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public partial class GetSAPCustomerPatientList_Result
    {
        public string CustomerType { get; set; }
        public string SAPCustomerNumber { get; set; }
        public string SalesOrg { get; set; }
        public string DistrChanel { get; set; }
        public string Division { get; set; }
        public string CareHomeId { get; set; }
        public string CareHomeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HouseNumber { get; set; }
        public string STREET { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string TelephoneNumber { get; set; }
        public string DeliveryFrequency { get; set; }
        public string RoundId { get; set; }
        public string LDDT { get; set; }
        public string NDDT { get; set; }
        public string DELINST { get; set; }
        public long FrontEndId { get; set; }
        public string SAPId { get; set; }
        public string PatientStatus { get; set; }
        public string PatientType { get; set; }
        public string Title { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Round { get; set; }
        public string SEQUENCE { get; set; }
        public string ADP { get; set; }
        public string Name { get; set; }
    }
}
