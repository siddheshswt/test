// ***********************************************************************
// Assembly         : SCA.Indigo.Model
// Author           : syerva
// Created          : 12-16-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="DBContext.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Model
{
    using System.Data.Entity;

    public class DBContext : Repository
    {
        public DbSet<Address> Addresses
        {
            get { return DataContext.Set<Address>(); }            
        }

        public DbSet<CareHome> CareHomes
        {
            get { return DataContext.Set<CareHome>(); }
        }

        public DbSet<ContactPerson> ContactPersons
        {
            get { return DataContext.Set<ContactPerson>(); }
        }

        public DbSet<Customer> Customers
        {
            get { return DataContext.Set<Customer>(); }
        }

        public DbSet<Interaction> Interactions
        {
            get { return DataContext.Set<Interaction>(); }
        }

        public DbSet<PasswordHistory> PasswordHistories
        {
            get { return DataContext.Set<PasswordHistory>(); }
        }

        public DbSet<CustomerMedicalStaffAnalysis> CustomerMedicalStaffAnalysis
        {
            get { return DataContext.Set<CustomerMedicalStaffAnalysis>(); }
        }

        public DbSet<IDXPatientMedicalStaff> IDXPatientMedicalStaffs
        {
            get { return DataContext.Set<IDXPatientMedicalStaff>(); }
        }       

        public DbSet<IDXPrescriptionProduct> IDXPrescriptionProducts
        {
            get { return DataContext.Set<IDXPrescriptionProduct>(); }
        }

        public DbSet<IDXRoleMenu> IDXRoleMenus
        {
            get { return DataContext.Set<IDXRoleMenu>(); }
        }

        public DbSet<IDXUserCustomer> IDXUserCustomers
        {
            get { return DataContext.Set<IDXUserCustomer>(); }
        }

        public DbSet<IDXUserRole> IDXUserRoles
        {
            get { return DataContext.Set<IDXUserRole>(); }
        }

        public DbSet<List> Lists
        {
            get { return DataContext.Set<List>(); }
        }

        public DbSet<ListType> ListTypes
        {
            get { return DataContext.Set<ListType>(); }
        }

        public DbSet<Order> Orders
        {
            get { return DataContext.Set<Order>(); }
        }

        public DbSet<OrderDisclaimer>OrderDisclaimers
        {
            get { return DataContext.Set<OrderDisclaimer>(); }
        }

        public DbSet<Patient> Patients
        {
            get { return DataContext.Set<Patient>(); }
        }

        public DbSet<PatientNote> PatientNotes
        {
            get { return DataContext.Set<PatientNote>(); }
        }

        public DbSet<PersonalInformation> PersonalInformations
        {
            get { return DataContext.Set<PersonalInformation>(); }
        }

        public DbSet<Prescription> Prescriptions
        {
            get { return DataContext.Set<Prescription>(); }
        }

        public DbSet<Product> Products
        {
            get { return DataContext.Set<Product>(); }
        }

        public DbSet<Role> Roles
        {
            get { return DataContext.Set<Role>(); }
        }

        public DbSet<Translation> Translations
        {
            get { return DataContext.Set<Translation>(); }
        }

        public DbSet<IDXPatientContactPerson> IDXPatientContactPersons
        {
            get { return DataContext.Set<IDXPatientContactPerson>(); }
        }

        public DbSet<Users> Users
        {
            get { return DataContext.Set<Users>(); }
        }

        public DbSet<IDXCustomerMedicalStaffAnalysisInfo> IdxCustomerMedicalStaffAnalysisInfo
        {
            get { return DataContext.Set<IDXCustomerMedicalStaffAnalysisInfo>(); }
        }

        public DbSet<IDXOrderProduct> IDXOrderProducts
        {
            get { return DataContext.Set<IDXOrderProduct>(); }
        }

        public DbSet<IDXCustomerProduct> IDXCustomerProducts
        {
            get { return DataContext.Set<IDXCustomerProduct>(); }
        }

        public DbSet<IDXCustomerContactPerson> IDXCustomerContactPersons
        {
            get { return DataContext.Set<IDXCustomerContactPerson>(); }
        }

        public DbSet<IDXCareHomeContactPerson> IDXCareHomeContactPersons
        {
            get { return DataContext.Set<IDXCareHomeContactPerson>(); }
        }

        public DbSet<CustomerParameter> CustomerParameters
        {
            get { return DataContext.Set<CustomerParameter>(); }
        }

        public DbSet<HolidayList> HolidayLists
        {
            get { return DataContext.Set<HolidayList>(); }
        }

        public DbSet<OrderNote> OrderNotes
        {
            get { return DataContext.Set<OrderNote>(); }
        }

        public DbSet<SampleProduct> SampleProducts
        {
            get { return DataContext.Set<SampleProduct>(); }
        }

        public DbSet<IDXCustomerPatientType> IDXCustomerPatientType
        {
            get { return DataContext.Set<IDXCustomerPatientType>(); }
        }

        public DbSet<SamplePatient> SamplePatients
        {
            get { return DataContext.Set<SamplePatient>(); }
        }

        public DbSet<PostcodeMatrix> PostcodeMatrix
        {
            get { return DataContext.Set<PostcodeMatrix>(); }
        }

        public DbSet<PatientTypeMatrix> PatientTypeMatrix
        {
            get { return DataContext.Set<PatientTypeMatrix>(); }
        }

        public DbSet<IDXPatientTypeMatrix> IDXPatientTypeMatrix
        {
            get { return DataContext.Set<IDXPatientTypeMatrix>(); }
        }

        public DbSet<PrescriptionNote> PrescriptionNotes
        {
            get { return DataContext.Set<PrescriptionNote>(); }
        }

        public DbSet<PrescriptionExtended> ExtendedPrescriptions
        {
            get { return DataContext.Set<PrescriptionExtended>(); }
        }

        public DbSet<HolidayProcessAdmin> HolidayProcessAdmins
        {
            get { return DataContext.Set<HolidayProcessAdmin>(); }
        }

        public DbSet<AuditLog> AuditLogs
        {
            get { return DataContext.Set<AuditLog>(); }
        }

        public DbSet<IDXCustomerReasonCodes> IDXCustomerReasonCodes
        {
            get { return DataContext.Set<IDXCustomerReasonCodes>(); }
        }

        public DbSet<ClinicalContactValueMapping> ClinicalContactValueMappings
        {
            get { return DataContext.Set<ClinicalContactValueMapping>(); }
        }

        public DbSet<ClinicalContactRoleMapping> ClinicalContactRoleMappings
        {
            get { return DataContext.Set<ClinicalContactRoleMapping>(); }
        }

        public DbSet<Note> Notes
        {
            get { return DataContext.Set<Note>(); }
        }

        public DbSet<Attachment> Attachments
        {
            get { return DataContext.Set<Attachment>(); }
        }

        public DbSet<ClinicalContactAnalysisMaster> ClinicalContactAnalysisMasters
        {
            get { return DataContext.Set<ClinicalContactAnalysisMaster>(); }
        }

        public DbSet<ReportBuilderFormat> ReportBuilderFormat
        {
            get { return DataContext.Set<ReportBuilderFormat>(); }
        }

        public DbSet<ClinicalValueMaster> ClinicalValueMasters
        {
            get { return DataContext.Set<ClinicalValueMaster>(); }
        }

        public DbSet<AuditLogField> AuditLogFields
        {
            get { return DataContext.Set<AuditLogField>(); }
        }

        public DbSet<Country> Countries
        {
            get { return DataContext.Set<Country>(); }
        }

        public DbSet<CountyList> CountyLists
        {
            get { return DataContext.Set<CountyList>(); }
        }


        public DbSet<IDXAuthorisedUserPrescription> IDXAuthorisedUserPrescriptions
        {
            get { return DataContext.Set<IDXAuthorisedUserPrescription>(); }
        }

        public DbSet<MassDataLog> MassDataLogs
        {
            get { return DataContext.Set<MassDataLog>(); }
        }

        public DbSet<LoginUserDetail> LoginUserDetails
        {
            get { return DataContext.Set<LoginUserDetail>(); }
        }

        public DbSet<UserActionMonitor> UserActionMonitor
        {
            get { return DataContext.Set<UserActionMonitor>(); }
        }

        public DbSet<IDXUserCarehome> IDXUserCarehome
        {
            get { return DataContext.Set<IDXUserCarehome>(); }
        }

		public DbSet<Google> Google
		{
			get { return DataContext.Set<Google>(); }
		}
    }
}