﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCA.Indigo.Common.Enums;

namespace SCA.Indigo.Common
{
    /// <summary>
    /// Common Constants
    /// </summary>
    public static class CommonConstants
    {
        #region Interaciton
        /// <summary>
        /// The new line character
        /// </summary>
        public const string NewLineCharacter = " \n";
        /// <summary>
        /// The date time format1
        /// </summary>
        public const string DateTimeFormat1 = "yyyyMMddhhmmss";
        
        /// <summary>
        /// The date time format2
        /// </summary>
        public const string DateTimeFormat2 = "dd/MM/yyyy HH:mm:ss";

        /// <summary>
        /// The date format DD/MM/yyyy
        /// </summary>
        public const string DateFormat1 = "dd/MM/yyyy";

        /// <summary>
        /// Date Format in MM/dd/yyyy
        /// </summary>
        public const string DateFormat2 = "MM/dd/yyyy";

        // Interaction Constants for Description
        /// <summary>
        /// The interaction notes
        /// </summary>
        public const string InteractionNotes = "Notes: ";
        /// <summary>
        /// The interaction product identifier
        /// </summary>
        public const string InteractionProductId = "Product Id: {0}";
        /// <summary>
        /// The interaction description
        /// </summary>
        public const string InteractionDescription = "Product Description: {0}";
        /// <summary>
        /// The interaction quantity
        /// </summary>
        public const string InteractionQuantity = "Quantity: {0}";
        /// <summary>
        /// The interaction automatic description
        /// </summary>
        public const string InteractionAutomaticDescription = "{0} order activated for delivery on  {1}.";

        /// <summary>
        /// Audit log remark for Patient Removed from Community/Hospital order
        /// </summary>
        public const string ALRmkPatientRemovedFrmCommunityOrder = "Removed From Order Activation";

        /// <summary>
        /// The foc
        /// </summary>
        public const string FOC = " FOC";

        #endregion

        #region MassUpdate Messages

        /// <summary>
		/// Invalid SAP Patient ID - Mass Update
		/// </summary>
		public const string MassMsgInvalidSapPatientId = "Invalid SAP Patient Id.";

		/// <summary>
		/// New Product not found in the master list - Mass Update
		/// </summary>
		public const string MassChangesNewProductNotFound = "New Product code is not found in the master product list";

		/// <summary>
		/// Old Product not found in the master list - Mass Update
		/// </summary>
		public const string MassChangesOldProductNotFound = "Old Product code is not found in the master product list";
		
		/// <summary>
		/// New Product and Old Product SAP Id cannot be same - Mass Update
		/// </summary>
		public const string MassChangesSAPProductIDSame = "Old Product SAP Id and New Product SAP Id cannot be same";
			
		/// <summary>
		/// No matching Patient Prescription Product - Mass Update
		/// </summary>
		public const string MassChangesNoMatchingPrescriptionProduct = "No matching Patient Prescription Product found to update";

		/// <summary>
		/// Duplicate New Product - Mass Update
		/// </summary>
		public const string MassChangesDuplicateNewProduct = "New Product is already existing in the Prescription.";

		/// <summary>
		/// New Product and Old Product SAP Id cannot be same - Mass Update
		/// </summary>
		public const string MassChangesHasActiveOrder = "Patient has an active order and cannot update the prescription since Count Changed is marked to Yes";

		/// <summary>
		/// New Product and Old Product SAP Id cannot be same - Mass Update
		/// </summary>
		public const string MassChangesProductNotMappedToCustomer = "Product is not mapped to the customer.";

		/// <summary>
		/// Invalid 'Valid From Date' - Mass Update
		/// </summary>
		public const string MassChangesInvalidFromDate= "Invalid 'Valid From Date' for the product";

		/// <summary>
		/// Invalid 'Valid From Date' - Mass Update
		/// </summary>
		public const string MassChangesInvalidToDate = "Invalid 'Valid To Date' for the product";        
		
		#endregion

        #region Mass Data Import Message 

        /// <summary>
        /// The post code matrix exists
        /// </summary>
        public const string PostCodeMatrixExists = "Falied to Import, Postcode record already exists.";

        /// <summary>
        /// The new post code matrix
        /// </summary>
        public const string NewPostCodeMatrix = "Falied to Change, Postcode record does not exists.";

        /// <summary>
        /// The yes
        /// </summary>
        public const string Yes = "Y";

        /// <summary>
        /// The no
        /// </summary>
        public const string No = "N";

        /// <summary>
        /// The success
        /// </summary>
        public const string Success = "Success";

        public const string TransactionStatus = "TransactionStatus";

        /// <summary>
        /// The error message
        /// </summary>
        public const string ErrorMessage = "ErrorMessage";

        /// <summary>
        /// The failed
        /// </summary>
        public const string Failed = "Failed";

        /// <summary>
        /// The mass data log file
        /// </summary>
        public const string MassDataLogFile = "MassDataLogFile";

        /// <summary>
        /// The mass import log file
        /// </summary>
        public const string MassImportLogFile = "MassImportLogFile";

        /// <summary>
        /// The mass change log file
        /// </summary>
        public const string MassChangeLogFile = "MassChangeLogFile";

        /// <summary>
        /// Sap Id length
        /// </summary>
        public const int SapIdLength = 10;

        /// <summary>
        /// The mass data invalid yes no message
        /// </summary>
        public const string MassDataInvalidYesNoMessage = "For Column {0} the values should be either Y or N";

        public const string DifferentCustomer = "This customer is other than first customer";
        public const string InvalidCustomerId = "Customer Id [{0}] is not valid";
        public const string InvalidCarehomeId = "Carehome Id [{0}] is not valid";
        public const string InvalidPatientsId = "Patient Id [{0}] is not valid";
        public const string CarehomeIdNotExists = "Carehome Id [{0}] does not exists";
        public const string InvalidCarehomeStatus = "Carehome Status [{0}] is not valid";
        public const string InvalidSendToSap = "Send To SAP [{0}] is not valid";
        public const string InvalidCarehomeType = "Carehome Type [{0}] is not valid";
        public const string InvalidOrderType = "Order Type [{0}] is not valid";
        public const string InvalidCountryCode = "Country [{0}] is not valid";
        public const string PatinetNotExists = "Patient Id [{0}] not exists";
        public const string PatinetStatusNotExists = "Patient Status [{0}] not exists";
        public const string DuplicateLoadId = "Load ID [{0}] already exists in excel";
        public const string DuplicateSAPPatientIdinExcel = "SAP Patient ID [{0}] already exists in excel";
        public const string DuplicateSAPPatientIdinDatabase = "SAP Patient ID [{0}] already exists in database";
        public const string DatabaseLoadId = "Load ID [{0}] already exists in database";
        public const string PatinetTypeNotExists = "Patient Type [{0}] not exists";
        public const string InvalidLoadId = "Invalid LoadId";
        public const string InvalidPatientId = "Invalid PatientId";        
        public const string InvalidCountyCode = "County [{0}] is not valid";
        public const string InvalidReasonCode = "Invalid [{0}] ReasonCode";
        public const string TitleNotExists = "Title [{0}] not exists";
        public const string InvalidGender = "Invalid Gender [{0}] ";
        public const string InvalidCounty = "Invalid County [{0}] ";
        public const string InvalidEmail = "Invalid Email [{0}] "; 
        public const string InvalidPatientType = "Invalid Patient Type [{0}]";
        public const string PatientTypeChangeNotAllowed  = "Patient Type change is not allowed from mass update";
        public const string CustomerParameternotfound = "Customer Parameter Not Defined for customerId-";
        public const string InvalidLength = "Invalid Length for [{0}].";
        public const string InvalidNumber = "[{0}] should be Numeric.";        
        public const string InvalidBlank = "[{0}] should be blank.";
        public const string InvalidNextDeliveryDate = "Invalid NextDeliveryDate";
        public const string SmallerNextDeliveryDate = "NextDeliveryDate should not be smaller than current date";
        public const string DobGreaterThanCurrentDate = "Date of borth should not be greater than current date";
        public const string DobGreaterThanAssessmentDate = "Date of borth should not be greater than assessment date";
        public const string NextAssessmentlessThanAssessmentDate = "NextAssessment Date should not be less than assessment date";
        public const string InvalidRoundFormat = "Invalid Round formate[{0}]";
        public const string InvalidRound = "First 8 characters of Round should be GBHD0000 and last two characters should be any digit";
        public const string InvalidSapPatientId = "Invalid SAP PatientId";
        public const string InvalidSapCareHomeId = "Invalid SAPCarehomeId";
        
        #endregion

        #region Clinical Contact / Analysis Info

        public const string INVALID_CUSTOMER = " Invalid Customer";
        public const string INVALID_PATIENT = " Invalid Patient";
        public const string CLINICALANALYSIS_ALREADY_EXIST = " Clinical Contact / Analysis: [{0}] already exist";
        public const string INVALID_CLINICALANALYSIS_TYPE = " Invalid Clinical Contact / Analysis Type: [{0}]";
        public const string INVALID_CLINICALANALYSIS_VALUE_TYPE = " Invalid Clinical Contact / Analysis value Type: [{0}]";
        public const string CLINICALANALYSIS_TYPE_ALREADY_EXIST = " Clinical Contact TypeId: [{0}] already exist";
        public const string INVALID_CONTACT_NAME_FOR_NEW_CONTACT = " Existing Clinical value ID [{0}] cannot be associated with other Clinical contact or Analysis Type";
        public const string MANDATORY_FIELDS_MISSING_KEYWORD = " Mandatory Fields Missing: ";
        public const string INVALID_DATA_TYPE_KEYWORD = " Invalid Data Type: ";
        public const string INACTIVE_CLINICAL_CONTACT = "The Clinical Contact [{0}] is in InActive state, cannot attach a value to it";
        public const string Comma = ",";
        public const string InActiveClinicalContctType = " The Clinical Contact / Analysis Info [{0}] is in InActive state, cannot attach a value to it";

        #endregion

        #region ContactPerson
        public const string ContactPersonAll = "Invalid entry - All CustomerId, PatientId or CarehomeId should not be present at a time.";
        public const string ContactPersonNotAll = "Invalid entry - Either of CustomerId, PatientId or CarehomeId should be present.";
        public const string InvalidPatientCarehome = "Invalid entry for PatientId and CarehomeId.";
        public const string InvalidCustomerCarehome = "Invalid entry for CustomerId and CarehomeId.";
        public const string InvalidPatientCustomer = "Invalid entry for CustomerId and PatientId.";
        public const string InvalidContactPersonId = "Contact Person Id [{0}] is not exist";

        public const string ContactPersonId = "ContactPersonId";
        public const string Phone = "Phone";
        public const string Customer = "Customer";
        public const string Patient = "Patient";
        public const string Carehome = "Carehome";
        public const string Mobile = "Mobile";        

        #endregion 

        #region CommunicationFormat
        public const string InvalidCommunication = "Invalid Communication preference.";
        public const string InvalidMarketting = "Invalid Marketting preference."; 
        #endregion

        #region DateFormat
        public const string DateFormat = "yyyyMMdd";
        public const string DateTimeFormatyyMMddhhmmss = "yyyyMMddhhmmss";
        public const string DateFormatddMMyyyy = "dd/MM/yyyy";
        public const string DateFormatddMyyyy = "dd/M/yyyy";
        public const string DateFormatdMMyyyy = "d/MM/yyyy";
        public const string DateFormatdMyyyy = "d/M/yyyy";
		public const string DateFormatyyyyMMdd = "yyyy/MM/dd";
        #endregion

        #region Prescription
        public const string PrescriptionUpdateTemplateFilePath = "PrescriptionUpdateTemplate";
        public const string PrescriptionImportTemplateFilePath = "PrescriptionImportTemplate";
        #endregion

        // seperator
        public const string Underscore = "_";
        public const string PipeSeparator = "|";
        public const string Astrik = " *";
        public const string ExcelFileFormat = ".xlsx";
        public const string XMLFileFormat = ".xml";

        public const Int64 SCAAdministratorRoleId = 1;
        public const string SystemUser = "00000000-0000-0000-0000-000000000000";
        public const string SAPUser = "ECDBCD8F-145A-411A-9FBD-98CC5D2F0786";
    }
}
