﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Common
{
    public class SCALogger
    {
        static SCALogger()
        {
            var db = new DatabaseProviderFactory().Create("SCAIndigoEnterpriseConn");
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());

            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();

            if (configurationSource.GetSection(LoggingSettings.SectionName) != null)
            {                
                    LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);                
                    Logger.SetLogWriter(logWriterFactory.Create());                
            }
        }

        //Logging actions in database
        public static void LoggAction(string message, string controllerName, string actionName, string userId)
        {
            try
            {
                if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(controllerName) ||
                    string.IsNullOrEmpty(actionName)) return;

                var extendedProperties = new Dictionary<string, object>();
                extendedProperties["ActionName"] = actionName;
                extendedProperties["Controller"] = controllerName;
                extendedProperties["UserID"] = userId;

                var logWriterFactory = new LogWriterFactory();
                var logger = logWriterFactory.Create();
                var logEntry = new LogEntry
                {
                    Title = "UserAction",
                    EventId = 100,
                    Priority = 2,
                    Message = "Informational message",
                    ExtendedProperties = extendedProperties
                };
                logEntry.Categories.Add("SCALogging");
                logger.Write(logEntry);
            }
            catch (Exception)
            {

                throw;
            }

        }
        
        //for logging exception in database and event log
        public static void LoggException(Exception e, string userId)
        {
            try
            {
                if (e.InnerException != null)
                    e = e.InnerException;
                IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
                e.Data.Add("UserID", userId);
                ExceptionPolicyFactory exceptionFactory = new ExceptionPolicyFactory(configurationSource);
                ExceptionManager manager = exceptionFactory.CreateManager();
                manager.HandleException(e, "ExceptionPolicy");                
            }
            catch(Exception ex)
            {
                // Empty block to enable Exception logging from Enterprise library                
            }
        }        
    }
}
