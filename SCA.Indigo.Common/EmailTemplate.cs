﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Common
{
   public static class EmailTemplate
    {
       /// <summary>
       /// Email from
       /// </summary>
       public const string EmailFrom = "noreply@sca.com";

       /// <summary>
       /// The email subject
       /// </summary>
       public const string OnlineTelephoneSubject = "Order Activated Scuccessfully";

       /// <summary>
       /// 
       /// </summary>
       public const string OnlineTelephoneEmailBody = " Dear {0}, \r\n\r\n Your order for Delivery Date {1} has been activated successfully. \r\n\r\n\r\n Regards,\r\n SCA Team";

       /// <summary>
       /// Interaction Assigned Mail Subject
       /// </summary>
       public const string InteractionAssignedSubject = "Interaction Assigned – Action Required – {0}";

       /// <summary>
       /// Interaction Assigned Email Body for Patient
       /// </summary>
       public const string InteractionAssignedEmailBody = "Dear Indigo User,\r\n\r\nA new interaction has been assigned to {0}.\r\n{1}\r\nKind Regards,\r\nIndigo Support Team";

       /// <summary>
       /// Interaction Patient Id
       /// </summary>
       public const string InteractionPatientId = "\r\nPatient Id: {0}\r\n";

       /// <summary>
       /// Interaction CareHome Id
       /// </summary>
       public const string InteractionCareHomeId = "\r\nCarehome Id: {0}\r\n";
    }
}
