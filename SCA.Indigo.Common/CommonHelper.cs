﻿// *********************************************************************** // TODO: Fix - All using directives must be placed inside of the namespace.
// Assembly         : SCA.Indigo.Common
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 12-18-2014
// ***********************************************************************
// <copyright file="CommonHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Common
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;    
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Security.Cryptography;
    using System.Text;
    using System.Linq;
	using System.Text.RegularExpressions;
    using SCA.Indigo.Common.Enums;

    /// <summary>
    /// Common Helper
    /// </summary>
    public static class CommonHelper
    {
        private static int fromYear = 1900;
        public static int FromYear { get { return fromYear; } set { fromYear = value; } }

        /// <summary>
        /// Decrypts the specified text to be decrypted.
        /// </summary>
        /// <param name="textToBeDecrypted">The text to be decrypted.</param>
        /// <returns></returns>
        public static string Decrypt(string textToBeDecrypted)
        {
            string decryptedData = textToBeDecrypted;
            using (var rijndaelCipher = new RijndaelManaged())
            {
                string password = ConfigurationManager.AppSettings["EncryptionPassword"];
                string saltString = ConfigurationManager.AppSettings["EncryptionSalt"];
                byte[] salt = System.Text.Encoding.ASCII.GetBytes(saltString);                
                var secretKey = new PasswordDeriveBytes(password, salt);
                try
                {
                    var encryptedData = Convert.FromBase64String(textToBeDecrypted);

                    var decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));
                    using (var memoryStream = new MemoryStream(encryptedData))
                    {
                        var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                        var plainText = new byte[encryptedData.Length];
                        var decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                        decryptedData = Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                    }
                }
                catch (ObjectDisposedException e)
                {
                    LogHelper.LogException(e);
                }
                catch
                {
                    throw;
                }
                finally
                {                  
                    if (secretKey != null)
                    {
                        secretKey.Dispose();
                }
            }
            }

            return decryptedData;
        }

        /// <summary>
        /// Encrypts the specified text to be encrypted.
        /// </summary>
        /// <param name="textToBeEncrypted">The text to be encrypted.</param>
        /// <returns></returns>
        public static string Encrypt(string textToBeEncrypted)
        {
            var encryptedData = string.Empty;
            using (var rijndaelCipher = new RijndaelManaged())
            {
                string password = ConfigurationManager.AppSettings["EncryptionPassword"];
                string saltString = ConfigurationManager.AppSettings["EncryptionSalt"];

                byte[] salt = System.Text.Encoding.ASCII.GetBytes(saltString);
                var plainText = System.Text.Encoding.Unicode.GetBytes(textToBeEncrypted);
                var secretKey = new PasswordDeriveBytes(password, salt);
                try
                {
                    var encryptor = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));
                    using (var memoryStream = new MemoryStream())
                    {
                        var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        var cipherBytes = memoryStream.ToArray();
                        encryptedData = Convert.ToBase64String(cipherBytes);
                    }
                }
                finally
                {
                    // Dispose Objects                   
                    if (secretKey != null)
                    {
                        secretKey.Dispose();
                }
            }
            }

            return encryptedData;
        }

        /// <summary>
        /// Convert the string to comma separaetd string
        /// </summary>
        /// <param name="textsToConvert">The string texts.</param>
        /// <returns>
        /// The string.
        /// </returns>
        public static string ConvertToCommaSeparatedString(string[] textsToConvert)
        {
            var sb = new StringBuilder();
            if (textsToConvert != null)
            {
                foreach (var text in textsToConvert)
                {
                    sb.Append(text).Append(',');
                }
            }

            return sb.ToString().Trim(',');
        }

        /// <summary>
        /// Convert the value to Comma separated string [overloaded method]
        /// </summary>
        /// <param name="textsToConvert">The string texts.</param>
        /// <returns>
        /// The string.
        /// </returns>
        public static string ConvertToCommaSeparatedString(long[] textsToConvert)
        {
            var sb = new StringBuilder();
            if (textsToConvert != null)
            {
                foreach (var text in textsToConvert)
                {
                    sb.Append(text).Append(',');
                }
            }

            return sb.ToString().Trim(',');
        }

        /// <summary>
        /// Function to trim the SAP Product Id [removes leading 0's and removes last 2 characters]
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns>
        /// string productId
        /// </returns>
        public static string TrimProductId(string productId)
        {
            productId = productId.TrimStart(new char[] { '0' });

            // old data may contain ProductId of length <= 2
            if (productId.Length > 2)
            {
                productId = productId.Substring(0, productId.Length - 2);
            }

            return productId;
        }


        /// <summary>
        /// Add the working days excluding Saturday and Sunday.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="noOfDays">The no of days.</param>
        /// <returns></returns>
        public static DateTime AddWorkingDays( DateTime date,int noOfDays)
        {
            //TODO: holiday list check pending
            var days = noOfDays;
            while (days != 0)
            {
                date = date.AddDays(1);               
                if (date.DayOfWeek == DayOfWeek.Saturday)
                {
                    date = date.AddDays(1);
                }
                if (date.DayOfWeek == DayOfWeek.Sunday)
                {
                    date = date.AddDays(1);
                }
                days = days - 1;
            }
            return date;           
        }

        /// <summary>
        /// Gets the next working day.
        /// </summary>
        /// <param name="holidayList">The holiday list.</param>
        /// <param name="date">The date.</param>
        /// <param name="noOfDays">The no of days.</param>
        /// <returns></returns>
        public static DateTime GetNextWorkingDay(List<DateTime> holidayList, DateTime date,int noOfDays)
        {
            date = AddWorkingDays(date, noOfDays);
            if (holidayList != null)
            {
                while (holidayList.Contains(date))
                {
                    date = AddWorkingDays(date, 1);
                }
            }

            return date; 
        }

        /// <summary>
        /// Creates a directory at provided path
        /// </summary>
        /// <param name="serverPath">The Physical path where the directory is to be created</param>
        /// <param name="uploadDirectory">Name of the directory to be created</param>
        public static void CreateUploadDirectoryIfNotExists(string serverPath, string uploadDirectory)
        {
           
            #region CreateDirectory If DoesNotExist
            try
            {

                var parentDirectory = Path.Combine(serverPath, uploadDirectory);

                if (!Directory.Exists(parentDirectory))
                {
                    Directory.CreateDirectory(parentDirectory);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            #endregion
        }

        /// <summary>
        /// Creates a directory at provided path
        /// </summary>
        /// <param name="serverPath">The Physical path where the directory is to be created</param>
        /// <param name="uploadDirectory">Name of the directory to be created</param>
        public static void CreateDirectoryIfNotExists(string serverPath)
        {

            #region CreateDirectory If DoesNotExist
            try
            {
                if (!Directory.Exists(serverPath))
                {
					Directory.CreateDirectory(serverPath);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            #endregion
        }

        /// <summary>
        /// Function to trim the leading Zero's from SAP Number
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// string with removed leading zero's
        /// </returns>
        public static string TrimLeadingZeros(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                id = id.TrimStart(new char[] { '0' });
            }            
            return id;
        }

        /// <summary>
        /// Function to trim the leading Zero's from SAP Number
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="length">The length.</param>
        /// <returns>
        /// string with removed leading zero's
        /// </returns>
        public static string AppendZeroAtStart(string id, int length)
        {
            if (!string.IsNullOrEmpty(id))
            {
                while (id.Length < length)
                {
                    id = "0" + id;
                }
            }
            return id;
        }

        /// <summary>
        /// Gets the description from enum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string GetDescriptionFromEnumValue(Enum value)
        { 
            var attribute = value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(EnumMemberAttribute), false);           
            if(attribute != null && attribute.Length > 0)
            {
                return ((EnumMemberAttribute)attribute[0]).Value;
            }
            return value.ToString();
        }

        /// <summary>
        /// Gets the Zero Appended SAP NUMBER
        /// </summary>
        /// <param name="value">Sap ID.</param>
        /// <returns></returns>
        public static string GetAppenedSapNumber(string id)
        {
            string sapNumber = string.Empty;
            if(string.IsNullOrEmpty(id))
            {
                return string.Empty;
            }
            else
            {
                string comma = ",";
                string[] words = id.Split(',');
                words.ToList().ForEach(d =>
                {
                    if (!string.IsNullOrEmpty(d) && id.Trim().Length < 10)
                    {
                        if (string.IsNullOrEmpty(sapNumber))
                        {
                            sapNumber = d.Trim().PadLeft(Convert.ToInt16(CommonConstants.SapIdLength), '0');
                        }
                        else
                        {
                            sapNumber += comma + d.Trim().PadLeft(Convert.ToInt16(CommonConstants.SapIdLength), '0');
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sapNumber))
                        {
                            sapNumber = d.Trim();
                        }
                        else
                        {
                            sapNumber += comma + d.Trim();
                        }
                    }
                });
            }

            return sapNumber;
		}

		#region ValidadatedataTypes
		/// <summary>
		/// Check whether the data is Null or Empty - Long
		/// </summary>
		/// <param name="value"></param>
		/// <returns>True if Null/Empty</returns>
		public static bool IsNullOrEmpty(long? value)
		{
			bool isValid = true;
			if (!value.HasValue || (value.HasValue && value.Value == 0))
			{
				isValid = false;
			}
			return !isValid;
		}

		/// <summary>
		/// Check whether the data is Null or Empty - Long
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool IsNullOrEmpty(double? value)
		{
			bool isValid = true;
			if (!value.HasValue || (value.HasValue && value.Value == 0))
			{
				isValid = false;
			}
			return !isValid;
		}

		/// <summary>
		/// Validates the integer.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>True or False</returns>
		public static bool ValidateInteger(string value)
		{
            bool isValid = false;
            if (!string.IsNullOrEmpty(value))
            {
                long convertedValue;
                isValid = long.TryParse(value, out convertedValue);
            }

			return isValid;
		}

		/// <summary>
		/// Validates the date.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <returns>
		/// True or False
		/// </returns>
		public static bool ValidateDate(string date)
		{
			DateTime newDate;
            return ValidateDate(date,out newDate);
		}

        /// <summary>
        /// Validates the date.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>True or False</returns>
        public static bool ValidateDate(string date, out DateTime convertedDate)
        {        
            bool isValid = false;
            int fromYear = CommonHelper.FromYear;
            string[] dateParts = date.Split('/');
            convertedDate = default(DateTime);
            if (dateParts.Length == 3)
            {
                if (dateParts[2].Length >= 4)
                {
                    date = dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2].Substring(0, 4);
                }
                else
                {
                    date = dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2];
                }

                string[] formats = { CommonConstants.DateFormatddMMyyyy, CommonConstants.DateFormatddMyyyy, CommonConstants.DateFormatdMMyyyy, CommonConstants.DateFormatdMyyyy };
                isValid = DateTime.TryParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out convertedDate);
                if (isValid && convertedDate.Year < fromYear)
                {
                    isValid = false;
                }
            }

            return isValid;
           
        }

		/// <summary>
		/// Vaildates the boolean.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True or False
		/// </returns>
		public static bool VaildateBoolean(string value)
		{
			bool convertedValue;
			var isValid = bool.TryParse(value, out convertedValue);
			return isValid;
		}


		/// <summary>
		/// Validates the email.
		/// </summary>
		/// <param name="email">The email.</param>
		/// <returns>
		/// True or False
		/// </returns>
		public static bool ValidateEmail(string email)
		{
			if (!string.IsNullOrEmpty(email))
			{
				string regularExpression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
				var regex = new Regex(regularExpression);
				if (regex.IsMatch(email))
					return true;
				else
					return false;
			}
			else
			{
				return true;
			}
		}

		/// <summary>
		/// Validates the datetime string to the exact format, with out parameter
		/// </summary>
		/// <param name="date">Datetime string to validate</param>
		/// <param name="dateTimeFormat">Format of the datetime string</param>
		/// <param name="convertedDate">out parameter</param>
		/// <returns>true/false</returns>
		public static bool IsValidDateTimeString(string date, out DateTime convertedDate)
		{
			var isValid = true;
            int fromYear = CommonHelper.FromYear;
			convertedDate = default(DateTime);
			if (string.IsNullOrEmpty(date))
			{
				isValid = false;				
			}
			else
			{
                string[] dateParts = date.Split('/');
                if (dateParts.Length == 3)
                {
                    if (dateParts[2].Length >= 4)
                    {
                        date = dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2].Substring(0, 4);
                    }
                    else
                    {
                        date = dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2];
                    }
                    string[] dateTimeFormats = { CommonConstants.DateFormatddMMyyyy, CommonConstants.DateFormatddMyyyy, CommonConstants.DateFormatdMMyyyy, CommonConstants.DateFormatdMyyyy, CommonConstants.DateFormatyyyyMMdd };
                    isValid = DateTime.TryParseExact(date, dateTimeFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out convertedDate);
                    convertedDate = convertedDate.Add(DateTime.Now.TimeOfDay);
                    if (isValid && convertedDate.Year < fromYear)
                    {
                        isValid = false;
                    }
                    
                }			              
			}
			return isValid;          
		}

		#endregion

        /// <summary>
        /// Removes the attachment.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>
        /// return status of remove attachment
        /// </returns>
        public static bool RemoveFile(string filepath)
        {
            bool issuccess = false;
            string removefilepath = filepath;
            try
            {
                if (System.IO.File.Exists(removefilepath))
                {
                    System.IO.File.Delete(removefilepath);
                    issuccess = true;
                }
            }
            catch (Exception ex)
            {
                issuccess = false;
				LogHelper.LogException(ex);
            }
            return issuccess;
        }

        /// <summary>
        /// Gets Order Type Text for OrderType Id
        /// </summary>
        /// <param name="orderType">OrderType Id</param>
        /// <returns>Returns Order Type if provided OrderType exists, else returns empty string</returns>
        public static string GetOrderTypeText(long orderType)
        {
            var typeId = Convert.ToInt64(orderType);
            var typeText = string.Empty;

            if (typeId != default(long))
            {
                if (typeId == Convert.ToInt64(SCAEnums.OrderType.SAMP, CultureInfo.InvariantCulture))
                {
                    typeText =   GetDescriptionFromEnumValue(SCAEnums.OrderType.SAMP);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDF, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderType.ZHDF);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDH, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderType.ZHDH);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDP, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderType.ZHDP);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDR, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderType.ZHDR);
                }
            }

            return typeText;
        }



        /// <summary>
        /// Gets Order Creation Type Text for OrderType Id
        /// </summary>
        /// <param name="orderType">OrderType Id</param>
        /// <returns>Returns Order Type if provided OrderType exists, else returns empty string</returns>
        public static string GetOrderCreationTypeText(long orderCreationType)
        {
            var typeId = Convert.ToInt64(orderCreationType);
            var typeText = string.Empty;

            if (typeId != default(long))
            {
                if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CommunityOrder, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CommunityOrder);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CommunityOneOffStandard, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CommunityOneOffStandard);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomeOneOffRush, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomeOneOffRush);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CommunityOneOffFOC, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CommunityOneOffFOC);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomePatientOrder, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomePatientOrder);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomePatientOneOffStandard, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomePatientOneOffStandard);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomePatientOneOffRush, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomePatientOneOffRush);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomePatientOneOffFOC, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomePatientOneOffFOC);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomeOneOffStandard, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomeOneOffStandard);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomeOneOffRush, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomeOneOffRush);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomeOneOffFOC, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomeOneOffFOC);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.CarehomeProductOrder, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.CarehomeProductOrder);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.SampleOrder, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.SampleOrder);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.AutomaticOrder, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.AutomaticOrder);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.Telephonic, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.Telephonic);
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderCreationType.Online, CultureInfo.InvariantCulture))
                {
                    typeText = GetDescriptionFromEnumValue(SCAEnums.OrderCreationType.Online);
                }
            }

            return typeText;
        }

        /// <summary>
        /// Converts the date todd m myyyy.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// datetime object
        /// </returns>
        public static DateTime ConvertDateToMMddyyyy(string date)
        {
            DateTime newDate;
            string[] dateParts = date.Split('/');
            if (dateParts[2].Length >= 4)
            {
                date = dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2].Substring(0, 4);
            }

            string[] formats = { CommonConstants.DateFormatddMMyyyy, CommonConstants.DateFormatddMyyyy, CommonConstants.DateFormatdMMyyyy, CommonConstants.DateFormatdMyyyy };
            DateTime.TryParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate);
            return newDate;
        }
	}

        

    /// <summary>
    /// 
    /// </summary>
    public class SSOHelper
    {
        #region SSO Decrypt

        /// <summary>
        /// The error description
        /// </summary>
        public StringBuilder ErrorDescription = new StringBuilder();
        /// <summary>
        /// The key
        /// </summary>
        protected string key = ConfigurationManager.AppSettings.Get("aesKey");
        /// <summary>
        /// The iv
        /// </summary>
        protected string iv = ConfigurationManager.AppSettings.Get("aesIV");

        /// <summary>
        /// Decrypts the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        public string Decrypt(string token)
        {
            string decoded = string.Empty;
            UTF8Encoding textConverter = new UTF8Encoding();
            byte[] btoDecrypt = ConvertHexstringToByte(token);
            byte[] decrypted = null;

            System.Security.Cryptography.RijndaelManaged aes = new RijndaelManaged();
            aes.Key = ConvertHexstringToByte(key);
            aes.IV = ConvertHexstringToByte(iv);

            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;

            try
            {
                using (ICryptoTransform decryptor = aes.CreateDecryptor())
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
                {
                    decrypted = new byte[btoDecrypt.Length];
                    cs.Write(btoDecrypt, 0, btoDecrypt.Length);
                    cs.FlushFinalBlock();
                    decoded = textConverter.GetString(ms.ToArray());
                    ErrorDescription.Length = 0;
                }
            }
            catch (Exception ex)
            {
                ErrorDescription.AppendFormat(CultureInfo.InvariantCulture, "Error: {0}", ex.Message);
            }
            finally
            {
                if (aes != null)
                {
                    aes.Dispose();
                }
            }
            return decoded;
        }

        /// <summary>
        /// Converts the hexstring to byte.
        /// </summary>
        /// <param name="hex_string">The hex_string.</param>
        /// <returns></returns>
        private byte[] ConvertHexstringToByte(string hex_string)
        {
            string hexString = hex_string;
            int discarded;
            byte[] byteArray = GetBytes(hexString, out discarded);
            var tempSb = new StringBuilder();            
            for (int i = 0; i < byteArray.Length; i++)
            {
                tempSb.Append(byteArray[i].ToString("D3", CultureInfo.InvariantCulture) + " ");
            }

            hex_string = tempSb.ToString();
            return byteArray;
        }

        /// <summary>
        /// Gets the bytes.
        /// </summary>
        /// <param name="hexString">The hexadecimal string.</param>
        /// <param name="discarded">The discarded.</param>
        /// <returns></returns>
        public byte[] GetBytes(string hexString, out int discarded)
        {
            discarded = 0;

            var newString = "";
            var newStringSb = new StringBuilder();
            char c;

            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                {
                    newStringSb.Append(c);
                }
                else
                {
                    discarded++;
                }
            }

            newString = newStringSb.ToString();

            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new string(new char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }

            return bytes;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        private string ToString(byte[] bytes)
        {
            var hexStringSb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                hexStringSb.Append(bytes[i].ToString("X2", CultureInfo.InvariantCulture));
            }

            return hexStringSb.ToString();
        }

        /// <summary>
        /// Hexadecimals to byte.
        /// </summary>
        /// <param name="hex">The hexadecimal.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">hex must be 1 or 2 characters in length</exception>
        private byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
            {
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            }

            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            return newByte;
        }

        /// <summary>
        /// Determines whether [is hexadecimal digit] [the specified c].
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        public bool IsHexDigit(char c)
        {
            int numChar;
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            c = char.ToUpper(c, CultureInfo.InvariantCulture);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < (numA + 6))
            {
                return true;
            }

            if (numChar >= num1 && numChar < (num1 + 10))
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}
