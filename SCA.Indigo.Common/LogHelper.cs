﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Common
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 12-16-2014
// ***********************************************************************
// <copyright file="LogHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Common
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;

    /// <summary>
    /// Logger Helper
    /// </summary>
    public static class LogHelper
    {
        /// <summary>
        /// Initializes the <see cref="LogHelper"/> class.
        /// </summary>
        static LogHelper()
        {
            new DatabaseProviderFactory().Create("SCAIndigoEnterpriseConn"); // This initialization is required to initiate Database Factory Connection for Enterprise library
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            using (IConfigurationSource configurationSource = ConfigurationSourceFactory.Create())
            {
                if (configurationSource.GetSection(LoggingSettings.SectionName) != null)
                {
                    LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
                    Logger.SetLogWriter(logWriterFactory.Create());
                }
            }
        }

        /// <summary>
        /// Common method to Log Controller level Actions
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="userId">The user identifier.</param>
		public static void LogAction(string message, string controllerName, string actionName, string userId)
		{
			if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(controllerName) ||
				string.IsNullOrEmpty(actionName))
			{
				return;
			}

			var extendedProperties = new Dictionary<string, object>();
			extendedProperties["ActionName"] = actionName;
			extendedProperties["Controller"] = controllerName;
			extendedProperties["UserID"] = userId;

			var logWriterFactory = new LogWriterFactory();
			var logger = logWriterFactory.Create();
			var logEntry = new LogEntry
			{
				Title = "UserAction",
				EventId = 100,
				Priority = 2,
				Message = message,
				ExtendedProperties = extendedProperties
			};
			logEntry.Categories.Add("SCALogging");
			logger.Write(logEntry);
		}

        /// <summary>
        /// For logging exception in database and event log
        /// </summary>
        /// <param name="exception">The exception.</param>
        public static void LogException(Exception exception)
        {
            string userId = null;
            LogException(exception, userId);
        }

        /// <summary>
        /// For logging exception in database and event log
        /// </summary>
        /// <param name="exception">The e.</param>
        /// <param name="userId">The user identifier.</param>
		public static void LogException(Exception exception, string userId)
		{
			try
			{
				if (string.IsNullOrEmpty(userId))
				{
					userId = ConfigurationManager.AppSettings["DefaultUserId"];
				}

				if (exception != null && exception.InnerException != null)
				{
					exception = exception.InnerException;
				}

				ExceptionPolicyFactory exceptionFactory;
				using (IConfigurationSource configurationSource = ConfigurationSourceFactory.Create())
				{
					exception.Data.Add("UserID", userId);
					exceptionFactory = new ExceptionPolicyFactory(configurationSource);
				}

				var manager = exceptionFactory.CreateManager();
				manager.HandleException(exception, "ExceptionPolicy");
			}
			catch(Exception ex)
			{
				//Do not remove this empty catch. Added this for a reason.
			}
		}
    }
}
