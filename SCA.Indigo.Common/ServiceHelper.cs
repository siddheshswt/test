﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Common
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="ServiceHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Common
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;

    /// <summary>
    /// Class ServiceHelper.
    /// </summary>
    public class ServiceHelper : IDisposable
    {
        #region Class level variables
        /// <summary>
        /// The _is disposed
        /// </summary>
        private bool isDisposed;

        /// <summary>
        /// Gets or sets the object HTTP client.
        /// </summary>
        /// <value>
        /// The object HTTP client.
        /// </value>
        public HttpClient ObjHttpClient { get; set; }

        /// <summary>
        /// Gets or sets the media type formatter.
        /// </summary>
        /// <value>
        /// The media type formatter.
        /// </value>
        public MediaTypeFormatter MediaTypeFormatter { get; set; }

        /// <summary>
        /// Gets or sets the HTTP status.
        /// </summary>
        /// <value>
        /// The HTTP status.
        /// </value>
        public int HTTPStatus { get; set; }

        /// <summary>
        /// http Time Out
        /// </summary>
        private static readonly int HttpTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["HttpTimeOut"]);

        /// <summary>
        /// Gets or sets the HTTP message.
        /// </summary>
        /// <value>
        /// The HTTP message.
        /// </value>
        public string HTTPMessage { get; set; }

        /// <summary>
        /// Gets or sets the request message.
        /// </summary>
        /// <value>
        /// The request message.
        /// </value>
        public string RequestMessage { get; set; }

        /// <summary>
        /// Gets or sets the request URL.
        /// </summary>
        /// <value>
        /// The request URL.
        /// </value>
        public Uri RequestUrl { get; set; }

        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>
        /// The type of the content.
        /// </value>
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets the method.
        /// </summary>
        /// <value>
        /// The method.
        /// </value>
        public string Method { get; set; }

        /// <summary>		
        /// Gets or sets the base URL.
        /// </summary>
        /// <value>
        /// The base URL.
        /// </value>
		public Uri BaseUrl
		{
			get
			{
				var baseUrl = Convert.ToString(ConfigurationManager.AppSettings["ServiceBaseUrl"], CultureInfo.InvariantCulture);
				return new Uri(baseUrl);
			}
		}

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceHelper"/> class.
        /// </summary>
        public ServiceHelper()
        {
            ContentType = "application/json";
            this.MediaTypeFormatter = new JsonMediaTypeFormatter();

            ObjHttpClient = new HttpClient
            {
                BaseAddress = BaseUrl
            };
            ObjHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        }

        public ServiceHelper(string userName, string password)
        {
            this.UserName = userName;
            this.Password = password;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceHelper"/> class.
		/// TODO : Needs clean up, may be remove function
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        public ServiceHelper(string contentType)
        {
            ContentType = contentType;
            this.MediaTypeFormatter = new JsonMediaTypeFormatter();
            ObjHttpClient = new HttpClient
            {
                BaseAddress = BaseUrl
            };
            ObjHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
            if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
            {
                // AuthenticationHeaderValue
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(UserName + ":" + Password);
                ObjHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            }
        }      

        /// <summary>
        /// Return Http response
        /// </summary>
        /// <param name="contract">Contact</param>
        /// <param name="userId">UserId</param>
        /// <returns> HttpResponse Message</returns>
        public HttpResponseMessage GetHttpResponse(object contract, Guid userId)
        {
            ObjectContent objectContent = null;
            try
            {
                objectContent = new ObjectContent<object>(contract, this.MediaTypeFormatter);
                ObjHttpClient.Timeout.Add(new TimeSpan(0, HttpTimeOut, 0));

                var response = ObjHttpClient.PostAsync(RequestUrl, objectContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    HTTPStatus = Convert.ToInt32(response.StatusCode, CultureInfo.InvariantCulture);
                    RequestMessage = Convert.ToString(response.RequestMessage, CultureInfo.InvariantCulture);
                    return response;
                }
                else
                {
                    var errorContent = response.Content.ReadAsStringAsync().Result;
                    throw new InvalidOperationException(response.StatusCode + ": " + response.ReasonPhrase, new InvalidOperationException(errorContent));
                }
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
                throw;
            }
            finally
            {
                if (objectContent != null)
                {
                    objectContent.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// returns Http Response(Calling method-Get)
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>HttpResponse Message.</returns>
        /// <exception cref="System.InvalidOperationException">The exception.</exception>
        /// <exception cref="System.Exception">The exception.</exception>
        public HttpResponseMessage GetHttpResponse(Guid userId)
        {
            try
            {
                // Uri requestUri = new Uri(RequestUrl);
                var response = ObjHttpClient.GetAsync(RequestUrl).Result;
                ObjHttpClient.Timeout.Add(new TimeSpan(0, HttpTimeOut, 0));
                if (response.IsSuccessStatusCode)
                {
                    HTTPStatus = Convert.ToInt32(response.StatusCode, CultureInfo.InvariantCulture);
                    RequestMessage = Convert.ToString(response.RequestMessage, CultureInfo.InvariantCulture);
                    return response;
                }
                else
                {
                    var errorContent = response.Content.ReadAsStringAsync().Result;
                    throw new InvalidOperationException(response.StatusCode + ": " + response.ReasonPhrase, new InvalidOperationException(errorContent));
                }
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
                throw;
            }

            return null;
        }


        /// <summary>
        /// Gets the HTTP Response Message
        /// </summary>
        /// <param name="contract">string contract</param>
        /// <param name="userId">Guid userId</param>
        /// <param name="contentType">string contentType</param>
        /// <returns>HTTP Response</returns>
        public HttpResponseMessage GetHttpResponseMessage(string contract, Guid userId, string contentType)
        {
            
            HttpClient ObjHttpClient = null;
            StringContent objStringContent = null;
            try
            {
                ObjHttpClient = new HttpClient();

                //// Set Time out 5 minut=300000 milisecond
                ObjHttpClient.Timeout = TimeSpan.FromMinutes(5);
                ObjHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
                {
                    ////AuthenticationHeaderValue
                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(UserName + ":" + Password);
                    ObjHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                }

                objStringContent = new StringContent(contract);
                var response = ObjHttpClient.PostAsync(RequestUrl, objStringContent).Result;
                
                return response;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, Convert.ToString(userId, CultureInfo.InvariantCulture));
                throw;
            }
            finally
            {
                if (objStringContent != null)
                {
                    objStringContent.Dispose();
                }

                if (ObjHttpClient != null)
                {
                    ObjHttpClient.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ServiceHelper"/> class.
        /// </summary>
        ~ServiceHelper()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }

            isDisposed = true;
        }

        /// <summary>
        /// Disposes the core.
        /// </summary>
        protected void DisposeCore()
        {
            if (ObjHttpClient != null)
            {
                ObjHttpClient.Dispose();
            }
        }
    }
}
