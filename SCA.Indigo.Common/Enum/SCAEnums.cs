﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Common
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="SCAEnums.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Runtime.Serialization;
namespace SCA.Indigo.Common.Enums
{
    /// <summary>
    /// SCA Enum
    /// </summary>
    public static class SCAEnums
    {
        /// <summary>
        /// Other Contacts
        /// </summary>
        public enum OtherConstants
        {
            Undefined = 0,
            LanguageId = 1
        }

        /// <summary>
        /// List Type
        /// </summary>
        public enum ListType
        {
            Undefined = 0,
            LanguageId = 1,
            MedicalStaff = 1,
            AnalysisInformation = 2,
            PatientType = 3,
            PatientStatus = 4,
            ReasonCode = 5,
            CommunicationFormat = 6,
            Title = 7,
            OrderStatus = 9,
            OrderType = 12,
            ActivePatient = 14,
            StoppedPatient = 15,
            RemovedPatient = 16,
            MaleTitle = 17,
            FemaleTitle = 18,
            Gender = 20,
            CareHomeType = 21,
            CareHomeStatus = 22,
            CareHomeOrderType = 23,
            MassDataTemplate = 25,
            CustomerStatus = 10023,
            InteractionStatus = 10020,
            InteractionType = 10021,
            InteractionSubType = 10022,
            Hospital = 10107,
            Hierarchy = 10024      
        }

        /// <summary>
        /// Patient Status
        /// </summary>
        public enum PatientStatus
        {
            Undefined = 0,
            Active = 10020,
            Stopped = 10021,
            Removed = 10022
        }

        /// <summary>
        /// Order Status
        /// </summary>
        public enum OrderStatus
        {
            Undefined = 0,
            Activated = 10047,
            Generated = 10048,
            SentToSAP = 10098,
            Failed = 10099,
            Cancelled = 10105
        }

        /// <summary>
        /// Order Type
        /// </summary>
        public enum OrderType
        {
            Undefined = 0,
            [EnumMember(Value = "SelfCare Community/ Carehome/ One-Off Standard")]
            ZHDP = 10053,
            [EnumMember(Value = "One-Off Rush")]
            ZHDR = 10054,
            [EnumMember(Value = "One off Rush FOC")]
            ZHDF = 10087,
            [EnumMember(Value = "Sample Order")]
            SAMP = 10100,
            [EnumMember(Value = "Carehome Product Level")]
            ZHDH = 20158
        }

		/// <summary>
		/// Order Type
		/// </summary>
		public enum OrderCreationType
		{
			Undefined = 0,
            [EnumMember(Value = "Auto Phone")]
            Telephonic = 20160,
            [EnumMember(Value = "Online")]
            Online = 20161,
            [EnumMember(Value = "Community Order")]
			CommunityOrder = 101,
            [EnumMember(Value = "Community OneOff Standard")]
			CommunityOneOffStandard = 102,
            [EnumMember(Value = "Community OneOff Emergency")]
			CommunityOneOffRush = 103,
            [EnumMember(Value = "Community OneOff FOC")]
			CommunityOneOffFOC = 104,
            [EnumMember(Value = "CH Patient Order")]
			CarehomePatientOrder = 201,
            [EnumMember(Value = "CH Patient OneOff Standard")]
			CarehomePatientOneOffStandard = 202,
            [EnumMember(Value = "CH Patient OneOff Emergency")]
			CarehomePatientOneOffRush = 203,
            [EnumMember(Value = "CH Patient OneOff FOC")]
			CarehomePatientOneOffFOC = 204,
            [EnumMember(Value = "CH OneOff Standard")]
			CarehomeOneOffStandard = 205,
            [EnumMember(Value = "CH OneOff Emergency")]
			CarehomeOneOffRush = 206,
            [EnumMember(Value = "CH OneOff FOC")]
			CarehomeOneOffFOC = 207,
            [EnumMember(Value = "CH Product Order")]
			CarehomeProductOrder = 208,
            [EnumMember(Value = "Sample Order")]
			SampleOrder = 301,
            [EnumMember(Value = "Automatic Order")]
			AutomaticOrder = 401
		}

        /// <summary>
        /// SQL Command Type
        /// </summary>
        public enum SQLCommandType
        {
            Undefined = 0,
            Inserted = 1,
            Updated = 2
        }

        /// <summary>
        /// Enum Description
        /// </summary>
        public enum PatientType
        {
            Undefined = 0,      
            //----New Patient Type------//
            CommunitySelfcareNursing=1006,
            CommunitySelfcareResidential=1007,
            PaediatricProcare=1008,
            PaediatricSelfcare = 1009,
            //----End Of New Patient Type------//
            Procare = 10010,
            SelfCare = 10011,
            Residential = 10012,
            ResidentialSelfcare = 10013,
            Nursing = 10014,
            NursingSelfcare = 10015,
            Hospital = 10016,
            ContinuingCare = 10017,
            Bulk = 10018,
            ChildSelfCare = 10019,
            Prospect = 10101
        }
        
        /// <summary>
        /// Carehome Patient Type
        /// </summary>
        public enum CarehomePatientType
        {
            Residential = PatientType.Residential,
            ResidentialSelfcare = PatientType.ResidentialSelfcare,
            Nursing = PatientType.Nursing,
            NursingSelfcare = PatientType.NursingSelfcare,
            Hospital = PatientType.Hospital,
            HospitalSelfcare = PatientType.ContinuingCare,
            Prospect = PatientType.Prospect
        }

        /// <summary>
        /// Enum OrderFor
        /// </summary>
        public enum OrderFor
        {
            Undefined = 0,
            PatientOrder = 10088,
            CareHomeOrder = 10089,
            SampleOrder = 10102,
            NonCareHomeOrder = 10088
        }

        /// <summary>
        /// Enum ReasonCodes
        /// </summary>
        public enum ReasonCodes
        {
            Undefined = 0,
            CustomerRequest = 10027,
            WaitingForApproval = 10028,
            Waitlisted = 10029,
            Clinical = 10030,
            InHospital = 10031,
            Other = 10032
        }

        /// <summary>
        /// Enum StoppedPatientReasonCode
        /// </summary>
        public enum StoppedPatientReasonCode
        {
            Undefined = 0,
            WaitingforApproval = 10070,
            CarehomeStopped = 20185
        }
        /// <summary>
        /// Enum RemovePatientReasonCode
        /// </summary>
        public enum RemovedPatientReasonCode
        {
            Undefined = 0,
            CarehomeRemoved = 20184,
            // 1.7.5 Uncomment For Sprint 2
            //Copied = 20186
            // 1.7.5 Uncomment For Sprint 2
        }

        /// <summary>
        /// Enum Gender
        /// </summary>
        public enum Gender
        {
            Undefined = 0,
            Male = 10103,
            Female = 10104
        }

        /// <summary>
        /// Enum UserType
        /// </summary>
        public enum UserType
        {
            Undefined = 0,
            SCA = 1,
            External = 2
        }

        /// <summary>
        /// Enum Description
        /// </summary>
        public enum Role
        {
            Undefined = 0,
            Administrator = 1,
            KeyUser = 2,
            Standard = 3,
            Display = 4,
            CareHome = 5,
            Patient = 6,
            Reports = 7,
            Nurse = 8
        }

        /// <summary>
        /// Enum HierarchyType
        /// </summary>
        public enum HierarchyType
        {
            Undefined = 0,
            Hierarchy1 = 20128,
            Hierarchy2 = 20129,
            Hierarchy3 = 20130,
            Hierarchy4 = 20131
        }

        /// <summary>
        /// Enum ClickEvent
        /// </summary>
        public enum ClickEvent
        {
            Undefined = 0,
            OnCellSelect = 1,
            OndblClickRow = 2
        }

        /// <summary>
        /// Enum Status
        /// </summary>
        public enum Status
        {
            Undefined = 0,
            Active = 1,
            Removed = 2
        }

        /// <summary>
        /// Enum ObjectType
        /// </summary>
        public enum ObjectType
        {
            Patient = 10113,
            CareHome = 10114,
            Customer = 10115,
            Product = 0
        }

        /// <summary>
        /// Enum CareHomeStatus
        /// </summary>
        public enum CareHomeStatus
        {
            Undefined = 0,
            Active = 10108,
            Stopped = 20125,
            Removed = 10109
        }

        /// <summary>
        /// Enum CareHomeOrderType
        /// </summary>
        public enum CareHomeOrderType
        {
            Undefined = 0,
            Automatic = 10110,
            PatientLevel = 10111,
            ProductLevel = 10112
        }

        public enum Country
        {
            Undefined = 0,
            UnitedKingdom = 1,
            Ireland = 2            
        }

        /// <summary>
        /// Enum InteractionStatus
        /// </summary>
        public enum InteractionStatus
        {
            Undefined = 0,
            InProgress = 20099,
            Completed = 20100, 
            //now name change to  "Closed"
            RequiresAction = 20101
        }

        /// <summary>
        /// Enum InteractionType
        /// </summary>
        public enum InteractionType
        {
            Undefined = 0,
            General = 20102,
            Delivery = 20103,
            Internal = 20104,
            Order = 20187,
            EmergencyOrder = 20303
        }

        /// <summary>
        /// Enum InteractionSubType
        /// </summary>
        public enum InteractionSubType
        {
            Undefined = 0,
            DataChange = 20105,
            GeneralCarehomeQuery = 20106,
            PrfReturned = 20107,
            ProductComplaint = 20108,
            TrustError = 20109,
            FormalComplaint = 20110,
            DeliveryComplaint = 20111,
            DamagedGoods = 20112,
            DeliveryFeedback = 20113,
            LateDelivery = 20114,
            ProductsMissing = 20115,
            ProductsWrong = 20116,
            PartDelivery = 20117,
            CustomerServices = 20118,
            Logistics = 20119,
            IT = 20120,
            OrderActivation = 20127,
            StandardOneOffOrder = 20243,
            EmergencyOneOffOrder = 20242,
            CareHomeOrder = 20241,
            SampleOrder = 20136,
            SampleOrderFromOrder = 20244,
            OrderAmendment = 20246,
            AutoPhoneActivation = 20176,
            OnlineActivation = 20175
        }

        /// <summary>
        /// Enum CustomerStatus
        /// </summary>
        public enum CustomerStatus
        {
            Undefined = 0,
            Active = 20122,
            Blocked = 20123
        }

        /// <summary>
        /// Enum PrescriptionStatus
        /// </summary>
        public enum PrescriptionStatus
        {
            Undefined = 0,
            Active = 1,
            Removed = 2
        }

        /// <summary>
        /// Enum NoteType
        /// </summary>
        public enum NoteType
        {
            Undefined = 0,
            PatientNote = 20132,
            OrderNote = 20133,
            PrescriptionNote = 20134,
            CareHomeNote = 20135,
            SampleOrderNote = 20261,
            CommunityInteractionNote = 20300,
            CarehomeInteractionNote = 20301,
            CarehomeProductLevelInteractionNote = 20302
        }

        /// <summary>
        /// Enum PatientTable
        /// </summary>
        public enum PatientTable
        {
            [EnumMember(Value = "SAP Patient ID")] 
            SAPPatientNumber = 0,

            [EnumMember(Value = "Gender")]
	        PatientGender,

            [EnumMember(Value = "Title")]
	        PatientTitle,

            [EnumMember(Value = "Patient First Name")]
	        PatientFirstName,

            [EnumMember(Value = "Patient Last Name")]
	        PatientLastName,

            [EnumMember(Value = "Patient Date of Birth")]
	        PatientDateofBirth,

            [EnumMember(Value = "Patient Type")]
	        PatientType,

            [EnumMember(Value = "Patient Status")]
	        PatientStatus,

            [EnumMember(Value = "Reason Code")]
	        PatientReasonCode,

            [EnumMember(Value = "Status Changed")]
            RemovedStoppedDateTime,

            [EnumMember(Value = "Patient Delivery Frequency")]
	        PatientDeliveryFrequency,

            [EnumMember(Value = "Delivery Address Line 1")]
	        PatientDeliveryAddressLine1,

            [EnumMember(Value = "Delivery Address Line 2")]
	        PatientDeliveryAddressLine2,

            [EnumMember(Value = "Delivery Address City")]
	        PatientDeliveryCity,

            [EnumMember(Value = "Delivery Address Country")]
	        PatientDeliveryCountry,

            [EnumMember(Value = "Delivery Address House Name")]
	        PatientDeliveryHouseName,

            [EnumMember(Value = "Delivery Address House Number")]
	        PatientDeliveryHouseNumber,

            [EnumMember(Value = "Delivery Address PostCode")]
	        PatientDeliveryPostCode,

            [EnumMember(Value = "Patient Address Address Line 1")]
	        PatientAddressAddressLine1,

            [EnumMember(Value = "Patient Address Address Line 2")]
	        PatientAddressAddressLine2,

            [EnumMember(Value = "Patient Address City")]
	        PatientAddressCity,

            [EnumMember(Value = "Patient Address Country")]
	        PatientAddressCountry,

            [EnumMember(Value = "Patient Address House Name")]
	        PatientAddressHouseName,

            [EnumMember(Value = "Patient Address House Number")]
	        PatientAddressHouseNumber,

            [EnumMember(Value = "Patient Address Post Code")]
	        PatientAddressPostCode,

            [EnumMember(Value = "Assessment Date")]
	        PatientAssessmentDate,

            [EnumMember(Value = "Next Assessment Date")]
	        PatientNextAssessmentDate,

            [EnumMember(Value = "Telephone")]
	        PatientTelephoneNumber,

            [EnumMember(Value = "Mobile")]
	        PatientMobileNumber,

            [EnumMember(Value = "Email")]
	        PatientEmail,

            [EnumMember(Value = "Communication Format")]
	        PatientCommunicationFormat,

            [EnumMember(Value = "Patient Last Delivery Date")]
	        PatientDeliveryDate,

            [EnumMember(Value = "Patient Next Delivery Date")]
	        PatientNextDeliveryDate,

            [EnumMember(Value = "Patient Round Id")]
	        PatientRoundId,

            [EnumMember(Value = "ADP LTC")]
	        PatientAdpLtc,

            [EnumMember(Value = "ADP NC")]
	        PatientAdpNc,

            [EnumMember(Value = "Created On")]
	        PatientCreatedDate,

            [EnumMember(Value = "Created By")]
	        PatientCreatedBy,

            [EnumMember(Value = "NHS Number")]
	        PatientNhsId,

            [EnumMember(Value = "Local Id")]
	        PatientLocalId,

            [EnumMember(Value = "Continence Advisor")]
            ContinenceAdvisor,

            [EnumMember(Value = "Nurse")]
            Nurse,

            [EnumMember(Value = "Nurse Base")]
            NurseBase,

            [EnumMember(Value = "Health Visitor")]
            HealthVisitor,

            [EnumMember(Value = "Healthcare Assistant")]
            HC,

            [EnumMember(Value = "Doctor")]
            Doctor,

            [EnumMember(Value = "GP Practice")]
            GPPractice,

            [EnumMember(Value = "Caseload Holder")]
            CaseloadHolder,

            [EnumMember(Value = "PO Number")]
            PONumber,

            [EnumMember(Value = "Type of Incontinence")]
            TypeofIncontinence,

            [EnumMember(Value = "Medical Condition")]
            MedicalCondition,

            [EnumMember(Value = "School")]
            School,

            [EnumMember(Value = "Ethnicity")]
            Ethnicity,

            [EnumMember(Value = "Locality")]
            Locality,

            [EnumMember(Value = "Sector")]
            Sector,

            [EnumMember(Value = "Health Centre")]
            HealthCentre,

            [EnumMember(Value = "Budget Centre")]
            BudgetCentre,

            [EnumMember(Value = "Data Protection")]
            IsDataProtected            
        }

        /// <summary>
        /// Enum CustomerTable
        /// </summary>
        public enum CustomerTable
        {
            [EnumMember(Value = "SAP Customer ID")]
            SapCustomerNumber = 0,

            [EnumMember(Value = "Customer Name")]
            CustomerName = 2,

            [EnumMember(Value = "Start Date")]
            CustomerStartDate = 3,

            [EnumMember(Value = "Customer Status")]
            CustomerStatus = 4,

            [EnumMember(Value = "Authorization PIN")]
            CustomerAuthorizationPIN = 6,

            [EnumMember(Value = "Authorized User First Name")]
            CustomerAuthorizedUserFirstName = 7,

            [EnumMember(Value = "Authorized User Last Name")]
            CustomerAuthorizedUserLastName = 8,

            [EnumMember(Value = "Authorized User Job Title")]
            CustomerAuthorizedUserJobTitle = 9,

            [EnumMember(Value = "Authorized User Telephone")]
            CustomerAuthorizedUserTelephoneNumber = 10,

            [EnumMember(Value = "Authorized User Mobile")]
            CustomerAuthorizedUserMobileNumber = 11,

            [EnumMember(Value = "Authorized User Email")]
            CustomerAuthorizedUserEmail = 12,
        }

        /// <summary>
        /// Enum CareHomeTable
        /// </summary>
        public enum CareHomeTable
        {
            [EnumMember(Value = "SAP Carehome ID")]
            SAPCareHomeNumber = 0,

            [EnumMember(Value = "Care Home Name")]
            CareHomeName,

            [EnumMember(Value = "Carehome Delivery Frequency")]
            CareHomeDeliveryFrequency,

            [EnumMember(Value = "Carehome Round Id")]
            CareHomeRoundId,

            [EnumMember(Value = "Address Line 1")]
            CareHomeAddressLine1,

            [EnumMember(Value = "Address Line 2")]
	        CareHomeAddressLine2,

            [EnumMember(Value = "City")]
	        CareHomeCity,

            [EnumMember(Value = "Country")]
	        CareHomeCountry,

            [EnumMember(Value = "House Name")]
	        CareHomeHouseName,

            [EnumMember(Value = "House Number")]
	        CareHomeHouseNumber,

            [EnumMember(Value = "Post Code")]
	        CareHomePostCode,

            [EnumMember(Value = "Carehome Status")]
            CareHomeStatus,

            [EnumMember(Value = "Type")]
	        CareHomeType,

            [EnumMember(Value = "Carehome Next Delivery Date")]
	        CarehomeNextDeliveryDate,

            [EnumMember(Value = "Purchase Order No")]
	        CarehomePurchaseOrderNo
        }

        /// <summary>
        /// Enum OrderTable
        /// </summary>
        public enum OrderTable
        {
            [EnumMember(Value = "Frontend Order ID")]
            OrderId = 0,

            [EnumMember(Value = "SAP Order Number")]
            SAPOrderNo,

            [EnumMember(Value = "SAP Delivery Number")]
            OrderSAPDeliveryNo,

            //[EnumMember(Value = "Order Type")]
            //OrderType,

            [EnumMember(Value = "Order Creation Type")]
            OrderCreationType,

            [EnumMember(Value = "Order Creation Date")]
            OrderDate,

            [EnumMember(Value = "Delivery Date")]
            SAPOrderDeliveryDate,

            [EnumMember(Value = "Actual Delivery Date")]
            SAPActualDeliveryDate,

            [EnumMember(Value = "Product ID")]
            OrderSAPProductID,

            [EnumMember(Value = "Product Description")]
            OrderProductDescription,

            [EnumMember(Value = "Order Quantity")]
            OrderQuantity,

            [EnumMember(Value = "Invoice Number")]
            OrderInvoiceNo,
            
            [EnumMember(Value = "Plant")]
            Plant,

            [EnumMember(Value = "Invoice Date")]
            BillingDate,

            [EnumMember(Value = "POD Date")]
            PODDate,

            [EnumMember(Value = "Currency")]
            Currency,

            [EnumMember(Value = "Invoice Qty")]
            BillingQuantity,

            [EnumMember(Value = "Delivery Qty")]
            DeliveryQuantity,

            [EnumMember(Value = "UoM")]
            BillingUnit,

            [EnumMember(Value = "Delivery Unit")]
            DeliveryUnit,

            [EnumMember(Value = "Net Value")]
            NetValue,

            [EnumMember(Value = "VAT")]
            VatValue,
        }

        /// <summary>
        /// Enum UserMaintenanceTable
        /// </summary>
        public enum UserMaintenanceTable
        {
            [EnumMember(Value = "User Type")]
            UserType = 0,

            [EnumMember(Value = "User ID")]
            UserId,

            [EnumMember(Value = "User Name")]
            UserName,

            [EnumMember(Value = "Customer ID")]
            UserSapCustomerNo,

            [EnumMember(Value = "User Customer Name")]
            UserCustomerName,

            [EnumMember(Value = "Role")]
            UserRoleName,
        }

        /// <summary>
        /// Enum PrescriptionTable
        /// </summary>
        public enum PrescriptionTable
        {
            [EnumMember(Value = "Product ID")]
            PrescriptionSapProductId = 0,

            [EnumMember(Value = "Product Description")]
            PrescriptionProductDescription,

            [EnumMember(Value = "Prescription Frequency")]
            PrescriptionFrequency,

            [EnumMember(Value = "Assessed PPD")]
            PrescriptionAssessedPadsPerDay,

            [EnumMember(Value = "Actual PPD")]
            PrescriptionActualPadsPerDay,

            [EnumMember(Value = "DQ1")]
            PrescriptionDQ1,

            [EnumMember(Value = "DQ2")]
            PrescriptionDQ2,

            [EnumMember(Value = "DQ3")]
            PrescriptionDQ3,

            [EnumMember(Value = "DQ4")]
            PrescriptionDQ4,

            [EnumMember(Value = "Valid From")]
            PrescriptionValidFromDate,

            [EnumMember(Value = "Valid To")]
            PrescriptionValidToDate,

            [EnumMember(Value = "Prescription Status")]
            PrescriptionStatus,

            [EnumMember(Value = "Prescription Last Delivery Date")]
            PrescriptionDeliveryDate,

            [EnumMember(Value = "Prescription Next Delivery Date")]
            PrescriptionNextDeliveryDate
        }

        /// <summary>
        /// Enum ClinicalContactsTable
        /// </summary>
        public enum ClinicalContactsTable
        {
            [EnumMember(Value = "Clinical Contacts ID")]
            ClinicalContactRoleId = 0,

            [EnumMember(Value = "Clinical Contacts Name")]
            ClinicalContactRoleName,

            [EnumMember(Value = "Clinical Contacts Value")]
            ClinicalContactRoleValue,

            [EnumMember(Value = "Clinical Contacts Modified By")]
            ClinicalContactModifiedBy,

            [EnumMember(Value = "Clinical Contacts Modified On")]
            ClinicalContactModifiedDate,
        }

        /// <summary>
        /// Enum AuditLogType
        /// </summary>
        public enum AuditLogType
        {
            Undefined =0,
            Patient = 1,
            CareHome = 2,
            Customer = 3,
            Custom = 4,
            Users = 5
        }

        /// <summary>
        /// Enum CustomerParameterTable
        /// </summary>
        public enum CustomerParameterTable
        {
            [EnumMember(Value = "Order Creation Allowed")]
            OrderCreationAllowed = 0,

            [EnumMember(Value = "Order Approval")]
            OrderApproval = 2,

            [EnumMember(Value = "Order Lead Time")]
            OrderLeadTime = 3,

            [EnumMember(Value = "Order Cut Off Time")]
            OrderCutOffTime = 4,
        }

        /// <summary>
        /// Enum PreferenceType
        /// </summary>
        public enum PreferenceType
        {
            Undefined = 0,
            CommunicationPreference = 20145,
            MarketingPreference = 20146
        }

        /// <summary>
        /// Enum CommunicationFormate
        /// </summary>
        public enum CommunicationFormate
        {
            Undefined = 0,
            Email = 10033,
            Post = 10034,
            Text = 10035,
            Fax = 10036,
            Nocommunication = 10037,
            TelephoneNo = 10097
        }

        /// <summary>
        /// Enum AutomaticOrderStatus
        /// </summary>
        public enum AutomaticOrderStatus
        {
            Undefined = 0,
            Success = 1,
            Failed = 2
        }

        /// <summary>
        /// Enum AutomaticOrderStatusMessage
        /// </summary>
        public enum AutomaticOrderStatusMessage
        {
            [EnumMember(Value = "Undefined")]
            Undefined = 0,

            [EnumMember(Value = "Success")] 
            Success = 200,

            [EnumMember(Value = "Invalid criteria to create the order.")] 
            InvalidCriteria = 101,

            [EnumMember(Value = "No Valid Customers.")]
            NoValidCustomers = 102,

            [EnumMember(Value = "Invalid Customer Id.")]
            InvalidCustomerId = 103,

            [EnumMember(Value = "Invalid SAP Patient Id.")]
            InvalidSAPPatientId = 104,

            [EnumMember(Value = "Invalid SAP Carehome Id.")]
            InvalidSAPCareHomeId = 105,

            [EnumMember(Value = "No valid patients attached for this Customer for Automatic Order creation.")]
            NoValidPatientsUnderCustomer = 106,

            [EnumMember(Value = "Patient already has an active order created.")] 
            PatientHasActiveOrder = 107,

            [EnumMember(Value = "No valid Prescription found for the patient")]
            NoValidPrescription = 108,

            [EnumMember(Value = "Customer is Blocked.")]
            CustomerBlocked = 109,

            [EnumMember(Value = "Order creation not allowed for the customer.")]
            OrderNotAllowedForCustomer = 110,

            [EnumMember(Value = "Carehome is not under the selected customer.")]
            CareHomeNotUnderCustomer = 111,

            [EnumMember(Value = "Patient is not under the selected customer.")]
            PatientNotUnderCustomer = 112,

            [EnumMember(Value = "Carehome Next Delivery Date is greater than the specified Next Delivery Date")]
            CareHomeNotDueForOrder = 113,

            [EnumMember(Value = "Patient Next Delivery Date is greater than the specified Next Delivery Date")]
            PatientNotDueForOrder = 114,

            [EnumMember(Value = "No valid patients attached for this CareHome for Automatic Order creation.")]
            NoValidPatientUnderCareHome = 115,

            [EnumMember(Value = "Patient is not Active.")] 
            PatientNotActive = 116,

            [EnumMember(Value = "For selfcare patient, only first order can be created Automatically.")]
            NotFirstOrderSelfCare = 117,

            [EnumMember(Value = "No valid Carehomes attached for this Customer for Automatic Order creation.")]
            NoValidCareHomeUnderCustomer = 118,

            [EnumMember(Value = "Runtime Exception. Please check exception log or contact Administrator for more details.")] 
            Exception = 120,

            [EnumMember(Value = "No Automatic order will be created for this patient type.")]
            AllAutomaticOrdersRestricted = 121,

            [EnumMember(Value = "Only subsequent Automatic order can be created.")]
            FirstAutomaticOrderRestricted = 122,
        }

        /// <summary>
        /// Enum PatientScreenSectionMenu
        /// </summary>
        public enum PatientScreenSectionMenu
        {
            Undefined = 0,
            PatientInfoSection = 29,
            MedicalInfoSection = 30,
            PrescriptionSection = 31
        }

        /// <summary>
        /// Enum Menu
        /// </summary>
        public enum Menu
        {
            Undefined = 0,
            Home = 1,
            Search = 2,
            Patient = 3,
            OrderActivation = 4,
            Authorisation = 5,
            Customer = 6,
            ContactUs = 7,
            AddPatient = 8,
            ViewEditPatient = 9,
            PatientNotes = 10,
            PatientRecentOrders = 11,
            CommunityOrder = 12,
            CarehomeHospitalOrder = 13,
            OneoffOrder = 14,
            PrescriptionAuthorisation = 15,
            SetCustomerParameters = 16,
            SampleOrder = 17,
            UserAdmin = 18,
            MapRoles = 19,
            MapCustomers = 20,
            NextDeliveryAmendment = 21,
            HolidayProcess = 22,
            CustomerMaintenance = 23,
            Carehome = 24,
            ViewEditCarehome = 25,
            AddCarehome = 26,
            AuditLog = 27,
            AutomaticOrderCriteria = 28,
            PatientDetails = 29,
            ClinicalContactsAnalysis = 30,
            PRESCRIPTION = 31,
            Report = 32,
            Interactions = 33,
            Admin = 34,
            PatientContactInfo = 35,
            PatientSampleOrders = 36,
            PatientAttachments = 37,
            PatientInteraction = 38,
            CareHomeAttachments = 39,
            CareHomeContactInfo = 40,
            CareHomeInteraction = 41,
            CareHomeNotes = 42,
            CareHomeRecentOrders = 43,
            CustomerAttachments = 44,
            CustomerContactInfo = 45,
            PatientAuditLog = 47,
            CarehomeAuditLog = 48,
            AddNewClinicalContact = 51,
            AddNewAnalysisInfo = 52
        }

        /// <summary>
        /// Enum InteractionDescription
        /// </summary>
        public enum InteractionDescription
        {
            Undefined = 0,
            InteractionWelcomeMessge = 30132,
            InteractionSelfcareCareHome = 30131,
            InteractionOneOfStandard = 30130,
            InteractionSelfCareCommunity = 30129,
            InteractionNotes = 30127,
            InteractionProductDescription = 30126,
            InteractionNDDManually = 30401,
            InteractionNDDHolidayProcessAdmin = 30402
        }

        /// <summary>
        /// Mass Data Template Enum
        /// </summary>
        public enum MassDataTemplate
        {
            [EnumMember(Value = "Contact Person Details")]
            ContactPerson = 20147,

            [EnumMember(Value = "Communication Format")]
            CommunicationFormat = 20148,

            [EnumMember(Value = "Customer-Post Code Matrix")]
            PostCodeMatrix = 20149,

            [EnumMember(Value = "Customer-Clinical Contacts")]
            CustomerClinicalContacts = 20150,

            [EnumMember(Value = "Patient")]
            Patient = 20151,

            [EnumMember(Value = "Presrciption")]
            Prescription = 20152,

            [EnumMember(Value = "Carehome")]
            Carehome = 20153,

            [EnumMember(Value = "Customer-Analysis Information")]
            CustomerAnalysisInformation = 20154,

            [EnumMember(Value = "Patient-Clinical Contacts")]
            PatientClinicalContacts = 20155,

            [EnumMember(Value = "Patient-Analysis Information")]
            PatientAnalysisInformation = 20156,

            [EnumMember (Value = "Order")]
            Order = 20159
            
        }

        /// <summary>
        /// Mass Upload Status Type Enum
        /// </summary>
        public enum MassUploadStatusType
        {
            Failed = 0,
            Success = 1
        }

        /// <summary>
        /// Data Type Enum
        /// </summary>
        public enum DataType
        {
            Int = 0,
            Long = 1,
            Decimal = 2,
            String = 3,
            Char = 4,
            Date = 5,
            NoCheck = 6,
            Bool = 7
        }

        /// <summary>
        /// PDF Format Enum
        /// </summary>
        public enum PDFFormat 
        { 
            Potraite = 0,
            Landscape = 1
        }
    }
}
