﻿//----------------------------------------------------------------------------------------------
// <copyright file="AuthorizePrescription.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
	using System.ServiceModel;

    using BusinessModels;
    using Interfaces;
    using Common;
    using Common.Enums;
    using Infrastructure.Repositories;
    using Model;
    

    /// <summary>
    /// Prescription Authorization Business class
    /// </summary>    
    public class AuthorizePrescription : IAuthorizePrescription
    {
        /// <summary>
        /// Approve the Patient's Prescription
        /// </summary>
        /// <param name="objPrescriptionNote">object of PrescriptionNoteBusinessModel</param>
        /// <returns>Boolean value</returns>
        public bool ApprovePrescription(PrescriptionNoteBusinessModel objPrescriptionNote)
        {            
			var userId = string.Empty;            
			IPatientRepository patientRepository = null;   
            try
            {
				if(objPrescriptionNote == null)
				{
					return false;
				}
				
				var currentTimeStamp = DateTime.Now;				
				patientRepository = new PatientRepository();
				
				userId = objPrescriptionNote.CreatedBy.ToString();
                var patient = patientRepository.Find(objPrescriptionNote.PatientId);
                var prescriptionProducts = patient.Prescriptions.Select(q => q.IDXPrescriptionProducts).FirstOrDefault();
                var productsToAuthorise = prescriptionProducts.Where(q => (q.PrescriptionAuthorizationApprovalFlag == true || q.ProductApprovalFlag == true) && q.IsRemoved == false).ToList();
                productsToAuthorise.ForEach(q =>  {
                    q.PrescriptionAuthorizationApprovalFlag = false;
                    q.ProductApprovalFlag = false;
                    q.ApprovedOn = currentTimeStamp;                    
                }                
                );

                patient.ModifiedBy = objPrescriptionNote.CreatedBy;
                patient.ModifiedDate = currentTimeStamp;
                if (patient.PatientStatus.Value == (long)SCAEnums.PatientStatus.Stopped && patient.ReasonCodeID == (long)SCAEnums.StoppedPatientReasonCode.WaitingforApproval)
                {
                    patient.PatientStatus = (long)SCAEnums.PatientStatus.Active;
                    patient.ReasonCodeID = null;
                    patient.RemovedStoppedDateTime = null;
                }                
                patientRepository.InsertOrUpdate(patient);

				IUnitOfWork unitOfWork = patientRepository.UnitOfWork;
				unitOfWork.Commit();
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);               
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);                
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);                
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);                
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);                
                throw;
            }
            finally
            {
                if(patientRepository != null)
                {
                    patientRepository.Dispose();                    
                }                
            }

            return false;
        }

        /// <summary>
        /// Validate User's AuthorizationPin
        /// </summary>
        /// <param name="userId">User's ID</param>
        /// <param name="PIN">User's Authorization PIN</param>
        /// <returns>Boolean value</returns>
		public bool ValidateUserAuthorizationPin(string userId, string pin)
        {
            IUsersRepository userRepository = new UsersRepository();
            try
            {
                var users = userRepository.GetUsersByUserId(userId);
                var userPin = 0;///User's PIN Stored in the database
                var enteredPin = 0;//userProvided PIN that is to be convetrd into int

                if (users.Any())
                {
					userPin = Convert.ToInt32(users.Select(u => u.AuthorizationPIN).FirstOrDefault(), CultureInfo.InvariantCulture);
                }
                int.TryParse(pin,out enteredPin);
				return userPin ==  enteredPin;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
                return false;
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
                return false;
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
                return false;
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
                return false;
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (userRepository != null)
                {
                    userRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Get Patient details whose Prescription is pending for Authorization 
        /// </summary>
        /// <param name="userName">User's Name</param>
        /// <param name="userId">User Id</param>
        /// <returns>List of PrescriptionAuthorizationBusinessModel</returns>
        public List<PrescriptionAuthorizationBusinessModel> GetAuthorizePrescription(string userName, string userId)
        {
            var prescriptionsToApprove = new List<PrescriptionAuthorizationBusinessModel>();
            try
            {
                if (!string.IsNullOrEmpty(userName) && userName.ToUpper(CultureInfo.CurrentCulture) == "NULL")
                {
                    userName = null;
                }

                var pendingAuthorizations = (new DBHelper()).GetPendingAuthorizations(userName);
                if (pendingAuthorizations.Any())
                {
                    prescriptionsToApprove = pendingAuthorizations.Select(q => new PrescriptionAuthorizationBusinessModel()
                                             {
                                                 PatientId = q.PatientId,
                                                 PatientName = Convert.ToString(q.PatientFirstName, CultureInfo.InvariantCulture) + " " + Convert.ToString(q.PatientLastName, CultureInfo.InvariantCulture),
                                                 Address1 = Convert.ToString(q.AddressLine1, CultureInfo.InvariantCulture),
                                                 Address2 = Convert.ToString(q.AddressLine2, CultureInfo.InvariantCulture),
                                                 DOB = q.DateofBirth != null ? Convert.ToDateTime(q.DateofBirth, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
                                                 CustomerName = Convert.ToString(q.CustomerFirstName, CultureInfo.InvariantCulture) + " " + Convert.ToString(q.CustomerLastName, CultureInfo.InvariantCulture),
                                                 SAPPatientNumber = q.SAPPatientNumber != null ? Convert.ToString(q.SAPPatientNumber, CultureInfo.InvariantCulture).TrimStart('0') : string.Empty,
                                                 CustomerId = Convert.ToInt64(q.CustomerId, CultureInfo.InvariantCulture),
                                                 Status = ((SCAEnums.PatientStatus)Convert.ToInt64(q.PatientStatus, CultureInfo.InvariantCulture)).ToString(),
                                                 Locality = Convert.ToString(q.Locality, CultureInfo.InvariantCulture),
                                             }).ToList();
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return prescriptionsToApprove;
        }

    }
}