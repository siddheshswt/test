﻿using SCA.Indigo.Business.BusinessModels;
using SCA.Indigo.Business.Interfaces;
using SCA.Indigo.Common;
using SCA.Indigo.Common.Enums;
using SCA.Indigo.Infrastructure.Repositories;
using SCA.Indigo.Infrastructure.Repositories.CustomRepositories;
using SCA.Indigo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCA.Indigo.BusinessCore;
using SCA.Indigo.BusinessCore.Interfaces;

namespace SCA.Indigo.Business
{
	public class GoogleSearch : IGoogleSearch
	{

        public bool IndexBuilder()
        {
            return BuildGoogleSearch("", "");
        }

        public bool BuildGoogleSearch(string indigoId, string objectType)
		{
            IBuildIndexCore _iBuildIndex = new GoogleSearchCore();
            var isSuccess = false;   
         
            isSuccess = _iBuildIndex.BuildGoogleSearch(indigoId, objectType);

            return isSuccess;
		}

		
        public List<SearchBusinessModel> GetGoogleSearchResults(SearchCriteriaBusinessModel objSearchCriteria)
		{
            var userId = objSearchCriteria.UserID;
            IGoogleSearchRepository repository = null;
            IIDXUserCustomerRepository customerReposiory = null;
            IUsersRepository userRepository = null;
            IIDXUserCareHomeRepository carehomeRepository = null;
            IIDXUserRoleRepository userRoleRepository = null;
            ICustomerRepository customerRepository = null;

            List<GoogleSearchIndigoID_Result> resultId = null;
            List<GoogleSearch_Result> resultPatient = null;
            List<GoogleSearch_Result> resultCarehome = null;
            List<GoogleSearch_Result> resultCustomer = null;
			
            try
            {
                repository = new GoogleSearchRepository();
                customerReposiory = new IDXUserCustomerRepository();
                userRepository = new UsersRepository();
                carehomeRepository = new IDXUserCareHomeRepository();
                userRoleRepository = new IDXUserRoleRepository();
                customerRepository = new CustomerRepository();

                resultId = GetResultIdOfSearch(objSearchCriteria, repository, customerReposiory, userRepository, carehomeRepository, userRoleRepository, customerRepository);

                if (!resultId.Any())
                {
					return new List<SearchBusinessModel>();
                }

				resultPatient = GetDataForIdType(SCAEnums.ObjectType.Patient, resultId, repository);
				resultCarehome = GetDataForIdType(SCAEnums.ObjectType.CareHome, resultId, repository);
				resultCustomer = GetDataForIdType(SCAEnums.ObjectType.Customer, resultId, repository);
			
                var result = GetGoogleSearchResult(resultId, resultPatient, resultCarehome, resultCustomer);

				if(result.Any())
				{
					return ConvertResultToSearchBusinessModel(result, resultId.FirstOrDefault().TotalRows);
				}                
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (repository != null)
                    repository.Dispose();

                if (customerReposiory != null)
                    customerReposiory.Dispose();

                if (userRepository != null)
                    userRepository.Dispose();

                if (carehomeRepository != null)
                    carehomeRepository.Dispose();
            }
			return new List<SearchBusinessModel>();
		}

        private List<SearchBusinessModel> ConvertResultToSearchBusinessModel(List<GoogleSearch_Result> searchResult, long totalRecords)
		{
			return searchResult.Select(q => new SearchBusinessModel() {
					PatientId = q.PatientId ?? 0,
					SAPPatientNumber = q.SAPPatientNumber,
					PatientCustomerId = q.CustomerId,
					SAPCustomerNumber = q.SAPCustomerNumber,
					CareHomeId = q.CareHomeId,					
					TitleId = q.TitleId,
					PatientName = q.PatientName,
					PatientCustomerName = q.CustomerName,
					CareHomeName = q.CareHomeName,
					DOB = q.DateofBirth.HasValue ? Convert.ToDateTime(q.DateofBirth, CultureInfo.InvariantCulture)
                                    .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
					PatientType = q.PatientType,
					PatientStatus = q.PatientStatus,
					DeliveryDate = q.NoOfProduct > 0 ? q.DeliveryDate.HasValue ? Convert.ToDateTime(q.DeliveryDate, CultureInfo.InvariantCulture)
                                    .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty : string.Empty,
					HasActiveOrder = q.NoOfActiveOrders > 0 ? true : false,
					OrderActivated = q.NoOfActiveOrders > 0 ? 1 : 0,
					ADD1 = q.AddressLine1,
					ADD2 = q.AddressLine2,
					HouseName = q.HouseName,
					HouseNumber = q.HouseNumber,
					City = q.City,
					Country = q.Country,
					County = q.County,	
					Postcode = q.PostCode,
					TelephoneNumber = q.TelephoneNumber,
					ObjectType = q.objectType,
					TotalRecords = totalRecords
			}).ToList();
		}

		private List<GoogleSearch_Result> GetDataForIdType(SCAEnums.ObjectType idType, List<GoogleSearchIndigoID_Result> resultIdList, IGoogleSearchRepository repository)
		{
			var idTypeKey = Convert.ToInt64(idType);
			var listOfIds = resultIdList.Where(q => q.IdType == idTypeKey).Select(i=>i.IndigoId).ToList();

			if(listOfIds.Any())
			{				
				switch(idType)
				{
					case SCAEnums.ObjectType.Patient:
						return GetGoogleSearchForPatient(listOfIds, repository);
						break;
					case SCAEnums.ObjectType.CareHome:
						return GetGoogleSearchForCarehome(listOfIds, repository);
						break;
					case SCAEnums.ObjectType.Customer:
						return GetGoogleSearchForCustomer(listOfIds, repository);
						break;
				}
			}
			return null;			
		}

        private List<GoogleSearchIndigoID_Result> GetResultIdOfSearch(SearchCriteriaBusinessModel objSearchCriteria, 
                                                                      IGoogleSearchRepository repository,
                                                                      IIDXUserCustomerRepository customerReposiory,
                                                                      IUsersRepository userRepository,
                                                                      IIDXUserCareHomeRepository carehomeRepository,
                                                                      IIDXUserRoleRepository userRoleRepository,
                                                                      ICustomerRepository customerRepository)
		{
            int pageNo = 1;
            int rowCount = 100;
            var result = new List<GoogleSearchIndigoID_Result>();

            if (!string.IsNullOrEmpty(objSearchCriteria.PageNo))
                pageNo = Convert.ToInt32(objSearchCriteria.PageNo, CultureInfo.InvariantCulture);

            if (!string.IsNullOrEmpty(objSearchCriteria.Rows))
                rowCount = Convert.ToInt32(objSearchCriteria.Rows, CultureInfo.InvariantCulture);

            if (!VerifySortingForSearch(objSearchCriteria.SortColumn))
                objSearchCriteria.SortColumn = " ObjectType DESC,LastName,FirstName,CareHomeName,Postcode ";

            if (!VerifySortingForSearch(objSearchCriteria.SortOrder))
                objSearchCriteria.SortOrder = " ASC ";

            var validCustomer = "";
            var isValid = ValidateUserCustomer(objSearchCriteria.CustomerId,
                                               Guid.Parse(objSearchCriteria.UserID), 
                                               customerReposiory, 
                                               userRoleRepository, 
                                               customerRepository, 
                                               out validCustomer);

            if (isValid)
                objSearchCriteria.CustomerId = validCustomer;
            else
                return result;

            var carehomeId = "";
            var isCarehomeUser = GetCarehomeForUser(objSearchCriteria.UserID, out carehomeId, userRepository, carehomeRepository);

            if (isCarehomeUser && string.IsNullOrEmpty(carehomeId))
                return result;

            var offSet = (pageNo - 1) * rowCount;

            if (!string.IsNullOrEmpty(IsInvalidSearch(objSearchCriteria.OrderID)))
            {
                objSearchCriteria.SortColumn = " IndigoId ";
                objSearchCriteria.OrderID = InterpretGoogleSearchUserInput(objSearchCriteria.OrderID.Trim(), true);
                result = GetGoogleSearchForOrder(objSearchCriteria.OrderID,
                                                    objSearchCriteria.CustomerId,
                                                    carehomeId,
                                                    offSet,
                                                    rowCount,
                                                    objSearchCriteria.SortColumn,
                                                    objSearchCriteria.SortOrder,
                                                    repository);
            }
            else if (!string.IsNullOrEmpty(IsInvalidSearch(objSearchCriteria.SearchID)))
            {
                objSearchCriteria.SearchID = InterpretGoogleSearchUserInput(objSearchCriteria.SearchID.Trim(), true);
                result = GetGoogleSearchForPatientCarehome(objSearchCriteria.SearchID,                                                        
                                                        objSearchCriteria.CustomerId,
                                                        carehomeId,
                                                        offSet,
                                                        rowCount,
                                                        objSearchCriteria.SortColumn,
                                                        objSearchCriteria.SortOrder,
                                                        repository);                                                      
            }
            else
            {
                result = repository.GetGoogleSearchIds(InterpretGoogleSearchUserInput(objSearchCriteria.SearchText, false),
                                                           objSearchCriteria.CustomerId,
                                                           carehomeId,
                                                           IsInvalidSearch(objSearchCriteria.PatientStatus),
                                                           IsInvalidSearch(objSearchCriteria.PatientType),
                                                           offSet,
                                                           rowCount,
                                                           objSearchCriteria.SortColumn,
                                                           objSearchCriteria.SortOrder);
            }
            return result;
		}

        private List<GoogleSearchIndigoID_Result> GetGoogleSearchForPatientCarehome(string searchId, string customerIds, string carehomeId, int offSet, int rowCount, string sortColumn, string sortOrder, IGoogleSearchRepository repository)
        {
            List<GoogleSearchIndigoID_Result> resultId = null;
            List<GoogleSearchIndigoID_Result> patientResult = null;
            List<GoogleSearchIndigoID_Result> carehomeResult = null;
            string patientSearchIds = "0", carehomeSearchIds = "0";

            patientResult = repository.GetPatientId(searchId, appendZero(searchId), customerIds, carehomeId);
            carehomeResult = repository.GetCarehomeId(searchId, appendZero(searchId), customerIds, carehomeId);

            if (!patientResult.Any() && !carehomeResult.Any())
            {
                return new List<GoogleSearchIndigoID_Result>();
            }           
            
            if (patientResult.Any())
                patientSearchIds += string.Join(",", patientResult.Select(q=>q.IndigoId));

            
            if (carehomeResult.Any())
                carehomeSearchIds = string.Join(",", carehomeResult.Select(q => q.IndigoId));
                        
            resultId = repository.GetGoogleSearchForPatientCarehomeId(carehomeSearchIds, patientSearchIds, customerIds, carehomeId, offSet, rowCount, sortColumn, sortOrder);  

            return resultId;
        }

        /// <summary>
        /// Get search result for sap order no & delivery no
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="offSet"></param>
        /// <param name="rowCount"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="repository"></param>
        /// <returns></returns>
        private List<GoogleSearchIndigoID_Result> GetGoogleSearchForOrder(string orderId, string customerIds, string carehomeId,  int offSet, int rowCount, string sortColumn, string sortOrder, IGoogleSearchRepository repository)
        {
            List<GoogleSearchIndigoID_Result> resultId = new List<GoogleSearchIndigoID_Result>(); ;
            List<GoogleSearchIndigoID_Result> resultPatient = null;
            List<GoogleSearchIndigoID_Result> resultCarehome = null;
            
            resultPatient = repository.GetGoogleSearchForPatientOrder(orderId, appendZero(orderId), customerIds, carehomeId, offSet, rowCount, sortColumn, sortOrder);
            resultCarehome = repository.GetGoogleSearchForCarehomeOrder(orderId, appendZero(orderId), customerIds, carehomeId, offSet, rowCount, sortColumn, sortOrder);

            if (resultPatient != null && resultPatient.Any())
                resultId.AddRange(resultPatient);

            if (resultCarehome != null && resultCarehome.Any())
                resultId.AddRange(resultCarehome);

            return resultId;
        }
        private string IsInvalidSearch(string value)
        {
            if (string.IsNullOrEmpty(value) || value == "" || value == "NULL" || value == "-1")
            {
                value = string.Empty;
            }
            return value;
        }

        /// <summary>
        /// Append zero's to the id for lenght of 10 char
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string appendZero(string value)
        {   
            int sapIdLength = 10;
            return value.Trim().PadLeft(sapIdLength, '0');
        }

        private List<GoogleSearch_Result> GetGoogleSearchForPatient(List<long> lstIds, IGoogleSearchRepository repository)
        {
            var result = repository.GetGoogleSearchForPatient(lstIds);
            return result;
        }

        private List<GoogleSearch_Result> GetGoogleSearchForCarehome(List<long> lstIds, IGoogleSearchRepository repository)
        {
            var result = repository.GetGoogleSearchForCarehome(lstIds);
            return result;
        }

        private List<GoogleSearch_Result> GetGoogleSearchForCustomer(List<long> lstIds, IGoogleSearchRepository repository)
        {
            var result = repository.GetGoogleSearchForCustomer(lstIds);
            return result;
        }

        private List<GoogleSearch_Result> GetGoogleSearchResult(List<GoogleSearchIndigoID_Result> lstId, List<GoogleSearch_Result> lstPatient, List<GoogleSearch_Result> lstCarehome, List<GoogleSearch_Result> lstCustomer)
        {
            List<GoogleSearch_Result> result = new List<GoogleSearch_Result>(new GoogleSearch_Result[lstId.Count]);

            if (lstPatient != null && lstPatient.Any())
            {
                lstPatient.ForEach(d =>
                {
                    result[lstId.FindIndex(a => a.IndigoId == d.PatientId && a.IdType == Convert.ToInt32(SCAEnums.ObjectType.Patient, CultureInfo.InvariantCulture))] = d;
                });
            }

            if (lstCarehome != null && lstCarehome.Any())
            {
                lstCarehome.ForEach(d =>
                {
                    result[lstId.FindIndex(a => a.IndigoId == d.CareHomeId && a.IdType == Convert.ToInt32(SCAEnums.ObjectType.CareHome, CultureInfo.InvariantCulture))] = d;
                });
            }

            if (lstCustomer != null && lstCustomer.Any())
            {
                lstCustomer.ForEach(d =>
                {
                    result[lstId.FindIndex(a => a.IndigoId == d.CustomerId && a.IdType == Convert.ToInt32(SCAEnums.ObjectType.Customer, CultureInfo.InvariantCulture))] = d;
                });

            }

            result.RemoveAll(q => q == null);

            return result;
        }

        /// <summary>
        /// Interpretd user input for FTS
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        private string InterpretGoogleSearchUserInput(string searchText, bool isIdSearch = false)
        {
            var separatedCharacter = ',';
            var processedSearchText = "";

            searchText = IsInvalidSearch(searchText);

            if (string.IsNullOrEmpty(searchText))
                return processedSearchText;

            string[] words = searchText.Split(separatedCharacter);
           
            if (!isIdSearch)
            {
                words.ToList().ForEach(d =>
                {
                    if (!string.IsNullOrEmpty(d))
                        processedSearchText += " \"" + d.Trim() + "*\" AND ";
                });
            }
            else
            {
                words.ToList().ForEach(d =>
                {
                    if (!string.IsNullOrEmpty(d))
                        processedSearchText +=  d.Trim() + " AND ";
                });
            }
           

            processedSearchText = processedSearchText.Substring(0, processedSearchText.Length - 4);

            return processedSearchText.Trim();
        }

        /// <summary>
        /// Validate user customers
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="userCustomerRepository"></param>
        /// <returns></returns>
        private bool ValidateUserCustomer(string customerId, Guid userId, IIDXUserCustomerRepository userCustomerRepository, IIDXUserRoleRepository userRoleRepository, ICustomerRepository customerRepository, out string validCustomers)
        {
            customerId = IsInvalidSearch(customerId);
            validCustomers = "";

            var isSCAAdminUser = IsSCAAdminUser(userId, userRoleRepository);
            var totalNoOfCustomers = customerRepository.GetAllCustomer().Count();
            var isAllCustomerSelected = IsAllCustomer(customerId.Split(',').Count(), totalNoOfCustomers);

            if (isSCAAdminUser)
            {
                if (isAllCustomerSelected)
                    customerId = "";
                else
                    validCustomers = customerId;
                return true;
            }

            //Not admin user
            var lstCustomerId = GetCustomerList(userId, userCustomerRepository);

            //user has no customers
            if (lstCustomerId.Count == 0)
                return false;

            var isAllCustomerAssigned = IsAllCustomer(lstCustomerId.Count(), totalNoOfCustomers);

			if (isAllCustomerAssigned && isAllCustomerSelected)
            {
				customerId = "";
				validCustomers = customerId;
				return true;
			}

			customerId = string.IsNullOrEmpty(customerId) ?
				string.Join(",", lstCustomerId) :
				SelectValidCustomers(customerId.Split(',').ToList(), lstCustomerId);

			validCustomers = customerId;
			return true;
        }

        private bool IsSCAAdminUser(Guid userId, IIDXUserRoleRepository userRoleRepository)
        {
            var isSCAAdminUser = false;
            var userRole = userRoleRepository.GetUserRoleByUserId(userId).Select(q => q.RoleID).FirstOrDefault();

            if (userRole == CommonConstants.SCAAdministratorRoleId)
                isSCAAdminUser = true;

            return isSCAAdminUser;
        }

        private bool IsAllCustomer(Int32 noOfCustomer, Int32 totalNoOfCustomers)
        {
            var isAllCustomerSelecetd = false;
            if (noOfCustomer == totalNoOfCustomers)
                isAllCustomerSelecetd = true;

            return isAllCustomerSelecetd;
        }

        /// <summary>
        /// Check users has access to specified customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="userCustomerRepository"></param>
        /// <returns></returns>
        private string SelectValidCustomers(List<string> customerId, List<long> lstValidCustomer)
        {
            List<long> lstUserSelectedCustomer = new List<long>();

            customerId.ForEach(q =>
                {
                    lstUserSelectedCustomer.Add(Convert.ToInt64(q, CultureInfo.InvariantCulture));
                });

            var validCustomer = lstUserSelectedCustomer.Intersect(lstValidCustomer);

            var strValidCustomer = string.Join(",", validCustomer);
            return strValidCustomer;
        }

        /// <summary>
        /// Gets CustomerId list for which user has access to
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userCustomerRepository"></param>
        /// <returns></returns>
        private List<long> GetCustomerList(Guid userId, IIDXUserCustomerRepository userCustomerRepository)
        {
            return userCustomerRepository.GetCustomersByUserId(userId).Select(q => q.CustomerId).ToList();
        }

        /// <summary>
        /// Gets CarehomeId for users if users is a carehome user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="carehomeId"></param>
        /// <param name="userRepository"></param>
        /// <param name="userCarehomeRepository"></param>
        /// <returns>IsCarehomeUser</returns>
        private bool GetCarehomeForUser(string userId, out string carehomeId, IUsersRepository userRepository, IIDXUserCareHomeRepository userCarehomeRepository)
        {
            carehomeId = "";
            var isValid = false;
            var user = userRepository.GetUsersByUserId(userId).Select(q => q.IsCarehomeUser).ToList();

            if (user.Any() && user.First())
            {
                isValid = true;
                var lstCarhomeId = userCarehomeRepository.FindByUserId(userId).Select(q => q.CarehomeId).ToList();
                carehomeId = string.Join(",", lstCarhomeId.ToArray());
            }
            return isValid;
        }

        private bool VerifySortingForSearch(string text)
        {
            List<string> value = text.Trim().ToUpper().Split(',').ToList();
            var flage = false;
            value.ForEach(q =>
            {
                switch (q)
                {
                    case "ASC":
                    case "DESC":
                    case "LASTNAME":
                    case "CAREHOMENAME":
                    case "PATIENTTYPE":
                    case "POSTCODE":
                    case "DATEOFBIRTH":
                    case "DELIVERYDATE":
                        flage = true;
                        break;
                    default:
                        return;
                }
            });
            return flage;
        }      
    }
}
