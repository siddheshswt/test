﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Arvind Nishad
// Created          : 06-04-2015
//
// ***********************************************************************
// <copyright file="CareHome.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Carehome Class</summary>
// ***********************************************************************
namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel;

    using BusinessModels;
    using Interfaces;
    using Common;
    using Common.Enums;
    using Infrastructure.Repositories;
    using Model;
    using EFModel = Model;

    /// <summary>
    /// Common Service Business Class
    /// </summary>    
    public class CareHome : ICareHome
    {
        /// <summary>
        /// The date format
        /// </summary>
        public const string DateFormat = "yyyy-MM-dd";

        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = string.Empty;

        /// <summary>
        /// Flag to check if incremental update required
        /// </summary>
        private bool isIncrementalUpdateRequired;

        /// <summary>
        /// Save CareHome Information
        /// </summary>
        /// <param name="objCareHomeBusinessModel">CareHomeBusinessModel objCareHomeBusinessModel</param>
        /// <returns>
        /// Boolean
        /// </returns>
        public long SaveCareHome(CareHomeBusinessModel objCareHomeBusinessModel)
        {
            ICareHomeRepository careHomeRepository = new CareHomeRepository();
            IUnitOfWork unitOfWork = careHomeRepository.UnitOfWork;

            try
            {
                isIncrementalUpdateRequired = CheckIncrementalUpdateRequired(objCareHomeBusinessModel.CareHomeId, objCareHomeBusinessModel);
                if (objCareHomeBusinessModel != null)
                {
                    userId = objCareHomeBusinessModel.UserId;
                    var userGuid = Guid.Parse(objCareHomeBusinessModel.UserId);
                    var modifiedDate = DateTime.Now;

                    PersonalInformation objPersonalInfo;
                    EFModel.CareHome objCareHome;
                    Address objAddress;
                    // Id
                    if (objCareHomeBusinessModel.CareHomeId > 0)
                    {
                        objCareHome = careHomeRepository.GetCareHome(objCareHomeBusinessModel.CareHomeId).FirstOrDefault();
                        objPersonalInfo = objCareHome.PersonalInformation;
                        objAddress = objPersonalInfo.Address;
                    }
                    else
                    {
                        objCareHome = new EFModel.CareHome();
                        objPersonalInfo = new EFModel.PersonalInformation();
                        objAddress = new EFModel.Address();
                    }
                    ///Set the Value for Address
                    SetAddressDetails(objCareHomeBusinessModel, userGuid, modifiedDate, objAddress);

                    /// set the value for PersonalInformation                    
                    SetPresonalInformationDetails(objCareHomeBusinessModel, userGuid, modifiedDate, objPersonalInfo, objAddress);

                    ///set the value for communication preference
                    SetPreferences(objCareHomeBusinessModel, modifiedDate, userGuid, objCareHome);

                    /// set the value for careHome
                    SetCareHomeDetails(objCareHomeBusinessModel, userGuid, modifiedDate, objPersonalInfo, objCareHome);
                    careHomeRepository.InsertOrUpdate(objCareHome);
                    unitOfWork.Commit();
                    if (isIncrementalUpdateRequired)
                    {
                        var incrementalUpdateObj = new IncrementalUpdateSearchIndex();
                        incrementalUpdateObj.CreateIncrementalUpdateIndexXml(objCareHome.CareHomeId, (long)SCAEnums.ObjectType.CareHome);
                    }
                    return objCareHome.CareHomeId;
                }
                return 0;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.userId);
                throw;
            }
            finally
            {
                if (careHomeRepository != null)
                {
                    careHomeRepository.Dispose();
                }
            }
            return 0;
        }

        /// <summary>
        /// Sets the preferences.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="prescriptionRepository">The prescription repository.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPatient">The object patient.</param>
        private void SetPreferences(CareHomeBusinessModel objCareHomeBusinessModel, DateTime currentDateTime, Guid modifiedBy, EFModel.CareHome objCarehome)
        {
            DeletePreferences(objCarehome, objCareHomeBusinessModel);
            var carehomePreferences = objCarehome.IDXPreferences;

            //Create Communication Preferences List
            if (objCareHomeBusinessModel.CommunicationPreferences != null)
            {
                foreach (var communicationFormatId in objCareHomeBusinessModel.CommunicationPreferences)
                {
                    var communicationFormat = Convert.ToInt64(communicationFormatId, CultureInfo.InvariantCulture);
                    if (!carehomePreferences.Any(q => q.Preference != null && (q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture) && q.Preference.Value == communicationFormat)))
                    {
                        var objIdxPreference = new IDXPreference();
                        objIdxPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture);
                        objIdxPreference.CarehomeId = objCarehome.CareHomeId;
                        objIdxPreference.Preference = Convert.ToInt64(communicationFormatId, CultureInfo.InvariantCulture);
                        objIdxPreference.ModifiedBy = modifiedBy;
                        objIdxPreference.ModifiedDate = currentDateTime;
                        objCarehome.IDXPreferences.Add(objIdxPreference);
                    }
                }
            }
        }

        /// <summary>
        /// Deletes the preferences.
        /// </summary>
        /// <param name="objPatient">The object patient.</param>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        private void DeletePreferences(EFModel.CareHome objCarehome, CareHomeBusinessModel objCareHomeBusinessModel)
        {
            //Delete CommunicationPreference 
            IDXPreference idxPreference;
            if (objCareHomeBusinessModel.CommunicationPreferences != null)
            {
                var selectedPreference = new List<long>();
                var communicationPreferenceList = objCarehome.IDXPreferences.Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture)).ToList();
                objCareHomeBusinessModel.CommunicationPreferences.ToList().ForEach(q => selectedPreference.Add(Convert.ToInt64(q, CultureInfo.InvariantCulture)));
                var dbPreferencesList = communicationPreferenceList.Select(q => q.Preference.Value).ToList();
                var removedPreferenceId = dbPreferencesList.Except(selectedPreference).ToList();
                removedPreferenceId.ForEach(d =>
                {
                    idxPreference = communicationPreferenceList.Find(q => q.Preference == d);
                    objCarehome.IDXPreferences.Remove(idxPreference);
                });
            }
        }

        /// <summary>
        /// Get carehome details
        /// </summary>
        /// <param name="careHomeId">string careHomeId</param>
        /// <param name="struserId">string userId</param>
        /// <returns>
        /// Return Care Home Business Model
        /// </returns>
        public CareHomeBusinessModel GetCareHome(string careHomeId, string userId)
        {
            if (!string.IsNullOrEmpty(careHomeId))
            {
                try
                {
                    ///Set CustomerId 0 when carehome specific details need to be loaded
                    long customerId = 0;
                    var careHomedata = (new DBHelper()).GetCarehomeDetails(customerId, Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture), userId).FirstOrDefault();
                    if (careHomedata != null)
                    {
                        var objCareHomeBusinessModel = new CareHomeBusinessModel();
                        ///Carehome information
                        objCareHomeBusinessModel.CareHomeId = careHomedata.CareHomeId;
                        objCareHomeBusinessModel.SapCareHomeNumber = !string.IsNullOrEmpty(careHomedata.SAPCareHomeNumber) ? careHomedata.SAPCareHomeNumber.TrimStart('0') : careHomedata.SAPCareHomeNumber;
                        objCareHomeBusinessModel.CustomerId = Convert.ToInt64(careHomedata.CustomerId, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.CustomerName = careHomedata.CustomerName;
                        objCareHomeBusinessModel.DeliveryFrequency = careHomedata.DeliveryFrequency;
                        objCareHomeBusinessModel.RoundId = careHomedata.RoundId;                        
                        objCareHomeBusinessModel.NextDeliveryDate = Convert.ToString(careHomedata.NextDeliveryDate, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.UserId = careHomedata.UserName;
                        objCareHomeBusinessModel.ModifiedDate = Convert.ToString(careHomedata.ModifiedDate, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.Round = careHomedata.CareHomeRound;
                        objCareHomeBusinessModel.BillTo = Convert.ToInt64(careHomedata.BillTo, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.CareHomeType = careHomedata.CareHomeType;
                        objCareHomeBusinessModel.OrderType = careHomedata.OrderType;
                        objCareHomeBusinessModel.PurchaseOrderNo = careHomedata.PurchaseOrderNo;
                        objCareHomeBusinessModel.CareHomeStatus = careHomedata.CareHomeStatus;
                        objCareHomeBusinessModel.CommunicationFormat = careHomedata.CommunicationFormat;
                        objCareHomeBusinessModel.CarehomeCommunicationPreferences = careHomedata.CommunicationPreferences;

                        /// Personal Information
                        objCareHomeBusinessModel.PersonalInformationId = Convert.ToInt64(careHomedata.PersonalInformationId, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.CareHomeName = careHomedata.CareHomeName;
                        objCareHomeBusinessModel.PhoneNo = Convert.ToString(careHomedata.TelephoneNumber, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.MobileNo = Convert.ToString(careHomedata.MobileNumber, CultureInfo.InvariantCulture);
                        objCareHomeBusinessModel.EmailAddress = careHomedata.Email;
                        objCareHomeBusinessModel.FaxNumber = careHomedata.FaxNumber;

                        /// Address Information
                        objCareHomeBusinessModel.AddressId = careHomedata.AddressId;
                        objCareHomeBusinessModel.HouseNumber = careHomedata.HouseNumber;
                        objCareHomeBusinessModel.HouseName = careHomedata.HouseName;
                        objCareHomeBusinessModel.Address1 = careHomedata.AddressLine1;
                        objCareHomeBusinessModel.Address2 = careHomedata.AddressLine2;
                        objCareHomeBusinessModel.City = careHomedata.City;
                        objCareHomeBusinessModel.County = careHomedata.County;
                        objCareHomeBusinessModel.Country = careHomedata.Country;
                        objCareHomeBusinessModel.CountyCode = careHomedata.CountyCode;
                        objCareHomeBusinessModel.PostCode = careHomedata.PostCode;
                        objCareHomeBusinessModel.SAPCustomerNumber = string.IsNullOrEmpty(careHomedata.CustomerSAPNumber) ? string.Empty : CommonHelper.TrimLeadingZeros(careHomedata.CustomerSAPNumber);
                        objCareHomeBusinessModel.HasAnyActiveOrderForOrderType = HasAnyActiveOrderForOrderType(Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture), Convert.ToInt64(careHomedata.OrderType, CultureInfo.InvariantCulture));
                        return objCareHomeBusinessModel;
                    }
                }
                catch (DataException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (FormatException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (InvalidOperationException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (SystemException e)
                {
                    LogHelper.LogException(e, userId);
                    throw;
                }
                catch (Exception e)
                {
                    LogHelper.LogException(e, userId);
                    throw;
                }
            }
            return null;
        }

        /// <summary>
        /// Set Care Home Details
        /// </summary>
        /// <param name="objCareHomeBusinessModel">CareHomeBusinessModel objCareHomeBusinessModel</param>
        /// <param name="UserId">Guid _userId</param>
        /// <param name="currentDate">DateTime currentDate</param>
        /// <param name="objPersonalInfo">PersonalInformation objPersonalInfo</param>
        /// <param name="objCareHome">EFModel.CareHome objCareHome</param>
        private static void SetCareHomeDetails(CareHomeBusinessModel objCareHomeBusinessModel, Guid UserId, DateTime currentDate, PersonalInformation objPersonalInfo, EFModel.CareHome objCareHome)
        {
            objCareHome.CareHomeId = objCareHomeBusinessModel.CareHomeId;
            objCareHome.CustomerId = objCareHomeBusinessModel.CustomerId;
            objCareHome.DeliveryFrequency = objCareHomeBusinessModel.DeliveryFrequency;
            objCareHome.RoundId = objCareHomeBusinessModel.RoundId;
            objCareHome.Round = objCareHomeBusinessModel.Round;

            objCareHome.NextDeliveryDate = CommonHelper.ConvertDateToMMddyyyy(objCareHomeBusinessModel.NextDeliveryDate);
            objCareHome.CareHomeStatus = objCareHomeBusinessModel.CareHomeStatus;
            objCareHome.OrderType = objCareHomeBusinessModel.OrderType;

            ///It is removed by SCA
            ///objCareHome.CareHomeType = objCareHomeBusinessModel.CareHomeType;
            objCareHome.CommunicationFormat = objCareHomeBusinessModel.CommunicationFormat;
            objCareHome.PurchaseOrderNo = objCareHomeBusinessModel.PurchaseOrderNo;
            objCareHome.ModifiedBy = UserId;
            objCareHome.ModifiedDate = currentDate;
            objCareHome.IsSentToSAP = false;
            objCareHome.PersonalInformation = objPersonalInfo;
        }

        /// <summary>
        /// Set Presonal Information Details
        /// </summary>
        /// <param name="objCareHomeBusinessModel">The object care home business model.</param>
        /// <param name="UserId">Guid _userId</param>
        /// <param name="currentDate">DateTime currentDate</param>
        /// <param name="objPersonalInfo">PersonalInformation objPersonalInfo</param>
        /// <param name="objAddress">Address objAddress</param>
        private static void SetPresonalInformationDetails(CareHomeBusinessModel objCareHomeBusinessModel, Guid UserId, DateTime currentDate, PersonalInformation objPersonalInfo, Address objAddress)
        {
            objPersonalInfo.FirstName = objCareHomeBusinessModel.CareHomeName;
            ///  objPersonalInfo.AddressId = objAddress.AddressId;
            objPersonalInfo.Address = objAddress;
            objPersonalInfo.TelephoneNumber = objCareHomeBusinessModel.PhoneNo;
            objPersonalInfo.MobileNumber = objCareHomeBusinessModel.MobileNo;
            objPersonalInfo.FaxNumber = objCareHomeBusinessModel.FaxNumber;
            objPersonalInfo.Email = objCareHomeBusinessModel.EmailAddress;
            if (objCareHomeBusinessModel.CareHomeId < 1)
            {
                objPersonalInfo.CreatedById = UserId;
                objPersonalInfo.CreatedDateTime = currentDate;
            }
            objPersonalInfo.ModifiedBy = UserId;
            objPersonalInfo.ModifiedDate = currentDate;

        }

        /// <summary>
        /// Set Address Details
        /// </summary>
        /// <param name="objCareHomeBusinessModel">CareHomeBusinessModel objCareHomeBusinessModel</param>
        /// <param name="userId">Guid _userId</param>
        /// <param name="currentDate">DateTime currentDate</param>
        /// <param name="objAddress">Address objAddress</param>
        private static void SetAddressDetails(CareHomeBusinessModel objCareHomeBusinessModel, Guid userId, DateTime currentDate, Address objAddress)
        {
            objAddress.HouseNumber = objCareHomeBusinessModel.HouseNumber;
            objAddress.HouseName = objCareHomeBusinessModel.HouseName;
            objAddress.AddressLine1 = objCareHomeBusinessModel.Address1;
            objAddress.AddressLine2 = objCareHomeBusinessModel.Address2;
            objAddress.City = objCareHomeBusinessModel.City;
            objAddress.County = objCareHomeBusinessModel.County;
            objAddress.Country = objCareHomeBusinessModel.Country;
            objAddress.PostCode = objCareHomeBusinessModel.PostCode;
            objAddress.ModifiedBy = userId;
            objAddress.ModifiedDate = currentDate;
        }

        /// <summary>
        /// Get CareHome Patient Details
        /// </summary>
        /// <param name="careHomeId">string careHomeId</param>
        /// <param name="patientUserId">string userId</param>
        /// <returns>
        /// return Care Home Business Model
        /// </returns>
        public List<PatientBusinessModel> GetPatientDetails(string careHomeId, string patientUserId)
        {
            if (string.IsNullOrEmpty(careHomeId))
            {
                return null;
            }


            try
            {
                ///long CustomerId = 0;
                var careHomedata = (new DBHelper()).GetCarehomePatientDetails(Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture));
                var patientBusinessModelList = careHomedata.Select(patientDetails => new PatientBusinessModel
                {
                    CustomerName = patientDetails.CustomerName,
                    PatientId = patientDetails.PatientId,
                    PatientStatusCode = patientDetails.PatientStatus,
                    SAPPatientNumber = CommonHelper.TrimLeadingZeros(patientDetails.SAPPatientNumber),
                    SAPCustomerNumber = patientDetails.SAPCustomerNumber,
                    SAPProductId = patientDetails.SAPProductID,
                    FirstName = patientDetails.FirstName,
                    LastName = patientDetails.LastName,
                    PatientTypeCode = patientDetails.PatientType,
                    ProductDescription = patientDetails.ProductName,
                    DeliveryFrequency = Convert.ToString(patientDetails.Frequency, CultureInfo.InvariantCulture),
                    Quantity = patientDetails.Quantity != null ? Convert.ToInt64(patientDetails.Quantity, CultureInfo.InvariantCulture) : 0,
                    DeliveryDate = patientDetails.DeliveryDate != null ? Convert.ToDateTime(patientDetails.DeliveryDate, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
                    PatientStatus = patientDetails.PatientStatusCode
                }).ToList();
                return patientBusinessModelList;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, patientUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, patientUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, patientUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, patientUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, patientUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, patientUserId);
                throw;
            }
            return null;
        }

        /// <summary>
        /// Get CareHome Report Data
        /// </summary>
        /// <param name="careHomeId">string careHomeId</param>
        /// <param name="patientUserId">string userId</param>
        /// <returns>
        /// return Care Home Report Data
        /// </returns>
        /// <returns></returns>
        public List<PatientBusinessModel> GetCareHomeReportData(string careHomeId, string patientUserId)
        {
            bool isExport = false;
            long page = 1;
            long rows = 100;
            if (string.IsNullOrEmpty(careHomeId))
            {
                return null;
            }

            try
            {
				var careHomedata = (new DBHelper()).GetCarehomeReportData(Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture), isExport, page, rows, null, "DESC");
                var patientBusinessModelList = careHomedata.Select(ReportDetails => new PatientBusinessModel
                {
                    FirstName = ReportDetails.PatientName,
                    SAPPatientNumber = ReportDetails.PatientSAPId,
                    DateOfBirth = ReportDetails.DateOfBirth,
                    PatientStatusCode = ReportDetails.PatientStatus,
                    AssessmentDate = ReportDetails.LastAssessDate,
                    NextAssessmentDate = ReportDetails.NextAssessDate,
                    SAPProductId = ReportDetails.SAPProductId,
                    ProductDescription = ReportDetails.ProductDescription,
                    AssessedPadsPerDay = ReportDetails.AssPPD,
                    ActPPD = ReportDetails.ActPPD,
                    DeliveryFrequency = ReportDetails.Frequency,
                    NextDeliveryDate = ReportDetails.NDD,
                    Comment = ReportDetails.Comment
                }).ToList();
                return patientBusinessModelList;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return null;
        }


        /// <summary>
        /// Check Has AnyActiveOrder For OrderType
        /// </summary>
        /// <param name="carehomeId"></param>
        /// <param name="carehomeOrderType"></param>
        /// <returns></returns>
        public bool HasAnyActiveOrderForOrderType(long carehomeId, long carehomeOrderType)
        {
            IOrderRepository carehomeOrderRepository = null;
            var isAnyActiveOrder = false;
            try
            {
                carehomeOrderRepository = new OrderRepository();
                if (carehomeId != 0)
                {
                    var orders = carehomeOrderRepository.GetCarehomeOrders(Convert.ToInt64(carehomeId, CultureInfo.InvariantCulture)).ToList();
                    if (carehomeOrderType == (long)SCAEnums.CareHomeOrderType.PatientLevel)
                    {
                        if (orders.Any(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) && (bool)q.IsRushFlag == false && (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP))
                        {
                            isAnyActiveOrder = true;
                        }
                    }
                    else if (carehomeOrderType == (long)SCAEnums.CareHomeOrderType.ProductLevel)
                    {
                        if (orders.Any(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) && (bool)q.IsRushFlag == false && (long)q.OrderType == (long)SCAEnums.OrderType.ZHDH))
                        {
                            isAnyActiveOrder = true;
                        }
                    }
                }
                return isAnyActiveOrder;
            }
            finally
            {
                if (carehomeOrderRepository != null)
                {
                    carehomeOrderRepository.Dispose();
                }
            }
        }
        /// <summary>
        /// Gets the patient Report.
        /// </summary>
        /// <param name="patientData">The patient data.</param>
        /// <returns>carehomeamintenace Report Business Model</returns>
        public PatientBusinessModel GetCareHomeReportMaintenaceData(PatientBusinessModel patientData)
        {
            IPatientRepository objPatientRepository = null;
            if (patientData == null)
            {
                return new PatientBusinessModel();
            }
            var userId = Convert.ToString(patientData.LoggedInUserId, CultureInfo.InvariantCulture); 
            try
            {
                objPatientRepository = new PatientRepository();
				var carehomeMaintainance = new PatientBusinessModel
                {
					CarehomeMaintanceReport = GetCareHomeMaintainanceReportlog(patientData)
                };

				return carehomeMaintainance;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (objPatientRepository != null)
                {
                    objPatientRepository.Dispose();
                }
            }
            return new PatientBusinessModel(); ;
        }

        /// <summary>
        /// Gets the care Home Report.
        /// </summary>
        /// <param name="patientRequestData">The care home request data.</param>
        /// <returns>carehomeamintenace Report Business Model</returns>
        private static List<CarehomeMaintenanceReportBusinessModel> GetCareHomeMaintainanceReportlog(PatientBusinessModel patientRequestData)
        {
            var userId = patientRequestData.LoggedInUserId.ToString();
            try
            {               
                var patientLogSpParameteres = new
                {
                    PageNumber = patientRequestData.PageNumber,
                    RecordsPerPage = patientRequestData.RecordsPerPage,                    
                    CareHomeId = Convert.ToInt64(patientRequestData.CareHomeId, CultureInfo.InvariantCulture),                    
                    IsExport = patientRequestData.IsExport,
                    Sortcolumn = patientRequestData.SortColumn,
                    SortBy = patientRequestData.SortBy                    
                };

                var objcarehomemaintenace = (new DBHelper()).GetCarehomeReportData(
                    patientLogSpParameteres.CareHomeId,
                    patientLogSpParameteres.IsExport,
                    patientLogSpParameteres.RecordsPerPage,
                    patientLogSpParameteres.PageNumber,
                    patientLogSpParameteres.Sortcolumn,
                    patientLogSpParameteres.SortBy).ToList();                

                if (objcarehomemaintenace.Any())
                {
                    var carehomelog = objcarehomemaintenace.Select(q => new CarehomeMaintenanceReportBusinessModel
                    {                       
                        PatientName = q.PatientName,
                        PatientSAPId = q.PatientSAPId,
                        DateOfBirth = q.DateOfBirth,
                        PatientStatus = q.PatientStatus,
                        SAPProductId = q.SAPProductId,
                        ProductDescription = q.ProductDescription,
                        LastAssessDate = q.LastAssessDate,
                        NextAssessDate = q.NextAssessDate,
                        Frequency = q.Frequency,
                        ActPPD = q.ActPPD,
                        AssPPD = q.AssPPD,
                        Comment = q.Comment,
                        NDD = q.NDD,
                        TotalRecords = Convert.ToInt64(q.TotalRecords, CultureInfo.InvariantCulture),
                        PatientId = Convert.ToInt64(q.PatientId,CultureInfo.InvariantCulture)
                    }).ToList();
                    return carehomelog;
                }
                return new List<CarehomeMaintenanceReportBusinessModel>();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return new List<CarehomeMaintenanceReportBusinessModel>();
        }

        /// <summary>
        /// Get list of carehome name and id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId">Customer identifier</param>
        /// <returns>
        /// Return list of products
        /// </returns>
        public List<DropDownDataBusinessModel> GetCareHomeOnCustomerId(string customerId, string userId)
        {
            try
            {
                var customerDetails = (new DBHelper()).GetCareHomeOnCustomerId(customerId, userId);
                var result = customerDetails.Select(item => new DropDownDataBusinessModel()
                {
                    Value = item.CareHomeId,
                    DisplayText = item.CareHomeName
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return null;
        }

        /// <summary>
        /// Check if incremental update required
        /// </summary>
        /// <param name="careHomeId"></param>
        /// <param name="objCareHomeBusinessModel"></param>
        /// <returns></returns>
        public bool CheckIncrementalUpdateRequired(long careHomeId, CareHomeBusinessModel objCareHomeBusinessModel)
        {
            ICareHomeRepository carehomeRepository = null;
            try
            {
                isIncrementalUpdateRequired = false;
                //CareHomeBusinessModel oldCareHomeBusinessModel;
                if (careHomeId != 0)
                {
                    carehomeRepository = new CareHomeRepository();
                    var oldCareHomeBusinessModel = carehomeRepository.Find(careHomeId);

                    var newNDD = !string.IsNullOrEmpty(objCareHomeBusinessModel.NextDeliveryDate) ? CommonHelper.ConvertDateToMMddyyyy(objCareHomeBusinessModel.NextDeliveryDate) : default(DateTime);
                    var oldNDD = oldCareHomeBusinessModel.NextDeliveryDate.HasValue ? oldCareHomeBusinessModel.NextDeliveryDate.Value.Date : default(DateTime);

                    if ((oldCareHomeBusinessModel.PersonalInformation.Address.PostCode != objCareHomeBusinessModel.PostCode) ||
                        (oldCareHomeBusinessModel.PersonalInformation.FirstName != objCareHomeBusinessModel.CareHomeName) ||
                        (oldCareHomeBusinessModel.PersonalInformation.TelephoneNumber != objCareHomeBusinessModel.PhoneNo) ||
                        (oldNDD != newNDD)
                        )
                    {

                        isIncrementalUpdateRequired = true;
                    }
                    else
                    {

                        isIncrementalUpdateRequired = false;
                    }


                }
                else
                {

                    isIncrementalUpdateRequired = true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (carehomeRepository != null)
                {
                    carehomeRepository.Dispose();
                }
            }
            return isIncrementalUpdateRequired;
        }
    }
}
