﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Arvind Nishad
// Created          : 29-04-2015
//
// ***********************************************************************
// <copyright file="Notes.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Notes Class</summary>
// ***********************************************************************
namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;
    using System.ServiceModel;
    using System.Linq;
    using System.Text;
    using System.IO; //git commit
    /// <summary>
    /// Note Service Business Class
    /// </summary>    
    public class Notes : INotes
    {
        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = string.Empty;
        /// <summary>
        /// The date time format
        /// </summary>
        public const string DateTimeFormat = "dd/MM/yyyy HH:mm";

        /// <summary>
        /// Save Note Information
        /// </summary>
        /// <param name="objNotesBusinessModel">NotesBusinessModel objNotesBusinessModel</param>
        /// <returns>
        /// Note Id
        /// </returns>
        public long SaveNote(NotesBusinessModel objNotesBusinessModel)
        {
            INotesRepository noteRepository = new NotesRepository();
            IUnitOfWork unitOfWork = noteRepository.UnitOfWork;
            try
            {
                if (objNotesBusinessModel != null)
                {
                    globalUserId = objNotesBusinessModel.CreatedBy;
                    var _userId = Guid.Parse(objNotesBusinessModel.CreatedBy);
                    var currentDate = DateTime.Now;

                    /// set the value for Note                    
                    var objNote = new Note();                        
                    objNote.NoteType = objNotesBusinessModel.NoteType;
                    objNote.NoteTypeId = objNotesBusinessModel.NoteTypeId;
                    objNote.NoteText = objNotesBusinessModel.NoteText;
                    objNote.CreatedBy = _userId;
                    objNote.CreatedDate = currentDate;                    
                    objNote.ModifiedBy = _userId;
                    objNote.ModifiedDate = currentDate;
                    noteRepository.InsertOrUpdate(objNote);
                    unitOfWork.Commit();
                    return objNote.NoteId;
                }
                return 0;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (noteRepository != null)
                {
                    noteRepository.Dispose();
                }
            }

            return 0;
        }

        /// <summary>
        /// Save Note Information For Copy Patient
        /// </summary>
        /// <param name="objNotesBusinessModel">NotesBusinessModel objNotesBusinessModel</param>
        /// <returns>
        /// Note Id
        /// </returns>
        public void SaveNoteForCopyPatient(List<NotesBusinessModel> objNotesBusinessModel)
        {
            INotesRepository noteRepository = new NotesRepository();
            IUnitOfWork unitOfWork = noteRepository.UnitOfWork;
            try
            {
                if (objNotesBusinessModel != null)
                {
                    foreach (var notes in objNotesBusinessModel)
                    {
                        globalUserId = notes.CreatedBy;
                        var _userId = Guid.Parse(notes.CreatedBy);
                        var currentDate = DateTime.Now;

                        /// set the value for Note                    
                        var objNote = new Note();
                        objNote.NoteType = notes.NoteType;
                        objNote.NoteTypeId = notes.NoteTypeId;
                        objNote.NoteText = notes.NoteText;
                        objNote.CreatedBy = _userId;
                        objNote.CreatedDate = currentDate;
                        objNote.ModifiedBy = _userId;
                        objNote.ModifiedDate = currentDate;
                        objNote.DisplayAsAlert = notes.IsDisplayAsAlert;
                        noteRepository.InsertOrUpdate(objNote);
                        unitOfWork.Commit();                        
                    }
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (noteRepository != null)
                {
                    noteRepository.Dispose();
                }
            }
        } 

        /// <summary>
        /// Save note alert details
        /// </summary>
        /// <param name="objNotesBusinessModel">List object of NotesBusinessModel class</param>
        /// <returns></returns>
        public long SaveAlert(List<NotesBusinessModel> objNotesBusinessModel)
        {
            var currentDate = DateTime.Now;
            try
            {
                if (objNotesBusinessModel != null && objNotesBusinessModel.Any())
                {
                    globalUserId = objNotesBusinessModel.Select(q => q.ModifiedBy).FirstOrDefault();
                    using (INotesRepository noteRepository = new NotesRepository())
                    {
                        IUnitOfWork unitOfWork = noteRepository.UnitOfWork;

                        var noteIdList = objNotesBusinessModel.Select(q => q.NoteId).ToList();
                        var getAllNotes = noteRepository.FindAll(noteIdList).ToList();
                        if (getAllNotes.Any())
                        {
                            for (int index = 0; index < objNotesBusinessModel.Count; index++)
                            {
                                var notesBusinessModel = objNotesBusinessModel[index];
                                var note = getAllNotes.FirstOrDefault(q => q.NoteId == notesBusinessModel.NoteId);
                                if (note != null)
                                {
                                    note.DisplayAsAlert = notesBusinessModel.IsDisplayAsAlert;
                                    note.ModifiedBy = Guid.Parse(notesBusinessModel.ModifiedBy);
                                    note.ModifiedDate = currentDate;
                                    noteRepository.InsertOrUpdate(note);
                                }
                            }
                            unitOfWork.Commit();
                            return 1;
                        }
                    }
                }
                return 0;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            return 0;
        }

        /// <summary>
        /// Get notes details
        /// </summary>
        /// <param name="noteType">Type of the note.</param>
        /// <param name="noteTypeId">The note type identifier.</param>
        /// <param name="userId">string userId</param>
        /// <param name="alertNotes">string alertNotes</param>
        /// <returns>
        /// Returns list of NotesBusinessModel class object
        /// </returns>
        public List<NotesBusinessModel> GetNotes(string noteType, string noteTypeId, string userId, string alertNotes)
        {
            string alert = "true";
            if (!string.IsNullOrEmpty(noteType) && !string.IsNullOrEmpty(noteTypeId))
            {
                try
                {
                    if (alertNotes.ToUpper(CultureInfo.InvariantCulture) == alert.ToUpper(CultureInfo.InvariantCulture))
                    {
                        return GetAllNotes(noteType, noteTypeId, true);
                    }
                    else
                    {
                        return GetAllNotes(noteType, noteTypeId, false);
                    }
                }
                catch (DataException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (FormatException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (InvalidOperationException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (SystemException e)
                {
                    LogHelper.LogException(e, userId);
                    throw;
                }
                catch (Exception e)
                {
                    LogHelper.LogException(e, userId);
                    throw;
                }
            }
            return null;
        }

        /// <summary>
        /// Get all notes details from database 
        /// </summary>
        /// <param name="noteType">Type of the note</param>
        /// <param name="noteTypeId">The note type identifier</param>
        /// <param name="isAlertNote">True/False</param>
        /// <returns>Returns list of NotesBusinessModel class object</returns>
        private List<NotesBusinessModel> GetAllNotes(string noteType, string noteTypeId, bool isAlertNote)
        {
            string noteFrom = "(noteFrom)";
            string system = "System";
            string htmlStrong = "<strong>";
            string htmlStrongClose = "</strong>";
            string space = "   ";

            var responseNotes = new List<NotesBusinessModel>();
            var notesList = (new DBHelper()).GetNotes(Convert.ToInt64(noteType, CultureInfo.InvariantCulture), Convert.ToInt64(noteTypeId, CultureInfo.InvariantCulture));
            if (notesList != null && notesList.Any())
            {
                if (isAlertNote)
                    notesList = notesList.FindAll(row => row.DisplayAsAlert == true);

                foreach (var note in notesList)
                {
                    var mappedUserName = !string.IsNullOrEmpty(note.CreatedBy) ? note.CreatedBy : system; //This is required for default user id.
                    var objNotesBusinessModel = new NotesBusinessModel();
                    objNotesBusinessModel.NoteId = Convert.ToInt64(note.NoteId, CultureInfo.InvariantCulture);
                    objNotesBusinessModel.CreatedBy = mappedUserName;
                    objNotesBusinessModel.CreatedDate = note.CreatedDate != null ? note.CreatedDate.ToShortDateString() : string.Empty;
                    objNotesBusinessModel.NoteType = note.NoteType;
                    objNotesBusinessModel.IsDisplayAsAlert = note.DisplayAsAlert;
                    objNotesBusinessModel.FormattedNote = htmlStrong 
                        + mappedUserName 
                        + space 
                        + (note.CreatedDate != null ? note.CreatedDate.ToString(DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty)
                        + space 
                        + noteFrom 
                        + htmlStrongClose 
                        + Environment.NewLine 
                        + Environment.NewLine 
                        + note.NoteText 
                        + Environment.NewLine 
                        + Environment.NewLine;
                    responseNotes.Add(objNotesBusinessModel);
                }
                return responseNotes;
            }
            return responseNotes;
        }
    }
}
