﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="Sample.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Business
{
    using SCA.Indigo.Business.Interfaces;

    /// <summary>
    /// Sample Test Class
    /// </summary>
    public class Sample : ISample
    {
        /// <summary>
        /// Test for Service
        /// </summary>
        /// <param name="value">Test data</param>
        /// <returns>
        /// return string
        /// </returns>
        public string GetDataJson(string value)
        {
            return "GET:siddhesh";
        }
    }
}