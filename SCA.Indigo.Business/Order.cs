﻿//----------------------------------------------------------------------------------------------
// <copyright file="Order.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
// <summary>This is the Order file.</summary>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
	using System;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Data;
	using System.Globalization;
	using System.Linq;
	using System.Text;
	using SCA.Indigo.Business.BusinessModels;
	using SCA.Indigo.Business.HelixService;
	using SCA.Indigo.Business.Interfaces;
	using SCA.Indigo.Common;
	using SCA.Indigo.Common.Enums;
	using SCA.Indigo.Infrastructure.Repositories;
	using SCA.Indigo.Model;
	using EFModel = SCA.Indigo.Model;
	using System.IO;
	using System.Net.Mail;

	/// <summary>
	/// Order Class
	/// </summary>    
	public class Order : IOrder
	{
		/// <summary>
		/// OnlineTelephoneOrderDirectory
		/// </summary>
		private string onlineTelephoneOrderDirectory = ConfigurationManager.AppSettings["OnlineTelephoneOrderLog"];

		/// <summary>
		/// The date format
		/// </summary>
		public const string DateFormat = "dd/MM/yyyy";

		/// <summary>
		/// The date format
		/// </summary>
		public const string DateFormatHHMMSS = "dd/MM/yyyy HH:mm:ss";

		/// <summary>
		/// The date format
		/// </summary>
		public const string DateFormatHHMMMSS = "dd-MMM-yyyy HH:mm:ss";

		/// <summary>
		/// The country
		/// </summary>
		private const string Country = "UK";
		/// <summary>
		/// The invalid
		/// </summary>
		private const string Invalid = "Invalid";
		/// <summary>
		/// The _lead time for automatic order
		/// </summary>
		private string leadTimeForAutomaticOrder = ConfigurationManager.AppSettings["LeadTimeForAutomaticOrder"];
		/// <summary>
		/// The _new patient round
		/// </summary>
		private string newPatientRound = ConfigurationManager.AppSettings["NewPatientRound"].ToUpper(CultureInfo.InvariantCulture);
		/// <summary>
		/// The _new patient round identifier
		/// </summary>
		private string newPatientRoundId = ConfigurationManager.AppSettings["NewPatientRoundId"].ToUpper(CultureInfo.InvariantCulture);
		/// <summary>
		/// The object common service
		/// </summary> 
		private static CommonService objCommonService = null;
		/// <summary>
		/// The format provider
		/// </summary>
		private IFormatProvider formatProvider = CultureInfo.InvariantCulture;
		/// <summary>
		/// The arranged NDD
		/// </summary>
		private static DateTime arrangedNDD;
		/// <summary>
		/// The HHMM format
		/// </summary>
		private const string HhmmFormat = "HH:mm";
		/// <summary>
		/// The automatic criteria identifier seperator
		/// </summary>
		private const char automaticCriteriaIdSeperator = ';';
		/// <summary>
		/// The OnlineTelephoneOrder
		/// </summary>
		private const string OnlineTelephoneOrder = "OnlineTelephoneOrder";

		/// <summary>
		/// The log server ip address
		/// </summary>
		private static string logServerIPAddress = ConfigurationManager.AppSettings["IPAddress"];
		/// <summary>
		/// The log server path
		/// </summary>
		private static string logServerPath = ConfigurationManager.AppSettings["ServerAddress"];
		/// <summary>
		/// The log directory
		/// </summary>
		private static string logDirectory = ConfigurationManager.AppSettings["LogDirectory"];

		/// <summary>
		/// The OnlineTelephoneOrderLog directory
		/// </summary>
		private static string onlineTelephoneOrderLog = ConfigurationManager.AppSettings["OnlineTelephoneOrderLog"];

        /// <summary>
        /// ADP2 Length
        /// </summary>
        private static int ADP2Length = Convert.ToInt32(ConfigurationManager.AppSettings["ADP2Length"], CultureInfo.InvariantCulture);

        /// <summary>
        /// The community non self care
        /// </summary>
        private List<long> communityNonSelfCare = new List<long>() {
                                                                        (long)SCAEnums.PatientType.Bulk,                                                                         
                                                                        (long)SCAEnums.PatientType.Procare,                                                                         
                                                                        (long)SCAEnums.PatientType.PaediatricProcare
                                                                    };

        /// <summary>
        /// The community self care
        /// </summary>
        private List<long> communitySelfCare = new List<long>() {                                                                        
                                                                        (long)SCAEnums.PatientType.SelfCare,
                                                                        (long)SCAEnums.PatientType.PaediatricSelfcare,
                                                                        (long)SCAEnums.PatientType.CommunitySelfcareNursing,
                                                                        (long)SCAEnums.PatientType.CommunitySelfcareResidential
                                                                };

		/// <summary>
		/// The care home non self care
		/// </summary>
		private List<long> careHomeNonSelfCare = new List<long>() {
                                                                        (long)SCAEnums.PatientType.Residential,                                                                         
                                                                        (long)SCAEnums.PatientType.Nursing,     
                                                                        (long)SCAEnums.PatientType.Hospital  
                                                                    };

		/// <summary>
		/// The care home self care
		/// </summary>
		private List<long> careHomeSelfCare = new List<long>() {                                                                        
                                                                        (long)SCAEnums.PatientType.ResidentialSelfcare,                                                                  
                                                                        (long)SCAEnums.PatientType.NursingSelfcare,
                                                                        (long)SCAEnums.PatientType.ContinuingCare 
                                                                };

		/// <summary>
		/// There will not be any first or subsequent automatic order for ResidentialSelfcare,NursingSelfcare, HospitalSelfcare and BulkSelfcare
		/// </summary>
		private List<long> allAutomaticOrdersRestrictedTypes = new List<long>() {                                                                        
                                                                        (long)SCAEnums.PatientType.ResidentialSelfcare,                                                                  
                                                                        (long)SCAEnums.PatientType.NursingSelfcare,   
                                                                        (long)SCAEnums.PatientType.ContinuingCare,                                                                      
                                                                        (long)SCAEnums.PatientType.ChildSelfCare,  
                                                                };

		/// <summary>
		/// There will be only subsequent automatic order for Residential, Nursing and Hospital. No first automatic order.
		/// </summary>
		private List<long> firstAutomaticOrderRestrictedTypes = new List<long>() {                                                                        
                                                                       (long)SCAEnums.PatientType.Residential,                                                                         
                                                                        (long)SCAEnums.PatientType.Nursing,     
                                                                        (long)SCAEnums.PatientType.Hospital,
                                                                };

		/// <summary>
		/// non Selfcare Patient
		/// </summary>
		long[] nonSelfcarePatient = new long[] {(long)SCAEnums.PatientType.Bulk,
                                                    (long)SCAEnums.PatientType.Hospital,
                                                    (long)SCAEnums.PatientType.Nursing,
                                                    (long)SCAEnums.PatientType.Procare, 
                                                    (long)SCAEnums.PatientType.Residential,
                                                    (long)SCAEnums.PatientType.PaediatricProcare
                                                };

        /// <summary>
        /// Selfcare Patient
        /// </summary>
        long[] selfcarePatient = new long[] {(long)SCAEnums.PatientType.SelfCare,
                                                    (long)SCAEnums.PatientType.ResidentialSelfcare,
                                                    (long)SCAEnums.PatientType.NursingSelfcare,
                                                    (long)SCAEnums.PatientType.ContinuingCare, 
                                                    (long)SCAEnums.PatientType.ChildSelfCare};
		/// <summary>
        /// One off standard orders for non-carehome patient and carehome patient
        /// </summary>
        long[] oneoffStandardOrders = new long[] {(long)SCAEnums.OrderCreationType.CommunityOneOffStandard,
                                                    (long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard};

        /// <summary>
        /// The online order for self care only
        /// </summary>
        private List<long> onlineOrderSelfCare = new List<long>() {                                                                        
                                                                        (long)SCAEnums.PatientType.SelfCare,
                                                                        (long)SCAEnums.PatientType.PaediatricSelfcare,
                                                                        (long)SCAEnums.PatientType.CommunitySelfcareNursing,
                                                                        (long)SCAEnums.PatientType.CommunitySelfcareResidential
                                                                };

        /// <summary>
        /// List of One Off Orders
        /// </summary>
        private List<long> oneOffOrders = new List<long>()
															{
																(long)SCAEnums.OrderCreationType.CarehomeOneOffFOC,
																(long)SCAEnums.OrderCreationType.CarehomeOneOffStandard,
																(long)SCAEnums.OrderCreationType.CarehomeOneOffRush,
																(long)SCAEnums.OrderCreationType.CarehomePatientOneOffFOC,
																(long)SCAEnums.OrderCreationType.CarehomePatientOneOffRush,
																(long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard,
																(long)SCAEnums.OrderCreationType.CommunityOneOffFOC,
																(long)SCAEnums.OrderCreationType.CommunityOneOffRush,
																(long)SCAEnums.OrderCreationType.CommunityOneOffStandard
															};

        /// <summary>
        /// List of One Off Rush/Emergency Orders
        /// </summary>
        List<long> oneOffOrdersRush = new List<long>(){ (long)SCAEnums.OrderCreationType.CarehomeOneOffRush,
                                                        (long)SCAEnums.OrderCreationType.CarehomeOneOffFOC,
														(long)SCAEnums.OrderCreationType.CommunityOneOffRush,
                                                        (long)SCAEnums.OrderCreationType.CommunityOneOffFOC,
														(long)SCAEnums.OrderCreationType.CarehomePatientOneOffRush,
														(long)SCAEnums.OrderCreationType.CarehomePatientOneOffFOC,																														
														};

        /// <summary>
        /// List of One Off Standard Orders
        /// </summary>
        List<long> oneOffOrdersStandard = new List<long>(){ (long)SCAEnums.OrderCreationType.CommunityOneOffStandard
															, (long)SCAEnums.OrderCreationType.CarehomeOneOffStandard
															, (long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard
														};

        /// <summary>
		/// The _user identifier
		/// </summary>
		private string globalUserId = string.Empty;

		/// <summary>
		/// Gets the lead time for automatic order.
		/// </summary>
		/// <value>
		/// The lead time for automatic order.
		/// </value>
		public int LeadTimeForAutomaticOrder
		{
			get
			{
				if (string.IsNullOrEmpty(leadTimeForAutomaticOrder))
				{
					return 0;
				}

				return Convert.ToInt32(leadTimeForAutomaticOrder, CultureInfo.InvariantCulture);
			}
		}

		#region RemoveProducts
		/// <summary>
		/// Remove the product from the already existing order
		/// </summary>
		/// <param name="productIdList">List of Product details</param>
		/// <returns>
		/// True if product[/s] are removed
		/// </returns>
		public bool RemoveProductFromOrder(Tuple<long, List<long>> productIdList)
		{
			var userId = string.Empty; ///TODO : to pass the userId from controller
			IOrderRepository orderRepository = null;
			try
			{
				if (productIdList == null)
				{
					return false;
				}
				var currentDate = DateTime.Now;
				var orderId = productIdList.Item1;
				var products = productIdList.Item2;
				orderRepository = new OrderRepository();
				IUnitOfWork unitOfWork = orderRepository.UnitOfWork;
				var orders = orderRepository.All.Where(q => q.OrderId == orderId);
				if (!orders.Any())
				{
					return false;
				}
				var order = orders.First();
				var productsToRemove = order.IDXOrderProducts.Where(p => products.Contains((long)p.ProductId)).ToList();
				foreach (var product in productsToRemove)
				{
					product.IsRemoved = true;

					/// product.ModifiedBy = userId;
					product.ModifiedDate = currentDate;
				}

				/// order.ModifiedBy = userId;
				order.ModifiedDate = currentDate;
				orderRepository.InsertOrUpdate(order);
				unitOfWork.Commit();
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			finally
			{
				if (orderRepository != null)
				{
					orderRepository.Dispose();
				}
			}

			return true;
		}
		#endregion

		#region SampleOrders
		/// <summary>
		/// Check Sample order in helix service
		/// </summary>
		/// <param name="sampleOrderPatientBusinessModel">Details of sample order</param>
		/// <returns>
		/// view model
		/// </returns>
		public SampleOrderPatientBusinessModel CheckSampleOrdersInHelix(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel)
		{
			var returnMessageSb = new StringBuilder();
			SampleOrderPatientBusinessModel objReturn = new SampleOrderPatientBusinessModel();
			objReturn = sampleOrderPatientBusinessModel;
			HelixSOPExtSoapClient objHelixService = null;

			try
			{
				if (sampleOrderPatientBusinessModel == null)
				{
					return null;
				}
				objHelixService = new HelixSOPExtSoapClient();
				List<SampleProductBusinessModel> orderItemsNotAvailable = new List<SampleProductBusinessModel>();

				/// Get the list of products available in Helix
				var helixProductDetailResponse = objHelixService.ProductExtGetAll(ConfigurationManager.AppSettings["consumerName"], ConfigurationManager.AppSettings["passCode"],
												   ConfigurationManager.AppSettings["helixClientName"], ConfigurationManager.AppSettings["helixUserName"],
												   ConfigurationManager.AppSettings["userPassword"]);

				if (helixProductDetailResponse.Success && helixProductDetailResponse.ReturnProductDetail != null)
				{
					var productList = helixProductDetailResponse.ReturnProductDetail.ProductList;
					var sampleProductList = sampleOrderPatientBusinessModel.SampleProductBusinessModel;
					if (productList != null || productList.Length > 0)
					{
						/// Check with all Products return from Helix,
						for (int index = 0; index < sampleProductList.Count; index++)
						{
							if (!productList.Where(p => p.ClientProductID == sampleProductList[index].HelixProductId && p.Quantity >= sampleProductList[index].MaxQuantity).Any())
							{
								orderItemsNotAvailable.Add(sampleProductList[index]);
							}
						}
					}
					else
					{
						orderItemsNotAvailable.AddRange(sampleProductList);
					}

					/// 1. Ask user if user wants to submit the order. Show details or products not available if any.                    
					if (orderItemsNotAvailable.Count > 0)
					{
						/// returnMessage += "Your order has been sent. Following items were unfortunately not available at the moment. <br/>";
						foreach (var basketItem in orderItemsNotAvailable)
						{
							returnMessageSb.Append(" - ").Append(basketItem.DescriptionUI).Append(" (").Append(basketItem.HelixProductId).Append(")<br/>");
						}
						objReturn.ReturnMessage = returnMessageSb.ToString();
					}
					/// Remove items from basket that weren't found in web service
					foreach (var item in orderItemsNotAvailable)
					{
						sampleOrderPatientBusinessModel.SampleProductBusinessModel.Remove(item);
					}
					objReturn.NotAvailableItems = orderItemsNotAvailable;
					objReturn.AvailableItems = sampleOrderPatientBusinessModel.SampleProductBusinessModel;
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (objHelixService != null)
				{

				}
			}

			return objReturn;
		}

		/// <summary>
		/// This function will send order to helix service after getting confirmation from user.
		/// </summary>
		/// <param name="sampleOrderPatientBusinessModel">Details of sample order</param>
		/// <returns>sampleOrderPatientBusinessModel object</returns>
		public SampleOrderPatientBusinessModel SendOrder(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel)
		{
			HelixSOPExtSoapClient objHelixService = null;
			try
			{
				if (sampleOrderPatientBusinessModel == null)
				{
					return null;
				}
				/// Customer setup correctly, now create order
				OrderDetail order = new OrderDetail();
				order.DespatchMethodID = int.Parse(ConfigurationManager.AppSettings["DespatchMethodID"], CultureInfo.InvariantCulture);
				order.OrderTakenBy = ConfigurationManager.AppSettings["helixUserName"];
				order.PaymentMethod = (MethodOfPayment)Enum.Parse(typeof(MethodOfPayment), ConfigurationManager.AppSettings["PaymentMethod"]);
				order.OutOfStockAction = (OutOfStockOption)Enum.Parse(typeof(OutOfStockOption), ConfigurationManager.AppSettings["OutOfStockAction"]);
				order.SubdivisionID = int.Parse(ConfigurationManager.AppSettings["SubDivisionID"], CultureInfo.InvariantCulture);
				order.OrderDate = DateTime.Now;
				order.CustomerID = ConfigurationManager.AppSettings["customerID"];
                order.Notes = sampleOrderPatientBusinessModel.NoteText;

				/// Delivery address
				AddressDetail deliveryAddress = new AddressDetail();
				deliveryAddress.Name = sampleOrderPatientBusinessModel.FirstName + " " + sampleOrderPatientBusinessModel.LastName;
				deliveryAddress.Street1 = sampleOrderPatientBusinessModel.Address1;
				deliveryAddress.Street2 = sampleOrderPatientBusinessModel.Address2;
				deliveryAddress.Street3 = sampleOrderPatientBusinessModel.SampleHouseName;
				deliveryAddress.Town = sampleOrderPatientBusinessModel.TownOrCity;
                deliveryAddress.County = !string.IsNullOrEmpty(sampleOrderPatientBusinessModel.CountyText) ? sampleOrderPatientBusinessModel.CountyText : string.Empty;
				deliveryAddress.Country = Country;
				deliveryAddress.Postcode = sampleOrderPatientBusinessModel.PostCode;

				order.DeliveryAddressDetail = deliveryAddress;

				/// Order items                
				List<OrderItemDetail> orderItems = new List<OrderItemDetail>();
				for (int i = 0; i < sampleOrderPatientBusinessModel.AvailableItems.Count; i++)
				{
					orderItems.Add(new OrderItemDetail()
					{
						ProductID = Convert.ToString(sampleOrderPatientBusinessModel.AvailableItems[i].HelixProductId, CultureInfo.InvariantCulture),
						QuantityOrdered = Convert.ToInt32(sampleOrderPatientBusinessModel.AvailableItems[i].MaxQuantity, CultureInfo.InvariantCulture)
					});
				}

				/// Add articles to order-object
				OrderItemDetail[] orderItemDetailArray = new OrderItemDetail[orderItems.Count];
				for (int index = 0; index < orderItems.Count; index++)
				{
					orderItemDetailArray[index] = orderItems[index];
				}

				order.OrderItemDetailsList = orderItemDetailArray;
				if (order.OrderItemDetailsList.Any())
				{
					//Send order to Helix service
					FunctionResponseOrder orderResponse = null;
					using (objHelixService = new HelixSOPExtSoapClient())
					{
						orderResponse = objHelixService.OrderProcessAllClientProdID(ConfigurationManager.AppSettings["consumerName"], ConfigurationManager.AppSettings["passCode"],
												   ConfigurationManager.AppSettings["helixClientName"], ConfigurationManager.AppSettings["helixUserName"],
												   ConfigurationManager.AppSettings["userPassword"], order, true);
					}
					if (orderResponse != null)
					{
						if (orderResponse.Success)
						{
							sampleOrderPatientBusinessModel.HelixOrderID = Convert.ToString(orderResponse.ReturnOrderDetail.OrderNumber, CultureInfo.InvariantCulture);
						}
						else
						{
							var responseInfoItems = orderResponse.ResponseInfoItems;
							if (responseInfoItems != null && responseInfoItems.Length > 0)
							{
								sampleOrderPatientBusinessModel.HelixResponseCode = Convert.ToString(responseInfoItems[0].ResponseCode, CultureInfo.InvariantCulture);
								sampleOrderPatientBusinessModel.HelixMessage = responseInfoItems[0].Message;
							}
							sampleOrderPatientBusinessModel.HelixOrderID = Invalid;
						}
					}
					else
					{
						sampleOrderPatientBusinessModel.HelixOrderID = Invalid;
					}
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			return sampleOrderPatientBusinessModel;
		}

		/// <summary>
		/// Save sample order
		/// </summary>
		/// <param name="sampleOrderPatientBusinessModel">The sample order patient business model.</param>
		/// <returns>
		/// true or false
		/// </returns>
		public bool SaveSampleOrders(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel)
		{
			IOrderRepository orderRepository = null;
			ISamplePatientRepository samplePatientRepository = null;

			try
			{
				if (sampleOrderPatientBusinessModel == null)
				{
					return false;
				}

				samplePatientRepository = new SamplePatientRepository();
				orderRepository = new OrderRepository();
				/// Saving data in the SC Indigo database
				IUnitOfWork unitOfWork = orderRepository.UnitOfWork;
				IUnitOfWork unitOfWorkSample = samplePatientRepository.UnitOfWork;
				var modifiedDate = DateTime.Now;
				Guid modifiedBy = Guid.Parse(sampleOrderPatientBusinessModel.UserId);
				var orderType = (long)SCAEnums.OrderType.SAMP;
				var orderStatus = (long)SCAEnums.OrderStatus.Generated; //Set status to generated instead activated as suggested by Roxy
                var orderCreationType = (long)SCAEnums.OrderCreationType.SampleOrder;
				long samplePatienId = 0;

				if (!Convert.ToBoolean(sampleOrderPatientBusinessModel.IsCalledFromEdit, CultureInfo.InvariantCulture))
				{
					var objSamplePatient = new EFModel.SamplePatient();

					var objAddressdetails = new EFModel.Address();
					objAddressdetails.AddressLine1 = sampleOrderPatientBusinessModel.Address1;
					objAddressdetails.AddressLine2 = sampleOrderPatientBusinessModel.Address2;
					objAddressdetails.City = sampleOrderPatientBusinessModel.TownOrCity;
					objAddressdetails.PostCode = sampleOrderPatientBusinessModel.PostCode;
					objAddressdetails.HouseName = sampleOrderPatientBusinessModel.SampleHouseName;
					objAddressdetails.HouseNumber = null;
					objAddressdetails.County = sampleOrderPatientBusinessModel.County;
					objAddressdetails.Country = Country;
					objAddressdetails.ModifiedBy = modifiedBy;
					objAddressdetails.ModifiedDate = modifiedDate;

					var objPersonalInformation = new EFModel.PersonalInformation();
					objPersonalInformation.FirstName = sampleOrderPatientBusinessModel.FirstName;
					objPersonalInformation.LastName = sampleOrderPatientBusinessModel.LastName;
					objPersonalInformation.ModifiedBy = modifiedBy;
					objPersonalInformation.ModifiedDate = modifiedDate;
					objPersonalInformation.CreatedById = modifiedBy;
					objPersonalInformation.CreatedDateTime = modifiedDate;
					objPersonalInformation.AddressId = objAddressdetails.AddressId;
					objPersonalInformation.Address = objAddressdetails;

					objSamplePatient.PersonalInformationId = objPersonalInformation.PersonalInformationId;
					objSamplePatient.PersonalInformation = objPersonalInformation;
					objSamplePatient.CreatedBy = modifiedBy;
					objSamplePatient.CreatedDate = modifiedDate;
					objSamplePatient.ModifiedBy = modifiedBy;
					objSamplePatient.ModifiedDate = modifiedDate;
					objSamplePatient.PatientType = (long)SCAEnums.PatientType.Prospect;

					samplePatientRepository.InsertOrUpdate(objSamplePatient);
					unitOfWorkSample.Commit();
					samplePatienId = objSamplePatient.SamplePatientId;
				}

				/// SapOrderDelieveryDate should be current date + 1 also should save date only instead datetime
                //var dtSapOrderDeliveryDate = CommonHelper.AddWorkingDays(modifiedDate, 1).ToString(DateFormat, CultureInfo.InvariantCulture);
                var dtSapOrderDeliveryDate = Convert.ToDateTime(sampleOrderPatientBusinessModel.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
				var strSapOrderDeliveryDate = DateTime.ParseExact(dtSapOrderDeliveryDate, DateFormat, formatProvider);

				var newOrder = new EFModel.Order()
				{
					IsRushFlag = false,
					OrderType = orderType,
					OrderStatus = orderStatus,
					ExcludeServiceCharge = false,
					ModifiedBy = modifiedBy,
					ModifiedDate = modifiedDate,
					OrderDate = modifiedDate,
					SAPOrderDeliveryDate = strSapOrderDeliveryDate,
                    HelixOrderNo = Convert.ToString(sampleOrderPatientBusinessModel.HelixOrderID, CultureInfo.InvariantCulture),
                    OrderCreationType = orderCreationType
				};

				if (samplePatienId == 0)
				{
					newOrder.PatientId = sampleOrderPatientBusinessModel.PatientID;
				}
				else
				{
					newOrder.SamplePatientId = samplePatienId;
				}

				var sampleProductType = sampleOrderPatientBusinessModel.SampleOrderProductType.ToLower();

				foreach (var orderProduct in sampleOrderPatientBusinessModel.SampleProductBusinessModel)
				{
					long? sampleProductId = sampleProductType == "tena" ? orderProduct.ProductID : null;
					long? customerProductId = sampleProductType != "tena" ? orderProduct.ProductID : null;
					
					IDXOrderProduct newOrderProduct = new EFModel.IDXOrderProduct()
					{
						ModifiedBy = modifiedBy,
						ModifiedDate = modifiedDate,
						Quantity = orderProduct.MaxQuantity.HasValue ? orderProduct.MaxQuantity.Value : 0,
						IsFOC = false,
						OrderId = newOrder.OrderId,
						IsRemoved = false,
						ProductDescription = orderProduct.DescriptionUI,
						ProductId = customerProductId,
                        SampleProductId = sampleProductId,
                        SAPActualDeliveryDate = strSapOrderDeliveryDate
					};
					newOrder.IDXOrderProducts.Add(newOrderProduct);
				}

				orderRepository.InsertOrUpdate(newOrder);
				unitOfWork.Commit();


                if (samplePatienId == 0)
                {
                    var orderEntities = orderRepository.GetAddedOrderEntities();
                    if (orderEntities.Any())
                    {
                        var addedOrder = orderEntities.FirstOrDefault();
                        if (addedOrder != null)
                        {
                            //Save order note text to Notes table
                            using (INotesRepository noteRepository = new NotesRepository())
                            {
                                IUnitOfWork unitOfWorkNote = noteRepository.UnitOfWork;
                                var orderNote = new Note()
                                {
                                    NoteType = (long)SCAEnums.NoteType.SampleOrderNote,
                                    NoteTypeId = (long)addedOrder.PatientId,
                                    NoteText = sampleOrderPatientBusinessModel.NoteText,
                                    CreatedBy = modifiedBy,
                                    CreatedDate = modifiedDate,
                                    ModifiedBy = modifiedBy,
                                    ModifiedDate = modifiedDate
                                };
                                noteRepository.InsertOrUpdate(orderNote);
                                unitOfWorkNote.Commit();
                            }
                        }
                    }
                }

                // TO MAINTAIN THE INTERACTION FOR SAMPLE ORDER ONLY FROM View/Edit Patient
                if (Convert.ToBoolean(sampleOrderPatientBusinessModel.IsCalledFromEdit, CultureInfo.InvariantCulture))
                {
                    List<EFModel.Order> SampleOrderInteraction = new List<EFModel.Order>();
                    SampleOrderInteraction.Add(newOrder);
                    this.InsertIntoInteraction(SampleOrderInteraction, sampleOrderPatientBusinessModel.CustomerID, false, Convert.ToString(modifiedBy, CultureInfo.InvariantCulture));
                }
                return true;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (orderRepository != null)
				{
					orderRepository.Dispose();
				}

				if (samplePatientRepository != null)
				{
					samplePatientRepository.Dispose();
				}
			}

			return false;
		}
		#endregion

		#region SaveOrder

		/// <summary>
		/// Get CareHome Orders
		/// Save Orders
		/// </summary>
		/// <param name="orders">List of PatientOrdersBusinessModel&gt;</param>
		/// <returns>
		/// Return boolean
		/// </returns>
		public bool SaveOrders(List<PatientOrdersBusinessModel> orders)
		{
			var result = new List<KeyValuePair<long, long>>();
			return SaveOrdersData(orders, DateTime.Now, result);
		}

		/// <summary>
		/// Saves the orders data.
		/// </summary>
		/// <param name="orders">The orders.</param>
		/// <param name="result">The result.</param>
		/// <returns></returns>
		private bool SaveOrdersData(List<PatientOrdersBusinessModel> orders, DateTime orderCreationDate, List<KeyValuePair<long, long>> result)
		{
			var modifiedBy = string.Empty;
			IOrderRepository orderRepository = null;
			IPatientRepository patientRepository = null;
			bool isOneOffOrder = false;
            string originalDerivedNDD = "";
            bool isNDDMismatch = false;
			try
			{
				long careHomeId = 0;
				if (orders == null)
				{
                    return false;
				}

				var firstRecord = orders.First(); /// Fetching the first record from the list for some common data.
				isOneOffOrder = firstRecord.IsOneOffOrder;
				careHomeId = Convert.ToInt64(firstRecord.CareHomeID, CultureInfo.InvariantCulture);
				modifiedBy = Convert.ToString(firstRecord.ModifiedBy, CultureInfo.InvariantCulture);
				var orderId = Convert.ToInt64(firstRecord.OrderId, CultureInfo.InvariantCulture);
				var orderType = Convert.ToInt64(firstRecord.OrderType, CultureInfo.InvariantCulture);
				var isRushFlag = orderType == (long)SCAEnums.OrderType.ZHDR ? true : false;
				var isProductLevel = firstRecord.IsProductLevel;
				if (isRushFlag && !orders.Where(q => q.IsFOC == null || q.IsFOC == false || q.ExcludeServiceCharge == null || q.ExcludeServiceCharge == false).Any())
				{
					orderType = (long)SCAEnums.OrderType.ZHDF;
                    var orderCreationType = firstRecord.OrderCreationType;
                    switch (orderCreationType)
                    {
                        case (long)SCAEnums.OrderCreationType.CommunityOneOffRush:
                            orderCreationType = (long)SCAEnums.OrderCreationType.CommunityOneOffFOC;
                            break;
                        case (long)SCAEnums.OrderCreationType.CarehomePatientOneOffRush:
                            orderCreationType = (long)SCAEnums.OrderCreationType.CarehomePatientOneOffFOC;
                            break;
                        case (long)SCAEnums.OrderCreationType.CarehomeOneOffRush:
                            orderCreationType = (long)SCAEnums.OrderCreationType.CarehomeOneOffFOC;
                            break;
                        default:
                            orderCreationType = (long)SCAEnums.OrderCreationType.Undefined;
                            break;
				}
                    orders.ForEach(o => o.OrderCreationType = orderCreationType);
                }

                if (!firstRecord.IsOrderCancel)
                {
                    originalDerivedNDD = firstRecord.DerivedNDD;
                    isNDDMismatch = firstRecord.DeliveryDate != firstRecord.DerivedNDD;
                    orders.ForEach(q => q.DerivedNDD = q.IsNDDManuallyChanged ? q.DeliveryDate : q.DerivedNDD);
                }

				orderRepository = new OrderRepository();
				IUnitOfWork unitOfWork = orderRepository.UnitOfWork;
				if (careHomeId != 0)
				{

					if (isProductLevel) ///it is a carehome product level order
					{
						var ordersList = orders.Select(q => q.OrderId).Distinct();
						foreach (var orderNo in ordersList)
						{
                            CommitOrders(orderType, isRushFlag, orders.Where(o => o.OrderId == orderNo).ToList(), orderRepository, null, orderCreationDate);
						}
					}
					else
					{ /// it is carehome patient order
                        var patients = orders.Where(q => (!q.IsPatientRemoved) || (q.IsPatientRemoved && q.OrderId != null)).Select(o => o.PatientID).Distinct().ToList();
						EFModel.OrderDisclaimer orderDisclaimer = null;
						foreach (var pat in patients)
						{
                            var iOrderProducts = orders.Where(o => o.PatientID == pat && (o.IsPatientRemoved ? o.IsProductOrderActivated : true)).Select(p => (long)p.ProductId).ToList();
							if (orderId == 0 && firstRecord.OrderType == (long)SCAEnums.OrderType.ZHDP)
							{
								UpdatePrescriptionProductsOfOrder(iOrderProducts, pat);
							}
							var patientOrders = orders.Where(o => o.PatientID == pat).Select(q => q.OrderId).Distinct().ToList();

							foreach (var patientOrderId in patientOrders)
							{
                                orderDisclaimer = CommitOrders(orderType, isRushFlag, orders.Where(o => o.PatientID == pat && o.OrderId == patientOrderId && (o.IsPatientRemoved ? o.IsProductOrderActivated : true)).ToList(), orderRepository, orderDisclaimer, orderCreationDate);
							}
						}
					}
				}
				else
				{
					/// it is commnunity order (non carehome patient)
					var ordersList = orders.Select(q => q.OrderId).Distinct();
					foreach (var orderNo in ordersList)
					{
						var orderProducts = orders.Where(q => q.OrderId == orderNo).Select(o => (long)o.ProductId).ToList();
                        if (orderId == 0 && firstRecord.OrderType == (long)SCAEnums.OrderType.ZHDP && firstRecord.OrderCreationType != (long)SCAEnums.OrderCreationType.CommunityOneOffStandard)
						{
							UpdatePrescriptionProductsOfOrder(orderProducts, firstRecord.PatientID);
						}

                        CommitOrders(orderType, isRushFlag, orders.Where(o => o.OrderId == orderNo).ToList(), orderRepository, null, orderCreationDate);
					}
				}

				unitOfWork.Commit();

				var orderEntities = orderRepository.GetAddedOrderEntities();
				var ordersCreated = orderEntities.Select(q => new KeyValuePair<long, long>((long)(q.PatientId == null ? q.CareHomeId : q.PatientId), q.OrderId)).ToList();
				result.AddRange(ordersCreated);

                var removedPatientList = orders.Where(q => q.IsPatientRemoved == true).Select(q => q.PatientID).Distinct().ToList();

                #region Patient Removed section for Order Activation at patient level
                if (!isProductLevel && removedPatientList.Any())
                {
                    patientRepository = new PatientRepository();
                    IUnitOfWork patientUnitOfWork = patientRepository.UnitOfWork;
                    EFModel.Patient objPatient = null;

                    var objPatients = patientRepository.GetPatient(removedPatientList);

                    foreach (var patientId in removedPatientList)
                    {
                        objPatient = objPatients.FirstOrDefault(q => q.PatientId == patientId);
                        var patientOrderBusinessModel = orders.FirstOrDefault(q => q.PatientID == patientId);

                        objPatient.ModifiedBy = patientOrderBusinessModel.ModifiedBy;
                        objPatient.ModifiedDate = DateTime.Now;
                        objPatient.PatientStatus = (long)SCAEnums.PatientStatus.Removed;
                        objPatient.ReasonCodeID = Convert.ToInt64(patientOrderBusinessModel.ReasonCode, CultureInfo.InvariantCulture);
                        objPatient.RemovedStoppedDateTime = objPatient.ModifiedDate;

                        patientRepository.InsertOrUpdate(objPatient);
                    }
                    patientUnitOfWork.Commit(CommonConstants.ALRmkPatientRemovedFrmCommunityOrder);                    
                }
                #endregion

				#region check for cancelled order for non selfcare patient
				if (!isProductLevel)
				{
					var cancelledOrders = orderEntities.Where(q => q.OrderStatus == (long)SCAEnums.OrderStatus.Cancelled);
					if (cancelledOrders.Any())
					{
						patientRepository = new PatientRepository();
						EFModel.Patient patient = null;
						foreach (var order in cancelledOrders.Where(q=> !oneOffOrders.Contains((long)q.OrderCreationType)))
						{
							patient = patientRepository.GetPatient(order.PatientId.Value).ToList().FirstOrDefault();
                            if (nonSelfcarePatient.Contains((long)patient.PatientType))
                            {
                                UpdateNddForCanceledOrder(order, patient);
                            }
						}
					}
				}
				#endregion

                #region NO NEED TO INSERT IN NOTES TABLE
                //if (!string.IsNullOrEmpty(firstRecord.OrderNote))
                //{
                //    INotesRepository notesRepository = new NotesRepository();
                            
                //    if (careHomeId != 0)//added for entry against carehome also
                //    {
                //        var orderObj = orderEntities.FirstOrDefault();
                //        var orderNoteCarehome = new EFModel.Note()
                //        {
                //            NoteTypeId = careHomeId,
                //            NoteText = firstRecord.OrderNote,
                //            NoteType = !isProductLevel ? (long)SCAEnums.NoteType.CarehomeInteractionNote : (long)SCAEnums.NoteType.CarehomeProductLevelInteractionNote,
                //            CreatedBy = orderObj.ModifiedBy,
                //            CreatedDate = orderObj.ModifiedDate,
                //            ModifiedBy = orderObj.ModifiedBy,
                //            ModifiedDate = orderObj.ModifiedDate
                //        };
                //        notesRepository.InsertOrUpdate(orderNoteCarehome);
                //    }
                //    else
                //    {
                //        foreach (var orderObj in orderEntities)
                //        {
                //            var orderNote = new EFModel.Note()
                //            {
                //                NoteTypeId = orderObj.OrderId,
                //                NoteText = firstRecord.OrderNote,
                //                NoteType = (long)SCAEnums.NoteType.CommunityInteractionNote,
                //                CreatedBy = orderObj.ModifiedBy,
                //                CreatedDate = orderObj.ModifiedDate,
                //                ModifiedBy = orderObj.ModifiedBy,
                //                ModifiedDate = orderObj.ModifiedDate

                //            };
                //            notesRepository.InsertOrUpdate(orderNote);
                //        }
                //    }
                //    IUnitOfWork notesUOW = notesRepository.UnitOfWork;
                //    notesUOW.Commit();
                //}
                #endregion


                if (firstRecord.IsCommunityOrder)
                {
                    var ADP = firstRecord.PatientADP != null ? firstRecord.PatientADP : "";
                    var orderobj = orderEntities.First();
                    
                    patientRepository = new PatientRepository();
                    EFModel.Patient patient = patientRepository.Find(firstRecord.PatientID);

                    patient.ADP1 = string.IsNullOrEmpty(patient.ADP1) ? "" : patient.ADP1;
                    if (patient.ADP1.Trim() != ADP.Trim())
                    {
                        patient.ADP1 = ADP.Trim();
                        patient.ADP2 = ADP.Length > ADP2Length ? ADP.Substring(0, ADP2Length).Trim() : ADP.Trim();
                        patient.ModifiedBy = orderobj.ModifiedBy;
                        patient.ModifiedDate = orderobj.ModifiedDate;
                        patient.IsSentToSAP = false;
                        patientRepository.InsertOrUpdate(patient);
                        IUnitOfWork patientUOW = patientRepository.UnitOfWork;
                        patientUOW.Commit();
                    }
                }
                this.InsertIntoInteraction(orderEntities, firstRecord.CustomerID, isOneOffOrder, modifiedBy, isNDDMismatch, firstRecord.IsNDDManuallyChanged, originalDerivedNDD, firstRecord.IsActivatedOrder, firstRecord.OrderNote);

				return true;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, modifiedBy);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, modifiedBy);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, modifiedBy);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, modifiedBy);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, modifiedBy);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, modifiedBy);
				throw;
			}
			finally
			{
				if (orderRepository != null)
				{
					orderRepository.Dispose();
				}

                if (patientRepository != null)
                    patientRepository.Dispose();
			}

			return false;
		}

		/// <summary>
		/// Gets the order by identifier.
		/// </summary>
		/// <param name="orderId">The order identifier.</param>
		/// <param name="orderRepository">The order repository.</param>
		/// <returns></returns>
		private static EFModel.Order GetOrderById(long orderId, IOrderRepository orderRepository)
		{
			var newOrder = new EFModel.Order();
			if (orderId != 0)
			{
				newOrder = orderRepository.Find(orderId);
				if (newOrder == null)
				{
					newOrder = new EFModel.Order();
				}
			}

			return newOrder;
		}

		/// <summary>
		/// Commits the orders.
		/// </summary>
		/// <param name="orderType">Type of the order.</param>
		/// <param name="isRushOrder">if set to <c>true</c> [is rush order].</param>
		/// <param name="orders">The orders.</param>
		/// <param name="orderRepository">The order repository.</param>
		/// <param name="orderDisclaimer">The order disclaimer.</param>
		/// <returns></returns>
		public OrderDisclaimer CommitOrders(long orderType, bool isRushOrder, List<PatientOrdersBusinessModel> orders, IOrderRepository orderRepository, OrderDisclaimer orderDisclaimer, DateTime orderCreationDate)
		{
			ICareHomeRepository careHomeRepository = null;           
            try
            {
                if (orders == null || orderRepository == null)
                {
                    return null;
                }

                careHomeRepository = new CareHomeRepository();
                var careHomeUnitofWork = careHomeRepository.UnitOfWork;

                var firstRecord = orders.First();
                var modifiedBy = firstRecord.ModifiedBy;
                var modifiedDate = orderCreationDate;
                var orderId = Convert.ToInt64(firstRecord.OrderId, CultureInfo.InvariantCulture);
                var carehomeId = Convert.ToInt64(firstRecord.CareHomeID, CultureInfo.InvariantCulture);

                var newOrder = GetOrderById(orderId, orderRepository);
                if (newOrder == null)
                {
                    return null;
                }

                if (firstRecord.IsDislcaimerRequired.Equals(true) && firstRecord.CareHomeID > 0)
                {
                    if (orderDisclaimer == null)
                    {
                        orderDisclaimer = newOrder.OrderDisclaimer != null ? newOrder.OrderDisclaimer : new EFModel.OrderDisclaimer();
                        orderDisclaimer.Name = firstRecord.PersonName;
                        orderDisclaimer.Position = firstRecord.PersonPosition;
                        orderDisclaimer.ModifiedBy = modifiedBy;
                        orderDisclaimer.ModifiedDate = modifiedDate;
                    }

                    newOrder.OrderDisclaimer = orderDisclaimer;
                }


                var orderDeliveryDate = DateTime.ParseExact(firstRecord.DeliveryDate, DateFormat, formatProvider);
                var orderDerivedNDD = DateTime.ParseExact(firstRecord.DerivedNDD, DateFormat, formatProvider);
                newOrder.OrderType = orderType;
                newOrder.IsRushFlag = isRushOrder;
                newOrder.OrderDate = newOrder.OrderId == 0 ? modifiedDate : newOrder.OrderDate;
                newOrder.ExcludeServiceCharge = firstRecord.ExcludeServiceCharge == null ? false : (bool)firstRecord.ExcludeServiceCharge;
                newOrder.SAPOrderDeliveryDate = orderDeliveryDate;
                newOrder.DerivedNDD = orderDerivedNDD;
                newOrder.ModifiedBy = modifiedBy;
                newOrder.ModifiedDate = modifiedDate;
                newOrder.PatientId = firstRecord.IsProductLevel == true ? (long?)null : firstRecord.PatientID;
                newOrder.CareHomeId = firstRecord.CareHomeID;
                if (!CommonHelper.IsNullOrEmpty(firstRecord.OrderCreationType))
                {
                    newOrder.OrderCreationType = firstRecord.OrderCreationType.Value;
                }
                newOrder.EmailId = firstRecord.EmailId;

                bool isProductExisting = true;
                foreach (var orderProduct in orders)
                {
                    isProductExisting = false;
                    if (!newOrder.OrderId.Equals(0))
                    {
                        var existingOrderProducts = newOrder.IDXOrderProducts.Where(p => p.ProductId == orderProduct.ProductId).ToList();
                        if (existingOrderProducts.Any())
                        {
                            isProductExisting = true;
                            var existingProduct = existingOrderProducts.First();
                            existingProduct.ModifiedBy = modifiedBy;
                            existingProduct.ModifiedDate = modifiedDate;
                            existingProduct.IsRemoved = orderProduct.Quantity == 0 ? true : false;
                            existingProduct.Quantity = orderProduct.Quantity;
                            existingProduct.IsFOC = orderProduct.IsFOC == null ? false : (bool)orderProduct.IsFOC;
                        }
                    }

                    if (!isProductExisting)
                    {
                        var newOrderProduct = new EFModel.IDXOrderProduct()
                        {
                            ModifiedBy = modifiedBy,
                            ModifiedDate = modifiedDate,
                            ProductId = orderProduct.ProductId,
                            Quantity = orderProduct.Quantity,
                            IsRemoved = orderProduct.Quantity == 0 ? true : false,
                            IsFOC = orderProduct.IsFOC == null ? false : (bool)orderProduct.IsFOC,
                        };
                        newOrder.IDXOrderProducts.Add(newOrderProduct);
                    }
                }

                if (firstRecord.IsProductLevel && carehomeId != null)
                {
                    var carehomeDetails = careHomeRepository.Find(carehomeId);
                    carehomeDetails.PurchaseOrderNo = firstRecord.PurchaseOrderNum;
                    careHomeUnitofWork.Commit();
                }

                var orderStatus = (long)SCAEnums.OrderStatus.Activated;
                if (!newOrder.IDXOrderProducts.Where(q => q.Quantity != 0).Any())
                {
                    orderStatus = (long)SCAEnums.OrderStatus.Cancelled;
                }
                newOrder.OrderStatus = orderStatus;

                orderRepository.InsertOrUpdate(newOrder);              
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
			finally
			{
				if (careHomeRepository != null)
				{
					careHomeRepository.Dispose();
				}
			}
			return orderDisclaimer;
		}

		/// <summary>
		/// Updates the nd dfor cancelled order.
		/// </summary>
		/// <param name="orderDetails">The order details.</param>
		/// <param name="patientDetails">The patient details.</param>
		public void UpdateNddForCanceledOrder(EFModel.Order orderDetails, EFModel.Patient patientDetails)
		{
			try
			{
				if (orderDetails == null || patientDetails == null)
				{
					return;
				}                

				/// get post code matirx
				var isCarehomeOrder = false;
				var CarehomeNDD = DateTime.Now;
				objCommonService = new CommonService();
              
				objCommonService.PostcodeMatrixList = objCommonService.GetPostcodeMatrixForPatient(Convert.ToString(patientDetails.CustomerId, CultureInfo.InvariantCulture),
																									 Convert.ToString(patientDetails.PatientId, CultureInfo.InvariantCulture),
																									 patientDetails.CareHomeId != null ? Convert.ToString(patientDetails.CareHomeId, CultureInfo.InvariantCulture) : "0",
																									 Convert.ToString(orderDetails.ModifiedBy, CultureInfo.InvariantCulture)
																									 );

				/// calculate & update NDD for products in order + return minimum NDD of product            
                var PatientNDD = objCommonService.UpdatePrescriptionProductNdd(Convert.ToInt64(orderDetails.OrderId, CultureInfo.CurrentCulture), orderDetails.SAPOrderDeliveryDate, Convert.ToInt64(patientDetails.CustomerId, CultureInfo.CurrentCulture), orderDetails.ModifiedBy);
				if (patientDetails.CareHomeId != null) //if carehome order then calculate carehome ndd
				{
					isCarehomeOrder = true;
					CarehomeNDD = objCommonService.GetNdd(
										Convert.ToDateTime(orderDetails.SAPOrderDeliveryDate, CultureInfo.InvariantCulture),
										 Convert.ToInt64(patientDetails.CareHome.DeliveryFrequency, CultureInfo.CurrentCulture),
										  Convert.ToInt64(patientDetails.CustomerId, CultureInfo.CurrentCulture),
										   orderDetails.ModifiedBy);
				}

                new DBHelper().OrderResponse(Convert.ToInt64(orderDetails.OrderId, CultureInfo.CurrentCulture), 
                    string.Empty, 
                    string.Empty, 
                    string.Empty, 
                    "C", 
                    string.Empty, 
                    string.Empty, 
                    PatientNDD, 
                    CarehomeNDD, 
                    isCarehomeOrder, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null, 
                    null,                    
                    null);
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}

		}

		/// <summary>
		/// Removes the care home patient orders.
		/// </summary>
		/// <param name="careHomeId">The care home identifier.</param>
		/// <param name="includedPatients">The included patients.</param>
		/// <param name="orderRepository">The order repository.</param>
		/// <param name="userId">The user identifier.</param>
		private static void RemoveCareHomePatientOrders(long careHomeId, List<long> includedPatients, IOrderRepository orderRepository, Guid userId)
		{
			var careHomeOrders = orderRepository.GetCarehomeOrders(careHomeId);
			if (careHomeOrders.Any())
			{
				var removeOrders = careHomeOrders.Where(q => q.OrderStatus == (long)SCAEnums.OrderStatus.Activated && q.PatientId != null && !includedPatients.Contains((long)q.PatientId));
				foreach (var item in removeOrders)
				{
					item.OrderStatus = (long)SCAEnums.OrderStatus.Cancelled;
					item.ModifiedBy = userId;
					item.ModifiedDate = DateTime.Now;
					orderRepository.InsertOrUpdate(item);
				}
			}
		}

		/// <summary>
		/// Updates the prescription products of order.
		/// </summary>
		/// <param name="orderProducts">The order products.</param>
		/// <param name="patientId">The patient identifier.</param>
		/// <returns></returns>
		public static bool UpdatePrescriptionProductsOfOrder(List<long> orderProducts, long patientId)
		{
			IIDXPrescriptionProductRepository prescriptionProductRepository = null;
			try
			{
				prescriptionProductRepository = new IDXPrescriptionProductRepository();
				var idxPrescriptionProducts = prescriptionProductRepository.GetProductsByPatientId(patientId);
				var validProducts = idxPrescriptionProducts.Where(q => orderProducts.Contains((long)q.ProductId));
				foreach (var idx in validProducts)
				{
					if (idx.FlagDQ1 == null || (bool)idx.FlagDQ1)
					{
						idx.FlagDQ1 = false;
						idx.FlagDQ2 = true;
						idx.FlagDQ3 = false;
						idx.FlagDQ4 = false;
					}
					else if ((bool)idx.FlagDQ2)
					{
						idx.FlagDQ1 = false;
						idx.FlagDQ2 = false;
						idx.FlagDQ3 = true;
						idx.FlagDQ4 = false;
					}
					else if ((bool)idx.FlagDQ3)
					{
						idx.FlagDQ1 = false;
						idx.FlagDQ2 = false;
						idx.FlagDQ3 = false;
						idx.FlagDQ4 = true;
					}
					else if ((bool)idx.FlagDQ4)
					{
						idx.FlagDQ1 = true;
						idx.FlagDQ2 = false;
						idx.FlagDQ3 = false;
						idx.FlagDQ4 = false;
					}

					prescriptionProductRepository.InsertOrUpdate(idx);
				}

				prescriptionProductRepository.UnitOfWork.Commit();
				return true;
			}
			finally
			{
				if (prescriptionProductRepository != null)
				{
					prescriptionProductRepository.Dispose();
				}
			}
		}

		#endregion

		#region CommunityOrders

		/// <summary>
		/// Gets the valid products for patient.
		/// </summary>
		/// <param name="patientPrescription">The patient prescription.</param>
		/// <returns></returns>
		private static List<PatientOrdersBusinessModel> GetValidProductsForPatient(Prescription patientPrescription)
		{
			var ordersFromPrescription = new List<PatientOrdersBusinessModel>();
			var patient = patientPrescription.Patient;
			if (patient != null && patient.NextDeliveryDate != null)
			{
				ordersFromPrescription = GetOrderModelFromPrescription(patientPrescription);
			}

			return ordersFromPrescription;
		}

		/// <summary>
		/// Gets the valid products for patient.
		/// </summary>
		/// <param name="patientId">The patient identifier.</param>
		/// <returns></returns>
		private List<PatientOrdersBusinessModel> GetValidProductsForPatient(long patientId)
		{
			IPrescriptionRepository prescriptionRepository = null;
			try
			{
				prescriptionRepository = new PrescriptionRepository();
				var ordersFromPrescription = new List<PatientOrdersBusinessModel>();

				var patientPrescriptionList = prescriptionRepository.GetPatientPrescription(patientId);
				if (patientPrescriptionList.Any())
				{
					var patientPrescription = patientPrescriptionList.Single();
					ordersFromPrescription = GetValidProductsForPatient(patientPrescription);
				}

				return ordersFromPrescription;
			}
			finally
			{
				if (prescriptionRepository != null)
				{
					prescriptionRepository.Dispose();
				}
			}
		}

		/// <summary>
		/// Get Community Orders
		/// </summary>
		/// <param name="patientId">The patient identifier.</param>
		/// <param name="userId">The user identifier.</param>
		/// <returns>
		/// List (PatientOrdersBusinessModel)
		/// </returns>

        public List<PatientOrdersBusinessModel> GetCommunityOrders(string patientId, string userId)
        {
            try
			{
				var ordersFromPrescription = GetValidProductsForPatient(Convert.ToInt64(patientId, CultureInfo.InvariantCulture));
				return ordersFromPrescription;
			}                           
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }           
            return null;
        }





		/// <summary>
		/// Converts the prescription to patient order bm.
		/// </summary>
		/// <param name="validPrescriptions">The valid prescriptions.</param>
		/// <param name="patient">The patient.</param>
		/// <param name="minNextDeliveryDate">The minimum next delivery date.</param>
		/// <returns></returns>
		private static List<PatientOrdersBusinessModel> ConvertPrescriptionToPatientOrderBM(List<IDXPrescriptionProduct> validPrescriptions, EFModel.Patient patient, DateTime minNextDeliveryDate)
		{
			var nextDeliveryDate = minNextDeliveryDate.ToString(DateFormat, CultureInfo.InvariantCulture);
			return validPrescriptions.Select(idx => new PatientOrdersBusinessModel
				{
					PatientID = patient.PatientId,
					PatientTypeId = Convert.ToInt64(patient.PatientType, CultureInfo.InvariantCulture),
					ProductId = idx.ProductId,
					ProductDisplayId = idx.Product.SAPProductID == null ? Convert.ToString(idx.ProductId, CultureInfo.InvariantCulture) : CommonHelper.TrimProductId(idx.Product.SAPProductID),
					ProductDescription = idx.Product.DescriptionUI,
					ProductName = idx.Product.DescriptionUI,
					Quantity = (bool)idx.FlagDQ1 ? (long)idx.Del1 : ((bool)idx.FlagDQ2 ? (long)idx.Del2 : ((bool)idx.FlagDQ3 ? (long)idx.Del3 : ((bool)idx.FlagDQ4 ? (long)idx.Del4 : 0))),
					QuantityToCompare = (bool)idx.FlagDQ1 ? (long)idx.Del1 : ((bool)idx.FlagDQ2 ? (long)idx.Del2 : ((bool)idx.FlagDQ3 ? (long)idx.Del3 : ((bool)idx.FlagDQ4 ? (long)idx.Del4 : 0))),
					Unit = idx.Product.SalesUnit.ToString(CultureInfo.InvariantCulture),
					DeliveryDate = string.Empty, ///Calculation of Delivery Date according to the business logic are handled at controller.
					NextDeliveryDate = nextDeliveryDate,
					IDXPrescriptionProductId = idx.IDXPrescriptionProductId,
					RoundID = patient.RoundId
				}).ToList();
		}

		/// <summary>
		/// Gets the order model from prescription.
		/// </summary>
		/// <param name="patientPrescription">The patient prescription.</param>
		/// <returns></returns>
		private static List<PatientOrdersBusinessModel> GetOrderModelFromPrescription(Prescription patientPrescription)
		{
			var ordersFromPrescription = new List<PatientOrdersBusinessModel>();
			var patient = patientPrescription.Patient;

			if (patientPrescription.IDXPrescriptionProducts != null && patientPrescription.IDXPrescriptionProducts.Any())
			{
				var currentdate = DateTime.Today;
				//   var nextDeliveryDates = patientPrescription.IDXPrescriptionProducts.Where(idx => currentdate >= idx.ValidFromDate && currentdate <= idx.ValidToDate && idx.NextDeliveryDate != null).ToList();
				//    var minNextDeliveryDate = Convert.ToDateTime(nextDeliveryDates.Min(q => q.NextDeliveryDate), CultureInfo.InvariantCulture);
				var minNextDeliveryDate = Convert.ToDateTime(patient.NextDeliveryDate, CultureInfo.InvariantCulture);
				var validPrescriptions = patientPrescription.IDXPrescriptionProducts.Where(idx => (idx.IsRemoved == false || idx.IsRemoved == null)
					&& (patient.NextDeliveryDate <= idx.ValidToDate)
					&& idx.NextDeliveryDate <= minNextDeliveryDate).ToList();
				ordersFromPrescription = ConvertPrescriptionToPatientOrderBM(validPrescriptions, patient, minNextDeliveryDate);
			}

			return ordersFromPrescription;
		}
		#endregion

		#region EditOrders

		/// <summary>
		/// Converts the activate orders to business model.
		/// </summary>
		/// <param name="carehomeOrder">The carehomeOrder order.</param>
		public static List<PatientOrdersBusinessModel> ConvertActivateOrdersToBusinessModel(EFModel.Order careHomeOrder)
		{
			var resultedActiveOrders = new List<PatientOrdersBusinessModel>();
            var activeOrderProducts = careHomeOrder.IDXOrderProducts.Where(p => p.IsRemoved != true);
			foreach (var item in activeOrderProducts)
			{
				var newOrderBM = new PatientOrdersBusinessModel();
				newOrderBM.OrderType = item.Order.OrderType;
				newOrderBM.OrderId = item.OrderId;
				newOrderBM.CareHomeID = careHomeOrder.CareHomeId;
				newOrderBM.ProductId = item.ProductId;
				newOrderBM.ProductDisplayId = item.Product.SAPProductID == null ? Convert.ToString(item.ProductId, CultureInfo.InvariantCulture) : CommonHelper.TrimProductId(item.Product.SAPProductID);
				newOrderBM.ProductDescription = item.Product.DescriptionUI;
				newOrderBM.ProductName = item.Product.DescriptionUI;
				newOrderBM.Quantity = item.Quantity;
				newOrderBM.QuantityToCompare = item.Quantity;
				newOrderBM.Unit = item.Product.SalesUnit.ToString(CultureInfo.InvariantCulture);
				newOrderBM.DeliveryDate = Convert.ToDateTime(careHomeOrder.SAPOrderDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
				newOrderBM.NextDeliveryDate = Convert.ToDateTime(careHomeOrder.SAPOrderDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
				newOrderBM.IsOrderActivated = item.IsRemoved == true ? false : true;
				newOrderBM.PurchaseOrderNum = careHomeOrder.CareHome.PurchaseOrderNo;
				newOrderBM.OrderStatus = careHomeOrder.OrderStatus.Value;
                newOrderBM.IsFOC = item.IsFOC;
                newOrderBM.ExcludeServiceCharge = item.Order.OrderType == (long)SCAEnums.OrderType.ZHDF ? true : false;
				resultedActiveOrders.Add(newOrderBM);
			}

			return resultedActiveOrders;
			return null;
		}

		/// <summary>
		/// Converts the activate orders to business model.
		/// </summary>
		/// <param name="patientOrder">The patient order.</param>
		/// <param name="minFrequency">The minimum frequency.</param>
		/// <returns></returns>
		private static List<PatientOrdersBusinessModel> ConvertActivateOrdersToBusinessModel(EFModel.Order patientOrder, long minFrequency)
		{
			var resultedActiveOrders = new List<PatientOrdersBusinessModel>();
			var activeOrderProducts = patientOrder.IDXOrderProducts.Where(p => p.IsRemoved != true);
			foreach (var item in activeOrderProducts)
			{
				var newOrderBM = new PatientOrdersBusinessModel();
				newOrderBM.OrderType = item.Order.OrderType;
				newOrderBM.OrderId = item.OrderId;
                if (patientOrder.Patient != null)
				{
				newOrderBM.CareHomeID = patientOrder.Patient.CareHomeId;
				newOrderBM.PatientID = (long)patientOrder.PatientId;
				newOrderBM.PatientFirstName = patientOrder.Patient.PersonalInformation.FirstName;
				newOrderBM.PatientLastName = patientOrder.Patient.PersonalInformation.LastName;
				newOrderBM.PatientName = patientOrder.Patient.PersonalInformation.LastName + " " + patientOrder.Patient.PersonalInformation.FirstName;

                newOrderBM.PatientType = ((SCAEnums.PatientType)patientOrder.Patient.PatientType).ToString();
                newOrderBM.PatientStatus = ((SCAEnums.PatientStatus)patientOrder.Patient.PatientStatus).ToString();
                newOrderBM.PatientTypeId = (long)patientOrder.Patient.PatientType;
                newOrderBM.LastDeliveryDate = Convert.ToDateTime(patientOrder.Patient.DeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
                }
                else if (patientOrder.CareHome != null)
                {
                    newOrderBM.CareHomeID = patientOrder.CareHome.CareHomeId;
                }
				newOrderBM.ProductId = item.ProductId;
				newOrderBM.ProductDisplayId = item.Product.SAPProductID == null ? Convert.ToString(item.ProductId, CultureInfo.InvariantCulture) : CommonHelper.TrimProductId(item.Product.SAPProductID);
				newOrderBM.ProductDescription = item.Product.DescriptionUI;
				newOrderBM.ProductName = item.Product.DescriptionUI;
				newOrderBM.Quantity = item.Quantity;
				newOrderBM.QuantityToCompare = item.Quantity;
				newOrderBM.Unit = item.Product.SalesUnit.ToString(CultureInfo.InvariantCulture);
				newOrderBM.DeliveryDate = Convert.ToDateTime(patientOrder.SAPOrderDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
				newOrderBM.NextDeliveryDate = Convert.ToDateTime(patientOrder.SAPOrderDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
                newOrderBM.ExcludeServiceCharge = patientOrder.OrderType == (long)SCAEnums.OrderType.ZHDF ? true : false;
				newOrderBM.DeliveryFrequency = minFrequency;
				newOrderBM.IsOrderActivated = true;
				newOrderBM.OrderStatus = patientOrder.OrderStatus.Value;
                newOrderBM.IsFOC = item.IsFOC;
				resultedActiveOrders.Add(newOrderBM);

			}

			return resultedActiveOrders;
		}

		/// <summary>
		/// Gets the active orders.
		/// </summary>
		/// <param name="patientId">The patient identifier.</param>
		/// <param name="careHomeId">The care home identifier.</param>
		/// <returns></returns>
        private List<EFModel.Order> GetActiveOrders(long patientId, long careHomeId, bool isOneOff, bool isRush)
		{
			IOrderRepository orderRepository = null;
			try
			{
				orderRepository = new OrderRepository();
				var orders = new List<EFModel.Order>();
                if (careHomeId != 0)
                {
                    orders = orderRepository.GetCarehomeOrders(careHomeId).ToList();
                }
                else
                {
                    orders = orderRepository.GetPatientOrders(patientId).ToList();
                }

                if (isOneOff)
                {
                    orders = patientId != 0 ? orders.Where(q => q.PatientId.HasValue && q.PatientId.Value == patientId).ToList() : orders.Where(q => !q.PatientId.HasValue).ToList();
                    orders = isRush ? orders.Where(q => q.OrderCreationType != null && oneOffOrdersRush.Contains(q.OrderCreationType.Value)).ToList() : orders.Where(q => q.OrderCreationType != null && oneOffOrdersStandard.Contains(q.OrderCreationType.Value)).ToList();
                }
                else
                {
                    orders = orders.Where(q => q.OrderCreationType != null
                                                && !oneOffOrdersStandard.Contains(q.OrderCreationType.Value)
                                                && !oneOffOrdersRush.Contains(q.OrderCreationType.Value)
                                        ).ToList();
                }

				return orders;
			}
			finally
			{
				if (orderRepository != null)
				{
					orderRepository.Dispose();
				}
			}
		}

		/// <summary>
		/// Get Activated orders of Patient/Customer
		/// </summary>
		/// <param name="patientId">Patient Id</param>
		/// <param name="customerId">Customer Id</param>
		/// <param name="userId">User Id</param>
		/// <param name="showRushFlag"></param>
		/// <returns>
		/// List GetActivatedOrders
		/// </returns>
        public List<PatientOrdersBusinessModel> GetActivatedOrders(string patientId, string carehomeId, string userId, string showRushFlag, string isProductOrder, string isOneOff)
		{
            try
            {
                long icareHomeID = 0;
                bool isProductOrderFlag = Convert.ToBoolean(isProductOrder, CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(carehomeId) && !carehomeId.Equals("0", StringComparison.OrdinalIgnoreCase))
                {
                    icareHomeID = Convert.ToInt64(carehomeId, CultureInfo.InvariantCulture);
                }

                //Empty string validation is not required, since isOneOff will never be empty.
                var isOneOffOrder = !string.IsNullOrEmpty(isOneOff) ? isOneOff.ToUpper(CultureInfo.InvariantCulture) == "TRUE" ? true : false : false;
                var isOneOffOrderRush = (isOneOffOrder && !string.IsNullOrEmpty(showRushFlag) ? showRushFlag.ToUpper(CultureInfo.InvariantCulture) == "TRUE" ? true : false : false);
                var orders = GetActiveOrders(Convert.ToInt64(patientId, CultureInfo.InvariantCulture), icareHomeID, isOneOffOrder, isOneOffOrderRush);

                if (orders == null || !orders.Any())
                {
                    return null;
                }
                var activeOrders = orders.Where(o => (o.OrderStatus == (long)SCAEnums.OrderStatus.Activated || o.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) && o.OrderType != (long)SCAEnums.OrderType.SAMP).ToList();
                if (activeOrders.Any())
                {
                    var resultedActiveOrders = new List<PatientOrdersBusinessModel>();

                    if (isProductOrderFlag)
                    {
                        if (isOneOffOrder)
                        {
                            activeOrders = isOneOffOrderRush ? activeOrders = activeOrders.Where(o => o.OrderType == (long)SCAEnums.OrderType.ZHDR || o.OrderType == (long)SCAEnums.OrderType.ZHDF).ToList() : activeOrders.Where(o => o.OrderType == (long)SCAEnums.OrderType.ZHDH).ToList();
                            foreach (var carehomeOrder in activeOrders)
                            {
                                var convertedOrders = ConvertActivateOrdersToBusinessModel(carehomeOrder);
                                resultedActiveOrders.AddRange(convertedOrders);
                            }
                        }
                        else
                        {
                            activeOrders = activeOrders.Where(o => o.OrderType == (long)SCAEnums.OrderType.ZHDH).ToList(); //existin carehome product order
                            foreach (var carehomeOrder in activeOrders)
                            {
                                var convertedOrders = ConvertActivateOrdersToBusinessModel(carehomeOrder);
                                var carehomePatientOrders = GetCareHomeOrders(Convert.ToString(icareHomeID, CultureInfo.InvariantCulture), isProductOrder, userId);
                                carehomePatientOrders = carehomePatientOrders.GroupBy(x => new { x.ProductId, x.ProductDescription, x.ProductDisplayId })
                                     .Select(group => new PatientOrdersBusinessModel()
                                     {
                                         ProductId = group.Key.ProductId,
                                         Quantity = group.Sum(x => x.Quantity),
                                         ProductDescription = group.Key.ProductDescription,

                                         ProductDisplayId = group.Key.ProductDisplayId,
                                     }).ToList<PatientOrdersBusinessModel>();

                                if (convertedOrders.Any())
                                {
                                    var firstRecord = convertedOrders.FirstOrDefault();
                                    var nextDeliveryDate = firstRecord.NextDeliveryDate;
                                    var orderId = firstRecord.OrderId;
                                    var orderStatus = firstRecord.OrderStatus;
                                    foreach (var item in carehomePatientOrders)
                                    {
                                        var orderItem = convertedOrders.Where(c => c.ProductId == item.ProductId).FirstOrDefault();
                                        if (orderItem != null && orderItem.Quantity == 0)
                                        {
                                            item.DeliveryDate = item.NextDeliveryDate = nextDeliveryDate;
                                            item.IsOrderActivated = false;
                                            orderItem.Quantity = item.Quantity;
                                        }
                                        else if (orderItem == null)
                                        {
                                            item.OrderStatus = orderStatus;
                                            item.OrderId = orderId;
                                            item.DeliveryDate = item.NextDeliveryDate = nextDeliveryDate;
                                            item.IsOrderActivated = false;
                                            resultedActiveOrders.Add(item);
                                        }
                                    }
                                    resultedActiveOrders.AddRange(convertedOrders);
                                }
                            }
                        }
                    }
                    else
                    {
                        //Existing residential or residential one off standard or community one off standard order
                        activeOrders = activeOrders.Where(o => o.OrderType != (long)SCAEnums.OrderType.ZHDH).ToList();  
                        if (activeOrders.Any())
                        {
                            long minFrequency = 0;
                            if (icareHomeID != 0)
                            {
                                minFrequency = activeOrders.Min(q => Convert.ToInt64(q.CareHome.DeliveryFrequency, CultureInfo.InvariantCulture));
                            }
                            else
                            {
                                minFrequency = activeOrders.Min(q => Convert.ToInt64(q.Patient.DeliveryFrequency, CultureInfo.InvariantCulture));
                            }
                            foreach (var patientOrder in activeOrders)
                            {
                                var convertedOrders = ConvertActivateOrdersToBusinessModel(patientOrder, minFrequency);
                                if (convertedOrders.Any())
                                {
                                    resultedActiveOrders.AddRange(convertedOrders);
                                }
                            }
                        }
                    }
                    return resultedActiveOrders;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
			return null;
		}
		#endregion

		#region CareHomeOrders

		/// <summary>
		/// Gets the care home patient orders.
		/// </summary>
		/// <param name="careHomePatientIds">The care home patient ids.</param>
		/// <param name="prescriptionRepository">The prescription repository.</param>
		/// <returns></returns>
		private List<PatientOrdersBusinessModel> GetCareHomePatientOrders(List<long> careHomePatientIds, IPrescriptionRepository prescriptionRepository, bool isProductOrder)
		{
			var ordersFromPrescription = new List<PatientOrdersBusinessModel>();
			var patientPrescriptionList = prescriptionRepository.GetPatientPrescription(careHomePatientIds);
			if (patientPrescriptionList.Any())
			{
				foreach (var patientPrescription in patientPrescriptionList)
				{
					if (patientPrescription.Patient != null && patientPrescription.Patient.NextDeliveryDate != null)
					{
						var orders = GetOrdersModelForCareHome(patientPrescription, false, isProductOrder);
						if (orders != null && orders.Any())
						{
							orders.ForEach(q => { q.CareHomeID = patientPrescription.Patient.CareHomeId; });
							ordersFromPrescription.AddRange(orders);
						}
					}
				}
			}

			return ordersFromPrescription;
		}

		/// <summary>
		/// GetCareHomeOrders
		/// </summary>
		/// <param name="careHomeId">CareHome ID</param>
		/// <param name="userId">User Id</param>
		/// <returns>
		/// Return list of PatientOrdersBusinessModel&gt;
		/// </returns>
		public List<PatientOrdersBusinessModel> GetCareHomeOrders(string careHomeId, string isProductOrder, string userId)
		{
            IPatientRepository patientRepository = new PatientRepository();
			IPrescriptionRepository prescriptionRepository = null;
			try
			{
				prescriptionRepository = new PrescriptionRepository();
				var iCareHomeId = Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture);
				var patients = patientRepository.GetCareHomePatients(iCareHomeId).ToList();

				if (!patients.Any())
				{
					return null;
				}

				var careHomePatientIds = new List<long>();
				careHomePatientIds = patients.Where(p => p.NextDeliveryDate != null && p.PatientStatus == (long)SCAEnums.PatientStatus.Active).Select(p => p.PatientId).ToList();

				return GetCareHomePatientOrders(careHomePatientIds, prescriptionRepository, Convert.ToBoolean(isProductOrder, CultureInfo.InvariantCulture));
			}               
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			finally
			{
				if (patientRepository != null)
				{
					patientRepository.Dispose();
				}

				if (prescriptionRepository != null)
				{
					prescriptionRepository.Dispose();
				}
			}
			return null;
		}       

		/// <summary>
		/// Gets the orders model for care home.
		/// </summary>
		/// <param name="patientPrescription">The patient prescription.</param>
		/// <param name="hasActiveOrder">if set to <c>true</c> [has active order].</param>
		/// <returns></returns>
		private static List<PatientOrdersBusinessModel> GetOrdersModelForCareHome(Prescription patientPrescription, bool hasActiveOrder, bool isProductOrder)
		{
			var patient = patientPrescription.Patient;
			if (patientPrescription.IDXPrescriptionProducts != null && patientPrescription.IDXPrescriptionProducts.Any())
			{
				var nextDeliveryDate = Convert.ToDateTime(patient.CareHome.NextDeliveryDate, CultureInfo.InvariantCulture);
				var orders = patientPrescription.IDXPrescriptionProducts.Where(idx => (idx.IsRemoved == false || idx.IsRemoved == null)
									&& (patient.NextDeliveryDate <= idx.ValidToDate)
									&& idx.NextDeliveryDate <= nextDeliveryDate);
				if (orders.Any())
				{
					var patientOrderItems = ConvertToPatientOrderBusinessModel(orders, patient, hasActiveOrder);
					///Please do not remove this code, would be required in Phase3
					if (patient.Orders.Any() && !isProductOrder)
					{
						var activeCarehomePatientOrders = patient.Orders.Where(o => o.CareHomeId == patient.CareHomeId
																			&& o.OrderStatus == (long)SCAEnums.OrderStatus.Activated
																			&& o.OrderType == (long)SCAEnums.OrderType.ZHDP
																			&& o.IsRushFlag == false
                                                                            && o.OrderCreationType != (long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard);
						if (activeCarehomePatientOrders.Any())
						{
							var activeOrder = activeCarehomePatientOrders.First();///At any point, there can be only one ZHDP active order
							long patientOrderId = activeOrder.OrderId;
							foreach (var orderItem in activeOrder.IDXOrderProducts.Where(q => q.IsRemoved != true))
							{
								var matchingProductItems = patientOrderItems.Where(q => q.ProductId == orderItem.ProductId);
								if (matchingProductItems.Any())
								{
									var matchingProduct = matchingProductItems.First();
									matchingProduct.Quantity = orderItem.Quantity;
									matchingProduct.IsOrderActivated = true;
									matchingProduct.NextDeliveryDate = Convert.ToDateTime(orderItem.Order.SAPOrderDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
									matchingProduct.DerivedNDD = Convert.ToDateTime(orderItem.Order.DerivedNDD, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
									matchingProduct.ProductDisplayId = string.IsNullOrEmpty(orderItem.Product.SAPProductID) ? string.Empty : CommonHelper.TrimProductId(orderItem.Product.SAPProductID);
								}
							}
							patientOrderItems.ForEach(q => q.OrderId = patientOrderId);
						}
					}
					return patientOrderItems;
				}
			}

			return null;
		}

		/// <summary>
		/// Converts to patient order business model.
		/// </summary>
		/// <param name="prescriptionProduct">The prescription product.</param>
		/// <param name="patient">The patient.</param>
		/// <param name="hasActiveOrder">if set to <c>true</c> [has active order].</param>
		/// <returns></returns>
		private static List<PatientOrdersBusinessModel> ConvertToPatientOrderBusinessModel(IEnumerable<IDXPrescriptionProduct> prescriptionProduct, EFModel.Patient patient, bool hasActiveOrder)
		{
			if (prescriptionProduct != null && prescriptionProduct.Any() && patient != null)
			{
				var deliveryDate = Convert.ToDateTime(patient.DeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
				var deliveryFrequency = Convert.ToInt64(patient.DeliveryFrequency, CultureInfo.InvariantCulture);
                return prescriptionProduct.Select(idx => new PatientOrdersBusinessModel
                {
                    PatientID = patient.PatientId,
                    PatientFirstName = patient.PersonalInformation.FirstName,
                    PatientLastName = patient.PersonalInformation.LastName,
                    PatientName = patient.PersonalInformation.LastName + " " + patient.PersonalInformation.FirstName,
                    PatientType = ((SCAEnums.PatientType)patient.PatientType).ToString(),
                    PatientTypeId = Convert.ToInt64(patient.PatientType, CultureInfo.InvariantCulture),
                    ProductId = idx.ProductId,
                    ProductDisplayId = idx.Product.SAPProductID == null ? Convert.ToString(idx.ProductId, CultureInfo.InvariantCulture) : CommonHelper.TrimProductId(idx.Product.SAPProductID),
                    ProductDescription = idx.Product.DescriptionUI,
                    ProductName = idx.Product.DescriptionUI,
                    Quantity = (bool)idx.FlagDQ1 ? (long)idx.Del1 : ((bool)idx.FlagDQ2 ? (long)idx.Del2 : ((bool)idx.FlagDQ3 ? (long)idx.Del3 : ((bool)idx.FlagDQ4 ? (long)idx.Del4 : 0))),
                    QuantityToCompare = (bool)idx.FlagDQ1 ? (long)idx.Del1 : ((bool)idx.FlagDQ2 ? (long)idx.Del2 : ((bool)idx.FlagDQ3 ? (long)idx.Del3 : ((bool)idx.FlagDQ4 ? (long)idx.Del4 : 0))),
                    Unit = idx.Product.SalesUnit.ToString(CultureInfo.InvariantCulture),
                    DeliveryDate = deliveryDate,
                    LastDeliveryDate = deliveryDate,
                    NextDeliveryDate = Convert.ToDateTime(patient.CareHome.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                    DeliveryFrequency = deliveryFrequency,
                    IDXPrescriptionProductId = idx.IDXPrescriptionProductId,
                    RoundID = patient.RoundId,
                    IsOrderActivated = hasActiveOrder,
                    PatientStatus = ((SCAEnums.PatientStatus)patient.PatientStatus).ToString(),
                    PurchaseOrderNum = patient.CareHome.PurchaseOrderNo,
                    OrderStatus = patient.Orders.Any() ? Convert.ToInt64(patient.Orders.FirstOrDefault().OrderStatus) : 0,
                    SAPPatientNo = patient.SAPPatientNumber != null ? patient.SAPPatientNumber.TrimStart('0') : string.Empty
                }).ToList();
			}

			return null;
		}
		#endregion

		#region AutomaticOrders
		/// <summary>
		/// Creates the automatic order log file.
		/// </summary>
		/// <param name="automaticOrderStatus">The automatic order status.</param>
		/// <param name="orderType">Type of the order.</param>
		private static void CreateAutomaticOrderLogFile(List<AutomaticOrderStatusBusinessModel> automaticOrderStatus, string orderType)
		{
			///TODO: Remove file name hardcoding - Mamatha
			string fileName = "AutomaticOrder_" + orderType + "_" + DateTime.Now.ToString(CommonConstants.DateTimeFormat1, CultureInfo.InvariantCulture) + ".csv";

			var logDirectoryPath = @"\\" + Path.Combine(logServerIPAddress, logServerPath);
			var fileDirectory = Path.Combine(logDirectoryPath, logDirectory);
			var filePath = fileDirectory + "\\" + fileName;
			if (!Directory.Exists(fileDirectory))
			{
				Directory.CreateDirectory(fileDirectory);
			}

			if (!File.Exists(filePath))
			{
				File.Create(filePath).Close();
			}

			var data = new List<string[]>();
			///TODO :- remove header name hardcoding - Mamatha
			var hearders = new string[] { "SAP Customer ID", "SAP CareHome ID", "SAP Patient ID", "Front-End OrderNo", "Delivery Date", "Status", "Error Message" };
			data.Add(hearders);
			string delimiter = ",";
			foreach (var item in automaticOrderStatus)
			{
				var rows = new string[] { 
                                           item.SAPCustomerId
                                            , item.SAPCareHomeId
                                            , item.SAPPatientId
                                            , item.OrderId.ToString(CultureInfo.InvariantCulture)
                                            , item.DeliveryDate, item.Status
                                            , String.IsNullOrEmpty(item.Message) ? item.Message : CommonHelper.GetDescriptionFromEnumValue(((SCAEnums.AutomaticOrderStatusMessage)Convert.ToInt64(item.Message, CultureInfo.InvariantCulture))) 
                                        };
				data.Add(rows);
			}

			StringBuilder sb = new StringBuilder();
			foreach (var item in data)
			{
				sb.AppendLine(string.Join(delimiter, item));
			}
			File.AppendAllText(filePath, sb.ToString());
		}

		/// <summary>
		/// Get Carehomes due for next order
		/// </summary>
		/// <returns>
		/// No of Carehomes Due For NextOrder
		/// </returns>
		public string GenerateAutomaticCareHomeOrders()
		{
			ICustomerRepository customerRepository = null;
			var automaticOrderStatus = new List<AutomaticOrderStatusBusinessModel>();
            try
            {
                customerRepository = new CustomerRepository();
                var customers = customerRepository.GetCustomersForOrderCreation();
                if (!customers.Any())
                {
                    automaticOrderStatus.Add(GenerateAutomaticOrderStatus(0, "", SCAEnums.AutomaticOrderStatusMessage.NoValidCustomers));
                    return "0"; ///not valid patients for order creation
                }

                var totalOrders = 0;
                var leadTime = 0;
                var nextDeliveryDate = new DateTime();

                foreach (var customer in customers)
                {
                    leadTime = customer.CustomerParameters.Any() ? customer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;

                    Int64 countryId = customer.CountryId.HasValue ? customer.CountryId.Value : 1;

                    nextDeliveryDate = AddWorkingDays(DateTime.Today, leadTime, countryId, default(Guid).ToString());

                    //nextDeliveryDate = CommonHelper.GetNextWorkingDay(null, DateTime.Today, leadTime);

                    totalOrders += GenerateAutomaticCareHomeOrdersForCustomer(customer.CustomerId, customer.SAPCustomerNumber, nextDeliveryDate, automaticOrderStatus);
                }
                return totalOrders.ToString(CultureInfo.InvariantCulture);
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
			finally
			{
				CreateAutomaticOrderLogFile(automaticOrderStatus, SCAEnums.OrderFor.CareHomeOrder.ToString(CultureInfo.InvariantCulture));

				if (customerRepository != null)
				{
					customerRepository.Dispose();
				}
			}
			return string.Empty;
		}

		/// <summary>
		/// Gets the care home patients.
		/// </summary>
		/// <param name="careHome">The care home.</param>
		/// <param name="nextDeliveryDate">The next delivery date.</param>
		/// <returns></returns>
		private List<EFModel.Patient> GetCareHomePatients(EFModel.CareHome careHome, DateTime nextDeliveryDate)
		{
			var validPatients = new List<EFModel.Patient>();
			var careHomePatients = careHome.Patients.Where(p => p.PatientStatus == (long)SCAEnums.PatientStatus.Active
								   && p.NextDeliveryDate != null && p.NextDeliveryDate <= nextDeliveryDate);


			///Only subsequent order will be created for carehome nonselfcare patients
			//var careHomeNonSelfCarePatients = careHomePatients.Where(p => careHomeNonSelfCare.Contains((long)p.PatientType) && p.DeliveryDate != null).ToList();

            //Create first as well as subsequent order for carehome nonselfcare patients
            var careHomeNonSelfCarePatients = careHomePatients.Where(p => careHomeNonSelfCare.Contains((long)p.PatientType)).ToList();

			if (careHomeNonSelfCarePatients.Any())
			{
				validPatients.AddRange(careHomeNonSelfCarePatients);
			}


			return validPatients;
		}

		/// <summary>
		/// Generates the automatic care home orders for customer.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="SAPCustomerID">The sap customer identifier.</param>
		/// <param name="nextDeliveryDate">The next delivery date.</param>
		/// <param name="automaticOrderStatus">The automatic order status.</param>
		/// <returns></returns>
		private int GenerateAutomaticCareHomeOrdersForCustomer(long customerId, string SAPCustomerID, DateTime nextDeliveryDate, List<AutomaticOrderStatusBusinessModel> automaticOrderStatus)
		{
			ICareHomeRepository careHomeRepository = null;
			try
			{
				careHomeRepository = new CareHomeRepository();
				var careHomes = careHomeRepository.GetCareHomesByNDDAndCustomer(customerId, nextDeliveryDate);
				if (!careHomes.Any())
				{
					automaticOrderStatus.Add(GenerateAutomaticOrderStatus(customerId, SAPCustomerID, SCAEnums.AutomaticOrderStatusMessage.NoValidCareHomeUnderCustomer));
					return default(int);
				}
				var successCount = 0;
				var failedCount = 0;
				var validPatients = new List<EFModel.Patient>();
				foreach (var careHome in careHomes)
				{
					var careHomePatients = GetCareHomePatients(careHome, nextDeliveryDate);
					if (!careHomePatients.Any())
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(customerId, SAPCustomerID, careHome.CareHomeId, careHome.SAPCareHomeNumber, SCAEnums.AutomaticOrderStatusMessage.NoValidPatientUnderCareHome));
						continue;
					}
					validPatients.AddRange(careHomePatients);
				}

				foreach (var patient in validPatients)
				{
					///  var careHomeId = patient.CareHomeId;
					if (patient.Orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) 
                                            && (bool)q.IsRushFlag == false && (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP
                                            && !oneOffOrders.Contains((long)q.OrderCreationType)).Any())
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.PatientHasActiveOrder));
						continue;
					}

					EFModel.Prescription patientPrescription = null;
					if (patient.Prescriptions.Any())
					{
						patientPrescription = patient.Prescriptions.First();
					}
					else
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.NoValidPrescription));
						continue;
					}

					var orderProducts = GetValidProductsForPatient(patientPrescription);
					if (!orderProducts.Any())
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.NoValidPrescription));
						continue; /// No products due for delivery for the patient.
					}

                    var newNDD = ValidatePostCodeMatrix(nextDeliveryDate, patient);
                    var derivedNDD = newNDD;
                    var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(newNDD, Convert.ToInt64(patient.CustomerId, CultureInfo.InvariantCulture), default(Guid));
                    ///an arranged date is found in Holiday Process table then use that as a delivery date
                    if (isArrangedDateFound)
                    {
                        newNDD = arrangedNDD;
                    }


                    StringBuilder orderNoteSb = new StringBuilder("Automatic Order Created for Patient - ").Append(patient.PatientId.ToString(CultureInfo.InvariantCulture));
                    orderNoteSb.Append(" for Delivery Date - ").Append(newNDD.ToString(DateFormat, CultureInfo.InvariantCulture));
                    orderProducts.ForEach(q =>
                    {
                        q.DerivedNDD = derivedNDD.ToString(DateFormat, CultureInfo.InvariantCulture);
                        q.CareHomeID = patient.CareHomeId;
                        q.DeliveryDate = newNDD.ToString(DateFormat, CultureInfo.InvariantCulture);
                        q.OrderType = (long)SCAEnums.OrderType.ZHDP;
                        q.OrderNote = orderNoteSb.ToString(); 
                        q.CustomerID = (long)patient.CustomerId;
                        q.OrderCreationType = (long)SCAEnums.OrderCreationType.AutomaticOrder;
                    });

					bool isSaved = SaveOrders(orderProducts);
					if (isSaved)
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.Success));
						++successCount;
					}
					else
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.Exception));
						++failedCount;
					}
				}
				return successCount;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (careHomeRepository != null)
				{
					careHomeRepository.Dispose();
				}
			}
			return default(int);
		}

		/// <summary>
		/// Get Patients due for next order
		/// </summary>
		/// <returns>
		/// List GetPatientsDueForNextOrder
		/// </returns>
		public string GenerateAutomaticCommunityOrders()
		{
			var automaticOrderStatus = new List<AutomaticOrderStatusBusinessModel>();
			ICustomerRepository customerRepository = null;
            try
            {
                customerRepository = new CustomerRepository();
                var customers = customerRepository.GetCustomersForOrderCreation();
                if (!customers.Any())
                {
                    automaticOrderStatus.Add(GenerateAutomaticOrderStatus(0, "", SCAEnums.AutomaticOrderStatusMessage.NoValidCustomers));
                    return "0"; /// not valid patients for order creation
                }

                ///var holidayList = GetHolidayList(); it is not required

                var totalOrders = 0;
                var leadTime = 0;
                var nextDeliveryDate = new DateTime();

                foreach (var customer in customers)
                {
                    leadTime = customer.CustomerParameters.Any() ? customer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;

                    Int64 countryId = customer.CountryId.HasValue ? customer.CountryId.Value : 1;

                    nextDeliveryDate = AddWorkingDays(DateTime.Today, leadTime, countryId, default(Guid).ToString());

                    //nextDeliveryDate = CommonHelper.GetNextWorkingDay(null, DateTime.Today, leadTime);
                    totalOrders += GenerateAutomaticCommunityOrdersForCustomer(customer.CustomerId, customer.SAPCustomerNumber, nextDeliveryDate, automaticOrderStatus);
                }

                return totalOrders.ToString(CultureInfo.InvariantCulture);
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
			finally
			{
				CreateAutomaticOrderLogFile(automaticOrderStatus, SCAEnums.OrderFor.NonCareHomeOrder.ToString(CultureInfo.InvariantCulture));

				if (customerRepository != null)
				{
					customerRepository.Dispose();
				}
			}

			return string.Empty;
		}

		/// <summary>
		/// Gets the community patients.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="nextDeliveryDate">The next delivery date.</param>
		/// <param name="patientRepository">The patient repository.</param>
		/// <returns></returns>
		private List<EFModel.Patient> GetCommunityPatients(long customerId, DateTime nextDeliveryDate, IPatientRepository patientRepository)
		{
			var patients = patientRepository.GetPatientsByCustomerAndNDD(customerId, nextDeliveryDate);
			if (patients.Any())
			{
				///Existing Non Self care Patients
				var communityPatients = patients.Where(p => communityNonSelfCare.Contains((long)p.PatientType) && p.PatientStatus == (long)SCAEnums.PatientStatus.Active && p.DeliveryDate != null).ToList();

				///New Non Self care patients
				var newNonSelfCarePatients = patients.Where(p => communityNonSelfCare.Contains((long)p.PatientType) && p.PatientStatus == (long)SCAEnums.PatientStatus.Active && p.DeliveryDate == null && (p.Round != null && p.Round.ToUpper() == newPatientRound) && (p.RoundId != null && p.RoundId.ToUpper() == newPatientRoundId)).ToList();
				if (newNonSelfCarePatients.Any())
				{
					communityPatients.AddRange(newNonSelfCarePatients);
				}

				///This is for new selfcare - first order (First order means patients DeliveryDate is null)
				var selfCarePatients = patients.Where(p => communitySelfCare.Contains((long)p.PatientType) && p.PatientStatus == (long)SCAEnums.PatientStatus.Active && p.DeliveryDate == null && (p.Round != null && p.Round.ToUpper() == newPatientRound) && (p.RoundId != null && p.RoundId.ToUpper() == newPatientRoundId)).ToList();
				if (selfCarePatients.Any())
				{
					communityPatients.AddRange(selfCarePatients);
				}

				return communityPatients;
			}

			return new List<EFModel.Patient>();
		}

		/// <summary>
		/// Generates the automatic community orders for customer.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="SAPCustomerID">The sap customer identifier.</param>
		/// <param name="nextDeliveryDate">The next delivery date.</param>
		/// <param name="automaticOrderStatus">The automatic order status.</param>
		/// <returns></returns>
		public int GenerateAutomaticCommunityOrdersForCustomer(long customerId, string sapCustomerId, DateTime nextDeliveryDate, List<AutomaticOrderStatusBusinessModel> automaticOrderStatus)
		{
			IPatientRepository patientRepository = null;
			try
			{
				/// Get All Procare/SelfCare patients due for delivery on "nextDeliveryDate"
				patientRepository = new PatientRepository();
				var patients = patientRepository.GetPatientsByCustomerAndNDD(customerId, nextDeliveryDate);
				if (patients.Any())
				{
					var successCount = 0;
					var failedCount = 0;

					var communityPatients = GetCommunityPatients(customerId, nextDeliveryDate, patientRepository);

					if (!communityPatients.Any())
					{
						automaticOrderStatus.Add(GenerateAutomaticOrderStatus(customerId, sapCustomerId, SCAEnums.AutomaticOrderStatusMessage.NoValidPatientsUnderCustomer));
						return default(int);
					}

					foreach (var patient in communityPatients)
					{
						if (patient.Orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) 
                                                && (bool)q.IsRushFlag == false && (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP
                                                && !oneOffOrders.Contains((long)q.OrderCreationType)).Any())
						{
							automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.PatientHasActiveOrder));
							continue;
						}

						var orderProducts = GetValidProductsForPatient(patient.PatientId);
						if (!orderProducts.Any())
						{
							automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.NoValidPrescription));
							continue; /// No products due for delivery for the patient.
						}

                        var newNDD = ValidatePostCodeMatrix(nextDeliveryDate, patient);
                        var derivedNDD = newNDD;
                        var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(newNDD, Convert.ToInt64(patient.CustomerId, CultureInfo.InvariantCulture), default(Guid));
                        ///an arranged date is found in Holiday Process table then use that as a delivery date
                        if (isArrangedDateFound)
                        {
                            newNDD = arrangedNDD;
                        }


                        StringBuilder orderNoteSb = new StringBuilder("Automatic Order Created for Patient - ").Append(patient.PatientId.ToString(CultureInfo.InvariantCulture));
                        orderNoteSb.Append(" for Delivery Date - ").Append(newNDD.ToString(DateFormat, CultureInfo.InvariantCulture));
                        orderProducts.ForEach(q =>
                        {
                            q.DerivedNDD = derivedNDD.ToString(DateFormat, CultureInfo.InvariantCulture);
                            q.DeliveryDate = newNDD.ToString(DateFormat, CultureInfo.InvariantCulture);
                            q.OrderType = (long)SCAEnums.OrderType.ZHDP;
                            q.OrderNote = orderNoteSb.ToString();
                            q.CustomerID = (long)patient.CustomerId;
                            q.OrderCreationType = (long)SCAEnums.OrderCreationType.AutomaticOrder;
                        });
						var isSaved = SaveOrders(orderProducts);
						if (isSaved)
						{
							automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.Success));
							++successCount;
						}
						else
						{
							automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.Exception));
							++failedCount;
						}
					}

					return successCount;
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (patientRepository != null)
				{
					patientRepository.Dispose();
				}
			}

			return default(int);
		}

		/// <summary>
		/// Calculate the next delivery date and upadte the customer parameter related sessin parameters
		/// </summary>
		/// <param name="deliveryDate">The delivery date.</param>
		/// <param name="patientData">The patient data.</param>
		/// <returns>
		/// Next Delivery Date.
		/// </returns>	
        private DateTime ValidatePostCodeMatrix(DateTime deliveryDate, EFModel.Patient patientData)
        {
            var nextDeliveryDate = new DateTime();
            objCommonService = new CommonService();

            Int64 countryId = patientData.Customer.CountryId.HasValue ? patientData.Customer.CountryId.Value : 1;

            objCommonService.PostcodeMatrixList = objCommonService.GetPostcodeMatrixForPatient(Convert.ToString(patientData.CustomerId, CultureInfo.InvariantCulture),
                                                                                                 Convert.ToString(patientData.PatientId, CultureInfo.InvariantCulture),
                                                                                                 patientData.CareHomeId != null ? Convert.ToString(patientData.CareHomeId, CultureInfo.InvariantCulture) : "0",
                                                                                                 Convert.ToString(patientData.ModifiedBy, CultureInfo.InvariantCulture)
                                                                                                 );

            //nextDeliveryDate = objCommonService.GetDateAfterPostcodeMatrix(deliveryDate);
            while (objCommonService.IsHoliday(nextDeliveryDate = objCommonService.GetDateAfterPostcodeMatrix(deliveryDate), countryId, default(Guid).ToString()))
            {
                deliveryDate = nextDeliveryDate.AddDays(1);
            }            
            return nextDeliveryDate;
        }

		/// <summary>
		/// Checks the date availability in holiday process.
		/// </summary>
		/// <param name="nextDeliveryDate">The next delivery date.</param>
		/// <param name="CustomerID">The customer identifier.</param>
		/// <param name="userId">The user identifier.</param>
		/// <returns></returns>
		private bool CheckDateAvailabilityInHolidayProcess(DateTime nextDeliveryDate, long CustomerID, Guid userId)
		{
			var objHolidayBusinessModel = new HolidayProcessAdminBusinessModels();
			objHolidayBusinessModel.CustomerId = CustomerID;
			objHolidayBusinessModel.DerivedNDD = Convert.ToString(nextDeliveryDate, CultureInfo.InvariantCulture);
			objHolidayBusinessModel.OrderCreationTime = Convert.ToString(DateTime.Now.ToString(HhmmFormat, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
			objHolidayBusinessModel.UserId = userId;
			var strArrangedDate = (string)new CommonService().GetArrangedDeliveryDate(objHolidayBusinessModel);
			if (strArrangedDate != null)
			{
				arrangedNDD = Convert.ToDateTime(strArrangedDate, CultureInfo.InvariantCulture);
				return true;
			}
			return false;
		}

		#endregion

		#region AutomaticOrdersCriteria

		#region StatusMessageObjectCreation
		/// <summary>
		/// Generates the automatic order status.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="SAPCustomerId">The sap customer identifier.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatus(long customerId, string SAPCustomerId, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			return GenerateAutomaticOrderStatus(customerId, SAPCustomerId, 0, "", 0, "", 0, "", null, 0, statusMessage);
		}

		/// <summary>
		/// Generates the automatic order status for patient.
		/// </summary>
		/// <param name="SAPPatientId">The sap patient identifier.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatusForPatient(string SAPPatientId, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			return GenerateAutomaticOrderStatus(0, "", 0, SAPPatientId, 0, "", 0, "", null, 0, statusMessage);
		}

		/// <summary>
		/// Generates the automatic order status for care home.
		/// </summary>
		/// <param name="SAPCareHomeId">The sap care home identifier.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatusForCareHome(string SAPCareHomeId, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			return GenerateAutomaticOrderStatus(0, "", 0, "", 0, SAPCareHomeId, 0, "", null, 0, statusMessage);
		}

		/// <summary>
		/// Generates the automatic order status.
		/// </summary>
		/// <param name="patient">The patient.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatus(EFModel.Patient patient, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			return GenerateAutomaticOrderStatus((long)patient.CustomerId, patient.Customer.SAPCustomerNumber, patient.PatientId, patient.SAPPatientNumber, Convert.ToInt64(patient.CareHomeId, CultureInfo.InvariantCulture),
				Convert.ToInt64(patient.CareHomeId, CultureInfo.InvariantCulture) != 0 ? patient.CareHome.SAPCareHomeNumber : "", (long)patient.PatientType, patient.PatientTypeList.Description, null, 0, statusMessage);
		}

		/// <summary>
		/// Generates the automatic order status.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="SAPCustomerId">The sap customer identifier.</param>
		/// <param name="careHomeId">The care home identifier.</param>
		/// <param name="SAPCareHomeId">The sap care home identifier.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatus(long customerId, string SAPCustomerId, long careHomeId, string SAPCareHomeId, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			return GenerateAutomaticOrderStatus(customerId, SAPCustomerId, 0, "", careHomeId, SAPCareHomeId, 0, "", null, 0, statusMessage);
		}

		/// <summary>
		/// Generates the automatic order status.
		/// </summary>
		/// <param name="patient">The patient.</param>
		/// <param name="deliveryDate">The delivery date.</param>
		/// <param name="orderId">The order identifier.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatus(EFModel.Patient patient, DateTime? deliveryDate, long orderId, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			var careHomeId = patient.CareHomeId != null ? (long)patient.CareHomeId : 0;
			return GenerateAutomaticOrderStatus((long)patient.CustomerId, patient.Customer.SAPCustomerNumber, patient.PatientId, patient.SAPPatientNumber, careHomeId,
				careHomeId != 0 ? patient.CareHome.SAPCareHomeNumber : "", (long)patient.PatientType, patient.PatientTypeList.Description, deliveryDate, orderId, statusMessage);
		}

		/// <summary>
		/// Generates the automatic order status.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="SAPCustomerId">The sap customer identifier.</param>
		/// <param name="patientId">The patient identifier.</param>
		/// <param name="SAPPatientId">The sap patient identifier.</param>
		/// <param name="careHomeId">The care home identifier.</param>
		/// <param name="SAPCareHomeId">The sap care home identifier.</param>
		/// <param name="patientTypeId">The patient type identifier.</param>
		/// <param name="patientType">Type of the patient.</param>
		/// <param name="deliveryDate">The delivery date.</param>
		/// <param name="orderId">The order identifier.</param>
		/// <param name="statusMessage">The status message.</param>
		/// <returns></returns>
		private AutomaticOrderStatusBusinessModel GenerateAutomaticOrderStatus(long customerId, string SAPCustomerId,
			long patientId, string SAPPatientId, long careHomeId, string SAPCareHomeId,
			long patientTypeId, string patientType, DateTime? deliveryDate, long orderId, SCAEnums.AutomaticOrderStatusMessage statusMessage)
		{
			var strDeliveryDate = string.Empty;

			if (deliveryDate != null)
			{
				strDeliveryDate = ((DateTime)deliveryDate).ToString(DateFormat, CultureInfo.InvariantCulture);
			}

			var status = SCAEnums.AutomaticOrderStatus.Failed.ToString(CultureInfo.InvariantCulture);
			var message = ((long)statusMessage).ToString(CultureInfo.InvariantCulture);
			if (statusMessage == SCAEnums.AutomaticOrderStatusMessage.Success)
			{
				status = SCAEnums.AutomaticOrderStatus.Success.ToString(CultureInfo.InvariantCulture);
				message = string.Empty;
			}

			var objStatus = new AutomaticOrderStatusBusinessModel()
			{
				CustomerId = customerId,
				SAPCustomerId = SAPCustomerId == null ? string.Empty : CommonHelper.TrimLeadingZeros(SAPCustomerId),
				PatientId = patientId,
				SAPPatientId = SAPPatientId == null ? string.Empty : CommonHelper.TrimLeadingZeros(SAPPatientId),
				DeliveryDate = strDeliveryDate,
				OrderId = orderId,
				PatientTypeId = patientTypeId,
				PatientType = patientType,
				CareHomeId = careHomeId,
				SAPCareHomeId = SAPCareHomeId == null ? string.Empty : CommonHelper.TrimLeadingZeros(SAPCareHomeId),
				Status = status,
				Message = message
			};
			return objStatus;
		}
		#endregion

		private List<AutomaticOrderStatusBusinessModel> ValidateCustomerForOrder(EFModel.Customer customer)
		{
			var errorList = new List<AutomaticOrderStatusBusinessModel>();

			if (customer.Customer_Status == null || Convert.ToInt64(customer.Customer_Status, CultureInfo.InvariantCulture) == (long)SCAEnums.CustomerStatus.Blocked)
			{
				///blocked customer
				errorList.Add(GenerateAutomaticOrderStatus(customer.CustomerId, customer.SAPCustomerNumber, SCAEnums.AutomaticOrderStatusMessage.CustomerBlocked));
			}
			else
			{
				var isOrderCreationAllowed = customer.CustomerParameters.Any() ? customer.CustomerParameters.FirstOrDefault().OrderCreationAllowed : false;
				if (!isOrderCreationAllowed)
				{
					///Order creation not allowed for the customer
					errorList.Add(GenerateAutomaticOrderStatus(customer.CustomerId, customer.SAPCustomerNumber, SCAEnums.AutomaticOrderStatusMessage.OrderNotAllowedForCustomer));
				}
			}

			return errorList;
		}

		/// <summary>
		/// Automatic Orders Generate Criteria
		/// </summary>
		/// <param name="automaticOrderBusinessModel"></param>
		/// <returns>
		/// Boolean value
		/// </returns>
		public List<AutomaticOrderStatusBusinessModel> GenerateAutomaticOrderCriteria(AutomaticOrderCriteriaBusinessModel automaticOrderBusinessModel)
		{
			ICustomerRepository customerRepository = null;
			IPatientRepository patientRepository = null;
			ICareHomeRepository careHomeRepository = null;
			List<AutomaticOrderStatusBusinessModel> automaticOrderStatus = new List<AutomaticOrderStatusBusinessModel>();
            try
            {
                if (automaticOrderBusinessModel == null)
                {
                    automaticOrderStatus.Add(GenerateAutomaticOrderStatus(0, "", SCAEnums.AutomaticOrderStatusMessage.InvalidCriteria));
                    return automaticOrderStatus;
                }

                var runDate = DateTime.ParseExact(automaticOrderBusinessModel.Rundate, DateFormat, CultureInfo.InvariantCulture);
                var isOnlyCustomer = automaticOrderBusinessModel.IsOnlyCustomerChecked;
                var isTestRun = automaticOrderBusinessModel.IsTestRun;

                var customerList = automaticOrderBusinessModel.Customers;
                if (!customerList.Any())
                {
                    automaticOrderStatus.Add(GenerateAutomaticOrderStatus(0, "", SCAEnums.AutomaticOrderStatusMessage.NoValidCustomers));
                    return automaticOrderStatus;
                }

                customerRepository = new CustomerRepository();
                patientRepository = new PatientRepository();
                careHomeRepository = new CareHomeRepository();

                var patientsForOrder = new List<EFModel.Patient>();
                var validCustomers = new List<EFModel.Customer>();

                foreach (var customerId in customerList)
                {
                    var customer = customerRepository.GetCustomerById(customerId);
                    if (customer == null)
                    {
                        ///Invalid customer Id
                        automaticOrderStatus.Add(GenerateAutomaticOrderStatus(customerId, "", SCAEnums.AutomaticOrderStatusMessage.InvalidCustomerId));
                        continue;
                    }
                    var customerErrors = ValidateCustomerForOrder(customer);
                    if (customerErrors.Any())
                    {
                        automaticOrderStatus.AddRange(customerErrors);
                        continue;
                    }
                    validCustomers.Add(customer);
                }

                if (isOnlyCustomer)
                {
                    foreach (var customer in validCustomers)
                    {

                        Int64 countryId = customer.CountryId.HasValue ? customer.CountryId.Value : 1;

                        var leadTime = customer.CustomerParameters.Any() ? customer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;

                        var originalNDD = AddWorkingDays(runDate, leadTime, countryId, default(Guid).ToString());
                        //var originalNDD = CommonHelper.GetNextWorkingDay(null, runDate, leadTime);

                        var customerPatients = GetCommunityPatients(customer.CustomerId, originalNDD, patientRepository);

                        var careHomes = customer.CareHomes.Where(q => q.NextDeliveryDate <= originalNDD).ToList();
                        if (careHomes.Any())
                        {
                            foreach (var careHome in careHomes)
                            {
                                var careHomePatients = GetCareHomePatients(careHome, originalNDD);
                                customerPatients.AddRange(careHomePatients);
                            }
                        }

                        if (!customerPatients.Any())
                        {
                            ///No valid patients for the specified criteria
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(customer.CustomerId, customer.SAPCustomerNumber, SCAEnums.AutomaticOrderStatusMessage.NoValidPatientsUnderCustomer));
                            continue;
                        }
                        else
                        {
                            patientsForOrder.AddRange(customerPatients);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(automaticOrderBusinessModel.CareHomeIds))
                {
                    var careHomesIdsList = automaticOrderBusinessModel.CareHomeIds.Split(automaticCriteriaIdSeperator).ToList();
                    careHomesIdsList = careHomesIdsList.Select(q => CommonHelper.AppendZeroAtStart(q, 10)).ToList(); //SAP ID Length is 10
                    var careHomes = careHomeRepository.GetCareHomesBySAPId(careHomesIdsList);

                    #region invalidCarehomeId
                    var invalidCareHomes = new List<string>();
                    if (!careHomes.Any())
                    {
                        invalidCareHomes.AddRange(careHomesIdsList);
                    }
                    else
                    {
                        foreach (var strSAPId in careHomesIdsList)
                        {
                            if (!careHomes.Where(q => q.SAPCareHomeNumber.Contains(strSAPId)).Any())
                            {
                                invalidCareHomes.Add(strSAPId);
                            }
                        }
                    }
                    foreach (var invalidCareHomeId in invalidCareHomes)
                    {
                        automaticOrderStatus.Add(GenerateAutomaticOrderStatusForCareHome(invalidCareHomeId, SCAEnums.AutomaticOrderStatusMessage.InvalidSAPCareHomeId));
                    }
                    #endregion

                    foreach (var careHome in careHomes)
                    {
                        var careHomeCustomer = careHome.Customer;
                        if (!customerList.Contains(careHomeCustomer.CustomerId))
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(careHomeCustomer.CustomerId, careHomeCustomer.SAPCustomerNumber, 0, "", careHome.CareHomeId, careHome.SAPCareHomeNumber, 0, "", null, 0, SCAEnums.AutomaticOrderStatusMessage.CareHomeNotUnderCustomer));
                            continue;
                        }

                        Int64 countryId = careHomeCustomer.CountryId.HasValue ? careHomeCustomer.CountryId.Value : 1;

                        var leadTime = careHomeCustomer.CustomerParameters.Any() ? careHomeCustomer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;
                        var originalNDD = AddWorkingDays(runDate, leadTime, countryId, default(Guid).ToString());
                        //var originalNDD = CommonHelper.GetNextWorkingDay(null, runDate, leadTime);

                        if (careHome.NextDeliveryDate > originalNDD)
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(careHomeCustomer.CustomerId, careHomeCustomer.SAPCustomerNumber, 0, "", careHome.CareHomeId, careHome.SAPCareHomeNumber, 0, "", null, 0, SCAEnums.AutomaticOrderStatusMessage.CareHomeNotDueForOrder));
                            continue;
                        }

                        var careHomePatients = GetCareHomePatients(careHome, originalNDD);
                        if (!careHomePatients.Any())
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(careHomeCustomer.CustomerId, careHomeCustomer.SAPCustomerNumber, 0, "", careHome.CareHomeId, careHome.SAPCareHomeNumber, 0, "", null, 0, SCAEnums.AutomaticOrderStatusMessage.NoValidPatientUnderCareHome));
                            continue;
                        }
                        patientsForOrder.AddRange(careHomePatients);
                    }
                }
                else if (!string.IsNullOrEmpty(automaticOrderBusinessModel.PatientIds))
                {
                    var patientIdsList = automaticOrderBusinessModel.PatientIds.Split(automaticCriteriaIdSeperator).ToList();
                    patientIdsList = patientIdsList.Select(q => CommonHelper.AppendZeroAtStart(q, 10)).ToList(); //SAP ID Length is 10
                    var patients = patientRepository.GetPatientsBySAPId(patientIdsList);
                    var invalidPatients = new List<string>();
                    if (!patients.Any())
                    {
                        invalidPatients.AddRange(patientIdsList);
                    }
                    else
                    {
                        foreach (var strSAPId in patientIdsList)
                        {
                            if (!patients.Where(q => q.SAPPatientNumber.Contains(strSAPId)).Any())
                            {
                                invalidPatients.Add(strSAPId);
                            }
                        }
                    }

                    foreach (var invalidPatientId in invalidPatients)
                    {
                        automaticOrderStatus.Add(GenerateAutomaticOrderStatusForPatient(invalidPatientId, SCAEnums.AutomaticOrderStatusMessage.InvalidSAPPatientId));
                    }

                    foreach (var patient in patients)
                    {
                        var patientCustomer = patient.Customer;
                        var leadTime = patientCustomer.CustomerParameters.Any() ? patientCustomer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;

                        Int64 countryId = patientCustomer.CountryId.HasValue ? patientCustomer.CountryId.Value : 1;

                        var originalNDD = AddWorkingDays(runDate, leadTime, countryId, default(Guid).ToString());

                        //var originalNDD = CommonHelper.GetNextWorkingDay(null, runDate, leadTime);

                        if (!customerList.Contains(patientCustomer.CustomerId))
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, SCAEnums.AutomaticOrderStatusMessage.PatientNotUnderCustomer));
                            continue;
                        }
                        if (patient.PatientStatus != (long)SCAEnums.PatientStatus.Active)
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, originalNDD, 0, SCAEnums.AutomaticOrderStatusMessage.PatientNotActive));
                            continue;
                        }
                        else if (allAutomaticOrdersRestrictedTypes.Contains((long)patient.PatientType))
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, originalNDD, 0, SCAEnums.AutomaticOrderStatusMessage.AllAutomaticOrdersRestricted));
                            continue;
                        }
                        else if (patient.NextDeliveryDate > originalNDD)
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, originalNDD, 0, SCAEnums.AutomaticOrderStatusMessage.PatientNotDueForOrder));
                            continue;
                        }
                        else if ((communitySelfCare.Contains((long)patient.PatientType) || careHomeSelfCare.Contains((long)patient.PatientType)) && patient.DeliveryDate != null)
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, originalNDD, 0, SCAEnums.AutomaticOrderStatusMessage.NotFirstOrderSelfCare));
                            continue;
                        }
                        else if (HasAnyActiveOrder(patient.PatientId))
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, originalNDD, 0, SCAEnums.AutomaticOrderStatusMessage.PatientHasActiveOrder));
                            continue;
                        }

                        patientsForOrder.Add(patient);
                    }
                }

                foreach (var patient in patientsForOrder.OrderBy(q => q.CustomerId))
                {
                    var patientCustomer = patient.Customer;

                    long careHomeId = (patient.CareHomeId == null || patient.CareHomeId == 0) ? 0 : (long)patient.CareHomeId;
                    var SAPCareHomeId = careHomeId == 0 ? "" : patient.CareHome.SAPCareHomeNumber;

                    var leadTime = patientCustomer.CustomerParameters.Any() ? patientCustomer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;

                    Int64 countryId = patientCustomer.CountryId.HasValue ? patientCustomer.CountryId.Value : 1;

                    var nextDeliveryDate = AddWorkingDays(runDate, leadTime, countryId, default(Guid).ToString());

                    //var nextDeliveryDate = CommonHelper.GetNextWorkingDay(null, runDate, leadTime);

                    if (HasAnyActiveOrder(patient.PatientId))
                    {
                        automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, nextDeliveryDate, 0, SCAEnums.AutomaticOrderStatusMessage.PatientHasActiveOrder));
                        continue;
                    }

                    nextDeliveryDate = ValidatePostCodeMatrix(nextDeliveryDate, patient);
                    var derivedNDD = nextDeliveryDate;
                    var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(nextDeliveryDate, Convert.ToInt64(patient.CustomerId, CultureInfo.InvariantCulture), default(Guid));
                    ///an arranged date is found in Holiday Process table then use that as a delivery date
                    if (isArrangedDateFound)
                    {
                        nextDeliveryDate = arrangedNDD;
                    }

                    var orderProducts = GetValidProductsForPatient(patient.PatientId);
                    if (!orderProducts.Any())
                    {
                        /// No products due for delivery for the patient.
                        automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, nextDeliveryDate, 0, SCAEnums.AutomaticOrderStatusMessage.NoValidPrescription));
                        continue;
                    }
                    orderProducts.ForEach(o => { o.CustomerID = (long)patient.CustomerId; });

                    if (isTestRun)
                    {
                        ///Success
                        automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, nextDeliveryDate, 0, SCAEnums.AutomaticOrderStatusMessage.Success));
                    }
                    else
                    {
                        var result = new List<KeyValuePair<long, long>>();
                        StringBuilder orderNoteSb = new StringBuilder("Automatic Order Created for Patient - ").Append(patient.PatientId.ToString(CultureInfo.InvariantCulture));
                        orderNoteSb.Append(" for Delivery Date - ").Append(nextDeliveryDate.ToString(DateFormat, CultureInfo.InvariantCulture));
                        orderProducts.ForEach(q =>
                        {
                            q.DerivedNDD = derivedNDD.ToString(DateFormat, CultureInfo.InvariantCulture);
                            q.DeliveryDate = nextDeliveryDate.ToString(DateFormat, CultureInfo.InvariantCulture);
                            q.OrderType = (long)SCAEnums.OrderType.ZHDP;
                            q.OrderNote = orderNoteSb.ToString();
                            q.OrderCreationType = (long)SCAEnums.OrderCreationType.AutomaticOrder;
                        });

                        var isSaved = SaveOrdersData(orderProducts, DateTime.Now, result);
                        if (isSaved)
                        {
                            long orderId = 0;
                            if (result.Any())
                            {
                                orderId = result.First().Value;
                            }
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, nextDeliveryDate, orderId, SCAEnums.AutomaticOrderStatusMessage.Success));
                        }
                        else
                        {
                            automaticOrderStatus.Add(GenerateAutomaticOrderStatus(patient, nextDeliveryDate, 0, SCAEnums.AutomaticOrderStatusMessage.Exception));
                        }
                    }
                }
                return automaticOrderStatus;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
			finally
			{
				if (customerRepository != null)
				{
					customerRepository.Dispose();
				}
				if (patientRepository != null)
				{
					patientRepository.Dispose();
				}
				if (careHomeRepository != null)
				{
					careHomeRepository.Dispose();
				}
			}
			return null;
		}


		/// <summary>
		/// Check for advance order activation days
		/// </summary>
		/// <param name="patient"></param>
		/// <param name="nextDeliveryDate"></param>
		/// <param name="DateForOrderActivation"></param>
		/// <param name="canActivateOrder"></param>
		private static void OrderActivationDays(EFModel.Patient patient, DateTime nextDeliveryDate, out string DateForOrderActivation, out bool canActivateOrder)
		{
			var advOrdActDays = patient.Customer.PatientTypeMatrices.Where(q => (q.PatientType == patient.PatientType && q.CustomerId == patient.CustomerId && (bool)q.IsRemove == false)).FirstOrDefault().AdvanceOrderActivationDays;
			DateForOrderActivation = nextDeliveryDate.AddDays(0 - Convert.ToInt64(advOrdActDays, CultureInfo.InvariantCulture)).ToString(DateFormat, CultureInfo.InvariantCulture);
			canActivateOrder = DateTime.Today >= nextDeliveryDate.AddDays(0 - Convert.ToInt64(advOrdActDays, CultureInfo.InvariantCulture)) ? true : false;
		}

		/// <summary>
		/// Has Any Active Order
		/// </summary>
		/// <param name="patient"></param>
		/// <returns>true or false</returns>
		private static bool HasAnyActiveOrder(EFModel.Patient patient)
		{
           List<long> oneOffOrder = new List<long>()
															{
																(long)SCAEnums.OrderCreationType.CarehomeOneOffFOC,
																(long)SCAEnums.OrderCreationType.CarehomeOneOffStandard,
																(long)SCAEnums.OrderCreationType.CarehomeOneOffRush,
																(long)SCAEnums.OrderCreationType.CarehomePatientOneOffFOC,
																(long)SCAEnums.OrderCreationType.CarehomePatientOneOffRush,
																(long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard,
																(long)SCAEnums.OrderCreationType.CommunityOneOffFOC,
																(long)SCAEnums.OrderCreationType.CommunityOneOffRush,
																(long)SCAEnums.OrderCreationType.CommunityOneOffStandard
															};
			if (patient.Orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) 
                                    && (bool)q.IsRushFlag == false 
                                    && (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP
                                    && !oneOffOrder.Contains((long)q.OrderCreationType)).Any())
			{
				return true;
			}
			return false;           

		}

		/// <summary>
		/// Determines whether [has any active order] [the specified patient identifier].
		/// </summary>
		/// <param name="patientId">The patient identifier.</param>
		/// <returns>true or false</returns>
		public bool HasAnyActiveOrder(long patientId)
		{
			IPatientRepository patientRepository = null;
			try
			{
				patientRepository = new PatientRepository();
				var selectedPatient = new EFModel.Patient();
				selectedPatient = patientRepository.Find(patientId);
				return HasAnyActiveOrder(selectedPatient);
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (patientRepository != null)
				{
					patientRepository.Dispose();
				}
			}
			return false;
		}

		/// <summary>
		/// Gets the customer order creation not allowed list.
		/// </summary>
		/// <returns>list of values</returns>
		public List<long> GetCustomerOrderCreationNotAllowedList()
		{
			ICustomerRepository customerRepository = null;
			List<long> orderCreationNotAllowedList = new List<long>();
			bool isOrderCreationAllowed = false;
			try
			{
				customerRepository = new CustomerRepository();
				var customers = customerRepository.All;
				foreach (var customer in customers)
				{
					isOrderCreationAllowed = customer.CustomerParameters.Any() ? customer.CustomerParameters.FirstOrDefault().OrderCreationAllowed : false;
					if (!isOrderCreationAllowed)
					{
						orderCreationNotAllowedList.Add(customer.CustomerId);
					}
				}
				return orderCreationNotAllowedList;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (customerRepository != null)
				{
					customerRepository.Dispose();
					customerRepository = null;
				}
			}
			return null;
		}

		#endregion

		#region Interaction

		/// <summary>
		/// Inserts the into interaction.
		/// </summary>
		/// <param name="orders">The orders.</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="isOneOffOrder">if set to <c>true</c> [is one off order].</param>
		/// <returns></returns>
		private bool InsertIntoInteraction(List<EFModel.Order> orders, long customerId, bool isOneOffOrder, string userId, bool isNDDMismatch = false, bool isNDDManuallyChanged = false, string derivedNDD = "", bool isActivatedOrder = false, string orderInteractionNote="")
        {            
            StringBuilder description = new StringBuilder();
            IInteractionRepository interactionRepository = null;
            IOrderRepository orderRepository = null;                    /// Required to fetch the description of Order Type
            IProductRepository productRepository = null;                /// Required to fetch the product description         
            INotesRepository notesRepository = null;                    /// Required to fetch the notes
            List<long?> productIdList = new List<long?>();
            List<EFModel.Product> products = new List<EFModel.Product>();
            List<EFModel.IDXOrderProduct> orderProducts = new List<EFModel.IDXOrderProduct>();
            Note orderNotes = new Note();
            DateTime date = DateTime.Now;
            long orderCount = 0;

            try
            {
                notesRepository = new NotesRepository();
                interactionRepository = new InteractionRepository();
                productRepository = new ProductRepository();
                orderRepository = new OrderRepository();

                string ordertype = string.Empty;
                string welcomeline = interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionWelcomeMessge, (long)SCAEnums.ListType.LanguageId);
                string productdescription = interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionProductDescription, (long)SCAEnums.ListType.LanguageId);
                EFModel.Interaction interactionRecord = null;
                EFModel.Order interactionOrder = null;

				foreach (var order in orders)
				{
					interactionOrder = orderRepository.Find(order.OrderId);
					if (interactionOrder == null)
					{
						LogHelper.LogAction("InsertIntoInteraction - Order Id " + order.OrderId + " not found ", "order.cs", "InsertIntoInteraction", userId);
						continue;
					}

					try
					{
						description = new StringBuilder();
						interactionRecord = new EFModel.Interaction();
						interactionRecord.OrderId = interactionOrder.OrderId;
						interactionRecord.CreatedBy = interactionOrder.ModifiedBy;
						interactionRecord.CreatedDate = date;
                        interactionRecord.ClosedBy = interactionOrder.ModifiedBy;
                        interactionRecord.ClosedDate = date;                        
                        interactionRecord.ResolvedBy = interactionOrder.ModifiedBy;
                        interactionRecord.ResolvedDate = date;
                        interactionRecord.Resolved = true;
					
						/// Search interaction record based on order Id    
						orderCount = interactionRepository.All.Where(a => a.OrderId == order.OrderId).Count();

                        bool isSampleOrderType = (orders.FirstOrDefault().OrderType == (long)SCAEnums.OrderType.SAMP) ? true : false; // checking for orderType and changing InteractionSubType
                        
                        if (!isSampleOrderType)
                        {
                        if (!(orderCount >= 1))
                        {
                                interactionRecord.InteractionSubTypeId = GetInteractionSubTypeId(interactionOrder.OrderCreationType);
                            }
                            else
                            {
                                interactionRecord.InteractionSubTypeId = Convert.ToInt64(SCAEnums.InteractionSubType.OrderAmendment, CultureInfo.InvariantCulture);
                            }
                        }
                        else
                        {
                            interactionRecord.InteractionSubTypeId = Convert.ToInt64(SCAEnums.InteractionSubType.SampleOrderFromOrder, CultureInfo.InvariantCulture);
                        }

                        /// Get  the Product Id list form particular order.
                        productIdList = interactionOrder.IDXOrderProducts.Select(a => a.ProductId != null ? a.ProductId : a.SampleProductId).ToList();

						/// Get the Product Description and Quantity for particular order
						products = productRepository.GetProductsByProductIds(productIdList);
						orderProducts = interactionOrder.IDXOrderProducts.ToList();

						if (order.OrderType == (long)SCAEnums.OrderType.ZHDP ||
								order.OrderType == (long)SCAEnums.OrderType.ZHDH)
						{
							if (order.PatientId != null)
							{
								ordertype = interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionSelfCareCommunity, (long)SCAEnums.ListType.LanguageId);
							}

							if (order.CareHomeId != null)
							{
								ordertype = interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionSelfcareCareHome, (long)SCAEnums.ListType.LanguageId);
							}

							if (isOneOffOrder)
							{
								bool isFOC = order.ExcludeServiceCharge == true && !order.IDXOrderProducts.Where(q => q.IsFOC != true).Any();
								ordertype = interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionOneOfStandard, (long)SCAEnums.ListType.LanguageId);

								if (isFOC)
								{
									ordertype = string.Concat(ordertype, CommonConstants.FOC);
								}
							}
						}
						else
						{
                            ordertype = (new CommonService()).GetTextFromTranslation(interactionOrder.List1.ListId, (long)SCAEnums.ListType.LanguageId);
                        }

                        if (!(string.IsNullOrEmpty(orderInteractionNote)))
                            description.AppendFormat("** " + orderInteractionNote.Trim() + " **").Append(CommonConstants.NewLineCharacter).Append(CommonConstants.NewLineCharacter);

                        var descriptionList = products.Select(q => new { q.DescriptionUI, q.BaseMaterial, Quantity = orderProducts.Where(o => (o.ProductId != null ? o.ProductId : o.SampleProductId) == q.ProductId).Select(o => o.Quantity).First() }).ToList();
                        description.AppendFormat(welcomeline, isSampleOrderType ? ordertype + "," : ordertype, order.SAPOrderDeliveryDate.Value.ToString(DateFormat, CultureInfo.InvariantCulture));

                        if (isNDDMismatch)
                        {
                            string strNDDChanged = " ";

                            if (isNDDManuallyChanged)
                                strNDDChanged += interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionNDDManually, (long)SCAEnums.ListType.LanguageId);
                            else if (!isActivatedOrder)
                                strNDDChanged += interactionRepository.GetDescriptionFromTransproxy((long)SCAEnums.InteractionDescription.InteractionNDDHolidayProcessAdmin, (long)SCAEnums.ListType.LanguageId);

                            description.AppendFormat(strNDDChanged, derivedNDD, order.SAPOrderDeliveryDate.Value.ToString(DateFormat, CultureInfo.InvariantCulture));
                        }

                        description.Append(CommonConstants.NewLineCharacter);

						foreach (var item in descriptionList)
						{
							productdescription = productdescription.Replace(@"\n", Environment.NewLine).Trim();
							description.AppendFormat(productdescription, item.BaseMaterial, item.DescriptionUI, item.Quantity).Append(CommonConstants.NewLineCharacter);
                        }
                        
						interactionRecord.Description = Convert.ToString(description, CultureInfo.InvariantCulture);
						interactionRecord.CustomerId = customerId;
						interactionRecord.PatientId = interactionOrder.PatientId;
						interactionRecord.CarehomeId = interactionOrder.CareHomeId;
                        interactionRecord.InteractionTypeId = GetInteractionTypeId(interactionOrder.OrderCreationType);
						interactionRecord.StatusId = Convert.ToInt64(SCAEnums.InteractionStatus.Completed, CultureInfo.InvariantCulture);
						interactionRecord.DisplayAsAlert = false;						
						interactionRepository.InsertOrUpdate(interactionRecord);
					}
					catch (Exception ex)
					{
						LogHelper.LogAction("Insert Into Interaction - Exception Occurred for order Id " + order.OrderId, "Order.cs", "InsertIntoInteraction", userId);
						LogHelper.LogException(ex);
					}
				}

                IUnitOfWork interactionUnitOfWork = interactionRepository.UnitOfWork;
                interactionUnitOfWork.Commit();                
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (interactionRepository != null)
                {
                    interactionRepository.Dispose();
                }

                if (orderRepository != null)
                {
                    orderRepository.Dispose();
                }

                if (productRepository != null)
                {
                    productRepository.Dispose();
                }

                if (notesRepository != null)
                {
                    notesRepository.Dispose();
                }
            }

            return true;
        }

		#endregion

		#region Telephone & Online Orders

		/// <summary>
		/// ValidateNDDforPastDate
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="currentNDD"></param>
		/// <returns>NDD</returns>
		private DateTime ValidateNDDforPastDate(EFModel.Customer customer, DateTime currentNDD, Int64 countryId, string userID)
		{
			var leadTime = customer.CustomerParameters.Any() ? customer.CustomerParameters.FirstOrDefault().OrderLeadTime : LeadTimeForAutomaticOrder;
            var nextDeliveryDate = AddWorkingDays(DateTime.Today, leadTime, countryId, userID);
			return currentNDD < nextDeliveryDate ? nextDeliveryDate : currentNDD;
		}


		/// <summary>
		/// Generate Online Telephone Order
		/// </summary>
		/// <param name="onlineOrders"></param>
		public void GenerateOnlineTelephoneOrder(List<OnlineOrdersBusinessModel> onlineOrders)
		{
			try
			{
				///No valid data for processing
				if (!onlineOrders.Any())
				{
					return;
				}

				foreach (var item in onlineOrders)
				{
					EFModel.Patient patient;
					if (ProcessOnlineTelephoneOrderForPatient(item, out patient))
					{
						item.Status = "Success";
						if ((!string.IsNullOrEmpty(item.AllowByEmail) && item.AllowByEmail.ToUpper() == "X")
							&& (!string.IsNullOrEmpty(item.Email) && CommonHelper.ValidateEmail(item.Email)))
						{
							var emailBody = string.Format(CultureInfo.InvariantCulture, EmailTemplate.OnlineTelephoneEmailBody, (patient.PersonalInformation.FirstName + " " + patient.PersonalInformation.LastName), Convert.ToDateTime(item.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture));
							SendMail(EmailTemplate.OnlineTelephoneSubject, emailBody, item.Email);
						}
					}
					else
					{
						item.Status = "Failed";
					}
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				LogOnlineTelephoneOrderForPatient(onlineOrders);
			}
		}

		/// <summary>
		/// Process Online Telephone Order For Patient
		/// </summary>
		/// <param name="onlineOrder"></param>
		/// <param name="OutPatient"></param>
		/// <returns>return true or false</returns>
		private bool ProcessOnlineTelephoneOrderForPatient(OnlineOrdersBusinessModel onlineOrder, out EFModel.Patient OutPatient)
		{
			var isValid = false;
			OutPatient = new EFModel.Patient();
			IPatientRepository patientRepository = null;
            try
            {
                if (string.IsNullOrEmpty(onlineOrder.UserID))
                {
                    onlineOrder.UserID = default(Guid).ToString();
                }
                patientRepository = new PatientRepository();
                var sbErrorMessage = new StringBuilder();
                if (string.IsNullOrEmpty(onlineOrder.SAPPatientID))
                {
                    sbErrorMessage.AppendLine("SAP Patient ID cannot be NULL/Empty.");
                }

                if (string.IsNullOrEmpty(onlineOrder.SAPCustomerID))
                {
                    sbErrorMessage.AppendLine("SAP Customer ID cannot be NULL/Empty.");
                }

                DateTime nextDeliveryDate = default(DateTime), createdDate = default(DateTime), dateOfBirth = default(DateTime);
                if (!CommonHelper.IsValidDateTimeString(onlineOrder.CreateDate, out createdDate))
                {
                    sbErrorMessage.AppendLine("Invalid Create Date.");
                }

                if (!CommonHelper.IsValidDateTimeString(onlineOrder.BirthDate, out dateOfBirth))
                {
                    sbErrorMessage.AppendLine("Invalid Date of Birth.");
                }

                //SR#1348359: NDD will be patient NDD always. NDD from xml/SAP should be ignored whether it is blank/invalid/valid
                //if (!CommonHelper.IsValidDateTimeString(onlineOrder.NextDeliveryDate, out nextDeliveryDate))
                //{
                //    sbErrorMessage.AppendLine("Invalid Next Delivery Date.");
                //}

                if (!string.IsNullOrEmpty(onlineOrder.Email) && !CommonHelper.ValidateEmail(onlineOrder.Email))
                {
                    sbErrorMessage.AppendLine("Invalid Email.");
                }

                if (!string.IsNullOrEmpty(sbErrorMessage.ToString()))
                {
                    onlineOrder.ErrorMessage = sbErrorMessage.ToString();
                    return isValid;
                }

                var correctedSAPPatientId = CommonHelper.GetAppenedSapNumber(onlineOrder.SAPPatientID);
                var patient = patientRepository.GetPatientsBySAPId(new List<string>() { correctedSAPPatientId }).ToList().FirstOrDefault();
                if (patient == null)
                {
                    sbErrorMessage.AppendLine("Invalid SAP Patient ID.");
                }

                else if (patient.Customer.SAPCustomerNumber != onlineOrder.SAPCustomerID)
                {
                    sbErrorMessage.AppendLine("Invalid Customer Id. Patient does not belong to the specifiend Customer Id " + onlineOrder.SAPCustomerID);
                }

                else if (patient.DateofBirth.HasValue &&
                    patient.DateofBirth.Value.Date != dateOfBirth.Date)
                {
                    sbErrorMessage.AppendLine("Patient ID and DOB do not match");
                }
                else if (patient.PatientStatus != (long)SCAEnums.PatientStatus.Active)
                {
                    sbErrorMessage.AppendLine("Patient is not Active.");
                }
                else if (!onlineOrderSelfCare.Contains((long)patient.PatientType))  //---online Telephone/Online Activations for 4 types of patient type only
                {
                    sbErrorMessage.AppendLine("Online Order cannot be created for patient type " + patient.PatientTypeList.DefaultText);
                }

                else if (HasAnyActiveOrder(patient))
                {
                    sbErrorMessage.AppendLine("Patient Already has an active order.");
                }
                if (!string.IsNullOrEmpty(sbErrorMessage.ToString()))
                {
                    onlineOrder.ErrorMessage = sbErrorMessage.ToString();
                    return isValid;
                }

                onlineOrder.PatientID = patient.PatientId;
                onlineOrder.CustomerID = Convert.ToInt64(patient.CustomerId, CultureInfo.InvariantCulture);
                nextDeliveryDate = patient.NextDeliveryDate ?? System.DateTime.Now;//assign patient's NDD

                Int64 countryId = patient.Customer.CountryId.HasValue ? patient.Customer.CountryId.Value : 1;

                nextDeliveryDate = ValidateNDDforPastDate(patient.Customer, nextDeliveryDate, countryId, onlineOrder.UserID); //validate for NDD is not a past NDD               

                nextDeliveryDate = ValidatePostCodeMatrix(nextDeliveryDate, patient); //validate post code matrix                                

                var derivedNDD = nextDeliveryDate;
                ///an arranged date is found in Holiday Process table then use that as a delivery date
                if (CheckDateAvailabilityInHolidayProcess(nextDeliveryDate, Convert.ToInt64(patient.CustomerId, CultureInfo.InvariantCulture), Guid.Parse(onlineOrder.UserID)))
                {
                    nextDeliveryDate = arrangedNDD;
                }
                //check for advance order activation days
                string DateForOrderActivation = string.Empty;
                bool canActivateOrder = true;
                //if (Array.IndexOf(selfcarePatient, (long)patient.PatientType) > -1)
                //{
                OrderActivationDays(patient, nextDeliveryDate, out DateForOrderActivation, out canActivateOrder);
                if (!canActivateOrder)
                {
                    onlineOrder.ErrorMessage = "Cannot activate order before " + DateForOrderActivation;
                    return isValid;
                }
                //}                				
                onlineOrder.NextDeliveryDate = Convert.ToString(nextDeliveryDate);
                var orderProducts = GetValidProductsForPatient(patient.PatientId);
                if (!orderProducts.Any())
                {
                    /// No products due for delivery for the patient.
                    onlineOrder.ErrorMessage = SCAEnums.AutomaticOrderStatusMessage.NoValidPrescription.ToString(CultureInfo.InvariantCulture);
                    return isValid;
                }

                if (patient.CareHome != null)
                {
                    orderProducts.ForEach(o => { o.CustomerID = (long)patient.CustomerId; o.CareHomeID = (long)patient.CareHomeId; });
                }
                else
                {
                    orderProducts.ForEach(o => { o.CustomerID = (long)patient.CustomerId; });
                }

                var result = new List<KeyValuePair<long, long>>();
                StringBuilder orderNoteSb = new StringBuilder();


                long orderCreationType = 0;
                if (!string.IsNullOrEmpty(onlineOrder.Email))
                {
                    orderNoteSb.Append("Online Order Created for Patient - ").Append(patient.PatientId.ToString(CultureInfo.InvariantCulture));
                    orderCreationType = (long)SCAEnums.OrderCreationType.Online;
                }
                else
                {
                    orderCreationType = (long)SCAEnums.OrderCreationType.Telephonic;
                    orderNoteSb.Append("Telephone Order Created for Patient - ").Append(patient.PatientId.ToString(CultureInfo.InvariantCulture));
                }
                orderNoteSb.Append(" for Delivery Date - ").Append(nextDeliveryDate.ToString(DateFormat, CultureInfo.InvariantCulture));
                orderProducts.ForEach(q =>
                        {
                            q.DerivedNDD = derivedNDD.ToString(DateFormat, CultureInfo.InvariantCulture);
                            q.DeliveryDate = nextDeliveryDate.ToString(DateFormat, CultureInfo.InvariantCulture);
                            q.OrderType = (long)SCAEnums.OrderType.ZHDP; q.OrderNote = orderNoteSb.ToString();
                            q.OrderCreationType = orderCreationType;
                            q.EmailId = onlineOrder.Email;
                        });
                var isSaved = SaveOrdersData(orderProducts, createdDate, result);
                if (isSaved && result.Any() && result.First().Value != 0)
                {
                    var orderId = result.First().Value;                    
                    onlineOrder.IndigoPortalOrderID = orderId;
                }
                else
                {
                    onlineOrder.ErrorMessage = "Some technical error occurred. Please contact Administrator.";
                    return isValid;
                }

                OutPatient = patient;
                isValid = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
			finally
			{
				if (patientRepository != null)
				{
					patientRepository.Dispose();
				}
			}
			return isValid;
		}

		/// <summary>
		/// Log Online Telephone Order For Patient
		/// </summary>
		/// <param name="onlineOrders"></param>
		private void LogOnlineTelephoneOrderForPatient(List<OnlineOrdersBusinessModel> onlineOrders)
		{
			onlineOrders.ForEach(q =>
			{
				q.CreateDate = q.CreateDate.Replace('/', '-');
				q.NextDeliveryDate = q.NextDeliveryDate.Replace('/', '-');
				q.BirthDate = q.BirthDate.Replace('/', '-');
			});
			var columnsToRemove = new List<string>() { "CustomerID", "PatientID", "UserID" };
			CommonService.ExportToExcel(onlineOrders, onlineTelephoneOrderLog, OnlineTelephoneOrder, columnsToRemove, this.onlineTelephoneOrderDirectory, false);
		}

		/// <summary>
		/// Send Mail
		/// </summary>
		/// <param name="subject"></param>
		/// <param name="message"></param>
		/// <param name="to"></param>
		public void SendMail(string subject, string message, string to)
		{
			SmtpClient smtpClient = null;
			MailMessage mail = null;
			try
			{
				var paramName = string.Empty;
				if (string.IsNullOrEmpty(subject))
				{
					paramName = "Subject";
					throw new ArgumentNullException(paramName, "Send mail subject is null or empty.");
				}

				if (string.IsNullOrEmpty(message))
				{
					paramName = "Message";
					throw new ArgumentNullException(paramName, "Send mail failed message is null or empty.");
				}

				if (string.IsNullOrEmpty(to))
				{
					paramName = "ToAddress";
					throw new ArgumentNullException(paramName, "Send mail failed to address is null or empty.");
				}

				smtpClient = new SmtpClient();
				if (smtpClient != null && smtpClient.DeliveryMethod == System.Net.Mail.SmtpDeliveryMethod.SpecifiedPickupDirectory)
				{
					var directoryPath = smtpClient.PickupDirectoryLocation;
					Common.CommonHelper.CreateDirectoryIfNotExists(directoryPath);
				}

				mail = new MailMessage();
				mail.BodyEncoding = System.Text.Encoding.UTF8;
				mail.IsBodyHtml = false;
				mail.To.Add(to);
				mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"]);
				mail.Subject = subject;
				mail.Body = message;

				smtpClient.Send(mail);
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			finally
			{
				if (smtpClient != null)
				{
					smtpClient.Dispose();
				}

				if (mail != null)
				{
					mail.Dispose();
				}
			}
		}

		#endregion

		#region CareHome ProductLevel Order

        /// <summary>
        /// GetCareHomeDetailsForProductOrder
        /// </summary>
        /// <param name="objcareHome"></param>
        /// <returns></returns>
		public CareHomeBusinessModel GetCareHomeDetailsForProductOrder(CareHomeBusinessModel objcareHome)
		{
			var userId = objcareHome.UserId;
			if (objcareHome.CareHomeId == default(long))
			{
				return new CareHomeBusinessModel();
			}

			try
			{

				ICareHomeRepository careHomeRepository = new CareHomeRepository();
				var carehome = careHomeRepository.Find(objcareHome.CareHomeId);
				var carehomeDetails = new CareHomeBusinessModel
				{
					PurchaseOrderNo = carehome.PurchaseOrderNo,
				};
				return carehomeDetails;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}

			return new CareHomeBusinessModel();
		}
		#endregion

		/// <summary>
		/// Getting recent orders. Order date column to have date with HH:MM:SS
		/// </summary>
		/// <param name="searchId"></param>
		/// <param name="userId">The user identifier.</param>
		/// <param name="recentOrderType"></param>
		/// <returns>
		/// List (PatientOrdersBusinessModel)
		/// </returns>
		public List<PatientOrdersBusinessModel> GetRecentOrders(string searchId, string userId, string recentOrderType)
		{
			IOrderRepository orderRepository = null;			
			try
			{
				orderRepository = new OrderRepository();
				objCommonService = new CommonService();
				IQueryable<EFModel.Order> resultOrders = null;
                var data = new List<IGrouping<string, PatientOrdersBusinessModel>>();
                bool isColor = true;
				if (Convert.ToInt32(recentOrderType, CultureInfo.InvariantCulture) == (int)SCAEnums.ObjectType.Patient)
				{
					resultOrders = orderRepository.GetPatientOrders(Convert.ToInt64(searchId, CultureInfo.InvariantCulture)).OrderByDescending(q => q.OrderDate);
				}
				else
				{
					resultOrders = orderRepository.GetCarehomeOrders(Convert.ToInt64(searchId, CultureInfo.InvariantCulture)).OrderByDescending(q => q.OrderDate);
				}
				var recentOrdersdata = new List<PatientOrdersBusinessModel>();

				foreach (var order in resultOrders)
				{                    
                    var orderProducts = order.IDXOrderProducts.Where(q => q.IsRemoved != true).Select(idx => new PatientOrdersBusinessModel
                    {                        
                        SAPOrderNo = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ?
                                        (idx.Order.HelixOrderNo != null ? Convert.ToString(idx.Order.HelixOrderNo, CultureInfo.InvariantCulture).TrimStart(new char[] { '0' }) : string.Empty) :
                                        (idx.Order.SAPOrderNo != null ? Convert.ToString(idx.Order.SAPOrderNo, CultureInfo.InvariantCulture).TrimStart(new char[] { '0' }) : string.Empty),                     
                        SAPDeliveryNo = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? string.Empty :
                                            (idx.SAPDeliveryNo != null ? Convert.ToString(idx.SAPDeliveryNo, CultureInfo.InvariantCulture).TrimStart(new char[] { '0' }) : string.Empty),
                        ProductDescription = (idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP && idx.SampleProductId != null)? idx.SampleProduct.ProductDescription : (idx.Product != null ? idx.Product.DescriptionUI : idx.ProductDescription),
                        OrderDate = Convert.ToDateTime(idx.Order.OrderDate, CultureInfo.InvariantCulture).ToString(DateFormatHHMMMSS, CultureInfo.InvariantCulture),
                        DeliveryDate = Convert.ToDateTime(idx.Order.SAPOrderDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                        Quantity = idx.Quantity,
                        OrderId = order.OrderId,
                        InvoiceNumber = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? string.Empty : (idx.InvoiceNo != null ? Convert.ToString(idx.InvoiceNo, CultureInfo.InvariantCulture).TrimStart(new char[] { '0' }) : string.Empty),                        
                        RecentOrderCreationType = objCommonService.GetTextFromTranslation(Convert.ToInt64(order.OrderCreationType), (long)SCAEnums.ListType.LanguageId),                        
                        SAPPatientNo = idx.Order.Patient != null ? (idx.Order.Patient.SAPPatientNumber != null ? idx.Order.Patient.SAPPatientNumber.TrimStart(new char[] { '0' }) : string.Empty) : null,
                        ProductCode =  idx.Product != null ? (idx.Product.BaseMaterial != null ? idx.Product.BaseMaterial : string.Empty) : (idx.SampleProduct != null ? idx.SampleProduct.ClientProductID :string.Empty),
                        Unit = idx.Product != null ? (idx.Product.SalesUnit != null ? idx.Product.SalesUnit : string.Empty) : string.Empty,
                        InvoiceDate = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? null : idx.BillingDate,
                        InvoiceQuantity = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? null : idx.BillingQuantity,
                        Plant = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? string.Empty : idx.Plant,
                        PODDate = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? null : idx.PODDate,
                        DeliveryQty = idx.Order.OrderType == (long)SCAEnums.OrderType.SAMP ? null : idx.DeliveryQuantity,
                        ActDeliveryDate = idx.SAPActualDeliveryDate,// Not in use in recent order popup.
                        ActualDeliveryDate = idx.SAPActualDeliveryDate!=null?Convert.ToDateTime(idx.SAPActualDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture):"",                        
                        PatientName = idx.Order.Patient != null ? idx.Order.Patient.PersonalInformation.FirstName + " " + idx.Order.Patient.PersonalInformation.LastName : string.Empty
                    }).ToList();

					recentOrdersdata.AddRange(orderProducts);
				}               
				return recentOrdersdata;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			finally
			{
				if (orderRepository != null)
				{
					orderRepository.Dispose();
				}
			}

			return null;
		}

        /// <summary>
        /// Gets Community Order Details: ADP & Note
        /// </summary>
        /// <param name="objComminityOrderDetails"></param>
        /// <returns></returns>
        public OrderActivationDetailsBusinessModel GetOrderActivationDetails(OrderActivationDetailsBusinessModel objCommunityOrderDetails)
        {
            IPatientRepository patientRepository = null;
            INotesRepository notesRepository = null;
            ICustomerParameterRepository customerRepository = null;
            string userId = "";
            if (objCommunityOrderDetails != null && !string.IsNullOrEmpty(objCommunityOrderDetails.UserID))
                userId = objCommunityOrderDetails.UserID;
            try
            {
                //long orderID = -1;
                //long carehomeID = 0;
                long patientID = 0;
                long customerID = 0;
                List<Note> notes = new List<Note>();
                if (objCommunityOrderDetails != null && !string.IsNullOrEmpty(objCommunityOrderDetails.PatientID))
                    patientID = Convert.ToInt64(objCommunityOrderDetails.PatientID, CultureInfo.InvariantCulture);

                patientRepository = new PatientRepository();

                var patient = patientRepository.Find(patientID);

                if (patient != null)
                    objCommunityOrderDetails.ADP = patient.ADP1;

                notesRepository = new NotesRepository();
                if (notes.Count > 0)
                {
                    objCommunityOrderDetails.NoteID = Convert.ToString(notes.LastOrDefault().NoteId, CultureInfo.InvariantCulture);
                    objCommunityOrderDetails.NoteText = notes.LastOrDefault().NoteText;
                }

                if (!string.IsNullOrEmpty(objCommunityOrderDetails.CustomerID))
                    customerID = Convert.ToInt64(objCommunityOrderDetails.CustomerID, CultureInfo.InvariantCulture);

                customerRepository = new CustomerParameterRepository();
                var customerParameter = customerRepository.GetAllCustomerParametersByCustomerId(customerID);

                if (customerParameter.ToList().Count > 0)
                    objCommunityOrderDetails.IsADPMandatory = customerParameter.FirstOrDefault().IsADPMandatory;

    }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (notesRepository != null)
                    notesRepository.Dispose();

                if (patientRepository != null)
                    patientRepository.Dispose();

                if (customerRepository != null)
                    customerRepository.Dispose();
            }
            return objCommunityOrderDetails;
        }

        /// <summary>
        /// Get Interaction sub type id
        /// </summary>
        /// <param name="orderCreationType"></param>
        /// <returns></returns>
        private long GetInteractionSubTypeId(long? orderCreationType)
        {
            long InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.Undefined, CultureInfo.InvariantCulture);;
            switch(orderCreationType){                
                case (long)SCAEnums.OrderCreationType.CommunityOrder:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.OrderActivation, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CommunityOneOffStandard:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.StandardOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CommunityOneOffRush:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.EmergencyOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CommunityOneOffFOC:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.EmergencyOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomePatientOrder:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.CareHomeOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.StandardOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomePatientOneOffRush:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.EmergencyOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomePatientOneOffFOC:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.EmergencyOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.Telephonic:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.AutoPhoneActivation, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.Online:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.OnlineActivation, CultureInfo.InvariantCulture);
                    break;                               
                case (long)SCAEnums.OrderCreationType.CarehomeOneOffStandard:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.StandardOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomeOneOffRush:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.EmergencyOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomeOneOffFOC:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.EmergencyOneOffOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.CarehomeProductOrder:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.CareHomeOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.SampleOrder:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.SampleOrder, CultureInfo.InvariantCulture);
                    break;
                case (long)SCAEnums.OrderCreationType.AutomaticOrder:
                    return InteractionsubType = Convert.ToInt64(SCAEnums.InteractionSubType.OrderActivation, CultureInfo.InvariantCulture);
                    break;
                default:
                    return InteractionsubType;
            }
        }


        /// <summary>
        /// Get Interaction type id
        /// </summary>
        /// <param name="orderCreationType"></param>
        /// <returns></returns>
        private long GetInteractionTypeId(long? orderCreationType)
        {
            long InteractionType = Convert.ToInt64(SCAEnums.InteractionType.Order, CultureInfo.InvariantCulture); ;
            switch (orderCreationType)
            {                              
                case (long)SCAEnums.OrderCreationType.CommunityOneOffRush:
                case (long)SCAEnums.OrderCreationType.CommunityOneOffFOC:
                case (long)SCAEnums.OrderCreationType.CarehomePatientOneOffRush:
                case (long)SCAEnums.OrderCreationType.CarehomePatientOneOffFOC:
                case (long)SCAEnums.OrderCreationType.CarehomeOneOffRush:
                case (long)SCAEnums.OrderCreationType.CarehomeOneOffFOC:
                    return InteractionType = Convert.ToInt64(SCAEnums.InteractionType.EmergencyOrder, CultureInfo.InvariantCulture);
                    break;                                 
                default:
                    return InteractionType;
            }
        }

        /// <summary>
        /// Is Holiday
        /// </summary>
        /// <param name="date"></param>
        /// <param name="countryId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        internal bool IsHoliday(DateTime date, long countryId, string userId)
        {
            var holidayList = new List<HolidayBusinessModel>();
            holidayList = (new CommonService()).GetHolidayList(userId, Convert.ToString(countryId, CultureInfo.InvariantCulture));
            if (holidayList.Where(q => q.HolidayDate == date).Any())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds the working days.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="noOfDays">The no of days.</param>
        /// <param name="countryId">Holiday as per country id</param>
        /// <returns></returns>
        internal DateTime AddWorkingDays(DateTime date, long noOfDays, long countryId, string userId)
        {
            var days = noOfDays;

            while (days != 0)
            {
                date = date.AddDays(1);
                days--;

                while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || IsHoliday(date, countryId, userId))
                    date = date.AddDays(1);
            }

            return date;
        }
	}
}
