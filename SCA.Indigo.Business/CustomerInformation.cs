﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : basahoo
// Created          : 07-04-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="CustomerInformation.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

/// <summary>
/// The Business namespace.
/// </summary>
namespace SCA.Indigo.Business
{
	using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data;
    using System.Globalization;
    using System.ServiceModel;

    using BusinessModels;
    using Interfaces;
    using Model;
    using Common;
    using Common.Enums;
    using Infrastructure.Repositories;
    

    /// <summary>
    /// Class CustomerInformation.
    /// </summary>    
    public class CustomerInformation : ICustomerInformation
    {
        /// <summary>
        /// Get Customer Information Details
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// List Customer Business Model.
        /// </returns>
        public List<CustomerBusinessModel> GetCustomerInformationDetails(string userId, string customerId)
        {
            ICustomerRepository customerRepository = null;
            var customerInfo = new List<CustomerBusinessModel>();
            IUsersRepository usersRepository = null;
            try
            {
                customerRepository = new CustomerRepository();
                var customer = customerRepository.Find(Convert.ToInt64(customerId, CultureInfo.InvariantCulture));
                if (customer != null && customer.PersonalInformation != null && customer.PersonalInformation.Address != null)
                {
                    var personalInfo = customer.PersonalInformation;
                    var address = personalInfo.Address;
                    usersRepository = new UsersRepository();
                    var user = usersRepository.All.FirstOrDefault(d => d.UserId == personalInfo.CreatedById);

	                var customerBusinessModel = new CustomerBusinessModel
	                {
		                CustomerId = customer.CustomerId,
		                SAPCustomerNumber = customer.SAPCustomerNumber.TrimStart('0'),
		                HouseNumber = address.HouseNumber,
		                AddressLine1 = address.AddressLine1,
		                AddressLine2 = address.AddressLine2,
		                TownCity = address.City,
		                County = address.County,
		                Country = address.Country,
		                PostCode = address.PostCode,
		                Phone = personalInfo.TelephoneNumber,
		                EmailAddress = personalInfo.Email,
		                TransportCompany = customer.Transport_Company,
		                StartDate =
			                Convert.ToDateTime(customer.CustomerStartDate, CultureInfo.InvariantCulture)
				                .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture),
		                Status = customer.Customer_Status.ToString(),
		                CustomerName = personalInfo.FirstName,
		                CreatedBy = user.UserName,
		                CreatedDate =
			                Convert.ToDateTime(personalInfo.CreatedDateTime, CultureInfo.InvariantCulture)
				                .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture),
		                PrescriptionApprovals = this.GetCustomerPrescriptionApprovals(userId, customerId)
	                };
                    
					/// Add Prescription Approval details
                    customerInfo.Add(customerBusinessModel);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }

                if(usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }
            return customerInfo;
        }

        /// <summary>
        /// Get Customer Information Details
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns>
        ///   <c>true</c> if Yes, <c>false</c> otherwise.
        /// </returns>
        public bool SaveCustomerInformationDetails(CustomerBusinessModel customerBusinessModel)
        {
            bool status = false;
            string userId = string.Empty;
            if (customerBusinessModel == null)
            {
                return false;
            }
            else
            {
                userId = customerBusinessModel.UserId;
            }
                
            ICustomerRepository customerRepository = new CustomerRepository();
            IUnitOfWork unitOfWork = customerRepository.UnitOfWork;

            try
            {
                long customerId = Convert.ToInt64(customerBusinessModel.CustomerId, CultureInfo.InvariantCulture);
                Customer customer = customerRepository.GetCustomerById(customerId);
                DateTime startDate = CommonHelper.ConvertDateToMMddyyyy(customerBusinessModel.StartDate);

                if (customer != null)
                {
                    /// Update the Email address, Phone, Transport, Status and StartDate
                    customer.Customer_Status = Convert.ToInt64(customerBusinessModel.Status, CultureInfo.InvariantCulture);
                    customer.CustomerStartDate = startDate; 
                    customer.ModifiedBy = Guid.Parse(userId);
                    customer.ModifiedDate = DateTime.Now;
                    customerRepository.InsertOrUpdate(customer);
                    unitOfWork.Commit();
                    status = true;
                }

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }
            return status;
        }

        /// <summary>
        /// Get Customer Prescription Approval Information Details
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// List Customer Prescription Approval BusinessModel
        /// </returns>
        public List<CustomerPrescriptionApprovalBusinessModel> GetCustomerPrescriptionApprovals(string userId, string customerId)
        {
            var prescriptionApprovals = new List<CustomerPrescriptionApprovalBusinessModel>();
            try
            {   
                var bindGridCustomerUserDetails = (new DBHelper()).GetCustomerUserDetails(Convert.ToInt64(customerId, CultureInfo.InvariantCulture)).ToList();
                if (bindGridCustomerUserDetails.Count > 0)
                {
                bindGridCustomerUserDetails.ForEach(d =>
                {
                    prescriptionApprovals.Add(new CustomerPrescriptionApprovalBusinessModel()
                        {
                            FirstName = d.FirstName,
                            SurName = d.LastName,
                            JobTitle = d.JobTitle,
                            UserName = d.UserName,
                            PhoneNumber = d.TelephoneNumber,
                            MobileNumber = d.MobileNumber,
                            EmailAddress = d.Email,
                            UserId = d.UserId.ToString(),
                            IDXAuthorisedUserPrescriptionID = d.IDXAuthorisedUserPrescriptionId,
                            AuthorizationPin = d.AuthorizationPIN.ToString()
                        });
                });
            }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
           
            return prescriptionApprovals;
        }

        /// <summary>
        /// Remove approved users
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns>
        ///   <c>true</c> if Yes, <c>false</c> otherwise.
        /// </returns>
        public bool RemoveUserPrescription(CustomerBusinessModel customerBusinessModel)
        {
            var status = false;
            IIDXAuthorisedUserPrescriptionRepository authorisedUsers = new IDXAuthorisedUserPrescriptionRepository();
            IUnitOfWork unitOfWork = authorisedUsers.UnitOfWork;
            var prescriptionIds = new List<string>();
            var userId = string.Empty;
            try
            {
                if (customerBusinessModel != null)
                {
                    userId = customerBusinessModel.UserId;
                    prescriptionIds = customerBusinessModel.PrescriptionIds;
                }

                prescriptionIds.ForEach(d =>
                {
                    authorisedUsers.Delete(Convert.ToInt64(d, CultureInfo.InvariantCulture));
                });
                
                unitOfWork.Commit();
                status = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (authorisedUsers != null)
                {
                    authorisedUsers.Dispose();
                }
            }
            return status;
        }

        /// <summary>
        /// Get Customer Active Patient
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns>
        ///   <c>true</c> if Yes, <c>false</c> otherwise.
        /// </returns>
        public bool CheckActivePatientsByCustomer(CustomerBusinessModel customerBusinessModel)
        {
            var userId = string.Empty;
            if (customerBusinessModel == null)
            {
                return false;
            }
                userId = customerBusinessModel.UserId;
	        var status = false;

            IPatientRepository patientRepository = new PatientRepository();
            try
            {
                long customerId = Convert.ToInt64(customerBusinessModel.CustomerId, CultureInfo.InvariantCulture);
                long? activePatientCode = Convert.ToInt64(SCAEnums.PatientStatus.Active, CultureInfo.InvariantCulture);

                var lstActivePatients = patientRepository.GetPatientsByCustomer(customerId).Where(d => d.PatientStatus == activePatientCode).ToList();

                if (lstActivePatients.Any())
                {
                    status = true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
            }
            return status;
        }

        /// <summary>
        /// Gets the user details by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="modifyBy">The modify by.</param>
        /// <returns>
        /// List&lt;CustomerPrescriptionApprovalBusinessModel&gt;.
        /// </returns>
        public List<CustomerPrescriptionApprovalBusinessModel> GetUserDetailsByUserId(string userId, string modifyBy)
        {
            var prescriptionApprovals = new List<CustomerPrescriptionApprovalBusinessModel>();
            IUsersRepository userRepository = new UsersRepository();
            try
            {
                var currentUser = userRepository.GetUsersByUserId(userId).FirstOrDefault();
	            if (currentUser != null)
	            {
		            var personalInfo = currentUser.PersonalInformation;

		            prescriptionApprovals.Add(new CustomerPrescriptionApprovalBusinessModel()
		            {
			            FirstName = personalInfo.FirstName,
			            SurName = personalInfo.LastName,
			            JobTitle = personalInfo.JobTitle,
			            UserName = currentUser.UserName,
			            PhoneNumber = personalInfo.TelephoneNumber,
			            MobileNumber = personalInfo.MobileNumber,
			            EmailAddress = personalInfo.Email,
			            UserId = currentUser.UserId.ToString(),
			            IDXAuthorisedUserPrescriptionID = 0,
			            AuthorizationPin = currentUser.AuthorizationPIN.ToString()
		            });
	            }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (userRepository != null)
                {
                    userRepository.Dispose();
                }
            }
            return prescriptionApprovals;
        }

        /// <summary>
        /// Save Authorised users for Prescription
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns>
        ///   <c>true</c> if Yes, <c>false</c> otherwise.
        /// </returns>
        public bool SaveAuthorisedUserPrescription(CustomerBusinessModel customerBusinessModel)
        {           
            if (customerBusinessModel == null)
            {
                return false;
            }
           
            var modifiedUserId = Guid.Parse(customerBusinessModel.UserId);
			IIDXAuthorisedUserPrescriptionRepository idxAuthorisedUserPrescriptionRepository = null;
            try
            {
                idxAuthorisedUserPrescriptionRepository  = new IDXAuthorisedUserPrescriptionRepository();
                IUnitOfWork unitOfWork = idxAuthorisedUserPrescriptionRepository.UnitOfWork;

				var customerId = customerBusinessModel.CustomerId;
                var idxPrescriptionUserList = idxAuthorisedUserPrescriptionRepository.All.Where(q => q.CustomerId == customerId).ToList();
                
                /// Update the Selected Entries.
                var invalidList = new List<IDXAuthorisedUserPrescription>();
                idxPrescriptionUserList.ForEach(d =>
                {
                    if (customerBusinessModel.PrescriptionApprovals.Find(q => Guid.Parse(q.UserId) == d.UserId) == null)
                    {
                        invalidList.Add(d);
                    }
                });

                /// Insert if not Exist
				 IDXAuthorisedUserPrescription idxAuthorisedUserPrescription;
                foreach (var users in customerBusinessModel.PrescriptionApprovals)
                {
                    idxAuthorisedUserPrescription = idxPrescriptionUserList.Find(d => d.UserId == Guid.Parse(users.UserId) && d.IsRemoved == false);
                    if (idxAuthorisedUserPrescription == null)
                    {
                        idxAuthorisedUserPrescription = new IDXAuthorisedUserPrescription()
                        {
                            IDXAuthorisedUserPrescriptionID = Convert.ToInt64(users.IDXAuthorisedUserPrescriptionID, CultureInfo.InvariantCulture),
                            CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture),
                            UserId = Guid.Parse(users.UserId),
                            ModifiedBy = modifiedUserId,
                            ModifiedDate = DateTime.Now,
                            IsRemoved = false
                        };
                        idxAuthorisedUserPrescriptionRepository.InsertOrUpdate(idxAuthorisedUserPrescription);
                    }
                }

                /// Update the Invalid List 
                invalidList.ForEach(d =>
                {
                    d.IsRemoved = true;
                    idxAuthorisedUserPrescriptionRepository.InsertOrUpdate(d);
                });

                unitOfWork.Commit();
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, modifiedUserId.ToString());
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, modifiedUserId.ToString());
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, modifiedUserId.ToString());
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, modifiedUserId.ToString());
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, modifiedUserId.ToString());
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, modifiedUserId.ToString());
                throw;
            }
            finally
            {
                if (idxAuthorisedUserPrescriptionRepository != null)
                {
                    idxAuthorisedUserPrescriptionRepository.Dispose();
                }
            }
            return false;            
        }

        /// <summary>
        /// Input : CustomerId, Info Id, UserId
        /// </summary>
        /// <param name="medicalStaffAnalysisInfoBusinessModel">Business Model</param>
        /// <returns>
        /// List of Parent Hierarchy Data
        /// </returns>
        public List<DropDownDataBusinessModel> GetHierarchyLinkData(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel)
        {
            var selectedHierarchyData = new List<DropDownDataBusinessModel>();
            IClinicalContactValueMappingRepository clinicalContactValueMappingRepository = null;

            ///Get User Id
            string userId = Convert.ToString(medicalStaffAnalysisInfoBusinessModel.UserId, CultureInfo.InvariantCulture);
            try
            {
                clinicalContactValueMappingRepository = new ClinicalContactValueMappingRepository();
                var customerIdValue = Convert.ToInt64(medicalStaffAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture);

                /// Get All Clinical Contact Linkage data as per Customer Id
                var allCustomerValueMapping = clinicalContactValueMappingRepository.GetAllClinicalContactValueMapping().Where(q => q.ClinicalContactRoleMapping.CustomerId == customerIdValue && q.ClinicalValueMaster.IsRemoved == false).ToList();

                /// Get the Role mapping details for Linkage Count
                int clinicalLinkageDataCount = new DBHelper().GetClinicalContactHierarchy(customerIdValue)
                                               .Where(d => d.HierarchyListId == medicalStaffAnalysisInfoBusinessModel.HierarchyListId).Count();

                /// Get the Paretnt Value Id for Slected least child
                long parentValueId = 0;

	            var parentItem =
		            allCustomerValueMapping.Find(
			            d => d.ChildValueId == medicalStaffAnalysisInfoBusinessModel.CustomerMedicalStaffAnalysisInfoId);
	            if (parentItem != null && parentItem.ParentValueId.HasValue)
                {
		            parentValueId = parentItem.ParentValueId.Value;
                }

                /// Get the Hirarchy Linkage Data
                var nextLinkItem = allCustomerValueMapping.ToList().Find(d => d.ChildValueId == parentValueId);

                var dataProcessed = 0;
                for (var i = 0; i < clinicalLinkageDataCount; i++)
                {
                    if (nextLinkItem != null && (dataProcessed == 0))
                    {
                        selectedHierarchyData.Add(new DropDownDataBusinessModel()
                        {
                            DisplayText = nextLinkItem.ClinicalValueMaster.IsRemoved == true ? string.Empty : nextLinkItem.ClinicalValueMaster.ContactName,
                            Value = nextLinkItem.ClinicalValueMaster.ClinicalValueMasterId
                        });
                        parentValueId = Convert.ToInt64(nextLinkItem.ParentValueId, CultureInfo.InvariantCulture);
                        if (parentValueId > 0)
                        {
                            nextLinkItem = allCustomerValueMapping.ToList().Find(d => d.ChildValueId == parentValueId);
                        }
                        else
                        {
                            dataProcessed = 1;
                        }
                    }
                }

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (clinicalContactValueMappingRepository != null)
                {
                    clinicalContactValueMappingRepository.Dispose();
                }
            }
            return selectedHierarchyData;
        }
    }
}
