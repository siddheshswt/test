﻿//----------------------------------------------------------------------------------------------
// <copyright file="AccountManagement.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using BusinessModels;
    using Interfaces;
    using Common;
    using Infrastructure.Repositories;
    using Model;
    using System.Configuration;
    using System.Collections.Generic;

    /// <summary>
    /// Account Management Business class
    /// </summary>   
    public class AccountManagement : IAccountManagement
    {
        /// <summary>
        /// ADP2 Length
        /// </summary>
        private static int allowedLoginAttempts = Convert.ToInt32(ConfigurationManager.AppSettings["AllowedLoginAttempts"], CultureInfo.InvariantCulture);

        /// <summary>
        /// change the password.
        /// Invoked from Web.UI through service
        /// </summary>
        /// <param name="userBusinessModel">The user business model.</param>
        /// <returns> boolean value </returns>
        public bool ChangePassword(UsersBusinessModel userBusinessModel)
        {
            bool isSuccess = false;
            IUsersRepository usersRepository = new UsersRepository();
            UnitOfWork unitOfWork = usersRepository.UnitOfWork;
            try
            {
                var users = usersRepository.GetUsersByUserId(Convert.ToString(userBusinessModel.UserId, CultureInfo.InvariantCulture));

                if (!users.Any())
                {
                    return false;
                }

                var user = users.FirstOrDefault();
                if (user != null)
                {
                    user.Password = userBusinessModel.Password;
                    user.ModifiedDate = DateTime.Now;
                    user.ModifiedBy = userBusinessModel.UserId;
                    user.IsNewUser = false;
                    var passwordHistories = user.PasswordHistories.Where(p => p.UserId == userBusinessModel.UserId).ToList();

                    if (passwordHistories.Any())
                    {
                        if (passwordHistories.Count >= 3)
                        {
                            passwordHistories.Remove(passwordHistories.OrderBy(p => p.Id).First());
                        }
                    }

                    var passwordHistory = new PasswordHistory
                    {
                        Password = userBusinessModel.Password,
                        UserId = userBusinessModel.UserId,
                        ModifiedBy = userBusinessModel.UserId,
                        ModifiedDate = DateTime.Now
                    };
                    passwordHistories.Add(passwordHistory);
                    user.PasswordHistories = passwordHistories;
                }

                usersRepository.InsertOrUpdate(user);
                unitOfWork.Commit();
                isSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
                throw;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userBusinessModel.UserId.ToString());
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }

            return isSuccess;
        }

        /// <summary>
        /// log user Login Details
        /// </summary>
        bool logLoginDetails = Convert.ToBoolean(ConfigurationManager.AppSettings["LogUserDetails"]);
        /// <summary>
        /// Fetch and validate the user login details
        /// </summary>
        /// <param name="userBusinessModel">The user business model.</param>
        /// <returns>Users Business Model.</returns>
        public UsersBusinessModel GetUserDetails(UsersBusinessModel userBusinessModel)
        {
            var userId = string.Empty;
            IUsersRepository usersRepository = new UsersRepository();

            try
            {
                if (userBusinessModel == null)
                {
                    return null;
                }

                IUnitOfWork unitOfWork = usersRepository.UnitOfWork;
                if (userBusinessModel.SSOAccess)
                {
                    return this.GetSsoUserDetails(userBusinessModel, usersRepository, unitOfWork);
                }

                var objUsersBusinessModel = new UsersBusinessModel();
                var objUsers = usersRepository.GetUsersByUserName(userBusinessModel.UserName);

                if (!objUsers.Any())
                {
                    return objUsersBusinessModel;
                }

                var objUser = objUsers.FirstOrDefault();

                userId = objUser.UserId.ToString();

                objUsersBusinessModel.IsAccountActive = true;
                objUsersBusinessModel.UserName = objUser.UserName;
                objUsersBusinessModel.IsCareHomeUser = objUser.IsCarehomeUser;
                objUsersBusinessModel.IsInternalUser = objUser.IsInternalUser;

                if (!objUser.IsActive)
                {
                    objUsersBusinessModel.IsAccountActive = false;
                }
                else if (objUser.Password != userBusinessModel.Password)
                {
                    objUser.NoOfAttempts = objUser.NoOfAttempts < allowedLoginAttempts ? objUser.NoOfAttempts + 1 : objUser.NoOfAttempts = allowedLoginAttempts;
                    if (objUser.NoOfAttempts >= allowedLoginAttempts)
                    {
                        objUsersBusinessModel.IsAccountLocked = true;
                        objUser.IsAccountLocked = true;
                    }
                }
                else
                {
                    var role = objUser.IDXUserRoles.Select(m => new UsersBusinessModel()
                                                    {
                                                        RoleName = m.Role.RoleName,
                                                        RoleId = m.RoleID,
                                                        UserType = m.Role.UserType
                                                    }).FirstOrDefault();

                    var passwordModifiedDate = new DateTime();
                    if (objUser.PasswordHistories.Any())
                    {
                        passwordModifiedDate = objUser.PasswordHistories.Select(m => m.ModifiedDate).LastOrDefault();
                    }

                    if (role != null)
                    {
                        objUsersBusinessModel = objUser.ToUsersBusinessModel(role.RoleId, role.RoleName, passwordModifiedDate, role.UserType);
                    }

                    if (objUser.Password == userBusinessModel.Password && objUser.IsAccountLocked == false &&
                        objUser.NoOfAttempts <= allowedLoginAttempts)
                    {
                        objUser.NoOfAttempts = 0;
                        LogUserLoginDetails(objUser, usersRepository);
                    }
                }

                usersRepository.InsertOrUpdate(objUser);
                unitOfWork.Commit();

                return objUsersBusinessModel;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Log User Login Details
        /// </summary>
        /// <param name="objUser">Users objUser</param>
        /// <param name="usersRepository">IUsersRepository usersRepository</param>
        private void LogUserLoginDetails(Users objUser, IUsersRepository usersRepository)
        {
            if (logLoginDetails)
            {
                var objLoginUserDetail = new LoginUserDetail();
                objLoginUserDetail.UserId = objUser.UserId;
                objLoginUserDetail.LoginDate = DateTime.Now;
                new DBHelper().LogUserLoginDetails(objLoginUserDetail);
            }
        }

        /// <summary>
        /// Get SSO User's Details
        /// </summary>
        /// <param name="userBusinessModel">User BusinessModel</param>
        /// <returns>User Business Model</returns>
        private UsersBusinessModel GetSsoUserDetails(UsersBusinessModel userBusinessModel, IUsersRepository usersRepository, IUnitOfWork unitOfWork)
        {
            var userId = string.Empty;

            try
            {
                if (userBusinessModel == null)
                {
                    return null;
                }

                var objUsersBusinessModel = new UsersBusinessModel();

                var objUsers = usersRepository.GetUsersByUserName(userBusinessModel.UserName);

                if (!objUsers.Any())
                {
                    return objUsersBusinessModel;
                }

                var objUser = objUsers.FirstOrDefault();
                if (objUser != null)
                {
                    userId = objUser.UserId.ToString();
                    var role =
                        objUser.IDXUserRoles.Select(
                            m => new UsersBusinessModel()
                            {
                                RoleName = m.Role.RoleName,
                                RoleId = m.RoleID,
                                UserType = m.Role.UserType
                            })
                        .FirstOrDefault();

                    var passwordModifiedDate = new DateTime();
                    if (role != null)
                    {
                        objUsersBusinessModel = objUser.ToUsersBusinessModel(role.RoleId, role.RoleName, passwordModifiedDate, role.UserType);
                    }
                }

                usersRepository.InsertOrUpdate(objUser);
                unitOfWork.Commit();

                return objUsersBusinessModel;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Validate the last 3 passwords.
        /// Invoked from Web.UI through service
        /// </summary>
        /// <param name="userBusinessModel">The user business model.</param>
        /// <returns> boolean value </returns>
        public bool ValidateNewPassword(UsersBusinessModel userBusinessModel)
        {
            IPasswordHistoryRepository passwordHistoryRepository = new PasswordHistoryRepository();
            try
            {
                var passwordHistories = passwordHistoryRepository.GetByUserId(userBusinessModel.UserId).ToList();
                if (passwordHistories.Any() && passwordHistories.Any(p => p.Password == userBusinessModel.Password))
                {
                    return false;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
                throw;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userBusinessModel.UserId.ToString());
                throw;
            }
            finally
            {
                if (passwordHistoryRepository != null)
                {
                    passwordHistoryRepository.Dispose();
                }
            }

            return true;
        }

        /// <summary>
        /// Reset the password.
        /// Invoked from Web.UI through service
        /// </summary>
        /// <param name="userBusinessModel">The user business model.</param>
        /// <returns> boolean value </returns>
        public bool ResetPassword(UsersBusinessModel userBusinessModel)
        {
            bool isSuccess = false;
            IUsersRepository usersRepository = new UsersRepository();
            UnitOfWork unitOfWork = usersRepository.UnitOfWork;
            try
            {
                var users = usersRepository.GetUsersByUserId(Convert.ToString(userBusinessModel.UserId, CultureInfo.InvariantCulture));
                if (!users.Any())
                {
                    return false;
                }
                var user = users.FirstOrDefault();
                if (user != null)
                {
                    user.Password = userBusinessModel.Password;
                    user.ModifiedDate = DateTime.Now;
                    user.ModifiedBy = userBusinessModel.UserId;
                    user.IsNewUser = false;

                    var passwordHistories = user.PasswordHistories.Where(p => p.UserId == userBusinessModel.UserId).ToList();
                    if (passwordHistories.Any())
                    {
                        if (passwordHistories.Count >= 3)
                        {
                            passwordHistories.Remove(passwordHistories.OrderBy(p => p.Id).First());
                        }
                    }
                    var passwordHistory = new PasswordHistory
                    {
                        Password = userBusinessModel.Password,
                        UserId = userBusinessModel.UserId,
                        ModifiedBy = userBusinessModel.UserId,
                        ModifiedDate = DateTime.Now
                    };
                    passwordHistories.Add(passwordHistory);
                    user.PasswordHistories = passwordHistories;
                }
                usersRepository.InsertOrUpdate(user);
                unitOfWork.Commit();
                isSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userBusinessModel.UserId.ToString());
                throw;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userBusinessModel.UserId.ToString());
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }
            return isSuccess;
        }

        /// <summary>
        /// Get Users For Reset-Unlock
        /// </summary>
        /// <param name="userResetUnlockDetails">Instance Of UserResetUnlockDetailsBusinessModel</param>
        /// <returns>List Of UserResetUnlockSummaryBusinessModel</returns>
        public List<UserResetUnlockSummaryBusinessModel> GetUserResetUnlockSummary(UserResetUnlockDetailsBusinessModel userResetUnlockDetails)
        {
            string userId = "";
            if (userResetUnlockDetails != null)
            {
                try
                {
                    userId = userResetUnlockDetails.UserId;

                    var objUserResetUnlock = (new DBHelper()).GetUserResetUnlockSummary(
                                                       userResetUnlockDetails.RowNumber,
                                                       userResetUnlockDetails.TotalRecords,
                                                       userResetUnlockDetails.searchText,
                                                       userResetUnlockDetails.SortColumn,
                                                       userResetUnlockDetails.SortBy
                                                       );
                    if (objUserResetUnlock.Any())
                    {
                        var interactionReport = objUserResetUnlock.Select(q => new UserResetUnlockSummaryBusinessModel
                        {
                            RowNumber = q.RowNumber.Value,
                            TotalRecords = q.TotalRecords.Value,
                            UserId = q.UserId,
                            UserName = q.UserName,
                            Name = q.Name,
                            EmailId = q.EmailId,
                            PinCode = q.PinCode,
                            IsAccountLocked = q.IsAccountLocked,
                            Locked = q.Locked,
                            Reset = q.Reset,
                        }).ToList();
                        return interactionReport;
                    }
                    return new List<UserResetUnlockSummaryBusinessModel>();
                }
                catch (DataException dataException)
                {
                    LogHelper.LogException(dataException, userId);
                }
                catch (ArgumentException argumentException)
                {
                    LogHelper.LogException(argumentException, userId);
                }
                catch (FormatException formatException)
                {
                    LogHelper.LogException(formatException, userId);
                }
                catch (InvalidOperationException invalidOperationException)
                {
                    LogHelper.LogException(invalidOperationException, userId);
                }
                catch (SystemException systemException)
                {
                    LogHelper.LogException(systemException, userId);
                    throw;
                }
                catch (Exception exception)
                {
                    LogHelper.LogException(exception, userId);
                    throw;
                }
            }
            return new List<UserResetUnlockSummaryBusinessModel>();
        }

        /// <summary>
        /// LockUnlcokUser
        /// </summary>
        /// <param name="objLockUnlockUser"></param>
        /// <returns></returns>
        public bool LockUnlcokUser(LockUnlockBusinessModel objLockUnlockUser)
        {
            bool isSuccess = false;
            IUsersRepository usersRepository = new UsersRepository();
            UnitOfWork unitOfWork = usersRepository.UnitOfWork;

            string userID = objLockUnlockUser.userId;

            try
            {
                var users = usersRepository.GetUsersByUserId(objLockUnlockUser.LockUnlockUserId);


                if (!users.Any())
                {
                    return isSuccess;
                }

                var user = users.FirstOrDefault();
                if (user != null)
                {
                    user.IsAccountLocked = !objLockUnlockUser.isAccountLocked;
                    user.NoOfAttempts = 0;
                    user.ModifiedDate = DateTime.Now;
                    user.ModifiedBy = Guid.Parse(objLockUnlockUser.userId);
                }

                usersRepository.InsertOrUpdate(user);
                unitOfWork.Commit();
                isSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userID);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userID);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userID);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userID);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userID);
                throw;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userID);
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }

            return isSuccess;
        }

        /// <summary>
        /// Capture Last access time of Home Page for User
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool TrackHomePageVisit(string userId)
        {
            bool isSuccess = false;

            IUserActionMonitorRepository usersRepository = new UserActionMonitorRepository();
            UnitOfWork unitOfWork = usersRepository.UnitOfWork;

            try
            {
                var user = usersRepository.FindByUserId(userId);

                if (user == null)
                {
                    user = new Model.UserActionMonitor();
                    user.UserId = Guid.Parse(userId);
                }

                user.LastAccessHome = DateTime.Now;
                usersRepository.InsertOrUpdate(user);
                unitOfWork.Commit();
                isSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userId);
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }
            return isSuccess;
        }

        /// <summary>
        /// Check For Interaction Assigned For Notification
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<NotificationBusinessModel> CheckForNotification(string userId)
        {
            try
            {
                var objNotification = (new DBHelper()).CheckForNotification(userId);

                if (objNotification.Any())
                {
                    var notifications = objNotification.Select(q => new NotificationBusinessModel
                    {
                        NotificationType = q.NotificationType,
                        Description = q.Description,
                        NoOfInteraction = objNotification.Count(w => w.NotificationType == "I")
                    }).ToList();
                    return notifications;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userId);
                throw;
            }
            return new List<NotificationBusinessModel>();
        }
    }
}