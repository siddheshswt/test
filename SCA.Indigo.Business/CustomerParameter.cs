﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Mamatha Shetty
// Created          : 02-04-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="CustomerParameter.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Business
{
	using System;
	using System.Collections.Generic;
    using System.Linq;
	using System.Data;
	using System.Globalization;
	using System.ServiceModel;

    using BusinessModels;
    using Interfaces;
    using Model;
    using Common;
    using Common.Enums;
    using Infrastructure.Repositories;
    using EFModel = Model;
    

    /// <summary>
    /// Customer Parameter Class
    /// </summary>    
    public class CustomerParameter : ICustomerParameter
    {
        /// <summary>
        /// Sets Time Format
        /// </summary>
        private readonly string timeFormat = "HH:mm:ss";

        #region CustomerParameter

        /// <summary>
        /// Save Customer Parameter
        /// </summary>
        /// <param name="customerParameter"></param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SaveCustomerParameter(CustomerParameterBusinessModel customerParameter)
        {
            if (customerParameter == null)
            {
                return false;
            }
            var userId = customerParameter.UserID;
            var modifiedDate = DateTime.Now;
            ICustomerRepository customerRepository = null;
            try
            {
                customerRepository = new CustomerRepository();
                EFModel.Customer customer = new Customer();
                EFModel.CustomerParameter customerParameterObj = new EFModel.CustomerParameter();
                if (customerParameter.CustomerID != 0)
                {
                    customer = customerRepository.GetCustomerById(customerParameter.CustomerID);
                    if (customer.CustomerParameters != null && customer.CustomerParameters.Any())
                    {
                        customerParameterObj = customer.CustomerParameters.First();
                    }
                }

                customerParameterObj.AllowIncreaseOrderQty = customerParameter.AllowIncreaseOrderQty;
                customerParameterObj.CheckAge = customerParameter.CheckAge;
                customerParameterObj.Age = customerParameter.CheckAge ? customerParameter.Age : null;
                customerParameterObj.IsADPMandatory = customerParameter.IsADPMandatory;
                customerParameterObj.IsCustomerApprovalReq = customerParameter.IsCustomerApprovalReq.HasValue && Convert.ToBoolean(customerParameter.IsCustomerApprovalReq.Value, CultureInfo.InvariantCulture);
                customerParameterObj.IsNDDAllow = customerParameter.IsNDDAllow;
                customerParameterObj.IsNextAssessmentDateMandatory = customerParameter.IsNextAssessmentDateMandatory;
                customerParameterObj.IsNHSIDMandatory = customerParameter.IsNHSIDMandatory;
                customerParameterObj.IsOrderAllowed = customerParameter.IsOrderAllowed;
                customerParameterObj.IsTelephoneMandatory = customerParameter.IsTelephoneMandatory;
                customerParameterObj.MaxPadPerDay = (customerParameterObj.MaxPadPerDay == customerParameter.MaxPadPerDay) ? customerParameterObj.MaxPadPerDay : customerParameter.MaxPadPerDay;
                customerParameterObj.NextAssessmentDate = customerParameter.NextAssessmentDate;
                customerParameterObj.OrderApproval = customerParameter.OrderApproval;
                customerParameterObj.OrderCreationAllowed = customerParameter.OrderCreationAllowed;
                customerParameterObj.OrderCutOffTime = TimeSpan.Parse(customerParameter.OrderCutOffTime, CultureInfo.InvariantCulture);
                customerParameterObj.OrderLeadTime = customerParameter.OrderLeadTime;
                customerParameterObj.IsCollectionApplicable = customerParameter.IsCollectionApplicable;
                customerParameterObj.IsClinicalContactWithLinkage = customerParameter.IsClinicalContactWithLinkage;
                customerParameterObj.ModifiedBy = Guid.Parse(userId);
                customerParameterObj.ModifiedDate = modifiedDate;
                customerParameterObj.SampleOrderProductType = customerParameter.SampleOrderProductType;

                customer.CustomerParameters.Clear();
                customer.CustomerParameters.Add(customerParameterObj);

                /// If Clinical Contact Linkage checked, Only Analysis info can be updated into the Table
                if (customerParameter.IsClinicalContactWithLinkage)
                {
                    customerParameter.MedicalAnalysisInformation = customerParameter.MedicalAnalysisInformation.FindAll(d => d.ListTypeId == (long)SCAEnums.ListType.AnalysisInformation);
                }

                /// Check for Add Edit new Clinical Contact or Analysis Information
                customerParameter.MedicalAnalysisInformation = UpdateClinicalContactAnalysis(customerParameter.MedicalAnalysisInformation, userId);

                ///Add Medical Analysis information
                foreach (var medicalAnalysis in customerParameter.MedicalAnalysisInformation)
                {
                    var idxMedicalAnalysisObj = new IDXCustomerMedicalStaffAnalysisInfo();
                    if (customer.IDXCustomerMedicalStaffAnalysisInfoes.Any(q => q.MedicalAnalysisListID == medicalAnalysis.ListId && q.IsFieldType == true))
                    {
                        idxMedicalAnalysisObj = customer.IDXCustomerMedicalStaffAnalysisInfoes.First(q => q.MedicalAnalysisListID == medicalAnalysis.ListId && q.IsFieldType == true);
                    }
                    else
                    {
                        idxMedicalAnalysisObj.MedicalAnalysisListID = medicalAnalysis.ListId;
                        idxMedicalAnalysisObj.IsFieldType = true;
                        customer.IDXCustomerMedicalStaffAnalysisInfoes.Add(idxMedicalAnalysisObj);
                    }

                    /// Save the IsMandatory
                    if (idxMedicalAnalysisObj.IsMandatory != medicalAnalysis.IsMandatory)
                    {
                        idxMedicalAnalysisObj.IsMandatory = medicalAnalysis.IsMandatory;
                    }

                    /// End

                    var isRemoved = !(medicalAnalysis.IsActive.HasValue ? medicalAnalysis.IsActive.Value : false);
                    if (idxMedicalAnalysisObj.IDXCustomerMedicalStaffAnalysisInfoID == 0 ||
                        (idxMedicalAnalysisObj.IDXCustomerMedicalStaffAnalysisInfoID != 0 && idxMedicalAnalysisObj.IsRemoved != isRemoved))
                    {
                        idxMedicalAnalysisObj.IsRemoved = medicalAnalysis.IsActive.HasValue ? !medicalAnalysis.IsActive.Value : true;
                        idxMedicalAnalysisObj.ModifiedBy = Guid.Parse(userId);
                        idxMedicalAnalysisObj.ModifiedDate = modifiedDate;
                    }
                }

                ///Add reason code mapping
                foreach (var reasonCode in customerParameter.RemovePatientReasonCodes)
                {
                    var idxReasonCode = new IDXCustomerReasonCodes();
                    if (customer.IDXCustomerReasonCodes.Any(q => q.ListID == reasonCode.ListId))
                    {
                        idxReasonCode = customer.IDXCustomerReasonCodes.First(q => q.ListID == reasonCode.ListId);
                    }
                    else
                    {
                        idxReasonCode.ListID = reasonCode.ListId;
                        customer.IDXCustomerReasonCodes.Add(idxReasonCode);
                    }

                    var isRemoved = !(reasonCode.IsActive.HasValue ? reasonCode.IsActive.Value : false);
                    if (idxReasonCode.IDXCustomerReasonCodeId == 0 ||
                        (idxReasonCode.IDXCustomerReasonCodeId != 0 && idxReasonCode.IsRemoved != isRemoved))
                    {
                        idxReasonCode.IsRemoved = reasonCode.IsActive.HasValue ? !reasonCode.IsActive.Value : true;
                        idxReasonCode.ModifiedBy = Guid.Parse(userId);
                        idxReasonCode.ModifiedDate = modifiedDate;
                    }
                }

                foreach (var reasonCode in customerParameter.StopPatientReasonCodes)
                {
                    var idxReasonCode = new IDXCustomerReasonCodes();
                    if (customer.IDXCustomerReasonCodes.Any(q => q.ListID == reasonCode.ListId))
                    {
                        idxReasonCode = customer.IDXCustomerReasonCodes.First(q => q.ListID == reasonCode.ListId);
                    }
                    else
                    {
                        idxReasonCode.ListID = reasonCode.ListId;
                        customer.IDXCustomerReasonCodes.Add(idxReasonCode);
                    }

                    var isRemoved = !(reasonCode.IsActive.HasValue ? reasonCode.IsActive.Value : false);
                    if (idxReasonCode.IDXCustomerReasonCodeId == 0 ||
                        (idxReasonCode.IDXCustomerReasonCodeId != 0 && idxReasonCode.IsRemoved != isRemoved))
                    {
                        idxReasonCode.IsRemoved = reasonCode.IsActive.HasValue ? !reasonCode.IsActive.Value : true;
                        idxReasonCode.ModifiedBy = Guid.Parse(userId);
                        idxReasonCode.ModifiedDate = modifiedDate;
                    }
                }

                customer.ModifiedBy = Guid.Parse(userId);
                customer.ModifiedDate = modifiedDate;

                customerRepository.InsertOrUpdate(customer);
                IUnitOfWork unitOfWork = customerRepository.UnitOfWork;
                unitOfWork.Commit();

                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }
            return false;
        }

        /// <summary>
        /// Retruns the removed patients reason Code for customer
        /// </summary>
        /// <param name="customerId">customerId of ListType Table</param>
        /// <param name="languageId">LanguageId from TransLang table</param>
        /// <returns>
        /// return the list ListTypeBusinessModel
        /// </returns>
        public List<ListTypeBusinessModel> GetRemovedPatientReasonCode(string customerId, string languageId)
        {
            var userId = string.Empty;
            IIDXCustomerReasonCodeRepository reasonCodeRepository = null;
            try
            {
                var listTypeId = Convert.ToString((long)SCAEnums.ListType.RemovedPatient, CultureInfo.InvariantCulture);
                var listsResult = (new DBHelper()).GetListDetails(listTypeId, Convert.ToInt32(languageId, CultureInfo.InvariantCulture));
                reasonCodeRepository = new IDXCustomerReasonCodeRepository();
                var reasonCodes = reasonCodeRepository.GetReasonCodesByCustomerId(Convert.ToInt64(customerId, CultureInfo.InvariantCulture));
                var responseData = new List<ListTypeBusinessModel>();
                foreach (var code in reasonCodes)
                {
                    var listItem = listsResult.FirstOrDefault(q => q.ListId == code.ListID);
                    if (listItem != null)
                    {
                        responseData.Add(new ListTypeBusinessModel()
                        {
                            ListId = listItem.ListId,
                            ListIndex = listItem.ListIndex,
                            ListTypeId = listItem.ListTypeId,
                            DisplayText = listItem.DisplayText
                        });
                    }
                }

                return responseData;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (reasonCodeRepository != null)
                {
                    reasonCodeRepository.Dispose();
                }
            }
            return null;
        }

        /// <summary>
        /// Retruns the stopped patients reason Code for customer
        /// </summary>
        /// <param name="customerId">customerId of ListType Table</param>
        /// <param name="languageId">LanguageId from TransLang table</param>
        /// <returns>
        /// return the list ListTypeBusinessModel
        /// </returns>
        public List<ListTypeBusinessModel> GetStoppedPatientReasonCode(string customerId, string languageId)
        {
            var userId = string.Empty;
            IIDXCustomerReasonCodeRepository reasonCodeRepository = null;
            try
            {
                var listTypeId = Convert.ToString((long)SCAEnums.ListType.StoppedPatient, CultureInfo.InvariantCulture);
                var listsResult = (new DBHelper()).GetListDetails(listTypeId, Convert.ToInt32(languageId, CultureInfo.InvariantCulture));
                reasonCodeRepository = new IDXCustomerReasonCodeRepository();
                var reasonCodes = reasonCodeRepository.GetReasonCodesByCustomerId(Convert.ToInt64(customerId, CultureInfo.InvariantCulture));
                var responseData = new List<ListTypeBusinessModel>();
                foreach (var code in reasonCodes)
                {
                    var listItem = listsResult.FirstOrDefault(q => q.ListId == code.ListID);
                    if (listItem != null)
                    {
                        responseData.Add(new ListTypeBusinessModel()
                        {
                            ListId = listItem.ListId,
                            ListIndex = listItem.ListIndex,
                            ListTypeId = listItem.ListTypeId,
                            DisplayText = listItem.DisplayText
                        });
                    }
                }

                return responseData;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (reasonCodeRepository != null)
                {
                    reasonCodeRepository.Dispose();
                }
            }
            return null;
        }

        /// <summary>
        /// check whether the reasonCode master data mapping can be removed for the customer
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="reasonCodeId">reasonCodeId</param>
        /// <param name="userId"></param>
        /// <returns>
        /// return true/false, stating whether the reasoncode mapping can be removed or not
        /// </returns>
        public bool RemoveReasonCodeMapping(string customerId, string reasonCodeId, string userId)
        {
            IPatientRepository patientRepository = null;
            try
            {
                patientRepository = new PatientRepository();
                var isMappingFound = patientRepository.IsReasonCodeMappedToPatient(Convert.ToInt64(customerId, CultureInfo.InvariantCulture), Convert.ToInt64(reasonCodeId, CultureInfo.InvariantCulture));

                ///if patient mapping is found, then cannot remove the reason code
                return !isMappingFound;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
            }
            return false;
        }

        /// <summary>
        /// Load Customer Parameter
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <returns>
        /// list of types
        /// </returns>
        public CustomerParameterBusinessModel GetCustomerParameter(string customerId, string userId)
        {
            ICustomerRepository customerRepository = null;
            try
            {
                customerRepository = new CustomerRepository();
                CustomerParameterBusinessModel customerParameterObj = new CustomerParameterBusinessModel();
                Customer customer = customerRepository.GetCustomerById(Convert.ToInt64(customerId, CultureInfo.InvariantCulture));
                if (customer != null)
                {
                    customerParameterObj.CustomerName = customer.PersonalInformation.FirstName;
                    customerParameterObj.SAPCustomerID = CommonHelper.TrimLeadingZeros(customer.SAPCustomerNumber);
                    customerParameterObj.CustomerID = customer.CustomerId;
                    if (customer.CustomerParameters.Any())
                    {
                        var objCustomerParameter = customer.CustomerParameters.First();
                        customerParameterObj.OrderLeadTime = objCustomerParameter.OrderLeadTime;
                        customerParameterObj.OrderCreationAllowed = objCustomerParameter.OrderCreationAllowed;
                        customerParameterObj.OrderCutOffTime = objCustomerParameter.OrderCutOffTime.HasValue ? new DateTime(objCustomerParameter.OrderCutOffTime.Value.Ticks).ToString(this.timeFormat, CultureInfo.InvariantCulture) : default(TimeSpan).ToString(this.timeFormat, CultureInfo.InvariantCulture);
                        customerParameterObj.CheckAge = objCustomerParameter.CheckAge;
                        customerParameterObj.Age = objCustomerParameter.Age;
                        customerParameterObj.MaxPadPerDay = objCustomerParameter.MaxPadPerDay.Value;
                        customerParameterObj.NextAssessmentDate = objCustomerParameter.NextAssessmentDate;
                        customerParameterObj.IsNDDAllow = objCustomerParameter.IsNDDAllow;
                        customerParameterObj.AllowIncreaseOrderQty = objCustomerParameter.AllowIncreaseOrderQty;
                        customerParameterObj.IsCollectionApplicable = objCustomerParameter.IsCollectionApplicable;

                        ///Mandatory fields
                        customerParameterObj.IsADPMandatory = objCustomerParameter.IsADPMandatory;
                        customerParameterObj.IsNHSIDMandatory = objCustomerParameter.IsNHSIDMandatory;
                        customerParameterObj.IsTelephoneMandatory = objCustomerParameter.IsTelephoneMandatory;
                        customerParameterObj.IsNextAssessmentDateMandatory = objCustomerParameter.IsNextAssessmentDateMandatory;

                        /// Clinical Contact Linkage
                        customerParameterObj.IsClinicalContactWithLinkage = objCustomerParameter.IsClinicalContactWithLinkage != true ? false : true;

                        /// Sample Order Product Type
                        customerParameterObj.SampleOrderProductType = objCustomerParameter.SampleOrderProductType;
                    }

                    ///Reason Codes
                    var masterList = GetReasonCodesMasterList();

                    ///Clinical Contacts and Analysis Info
                    var medicalStaffAnalysisList = GetClinicalContactsMasterList(customer);

                    ///patient Removal Reason Codes
                    var removedPatientReasonCodes = masterList.Where(a => a.ListTypeId == (long)SCAEnums.ListType.RemovedPatient).Select(q => new ListTypeAdvancedBusinessModel
                    {
                        DisplayText = q.DisplayText,
                        ListId = q.ListId,
                        ListTypeId = q.ListTypeId,
                        ListIndex = q.ListIndex,
                        IsActive = customer.IDXCustomerReasonCodes.Any(m => m.ListID == q.ListId && m.IsRemoved != true)
                    }).ToList();

                    ///patient Stopped Reason Codes
                    var stoppedPatientReasonCodes = masterList.Where(a => a.ListTypeId == (long)SCAEnums.ListType.StoppedPatient).Select(q => new ListTypeAdvancedBusinessModel
                    {
                        DisplayText = q.DisplayText,
                        ListId = q.ListId,
                        ListTypeId = q.ListTypeId,
                        ListIndex = q.ListIndex,
                        IsActive = customer.IDXCustomerReasonCodes.Any(m => m.ListID == q.ListId && m.IsRemoved != true)
                    }).ToList();

                    customerParameterObj.MedicalAnalysisInformation = medicalStaffAnalysisList.OrderBy(d => d.DisplayText).ToList();
                    customerParameterObj.RemovePatientReasonCodes = removedPatientReasonCodes;
                    customerParameterObj.StopPatientReasonCodes = stoppedPatientReasonCodes;
                }
                return customerParameterObj;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }
            return null;
        }

        private static List<ListTypeAdvancedBusinessModel> GetClinicalContactsMasterList(Customer customer)
        {
            /// Ger Clinical Contact Analysis Master data
            var clinicalContactMasterData = new DBHelper().GetClinicalContactAnalysisMaster(Convert.ToString((long)SCAEnums.ListType.MedicalStaff, CultureInfo.InvariantCulture) + "," + Convert.ToString((long)SCAEnums.ListType.AnalysisInformation, CultureInfo.InvariantCulture), (int)SCAEnums.OtherConstants.LanguageId);

            var medicalStaffAnalysisList = clinicalContactMasterData.Select(q => new ListTypeAdvancedBusinessModel
            {
                DisplayText = q.ContactName,
                ListId = q.clinicalcontactanalysismasterid,
                ListTypeId = q.TypeId,
                ListIndex = Convert.ToInt32(q.Index, CultureInfo.InvariantCulture),
                IsActive = customer.IDXCustomerMedicalStaffAnalysisInfoes.Any(m => m.MedicalAnalysisListID == q.clinicalcontactanalysismasterid && m.IsFieldType == true && m.IsRemoved != true),
                IsMandatory = customer.IDXCustomerMedicalStaffAnalysisInfoes.Any(m => m.MedicalAnalysisListID == q.clinicalcontactanalysismasterid && m.IsFieldType == true && m.IsMandatory == true),
                IsFieldType = customer.IDXCustomerMedicalStaffAnalysisInfoes.Any(m => m.MedicalAnalysisListID == q.clinicalcontactanalysismasterid && m.IsFieldType == true)

            }).ToList();
            return medicalStaffAnalysisList;
        }

        private static List<GetListDetails_usp_Result> GetReasonCodesMasterList()
        {
            ///Medical Staff Analysis information
            var listTypeId = Convert.ToString((long)SCAEnums.ListType.RemovedPatient, CultureInfo.InvariantCulture) + ","
                            + Convert.ToString((long)SCAEnums.ListType.StoppedPatient, CultureInfo.InvariantCulture);
            var masterList = new DBHelper().GetListDetails(listTypeId, (int)SCAEnums.OtherConstants.LanguageId);
            return masterList;
        }
        #endregion

        #region CustomerPostcodeMatrix

        /// <summary>
        /// Get Customer's PostCode Matrix for Customer Parameter maintenance
        /// </summary>
        /// <param name="customerId">Customer's Id</param>
        /// <param name="userId">Logged in User's Id</param>
        /// <returns>
        /// List of Postcodes assigned to the customer
        /// </returns>
        public List<PostCodeMatrixBusinessModel> GetCustomerPostcodeMatrix(string customerId, string userId)
        {
            IPostCodeMatrixRepository objPostcodeMatrixRepository = null;
            try
            {
                var newCustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                objPostcodeMatrixRepository = new PostCodeMatrixRepository();
                var customerPostCodeMatrix = objPostcodeMatrixRepository.GetPostCodeByCustomerId(newCustomerId);

                customerPostCodeMatrix = customerPostCodeMatrix.Where(q => q.IsActive == true);
                var result = customerPostCodeMatrix.Select(q => new PostCodeMatrixBusinessModel
                 {
                     CustomerID = (long)q.CustomerID,
                     Round = string.IsNullOrEmpty(q.RoundID) ? string.Empty : q.Round,
                     RoundID = string.IsNullOrEmpty(q.RoundID) ? string.Empty : q.RoundID,
                     Postcode = string.IsNullOrEmpty(q.Postcode) ? string.Empty : q.Postcode,
                     Monday = q.Monday,
                     Tuesday = q.Tuesday,
                     Wednesday = q.Wednesday,
                     Thursday = q.Thursday,
                     Friday = q.Friday,
                     PostcodeMatrixID = q.PostcodeMatrixID,

                 }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (objPostcodeMatrixRepository != null)
                {
                    objPostcodeMatrixRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Save/Update the Customer's Postcode Matrix
        /// </summary>
        /// <param name="postcodeMatrixList">List of Postcode matrices with delivery days assigned to postcode</param>
        /// <returns>
        /// True if Save is successful else returns False
        /// </returns>
        public bool SaveCustomerPostcode(List<PostCodeMatrixBusinessModel> postcodeMatrixList)
        {
            IPostCodeMatrixRepository objPostcodeRepository = null;

            var userId = string.Empty;
            var isSaveSuccess = false;
            try
            {
                objPostcodeRepository = new PostCodeMatrixRepository();
                IUnitOfWork unitOfWork = objPostcodeRepository.UnitOfWork;
                userId = postcodeMatrixList.First().UserId;
                postcodeMatrixList = postcodeMatrixList.Where(q => q.Round != null && q.RoundID != null && q.Postcode != null).ToList();

                if (postcodeMatrixList.Any())
                {
                    var customerPostcodeMatrix = ConvertToPostCodeModel(postcodeMatrixList);

                    ///Get list of all existing postodes
                    var existingPostcodeMatrix = objPostcodeRepository.GetPostCodeByCustomerId(postcodeMatrixList.First().CustomerID).ToList();

                    foreach (var obj in customerPostcodeMatrix)
                    {
                        var objectToinsert = obj;
						var existingPostCode = existingPostcodeMatrix.
												  Where(q => ((q.Postcode == null ? "" : q.Postcode.ToUpper()) == obj.Postcode.ToUpper())
														  && q.Round.ToUpper() == obj.Round.ToUpper()
														  && q.RoundID.ToUpper() == obj.RoundID.ToUpper()
												  );
						if (existingPostCode.Any())
						{
							objectToinsert = existingPostCode.First();
                            objectToinsert = UpdateExistingPostcode(objectToinsert, obj);
                            objectToinsert.Customer.ModifiedDate = DateTime.Now;
                        }
                        objPostcodeRepository.InsertOrUpdate(objectToinsert);
                    }
                    unitOfWork.Commit();
                }
	            isSaveSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {

                if (objPostcodeRepository != null)
                {
                    objPostcodeRepository.Dispose();
                }
            }
            return isSaveSuccess;
        }

        /// <summary>
        /// Updates the existing postcode.
        /// </summary>
        /// <param name="existingPostcode">The existing postcode.</param>
        /// <param name="updatedPostcode">The updated postcode.</param>
        /// <returns></returns>
        private static PostcodeMatrix UpdateExistingPostcode(PostcodeMatrix existingPostcode, PostcodeMatrix updatedPostcode)
        {
            existingPostcode.Monday = updatedPostcode.Monday;
            existingPostcode.Tuesday = updatedPostcode.Tuesday;
            existingPostcode.Wednesday = updatedPostcode.Wednesday;
            existingPostcode.Thursday = updatedPostcode.Thursday;
            existingPostcode.Friday = updatedPostcode.Friday;
            existingPostcode.IsActive = updatedPostcode.IsActive;
            existingPostcode.ModifiedDate = updatedPostcode.ModifiedDate;

            return existingPostcode;
        }

        /// <summary>
        /// Converts to post code model.
        /// </summary>
        /// <param name="postCodeMatrixList">The post code matrix list.</param>
        /// <returns></returns>
        private static List<PostcodeMatrix> ConvertToPostCodeModel(List<PostCodeMatrixBusinessModel> postCodeMatrixList)
        {
            var postCodes = new List<PostcodeMatrix>();
            var modifiedDate = DateTime.Now;
            foreach (var postCode in postCodeMatrixList)
            {
	            var objPostCode = new PostcodeMatrix
	            {
		            CustomerID = postCode.CustomerID,
		            Monday = postCode.Monday,
		            Tuesday = postCode.Tuesday,
		            Wednesday = postCode.Wednesday,
		            Thursday = postCode.Thursday,
		            Friday = postCode.Friday,
		            Round = postCode.Round,
		            RoundID = postCode.RoundID,
		            Postcode = postCode.Postcode == "-1" ? string.Empty : postCode.Postcode,
		            IsActive = postCode.IsActive,
		            ModifiedBy = Guid.Parse(postCode.UserId),
		            ModifiedDate = modifiedDate
	            };

	            postCodes.Add(objPostCode);
            }
            return (postCodes.ToList());
        }

        #endregion

        #region Patient Type Matrix
        /// <summary>
        /// Get patient type matrix
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>
        /// list on values
        /// </returns>
        public List<PatientTypeMatrixBusinessModel> GetPatientTypeMatrixByCustomerId(string customerId)
        {
            var userId = string.Empty;
            IPatientTypeMatrixRepository patientMatrixRepository = null;
            try
            {
                patientMatrixRepository = new PatientTypeMatrixRepository();
                var patientTypeMatrix = patientMatrixRepository.GetPatientTypeMatrixByCustomerId(Convert.ToInt64(customerId, CultureInfo.InvariantCulture)).ToList();
                
                var lstPatientType = (new DBHelper()).GetTextFromTranslationByListTypeId((long)SCAEnums.ListType.PatientType, (long)SCAEnums.ListType.LanguageId).ToList();

                return patientTypeMatrix.Select(q => new PatientTypeMatrixBusinessModel
                {
                    ListIndex = q.List != null ? q.List.ListIndex : 0,
                    PrescMinFrequency = q.Presc_Min_Freq,
                    PrescMaxFrequency = q.Presc_Max_Freq,
                    IsAddFromPrescriptionAllowed = q.IsAddFromPrescriptionAllowed,
                    IsAddFromProductAllowed = q.IsAddFromProductAllowed,
                    AdvanceOrderActivationDays = q.AdvanceOrderActivationDays,
                    ChangeOrderNddDays = q.ChangeOrderNddDays,
                    IsOneOffAddFromPrescriptionAllowed = q.IsOneOffAddFromPrescriptionAllowed,
                    IsOneOffAddFromProductAllowed = q.IsOneOffAddFromProductAllowed,
                    IsPrescriptionApprovalRequired = q.IsPrescriptionApprovalRequired,
                    AdultBillTo = q.AdultBillTo,
                    PaedBillTo = q.PaedBillTo,
                    IncrementFrequency = q.IncrementFrequency,
                    PatientType = q.PatientType.ToString(),
                    IsRemove = (bool)q.IsRemove,
                    PatientTypeMatrixID = q.PatientTypeMatrixID,
                    DefaultFrequency=q.DefaultFrequency,
                    PatientTypeText = lstPatientType.Where(w => w.ListId == Convert.ToInt64(q.PatientType)).FirstOrDefault().TranslationType
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (patientMatrixRepository != null)
                {
                    patientMatrixRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Get master list by id
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="userId"></param>
        /// <returns>
        /// list of types
        /// </returns>
        public List<ListTypeBusinessModel> GetMasterListById(string listTypeId, string userId)
        {
            try
            {
                var masterList = new DBHelper().GetListDetails(listTypeId, (int)SCAEnums.OtherConstants.LanguageId);
                return masterList.Select(q => new ListTypeBusinessModel
                {
                    DisplayText = q.DisplayText,
                    ListId = q.ListId,
                    ListTypeId = q.ListTypeId,
                    ListIndex = q.ListIndex
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return null;
        }

        /// <summary>
        /// Save Patient Matrix Data
        /// </summary>
        /// <param name="patientTypeMatrixMainBusinessModel"></param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SavePatientTypeMatrixDetails(PatientTypeMatrixMainBusinessModel patientTypeMatrixMainBusinessModel)
        {
			ICustomerRepository customerRepository = null;           
            if (patientTypeMatrixMainBusinessModel == null)
            {
                return false;
            }

            var userId = patientTypeMatrixMainBusinessModel.UserId;
            try
            {
				customerRepository = new CustomerRepository();
                IUnitOfWork unitOfWork = customerRepository.UnitOfWork;
                var customer = customerRepository.GetCustomerById(patientTypeMatrixMainBusinessModel.CustomerId.Value);
                var _userId = Guid.Parse(userId);
                PatientTypeMatrix patientTypeMatrix = null;

                if (patientTypeMatrixMainBusinessModel.PatientTypeMatrixList != null)
                {
                    foreach (var patient in patientTypeMatrixMainBusinessModel.PatientTypeMatrixList)
                    {
                        if (patient.PatientTypeMatrixID > 0)
                        {
                            patientTypeMatrix = new PatientTypeMatrix();
                            patientTypeMatrix = customer.PatientTypeMatrices.Where(q => q.PatientTypeMatrixID == patient.PatientTypeMatrixID).First();
                            patientTypeMatrix.PatientType = Convert.ToInt64(patient.PatientTypeId, CultureInfo.InvariantCulture);
                            patientTypeMatrix.Presc_Min_Freq = patient.PrescMinFrequency;
                            patientTypeMatrix.Presc_Max_Freq = patient.PrescMaxFrequency;
                            patientTypeMatrix.IncrementFrequency = patient.IncrementFrequency;
                            patientTypeMatrix.AdultBillTo = patient.AdultBillTo;
                            patientTypeMatrix.PaedBillTo = patient.PaedBillTo;
                            patientTypeMatrix.AdvanceOrderActivationDays = patient.AdvanceOrderActivationDays;
                            patientTypeMatrix.ChangeOrderNddDays = patient.ChangeOrderNddDays;
                            patientTypeMatrix.IsAddFromPrescriptionAllowed = patient.IsAddFromPrescriptionAllowed;
                            patientTypeMatrix.IsAddFromProductAllowed = patient.IsAddFromProductAllowed;
                            patientTypeMatrix.IsOneOffAddFromPrescriptionAllowed = patient.IsOneOffAddFromPrescriptionAllowed;
                            patientTypeMatrix.IsOneOffAddFromProductAllowed = patient.IsOneOffAddFromProductAllowed;
                            patientTypeMatrix.IsPrescriptionApprovalRequired = patient.IsPrescriptionApprovalRequired;
                            patientTypeMatrix.IsRemove = patient.IsRemove;
                            patientTypeMatrix.ModifiedBy = _userId;
                            patientTypeMatrix.ModifiedDate = DateTime.Now;
                            patientTypeMatrix.DefaultFrequency = patient.DefaultFrequency;
                        }
                        else
                        {
                            /// Insert all details for the user.
                            patientTypeMatrix = new PatientTypeMatrix()
                            {
                                PatientTypeMatrixID = patient.PatientTypeMatrixID,
                                CustomerId = patientTypeMatrixMainBusinessModel.CustomerId,
                                PatientType = Convert.ToInt64(patient.PatientTypeId, CultureInfo.InvariantCulture),
                                Presc_Min_Freq = patient.PrescMinFrequency,
                                Presc_Max_Freq = patient.PrescMaxFrequency,
                                IncrementFrequency = patient.IncrementFrequency,
                                AdultBillTo = patient.AdultBillTo,
                                PaedBillTo = patient.PaedBillTo,
                                AdvanceOrderActivationDays = patient.AdvanceOrderActivationDays,
                                ChangeOrderNddDays = patient.ChangeOrderNddDays,
                                IsAddFromPrescriptionAllowed = patient.IsAddFromPrescriptionAllowed,
                                IsAddFromProductAllowed = patient.IsAddFromProductAllowed,
                                IsOneOffAddFromPrescriptionAllowed = patient.IsOneOffAddFromPrescriptionAllowed,
                                IsOneOffAddFromProductAllowed = patient.IsOneOffAddFromProductAllowed,
                                IsPrescriptionApprovalRequired = patient.IsPrescriptionApprovalRequired,
                                IsRemove = patient.IsRemove,
                                CreatedBy = _userId,
                                CreatedDate = DateTime.Now,
                                DefaultFrequency = patient.DefaultFrequency,
                            };
                            customer.PatientTypeMatrices.Add(patientTypeMatrix);
                        }
                    }

                    /// Update Modify date in the customer table for audit log
                    customer.ModifiedDate = DateTime.Now;
                    customerRepository.InsertOrUpdate(customer);
                    unitOfWork.Commit();
                }
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Get Patient Associated With Patient
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientType"></param>
        /// <param name="userId"></param>
        /// <returns>
        /// business model
        /// </returns>
        public List<PatientTypeMatrixBusinessModel> GetPatientAssociatedWithPatient(string customerId, string patientType, string userId)
        {
            IPatientRepository patientRepository = null;
            try
            {
                patientRepository = new PatientRepository();
                var customer = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                var patientTypeId = Convert.ToInt64(patientType, CultureInfo.InvariantCulture);
                var patient = patientRepository.All.Where(q => q.CustomerId == customer && q.PatientType == patientTypeId);
                return patient.Select(q => new PatientTypeMatrixBusinessModel
                {
                    PatientTypeId = q.PatientType
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
            }

            return null;
        }
        #endregion

        #region Clinical Contact Role Mapping

        /// <summary>
        /// Gets the clinical contact role mapping by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// List Clinical Business Model.
        /// </returns>
        public List<ClinicalContactMappingBusinessModel> GetClinicalContactRoleMappingByCustomerId(string customerId)
        {
            var userId = string.Empty;
            IClinicalContactRoleMappingRepository clinicalContactRoleMappingRepository = null;
            var clinicalContactRoleMappingList = new List<ClinicalContactMappingBusinessModel>();
			IListRepository listRepository = null;
            try
            {
                clinicalContactRoleMappingRepository = new ClinicalContactRoleMappingRepository();
                var lstCustomersClinicalContactRoleMapping = clinicalContactRoleMappingRepository.GetCustomersClinicalContactRoleMapping(Convert.ToInt64(customerId, CultureInfo.InvariantCulture)).ToList();

                /// Skip the Null OR 0 Parent Id
                lstCustomersClinicalContactRoleMapping = lstCustomersClinicalContactRoleMapping.FindAll(d => d.ParentListId != null && d.ParentListId > 0);

                lstCustomersClinicalContactRoleMapping.ForEach(d =>
                {
                    ///child
					listRepository = new ListRepository();
					var lstChild = listRepository.GetById((long)d.ChildListId).FirstOrDefault();

                    var translationChild = new Translation();
                    if (lstChild != null)
                    {
                        translationChild = lstChild.TranslationProxy.Translations.FirstOrDefault(q => q.TransProxyID == lstChild.TranslationProxy.TransProxyID
                        && q.LanguageID == (long)SCAEnums.OtherConstants.LanguageId);
                    }

                    ///parent
                    if (d.ParentListId == null)
                    {
                        d.ParentListId = 0;
                    }

					List lstParent = listRepository.GetById((long)d.ParentListId).FirstOrDefault();

                    Translation translationParent = new Translation();
                    if (lstParent != null)
                    {
                        translationParent = lstParent.TranslationProxy.Translations.FirstOrDefault(q => q.TransProxyID == lstParent.TranslationProxy.TransProxyID
                        && q.LanguageID == (long)SCAEnums.OtherConstants.LanguageId);
                    }

                    ///hierarchy
					List lstHierarchy = listRepository.GetById((long)d.HierarchyListId).FirstOrDefault();

                    Translation translationHierarchy = new Translation();
                    if (lstHierarchy != null)
                    {
                        translationHierarchy = lstHierarchy.TranslationProxy.Translations.FirstOrDefault(q => q.TransProxyID == lstHierarchy.TranslationProxy.TransProxyID
                        && q.LanguageID == (long)SCAEnums.OtherConstants.LanguageId);
                    }

                    clinicalContactRoleMappingList.Add(new ClinicalContactMappingBusinessModel()
                    {
                        ClinicalContactRoleMappingId = d.ClinicalContactRoleMappingId,
                        ChildListId = d.ChildListId,
                        Child = translationChild.TranslationType,
                        ParentListId = d.ParentListId,
                        Parent = translationParent.TranslationType,
                        Hierarchy = translationHierarchy.TranslationType,
                        HierarchyListId = d.HierarchyListId,
                        IsNewRow = true
                    });
                });
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (clinicalContactRoleMappingRepository != null)
                {
                    clinicalContactRoleMappingRepository.Dispose();
                }

				if(listRepository != null)
				{
					listRepository.Dispose();
				}
            }

            return clinicalContactRoleMappingList;
        }

        /// <summary>
        /// Save clinical Role Details
        /// </summary>
        /// <param name="objClinicalRoleMapping"></param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SaveClinicalRoleMapping(List<ClinicalContactMappingBusinessModel> objClinicalRoleMapping)
        {
            bool status = false;
            var userId = string.Empty;
            IClinicalContactRoleMappingRepository roleMappingRepository = new ClinicalContactRoleMappingRepository();
            IUnitOfWork unitOfWork = roleMappingRepository.UnitOfWork;
            try
            {
            if (objClinicalRoleMapping != null)
            {
                ClinicalContactRoleMapping clinicalContactRoleMapping = null;

                foreach (var item in objClinicalRoleMapping)
                {
                    if (item.ClinicalContactRoleMappingId == 0)
                    {
                        /// Check if the Same Child Id exist in Database.
                        ClinicalContactRoleMapping childExist = roleMappingRepository.GetCustomersClinicalContactRoleMapping((long)item.CustomerId).FirstOrDefault(q => q.ChildListId == item.ChildListId);

                            userId = item.UserId;
                        if (childExist != null)
                        {
                            /// If Record exist and Child has no parent. then Update that with assigned new parent.
                            childExist.CustomerId = item.CustomerId;
                            childExist.HierarchyListId = item.HierarchyListId;
                            childExist.ChildListId = item.ChildListId;
                            childExist.ParentListId = item.ParentListId;
                            childExist.ModifiedBy = Guid.Parse(item.UserId);
                            childExist.ModifiedDate = DateTime.Now;
                            roleMappingRepository.InsertOrUpdate(childExist);

                            /// Check for the Parent if not exist
                            if (item.IsParentExist == false && !roleMappingRepository.GetParent((long)item.ParentListId, (long)item.CustomerId).Any())
                            {
	                            clinicalContactRoleMapping = new ClinicalContactRoleMapping
	                            {
		                            ClinicalContactRoleMappingId = 0,
		                            CustomerId = item.CustomerId,
		                            HierarchyListId = item.HierarchyListId,
		                            ChildListId = item.ParentListId,
		                            ParentListId = 0,
		                            ModifiedBy = Guid.Parse(item.UserId),
		                            ModifiedDate = DateTime.Now
	                            };
	                            roleMappingRepository.InsertOrUpdate(clinicalContactRoleMapping);
                            }
                        }
                        else
                        {
                            /// Check if the Parent Not Exist
                            if (item.IsParentExist == false && !roleMappingRepository.GetParent((long)item.ParentListId, (long)item.CustomerId).Any())
                            {
	                            clinicalContactRoleMapping = new ClinicalContactRoleMapping
	                            {
		                            ClinicalContactRoleMappingId = 0,
		                            CustomerId = item.CustomerId,
		                            HierarchyListId = item.HierarchyListId,
		                            ChildListId = item.ParentListId,
		                            ParentListId = 0,
		                            ModifiedBy = Guid.Parse(item.UserId),
		                            ModifiedDate = DateTime.Now
	                            };
	                            roleMappingRepository.InsertOrUpdate(clinicalContactRoleMapping);
                            }

	                        clinicalContactRoleMapping = new ClinicalContactRoleMapping
	                        {
		                        ClinicalContactRoleMappingId = (long) item.ClinicalContactRoleMappingId,
		                        CustomerId = item.CustomerId,
		                        HierarchyListId = item.HierarchyListId,
		                        ChildListId = item.ChildListId,
		                        ParentListId = item.ParentListId,
		                        ModifiedBy = Guid.Parse(item.UserId),
		                        ModifiedDate = DateTime.Now
	                        };
	                        roleMappingRepository.InsertOrUpdate(clinicalContactRoleMapping);
                        }
                    }
                }
                unitOfWork.Commit();
                status = true;
            }
            }
            catch (DataException e)
            {
                status = false;
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                status = false;
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                status = false;
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                status = false;
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                status = false;
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                status = false;
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (roleMappingRepository != null)
                {
                    roleMappingRepository.Dispose();
                }
            }
            return status;
        }

        #endregion

        #region Clinical Contact Value Mapping

        /// <summary>
        /// Get Role Type For Clinical Contact
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="languageId"></param>
        /// <param name="userId"></param>
        /// <returns>
        /// list value
        /// </returns>
        public List<ListTypeBusinessModel> GetRoleTypeForClinicalContact(string customerId, string languageId, string userId)
        {
            try
            {
                var result = (new DBHelper()).GetRoleTypeForClinicalValueMapping(Convert.ToInt64(customerId, CultureInfo.InvariantCulture), Convert.ToInt32(languageId, CultureInfo.InvariantCulture));
                var response = result.ToRoleTypeForClinicalContactBusinessModel();
                return response;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Gets the clinical contact value mapping by customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="roleType">Type of the role.</param>
        /// <param name="userId"></param>
        /// <returns>
        /// List Clinical Business Model.
        /// </returns>
        public List<ClinicalContactValueMappingBusinessModel> GetClinicalContactValueMappingByCustomerId(string customerId, string roleType, string userId)
        {
            List<ClinicalContactValueMappingBusinessModel> clinicalContactValueMapping = new List<ClinicalContactValueMappingBusinessModel>();
            try
            {
                var getClinicalContactValueMapping = (new DBHelper()).GetClinicalContactsValueMapping(Convert.ToInt64(customerId, CultureInfo.InvariantCulture), Convert.ToInt64(roleType, CultureInfo.InvariantCulture));

                getClinicalContactValueMapping.ForEach(d =>
                {
                    var childId = d.ChildListId;
                    if (childId == null)
                    {
                        childId = 0;
                    }

                    var parentListId = d.ParentListId;
                    if (parentListId == null)
                    {
                        parentListId = 0;
                    }

                    var childName = GetRoleNameForClinicalContact(Convert.ToString(childId, CultureInfo.InvariantCulture), userId);
                    var parentName = GetRoleNameForClinicalContact(Convert.ToString(parentListId, CultureInfo.InvariantCulture), userId);

                    clinicalContactValueMapping.Add(new ClinicalContactValueMappingBusinessModel()
                    {
                        ClinicalContactRoleMappingId = (long)d.ClinicalContactRoleMappingId,
                        ClinicalContactValueMappingId = (long)d.ClinicalContactValueMappingId,
                        ChildValue = d.ChildValue,
                        ParentValue = d.ParentValue,
                        ChildListId = d.ChildListId,
                        HierarchyListId = (long)d.HierarchyListId,
                        IsRemoved = (bool)d.IsRemoved,
                        ChildRoleName = childName,
                        ParentRoleName = parentName,
                        ParentValueId = d.ParentValueId,
                        ChildValueId = d.ChildValueId
                    });
                });
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return clinicalContactValueMapping;
        }

        /// <summary>
        /// This method will call on Click of the Add button.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="childId"></param>
        /// <param name="userId"></param>
        /// <returns>
        /// list of value
        /// </returns>
        public List<ClinicalContactValueMappingBusinessModel> GetParentValueForSelectedRole(string customerId, string childId, string userId)
        {
            var parentValues = new List<ClinicalContactValueMappingBusinessModel>();
			IClinicalContactValueMappingRepository clinicalContactValueMappingRepository = null;
			IClinicalContactRoleMappingRepository clinicalContactRoleMappingRepository = null; 
            try
            {
                long clinicalCustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                long clinicalChildId = Convert.ToInt64(childId, CultureInfo.InvariantCulture);

				clinicalContactValueMappingRepository = new ClinicalContactValueMappingRepository();
				clinicalContactRoleMappingRepository = new ClinicalContactRoleMappingRepository(); 
 
				/// Get the Available Parent Id forselected Child
                long parentId = 0;
                long clinicalContactRoleMappingId = 0;

                /// Get Clnical Contact Role Mapping as per the CustomerId and Child List Id
				var clinicalContactRoleMapping = clinicalContactRoleMappingRepository.GetCustomersClinicalContactRoleMapping(clinicalCustomerId).Where(q => q.ChildListId == clinicalChildId).FirstOrDefault();

                parentId = Convert.ToInt64(clinicalContactRoleMapping.ParentListId, CultureInfo.InvariantCulture);

                if (parentId == null)
                {
                    parentId = 0;
                }
                var parentName = GetRoleNameForClinicalContact(Convert.ToString(parentId, CultureInfo.InvariantCulture), userId);
                var childName = GetRoleNameForClinicalContact(Convert.ToString(childId, CultureInfo.InvariantCulture), userId);

                /// Is the Parent is the Top Parent, Send the Empty record.
                if (parentId == 0)
                {
                    parentValues.Add(new ClinicalContactValueMappingBusinessModel()
                    {
                        ChildListId = 0,
                        ChildValue = string.Empty,
                        ChildValueId = 0,
                        HierarchyListId = clinicalContactRoleMapping.HierarchyListId,
                        ParentListId = clinicalContactRoleMapping.ParentListId,
                        ClinicalContactRoleMappingId = clinicalContactRoleMapping.ClinicalContactRoleMappingId,
                        ParentRoleName = parentName,
                        ChildRoleName = childName
                    });
                }
                else
                {
                    clinicalContactRoleMappingId = Convert.ToInt64(clinicalContactRoleMappingRepository.GetCustomersClinicalContactRoleMapping(clinicalCustomerId).Where(q => q.ChildListId == parentId).FirstOrDefault().ClinicalContactRoleMappingId);
                                        
                    /// Get the Values from ClinicalContactValueMapping for Parent Dropdrop down.
                    // var isParentHasValues = clinicalContactValueMappingRepository.GetAllClinicalContactValueMapping().Where(q => q.ChildListId == parentId && q.IsRemoved == false);
                    var isParentHasValues = clinicalContactValueMappingRepository.GetAllClinicalContactValueMapping().Where(q => q.ChildListId == parentId && q.IsRemoved == false && q.ClinicalContactRoleMappingId == clinicalContactRoleMappingId);
                    if (isParentHasValues.Any())
                    {
                        isParentHasValues.ToList().ForEach(d =>
                        {
                            parentValues.Add(new ClinicalContactValueMappingBusinessModel()
                            {
                                ChildListId = d.ChildListId,
                                ChildValue = d.ClinicalValueMaster.ContactName,
                                ChildValueId = d.ChildValueId,
                                HierarchyListId = clinicalContactRoleMapping.HierarchyListId,
                                ParentListId = clinicalContactRoleMapping.ParentListId,
                                ClinicalContactRoleMappingId = clinicalContactRoleMapping.ClinicalContactRoleMappingId,
                                ParentRoleName = parentName,
                                ChildRoleName = childName
                            });
                        });
                    }
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (clinicalContactValueMappingRepository != null)
                {
                    clinicalContactValueMappingRepository.Dispose();
                }

				if(clinicalContactRoleMappingRepository != null)
				{
					clinicalContactRoleMappingRepository.Dispose();
				}
            }
            return parentValues;
        }

        /// <summary>
        /// Get Role Name
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="userId"></param>
        /// <returns>
        /// Role Name
        /// </returns>
        public string GetRoleNameForClinicalContact(string roleId, string userId)
        {
			IClinicalContactAnalysisMasterRepository clinicalMasterRepository = null;
			try
            {
                var parentName = string.Empty;
				clinicalMasterRepository = new ClinicalContactAnalysisMasterRepository();
				var clinicalContactParent = clinicalMasterRepository.Find(Convert.ToInt64(roleId, CultureInfo.InvariantCulture));

                if (clinicalContactParent != null)
                {
                    parentName = clinicalContactParent.ContactName;
                }
                return parentName;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
			finally
			{
				if(clinicalMasterRepository != null)
				{
					clinicalMasterRepository.Dispose();
				}
			}
            return null;
        }

        /// <summary>
        /// Get parent name by child id
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <returns>
        /// role name
        /// </returns>
        public string GetRoleNameForClinicalContactByChildId(string roleId, string userId, string customerId)
        {
			IClinicalContactRoleMappingRepository clinicalRoleRepository = null;
			IClinicalContactAnalysisMasterRepository clinicalMasterRepository = null;
            try
            {
                var parentName = string.Empty;

                var iCustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                var iRoleId = Convert.ToInt64(roleId, CultureInfo.InvariantCulture);

                clinicalRoleRepository = new ClinicalContactRoleMappingRepository();
                clinicalMasterRepository = new ClinicalContactAnalysisMasterRepository();
                ClinicalContactAnalysisMaster clinicalContactAnalysisMaster = null;

                var clinicalContactRoleMapping = clinicalRoleRepository.GetCustomersClinicalContactRoleMapping(iCustomerId).FirstOrDefault(q => q.ChildListId == iRoleId);

                if (clinicalContactRoleMapping != null)
                {
                    clinicalContactAnalysisMaster = clinicalMasterRepository.Find(Convert.ToInt64(clinicalContactRoleMapping.ParentListId, CultureInfo.InvariantCulture));
                }

                if (clinicalContactAnalysisMaster != null)
                {
                    parentName = clinicalContactAnalysisMaster.ContactName;
                }

                return parentName;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
			finally
			{
				if(clinicalMasterRepository != null)
				{
					clinicalMasterRepository.Dispose();
				}

				if(clinicalRoleRepository != null)
				{
					clinicalRoleRepository.Dispose();
				}

			}
            return null;
        }

        /// <summary>
        /// Save clinical values
        /// </summary>
        /// <param name="clinicalContactMappingBusinessModel"></param>
        /// <returns>
        /// true or false
        /// </returns>
		public bool SaveClinicalContactValueMapping(ClinicalContactMappingBusinessModel clinicalContactMappingBusinessModel)
		{
			IClinicalContactValueMappingRepository clinicalContactValueMappingRepository = null;

			if (clinicalContactMappingBusinessModel == null)
			{
				return false;
			}

			var userId = clinicalContactMappingBusinessModel.UserId;

			try
			{
				clinicalContactValueMappingRepository = new ClinicalContactValueMappingRepository();
				IUnitOfWork unitOfWork = clinicalContactValueMappingRepository.UnitOfWork;
				Guid _userId = Guid.Parse(userId);

				ClinicalContactValueMapping clinicalContactValueMapping = null;

				if (clinicalContactMappingBusinessModel.ClinicalContactValueMappingList != null)
				{
					foreach (var contact in clinicalContactMappingBusinessModel.ClinicalContactValueMappingList)
					{

						/// Check for existing Value mapping record.
						if (contact.ClinicalContactValueMappingId > 0)
						{
							clinicalContactValueMapping = new ClinicalContactValueMapping();

							/// Get the Clinical Value Mapping By Id and Update the Same
							clinicalContactValueMapping = clinicalContactValueMappingRepository.Find(contact.ClinicalContactValueMappingId.Value);
							clinicalContactValueMapping.ParentValueId = contact.ParentValueId > 0 ? contact.ParentValueId : null;
							clinicalContactValueMapping.ClinicalValueMaster.ContactName = contact.ChildValue;
							clinicalContactValueMapping.IsRemoved = contact.IsRemoved;
							clinicalContactValueMapping.ModifiedBy = _userId;
							clinicalContactValueMapping.ModifiedDate = DateTime.Now;

							/// Update the Clinical Contact Value Master as per the Clinild Value Id
							clinicalContactValueMapping.ClinicalValueMaster.IsRemoved = contact.IsRemoved;

							clinicalContactValueMappingRepository.InsertOrUpdate(clinicalContactValueMapping);
						}
						else
						{
							///Insert parent value and child value in ClinicalValueMaster
							ClinicalValueMaster childClinicalValueMaster = new ClinicalValueMaster()
							{
								ContactName = contact.ChildValue,
								IsRemoved = contact.IsRemoved,
								ModifiedBy = _userId,
								ModifiedDate = DateTime.Now
							};

							/// Insert all details.
							clinicalContactValueMapping = new ClinicalContactValueMapping()
							{
								ClinicalContactValueMappingId = Convert.ToInt64(contact.ClinicalContactValueMappingId, CultureInfo.InvariantCulture),
								ClinicalContactRoleMappingId = contact.ClinicalContactRoleMappingId,
								ChildListId = contact.ChildListId,
								ChildValueId = childClinicalValueMaster.ClinicalValueMasterId,
								ParentValueId = contact.ParentValueId > 0 ? contact.ParentValueId : null,
								IsRemoved = contact.IsRemoved,
								ModifiedBy = _userId,
								ModifiedDate = DateTime.Now
							};

							clinicalContactValueMapping.ClinicalValueMaster = childClinicalValueMaster;
							clinicalContactValueMappingRepository.InsertOrUpdate(clinicalContactValueMapping);
						}
					}

					unitOfWork.Commit();
				}

				return true;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			finally
			{
				if (clinicalContactValueMappingRepository != null)
				{
					clinicalContactValueMappingRepository.Dispose();
				}
			}

			return false;
		}

        #endregion

        #region Add/ Update Clinical Contact & Analysis Information

        /// <summary>
        /// Add Edit to ClinicalContactAnalysis Master Table
        /// </summary>
        /// <param name="listTypeAdvanceBusinessModel">The list type advance business model.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public List<ListTypeAdvancedBusinessModel> UpdateClinicalContactAnalysis(List<ListTypeAdvancedBusinessModel> listTypeAdvanceBusinessModel, string userId)
        {            
            IClinicalContactAnalysisMasterRepository clinicalMasterRepository = new ClinicalContactAnalysisMasterRepository();
            IUnitOfWork unitOfWork = clinicalMasterRepository.UnitOfWork;
            var displayText = string.Empty;
            long listIndex = default(long);

            try
            {
                listTypeAdvanceBusinessModel.ForEach(listTypeBusinessModel =>
                    {
                        displayText = listTypeBusinessModel.DisplayText;

                        if (listTypeBusinessModel.ListId > 0)
                        {
                            /// Edit the List Item
                            var clinicalContactAnalysisMaster = clinicalMasterRepository.Find(listTypeBusinessModel.ListId);

                            /// Update the Translation
                            var translation = clinicalContactAnalysisMaster.TranslationProxy.Translations.FirstOrDefault(q => q.TransProxyID == clinicalContactAnalysisMaster.TranslationProxy.TransProxyID
                                && q.LanguageID == (long)SCAEnums.OtherConstants.LanguageId);

                            /// Verify if any update on the List Item Text
                            if (translation.TranslationType.ToUpper(CultureInfo.InvariantCulture) != displayText.ToUpper(CultureInfo.InvariantCulture))
                            {
                                translation.TranslationType = displayText;
                                translation.ModifiedBy = Guid.Parse(userId);
                                translation.ModifiedDate = DateTime.Now;

                                clinicalContactAnalysisMaster.ContactName = displayText;

                                /// Update the TranslationProxy
                                clinicalContactAnalysisMaster.TranslationProxy.Text = displayText;

                                /// Update List
                                clinicalContactAnalysisMaster.ModifiedBy = Guid.Parse(userId);
                                clinicalContactAnalysisMaster.ModifiedDate = DateTime.Now;

                                clinicalMasterRepository.InsertOrUpdate(clinicalContactAnalysisMaster);
                            }
                        }
                        else
                        {
                            /// Get the Lists with ListTypeId for Index
                            if (listTypeBusinessModel.ListTypeId > 0)
                            {
                                listIndex = clinicalMasterRepository.GetAllClinicalContactAnalysisMasterByTypeId(listTypeBusinessModel.ListTypeId).ToList().LastOrDefault().Index;
                            }

                            /// Add New List Item
                            var clinicalContactAnalysisMasterToAdd = new ClinicalContactAnalysisMaster();

                            /// No need to insert for ListType
                            /// Add New Translation Proxy
                            var translationProxy = new TranslationProxy()
                            {
                                Text = displayText,
                                ModifiedBy = Guid.Parse(userId),
                                ModifiedDate = DateTime.Now
                            };

                            clinicalContactAnalysisMasterToAdd.TranslationProxy = translationProxy;

                            /// Add New Translation
                            var translation = new Translation()
                            {
                                TranslationType = displayText,
                                LanguageID = Convert.ToInt64(SCAEnums.OtherConstants.LanguageId, CultureInfo.InvariantCulture),
                                ModifiedBy = Guid.Parse(userId),
                                TransProxyID = clinicalContactAnalysisMasterToAdd.TranslationProxy.TransProxyID,
                                ModifiedDate = DateTime.Now
                            };

                            clinicalContactAnalysisMasterToAdd.TranslationProxy.Translations.Add(translation);

                            /// Update the List to Save
                            clinicalContactAnalysisMasterToAdd.TypeId = listTypeBusinessModel.ListTypeId;
                            clinicalContactAnalysisMasterToAdd.Index = Convert.ToInt32(listIndex, CultureInfo.InvariantCulture) + 1;
                            clinicalContactAnalysisMasterToAdd.ContactName = displayText;
                            clinicalContactAnalysisMasterToAdd.TransProxyId = clinicalContactAnalysisMasterToAdd.TranslationProxy.TransProxyID;
                            clinicalContactAnalysisMasterToAdd.ModifiedBy = Guid.Parse(userId);
                            clinicalContactAnalysisMasterToAdd.ModifiedDate = DateTime.Now;

                            listTypeBusinessModel.ListId = clinicalContactAnalysisMasterToAdd.ClinicalContactAnalysisMasterId;
                            listTypeBusinessModel.ListIndex = Convert.ToInt32(clinicalContactAnalysisMasterToAdd.Index, CultureInfo.InvariantCulture);

                            clinicalMasterRepository.InsertOrUpdate(clinicalContactAnalysisMasterToAdd);
                        }
                    });

                unitOfWork.Commit();

                /// Get ListId for all List Item
                var typeIds = Convert.ToString((long)SCAEnums.ListType.MedicalStaff, CultureInfo.InvariantCulture) + "," + Convert.ToString((long)SCAEnums.ListType.AnalysisInformation, CultureInfo.InvariantCulture);

                /// Ger Clinical Contact Analysis Master data
                var clinicalContactMasterData = new DBHelper().GetClinicalContactAnalysisMaster(typeIds, (int)SCAEnums.OtherConstants.LanguageId);

                listTypeAdvanceBusinessModel.ForEach(d =>
                {
                    if (d.ListId == 0)
                    {
                        d.ListId = clinicalContactMasterData.Where(q => q.TypeId == d.ListTypeId && q.Index == d.ListIndex && q.ContactName.ToUpper(CultureInfo.InvariantCulture) == d.DisplayText.ToUpper(CultureInfo.InvariantCulture)).FirstOrDefault().clinicalcontactanalysismasterid;
                    }
                });
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (clinicalMasterRepository != null)
                {
                    clinicalMasterRepository.Dispose();
                }
            }
            return listTypeAdvanceBusinessModel;
        }

        /// <summary>
        /// Check for Remove Clinical Contact or Analysis Mapping for that customer
        /// </summary>
        /// <param name="medicalAnalysisInfoBusinessModel">Medical Analysis InfoBusiness Model</param>
        /// <returns>
        /// return true/false
        /// </returns>
        public bool RemoveClinicalContactAnalysisMapping(MedicalStaffAnalysisInfoBusinessModel medicalAnalysisInfoBusinessModel)
        {
            string userId = medicalAnalysisInfoBusinessModel.UserId.ToString();
            IIDXPatientMedicalStaffRepository idxPatientMedicalStaffRepository = new IDXPatientMedicalStaffRepository();

            ClinicalContactRoleMappingRepository clinicalContactRoleMappingRepository = null;
            try
            {
                var isMappingFound = false;
                if (medicalAnalysisInfoBusinessModel.CheckForChildLinkage)
                {
                    clinicalContactRoleMappingRepository = new ClinicalContactRoleMappingRepository();
                    var lstCustomersClinicalContactRoleMapping = clinicalContactRoleMappingRepository.GetCustomersClinicalContactRoleMapping(Convert.ToInt64(medicalAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture)).ToList();
                    isMappingFound = lstCustomersClinicalContactRoleMapping.SelectMany(d => d.ClinicalContactValueMappings).Any(d => d.ParentValueId == medicalAnalysisInfoBusinessModel.CustomerMedicalStaffAnalysisInfoId);

                }
                else
                {
                    var selectedPatientMedicalStaff = idxPatientMedicalStaffRepository.GetAllPatientMedicalStaff()
                        .Where(d => d.CustomerMedicalStaffId == medicalAnalysisInfoBusinessModel.CustomerMedicalStaffAnalysisInfoId
                        && d.Patient.CustomerId == medicalAnalysisInfoBusinessModel.CustomerId);

                    isMappingFound = selectedPatientMedicalStaff.Any();

                }
                return isMappingFound;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (idxPatientMedicalStaffRepository != null)
                {
                    idxPatientMedicalStaffRepository.Dispose();
                }
                if (clinicalContactRoleMappingRepository != null)
                {
                    clinicalContactRoleMappingRepository.Dispose();
                }
            }
            return false;
        }

        #endregion
    }
}