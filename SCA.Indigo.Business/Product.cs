﻿//----------------------------------------------------------------------------------------------
// <copyright file="Product.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;
    using System.ServiceModel;

    /// <summary>
    /// Product Class
    /// </summary>        
    public class Product : IProduct
    {
        /// <summary>
        /// Get Product Details
        /// </summary>
        /// <param name="productBusinessModel"></param>
        /// <returns>
        /// List of Product details
        /// </returns>
        public List<ProductBusinessModel> GetProductDetails(ProductBusinessModel productBusinessModel)
        {
            var userId = string.Empty;
            var searchDetails = new List<ProductBusinessModel>();
            IIDXCustomerProductRepository productRepository = new IDXCustomerProductRepository();
            long iCustomerId = 0;
            var isOverridden = false;
            try
            {
                if (productBusinessModel == null)
                {
                    return null;
                }

                string searchText = productBusinessModel.SearchText;
                iCustomerId = productBusinessModel.CustomerId;

                if (searchText != "blank")
                {
                    var productList = productRepository.SearchProduct(searchText, iCustomerId).OrderBy(q => q.Product.SortOrder).ToList();
                    
                    productList.ForEach(q =>
                    {
                        isOverridden = false;

                        if (q.IsOverridden && q.Quantity.HasValue && q.Frequency.HasValue)
                        {
                            isOverridden = true;
                        }
                        else if (q.IsOverridden)
                        {
                            //As part of Sprint 4, 3 new columns being added in IDXCustomerProduct table - Override, Frequency and Quantity. Reference  - Jira Task ID: HDUK-258
                            //These columns would be maintained manually by SCA internal admin users till a separate screen is enabled for customer product maintenance. 
                            //Manual entry error of incorrect configuration should ideally be logged into Indigo logging table, however, as part of future sprints - Logging 
                            //functionality will be revamped and hence this exception will be handled in the appropriate forthcoming sprint.
                        }

                        searchDetails.Add(new ProductBusinessModel()
                         {
                             ProductID = q.ProductId,
                             ProductDisplayID = CommonHelper.TrimProductId(q.Product.SAPProductID),
                             ProductName = q.Product.DescriptionUI,
                             ApprovalFlag = q.IsApporvalRequired,
                             SalesUnit = q.Product.SalesUnit,
                             PackSize = q.Product.PackSize,
                             IsOverridden = isOverridden,
                             Quantity = q.Quantity ?? 0,
                             Frequency = q.Frequency ?? 0
                         });
                    });
                }
                else
                {
                    var customerProducts = productRepository.GetCustomerProducts(iCustomerId);
                    if (customerProducts.Any())
                    {
                        customerProducts.ToList().ForEach(q =>
                        {
                            isOverridden = false;

                            if (q.IsOverridden && q.Quantity.HasValue && q.Frequency.HasValue)
                            {
                                isOverridden = true;
                            }
                            else if (q.IsOverridden)
                            {
                                //As part of Sprint 4, 3 new columns being added in IDXCustomerProduct table - Override, Frequency and Quantity. Reference  - Jira Task ID: HDUK-258
                                //These columns would be maintained manually by SCA internal admin users till a separate screen is enabled for customer product maintenance. 
                                //Manual entry error of incorrect configuration should ideally be logged into Indigo logging table, however, as part of future sprints - Logging 
                                //functionality will be revamped and hence this exception will be handled in the appropriate forthcoming sprint.
                            }

                            searchDetails.Add(new ProductBusinessModel()
                            {
                                ProductDisplayID = CommonHelper.TrimProductId(q.Product.SAPProductID),
                                ProductID = q.ProductId,
                                ProductName = q.Product.DescriptionUI,
                                ApprovalFlag = q.IsApporvalRequired,
                                SalesUnit = q.Product.SalesUnit,
                                PackSize = q.Product.PackSize,
                                IsOverridden = isOverridden,
                                Quantity = q.Quantity ?? 0,
                                Frequency = q.Frequency ?? 0
                            });
                        });
                    }
                }

                return searchDetails;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (productRepository != null)
                {
                    productRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Get SearchResult Product
        /// </summary>
        /// <param name="searchText">search Text</param>
        /// <returns>
        /// Return list of ProductBusinessModel
        /// </returns>
        public List<ProductBusinessModel> GetSearchResult(string searchText)
        {
            var searchDetails = new List<ProductBusinessModel>();
            IProductRepository productRepository = new ProductRepository();
            try
            {
                searchDetails = productRepository.All.Where(prod => prod.DescriptionUI.Contains(searchText))
                                .Select(prod => new ProductBusinessModel
                {
                    ProductName = prod.DescriptionUI
                }).ToList();
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, string.Empty);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, string.Empty);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, string.Empty);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, string.Empty);
                throw;
            }
            finally
            {
                if (productRepository != null)
                {
                    productRepository.Dispose();
                }
            }

            return searchDetails;
        }

        /// <summary>
        /// Get Product Details of selected Product
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// List of Products
        /// </returns>
        public List<ProductBusinessModel> ProductDetails(string customerId)
        {
            var productDetails = new List<ProductBusinessModel>();
            var userId = string.Empty;
            IIDXCustomerProductRepository productRepository = new IDXCustomerProductRepository();
            long iCustomerId = 0;
            var isOverridden = false;
            try
            {
                if (!string.IsNullOrEmpty(customerId))
                {
                    iCustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                }

                var customerProducts = productRepository.GetCustomerProducts(iCustomerId).OrderBy(q => q.Product.SortOrder).ToList();
                if (customerProducts.Any())
                {
                    customerProducts.ToList().ForEach(q =>
                    {
                        isOverridden = false;

                        if (q.IsOverridden && q.Quantity.HasValue && q.Frequency.HasValue)
                        {
                            isOverridden = true;
                        }
                        else if (q.IsOverridden)
                        {
                            //As part of Sprint 4, 3 new columns being added in IDXCustomerProduct table - Override, Frequency and Quantity. Reference  - Jira Task ID: HDUK-258
                            //These columns would be maintained manually by SCA internal admin users till a separate screen is enabled for customer product maintenance. 
                            //Manual entry error of incorrect configuration should ideally be logged into Indigo logging table, however, as part of future sprints - Logging 
                            //functionality will be revamped and hence this exception will be handled in the appropriate forthcoming sprint.
                        }

                        productDetails.Add(new ProductBusinessModel()
                        {
                            ProductDisplayID = CommonHelper.TrimProductId(q.Product.SAPProductID),
                            ProductID = q.ProductId,
                            ProductName = q.Product.DescriptionUI,
                            ApprovalFlag = q.IsApporvalRequired,
                            SalesUnit = q.Product.SalesUnit,
                            PackSize = q.Product.PackSize,
                            IsOverridden = isOverridden,
                            Quantity = q.Quantity ?? 0,
                            Frequency = q.Frequency ?? 0
                        });
                    });
                }
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (productRepository != null)
                {
                    productRepository.Dispose();
                }
            }

            return productDetails;
        }

        #region SampleProducts
        /// <summary>
        /// Get Sample Product
        /// </summary>
        /// <param name="searchText">Search input</param>
        /// <returns>
        /// list of SampleProductBusinessModel
        /// </returns>
        public List<SampleProductBusinessModel> GetSampleProductDetails(SampleProductBusinessModel sampleProductBusinessModel)
        {
            var userId = string.Empty;
            var searchDetails = new List<SampleProductBusinessModel>();
            ICustomerRepository _customerRepository = new CustomerRepository();
            IProductRepository productRepository = new ProductRepository();
            IIDXCustomerProductRepository customerRepository = new IDXCustomerProductRepository();
            try
            {
                // If sample order is not associated with customer 
                if (sampleProductBusinessModel.CustomerId == null)
                {
                    if (sampleProductBusinessModel.SearchText != "blank")
                {
                        var ProductList = productRepository.SearchSampleProduct(sampleProductBusinessModel.SearchText).ToList();
                    ProductList.ToList().ForEach(q =>
                    {
                        searchDetails.Add(new SampleProductBusinessModel()
                {
                    ProductID = q.SampleProductId,
                    HelixProductId = q.HelixProductID,
                    DescriptionUI = q.ProductDescription,
                    MaxQuantity = q.MaxQuantity
                });
                    });
                }
                else
                {
                    productRepository.AllSampleProducts.ToList().ForEach(d =>
                    {
                        searchDetails.Add(new SampleProductBusinessModel()
            {
                ProductID = d.SampleProductId,
                HelixProductId = d.HelixProductID,
                DescriptionUI = d.ProductDescription,
                MaxQuantity = d.MaxQuantity
            });
                    });
                }
                }
                else // This are assocaited with customer
                {
                    Customer customer = _customerRepository.GetCustomerById(Convert.ToInt64(sampleProductBusinessModel.CustomerId, CultureInfo.InvariantCulture));
                    var objCustomerParameter = customer.CustomerParameters.First();
                    var searchProductType = objCustomerParameter.SampleOrderProductType;
                    if (sampleProductBusinessModel.SearchText != "blank")
                    {
                        if (searchProductType.ToLower(CultureInfo.InvariantCulture).Trim() == "tena")
                        {
                            var ProductList = productRepository.SearchSampleProduct(sampleProductBusinessModel.SearchText).ToList();
                            ProductList.ToList().ForEach(q =>
                            {
                                searchDetails.Add(new SampleProductBusinessModel()
                        {
                            ProductID = q.SampleProductId,
                            HelixProductId = q.HelixProductID,
                            DescriptionUI = q.ProductDescription,
                            MaxQuantity = q.MaxQuantity
                        });
                            });
                        }
                        else if (searchProductType.ToLower(CultureInfo.InvariantCulture).Trim() == "customer")
                        {
                            customerRepository.SearchProduct(sampleProductBusinessModel.SearchText, Convert.ToInt64(sampleProductBusinessModel.CustomerId, CultureInfo.InvariantCulture)).Where(s => s.IsDisplayForSample == true && s.IsAvailable == true).ToList().ForEach(q =>
                            {
                                var allProduct = productRepository.All.Where(p => p.ProductId == q.ProductId).ToList();
                                searchDetails.Add(new SampleProductBusinessModel()
                                {
                                    ProductID = q.ProductId,
                                    HelixProductId = allProduct[0].BaseMaterial,
                                    DescriptionUI = allProduct[0].DescriptionUI,
                                  //  MaxQuantity = q.Quantity
                                });
                            });
                        }
                    }
                    else
                    {
                        if (searchProductType.ToLower(CultureInfo.InvariantCulture).Trim() == "tena")
                        {
                            productRepository.AllSampleProducts.ToList().ForEach(d =>
                            {
                                searchDetails.Add(new SampleProductBusinessModel()
                    {
                        ProductID = d.SampleProductId,
                        HelixProductId = d.HelixProductID,
                        DescriptionUI = d.ProductDescription,
                        MaxQuantity = d.MaxQuantity
                    });
                            });
                        }
                        if (searchProductType.ToLower(CultureInfo.InvariantCulture).Trim() == "customer")
                        {
                            customerRepository.GetCustomerProducts(Convert.ToInt64(sampleProductBusinessModel.CustomerId, CultureInfo.InvariantCulture)).ToList().ForEach(q =>
                            {
                                var allProduct = productRepository.All.Where(p => p.ProductId == q.ProductId).ToList();
                                searchDetails.Add(new SampleProductBusinessModel()
                                {
                                    ProductID = q.ProductId,
                                    HelixProductId = allProduct[0].BaseMaterial,
                                    DescriptionUI = allProduct[0].DescriptionUI,
                                });
                            });
                        }
                    }
                }
                return searchDetails;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (productRepository != null)
                {
                    productRepository.Dispose();
                }
                if (_customerRepository != null)
                {
                    _customerRepository.Dispose();
                }
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// get all sample products
        /// </summary>
        /// <returns>
        /// list of products
        /// </returns>
        public List<SampleProductBusinessModel> SampleProductDetails(string customerId, string sampleOrderCustomerProductType, string sampleOrderTenaProductType)
        {
            var sampleProductBusinessModel = new List<SampleProductBusinessModel>();
            var userId = string.Empty;
            ICustomerRepository _customerRepository = null;
            IProductRepository productRepository = new ProductRepository();
            IIDXCustomerProductRepository customerRepository = new IDXCustomerProductRepository();
            _customerRepository = new CustomerRepository();
            Customer customer = _customerRepository.GetCustomerById(Convert.ToInt64(customerId, CultureInfo.InvariantCulture));
            var objCustomerParameter = customer == null ? null : customer.CustomerParameters.First();
            try
            {
                //--- Show Product list based on Customer product list or Tena product list---.
                if (objCustomerParameter == null || string.IsNullOrEmpty(objCustomerParameter.SampleOrderProductType)) // If sample order is not associated with any customer
                {
                    productRepository.AllSampleProducts.ToList().ForEach(d =>
                            {
                                sampleProductBusinessModel.Add(new SampleProductBusinessModel()
                                    {
                                        ProductID = d.SampleProductId,
                                        HelixProductId = d.HelixProductID,
                                        DescriptionUI = d.ProductDescription,
                                        MaxQuantity = d.MaxQuantity
                                    });
                            });
                }
                else
                { 
                    if (objCustomerParameter.SampleOrderProductType.ToLower(CultureInfo.InvariantCulture).Trim() == sampleOrderTenaProductType)
                    {
                productRepository.AllSampleProducts.ToList().ForEach(d =>
                    {
                        sampleProductBusinessModel.Add(new SampleProductBusinessModel()
                            {
                                ProductID = d.SampleProductId,
                                HelixProductId = d.HelixProductID,
                                DescriptionUI = d.ProductDescription,
                                MaxQuantity = d.MaxQuantity
                            });
                    });
            }
                    else if (objCustomerParameter.SampleOrderProductType.ToLower(CultureInfo.InvariantCulture).Trim() == sampleOrderCustomerProductType)
                        customerRepository.GetCustomerProducts(Convert.ToInt64(customerId, CultureInfo.InvariantCulture)).Where(s => s.IsDisplayForSample == true && s.IsAvailable == true).ToList().ForEach(q =>
                    {
                        var allProduct = productRepository.All.Where(p => p.ProductId == q.ProductId).ToList();
                        sampleProductBusinessModel.Add(new SampleProductBusinessModel()
                        {
                            ProductID = q.ProductId,
                            HelixProductId = allProduct[0].BaseMaterial,
                            DescriptionUI = allProduct[0].DescriptionUI                            
                        });
                    });
                }
                
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (productRepository != null)
                {
                    productRepository.Dispose();
                }
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
                if (_customerRepository != null)
                {
                    _customerRepository.Dispose();
                }
            }

            return sampleProductBusinessModel;
        }

        #endregion

        /// <summary>
        /// Get list of product name and id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId">Customer identifier</param>
        /// <returns>
        /// Return list of products
        /// </returns>
        public List<DropDownDataBusinessModel> GetProductsOnCustomerId(string customerId, string userId)
        {
            try
            {
                var customerDetails = (new DBHelper()).GetProductsOnCustomerId(customerId);
                var result = customerDetails.Select(item => new DropDownDataBusinessModel()
                {
                    Value = item.ProductId,
                    DisplayText = item.ProductName
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }
    }
}