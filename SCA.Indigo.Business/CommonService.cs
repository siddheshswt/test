﻿//----------------------------------------------------------------------------------------------
// <copyright file="CommonService.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Configuration;
    using System.ServiceModel;

    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using System.IO;
    using DocumentFormat.OpenXml;

    using BusinessModels;
    using Interfaces;
    using Common;
    using Infrastructure.Repositories;
    using Model;
    using Common.Enums;

    /// <summary>
    /// Common Service Business Class
    /// </summary>    
    public class CommonService : ICommonService
    {
        /// <summary>
        /// Sets Time Format
        /// </summary>
        private readonly string timeFormat = "HH:mm";
        private static readonly string ipAddress = ConfigurationManager.AppSettings["IPAddress"];
        private static readonly string sharedLocation = ConfigurationManager.AppSettings["SharedLocation"];
        private static readonly string uploadDirectory = ConfigurationManager.AppSettings["UploadLocation"];
        private static readonly string scaTaskScheduler = ConfigurationManager.AppSettings["ServerAddress"];
        private static readonly string onlineTelephoneOrderLog = ConfigurationManager.AppSettings["OnlineTelephoneOrderLog"];

        /// <summary>
        /// Gets or sets the Log File Path
        /// </summary>
        /// <value>
        /// The Log File Path
        /// </value>        
        public static string UILogIPAddress { get; set; }

        /// <summary>
        /// Public property for arrangedNdd
        /// </summary>
        public DateTime ArrangedNDD { get; set; }

        /// <summary>
        /// Gets or sets postCode Matrix List
        /// </summary>
        /// <value>
        /// The post code matrix list.
        /// </value>
        public List<PostCodeMatrixBusinessModel> PostcodeMatrixList { get; set; }

        /// <summary>
        /// Gets or sets customer Parameter List
        /// </summary>
        /// <value>
        /// The customer parameter list.
        /// </value>
        public List<CustomerParameterBusinessModel> CustomerParameterList { get; set; }

        /// <summary>
        /// Get Customer's Parameter
        /// </summary>
        /// <param name="objCustomerParameterBusinessModel">object of CustomerParameterBusinessModel</param>
        /// <returns>
        /// List of CustomerParameterBusinessModel
        /// </returns>
        public List<CustomerParameterBusinessModel> GetCustomerParameter(CustomerParameterBusinessModel objCustomerParameterBusinessModel)
        {
            if (objCustomerParameterBusinessModel == null)
            {
                return null;
            }

            ICustomerParameterRepository customParameterRepository = new CustomerParameterRepository();
            IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository = null;
            var userId = objCustomerParameterBusinessModel.UserID;

            try
            {
                idxCustomerMedicalStaffAnalysisInfoRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();
                var objCustomerParametersRules = customParameterRepository.GetAllCustomerParametersByCustomerId(objCustomerParameterBusinessModel.CustomerID).ToList();
                if (objCustomerParametersRules.Any())
                {
                    var medicalAnalysisList = idxCustomerMedicalStaffAnalysisInfoRepository
                                                        .GetListIdByCustomerId(objCustomerParameterBusinessModel.CustomerID).Where(q => q.IsMandatory == true).ToList();

                    return objCustomerParametersRules.Select(customerparam => new CustomerParameterBusinessModel
                    {
                        CustomerParameterID = customerparam.CustomerParameterID,
                        CustomerID = customerparam.CustomerID,
                        OrderCreationAllowed = customerparam.OrderCreationAllowed,
                        OrderApproval = customerparam.OrderApproval,
                        OrderLeadTime = customerparam.OrderLeadTime,
                        OrderCutOffTime = customerparam.OrderCutOffTime.HasValue ? new DateTime(customerparam.OrderCutOffTime.Value.Ticks).ToString(this.timeFormat, CultureInfo.InvariantCulture) : default(TimeSpan).ToString(this.timeFormat, CultureInfo.InvariantCulture),
                        CheckAge = customerparam.CheckAge,
                        IsNHSIDMandatory = customerparam.IsNHSIDMandatory,
                        IsADPMandatory = customerparam.IsADPMandatory,
                        IsTelephoneMandatory = customerparam.IsTelephoneMandatory,
                        IsNextAssessmentDateMandatory = customerparam.IsNextAssessmentDateMandatory,
                        MaxPadPerDay = customerparam.MaxPadPerDay.Value,
                        NextAssessmentDate = customerparam.NextAssessmentDate,
                        IsCustomerApprovalReq = customerparam.IsCustomerApprovalReq,
                        AllowIncreaseOrderQty = customerparam.AllowIncreaseOrderQty,
                        Age = customerparam.Age,
                        IsOrderAllowed = customerparam.IsOrderAllowed,
                        IsNDDAllow = customerparam.IsNDDAllow,
                        IsClinicalContactWithLinkage = customerparam.IsClinicalContactWithLinkage == true,
                        SampleOrderProductType = customerparam.SampleOrderProductType,
                        CountryId = Convert.ToInt32(customerparam.Customer.CountryId, CultureInfo.InvariantCulture) == 0 ? (int)SCAEnums.Country.UnitedKingdom : (int)customerparam.Customer.CountryId,
                        CountOfMandatoryAnalysisField = medicalAnalysisList.Where(q => (q.ClinicalContactAnalysisMaster.TypeId == (int)SCAEnums.ListType.AnalysisInformation
                                                           || ((bool)customerparam.IsClinicalContactWithLinkage ? false : q.ClinicalContactAnalysisMaster.TypeId == (int)SCAEnums.ListType.MedicalStaff))
                                                           ).Count()
                    }).ToList();
                }

                return new List<CustomerParameterBusinessModel>();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (customParameterRepository != null)
                {
                    customParameterRepository.Dispose();
                }

                if (idxCustomerMedicalStaffAnalysisInfoRepository != null)
                {
                    idxCustomerMedicalStaffAnalysisInfoRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Gets Get Holiday List
        /// </summary>
        /// <param name="userId">User's Id</param>
        /// <param name="countryId"></param>
        /// <returns>
        /// List of HolidayBusinessModel
        /// </returns>
        public List<HolidayBusinessModel> GetHolidayList(string userId, string countryId)
        {
            IHolidayListRepository holidayRepository = null;
            try
            {
                int iCountryId = Convert.ToInt32(countryId, CultureInfo.InvariantCulture);
                holidayRepository = new HolidayListRepository();
                var holidayList = holidayRepository.All.Where(q => q.CountryID == iCountryId);

                List<HolidayBusinessModel> objHolidayList = new List<HolidayBusinessModel>();
                HolidayBusinessModel holiday;

                foreach (var objHoliday in holidayList)
                {
                    holiday = new HolidayBusinessModel
                    {
                        HolidayDate = objHoliday.HolidayDate,
                        HolidayDateStringFormat = objHoliday.HolidayDate.ToString(CommonConstants.DateFormat2, CultureInfo.InvariantCulture)
                    };
                    objHolidayList.Add(holiday);
                }

                return objHolidayList;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (holidayRepository != null)
                {
                    holidayRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Common service call to get the lists
        /// </summary>
        /// <param name="listTypeId">list type identifier</param>
        /// <param name="languageId">language identifier</param>
        /// <returns>
        /// List of ListTypeBusinessModel
        /// </returns>
        public List<ListTypeBusinessModel> GetListType(string listTypeId, string languageId)
        {
            var userId = string.Empty;
            try
            {
                var result = (new DBHelper()).GetListDetails(listTypeId, Convert.ToInt32(languageId, CultureInfo.InvariantCulture));
                var response = result.ToListTypeBusinessModel();
                return response;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Get List Type for Clinical Contact
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns>
        /// list of object
        /// </returns>
        public List<ListTypeBusinessModel> GetListTypeForClinicalContact(string listTypeId, string languageId)
        {
            var userId = string.Empty;
            try
            {
                var result = (new DBHelper()).GetClinicalContactAnalysisMaster(listTypeId, Convert.ToInt32(languageId, CultureInfo.InvariantCulture));
                var response = result.ToClinicalContactListTypeBusinessModel();
                return response;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Common service call to get the lists
        /// </summary>
        /// <param name="listTypeId">list type identifier</param>
        /// <param name="listId">The list identifier.</param>
        /// <param name="languageId">language identifier</param>
        /// <returns>
        /// List of ListTypeBusinessModel
        /// </returns>
        public List<ListTypeBusinessModel> GetListTypeForInteraction(string listTypeId, string listId, string languageId)
        {
            var userId = string.Empty;
            try
            {
                var result = (new DBHelper()).GetListDetails(listTypeId, listId, Convert.ToInt32(languageId, CultureInfo.InvariantCulture));
                var response = result.ToListTypeBusinessModel();
                return response;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Load Patient type matrix
        /// </summary>
        /// <param name="patientTypes">List of PatientType</param>
        /// <returns>
        /// List of PatientTypeMatrixBusinessModel
        /// </returns>
        public List<PatientTypeMatrixBusinessModel> GetPatientTypeMatrix(List<long> patientTypes)
        {
            string userId = string.Empty;
            IPatientTypeMatrixRepository patientMatrixRepository = null;
            try
            {
                patientMatrixRepository = new PatientTypeMatrixRepository();
                var patientTypeMatrix = patientMatrixRepository.GetMatrixByPatientType(patientTypes);
                return patientTypeMatrix.Select(q => new PatientTypeMatrixBusinessModel
                {
                    PrescMinFrequency = q.Presc_Min_Freq,
                    PrescMaxFrequency = q.Presc_Max_Freq,
                    IsAddFromPrescriptionAllowed = q.IsAddFromPrescriptionAllowed,
                    IsAddFromProductAllowed = q.IsAddFromProductAllowed,
                    AdvanceOrderActivationDays = q.AdvanceOrderActivationDays,
                    ChangeOrderNddDays = q.ChangeOrderNddDays,
                    IsOneOffAddFromPrescriptionAllowed = q.IsOneOffAddFromPrescriptionAllowed,
                    IsOneOffAddFromProductAllowed = q.IsOneOffAddFromProductAllowed,
                    IsPrescriptionApprovalRequired = q.IsPrescriptionApprovalRequired,
                    AdultBillTo = q.AdultBillTo,
                    PaedBillTo = q.PaedBillTo,
                    IncrementFrequency = q.IncrementFrequency,
                    PatientType = q.List.DefaultText,
                    PatientTypeId = q.PatientType,
                    customerID = q.CustomerId,
                    DefaultFrequency = q.DefaultFrequency
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (patientMatrixRepository != null)
                {
                    patientMatrixRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Loads PostCode Matrix
        /// </summary>
        /// <param name="customerId">customer's ID</param>
        /// <param name="patientID">The patient identifier.</param>
        /// <param name="carehomeID">The carehome identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// List of PostCodeMatrixBusinessModel
        /// </returns>
        public List<PostCodeMatrixBusinessModel> GetPostcodeMatrixForPatient(string customerId, string patientId, string careHomeId, string userId)
        {
            try
            {
                var intCustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                var intPatientId = Convert.ToInt64(patientId, CultureInfo.InvariantCulture);
                var intCarehomeId = Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture);
                var postCodeMatrixDetails = (new DBHelper()).GetPostCodeMatrixForPatient(intCustomerId, intPatientId, intCarehomeId);
                if (postCodeMatrixDetails.Any())
                {
                    return postCodeMatrixDetails.Select(q => new PostCodeMatrixBusinessModel
                    {
                        CustomerID = intCustomerId,
                        PostcodeMatrixID = q.PostcodeMatrixID,
                        Monday = q.Monday,
                        Tuesday = q.Tuesday,
                        Wednesday = q.Wednesday,
                        Thursday = q.Thursday,
                        Friday = q.Friday,
                        RoundID = q.RoundID != null ? q.RoundID.ToUpper(CultureInfo.InvariantCulture) : "0",
                        Round = q.Round != null ? q.Round.ToUpper(CultureInfo.InvariantCulture) : "0",
                    }).ToList();
                }

                return new List<PostCodeMatrixBusinessModel>();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Return the Postcode Matrix
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="round"></param>
        /// <param name="roundId"></param>
        /// <param name="postCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public PostCodeMatrixBusinessModel GetPostcodeMatrix(string customerId, string round, string roundId, string postCode, string userId)
        {
            PostCodeMatrixBusinessModel objPostCodeMatrixBusinessModel = new PostCodeMatrixBusinessModel();
            try
            {
                using (IPostCodeMatrixRepository postCodeRepository = new PostCodeMatrixRepository())
                {
                    var custId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                    IUnitOfWork unitOfWork = postCodeRepository.UnitOfWork;

                    var objPostCode = postCodeRepository.All.Where(q => q.CustomerID == custId &&
                                                                        q.Round == round &&
                                                                        q.RoundID == roundId &&
                                                                        q.IsActive == true &&
                                                                        q.Postcode == (postCode == "null" ? "" : postCode)).FirstOrDefault();

                    if(objPostCode == null)
                        objPostCode = postCodeRepository.All.Where(q => q.CustomerID == custId &&
                                                                        q.Round == round &&
                                                                        q.RoundID == roundId &&
                                                                        q.IsActive == true).FirstOrDefault();
					if (objPostCode == null)
					{
						objPostCodeMatrixBusinessModel = new PostCodeMatrixBusinessModel
						{
							CustomerID = custId,
							PostcodeMatrixID = 0,
							Monday = true,
							Tuesday = true,
							Wednesday = true,
							Thursday = true,
							Friday = true,
							RoundID = roundId,
							Round = round,
						};
					}
					else
					{
                        objPostCodeMatrixBusinessModel = new PostCodeMatrixBusinessModel
                        {
                            CustomerID = custId,
                            PostcodeMatrixID = objPostCode.PostcodeMatrixID,
                            Monday = objPostCode.Monday,
                            Tuesday = objPostCode.Tuesday,
                            Wednesday = objPostCode.Wednesday,
                            Thursday = objPostCode.Thursday,
                            Friday = objPostCode.Friday,
                            RoundID = objPostCode.RoundID != null ? objPostCode.RoundID.ToUpper(CultureInfo.InvariantCulture) : "0",
                            Round = objPostCode.Round != null ? objPostCode.Round.ToUpper(CultureInfo.InvariantCulture) : "0",
                        };
                }
            }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return objPostCodeMatrixBusinessModel;
        }

        #region ***** NDD Functions *******

        /// <summary>
        /// UpdatePrescriptionProductNdd
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="deliveryDate">The delivery date.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Date Time</returns>
        public DateTime UpdatePrescriptionProductNdd(long orderId, DateTime? deliveryDate, long customerId, Guid userId, bool isHolidayCheckRequired = true)
        {
            var minimumNDD = DateTime.Today;
            var setUserId = Convert.ToString(userId, CultureInfo.InvariantCulture);
            try
            {
                if (deliveryDate != null)
                {
                    var NDD = Convert.ToDateTime(deliveryDate, CultureInfo.InvariantCulture);
                    minimumNDD = NDD;
                    var isFirstRecord = true;
                    var dbHelper = new DBHelper();

                    var orderItems = dbHelper.GetOrderItemsDetails(orderId);

                    foreach (var item in orderItems)
                    {
                        if (item.IDXPrescriptionProductId != null)
                        {
                            NDD = GetNdd(Convert.ToDateTime(deliveryDate, CultureInfo.InvariantCulture), Convert.ToInt64(item.Frequency, CultureInfo.InvariantCulture), customerId, userId, isHolidayCheckRequired); //calculate NDD for each product                              
                            dbHelper.UpdatePrescriptionNDD(item.IDXPrescriptionProductId, NDD); //update NDD for each product
                            
                            //if (isFirstRecord)
                            //{
                            //    minimumNDD = NDD;
                            //    isFirstRecord = false;
                            //}
                            //else
                            //{
                            //    minimumNDD = minimumNDD < NDD ? minimumNDD : NDD;
                            //}
                        }
                    }
                    minimumNDD = dbHelper.GetNDDWithMinimumFrequency(orderId).Value;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, setUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, setUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, setUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, setUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, setUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, setUserId);
                throw;
            }
            return minimumNDD;
        }

        /// <summary>
        /// GetNdd
        /// </summary>
        /// <param name="deliveryDate">The delivery date.</param>
        /// <param name="frequency">The frequency.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Date Time</returns>
        public DateTime GetNdd(DateTime deliveryDate, long? frequency, long customerId, Guid userId, bool isHolidayCheckRequired = true)
        {
            var countryId = 1;
            var NDD = deliveryDate;
            NDD = NDD.AddDays(Convert.ToDouble(frequency, CultureInfo.InvariantCulture) * 7);

            countryId = GetCountryId(customerId, Convert.ToString(userId, CultureInfo.InvariantCulture));

            NDD = GetNextWorkingDay(NDD, countryId, Convert.ToString(userId, CultureInfo.InvariantCulture), isHolidayCheckRequired);

			var isHoliday = false;
			do
			{
				if(isHoliday)
				{
					NDD = NDD.AddDays(1);
				}
				NDD = GetDateAfterPostcodeMatrix(NDD);
			}
			while (isHolidayCheckRequired && (isHoliday = IsHoliday(NDD, countryId, default(Guid).ToString())));

            var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(NDD, customerId, userId);
            ///an arranged date is found in Holiday Process table then use that as a delivery date
            if (isArrangedDateFound)
            {
                NDD = ArrangedNDD;
            }

            return NDD;
        }

        /// <summary>
        /// GetNextWorkingDay
        /// </summary>
        /// <param name="date"></param>
        /// <param name="countryId"></param>
        /// <param name="userId"></param>
        /// <returns>date</returns>
        public DateTime GetNextWorkingDay(DateTime date, int countryId, string userId, bool isHolidayCheckRequired = true)
        {
            while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || (isHolidayCheckRequired && IsHoliday(date, countryId, userId)))
            {
                date = date.AddDays(1);
            }

            return date;
        }


        /// <summary>
        /// IsHoliday
        /// </summary>
        /// <param name="date"></param>
        /// <param name="countryId"></param>
        /// <param name="userId"></param>
        /// <returns>true/false</returns>
        public bool IsHoliday(DateTime date, long countryId, string userId)
        {
            var holidayList = new List<HolidayBusinessModel>();
            holidayList = GetHolidayList(userId, Convert.ToString(countryId, CultureInfo.InvariantCulture));
            if (holidayList.Where(q => q.HolidayDate == date).Any())
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// GetCountryId
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <returns>Country Id</returns>
        public int GetCountryId(long customerId, string userId)
        {
            ICustomerRepository objCustomerRepository = null;
            try
            {
                objCustomerRepository = new CustomerRepository();
                var countryId = objCustomerRepository.GetCountryId(customerId);
                return countryId;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (objCustomerRepository != null)
                {
                    objCustomerRepository.Dispose();
                }
            }
            return 1;
        }

        /// <summary>
        /// CheckDateAvailabilityInHolidayProcess
        /// </summary>
        /// <param name="nextDeliveryDate">The next delivery date.</param>
        /// <param name="CustomerID">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>True Or False</returns>
        public bool CheckDateAvailabilityInHolidayProcess(DateTime nextDeliveryDate, long customerId, Guid userId)
        {
            var objHolidayBusinessModel = new HolidayProcessAdminBusinessModels
            {
                CustomerId = customerId,
                DerivedNDD = Convert.ToString(nextDeliveryDate, CultureInfo.InvariantCulture),
                OrderCreationTime = Convert.ToString(DateTime.Now.ToString(new CommonService().timeFormat, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture),
                UserId = userId
            };
            var strArrangedDate = (string)new CommonService().GetArrangedDeliveryDate(objHolidayBusinessModel);
            if (strArrangedDate != null)
            {
                ArrangedNDD = Convert.ToDateTime(strArrangedDate, CultureInfo.InvariantCulture);
                return true;
            }
            return false;
        }


        /// <summary>
        /// Adds the working days.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="noOfDays">The no of days.</param>
        /// <param name="countryId">Holiday as per country id</param>
        /// <returns>date</returns>
        public DateTime AddWorkingDays(DateTime date, long noOfDays, long countryId, string userId, bool isHolidayCheckRequired = true)
        {
            var days = noOfDays;

            while (days != 0)
            {
                date = date.AddDays(1);
                days--;

                while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || (isHolidayCheckRequired && IsHoliday(date, countryId, userId)))
                    date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// Get the immediate DateAfterPostcodeMatrix
        /// </summary>
        /// <param name="currentDate">Current Date</param>
        /// <returns>
        /// DateTime object
        /// </returns>        
        public DateTime GetDateAfterPostcodeMatrix(DateTime currentDate)
        {
            if (this.PostcodeMatrixList != null && this.PostcodeMatrixList.Any())
            {
                var postCodeMatrix = this.PostcodeMatrixList.First();

                ///if all days are false i.e not set (rare scenario)
                if (postCodeMatrix.Monday == false &&
                    postCodeMatrix.Tuesday == false &&
                    postCodeMatrix.Wednesday == false &&
                    postCodeMatrix.Thursday == false &&
                    postCodeMatrix.Friday == false
                    )
                {
                    return currentDate;
                }

                while (!CheckPostcodeDay(currentDate, postCodeMatrix, false))
                {
                    currentDate = currentDate.AddDays(1);
                }
            }
            return currentDate;
        }


        /// <summary>
        /// Gets the text from transproxy.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns></returns>
        public string GetTextFromTranslation(long translationId, long languageId)
        {
            try
            {
                using (var context = new SCAIndigoEntities())
                {
                    var description = context.GetTextFromTranslation(translationId, languageId).ToList().FirstOrDefault();
                    return description;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /* Please do not delete this code.
        public DateTime GetDateAfterPostcodeMatrix(DateTime currentDate)
        {
            if (this.PostcodeMatrixList != null)
            {
                if (this.PostcodeMatrixList.Any())
                {
                    var postCodeMatrix = this.PostcodeMatrixList.First();

                    ///if all days are false i.e not set (rare scenario)
                    if (postCodeMatrix.Monday == false &&
                        postCodeMatrix.Tuesday == false &&
                        postCodeMatrix.Wednesday == false &&
                        postCodeMatrix.Thursday == false &&
                        postCodeMatrix.Friday == false
                        )
                    {
                        return currentDate;
                    }

                    var datematched = false;

                    while (!datematched)
                    {
                        datematched = CheckPostcodeDay(currentDate, postCodeMatrix, datematched);
                        if (datematched)
                        {
                            datematched = true;
                            return currentDate;
                        }
                        else
                        {
                            datematched = false;
                            currentDate = currentDate.AddDays(1);
                        }
                    }
                }

            }
            return currentDate;
        }
        */


        /// <summary>
        /// Checks the postcode day.
        /// </summary>
        /// <param name="currentDate">The current date.</param>
        /// <param name="postCodeMatrix">The post code matrix.</param>
        /// <param name="datematched">if set to <c>true</c> [datematched].</param>
        /// <returns></returns>
        private static bool CheckPostcodeDay(DateTime currentDate, PostCodeMatrixBusinessModel postCodeMatrix, bool datematched)
        {
            var dayOfWeek = currentDate.DayOfWeek;
            if (postCodeMatrix == null)
            {
                return false;
            }
            if (!datematched && dayOfWeek == DayOfWeek.Monday && postCodeMatrix.Monday == true)
            {
                datematched = true;
            }

            if (!datematched && dayOfWeek == DayOfWeek.Tuesday && postCodeMatrix.Tuesday == true)
            {
                datematched = true;
            }

            if (!datematched && dayOfWeek == DayOfWeek.Wednesday && postCodeMatrix.Wednesday == true)
            {
                datematched = true;
            }

            if (!datematched && dayOfWeek == DayOfWeek.Thursday && postCodeMatrix.Thursday == true)
            {
                datematched = true;
            }

            if (!datematched && dayOfWeek == DayOfWeek.Friday && postCodeMatrix.Friday == true)
            {
                datematched = true;
            }
            return datematched;
        }

        #endregion

        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>
        /// Return list of ListTypeBusinessModel
        /// </returns>
        public List<ListTypeBusinessModel> GetAuthorizeCustomers(string userId)
        {
            try
            {
                var customerDetails = (new DBHelper()).GetAuthoriseCustomers(Guid.Parse(userId));

                var result = customerDetails.Select(item => new ListTypeBusinessModel()
                {
                    ListId = item.CustomerId,
                    DisplayText = item.CustomerName
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="objHolidayBusinessModel"></param>
        /// <returns>
        /// Return list of ListTypeBusinessModel
        /// </returns>
        public string GetArrangedDeliveryDate(HolidayProcessAdminBusinessModels objHolidayBusinessModel)
        {
            var userId = string.Empty;
            try
            {
                if (objHolidayBusinessModel != null)
                {
                    userId = Convert.ToString(objHolidayBusinessModel.UserId, CultureInfo.InvariantCulture);
                    var arrangeDeliveryDate = (new DBHelper()).GetArrangedDeliveryDate(objHolidayBusinessModel.CustomerId, Convert.ToDateTime(objHolidayBusinessModel.DerivedNDD, CultureInfo.InvariantCulture), objHolidayBusinessModel.OrderCreationTime);

                    if (arrangeDeliveryDate != null)
                    {
                        return Convert.ToString(arrangeDeliveryDate.ArrangedNDD, CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Get list of Customers name and id
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>
        /// Return list of Customers
        /// </returns>
        public List<DropDownDataBusinessModel> GetCustomersForDropDown(string userId)
        {
            try
            {
                var customerDetails = (new DBHelper()).GetAuthoriseCustomers(Guid.Parse(userId));
                var result = customerDetails.Select(item => new DropDownDataBusinessModel()
                {
                    Value = item.CustomerId,
                    DisplayText = item.CustomerName
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return null;
        }

        /// <summary>
        /// Get list of Users for interaction report
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>
        /// Return list Users
        /// </returns>
        public List<InteractionReportUsersBusinessModel> GetUsersOnInteractionForDropDown(string userId, string customerIds)
        {
            try
            {
                var interactionReportUsers = (new DBHelper()).GetInteractionReportUsers(customerIds);
                var result = interactionReportUsers.Select(item => new InteractionReportUsersBusinessModel()
                {
                    DisplayText = item.UserName,
                    Value = item.UserId.ToString()
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return null;
        }

        /// <summary>
        /// Get list of careHome name and id
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>
        /// Return list of careHome
        /// </returns>
        public List<DropDownDataBusinessModel> GetCareHomesForDropDown(string userId)
        {
            try
            {
                var customerDetails = (new DBHelper()).GetCareHomeNameIdList(Guid.Parse(userId));
                var result = customerDetails.Select(item => new DropDownDataBusinessModel()
                {
                    Value = item.CareHomeId,
                    DisplayText = item.CareHomeName
                }).ToList();
                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        #region Common method for Export to Excel Functionality
        /// <summary>
        /// Export to Excel functionality.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exportDataList">The export data list.</param>
        /// <param name="serverDirectory">The server directory.</param>
        /// <param name="fileNamePrefix">The file name prefix.</param>
        /// <param name="columnsToRemove">The columns to remove.</param>
        /// <returns>
        /// file path
        /// </returns>
        public static string ExportToExcel<T>(List<T> exportDataList, string massTemplateType, string fileNamePrefix, List<string> columnsToRemove, string mainFolder, bool isSubfolderRequired)
        {
            /// set variables 
            var logDirectory = string.Empty;
            var fileName = string.Empty;

            string serverPath = @"\\" + Path.Combine(ipAddress, sharedLocation, uploadDirectory, mainFolder);
            //If UILogIPAddress is provided then create the log at UI folder else On WCF server
            if (!string.IsNullOrEmpty(UILogIPAddress))
            {
                serverPath = @"\\" + Path.Combine(UILogIPAddress, sharedLocation, uploadDirectory, mainFolder);
            }
            else if (massTemplateType == onlineTelephoneOrderLog)
            {
                serverPath = @"\\" + Path.Combine(ipAddress, scaTaskScheduler, mainFolder);
            }

            /// create directory
            Common.CommonHelper.CreateDirectoryIfNotExists(serverPath);

            if (isSubfolderRequired)
            {
                logDirectory = Path.Combine(serverPath, massTemplateType);
                Common.CommonHelper.CreateDirectoryIfNotExists(logDirectory);

                /// build xls file path                
                fileName = fileNamePrefix + CommonConstants.Underscore + massTemplateType + CommonConstants.Underscore + DateTime.Now.ToString(CommonConstants.DateTimeFormatyyMMddhhmmss, CultureInfo.InvariantCulture) + CommonConstants.ExcelFileFormat;
            }
            else
            {
                logDirectory = Path.Combine(serverPath);
                /// build xls file path
                fileName = fileNamePrefix + CommonConstants.Underscore + DateTime.Now.ToString(CommonConstants.DateTimeFormatyyMMddhhmmss, CultureInfo.InvariantCulture) + CommonConstants.ExcelFileFormat;
            }

            var filePath = Path.Combine(logDirectory, fileName);

            /// create excel document
            CreateExcelDocument(ListToDataTable(exportDataList, columnsToRemove), filePath);

            return filePath;
        }

        /// <summary>
        /// Lists to data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="columnsToRemove">The columns to remove.</param>
        /// <returns></returns>
        public static DataTable ListToDataTable<T>(List<T> list, List<string> columnsToRemove)
        {
            var dt = new DataTable { Locale = CultureInfo.InvariantCulture };
            try
            {
                foreach (var info in typeof(T).GetProperties())
                {
                    dt.Columns.Add(new DataColumn(info.Name, GetNullableType(info.PropertyType)));
                }

                if (list != null && list.Any())
                {
                    foreach (T t in list)
                    {
                        DataRow row = dt.NewRow();
                        foreach (PropertyInfo info in typeof(T).GetProperties())
                        {
                            if (!IsNullableType(info.PropertyType))
                                row[info.Name] = info.GetValue(t, null);
                            else
                                row[info.Name] = (info.GetValue(t, null) ?? DBNull.Value);
                        }
                        dt.Rows.Add(row);
                    }
                }

                if (columnsToRemove != null && columnsToRemove.Count > 0)
                {
                    foreach (var item in columnsToRemove)
                    {
                        dt.Columns.Remove(item);
                    }
                }
                return dt;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
                throw;
            }
        }

        /// <summary>
        /// Get nullable type
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>
        /// nullable type
        /// </returns>
        private static Type GetNullableType(Type t)
        {
            Type returnType = t;
            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                returnType = Nullable.GetUnderlyingType(t);
            }
            return returnType;
        }

        /// <summary>
        /// check if nullable
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// nullable type
        /// </returns>
        private static bool IsNullableType(Type type)
        {
            return (type == typeof(string) ||
                    type.IsArray ||
                    (type.IsGenericType &&
                     type.GetGenericTypeDefinition() == typeof(Nullable<>)));
        }

        #endregion

        #region Create and Write in the excel file

        /// <summary>
        /// Create an Excel file, and write it to a file.
        /// </summary>
        /// <param name="ds">DataSet containing the data to be written to the Excel.</param>
        /// <param name="excelFileName">Name of file to be written.</param>
        /// <returns>
        /// True if successful, false if something went wrong.
        /// </returns>
        public static bool CreateExcelDocument(DataSet ds, string excelFilePath)
        {
            try
            {
                using (var document = SpreadsheetDocument.Create(excelFilePath, SpreadsheetDocumentType.Workbook))
                {
                    WriteExcelFile(ds, document);
                }
                System.Diagnostics.Trace.WriteLine("Successfully created: " + excelFilePath);
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Create an Excel file, and write it to a file.
        /// </summary>
        /// <param name="dataTable">The dt.</param>
        /// <param name="excelFilePath">The XLSX file path.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public static bool CreateExcelDocument(DataTable dataTable, string excelFilePath)
        {
            var ds = new DataSet();
            try
            {
                ds.Locale = CultureInfo.InvariantCulture;
                ds.Tables.Add(dataTable);
                var result = CreateExcelDocument(ds, excelFilePath);
                ds.Tables.Remove(dataTable);
                return result;
            }
            finally
            {
                if (ds != null)
                {
                    ds.Dispose();
                }
            }
            return false;
        }

        /// <summary>
        /// Main function to write data in Excel file
        /// </summary>
        /// <param name="ds">The ds.</param>
        /// <param name="spreadsheet">The spreadsheet.</param>
        private static void WriteExcelFile(DataSet ds, SpreadsheetDocument spreadsheet)
        {
            ///  Create the Excel file contents.  This function is used when creating an Excel file either writing 
            ///  to a file, or writing to a MemoryStream.
            spreadsheet.AddWorkbookPart();
            spreadsheet.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            spreadsheet.WorkbookPart.Workbook.Append(new BookViews(new WorkbookView()));

            ///  If we don't add a "WorkbookStylesPart", OLEDB will refuse to connect to this .xlsx file !
            var workbookStylesPart = spreadsheet.WorkbookPart.AddNewPart<WorkbookStylesPart>("rIdStyles");
            var stylesheet = new Stylesheet();
            workbookStylesPart.Stylesheet = stylesheet;

            ///  Loop through each of the DataTables in our DataSet, and create a new Excel Worksheet for each.
            uint worksheetNumber = 1;
            foreach (DataTable dt in ds.Tables)
            {
                WorksheetPart newWorksheetPart = spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
                newWorksheetPart.Worksheet = new Worksheet();

                /// create sheet data
                newWorksheetPart.Worksheet.AppendChild(new SheetData());

                /// save worksheet
                WriteDataTableToExcelWorksheet(dt, newWorksheetPart);
                newWorksheetPart.Worksheet.Save();

                /// create the worksheet to workbook relation
                if (worksheetNumber == 1)
                    spreadsheet.WorkbookPart.Workbook.AppendChild(new Sheets());

                spreadsheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
                {
                    Id = spreadsheet.WorkbookPart.GetIdOfPart(newWorksheetPart),
                    SheetId = worksheetNumber,
                    Name = dt.TableName
                });

                worksheetNumber++;
            }

            spreadsheet.WorkbookPart.Workbook.Save();
        }

        /// <summary>
        /// Sub function to write data table to excel worksheet.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <param name="worksheetPart">The worksheet part.</param>
        private static void WriteDataTableToExcelWorksheet(DataTable dt, WorksheetPart worksheetPart)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();

            var numericTypes = new[] { typeof(Byte), typeof(Decimal), typeof(Double),
        typeof(Int16), typeof(Int32), typeof(Int64), typeof(SByte),
        typeof(Single), typeof(UInt16), typeof(UInt32), typeof(UInt64)};

            string cellValue = "";

            ///  Create a Header Row in our Excel file, containing one header for each Column of data in our DataTable.            
            ///  We'll also create an array, showing which type each column of data is (Text or Numeric), so when we come to write the actual
            ///  cells of data, we'll know if to write Text values or Numeric cell values.
            var numberOfColumns = dt.Columns.Count;
            var isNumericColumn = new bool[numberOfColumns];

            var excelColumnNames = new string[numberOfColumns];
            for (var n = 0; n < numberOfColumns; n++)
                excelColumnNames[n] = GetExcelColumnName(n);

            //  Create the Header row in our Excel Worksheet
            uint rowIndex = 1;

            var headerRow = new Row { RowIndex = rowIndex };  /// add a row at the top of spreadsheet
            sheetData.Append(headerRow);

            for (var colInx = 0; colInx < numberOfColumns; colInx++)
            {
                DataColumn col = dt.Columns[colInx];
                AppendTextCell(excelColumnNames[colInx] + "1", col.ColumnName, headerRow);
                isNumericColumn[colInx] = numericTypes.Contains(col.DataType);
            }

            ///  Now, step through each row of data in our DataTable...            
            double cellNumericValue = 0;
            foreach (DataRow dr in dt.Rows)
            {
                /// ...create a new row, and append a set of this row's data to it.
                ++rowIndex;
                var newExcelRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
                sheetData.Append(newExcelRow);

                for (int colInx = 0; colInx < numberOfColumns; colInx++)
                {
                    cellValue = dr.ItemArray[colInx].ToString();

                    /// Create cell with data
                    if (isNumericColumn[colInx])
                    {
                        ///  For numeric cells, make sure our input data IS a number, then write it out to the Excel file.
                        ///  If this numeric value is NULL, then don't write anything to the Excel file.
                        cellNumericValue = 0;
                        if (double.TryParse(cellValue, out cellNumericValue))
                        {
                            cellValue = Convert.ToString(cellNumericValue, CultureInfo.InvariantCulture);
                            AppendNumericCell(excelColumnNames[colInx] + Convert.ToString(rowIndex, CultureInfo.InvariantCulture), cellValue, newExcelRow);
                        }
                    }
                    else
                    {
                        ///  For text cells, just write the input data straight out to the Excel file.
                        AppendTextCell(excelColumnNames[colInx] + Convert.ToString(rowIndex, CultureInfo.InvariantCulture), cellValue, newExcelRow);
                    }
                }
            }
        }

        /// <summary>
        /// Append text into cell
        /// </summary>
        /// <param name="cellReference">The cell reference.</param>
        /// <param name="cellStringValue">The cell string value.</param>
        /// <param name="excelRow">The excel row.</param>
        private static void AppendTextCell(string cellReference, string cellStringValue, Row excelRow)
        {
            ///  Add a new Excel Cell to our Row 
            var cell = new Cell() { CellReference = cellReference, DataType = CellValues.String };
            var cellValue = new CellValue { Text = cellStringValue };
            cell.Append(cellValue);
            excelRow.Append(cell);
        }

        /// <summary>
        /// append numeric into cell
        /// </summary>
        /// <param name="cellReference">The cell reference.</param>
        /// <param name="cellStringValue">The cell string value.</param>
        /// <param name="excelRow">The excel row.</param>
        private static void AppendNumericCell(string cellReference, string cellStringValue, Row excelRow)
        {
            ///  Add a new Excel Cell to our Row 
            var cell = new Cell() { CellReference = cellReference, DataType = CellValues.Number };
            var cellValue = new CellValue { Text = cellStringValue };
            cell.Append(cellValue);
            excelRow.Append(cell);
        }

        /// <summary>
        /// Get excel column name
        /// </summary>
        /// <param name="columnIndex">Index of the column.</param>
        /// <returns></returns>
        private static string GetExcelColumnName(int columnIndex)
        {
            ///  Convert a zero-based column index into an Excel column reference  (A, B, C.. Y, Y, AA, AB, AC... AY, AZ, B1, B2..)
            if (columnIndex < 26)
                return ((char)('A' + columnIndex)).ToString(CultureInfo.InvariantCulture);

            var firstChar = (char)('A' + (columnIndex / 26) - 1);
            var secondChar = (char)('A' + (columnIndex % 26));

            return string.Format(CultureInfo.InvariantCulture, "{0}{1}", firstChar, secondChar);
        }

        #endregion

        /// <summary>
        /// Calculate age
        /// </summary>
        /// <param name="dateOfBirth">date Of Birth</param>
        /// <returns>int age</returns>
        public static int GetAge(DateTime dateOfBirth)
        {
            var today = DateTime.Now;
            return ((today.Year - dateOfBirth.Year) * 372 + (today.Month - dateOfBirth.Month) * 31 + (today.Day - dateOfBirth.Day)) / 372;
        }

        /// <summary>
        /// Get List of County
        /// </summary>
        /// <returns>
        /// List of County
        /// </returns>
        public List<CountyBusinessModel> GetCountyList()
        {
            var countyList = new List<CountyBusinessModel>();
			var userId = string.Empty;
            try
            {
                var result = new DBHelper().GetCountyList();
                countyList = result.Select(county => new CountyBusinessModel
                {
                    CountyText = county.County,
                    CountyCode = county.CountyCode
                }).ToList();
            }
            catch (DataException e)
            {
				LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
				LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
				LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
				LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
				LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
				LogHelper.LogException(e, userId);
                throw;
            }
            return countyList;
        }

        /// <summary>
        /// Gets Carehome Users
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<DropDownBusinessModel> GetCareHomeUsersOnCustomerId(string customerId, string userId)
        {
            try
            {
                var userDetails = (new DBHelper()).GetCareHomeUsersOnCustomerId(customerId);
                var result = userDetails.Select(item => new DropDownBusinessModel()
                {
                    Value = Convert.ToString(item.UserId, CultureInfo.InvariantCulture),
                    DisplayText = item.UserName
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return null;
        }

        /// <summary>
        /// Checks wheather user is carehome user or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsCareHomeUser(string userId) 
        {
            var isCareHomeUser = false;
            IUsersRepository userRepository = null;
            try
            {
                userRepository = new UsersRepository();
                IUnitOfWork unitOfWork = userRepository.UnitOfWork;

                if (userRepository.GetUsersByUserId(userId).Where(q => q.IsCarehomeUser).Any())
                    isCareHomeUser = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally 
            {
                if (userRepository != null)
                    userRepository.Dispose();
            }
            return isCareHomeUser;
        }
    }
}