﻿// ***********************************************************************
// <copyright file="SAPServiceHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Security;
    using System.Text;
    using System.Web;
    using System.Web.Hosting;
    using Microsoft.VisualBasic.FileIO;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Model;
    
    /// <summary>
    /// SAP Service Helper Class is used to send service request SAP service
    /// </summary>       
    public class SAPServiceHelper
    {
        /// <summary>
        /// Gets or sets BaseUrl
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Gets or sets ActionName
        /// </summary>
        public string ActionName { get; set; }
       
        /// <summary>
        /// ActionParam
        /// </summary>
        public string[] ActionParam { get; set; }

        /// <summary>
        /// Gets or sets MethodType
        /// </summary>
        public string MethodType { get; set; }

        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets HttpMessage
        /// </summary>
        public string HttpMessage { get; set; }

        /// <summary>
        /// Gets or sets RequestMessage
        /// </summary>
        public string RequestMessage { get; set; }

        /// <summary>
        /// Gets or sets UserName
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>        
        /// Initializes members of the SAP Service Helper
        /// </summary>
        public SAPServiceHelper()
        {
            BaseUrl = Convert.ToString(ConfigurationManager.AppSettings["SAPPIBaseUrl"], CultureInfo.InvariantCulture);
            UserId = Guid.Parse(new DBHelper().GetSAPUserDetails());
            UserName = Convert.ToString(ConfigurationManager.AppSettings["SAPPIUserName"], CultureInfo.InvariantCulture);
            Password = Convert.ToString(ConfigurationManager.AppSettings["SAPPIPassword"], CultureInfo.InvariantCulture);
        }

        /// <summary>        
        /// Initializes members of the SAP Service Helper
        /// </summary>
        public SAPServiceHelper(string actionName)
            : this()
        {
            ActionName = actionName;
            ActionParam = null;
        }

        /// <summary>        
        /// Initializes members of the SAP Service Helper
        /// </summary>
        public SAPServiceHelper(string actionName, string[] actionParam)
            : this(actionName)
        {
            ActionParam = actionParam;
        }

        

        #region PostMethods

        ///// <summary>
        /// Method to post the request.
        /// User only in case if return types are boolean or string or long 
        /// </summary>
        /// <param name="contract">The contract.</param>
        /// <returns>response object</returns>
        public HttpResponseMessage PostMethodService(string contract)
        {
            ServiceHelper objServiceHelper = null;
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            try
            {
                objServiceHelper = new ServiceHelper(this.UserName, this.Password);
                objServiceHelper.RequestUrl = new Uri(this.BaseUrl + "/" + this.ActionName);
                //{ RequestUrl = new Uri(BaseUrl + "/" + ActionName), UserName = this.UserName, Password = this.Password };
                var response = objServiceHelper.GetHttpResponseMessage(contract, UserId, "text/xml");
                endTime = DateTime.Now;
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append(response.RequestMessage.ToString() + "\n");
                sbMessage.Append(response.ToString());
                this.LogToFile(startTime, endTime, sbMessage.ToString(), "HttpRequestLog");
                return response;
            }            
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
                      
        }

        #endregion

        #region PrivateMethods
        private void LogToFile(DateTime startTime, DateTime endTime, string logMessage, string LogFileName)
        {
            // create a writer and open the file
            string strLogFilePath = HostingEnvironment.MapPath("~/SAPFiles/" + LogFileName + ".txt");
            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine("");
                textWriter.WriteLine("+++++++++++ Batch Started at " + startTime + "  +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++ Batch End at " + endTime + "        +++++++++++++++");
            }
        }
        #endregion
    }
}