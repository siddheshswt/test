﻿//----------------------------------------------------------------------------------------------
// <copyright file="AccountManagement.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
	using System;
	using System.Data;
	using System.Globalization;
	using BusinessModels;
	using Interfaces;
	using Common;
	using Model;
	using System.Collections.Generic;
	using SCA.Indigo.Common.Enums;
	using SCA.Indigo.Infrastructure.Repositories;
	using System.Linq;

	/// <summary>
	/// Account Management Business class
	/// </summary>   
	public class NDDCalculation : INDDCalculation
	{
		/// <summary>
		/// Calculate NDD for Community Patients
		/// CD + LT + PM + HL
		/// </summary>
		/// <param name="nddParamaters"></param>
		/// <returns></returns>
		public string GetNDDForNewCommunityPatient(string patientId, string patientType, NDDParametersBusinessModel nddParamaters)
		{
			var response = string.Empty;
			var userId = nddParamaters.UserId;
			try
			{
				
				if (IsNewPatient(Convert.ToInt64(patientId), nddParamaters.RoundID) && IsCommunityPatient(Convert.ToInt64(patientType)))
				{
					response = CalculateNDD(nddParamaters).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture);
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (NullReferenceException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			return response;

		}

		/// <summary>
		/// Calculate NDD for Community Patients
		/// CD + LT + PM + HL
		/// </summary>
		/// <param name="nddParamaters"></param>
		/// <returns></returns>
		public string GetNDDForPatient(NDDParametersBusinessModel nddParamaters)
		{
			var response = string.Empty;
			var userId = nddParamaters.UserId;
			try
			{
				response = CalculateNDD(nddParamaters).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture);
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (NullReferenceException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			return response;

		}

		private bool IsNewPatient(long patientId, string roundId)
		{
			IPatientRepository patientRepository = new PatientRepository();
			var patient = patientRepository.Find(patientId);
			if (patient != null && patient.DeliveryDate == null && roundId == "XXXX")
			{
				return true;
			}
			return false;
		}

		private bool IsCommunityPatient(long patientType)
		{
			var isCommunity = false;
			switch (patientType)
			{
				case (long)SCAEnums.PatientType.SelfCare:
				case (long)SCAEnums.PatientType.PaediatricSelfcare:
				case (long)SCAEnums.PatientType.CommunitySelfcareNursing:
				case (long)SCAEnums.PatientType.CommunitySelfcareResidential:
				case (long)SCAEnums.PatientType.Procare:
				case (long)SCAEnums.PatientType.PaediatricProcare:
				case (long)SCAEnums.PatientType.Bulk:
					isCommunity = true;
					break;
			}
			return isCommunity;
		}

		private int GetLeadTime(long customerId)
		{
			ICustomerParameterRepository customerRepository = new CustomerParameterRepository();
			var customerParameter = customerRepository.GetAllCustomerParametersByCustomerId(customerId).ToList();

			if (customerParameter.Any())
			{
				return customerParameter.FirstOrDefault().OrderLeadTime;
			}
			return 0;
		}

		public DateTime CalculateNDD(NDDParametersBusinessModel nddParamaters)
		{
			var objCommon = new CommonService();
			var customerId = Convert.ToInt64(nddParamaters.CustomerId, CultureInfo.InvariantCulture);
			var leadTime = Convert.ToInt32(nddParamaters.LeadTime, CultureInfo.InvariantCulture);
			if(leadTime == 0)
			{
				leadTime = GetLeadTime(customerId);
			}
			var countryId = 0;
			if (!string.IsNullOrEmpty(nddParamaters.CountryId) && nddParamaters.CountryId != "0")
			{
				countryId = Convert.ToInt32(nddParamaters.CountryId, CultureInfo.InvariantCulture);
			}
			else
			{
				countryId = objCommon.GetCountryId(customerId, nddParamaters.UserId);
			}

			var responseDate = objCommon.AddWorkingDays(DateTime.Now.Date, leadTime, countryId, nddParamaters.UserId);

			List<PostCodeMatrixBusinessModel> lstPostCode = new List<PostCodeMatrixBusinessModel>();

			var postCodeMatrix = objCommon.GetPostcodeMatrix(nddParamaters.CustomerId, nddParamaters.Round, nddParamaters.RoundID, nddParamaters.PostCode, nddParamaters.UserId);
			lstPostCode.Add(postCodeMatrix);
			objCommon.PostcodeMatrixList = lstPostCode;

			while (objCommon.IsHoliday(responseDate = objCommon.GetDateAfterPostcodeMatrix(responseDate), countryId, nddParamaters.UserId))
			{
				responseDate = responseDate.AddDays(1);
			}

			return responseDate;
		}
	}
}