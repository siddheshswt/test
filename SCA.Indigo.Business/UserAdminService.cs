﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Sachin / Basanata
// Created          : 09-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="UserAdminService.cs" company="Cap gemini">
//     Cap gemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;
    using System.ServiceModel;

    /// <summary>
    /// User Admin Service
    /// </summary>    
    public class UserAdminService : IUserAdminService
    {
        /// <summary>
        /// User Id
        /// </summary>
        private string globalUserId = string.Empty;

        /// <summary>
        /// Date Format
        /// </summary>
        protected const string DateFormat = "yyyy-MM-dd";

        /// <summary>
        /// Date Time Format
        /// </summary>
        protected const string DateTimeFormat = "yyyy-MM-dd HH:mm";

        /// <summary>
        /// Single comma
        /// </summary>
        private const string SingleComma = ",";

        /// <summary>
        /// Map Roles Menu Id
        /// </summary>
        private const string MapRolesMenuId = "MapRolesMenuId";

        /// <summary>
        /// About menu id
        /// </summary>
        private const long AboutMenuId = 7;

        /// <summary>
        /// Home menu id
        /// </summary>
        private const long HomeMenuId = 1;

        /// <summary>
        /// Search menu id
        /// </summary>
        private const long SearchMenuId = 2;

        /// <summary>
        /// AuthorisationPrescription menu id
        /// </summary>
        private const long AuthorisationPrescription = 5;

        #region Role User Menu functionlity

        /// <summary>
        /// Get all roles
        /// </summary>
        /// <returns>
        /// list of value
        /// </returns>
        public List<UsersBusinessModel> GetAllRoles()
        {
            string userId = string.Empty;

            IRoleRepository roleRepository = null;
            List<UsersBusinessModel> allroles = new List<UsersBusinessModel>();
            try
            {
                roleRepository = new RoleRepository();
                List<Role> roles = null;
                roles = roleRepository.All.Where(m => m.RoleName.Trim().Length > 0).ToList();
                roles.ForEach(
                    d =>
                    {
                        allroles.Add(new UsersBusinessModel() { RoleId = d.RoleId, RoleName = string.Format(CultureInfo.InvariantCulture, "{0} ( {1} )", d.RoleName, d.UserType) });
                    });
                allroles.ForEach(d => d.RoleName = d.RoleName.Trim());

                allroles = allroles.OrderBy(m => m.RoleName).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (roleRepository != null)
                {
                    roleRepository.Dispose();
                }
            }

            return allroles;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="customerUser"></param>
        /// <param name="userId"></param>
        /// <param name="isInteraction"></param>
        /// <returns>
        /// list of users
        /// </returns>
        public List<UsersBusinessModel> GetAllUsers(string customerId, string customerUser, string userId, string isInteraction = "false")
        {
            IIDXUserCustomerRepository userCustomerRepository = null;
            IUsersRepository usersRepository = null;
            List<UsersBusinessModel> allUser = new List<UsersBusinessModel>();
            try
            {
                userCustomerRepository = new IDXUserCustomerRepository();
                usersRepository = new UsersRepository();
                long customer = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                if (Convert.ToBoolean(customerUser, CultureInfo.InvariantCulture))
                {
                    var calledFromInteraction = Convert.ToBoolean(isInteraction, CultureInfo.InvariantCulture);
                    List<IDXUserCustomer> allUserCustomer = userCustomerRepository.All.Where(d => d.CustomerId == customer && d.User.IsActive).OrderBy(d=>d.User.UserName).ToList();
                    if (!calledFromInteraction)///this for customer information page
                    {
                        allUserCustomer = allUserCustomer.Where(p => p.User.AuthorizationPIN != null).ToList();
                    }

                    allUserCustomer.ForEach(
                        a =>
                        {
                            allUser.Add(new UsersBusinessModel() { UserId = a.UserId, UserName = a.User.UserName });
                        });
                }
                else
                {
                    /// return all users except SapUser and System User
                    Guid systemUser = Guid.Parse(CommonConstants.SystemUser);
                    Guid sapUser = Guid.Parse(CommonConstants.SAPUser);

                    List<Users> users = usersRepository.All.Where(d=> d.UserId != systemUser && d.UserId != sapUser && d.IsActive && !d.IsCarehomeUser).OrderBy(a=>a.UserName).ToList();
                    users.ForEach(
                        d =>
                        {
                            allUser.Add(new UsersBusinessModel() { UserId = d.UserId, UserName = d.UserName });
                        });
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (userCustomerRepository != null)
                {
                    userCustomerRepository.Dispose();
                }
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }

            return allUser;
        }

        /// <summary>
        /// Get all users customer mapping
        /// </summary>
        /// <param name="userName">User's name</param>
        /// <param name="roleId">User's Role Id</param>
        /// <param name="sapCustomerId">Customer's SAP Id</param>
        /// <returns>
        /// List of values
        /// </returns>
        public List<UserMenuDetailsBusinessModel> GetAllUserCustomerMapping(string userName, string roleId, string sapCustomerId)
        {
            userName = userName == "NULL" ? string.Empty : userName;
            sapCustomerId = sapCustomerId == "NULL" ? string.Empty : sapCustomerId;
            IRoleRepository roleRepository = new RoleRepository();
            List<UserMenuDetailsBusinessModel> allUserCustomerMapping = new List<UserMenuDetailsBusinessModel>();

            List<UserMenuDetailsBusinessModel> resultUserCustomerMapping = new List<UserMenuDetailsBusinessModel>();
            try
            {
                var bindGridUserCustomer = new DBHelper().GetUserRoleCustomerMapping(string.Empty, string.Empty).ToList();

                /// Removed all Proceeding Zeros from SAPCustomerId
                bindGridUserCustomer.ForEach(d =>
                   {
                       d.SAPCustomerNumber = !string.IsNullOrEmpty(d.SAPCustomerNumber) ? d.SAPCustomerNumber.TrimStart(new char[] { '0' }) : string.Empty;
                   });

                /// Fill the Business Model as per Given Data
                bindGridUserCustomer.ForEach(d =>
                {
                    allUserCustomerMapping.Add(new UserMenuDetailsBusinessModel()
                        {
                            SAPCustomerID = string.Join(", ", bindGridUserCustomer.FindAll(e => e.UserId == d.UserId && !string.IsNullOrEmpty(e.SAPCustomerNumber)).Select(k => k.SAPCustomerNumber)),
                            UserName = d.UserName,
                            UserID = d.UserId.ToString(),
                            IsRoleAssigned = false,
                            RoleID = Convert.ToInt64(d.RoleID, CultureInfo.InvariantCulture)
                        });
                });

                /// Filter for duplicate rows
                allUserCustomerMapping.ForEach(d =>
                {
                    if (d.UserID.ToUpper() != CommonConstants.SystemUser.ToUpper() && d.UserID.ToUpper() != CommonConstants.SAPUser.ToUpper())
                    {
                        if (resultUserCustomerMapping.Find(q => q.UserID == d.UserID.ToString()) == null)
                        {
                            resultUserCustomerMapping.Add(d);
                        }
                    }
                    
                });

                //Get All Roles
                List<Role> roles = null;
                roles = roleRepository.All.Where(m => m.RoleName.Trim().Length > 0).ToList();

                Role currentRole = null;
                /// Remove Trailing Comma from SAP Customer Id
                resultUserCustomerMapping.ForEach(w =>
                {
                    currentRole = roles.Where(d => d.RoleId == w.RoleID).FirstOrDefault();
                    if (currentRole != null)
                    {
                        w.RoleName = string.Format(CultureInfo.InvariantCulture, "{0} ( {1} )", currentRole.RoleName, currentRole.UserType);
                    }
                    if (w.SAPCustomerID == SingleComma)
                    {
                        w.SAPCustomerID = string.Empty;
                    }
                });

                if (!string.IsNullOrEmpty(userName))
                {
					resultUserCustomerMapping = resultUserCustomerMapping.FindAll(d => d.UserName.ToUpper() == userName.ToUpper());    
                }
                
                if (!string.IsNullOrEmpty(sapCustomerId))
                {
					resultUserCustomerMapping = resultUserCustomerMapping.FindAll(d => d.SAPCustomerID.ToUpper().Contains(sapCustomerId.ToUpper()));    
                }
                resultUserCustomerMapping = resultUserCustomerMapping.OrderBy(d => d.UserName).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (roleRepository != null)
                {
                    roleRepository.Dispose();
                }
            }

            return resultUserCustomerMapping;
        }

        /// <summary>
        /// Get Menu Details
        /// </summary>
        /// <param name="roleId">User's Role Id</param>
        /// <returns>
        /// list of value
        /// </returns>
        public List<UserMenuDetailsBusinessModel> GetMenuDetails(string roleId, string languageId)
        {
            long longLanguageId = Convert.ToInt64(languageId, CultureInfo.InvariantCulture);
            List<UserMenuDetailsBusinessModel> userMenuDetailsBusinessModel = new List<UserMenuDetailsBusinessModel>();
            IDXRoleMenuRepository idxRoleMenuRepository = null;
            long dbRolId = Convert.ToInt64(roleId, CultureInfo.InvariantCulture);
            try
            {
                idxRoleMenuRepository = new IDXRoleMenuRepository();
                new DBHelper().GetMenuDetails(longLanguageId).ForEach(d =>
                {
                    userMenuDetailsBusinessModel.Add(new UserMenuDetailsBusinessModel()
                    {
                        MenuId = d.MenuId,
                        SubMenuId = d.SubMenuID,
                        MenuName = string.Format(CultureInfo.InvariantCulture, "{0} {1}", d.MenuName, (!string.IsNullOrEmpty(d.SubMenuName)) ? (" - " + d.SubMenuName) : string.Empty),
                    });
                });

                /// Get the MenuId from Config and set the disable property to True
                int userRoleMappingMenuId = Convert.ToInt32(ConfigurationManager.AppSettings[MapRolesMenuId], CultureInfo.InvariantCulture);

                if (dbRolId > 0)
                {
                    /// Get IDXRoleMenu 
                    List<IDXRoleMenu> roleMenuList = idxRoleMenuRepository.All.Where(d => d.RoleID == dbRolId).ToList();

                    userMenuDetailsBusinessModel.ForEach(d =>
                    {
                        roleMenuList.ForEach(e =>
                        {
                            if (d.SubMenuId == e.MenuID)
                            {
                                d.FullRights = Convert.ToBoolean(e.FullRights, CultureInfo.InvariantCulture);
                                d.ViewOnly = Convert.ToBoolean(e.ViewOnly, CultureInfo.InvariantCulture);
                                d.IDXRoleMenuID = e.IDXRoleMenuID;
                            }
                        });
                    });

                    /// Disable the VewOnly and Fullrights checkbox for User role mapping menu on Admin role
                    if (dbRolId == 1 || dbRolId == 2)
                    {
                        if (userMenuDetailsBusinessModel.Find(d => d.SubMenuId == userRoleMappingMenuId) != null)
                        {
                            userMenuDetailsBusinessModel.Find(d => d.SubMenuId == userRoleMappingMenuId).DisableMapRoleMenu = true;
                        }
                    }

                    /// Disable the Home and About Us menu
                    userMenuDetailsBusinessModel.ForEach(d =>
                    {
                        if (d.MenuId == AboutMenuId || d.MenuId == HomeMenuId)
                        {
                            d.FullRights = true;
                            d.DisableMapRoleMenu = true;
                        }
                        if (d.MenuId == SearchMenuId || d.MenuId == AuthorisationPrescription)
                        {
                            d.ViewOnly = false;
                            d.DisableViewOnlyMenu = true;
                        }
                    });
                }

                userMenuDetailsBusinessModel = userMenuDetailsBusinessModel.OrderBy(d => d.MenuName).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxRoleMenuRepository != null)
                {
                    idxRoleMenuRepository.Dispose();
                }
            }

            return userMenuDetailsBusinessModel;
        }

        /// <summary>
        /// Save New role
        /// </summary>
        /// <param name="usersBusinessModel">The users business model.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SaveNewRole(UsersBusinessModel usersBusinessModel)
        {
            bool status = false;
            IRoleRepository iRoleRepository = null;

            Role newRole = new Role()
            {
                RoleName = usersBusinessModel.RoleName,
                UserType = usersBusinessModel.UserType,
                ModifiedBy = usersBusinessModel.UserId,
                ModifiedDate = DateTime.Now
            };

            try
            {
                iRoleRepository = new RoleRepository();
                UnitOfWork unitOfWork = iRoleRepository.UnitOfWork;
                if (iRoleRepository.GetAllRoles().ToList().Find(
                        d => d.RoleName.ToUpper(CultureInfo.InvariantCulture) == usersBusinessModel.RoleName.ToUpper(CultureInfo.InvariantCulture)
                    && d.UserType.ToUpper(CultureInfo.InvariantCulture) == usersBusinessModel.UserType.ToUpper(CultureInfo.InvariantCulture)
                            ) == null)
                {
                    iRoleRepository.InsertOrUpdate(newRole);
                    unitOfWork.Commit();

                    SaveNewRoleMenuMapping(newRole.RoleId, usersBusinessModel.UserId);

                    status = true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (iRoleRepository != null)
                {
                    iRoleRepository.Dispose();
                }
            }
            return status;
        }

        /// <summary>
        /// Save new role menu mapping
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="userIdForRole">The user identifier for role.</param>
        /// <returns>
        /// true or false
        /// </returns>
        private bool SaveNewRoleMenuMapping(long roleId, Guid userIdForRole)
        {
            bool status = false;
            IIDXRoleMenuRepository idxRoleMenuRepository = new IDXRoleMenuRepository();
            IUnitOfWork unitOfWork = idxRoleMenuRepository.UnitOfWork;
            try
            {
                /// Insert Home Menu Mapping
                idxRoleMenuRepository.InsertOrUpdate(new IDXRoleMenu()
                  {
                      RoleID = roleId,
                      MenuID = 1,
                      FullRights = true,
                      ViewOnly = false,
                      ModifiedBy = userIdForRole,
                      ModifiedDate = DateTime.Now
                  });

                /// Insert Contact Us Mapping
                idxRoleMenuRepository.InsertOrUpdate(new IDXRoleMenu()
                {
                    RoleID = roleId,
                    MenuID = 7,
                    FullRights = true,
                    ViewOnly = false,
                    ModifiedBy = userIdForRole,
                    ModifiedDate = DateTime.Now
                });

                /// Insert Search Mapping
                idxRoleMenuRepository.InsertOrUpdate(new IDXRoleMenu()
                {
                    RoleID = roleId,
                    MenuID = 2,
                    FullRights = true,
                    ViewOnly = false,
                    ModifiedBy = userIdForRole,
                    ModifiedDate = DateTime.Now
                });

                unitOfWork.Commit();
                status = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxRoleMenuRepository != null)
                {
                    idxRoleMenuRepository.Dispose();
                }
            }

            return status;
        }

        /// <summary>
        /// Save Role Menu Mapping
        /// </summary>
        /// <param name="userAdminBusinessModel">User's detail in UserAdminBusinessModel object</param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SaveRoleMenuMapping(UserAdminBusinessModel userAdminBusinessModel)
        {
            bool isSuccess = false;
            IIDXRoleMenuRepository idxRoleMenuRepository = new IDXRoleMenuRepository();
            IUnitOfWork unitOfWork = idxRoleMenuRepository.UnitOfWork;

            if (userAdminBusinessModel == null)
            {
                return false;
            }

            long roleId = Convert.ToInt64(userAdminBusinessModel.RoleId, CultureInfo.InvariantCulture);
            List<long> parentMenuIds = new List<long>();
            List<IDXRoleMenu> roleMenuMasterList = new List<IDXRoleMenu>();

            try
            {
                if (userAdminBusinessModel == null)
                {
                    return isSuccess;
                }

                roleMenuMasterList = idxRoleMenuRepository.GetAllRoleMenu().ToList();

                foreach (var roleMenuMapping in userAdminBusinessModel.userMenuBusinessModel)
                {
                    /// Save RoleMenu Mapping in IDXRoleMenu
                    /// First check if exist the mapping then Update else insert
                    /// IDXRoleMenuId ==0
                    if (roleMenuMapping.IDXRoleMenuID > 0)
                    {
                        if (roleMenuMapping.MenuId != roleMenuMapping.SubMenuId)
                        {
                            parentMenuIds.Add(roleMenuMapping.MenuId);
                        }

                        /// Update the IDXRoleMenu table
                        IDXRoleMenu idxRoleMenu = idxRoleMenuRepository.GetAllRoleMenu().ToList().Find(d => d.IDXRoleMenuID == roleMenuMapping.IDXRoleMenuID);
                        idxRoleMenu.RoleID = roleId;
                        idxRoleMenu.MenuID = Convert.ToInt64(roleMenuMapping.SubMenuId, CultureInfo.InvariantCulture);
                        idxRoleMenu.FullRights = roleMenuMapping.FullRights;
                        idxRoleMenu.ViewOnly = roleMenuMapping.ViewOnly;
                        idxRoleMenu.ModifiedBy = Guid.Parse(userAdminBusinessModel.UserID);
                        idxRoleMenu.ModifiedDate = DateTime.Now;

                        idxRoleMenuRepository.InsertOrUpdate(idxRoleMenu);
                        idxRoleMenu = null;
                    }
                    else
                    {
                        /// IF Any of FullRights or View Only selected then Insert new records for that.
                        if (roleMenuMapping.FullRights || roleMenuMapping.ViewOnly)
                        {
                            /// Check if its SubMenu and its Parent entry in IDXRoleMenu Table
                            /// Need to insert the Parent menu into the table IDXRoleMenu table
                            if (roleMenuMapping.MenuId != roleMenuMapping.SubMenuId)
                            {
                                parentMenuIds.Add(roleMenuMapping.MenuId);

                                /// Check with the Offline List
                                if (!(roleMenuMasterList.Where(d => d.MenuID == roleMenuMapping.MenuId && d.RoleID == roleId).Any()))
                                {
                                    /// Insert for Parent Menu Id
                                    IDXRoleMenu idxRoleMenu = new IDXRoleMenu()
                                        {
                                            RoleID = roleId,
                                            MenuID = Convert.ToInt64(roleMenuMapping.MenuId, CultureInfo.InvariantCulture),
                                            FullRights = true,
                                            ViewOnly = roleMenuMapping.ViewOnly,
                                            ModifiedBy = Guid.Parse(userAdminBusinessModel.UserID),
                                            ModifiedDate = DateTime.Now
                                        };
                                    idxRoleMenuRepository.InsertOrUpdate(idxRoleMenu);

                                    /// Add into the Offline List
                                    roleMenuMasterList.Add(idxRoleMenu);
                                }
                            }

                            /// Insert new record for that roleId and MenuId with FullRights,ViewOnly
                            idxRoleMenuRepository.InsertOrUpdate(
                            new IDXRoleMenu()
                            {
                                RoleID = roleId,
                                MenuID = Convert.ToInt64(roleMenuMapping.SubMenuId, CultureInfo.InvariantCulture),
                                FullRights = roleMenuMapping.FullRights,
                                ViewOnly = roleMenuMapping.ViewOnly,
                                ModifiedBy = Guid.Parse(userAdminBusinessModel.UserID),
                                ModifiedDate = DateTime.Now
                            });
                        }
                    }
                }

                unitOfWork.Commit();

                /// Check for Parent Updatation
                /// If any of the Parent Menu has no ViewOnly or FullRights submenu in IDXRoleMenu Table, Then we have to remove that row for IDXRoleMenu Table
                /// If any of the Parent Menu has ViewOnly or Full Rights with the submenu in IDXRoleMenu, then we have to add the row into the IDX Role Menu Table

                parentMenuIds = parentMenuIds.Distinct().ToList();
                parentMenuIds.ForEach(d =>
                {
                    IDXRoleMenu parentRoleMenu = roleMenuMasterList.Where(q => q.RoleID == roleId && q.MenuID == d).FirstOrDefault();
                    var idxRoleMenuParentList = new DBHelper().GetRoleMenuBy((long)d, (long)roleId);

                    if (parentRoleMenu != null)
                    {
                        /// If Sub Menu has either Viewoly OR Full Rights checked 
                        if (idxRoleMenuParentList.Count > 0)
                        {
                            parentRoleMenu.FullRights = true;
                            parentRoleMenu.ViewOnly = false;
                        }

                        /// If both View Only and Full Rights are not checked
                        else if (parentRoleMenu.IDXRoleMenuID != 0)
                        {
                            parentRoleMenu.FullRights = false;
                            parentRoleMenu.ViewOnly = false;
                        }

                        idxRoleMenuRepository.InsertOrUpdate(parentRoleMenu);
                    }
                });
                unitOfWork.Commit();
                isSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxRoleMenuRepository != null)
                {
                    idxRoleMenuRepository.Dispose();
                }
            }

            return isSuccess;
        }

        /// <summary>
        /// Save User Role Mapping
        /// </summary>
        /// <param name="userAdminBusinessModel">User's detail in UserAdminBusinessModel object</param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SaveUserRoleMapping(UserAdminBusinessModel userAdminBusinessModel)
        {
            bool isSuccess = false;
            IIDXUserRoleRepository iIDXUserRoleRepository = null;

            if (userAdminBusinessModel == null)
            {
                return isSuccess;
            }

            long roleId = Convert.ToInt64(userAdminBusinessModel.RoleId, CultureInfo.InvariantCulture);

            try
            {
                iIDXUserRoleRepository = new IDXUserRoleRepository();
                IUnitOfWork unitOfWork = iIDXUserRoleRepository.UnitOfWork;
                foreach (var userRoleMapping in userAdminBusinessModel.userMenuBusinessModel)
                {
                    /// Get UserRole mapping by Role Id and User Id
                    IDXUserRole idxUserRole = iIDXUserRoleRepository.GetUserRoleByRoleId(roleId).Where(d => d.UserID.ToString() == userRoleMapping.UserID).FirstOrDefault();

                    /// Save User Role Mapping in IDXRoleMenu
                    /// Checked
                    /// IsRoleAssigned is not assigned (Unchecked)
                    if (userRoleMapping.IsRoleAssigned)
                    {
                        if (idxUserRole == null)
                        {
                            /// Update the Role Id for existing user
                            IDXUserRole idxUserRoleByUserId = iIDXUserRoleRepository.GetUserRoleByUserId(Guid.Parse(userRoleMapping.UserID)).FirstOrDefault();
                            if (idxUserRoleByUserId != null)
                            {
                                idxUserRoleByUserId.RoleID = roleId;
                                idxUserRoleByUserId.UserID = Guid.Parse(userRoleMapping.UserID);
                                idxUserRoleByUserId.IsActive = true;
                                idxUserRoleByUserId.ModifiedDate = DateTime.Now;
                                iIDXUserRoleRepository.InsertOrUpdate(idxUserRoleByUserId);
                            }
                            else
                            {
                                /// For New Entry with Role Id and User Id
                                /// Insert into User Role Mapping
                                iIDXUserRoleRepository.InsertOrUpdate(
                                new IDXUserRole()
                                {
                                    RoleID = roleId,
                                    UserID = Guid.Parse(userRoleMapping.UserID),
                                    ModifiedBy = Guid.Parse(userAdminBusinessModel.UserID),
                                    ModifiedDate = DateTime.Now,
                                    IsActive = true
                                });
                            }
                        }
                        else
                        {
                            if (!Convert.ToBoolean(idxUserRole.IsActive, CultureInfo.InvariantCulture))
                            {
                                idxUserRole.IsActive = true;
                                idxUserRole.ModifiedDate = DateTime.Now;
                                iIDXUserRoleRepository.InsertOrUpdate(idxUserRole);
                            }
                        }
                    }
                    else
                    {
                        /// Remove existing Entry
                        if (idxUserRole != null)
                        {
                            /// Delete from IDXUserRole Table
                            idxUserRole.IsActive = false;
                            idxUserRole.ModifiedDate = DateTime.Now;
                            iIDXUserRoleRepository.InsertOrUpdate(idxUserRole);
                        }
                    }
                }

                unitOfWork.Commit();
                isSuccess = true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (iIDXUserRoleRepository != null)
                {
                    iIDXUserRoleRepository.Dispose();
                }
            }

            return isSuccess;
        }

        #endregion

        #region User to Customer Mapping

        /// <summary>
        /// Get user to customers
        /// </summary>
        /// <param name="userId">User Id to get Mapped Customers</param>
        /// <returns>
        /// list of users
        /// </returns>
        public List<UsersBusinessModel> GetMappedUserToCustomer(string userId)
        {
            var result = new List<UsersBusinessModel>();
            try
            {
                var customerDetails = (new DBHelper()).GetAuthoriseCustomers(Guid.Parse(userId));

                result = customerDetails.Select(item => new UsersBusinessModel()
                {
                    CustomerId = item.CustomerId,
                    CustomerName = item.CustomerName.Split('(')[0].Trim(),
                    SAPCustomerNumber = item.CustomerName.Split('(')[1].Split(')')[0]
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }

            return result;
        }

        /// <summary>
        /// Get admin user id to load all customers first time.
        /// </summary>
        /// <param name="userBusinessModel">Object of User Business Model</param>
        /// <returns>
        /// user model
        /// </returns>
        public UsersBusinessModel GetAdminUserId(UsersBusinessModel userBusinessModel)
        {
            IUsersRepository usersRepository = new UsersRepository();

            try
            {
                if (userBusinessModel == null)
                {
                    return null;
                }

                var objUsersBusinessModel = new UsersBusinessModel();
                var objUsers = usersRepository.GetUsersByUserName(userBusinessModel.UserName);
                objUsersBusinessModel.UserId = objUsers.FirstOrDefault().UserId;
                return objUsersBusinessModel;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Save User customer mappings.
        /// </summary>
        /// <param name="userAdminBusinessModel">User Admin Business Model</param>
        /// <returns>
        /// number of available customers
        /// </returns>
        public bool SaveUserCustomerMapping(UserAdminBusinessModel userAdminBusinessModel)
        {
            IIDXUserCustomerRepository idxUserCustomerRepository = null;

            if (userAdminBusinessModel == null)
            {
                return false;
            }

            Guid userId = Guid.Parse(userAdminBusinessModel.UserID);
            try
            {
                idxUserCustomerRepository = new IDXUserCustomerRepository();
                IUnitOfWork unitOfWork = idxUserCustomerRepository.UnitOfWork;
                /// Delete existing customers for user.                
                var idxUserCustomerList = idxUserCustomerRepository.All.Where(q => q.UserId == userId);
                foreach (var userCustomer in idxUserCustomerList)
                {
                    idxUserCustomerRepository.Delete(userCustomer.IDXUserCustomerId);
                }

                unitOfWork.Commit();

                if (userAdminBusinessModel.UsersBusinessModel != null)
                {
                    foreach (var customer in userAdminBusinessModel.UsersBusinessModel)
                    {
                        /// Insert all mapped customers for the user.
                        IDXUserCustomer UserCustomer = new IDXUserCustomer()
                        {
                            UserId = userId,
                            CustomerId = (long)customer.CustomerId,
                            ModifiedBy = userId,
                            ModifiedDate = DateTime.Now
                        };
                        idxUserCustomerRepository.InsertOrUpdate(UserCustomer);
                    }

                    unitOfWork.Commit();
                }

                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxUserCustomerRepository != null)
                {
                    idxUserCustomerRepository.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Save customers by user mapping
        /// </summary>
        /// <param name="userAdminBusinessModel">The user admin business model.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool SaveCustomerByUserMapping(UserAdminBusinessModel userAdminBusinessModel)
        {
            IIDXUserCustomerRepository idxUserCustomerRepository = null;

            if (userAdminBusinessModel == null)
            {
                return false;
            }

            var customerId = userAdminBusinessModel.CustomerId;
            var modifiedUserId = Guid.Parse(userAdminBusinessModel.UserID);


            try
            {
                idxUserCustomerRepository = new IDXUserCustomerRepository();
                IUnitOfWork unitOfWork = idxUserCustomerRepository.UnitOfWork;
                /// Delete existing customers for user.                
                var idxUserCustomerList = idxUserCustomerRepository.All.Where(q => q.CustomerId == customerId);
                foreach (var userToCustomer in idxUserCustomerList)
                {
                    idxUserCustomerRepository.Delete(userToCustomer.IDXUserCustomerId);
                }

                unitOfWork.Commit();

                IDXUserCustomer userCustomer = null;
                foreach (var customer in userAdminBusinessModel.UsersBusinessModel)
                {
                    /// Insert all mapped customers for the user.
                    userCustomer = new IDXUserCustomer()
                    {
                        UserId = customer.UserId,
                        CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture),
                        ModifiedBy = modifiedUserId,
                        ModifiedDate = DateTime.Now
                    };
                    idxUserCustomerRepository.InsertOrUpdate(userCustomer);
                }

                unitOfWork.Commit();
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxUserCustomerRepository != null)
                {
                    idxUserCustomerRepository.Dispose();
                }
            }

            return false;
        }

        #endregion

        #region GetCustomerDetails
        /// <summary>
        /// Get Customer Details
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>
        /// list of values
        /// </returns>
        public List<HolidayProcessAdminDetails> GetCustomerDetails(string customerId)
        {
            IHolidayProcessAdminRepository holidayProcessAdminRepository = null;
            var result = new List<HolidayProcessAdminDetails>();

            try
            {
                holidayProcessAdminRepository = new HolidayProcessAdminRepository();
                var objCustomers = holidayProcessAdminRepository.GetDetailById(customerId);

                result = objCustomers.Select(item => new HolidayProcessAdminDetails()
                {
                    StartTime = item.StartTime.ToString(),
                    EndTime = item.EndTime.ToString(),
                    NDD = item.NDD.ToString(),
                    ArrangedNDD = item.ArrangedNDD.ToString(),
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (holidayProcessAdminRepository != null)
                {
                    holidayProcessAdminRepository.Dispose();
                }
            }

            return result;
        }
        #endregion

        #region Holiday Process Admin
        /// <summary>
        /// Save Holiday Admin Data
        /// </summary>
        /// <param name="holidayProcessAdminBusinessModels"></param>
        /// <returns>
        /// return true or false
        /// </returns>
        public bool SaveHolidayProcessData(HolidayProcessAdminBusinessModels holidayProcessAdminBusinessModels)
        {
            IHolidayProcessAdminRepository holidayProcessAdminRepository = new HolidayProcessAdminRepository();
            IUnitOfWork unitOfWork = holidayProcessAdminRepository.UnitOfWork;

            try
            {
                globalUserId = holidayProcessAdminBusinessModels.UserID;
                Guid _userId = Guid.Parse(globalUserId);

                /// Delete existing customers for user.                                
                var holidayProcessAdmin = holidayProcessAdminRepository.All.Where(q => q.CustomerId == holidayProcessAdminBusinessModels.CustomerId);
                foreach (var objCust in holidayProcessAdmin)
                {
                    holidayProcessAdminRepository.Delete(objCust.HolidayProcessID);
                }

                unitOfWork.Commit();

                if (holidayProcessAdminBusinessModels.HolidayProcessAdminDetails != null)
                {
                    HolidayProcessAdmin holidayProcessAdminObj = null;
                    DateTime startTime = new DateTime();
                    DateTime endTime = new DateTime();
                    DateTime modifiedDate = DateTime.Now;

                    foreach (var customer in holidayProcessAdminBusinessModels.HolidayProcessAdminDetails)
                    {
                        startTime = DateTime.ParseExact(customer.StartTime, DateTimeFormat, null);
                        endTime = DateTime.ParseExact(customer.EndTime, DateTimeFormat, null);

                        /// Insert all mapped customers for the user.
                        holidayProcessAdminObj = new HolidayProcessAdmin()
                        {
                            CustomerId = holidayProcessAdminBusinessModels.CustomerId,
                            NDD = DateTime.ParseExact(customer.NDD, DateFormat, CultureInfo.InvariantCulture),
                            ArrangedNDD = DateTime.ParseExact(customer.ArrangedNDD, DateFormat, CultureInfo.InvariantCulture),
                            StartTime = startTime,
                            EndTime = endTime,
                            ModifiedBy = _userId,
                            ModifiedDate = modifiedDate
                        };

                        holidayProcessAdminRepository.InsertOrUpdate(holidayProcessAdminObj);
                    }

                    unitOfWork.Commit();
                }

                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (holidayProcessAdminRepository != null)
                {
                    holidayProcessAdminRepository.Dispose();
                }
            }

            return false;
        }
        #endregion

        #region User Carehome

        /// <summary>
        /// Save User carehome mappings.
        /// </summary>
        /// <param name="userCarehomeBusinessModel">User Carehome Business Model</param>
        public bool SaveUserCareHomeMapping(UserCareHomeBusinessModel userCareHomeBusinessModel)
        {
            if (userCareHomeBusinessModel == null)
            {
                return false;
            }

            IIDXUserCareHomeRepository idxUserCareHomeRepository = null;
            IDXUserCarehome userCarehome = null;
            this.globalUserId = userCareHomeBusinessModel.UserId;

            try
            {
                idxUserCareHomeRepository = new IDXUserCareHomeRepository();
                IUnitOfWork unitOfWork = idxUserCareHomeRepository.UnitOfWork;

                var userIds = userCareHomeBusinessModel.UserIds.Split(',').ToList();
                var carehomeIds = userCareHomeBusinessModel.CareHomeId.Split(',').ToList();

                foreach (var userId in userIds)
                {
                    foreach (var carehomeId in carehomeIds)
                    {
                        userCarehome = idxUserCareHomeRepository.FindByUserIdCareHomeId(userId, carehomeId);
                        if (userCarehome == null)
                        {
                            userCarehome = new IDXUserCarehome()
                            {
                                UserId = Guid.Parse(userId),
                                CarehomeId = Convert.ToInt64(carehomeId, CultureInfo.InvariantCulture),
                                ModifiedBy = Guid.Parse(this.globalUserId),
                                ModifiedDate = DateTime.Now
                            };
                            idxUserCareHomeRepository.InsertOrUpdate(userCarehome);
                        }
                    }
                }
                unitOfWork.Commit();

                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxUserCareHomeRepository != null)
                {
                    idxUserCareHomeRepository.Dispose();
                }
            }
            return false;
        }

        #endregion
    }
}
