﻿//----------------------------------------------------------------------------------------------
// <copyright file="IncrementalUpdateSearchIndex.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Configuration;
    using System.ServiceModel;

    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using System.IO;
    using DocumentFormat.OpenXml;

    using BusinessModels;
    using Interfaces;
    using Common;
    using Infrastructure.Repositories;
    using Model;
    using Common.Enums;
    using System.Xml.Linq;
    using System.Xml;
    using System.Text;
    using SCA.Indigo.Infrastructure.Repositories.CustomRepositories;

    /// <summary>
    /// Incremental Update Search Index Business Class
    /// </summary>    
    public class IncrementalUpdateSearchIndex
    {

        private readonly string ipAddress = ConfigurationManager.AppSettings["IPAddress"];
        private readonly string scaTaskScheduler = ConfigurationManager.AppSettings["ServerAddress"];
        private readonly string incrementalUpdateSearchIndexLog = ConfigurationManager.AppSettings["IncrementalUpdateSearchIndexLog"];
        private readonly string archivalDirectoryLog = ConfigurationManager.AppSettings["ArchivePath"];
        private string xmlFileNamePrefix;

        /// <summary>
        /// Create XML for Incremental Update
        /// </summary>
        /// <param name="indigoId">IndigoId</param>
        /// <param name="objectType">ObjectType</param>
        public void CreateIncrementalUpdateIndexXml(long indigoId, long objectType)
        {
            try
            {
                /// set variables 
                var logDirectory = string.Empty;
                var fileName = string.Empty;
                xmlFileNamePrefix = Convert.ToString(indigoId) + "_" + Convert.ToString(objectType);
                string serverPath = @"\\" + Path.Combine(ipAddress, scaTaskScheduler, this.incrementalUpdateSearchIndexLog);

                /// create directory
                Common.CommonHelper.CreateDirectoryIfNotExists(serverPath);
                logDirectory = Path.Combine(serverPath);
                /// build xml file path

                fileName = xmlFileNamePrefix + CommonConstants.Underscore + DateTime.Now.ToString(CommonConstants.DateTimeFormatyyMMddhhmmss, CultureInfo.InvariantCulture) + CommonConstants.XMLFileFormat;
                var filePath = Path.Combine(logDirectory, fileName);

                using (XmlWriter xmlWriter = XmlWriter.Create(filePath))
                {
                    WriteXMLToFile(xmlWriter, this.incrementalUpdateSearchIndexLog, xmlFileNamePrefix, indigoId, objectType);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlWriter"></param>
        /// <param name="element"></param>
        /// <param name="value"></param>
        private void CreateElement(XmlWriter xmlWriter, string element, string value)
        {
            xmlWriter.WriteStartElement(element);
            xmlWriter.WriteString(value);
            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// WriteXMLToFile
        /// </summary>
        /// <param name="xmlDoc">XMLDoc</param>
        /// <param name="xmlFilePath">XMLFilePath</param>
        /// <param name="xmlFileNamePrefix">XMLFileNamePrefix</param>
        private void WriteXMLToFile(XmlWriter xmlWriter, string xmlFilePath, string xmlFileNamePrefix, long indigoId, long objectType)
        {
            xmlWriter.WriteStartDocument();

            xmlWriter.WriteStartElement("DATAFORINDEX");

            CreateElement(xmlWriter, "INDIGOID", Convert.ToString(indigoId, CultureInfo.InvariantCulture));
            CreateElement(xmlWriter, "OBJECTTYPE", Convert.ToString(objectType, CultureInfo.InvariantCulture));

            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();        
        }
    }
}