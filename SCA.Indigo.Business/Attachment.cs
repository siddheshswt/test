﻿//----------------------------------------------------------------------------------------------
// <copyright file="Attachment.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using BusinessModels;
    using Interfaces;
    using Common;
    using Infrastructure.Repositories;
    using EFModel = Model;
    using System.IO;
    using System.ServiceModel;
    using SCA.Indigo.Model;
    
    /// <summary>
    /// Attachment service
    /// </summary>    
    public class Attachment : IAttachment
    {
        #region Attachment 

        /// <summary>
        /// Uploads the attachment.
        /// </summary>
        /// <param name="attachmentBusinessViewModelList">The attachment business view model list.</param>
        /// <returns>Returns the message</returns>
        public string UploadAttachment(List<AttachmentBusinessViewModel> attachmentBusinessViewModelList)
        {
            IAttachmentRepository iAttachmentRepository = null;
            bool isSuccess = false;
            string userId = string.Empty;
            try
            {
                if (attachmentBusinessViewModelList != null)
                {
                    var attachmentList = new List<EFModel.Attachment>();
                    EFModel.Attachment attachmentFile;
                    foreach (var fileItem in attachmentBusinessViewModelList)
                    {
	                    attachmentFile = new Model.Attachment
	                    {
		                    TypeId = Convert.ToInt64(fileItem.TypeId, CultureInfo.InvariantCulture),
		                    AttachmentType = Convert.ToInt64(fileItem.AttachmentType, CultureInfo.InvariantCulture),
		                    FileName = Path.GetFileName(fileItem.FileName),
		                    AttachmentLocation = fileItem.AttachmentLocation,
		                    CreatedBy = Guid.Parse(fileItem.CreatedBy),
		                    CreatedDate = DateTime.Now
	                    };
	                    userId = fileItem.CreatedBy;
                        attachmentList.Add(attachmentFile);
                    }
                    iAttachmentRepository = new AttachmentRepository();                    
                    iAttachmentRepository.InsertOrUpdate(attachmentList);
                    IUnitOfWork attachementUnitWork = iAttachmentRepository.UnitOfWork;
                    attachementUnitWork.Commit();
                    isSuccess = true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
	            LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
	            LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if(iAttachmentRepository != null)
                {
                    iAttachmentRepository.Dispose();
                }
            }

            return isSuccess ? "true" : "false";
        }

        /// <summary>
        /// Displays the attachment.
        /// </summary>
        /// <param name="typeId">The type identifier.</param>
        /// <param name="inputAttachmentType">Type of the input attachment.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Return list of Attachment business view model</returns>
        public List<AttachmentBusinessViewModel> DisplayAttachment(string typeId, string inputAttachmentType, string userId)
        {
            var attachmentDetailList = new List<AttachmentBusinessViewModel>();
            try
            {
                var attachmentTypeId = Convert.ToInt64(typeId, CultureInfo.InvariantCulture);
                var attachmentType = Convert.ToInt64(inputAttachmentType, CultureInfo.InvariantCulture);
                var attachmentList = (new EFModel.DBHelper()).GetAttachmentDetails(attachmentTypeId, attachmentType);
				AttachmentBusinessViewModel attachmentItem;
                foreach (var item in attachmentList)
                {
	                attachmentItem = new AttachmentBusinessViewModel
	                {
		                FileName = item.FileName,
		                AttachmentId = Convert.ToString(item.AttachmentId, CultureInfo.InvariantCulture),
		                CreatedBy = item.CreatedBy,
		                CreatedDate = Convert.ToString(item.AttachmentDate, CultureInfo.InvariantCulture),
		                AttachmentLocation = item.AttachmentLocation
	                };
	                attachmentDetailList.Add(attachmentItem);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return attachmentDetailList;
        }

        /// <summary>
        /// Gets the attachment path.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>Gets the list of attachment path </returns>
        public List<string> GetAttachmentPath(List<string> attachmentListId)
        {
            var userId = string.Empty;
            IAttachmentRepository iAttachmentRepository = null;
            try
            {
                iAttachmentRepository = new AttachmentRepository();
                var result = iAttachmentRepository.All.Where(x => attachmentListId.Contains(x.AttachmentId.ToString()))
		                .Select(x => x.AttachmentLocation);
				return result.ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if(iAttachmentRepository != null)
                {
                    iAttachmentRepository.Dispose();
                }
            }
            return null;

        }

        /// <summary>
        /// Removes the attachment.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>Returns the status of Remove attachment</returns>
        public bool RemoveAttachment(List<string> attachmentListId)
        {
            IAttachmentRepository iAttachmentRepository = null;
            var userId = string.Empty;
            try
            {
                iAttachmentRepository = new AttachmentRepository();
                var query = iAttachmentRepository.All.Where(x => attachmentListId.Contains(x.AttachmentId.ToString()));
                iAttachmentRepository.Delete(query.ToList());
                iAttachmentRepository.Save();
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (iAttachmentRepository != null)
                {
                    iAttachmentRepository.Dispose();
                }
            }

            return true;
        }

        #endregion                
    }
}
