﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Sachin
// Created          : 12-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassDataImport.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;
    using EFModel = SCA.Indigo.Model;
    using System.Text;
    using LinqToExcel;
    using SCA.Indigo.Business.BusinessModels.MassDataImport;

    using System.IO;
    using System.ServiceModel;

    /// <summary>
    /// Mass Data Import Class.
    /// </summary>    
    public class MassDataImport : IMassDataImport
    {
        #region Constants

		/// <summary>
		/// Mass Changes Log file Path
		/// </summary>
		private string massChangesLogs = ConfigurationManager.AppSettings["MassChangesLogs"];

        /// <summary>
        /// data file name
        /// </summary>
        private static string massDataFileName = CommonConstants.MassDataLogFile;

        /// <summary>
        /// Maximum data limit for Mass Update
        /// </summary>
        private static string massUploadMaxLimit = ConfigurationManager.AppSettings["MassUploadMaxLimit"];

        /// <summary>
        /// Maximum data limit for Patient Mass Update
        /// </summary>
        private static string patientUploadMaxLimit = ConfigurationManager.AppSettings["PatientUploadMaxLimit"];

        #endregion

        #region Variables

        /// <summary>
        /// Gets or sets The in valid data type
        /// </summary>
        public StringBuilder InvalidDataType { get; set; }

        /// <summary>
        /// Gets or sets The mandatory fields
        /// </summary>
        public StringBuilder MandatoryFields { get; set; }

        /// <summary>
        /// Gets or sets The database validation
        /// </summary>
        public StringBuilder DatabaseValidation { get; set; }

        /// <summary>
        /// Gets or sets The invalid length
        /// </summary>
        public StringBuilder InvalidLength { get; set; }

        /// <summary>
        /// Gets or sets The custom validation
        /// </summary>
        public StringBuilder CustomValidation { get; set; }

        /// <summary>
        /// The remove column
        /// </summary>
        private List<string> removeColumn = null;

        /// <summary>
        /// The is mandatory
        /// </summary>
        private bool isMandatoryGlobal/* = false*/;

        /// <summary>
        /// The is import
        /// </summary>
        private bool isImport = true;

        /// <summary>
        /// The import status
        /// </summary>
        private bool importStatus = false;

        /// <summary>
        /// The user id
        /// </summary>
        string globalUserId = string.Empty;

        /// <summary>
        /// The gu user identifier
        /// </summary>
        Guid guUserId;

        /// <summary>
        /// The string round
        /// </summary>
        private string strRound = string.Empty;

        /// <summary>
        /// The current date
        /// </summary>
        DateTime currentDate;

        /// <summary>
        /// Gets or sets The row status
        /// </summary>
        public string RowStatus { get; set; }

        /// <summary>
        /// The is valid
        /// </summary>
        bool isValidGlobal = true;

        /// <summary>
        /// The list model
        /// </summary>
        private List<ListTypeBusinessModel> listModel;
        /// <summary>
        /// All country list
        /// </summary>
        private List<EFModel.Country> allCountryList = null;
        /// <summary>
        /// All county list
        /// </summary>
        private List<EFModel.CountyList> allCountyList = null;
        /// <summary>
        /// The existing patient list
        /// </summary>
        private List<EFModel.Patient> existingPatientList;

        /// <summary>
        /// The existing patient list by SAP Id
        /// </summary>
        private List<EFModel.Patient> existingPatientListbySapId;

        /// <summary>
        /// The patient list by load ids
        /// </summary>
        private List<string> patientLoadIds;

        /// <summary>
        /// The object customer
        /// </summary>
        private Customer objCustomer = null;

        /// <summary>
        /// The existing care home list
        /// </summary>
        private List<EFModel.CareHome> existingCarehomeList = null;

        /// <summary>
        /// Gets or sets The return log file path
        /// </summary>
        public string ReturnLogFilePath { get; set; }

        /// <summary>
        /// The customerId
        /// </summary>
        private long globalCustomerId;

        /// <summary>
        /// The SAP PatientIds
        /// </summary>
        private List<string> sapPatientIds = null;

        /// <summary>
        /// Carehome List
        /// </summary>
        private List<EFModel.CareHome> carehomeList = null;

        #endregion

        #region Common Functions
        /// <summary>
        /// This will remove blank rows from the excel file
        /// </summary>
        /// <typeparam name="T">Anonymous Object</typeparam>
        /// <param name="queryFactory">query Factory</param>
        /// <param name="maxRecords">max Records</param>
        /// <returns>non blank rows</returns>
        public List<T> GetNonblankRows<T>(ExcelQueryFactory queryFactory, string maxRecords)
        {
            long maxIndex = 0;
            if (!string.IsNullOrEmpty(maxRecords))
            {
                maxIndex = Convert.ToInt64(maxRecords, CultureInfo.InvariantCulture);
            }
            if (queryFactory == null)
            {
                return null;
            }
            List<T> onlyNonBlankRows = queryFactory.Worksheet<T>(0)                
                                    .ToList()
                                    .Select((typedRow, index) => new { typedRow, index })                
                                    .Join(
                                        queryFactory.Worksheet(0)                
                                                    .ToList()
                                                    .Select(
                                                        (untypedRow, indexForUntypedRow) =>
                                                        new { untypedRow, indexForUntypedRow }),                
                                        arg => arg.index, arg => arg.indexForUntypedRow,
                                        (a, b) => new { a.index, a.typedRow, b.untypedRow })                
                                    .Where(x => x.untypedRow.Any(cell => cell.Value != DBNull.Value) && x.index < maxIndex)
                                    .Select(joined => joined.typedRow).ToList();
            return onlyNonBlankRows;
        }

        /// <summary>
        /// Populates the list data.
        /// </summary>
        /// <param name="listTypeIds">The list type ids.</param>
        /// <param name="languageId">The language identifier.</param>
        private void PopulateListData(string listTypeIds, string languageId)
        {
            CommonService commonService = new CommonService();
            try
            {
                listModel = commonService.GetListType(listTypeIds, languageId);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, globalUserId);
            }
            finally
            {
                commonService = null;
            }
        }

        /// <summary>
        /// Populates all country.
        /// </summary>
        private void PopulateAllCountry()
        {
            ICountryRepository countryRepository = new CountryRepository();
            try
            {
                allCountryList = countryRepository.All.ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            finally
            {
                if (countryRepository != null)
                {
                    countryRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Populates the county list.
        /// </summary>
        /// <param name="countyCodeList">The county code list.</param>
        private void PopulateCountyList(List<string> countyCodeList)
        {
            ICountyListRepository countyListRepository = new CountyListRepository();
            try
            {
                allCountyList = countyListRepository.GetAllCountyListByCode(countyCodeList).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, globalUserId);
            }
            finally
            {
                if (countyListRepository != null)
                {
                    countyListRepository.Dispose();
                }
            }
        }

        #endregion

        #region Process Mass Data Imports

        /// <summary>
        /// Upload Customer Clinical Contact
        /// </summary>
        /// <param name="massChangesBusinessModel">Mass Data BusinessModel</param>
        /// <returns>
        /// true or false
        /// </returns>
        public string ProcessImportData(MassDataBusinessModel massChangesBusinessModel)
        {
            ExcelQueryFactory excelFactory = null;
            try
            {
                if (massChangesBusinessModel != null)
                {
                    globalUserId = massChangesBusinessModel.UserId;
                    guUserId = Guid.Parse(globalUserId);
                    isImport = massChangesBusinessModel.IsImport;
                    currentDate = DateTime.Now;
                    CommonService.UILogIPAddress = massChangesBusinessModel.UILogIPAddress;
                    string templateName = string.Empty;
                    excelFactory = new ExcelQueryFactory(massChangesBusinessModel.FilePath);

                    if (massChangesBusinessModel.IsImport)
                    {
                        massDataFileName = CommonConstants.MassImportLogFile;
                    }
                    else
                    {
                        massDataFileName = CommonConstants.MassChangeLogFile;
                    }

                    switch (massChangesBusinessModel.SelectedTemplate)
                    {
                        case (int)SCAEnums.MassDataTemplate.PostCodeMatrix:
                            {
                                this.ImportPostcodeMatrix(excelFactory);
                                templateName = SCAEnums.MassDataTemplate.PostCodeMatrix.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.Patient:
                            {
                                this.ImportPatientData(excelFactory, massChangesBusinessModel.IsImport);
                                templateName = SCAEnums.MassDataTemplate.Patient.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.Carehome:
                            {
                                this.ImportCarehome(excelFactory);
                                templateName = SCAEnums.MassDataTemplate.Carehome.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.ContactPerson:
                            {
                                this.ImportContactPerson(excelFactory, massChangesBusinessModel);
                                templateName = SCAEnums.MassDataTemplate.ContactPerson.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.CommunicationFormat:
                            {
                                this.ImportCommunicationFormat(excelFactory, massChangesBusinessModel);
                                templateName = SCAEnums.MassDataTemplate.CommunicationFormat.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.CustomerClinicalContacts:
                            {
                                this.ImportClinicalContactsAnalysis(excelFactory, false, true, massChangesBusinessModel.IsImport);
                                templateName = SCAEnums.MassDataTemplate.CustomerClinicalContacts.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.CustomerAnalysisInformation:
                            {
                                this.ImportClinicalContactsAnalysis(excelFactory, false, false, massChangesBusinessModel.IsImport);
                                templateName = SCAEnums.MassDataTemplate.CustomerAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.PatientClinicalContacts:
                            {
                                this.ImportClinicalContactsAnalysis(excelFactory, true, true, massChangesBusinessModel.IsImport);
                                templateName = SCAEnums.MassDataTemplate.PatientClinicalContacts.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        case (int)SCAEnums.MassDataTemplate.PatientAnalysisInformation:
                            {
                                this.ImportClinicalContactsAnalysis(excelFactory, true, false, massChangesBusinessModel.IsImport);
                                templateName = SCAEnums.MassDataTemplate.PatientAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                                break;
                            }
                        default:
                            // do the default action
                            break;
                    }

                    // Remove File after process complete
                    CommonHelper.RemoveFile(massChangesBusinessModel.FilePath);

                    // Create entry in MassDataLog table for uploaded template
                    this.CreateMassDataLog(templateName, isImport, ReturnLogFilePath, currentDate);
                }
                return ReturnLogFilePath;
            }
            finally
            {
                if (excelFactory != null)
                {
                    excelFactory.Dispose();
                }
            }
        }

        /// <summary>
        /// Creates the mass data log.
        /// </summary>
        /// <param name="templateName">Name of the template</param>
        /// <param name="isImport">is Import</param>
        /// <param name="logFilePath">log File Path</param>
        /// <param name="currentDate">current Date</param>
        public void CreateMassDataLog(string templateName, bool isImport, string logFilePath, DateTime currentDate)
        {
            if (!string.IsNullOrEmpty(logFilePath) && logFilePath.LastIndexOf(@"\", StringComparison.InvariantCulture) != -1)
            {
                IMassDataLogRepository massDataLogRepository = new MassDataLogRepository();
                try
                {
                    IUnitOfWork unitOfWork = massDataLogRepository.UnitOfWork;
                    EFModel.MassDataLog massDataLog = new MassDataLog();
                    massDataLog.IsInsert = isImport;
                    massDataLog.TemplateName = templateName;
                    massDataLog.CreatedBy = guUserId;
                    massDataLog.CreatedDate = currentDate;
                    massDataLog.DataFileName = logFilePath.Substring(logFilePath.LastIndexOf(@"\", StringComparison.InvariantCulture) + 1);
                    massDataLogRepository.InsertOrUpdate(massDataLog);
                    unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, globalUserId);
                }
                finally
                {
                    if (massDataLogRepository != null)
                    {
                        massDataLogRepository.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Imports the post code matrix.
        /// </summary>
        /// <param name="excelFactory">The excel factory.</param>
        /// <returns>string value</returns>
        public string ImportPostcodeMatrix(ExcelQueryFactory excelFactory)
        {
            if (excelFactory != null)
            {
                var postCodeMatrixBusinessModelData = GetNonblankRows<MassDataPostCodeMatrixBusinessModel>(excelFactory, massUploadMaxLimit);
                this.MassImportPostCodeMatrix(postCodeMatrixBusinessModelData);
            }
            return ReturnLogFilePath;
        }

        /// <summary>
        /// Imports the clinical contacts analysis.
        /// </summary>
        /// <param name="excelFactory">The excel factory.</param>
        /// <param name="isPatientType">if set to <c>true</c> [is patient type].</param>
        /// <param name="isClinicalContact">if set to <c>true</c> [is clinical contact].</param>
        /// <param name="isImport">if set to <c>true</c> [is import].</param>
        private void ImportClinicalContactsAnalysis(ExcelQueryFactory excelFactory, bool isPatientType, bool isClinicalContact, bool isImport)
        {
            if (excelFactory != null)
            {
                string templatename = string.Empty;
                // Add Mapping
                excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.SAPCustomerNumber, "CustomerId");

                long listTypeId = default(long);

                if (isClinicalContact)
                {
                    listTypeId = (int)SCAEnums.ListType.MedicalStaff;
                    templatename = SCAEnums.MassDataTemplate.CustomerClinicalContacts.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    templatename = SCAEnums.MassDataTemplate.CustomerAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                    listTypeId = (int)SCAEnums.ListType.AnalysisInformation;
                    // Add Mapping for Clinical Contact
                    excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.ClinicalContactTypeId, "AnalysisInformationTypeId");
                    excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.ClinicalContactType, "AnalysisInformationType");
                    excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.ClinicalContactValue, "AnalysisInformationValue");
                    excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.ClinicalContactValueId, "AnalysisInformationValueId");
                }

                if (isPatientType)
                {
                    //excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.PatientId, "IndigoPortalPatientId");
                    excelFactory.AddMapping<ClinicalContactAnalysisMassBusinessModel>(x => x.SAPPatientNumber, "PatientId");
                    if (isClinicalContact)
                    {
                        templatename = SCAEnums.MassDataTemplate.PatientClinicalContacts.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        templatename = SCAEnums.MassDataTemplate.PatientAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                    }
                }

                // Get the Excel Data                
                var clinicalContactAnalysis = GetNonblankRows<ClinicalContactAnalysisMassBusinessModel>(excelFactory, massUploadMaxLimit);

                // Pass the Data To Service for Update/Insert
                if (isPatientType)
                {
                    // Process the Mass Insert for Patient
                    clinicalContactAnalysis = this.MassInsertPatientClinicalContactAnalysis(clinicalContactAnalysis, listTypeId, isImport);

                    if (listTypeId == (int)SCAEnums.ListType.AnalysisInformation)
                    {
                        clinicalContactAnalysis.ForEach(d =>
                        {
                            // Analysis Information
                            d.AnalysisInformationType = d.ClinicalContactType;
                            d.AnalysisInformationTypeId = d.ClinicalContactTypeId;
                            d.AnalysisInformationValue = d.ClinicalContactValue;
                            d.AnalysisInformationValueId = d.ClinicalContactValueId;
                        });

                        removeColumn = new List<string>() { "SAPPatientNumber", "ClinicalContactTypeId", "ClinicalContactType", "ClinicalContactValue", "ClinicalContactValueId", "SAPCustomerNumber", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                        ReturnLogFilePath = CommonService.ExportToExcel(clinicalContactAnalysis, SCAEnums.MassDataTemplate.PatientAnalysisInformation.ToString(CultureInfo.InvariantCulture),
                                            massDataFileName, removeColumn, this.massChangesLogs, true);
                    }
                    else
                    {
                        // Clinical Contact 
                        removeColumn = new List<string>() { "SAPPatientNumber", "AnalysisInformationTypeId", "AnalysisInformationType", "AnalysisInformationValue", "AnalysisInformationValueId", "SAPCustomerNumber", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                        ReturnLogFilePath = CommonService.ExportToExcel(clinicalContactAnalysis, SCAEnums.MassDataTemplate.PatientClinicalContacts.ToString(CultureInfo.InvariantCulture),
                                            massDataFileName, removeColumn, this.massChangesLogs, true);
                    }
                }
                else
                {
                    // Process the Mass Insert for Customer
                    clinicalContactAnalysis = this.MassInsertCustomerClinicalContactAnalysis(clinicalContactAnalysis, listTypeId, isImport);

                    if (listTypeId == (int)SCAEnums.ListType.AnalysisInformation)
                    {
                        clinicalContactAnalysis.ForEach(d =>
                        {
                            // Analysis Information
                            d.AnalysisInformationType = d.ClinicalContactType;
                            d.AnalysisInformationTypeId = d.ClinicalContactTypeId;
                            d.AnalysisInformationValue = d.ClinicalContactValue;
                            d.AnalysisInformationValueId = d.ClinicalContactValueId;
                        });

                        removeColumn = new List<string>() { "IndigoPortalPatientId", "ClinicalContactTypeId", "ClinicalContactType", "ClinicalContactValue", "ClinicalContactValueId", "SAPCustomerNumber", "PatientId", "SAPPatientNumber", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" }; ;
                        ReturnLogFilePath = CommonService.ExportToExcel(clinicalContactAnalysis, SCAEnums.MassDataTemplate.CustomerAnalysisInformation.ToString(CultureInfo.InvariantCulture),
                                            massDataFileName, removeColumn, this.massChangesLogs, true);
                    }
                    else
                    {
                        // Clinical Contact 
                        removeColumn = new List<string>() { "IndigoPortalPatientId", "AnalysisInformationTypeId", "AnalysisInformationType", "AnalysisInformationValue", "AnalysisInformationValueId", "SAPCustomerNumber", "PatientId", "SAPPatientNumber", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" }; ;
                        ReturnLogFilePath = CommonService.ExportToExcel(clinicalContactAnalysis, SCAEnums.MassDataTemplate.CustomerClinicalContacts.ToString(CultureInfo.InvariantCulture),
                                            massDataFileName, removeColumn, this.massChangesLogs, true);
                    }
                }
            }
        }

        /// <summary>
        /// Import Carehome
        /// </summary>
        /// <param name="excelFactory">excel Factory</param>
        private void ImportCarehome(ExcelQueryFactory excelFactory)
        {
            if (excelFactory != null)
            {
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.LoadId, "LoadId");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.CustomerId, "CustomerId");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.IndigoCarehomeId, "IndigoCarehomeId");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.SAPCarehomeId, "SAPCarehomeId");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.HouseName, "HouseName");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.AddressLine1, "AddressLine1");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.AddressLine2, "AddressLine2");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.TownOrCity, "TownOrCity");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.County, "County");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.Country, "Country");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.PostCode, "PostCode");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.PhoneNumber, "PhoneNumber");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.Mobile, "Mobile");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.Email, "Email");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.CarehomeStatus, "CarehomeStatus");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.NextDeliveryDate, "NextDeliveryDate");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.Round, "Round");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.RoundId, "RoundId");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.DeliveryFrequency, "DeliveryFrequency");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.CarehomeType, "CarehomeType");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.OrderType, "OrderType");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.PurchaseOrderNumber, "PurchaseOrderNumber");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.SendToSAP, "SendToSAP");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.BillTo, "BillTo");
                excelFactory.AddMapping<MassDataCareHomeBusinessModel>(x => x.Remarks, "Remarks");

                var careHomeDetailsList = GetNonblankRows<MassDataCareHomeBusinessModel>(excelFactory, massUploadMaxLimit);

                if (careHomeDetailsList != null)
                {
                    this.MassInsertUpdateCareHome(careHomeDetailsList);
                }
            }
        }

        /// <summary>
        /// Import contact person function
        /// </summary>
        /// <param name="excelFactory">The excel factory.</param>
        /// <param name="massDataBusinessModel">The mass data business model.</param>
        /// <returns>
        /// true or false
        /// </returns>
        private bool ImportContactPerson(ExcelQueryFactory excelFactory, MassDataBusinessModel massDataBusinessModel)
        {
            bool retVal = false;

            if (excelFactory != null)
            {
                // Add Mapping
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.CustomerId, "CustomerId");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.IndigoPatientId, "IndigoPatientId");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.IndigoCarehomeId, "IndigoCarehomeId");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.ContactPersonFirstName, "ContactPersonFirstName");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.ContactPersonSurname, "ContactPersonSurname");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.JobTitle, "JobTitle");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.Phone, "Phone");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.Mobile, "Mobile");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.EmailId, "EmailId");
                excelFactory.AddMapping<ContactPersonBusinessModel>(x => x.Remarks, "Remarks");

                // Get the Excel Data
                var contactPersonList = GetNonblankRows<ContactPersonBusinessModel>(excelFactory, massUploadMaxLimit);

                // Send to validate and Insert/ Update data if valid.
                var result = MassInsertUpdateContactPerson(contactPersonList, massDataBusinessModel);

                // Write Log File from contactPersonList
                if (result != null)
                {
                    removeColumn = new List<string>() { "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                    ReturnLogFilePath = CommonService.ExportToExcel(result, SCAEnums.MassDataTemplate.ContactPerson.ToString(CultureInfo.InvariantCulture),
                                            massDataFileName, removeColumn, this.massChangesLogs, true);
                }
            }

            return retVal;
        }

        /// <summary>
        /// Imports the communication format.
        /// </summary>
        /// <param name="excelFactory">The excel factory.</param>
        /// <param name="massDataBusinessModel">The mass data business model.</param>
        /// <returns>true or false</returns>
        private bool ImportCommunicationFormat(ExcelQueryFactory excelFactory, MassDataBusinessModel massDataBusinessModel)
        {
            bool retVal = false;

            if (excelFactory != null)
            {
                // Add Mapping                
                excelFactory.AddMapping<CommunicationPreferencesBusinessModel>(x => x.CustomerId, "CustomerId");
                excelFactory.AddMapping<CommunicationPreferencesBusinessModel>(x => x.IndigoPatientId, "IndigoPatientId");
                excelFactory.AddMapping<CommunicationPreferencesBusinessModel>(x => x.IndigoCarehomeId, "IndigoCarehomeId");
                excelFactory.AddMapping<CommunicationPreferencesBusinessModel>(x => x.CommunicationPreference, "CommunicationPreference");
                excelFactory.AddMapping<CommunicationPreferencesBusinessModel>(x => x.MarketingPreference, "MarketingPreference");
                excelFactory.AddMapping<CommunicationPreferencesBusinessModel>(x => x.Remarks, "Remarks");

                // Get the Excel Data                
                var communicationFormatList = GetNonblankRows<CommunicationPreferencesBusinessModel>(excelFactory, massUploadMaxLimit);

                // Send to validate and Insert/ Update data if valid.
                var result = MassInsertUpdateCommunicationFormat(communicationFormatList, massDataBusinessModel);

                // Write Log File from communicationFormatList                
                if (result != null)
                {
                    removeColumn = new List<string>() { "UserId", "SelectedTemplate", "FilePath", "IsImport", "CommunicationPrefListId", "MarkettingPrefListId", "IsPatient", "UILogIPAddress" };
                    ReturnLogFilePath = CommonService.ExportToExcel(result, SCAEnums.MassDataTemplate.CommunicationFormat.ToString(CultureInfo.InvariantCulture),
                                        massDataFileName, removeColumn, this.massChangesLogs, true);
                }
            }

            return retVal;
        }

        #endregion

        #region Validate Data Type Functions

        /// <summary>
        /// Validate Individual Column Data
        /// </summary>
        /// <param name="value">column value</param>
        /// <param name="columnName">Column Name</param>
        /// <param name="isMandatory">Is Mandatory</param>
        /// <param name="dataTypeCheck">data Type Check</param>		
        public void ValidateColumn(string value, string columnName, bool isMandatory, SCAEnums.DataType dataTypeCheck)
        {
            ValidateColumn(value, columnName, isMandatory, dataTypeCheck, 0);
        }

        /// <summary>
        /// Validate Individual Column Data
        /// </summary>
        /// <param name="value">Column value</param>
        /// <param name="columnName">Column Name</param>
        /// <param name="isMandatory">Is Mandatory</param>
        /// <param name="dataTypeCheck">data Type Check</param>
        /// <param name="dataLength">The length.</param>
        public void ValidateColumn(string value, string columnName, bool isMandatory, SCAEnums.DataType dataTypeCheck, long dataLength)
        {
            if (string.IsNullOrEmpty(value) && isMandatory)
            {
                MandatoryFields.Append(", " + columnName);
                isValidGlobal = false;
            }
            if (dataTypeCheck != SCAEnums.DataType.NoCheck && !string.IsNullOrEmpty(value))
            {
                if (dataTypeCheck == SCAEnums.DataType.Int)
                {
                    int newDataTpe;
                    isValidGlobal = int.TryParse(value, out newDataTpe);
                    if (!isValidGlobal)
                    {
                        InvalidDataType.Append(", " + columnName);
                    }
                }
                else if (dataTypeCheck == SCAEnums.DataType.Long)
                {
                    long newDataTpe;
                    isValidGlobal = long.TryParse(value, out newDataTpe);
                    if (!isValidGlobal)
                    {
                        InvalidDataType.Append(", " + columnName);
                    }
                }
                else if (dataTypeCheck == SCAEnums.DataType.Date)
                {
                    // 31/05/2015                    
                    if (!CommonHelper.ValidateDate(value))
                    {
                        InvalidDataType.Append(", " + columnName);
                    }
                }
                else if (dataTypeCheck == SCAEnums.DataType.Decimal)
                {
                    decimal newDataTpe;
                    isValidGlobal = decimal.TryParse(value, out newDataTpe);
                    if (!isValidGlobal)
                    {
                        InvalidDataType.Append(", " + columnName);
                    }
                }
                else if (dataTypeCheck == SCAEnums.DataType.String)
                {
                    if (dataLength != 0 && value.Length > dataLength)
                    {
                        isValidGlobal = false;
                        InvalidLength.Append(", " + columnName);
                    }
                }
                else if (dataTypeCheck == SCAEnums.DataType.Bool)
                {
                    if (string.IsNullOrEmpty(value) || (value.ToUpper() != CommonConstants.Yes && value.ToUpper() != CommonConstants.No))
                    {
                        MandatoryFields.Append("," + string.Format(CultureInfo.InvariantCulture, CommonConstants.MassDataInvalidYesNoMessage, columnName));
                        isValidGlobal = false;
                    }
                }
            }
        }

        /// <summary>
        /// Converts the date todd m myyyy.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// datetime object
        /// </returns>
        private DateTime ConvertDateToMMddyyyy(string date)
        {
            DateTime newDate;
            string[] dateParts = date.Split('/');
            if (dateParts[2].Length >= 4)
            {
                date = dateParts[0] + "/" + dateParts[1] + "/" + dateParts[2].Substring(0, 4);
            }

            string[] formats = { CommonConstants.DateFormatddMMyyyy, CommonConstants.DateFormatddMyyyy, CommonConstants.DateFormatdMMyyyy, CommonConstants.DateFormatdMyyyy };
            DateTime.TryParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate);
            return newDate;
        }

        #endregion

        #region Customer Clinical Contact Analysis

        /// <summary>
        /// Gets all clinical contact master data.
        /// </summary>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <returns>List of ClinicalContact Analysis Master></returns>
        private List<ClinicalContactAnalysisMaster> GetAllClinicalContactMasterData(long listTypeId)
        {
            IClinicalContactAnalysisMasterRepository clinicalContactAnalysisMasterRepository = null;
            List<ClinicalContactAnalysisMaster> clinicalContactAnalysisMasterData = null;
            try
            {
                clinicalContactAnalysisMasterRepository = new ClinicalContactAnalysisMasterRepository();
                clinicalContactAnalysisMasterData = clinicalContactAnalysisMasterRepository.GetAllClinicalContactAnalysisMasterByTypeId(listTypeId).ToList();
            }

            finally
            {
                if (clinicalContactAnalysisMasterRepository != null)
                {
                    clinicalContactAnalysisMasterRepository.Dispose();
                }
            }

            return clinicalContactAnalysisMasterData;
        }             

        /// <summary>
        /// Masses the insert customer clinical contact analysis.
        /// </summary>
        /// <param name="clinicalContactAnalysisData">The clinical contact analysis data.</param>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <param name="isImport">is Import</param>
        /// <returns>List of ClinicalContact AnalysisMassBusinessModel</returns>
        private List<ClinicalContactAnalysisMassBusinessModel> MassInsertCustomerClinicalContactAnalysis(List<ClinicalContactAnalysisMassBusinessModel> clinicalContactAnalysisData, long listTypeId, bool isImport)
        {
            if (clinicalContactAnalysisData.Any())
            {
                // Get All Customer for ClinicalContact Analysis Master
                List<ClinicalContactAnalysisMaster> allClinicalContactAnalysisMaster = this.GetAllClinicalContactMasterData(listTypeId); // new ClinicalContactAnalysisMasterRepository().GetAllClinicalContactAnalysisMasterByTypeId(listTypeId);

                ICustomerRepository customerRepository = null;
                IUnitOfWork unitOfWork = null;
                IDXCustomerMedicalStaffAnalysisInfo idxCustomerMedicalStaffAnalysisInfo = null;
                Customer customerToModify = null;

                try
                {
                    customerRepository = new CustomerRepository();
                    unitOfWork = customerRepository.UnitOfWork;

                    string clinicalContactTypeId = string.Empty;
                    string clinicalContactType = string.Empty;
                    string clinicalContactValueId = string.Empty;
                    string clinicalContactValue = string.Empty;
                    string remarks = string.Empty;

                    long clinicalContactListId = default(long);
                    long clinicalContactValueListId = default(long);

                    List<ListTypeAdvancedBusinessModel> listTypeAdvanceBusinessModel = new List<ListTypeAdvancedBusinessModel>();
                    IQueryable<Customer> allCustomer = customerRepository.GetAllCustomer();

                    long validCustomerId = 0;

                    // Start the Processing
                    foreach (var clinicalContact in clinicalContactAnalysisData)
                    {
                        try
                        {
                            #region Start Mass Update Process

                            // Start Actual Processing
                            listTypeAdvanceBusinessModel = new List<ListTypeAdvancedBusinessModel>();
                            if (this.ValidateCustomerClinicalDataclinicalContact(clinicalContact, allClinicalContactAnalysisMaster, ref customerToModify, false, isImport, allCustomer, ref validCustomerId))
                            {
                                clinicalContactTypeId = clinicalContact.ClinicalContactTypeId;
                                clinicalContactType = clinicalContact.ClinicalContactType;
                                clinicalContactValueId = clinicalContact.ClinicalContactValueId;
                                clinicalContactValue = clinicalContact.ClinicalContactValue;
                                remarks = clinicalContact.Remarks;

                                // Check the Clinical Contact TypeId
                                if (string.IsNullOrEmpty(clinicalContactTypeId))
                                {
                                    // INSERT MODE
                                    #region Insert Mode

                                    if (!string.IsNullOrEmpty(clinicalContactType))
                                    {
                                        if (allClinicalContactAnalysisMaster.Where(d => d.ContactName.ToUpper() == clinicalContactType.ToUpper().Trim()).Any())
                                        {
                                            // Log the Message Invalid Clinical Contact Column Type
                                            clinicalContact.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.CLINICALANALYSIS_ALREADY_EXIST, clinicalContactType);
                                            clinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                                        }
                                        else
                                        {
                                            // Need to add new Clinical Contact Column
                                            listTypeAdvanceBusinessModel.Add(new ListTypeAdvancedBusinessModel()
                                            {
                                                DisplayText = clinicalContactType,
                                                IsFieldType = true,
                                                ListTypeId = listTypeId,
                                                ListId = 0
                                            });

                                            // Insert into Master, Translation and Translation Proxy
                                            listTypeAdvanceBusinessModel = new CustomerParameter().UpdateClinicalContactAnalysis(listTypeAdvanceBusinessModel, globalUserId);
                                            long listid = listTypeAdvanceBusinessModel.FirstOrDefault().ListId;

                                            // Insert into the IDXCustomerMedicalStaffAnalysisInfo
                                            idxCustomerMedicalStaffAnalysisInfo = new IDXCustomerMedicalStaffAnalysisInfo();
                                            this.AddUpdateClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, listid, string.Empty, true, remarks);

                                            // Insert the Clinical Contact Value 
                                            if (string.IsNullOrEmpty(clinicalContactValueId))
                                            {
                                                if (!string.IsNullOrEmpty(clinicalContactValue))
                                                {
                                                    // Need to add new Clinical Contact Value
                                                    idxCustomerMedicalStaffAnalysisInfo = new IDXCustomerMedicalStaffAnalysisInfo();
                                                    this.AddUpdateClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, listid, clinicalContactValue, false, remarks);
                                                }
                                            }

                                            clinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                                        }
                                    }

                                    #endregion
                                }
                                // UPDATE
                                else
                                {
                                    #region Update Mode

                                    clinicalContactListId = Convert.ToInt64(clinicalContactTypeId, CultureInfo.InvariantCulture);

                                    // Check for the ListID exist in Clinical Master 
                                    if (!allClinicalContactAnalysisMaster.Where(d => d.ClinicalContactAnalysisMasterId == clinicalContactListId).Any())
                                    {
                                        // Log the Message Invalid Clinical Contact Column Type
                                        clinicalContact.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.INVALID_CLINICALANALYSIS_TYPE, clinicalContactListId);
                                        clinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        // Update the Column and Insert / Update the Column Contact Name
                                        listTypeAdvanceBusinessModel.Add(new ListTypeAdvancedBusinessModel()
                                        {
                                            DisplayText = clinicalContactType,
                                            IsFieldType = true,
                                            ListTypeId = listTypeId,
                                            ListId = clinicalContactListId
                                        });

                                        // Update Clinical Contact Analysis Master
                                        listTypeAdvanceBusinessModel = new CustomerParameter().UpdateClinicalContactAnalysis(listTypeAdvanceBusinessModel, globalUserId);

                                        // Get the idxCustomerMedicalStaff Analysis Info by ListId, Customer Id, IsFieldType
                                        idxCustomerMedicalStaffAnalysisInfo = customerToModify.IDXCustomerMedicalStaffAnalysisInfoes
                                        .Where(d => d.MedicalAnalysisListID == clinicalContactListId
                                            && d.IsFieldType == true).FirstOrDefault();

                                        // Update the Clinical Contact Column Only
                                        if (idxCustomerMedicalStaffAnalysisInfo == null)
                                        {
                                            idxCustomerMedicalStaffAnalysisInfo = new IDXCustomerMedicalStaffAnalysisInfo();
                                        }

                                        this.AddUpdateClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, clinicalContactListId, string.Empty, true, remarks);

                                        // Check for the Contact Value
                                        if (string.IsNullOrEmpty(clinicalContactValueId))
                                        {
                                            if (!string.IsNullOrEmpty(clinicalContactValue))
                                            {
                                                // Need to add new Clinical Contact Value
                                                idxCustomerMedicalStaffAnalysisInfo = new IDXCustomerMedicalStaffAnalysisInfo();
                                                this.AddUpdateClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, clinicalContactListId, clinicalContactValue, false, remarks);
                                            }
                                        }
                                        else
                                        {
                                            // Update the Clinical Contact Value for that Customer
                                            clinicalContactValueListId = Convert.ToInt64(clinicalContactValueId, CultureInfo.InvariantCulture);

                                            idxCustomerMedicalStaffAnalysisInfo = customerToModify.IDXCustomerMedicalStaffAnalysisInfoes
                                        .Where(d => d.IDXCustomerMedicalStaffAnalysisInfoID == clinicalContactValueListId
                                         && d.IsFieldType == false).FirstOrDefault();

                                            if (idxCustomerMedicalStaffAnalysisInfo != null)
                                            {
                                                // Update the Clinical Contact Column
                                                this.AddUpdateClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, clinicalContactListId, clinicalContactValue, false, remarks);
                                            }
                                        }

                                        clinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                clinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            }


                            #endregion
                        }
                        catch (Exception ex)
                        {
                            clinicalContact.ErrorMessage = ex.Message;
                            clinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        }
                    }

                    // Update the Customer
                    if (customerToModify != null)
                    {
                        customerToModify.ModifiedDate = DateTime.Now;
                        customerRepository.InsertOrUpdate(customerToModify);

                        // Commit the changes 
                        unitOfWork.Commit();
                    }
                }
                catch (DataException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (FormatException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (InvalidOperationException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (SystemException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (Exception e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                finally
                {
                    if (customerRepository != null)
                    {
                        customerRepository.Dispose();
                    }
                }
            }
            return clinicalContactAnalysisData;
        }     

        /// <summary>
        /// Validates the customer clinical data clinical contact.
        /// </summary>
        /// <param name="clinicalContact">The clinical contact.</param>
        /// <param name="allClinicalContactMaster">All clinical contact master.</param>
        /// <param name="customerToModify">The customer to modify.</param>
        /// <param name="isPatientClinicalContact">if set to <c>true</c> [is patient clinical contact].</param>
        /// <param name="isImport">is Import</param>
        /// <param name="allCustomer">all Customer</param>
        /// <param name="validCustomerId">valid Customer Id</param>
        /// <returns>true or false</returns>
        private bool ValidateCustomerClinicalDataclinicalContact(
            ClinicalContactAnalysisMassBusinessModel clinicalContact,
            List<ClinicalContactAnalysisMaster> allClinicalContactMaster,
            ref Customer customerToModify, bool isPatientClinicalContact,
            bool isImport,
            IQueryable<Customer> allCustomer, ref long validCustomerId
            )
        {
            MandatoryFields = new StringBuilder();
            InvalidDataType = new StringBuilder();
            DatabaseValidation = new StringBuilder();
            CustomValidation = new StringBuilder();
            var currentSapPatientId = string.Empty;
            bool status = true;
            StringBuilder processError = new StringBuilder();

            #region Customer Validation

            var currentSapid = string.Empty;
            string sapCustomerId = string.Empty;

            if (!string.IsNullOrEmpty(clinicalContact.SAPCustomerNumber))
            {
                currentSapid = CommonHelper.GetAppenedSapNumber(clinicalContact.SAPCustomerNumber);
                customerToModify = allCustomer.Where(d => d.SAPCustomerNumber == currentSapid).FirstOrDefault();
                if (customerToModify != null)
                {
                    if (validCustomerId == 0)
                    {
                        sapCustomerId = clinicalContact.SAPCustomerNumber;
                        validCustomerId = customerToModify.CustomerId;
                    }
                    // Check For Patient
                    if (isPatientClinicalContact)
                    {
                        currentSapPatientId = CommonHelper.GetAppenedSapNumber(clinicalContact.SAPPatientNumber);
                        if (string.IsNullOrEmpty(currentSapPatientId))
                        {
                            processError.Append(CommonConstants.Comma);
                            processError.Append(CommonConstants.InvalidSapPatientId);
                            status = false;
                        }
                        else if (customerToModify.Patients.Where(d => d.SAPPatientNumber == currentSapPatientId).FirstOrDefault() == null)
                        {
                            processError.Append(CommonConstants.Comma);
                            processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidPatientsId, clinicalContact.SAPPatientNumber));
                            status = false;
                        }
                    }

                    // Check for Different Customer
                    if (validCustomerId != customerToModify.CustomerId)
                    {
                        processError.Append(CommonConstants.Comma);
                        processError.Append(CommonConstants.DifferentCustomer);
                        status = false;
                    }
                }
                else
                {
                    processError.Append(CommonConstants.Comma);
                    processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCustomerId, clinicalContact.SAPCustomerNumber));
                    status = false;
                }
            }
            else
            {
                processError.Append(CommonConstants.Comma);
                processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + " CustomerId ");
                status = false;
            }

            #endregion

            #region Mass Chnages Validation

            // Mass Changes Option
            if (!isImport)
            {
                if (string.IsNullOrEmpty(clinicalContact.ClinicalContactTypeId)
                    || string.IsNullOrEmpty(clinicalContact.ClinicalContactType)
                    )
                {
                    processError.Append(CommonConstants.Comma);
                    processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + " ClinicalContact / AnalysisInfo Type / TypeId");
                    status = false;
                }

                if (isPatientClinicalContact && (string.IsNullOrEmpty(clinicalContact.ClinicalContactValue) || string.IsNullOrEmpty(clinicalContact.ClinicalContactValueId)))
                {
                    processError.Append(CommonConstants.Comma);
                    processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ClinicalContactValue / AnalysisInfoValue / ValueId");
                    status = false;
                }
            }

            #endregion

            #region Patient Type Validation


            if (isPatientClinicalContact && (string.IsNullOrEmpty(clinicalContact.ClinicalContactTypeId) || string.IsNullOrEmpty(clinicalContact.ClinicalContactType)))
            {
                processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ClinicalContact / AnalysisInfo Type / TypeId");
                clinicalContact.ErrorMessage = processError.ToString();
                status = false;
            }

            #endregion

            #region Clinical /Analyis Type Validation


            // Check for Clinical Type Id
            if (!string.IsNullOrEmpty(clinicalContact.ClinicalContactTypeId))
            {
                // Validate ClinicalContactTypeId Data Type
                ValidateColumn(clinicalContact.ClinicalContactTypeId, "ClinicalContactTypeId", false, SCAEnums.DataType.Long);
                if (!this.isValidGlobal)
                {
                    processError.Append(CommonConstants.Comma);
                    processError.Append(CommonConstants.INVALID_DATA_TYPE_KEYWORD + clinicalContact.ClinicalContactTypeId);
                    status = false;
                }
                else
                {
                    // Check if Clinical Contact Type is Blank
                    if (string.IsNullOrEmpty(clinicalContact.ClinicalContactType))
                    {
                        processError.Append(CommonConstants.Comma);
                        processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ClinicalContactType.");
                        status = false;
                    }

                    // Checked in Database if exists
                    long clinicalContactListId = Convert.ToInt64(clinicalContact.ClinicalContactTypeId, CultureInfo.InvariantCulture);
                    if (!allClinicalContactMaster.Where(d => d.ClinicalContactAnalysisMasterId == clinicalContactListId).Any())
                    {
                        processError.Append(CommonConstants.Comma);
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.INVALID_CLINICALANALYSIS_TYPE, clinicalContact.ClinicalContactTypeId));
                        status = false;
                    }
                    else
                    {
                        // For Patient, Check that Clinical Contact Type State
                        if (isPatientClinicalContact && customerToModify != null)
                        {
                            if (customerToModify.IDXCustomerMedicalStaffAnalysisInfoes.Where(d => d.MedicalAnalysisListID == clinicalContactListId
                                    && d.IsFieldType == true && d.IsRemoved == true).Any())
                            {
                                processError.Append(CommonConstants.Comma);
                                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.InActiveClinicalContctType, clinicalContact.ClinicalContactType));
                                status = false;
                            }
                        }
                    }

                    // Check for the Clinical Contact Value Id if available
                    if (!string.IsNullOrEmpty(clinicalContact.ClinicalContactValueId))
                    {
                        // Validate ClinicalContactValueId
                        ValidateColumn(clinicalContact.ClinicalContactValueId, "ClinicalContactValueId", false, SCAEnums.DataType.Long);
                        if (!this.isValidGlobal)
                        {
                            processError.Append(CommonConstants.Comma);
                            processError.Append(CommonConstants.INVALID_DATA_TYPE_KEYWORD + clinicalContact.ClinicalContactValueId);
                            status = false;
                        }
                        else
                        {
                            if (customerToModify != null)
                            {
                                // Check if Value Id exist in Database
                                long clinicalContactValueListId = Convert.ToInt64(clinicalContact.ClinicalContactValueId, CultureInfo.InvariantCulture);

                                IDXCustomerMedicalStaffAnalysisInfo idxCustomerMedicalStaffAnalysisInfo = null;
                                idxCustomerMedicalStaffAnalysisInfo = customerToModify.IDXCustomerMedicalStaffAnalysisInfoes.Where(d => d.IDXCustomerMedicalStaffAnalysisInfoID == clinicalContactValueListId
                                 && d.IsFieldType == false).FirstOrDefault();

                                // Check for Contact Name exist by Id
                                if (idxCustomerMedicalStaffAnalysisInfo == null)
                                {
                                    processError.Append(CommonConstants.Comma);
                                    processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.INVALID_CLINICALANALYSIS_VALUE_TYPE, clinicalContact.ClinicalContactValueId));
                                    status = false;
                                }
                                else
                                {
                                    // Check for the Id association with Other clinical type
                                    if (idxCustomerMedicalStaffAnalysisInfo.MedicalAnalysisListID != clinicalContactListId)
                                    {
                                        processError.Append(CommonConstants.Comma);
                                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.INVALID_CONTACT_NAME_FOR_NEW_CONTACT, clinicalContact.ClinicalContactValueId));
                                        status = false;
                                    }
                                }
                            }
                        }

                        // Check for the Clinical Value
                        if (string.IsNullOrEmpty(clinicalContact.ClinicalContactValue))
                        {
                            processError.Append(CommonConstants.Comma);
                            processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ClinicalContactValue");
                            status = false;
                        }
                    }
                }
            }
            else
            {
                // Validate ClinicalContactType, Type can not be blank
                if (string.IsNullOrEmpty(clinicalContact.ClinicalContactType))
                {
                    processError.Append(CommonConstants.Comma);
                    processError.Append(CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ClinicalContactType");
                    status = false;
                }
                else
                {
                    if (allClinicalContactMaster.Where(d => d.ContactName.ToUpper() == clinicalContact.ClinicalContactType.ToUpper().Trim()).Any())
                    {
                        // Log the Message Invalid Clinical Contact Column Type
                        processError.Append(CommonConstants.Comma);
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.CLINICALANALYSIS_ALREADY_EXIST, clinicalContact.ClinicalContactType));
                        status = false;
                    }

                    // Check for Clinical Contact Value Id
                    if (!string.IsNullOrEmpty(clinicalContact.ClinicalContactValueId))
                    {
                        processError.Append(CommonConstants.Comma);
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.INVALID_CONTACT_NAME_FOR_NEW_CONTACT, clinicalContact.ClinicalContactValueId));
                        status = false;
                    }
                }
            }

            #endregion

            if (processError.ToString().StartsWith(",", StringComparison.Ordinal))
            {
                clinicalContact.ErrorMessage = processError.ToString().Substring(1, processError.ToString().Length - 1);
            }
            else
            {
                clinicalContact.ErrorMessage = processError.ToString();
            }
            return status;
        }

        /// <summary>
        /// Adds the update clinical analysis information.
        /// </summary>
        /// <param name="idxCustomerMedicalStaffAnalysisInfo">The index customer medical staff analysis information.</param>
        /// <param name="customerToModify">The customer to modify.</param>
        /// <param name="listid">The list id.</param>
        /// <param name="contactName">Name of the contact.</param>
        /// <param name="isFieldType">if set to <c>true</c> [is field type].</param>
        /// <param name="remarks">The remarks.</param>
        private void AddUpdateClinicalAnalysisInfo(
            IDXCustomerMedicalStaffAnalysisInfo idxCustomerMedicalStaffAnalysisInfo,
            Customer customerToModify,
            long listid,
            string contactName, bool isFieldType, string remarks)
        {
            // Insert into the Clicnical Contact Column 
            idxCustomerMedicalStaffAnalysisInfo.CustomerID = customerToModify.CustomerId;
            idxCustomerMedicalStaffAnalysisInfo.MedicalAnalysisListID = listid;
            idxCustomerMedicalStaffAnalysisInfo.IsRemoved = false;
            idxCustomerMedicalStaffAnalysisInfo.IsMandatory = false;
            idxCustomerMedicalStaffAnalysisInfo.ContactName = contactName;
            idxCustomerMedicalStaffAnalysisInfo.IsFieldType = isFieldType;
            idxCustomerMedicalStaffAnalysisInfo.ModifiedBy = Guid.Parse(globalUserId);
            idxCustomerMedicalStaffAnalysisInfo.ModifiedDate = DateTime.Now;
            idxCustomerMedicalStaffAnalysisInfo.Remarks = remarks;

            // Insert the Value
            customerToModify.IDXCustomerMedicalStaffAnalysisInfoes.Add(idxCustomerMedicalStaffAnalysisInfo);
        }      

        /// <summary>
        /// Adds the update clinical analysis information.
        /// </summary>
        /// <param name="idxCustomerMedicalStaffAnalysisInfo">The index customer medical staff analysis information.</param>
        /// <param name="customerToModify">The customer to modify.</param>
        /// <param name="listid">The list id.</param>
        /// <param name="contactName">Name of the contact.</param>
        /// <param name="isFieldType">if set to <c>true</c> [is field type].</param>
        /// <param name="remarks">The remarks.</param>
        /// <param name="idxCustomerMedicalStaffAnalysisInfoRepository">Customer MedicalStaffAnalysisInfo Repository</param>
        private void AddUpdatePatientClinicalAnalysisInfo(
            IDXCustomerMedicalStaffAnalysisInfo idxCustomerMedicalStaffAnalysisInfo,
            Customer customerToModify, long listid,
            string contactName, bool isFieldType, string remarks, IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository)
        {
            try
            {
                UnitOfWork unitOfWork = idxCustomerMedicalStaffAnalysisInfoRepository.UnitOfWork;

                // Insert into the Clicnical Contact Column 
                idxCustomerMedicalStaffAnalysisInfo.CustomerID = customerToModify.CustomerId;
                idxCustomerMedicalStaffAnalysisInfo.MedicalAnalysisListID = listid;
                idxCustomerMedicalStaffAnalysisInfo.IsRemoved = false;
                idxCustomerMedicalStaffAnalysisInfo.IsMandatory = false;
                idxCustomerMedicalStaffAnalysisInfo.ContactName = contactName;
                idxCustomerMedicalStaffAnalysisInfo.IsFieldType = isFieldType;
                idxCustomerMedicalStaffAnalysisInfo.ModifiedBy = Guid.Parse(globalUserId);
                idxCustomerMedicalStaffAnalysisInfo.ModifiedDate = DateTime.Now;
                idxCustomerMedicalStaffAnalysisInfo.Remarks = remarks;

                // Insert the Value
                idxCustomerMedicalStaffAnalysisInfoRepository.InsertOrUpdate(idxCustomerMedicalStaffAnalysisInfo);
                unitOfWork.Commit();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, globalUserId);
            }
        }

        #endregion

        #region Patient Clinical Contact Analysis

        /// <summary>
        /// Masses the insert patient clinical contact analysis.
        /// </summary>
        /// <param name="clinicalContactAnalysisData">The clinical contact analysis data.</param>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <param name="isImport">is Import</param>        
        /// <returns>List of ClinicalContact AnalysisMassBusinessModel</returns>        
        private List<ClinicalContactAnalysisMassBusinessModel> MassInsertPatientClinicalContactAnalysis(List<ClinicalContactAnalysisMassBusinessModel> clinicalContactAnalysisData, long listTypeId, bool isImport)
        {
            if (clinicalContactAnalysisData.Any())
            {
                // Get All Customer for ClinicalContact Analysis Master
                List<ClinicalContactAnalysisMaster> allClinicalContactAnalysisMaster = this.GetAllClinicalContactMasterData(listTypeId);

                ICustomerRepository customerRepository = null;
                IUnitOfWork unitOfWork = null;

                IDXCustomerMedicalStaffAnalysisInfo idxCustomerMedicalStaffAnalysisInfo = null;

                Customer customerToModify = null;

                var currentSapPatientId = string.Empty;
                long validCustomerId = 0;

                IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository = null;
                try
                {
                    customerRepository = new CustomerRepository();
                    unitOfWork = customerRepository.UnitOfWork;

                    // IDXCustomerMedical Staff Analysis Info Repository
                    idxCustomerMedicalStaffAnalysisInfoRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();

                    List<IDXCustomerMedicalStaffAnalysisInfo> allIdxCustomerMedicalStaffAnalysisInfo = null;

                    // Start the Import Process
                    string clinicalContactTypeId = string.Empty;
                    string clinicalContactType = string.Empty;
                    string clinicalContactValueId = string.Empty;
                    string clinicalContactValue = string.Empty;
                    string remarks = string.Empty;

                    long clinicalContactListId = default(long);
                    long idxAnalysisInfoId = default(long);
                    IDXPatientMedicalStaff idxPatientMedicalStaff = null;

                    IQueryable<Customer> allCustomer = customerRepository.GetAllCustomer();

                    Model.Patient currentPatient = null;

                    List<long> clinicalTypeList = new List<long>();

                    foreach (var patientClinicalContact in clinicalContactAnalysisData)
                    {
                        try
                        {
                            #region Start Mass Update Process

                            // Start Processing
                            if (this.ValidateCustomerClinicalDataclinicalContact(patientClinicalContact, allClinicalContactAnalysisMaster, ref customerToModify, true, isImport, allCustomer, ref validCustomerId))
                            {
                                if (allIdxCustomerMedicalStaffAnalysisInfo == null)
                                {
                                    allIdxCustomerMedicalStaffAnalysisInfo = idxCustomerMedicalStaffAnalysisInfoRepository.All.ToList();
                                }

                                if (clinicalTypeList.Where(d => Convert.ToString(d, CultureInfo.InvariantCulture) == patientClinicalContact.ClinicalContactTypeId).Any() && listTypeId == (long)SCAEnums.ListType.MedicalStaff)
                                {
                                    patientClinicalContact.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.CLINICALANALYSIS_TYPE_ALREADY_EXIST, patientClinicalContact.ClinicalContactTypeId);
                                    patientClinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    // Add the Clinical Type Id to List.
                                    clinicalTypeList.Add(Convert.ToInt64(patientClinicalContact.ClinicalContactTypeId, CultureInfo.InvariantCulture));

                                    currentSapPatientId = CommonHelper.GetAppenedSapNumber(patientClinicalContact.SAPPatientNumber);
                                    currentPatient = customerToModify.Patients.Where(d => d.SAPPatientNumber == currentSapPatientId).FirstOrDefault();

                                    clinicalContactTypeId = patientClinicalContact.ClinicalContactTypeId;
                                    clinicalContactType = patientClinicalContact.ClinicalContactType;
                                    clinicalContactValueId = patientClinicalContact.ClinicalContactValueId;
                                    clinicalContactValue = patientClinicalContact.ClinicalContactValue;
                                    remarks = patientClinicalContact.Remarks;

                                    clinicalContactListId = Convert.ToInt64(clinicalContactTypeId, CultureInfo.InvariantCulture);

                                    // Check for the Clinical Contact Value
                                    if (string.IsNullOrEmpty(clinicalContactValueId))
                                    {
                                        // INSERT 

                                        if (string.IsNullOrEmpty(clinicalContactValue))
                                        {
                                            patientClinicalContact.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.INVALID_CLINICALANALYSIS_VALUE_TYPE, clinicalContactValue);
                                            patientClinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                                        }
                                        else
                                        {
                                            // INSERT TO IDX
                                            idxCustomerMedicalStaffAnalysisInfo = new IDXCustomerMedicalStaffAnalysisInfo();
                                            AddUpdatePatientClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, clinicalContactListId, clinicalContactValue, false, remarks, idxCustomerMedicalStaffAnalysisInfoRepository);

                                            idxPatientMedicalStaff = new IDXPatientMedicalStaff()
                                            {
                                                CustomerMedicalStaffId = idxCustomerMedicalStaffAnalysisInfo.IDXCustomerMedicalStaffAnalysisInfoID,
                                                IsClinicalLinkage = false,
                                                PatientId = currentPatient.PatientId,
                                                ModifiedBy = Guid.Parse(globalUserId),
                                                ModifiedDate = DateTime.Now,
                                                Remarks = patientClinicalContact.Remarks
                                            };

                                            customerToModify.ModifiedDate = DateTime.Now;

                                            customerToModify.Patients.Where(d => d.PatientId == currentPatient.PatientId).FirstOrDefault().IDXPatientMedicalStaffs.Add(idxPatientMedicalStaff);
                                            customerRepository.InsertOrUpdate(customerToModify);
                                            patientClinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                                        }
                                    }
                                    else
                                    {
                                        // UPDATE 
                                        idxAnalysisInfoId = Convert.ToInt64(clinicalContactValueId, CultureInfo.InvariantCulture);

                                        idxCustomerMedicalStaffAnalysisInfo = allIdxCustomerMedicalStaffAnalysisInfo.Find(d => d.IDXCustomerMedicalStaffAnalysisInfoID == idxAnalysisInfoId);

                                        AddUpdatePatientClinicalAnalysisInfo(idxCustomerMedicalStaffAnalysisInfo, customerToModify, Convert.ToInt64(clinicalContactTypeId, CultureInfo.InvariantCulture), clinicalContactValue, false, remarks, idxCustomerMedicalStaffAnalysisInfoRepository);

                                        // Insert into the IDXPatientMedicalStaff also
                                        idxPatientMedicalStaff = customerToModify.Patients
                                            .Where(d => d.PatientId == currentPatient.PatientId).FirstOrDefault()
                                            .IDXPatientMedicalStaffs.Where(q => q.CustomerMedicalStaffId == idxAnalysisInfoId).FirstOrDefault();

                                        if (idxPatientMedicalStaff != null)
                                        {
                                            idxPatientMedicalStaff.CustomerMedicalStaffId = idxCustomerMedicalStaffAnalysisInfo.IDXCustomerMedicalStaffAnalysisInfoID;
                                            idxPatientMedicalStaff.ModifiedBy = Guid.Parse(globalUserId);
                                            idxPatientMedicalStaff.ModifiedDate = DateTime.Now;
                                            idxPatientMedicalStaff.Remarks = patientClinicalContact.Remarks;
                                        }

                                        customerToModify.ModifiedDate = DateTime.Now;
                                        customerToModify.Patients.Where(d => d.PatientId == currentPatient.PatientId).FirstOrDefault().IDXPatientMedicalStaffs.Add(idxPatientMedicalStaff);
                                        customerRepository.InsertOrUpdate(customerToModify);

                                        patientClinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                            else
                            {
                                patientClinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            }

                            #endregion

                        }
                        catch (Exception ex)
                        {
                            patientClinicalContact.ErrorMessage = ex.Message;
                            patientClinicalContact.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        }
                    }

                    // Update the Customer
                    if (customerToModify != null)
                    {
                        customerToModify.ModifiedDate = DateTime.Now;
                        customerRepository.InsertOrUpdate(customerToModify);

                        // Commit the changes 
                        unitOfWork.Commit();

                    }
                }
                catch (DataException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (FormatException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (InvalidOperationException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (SystemException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (Exception e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                finally
                {
                    if (customerRepository != null)
                    {
                        customerRepository.Dispose();
                    }
                    allClinicalContactAnalysisMaster = null;

                    if (idxCustomerMedicalStaffAnalysisInfoRepository != null)
                    {
                        idxCustomerMedicalStaffAnalysisInfoRepository.Dispose();
                    }
                }
            }

            return clinicalContactAnalysisData;
        }

        #endregion

        #region CareHome

        /// <summary>
        /// Masses the insert care home.
        /// </summary>
        /// <param name="careHomeDetailsList">The care home details list.</param>
        /// <returns>
        /// Success or Failure
        /// </returns>
        private bool MassInsertUpdateCareHome(List<MassDataCareHomeBusinessModel> careHomeDetailsList)
        {
            bool success = false;
            if (careHomeDetailsList != null)
            {
                ICareHomeRepository careHomeRepository = null;
                ICustomerRepository customerRepository = null;
                try
                {
                    IUnitOfWork unitOfWork;
                    customerRepository = new CustomerRepository();
                    Guid userID = Guid.Parse(globalUserId);
                    string sapCustomerId = string.Empty;
                    string sapCustomerIdWithZero = string.Empty;
                    bool isLoadIdValid = true;
                    sapCustomerId = careHomeDetailsList.FirstOrDefault().CustomerId;
                    if (!string.IsNullOrEmpty(sapCustomerId))
                    {
                        sapCustomerIdWithZero = CommonHelper.GetAppenedSapNumber(sapCustomerId);
                        EFModel.Customer customerDetails = customerRepository.GetAllCustomer().Where(c => c.SAPCustomerNumber.Equals(sapCustomerIdWithZero, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                        if (customerDetails != null)//Customer is valid
                        {
                            careHomeRepository = new CareHomeRepository();
                            unitOfWork = careHomeRepository.UnitOfWork;
                            this.PopulateData(careHomeDetailsList, careHomeRepository);

                            foreach (MassDataCareHomeBusinessModel careHomeDetails in careHomeDetailsList)
                            {
                                isLoadIdValid = true;
                                InvalidDataType = new StringBuilder();
                                MandatoryFields = new StringBuilder();
                                InvalidLength = new StringBuilder();
                                CustomValidation = new StringBuilder();
                                if (sapCustomerId == careHomeDetails.CustomerId)
                                {
                                    if (string.IsNullOrEmpty(careHomeDetails.TransactionStatus))
                                    {
                                        isLoadIdValid = this.ValidateLoadId(careHomeDetailsList, careHomeDetails);

                                        if (isLoadIdValid)
                                        {
                                            var isSapCareHomeIdDuplicate = this.IsDuplicateSapCareHomeId(careHomeDetailsList, careHomeDetails);
                                            if (!isSapCareHomeIdDuplicate && string.IsNullOrEmpty(careHomeDetails.TransactionStatus))
                                            {
                                                this.InsertUpdateCareHome(careHomeRepository, customerDetails.CustomerId, careHomeDetails, userID);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                                    CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCustomerId, careHomeDetails.CustomerId));
                                }

                                if (careHomeDetails.TransactionStatus == SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture))
                                {
                                    careHomeDetails.ErrorMessage = this.GetErrorMessage();
                                }
                            }

                            if (careHomeDetailsList.Where(c => c.TransactionStatus == SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture)).FirstOrDefault() != null)
                            {
                                success = true;
                                unitOfWork.Commit();
                                if (isImport)
                                {
                                    var addedCarehomeData = careHomeRepository.GetAddedCarehomeEntities();
                                    addedCarehomeData.ForEach(addedCarehome => careHomeDetailsList.First(c => c.LoadId.Equals(Convert.ToString(addedCarehome.LoadId, CultureInfo.InvariantCulture), StringComparison.OrdinalIgnoreCase)).IndigoCarehomeId = Convert.ToString(addedCarehome.CareHomeId, CultureInfo.InvariantCulture));
                                }
                            }
                        }
                        else // Customer is not valid
                        {
                            careHomeDetailsList.FirstOrDefault().ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCustomerId, sapCustomerId);
                            careHomeDetailsList.FirstOrDefault().TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        }
                    }
                    else // Customer Id is blank
                    {
                        careHomeDetailsList.FirstOrDefault().ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCustomerId, sapCustomerId);
                        careHomeDetailsList.FirstOrDefault().TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    }

                    // Write Log File               
                    if (careHomeDetailsList.Any())
                    {
                        removeColumn = new List<string>() { "CarehomeName", "UserId", "SelectedTemplate", "FilePath", "IsImport", "CareHomeStatusId", "CareHomeTypeId", "OrderTypeId", "UILogIPAddress" };
                        ReturnLogFilePath = CommonService.ExportToExcel(careHomeDetailsList, SCAEnums.MassDataTemplate.Carehome.ToString(CultureInfo.InvariantCulture),
                                            massDataFileName, removeColumn, this.massChangesLogs, true);
                    }
                }
                catch (DataException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (FormatException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (InvalidOperationException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (SystemException e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                catch (Exception e)
                {
                    LogHelper.LogException(e, globalUserId);
                }
                finally
                {
                    if (careHomeRepository != null)
                    {
                        careHomeRepository.Dispose();
                    }

                    if (customerRepository != null)
                    {
                        customerRepository.Dispose();
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Validates the load identifier.
        /// </summary>
        /// <param name="careHomeDetailsList">The care home details list.</param>
        /// <param name="isLoadIdValid">if set to <c>true</c> [is load identifier valid].</param>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <returns></returns>
        private bool ValidateLoadId(List<MassDataCareHomeBusinessModel> careHomeDetailsList, MassDataCareHomeBusinessModel careHomeDetails)
        {
            bool isLoadIdValid = true;
            if (isImport)
            {
                if (string.IsNullOrEmpty(careHomeDetails.LoadId))
                {
                    MandatoryFields.Append(", LoadID");
                    careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    isLoadIdValid = false;
                }
                else if (!CommonHelper.ValidateInteger(careHomeDetails.LoadId))
                {
                    InvalidDataType.Append(", LoadID");
                    careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    isLoadIdValid = false;
                }
                else
                {
                    var data = careHomeDetailsList.Where(c => c.LoadId == careHomeDetails.LoadId && !string.IsNullOrEmpty(c.TransactionStatus)).FirstOrDefault();
                    if (data != null)
                    {
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.DuplicateLoadId, careHomeDetails.LoadId));
                        careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        isLoadIdValid = false;
                    }
                }
            }

            return isLoadIdValid;
        }

        /// <summary>
        /// Populates the data.
        /// </summary>
        /// <param name="careHomeDetailsList">The care home details list.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        private void PopulateData(List<MassDataCareHomeBusinessModel> careHomeDetailsList, ICareHomeRepository careHomeRepository)
        {
            StringBuilder listTypeIds = new StringBuilder();
            List<string> countyCodeList = new List<string>();
            List<string> sapCareHomeIdList = new List<string>();
            List<long> careHomeIdList = new List<long>();
            listTypeIds.Append(Convert.ToString(Convert.ToInt64(SCAEnums.ListType.CareHomeStatus, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture));
            listTypeIds.Append("," + Convert.ToString(Convert.ToInt64(SCAEnums.ListType.CareHomeType, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture));
            listTypeIds.Append("," + Convert.ToString(Convert.ToInt64(SCAEnums.ListType.CareHomeOrderType, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture));
            string languageId = Convert.ToString(Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);

            foreach (MassDataCareHomeBusinessModel careHomeDetails in careHomeDetailsList)
            {
                if (!string.IsNullOrEmpty(careHomeDetails.County) && !countyCodeList.Contains(careHomeDetails.County))
                {
                    countyCodeList.Add(careHomeDetails.County);
                }

                if (!string.IsNullOrEmpty(careHomeDetails.SAPCarehomeId) && !sapCareHomeIdList.Contains(careHomeDetails.SAPCarehomeId))
                {
                    sapCareHomeIdList.Add(CommonHelper.GetAppenedSapNumber(careHomeDetails.SAPCarehomeId));
                }

                if (!isImport && CommonHelper.ValidateInteger(careHomeDetails.IndigoCarehomeId)
                    && !careHomeIdList.Contains(long.Parse(careHomeDetails.IndigoCarehomeId, CultureInfo.InvariantCulture)))
                {
                    careHomeIdList.Add(long.Parse(careHomeDetails.IndigoCarehomeId, CultureInfo.InvariantCulture));
                }
            }

            if (countyCodeList != null && countyCodeList.Count() > 0)
            {
                this.PopulateCountyList(countyCodeList);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(listTypeIds, CultureInfo.InvariantCulture)))
            {
                this.PopulateListData(listTypeIds.ToString(), languageId);
            }

            this.PopulateAllCountry();
            existingCarehomeList = careHomeRepository.GetAllCareHomesBySapCareHomeId(sapCareHomeIdList).ToList();
            if (careHomeIdList != null && careHomeIdList.Count > 0)
            {
                carehomeList = careHomeRepository.All.Where(c => careHomeIdList.Contains(c.CareHomeId)).ToList();
            }
        }

        /// <summary>
        /// Inserts the update care home.
        /// </summary>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <param name="userId">The user identifier.</param>
        private void InsertUpdateCareHome(ICareHomeRepository careHomeRepository, long customerId, MassDataCareHomeBusinessModel careHomeDetails, Guid userId)
        {
            EFModel.CareHome careHome = null;
            EFModel.PersonalInformation personalInfo = null;
            EFModel.Address address = null;
            bool isCareHomeValid = true;
            if (!isImport)
            {
                if (CommonHelper.ValidateInteger(careHomeDetails.IndigoCarehomeId) && long.Parse(careHomeDetails.IndigoCarehomeId, CultureInfo.InvariantCulture) > 0)
                {
                    careHome = carehomeList.Where(c => c.CareHomeId == long.Parse(careHomeDetails.IndigoCarehomeId, CultureInfo.InvariantCulture)).FirstOrDefault();
                    if (careHome != null)
                    {
                        personalInfo = careHome.PersonalInformation;
                        address = personalInfo.Address;
                    }
                    else
                    {
                        isCareHomeValid = false;
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.CarehomeIdNotExists, careHomeDetails.IndigoCarehomeId));
                        careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    }
                }
                else if (string.IsNullOrEmpty(careHomeDetails.IndigoCarehomeId))
                {
                    isCareHomeValid = false;
                    MandatoryFields.Append(", IndigoCarehomeId");
                    careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    isCareHomeValid = false;
                    CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCarehomeId, careHomeDetails.IndigoCarehomeId));
                    careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                }
            }
            else
            {
                careHome = new EFModel.CareHome();
                personalInfo = new EFModel.PersonalInformation();
                address = new EFModel.Address();
            }

            if (isCareHomeValid && ValidateCareHomeData(careHomeDetails))
            {
                // Set Address details
                SetAddressDetails(careHomeDetails, userId, address);
                // Set PersonalInformation details                     
                SetPresonalInformationDetails(careHomeDetails, userId, personalInfo, address);
                // Set CareHome details
                SetCareHomeDetails(careHomeDetails, userId, personalInfo, careHome, customerId);
                careHomeRepository.InsertOrUpdate(careHome);
                careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Sets the address details.
        /// </summary>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="address">The address.</param>
        private void SetAddressDetails(MassDataCareHomeBusinessModel careHomeDetails, Guid userId, Address address)
        {
            address.HouseName = careHomeDetails.HouseName;
            address.AddressLine1 = careHomeDetails.AddressLine1;
            address.AddressLine2 = careHomeDetails.AddressLine2;
            address.City = careHomeDetails.TownOrCity;
            address.County = careHomeDetails.County;
            address.Country = careHomeDetails.Country;
            address.PostCode = careHomeDetails.PostCode;
            address.ModifiedBy = userId;
            address.ModifiedDate = currentDate;
        }

        /// <summary>
        /// Sets the presonal information details.
        /// </summary>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="personalInfo">The personal information.</param>
        /// <param name="address">The address.</param>
        private void SetPresonalInformationDetails(MassDataCareHomeBusinessModel careHomeDetails, Guid userId, PersonalInformation personalInfo, Address address)
        {
            personalInfo.FirstName = careHomeDetails.HouseName;
            personalInfo.Address = address;
            personalInfo.TelephoneNumber = careHomeDetails.PhoneNumber;
            personalInfo.MobileNumber = careHomeDetails.Mobile;
            personalInfo.Email = careHomeDetails.Email;
            if (isImport)
            {
                personalInfo.CreatedById = userId;
                personalInfo.CreatedDateTime = currentDate;
            }

            personalInfo.ModifiedBy = userId;
            personalInfo.ModifiedDate = currentDate;

        }

        /// <summary>
        /// Sets the care home details.
        /// </summary>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="personalInfo">The personal information.</param>
        /// <param name="careHome">The care home.</param>
        /// <param name="customerId">The customer identifier.</param>
        private void SetCareHomeDetails(MassDataCareHomeBusinessModel careHomeDetails, Guid userId, PersonalInformation personalInfo, EFModel.CareHome careHome, long customerId)
        {
            var sapCareHomeId = CommonHelper.GetAppenedSapNumber(careHomeDetails.SAPCarehomeId);
            if (isImport)
            {
                careHome.LoadId = long.Parse(careHomeDetails.LoadId, CultureInfo.InvariantCulture);
            }

            careHome.CustomerId = customerId;
            careHome.SAPCareHomeNumber = !string.IsNullOrEmpty(sapCareHomeId) == true ? sapCareHomeId : null;
            careHome.DeliveryFrequency = careHomeDetails.DeliveryFrequency;
            careHome.RoundId = careHomeDetails.RoundId;
            careHome.Round = careHomeDetails.Round;
            careHome.NextDeliveryDate = this.ConvertDateToMMddyyyy(careHomeDetails.NextDeliveryDate);
            if (!string.IsNullOrEmpty(careHomeDetails.CareHomeStatusId))
            {
                careHome.CareHomeStatus = long.Parse(careHomeDetails.CareHomeStatusId, CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(careHomeDetails.CareHomeTypeId))
            {
                careHome.CareHomeType = long.Parse(careHomeDetails.CareHomeTypeId, CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(careHomeDetails.OrderTypeId))
            {
                careHome.OrderType = long.Parse(careHomeDetails.OrderTypeId, CultureInfo.InvariantCulture);
            }

            if (CommonHelper.ValidateInteger(careHomeDetails.PurchaseOrderNumber))
            {
                careHome.PurchaseOrderNo = Convert.ToString(careHomeDetails.PurchaseOrderNumber);
            }

            careHome.IsSentToSAP = this.ConvertYesNoToBool(careHomeDetails.SendToSAP);
            careHome.Remarks = careHomeDetails.Remarks;
            careHome.ModifiedBy = userId;
            careHome.ModifiedDate = currentDate;
            careHome.PersonalInformation = personalInfo;
        }

        /// <summary>
        /// Validates the care home data.
        /// </summary>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <returns>
        /// True or False
        /// </returns>
        private bool ValidateCareHomeData(MassDataCareHomeBusinessModel careHomeDetails)
        {
            bool isValid = true;
            try
            {
                string round = ConfigurationManager.AppSettings["PatientRoundFormat"].ToString(CultureInfo.CurrentCulture);
                if (string.IsNullOrEmpty(careHomeDetails.CustomerId))
                {
                    MandatoryFields.Append(", CustomerID");
                    isValid = false;
                }
                else if (careHomeDetails.CustomerId.Length > 100)
                {
                    InvalidLength.Append(", CustomerID");
                }

                if (!string.IsNullOrEmpty(careHomeDetails.SAPCarehomeId) && careHomeDetails.SAPCarehomeId.Length > 10)
                {
                    InvalidLength.Append(", SAPCarehomeID");
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.HouseName))
                {
                    MandatoryFields.Append(", HouseName");
                    isValid = false;
                }
                else if (careHomeDetails.HouseName.Length > 64)
                {
                    InvalidLength.Append(", HouseName");
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.AddressLine1))
                {
                    MandatoryFields.Append(", AddressLine1");
                    isValid = false;
                }
                else if (careHomeDetails.AddressLine1.Length > 160)
                {
                    InvalidLength.Append(", AddressLine1");
                    isValid = false;
                }

                if (!string.IsNullOrEmpty(careHomeDetails.AddressLine2) && careHomeDetails.AddressLine2.Length > 160)
                {
                    InvalidLength.Append(", AddressLine2");
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.TownOrCity))
                {
                    MandatoryFields.Append(", TownOrCity");
                    isValid = false;
                }
                else if (careHomeDetails.TownOrCity.Length > 128)
                {
                    InvalidLength.Append(", TownOrCity");
                }

                if (string.IsNullOrEmpty(careHomeDetails.County))
                {
                    MandatoryFields.Append(", County");
                    isValid = false;
                }
                else if (careHomeDetails.County.Length > 100)
                {
                    InvalidLength.Append(", County");
                    isValid = false;
                }
                else
                {
                    var countyDetails = allCountyList.Where(c => c.CountyCode.Equals(careHomeDetails.County, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (countyDetails == null)
                    {
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCountyCode, careHomeDetails.County));
                        isValid = false;
                    }
                    else
                    {
                        careHomeDetails.County = countyDetails.CountyCode;
                    }
                }

                if (string.IsNullOrEmpty(careHomeDetails.Country))
                {
                    MandatoryFields.Append(", Country");
                    isValid = false;
                }
                else if (careHomeDetails.Country.Length > 20)
                {
                    InvalidLength.Append(", Country");
                    isValid = false;
                }
                else
                {
                    var countrydata = allCountryList.Where(c => c.CountryCode.Equals(careHomeDetails.Country, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (countrydata == null)
                    {
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCountryCode, careHomeDetails.Country));
                        isValid = false;
                    }
                }

                if (string.IsNullOrEmpty(careHomeDetails.PostCode))
                {
                    MandatoryFields.Append(", PostCode");
                    isValid = false;
                }
                else if (careHomeDetails.PostCode.Length > 20)
                {
                    InvalidLength.Append(", PostCode");
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.PhoneNumber))
                {
                    MandatoryFields.Append(", PhoneNumber");
                    isValid = false;
                }
                else if (careHomeDetails.PhoneNumber.Length > 40)
                {
                    InvalidLength.Append(", PhoneNumber");
                    isValid = false;
                }

                if (!string.IsNullOrEmpty(careHomeDetails.Mobile) && careHomeDetails.Mobile.Length > 40)
                {
                    InvalidLength.Append(", Mobile");
                    isValid = false;
                }

                if (!string.IsNullOrEmpty(careHomeDetails.Email) && careHomeDetails.Email.Length > 100)
                {
                    InvalidLength.Append(", Email");
                    isValid = false;
                }
                else if (!CommonHelper.ValidateEmail(careHomeDetails.Email))
                {
                    CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidEmail, careHomeDetails.Email));
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.CarehomeStatus))
                {
                    MandatoryFields.Append(", CarehomeStatus");
                    isValid = false;
                }
                else
                {
                    var list = listModel.Where(l => l.ListCode.Equals(careHomeDetails.CarehomeStatus, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (list == null)
                    {
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCarehomeStatus, careHomeDetails.CarehomeStatus));
                        isValid = false;
                    }
                    else
                    {
                        careHomeDetails.CareHomeStatusId = Convert.ToString(list.ListId, CultureInfo.InvariantCulture);
                    }
                }

                if (string.IsNullOrEmpty(careHomeDetails.NextDeliveryDate))
                {
                    MandatoryFields.Append(", NextDeliveryDate");
                    isValid = false;
                }
                else if (!CommonHelper.ValidateDate(careHomeDetails.NextDeliveryDate))
                {
                    CustomValidation.Append(", " + CommonConstants.InvalidNextDeliveryDate);
                    isValid = false;
                }
                else if (ConvertDateToMMddyyyy(careHomeDetails.NextDeliveryDate) < currentDate)
                {
                    CustomValidation.Append(", " + CommonConstants.SmallerNextDeliveryDate);
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.Round))
                {
                    MandatoryFields.Append(", Round");
                    isValid = false;
                }
                else if (careHomeDetails.Round.Length != 10 || (careHomeDetails.Round.Length == 10 && (!careHomeDetails.Round.Substring(0, 8).Equals(round, StringComparison.OrdinalIgnoreCase)) || !CommonHelper.ValidateInteger(careHomeDetails.Round.Substring(8, 2))))
                {
                    CustomValidation.Append(", " + CommonConstants.InvalidRound);
                    isValid = false;
                }

                if (!string.IsNullOrEmpty(careHomeDetails.RoundId) && careHomeDetails.RoundId.Length > 4)
                {
                    InvalidLength.Append(", RoundID");
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.DeliveryFrequency))
                {
                    MandatoryFields.Append(", DeliveryFrequency");
                    isValid = false;
                }
                else if (careHomeDetails.DeliveryFrequency.Length > 2)
                {
                    InvalidLength.Append(", DeliveryFrequency");
                    isValid = false;
                }
                else if (!CommonHelper.ValidateInteger(careHomeDetails.DeliveryFrequency))
                {
                    InvalidDataType.Append(", DeliveryFrequency");
                    isValid = false;
                }

                if (!string.IsNullOrEmpty(careHomeDetails.CarehomeType))
                {
                    var list = listModel.Where(l => l.ListCode.Equals(careHomeDetails.CarehomeType, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (list == null)
                    {
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCarehomeType, careHomeDetails.CarehomeType));
                        isValid = false;
                    }
                    else
                    {
                        careHomeDetails.CareHomeTypeId = Convert.ToString(list.ListId, CultureInfo.InvariantCulture);
                    }
                }

                if (string.IsNullOrEmpty(careHomeDetails.OrderType))
                {
                    MandatoryFields.Append(", OrderType");
                    isValid = false;
                }
                else
                {
                    var list = listModel.Where(l => l.ListCode.Equals(careHomeDetails.OrderType, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (list == null)
                    {
                        CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidOrderType, careHomeDetails.OrderType));
                        isValid = false;
                    }
                    else
                    {
                        careHomeDetails.OrderTypeId = Convert.ToString(list.ListId, CultureInfo.InvariantCulture);
                    }
                }

                if (!CommonHelper.ValidateInteger(careHomeDetails.PurchaseOrderNumber))
                {
                    InvalidDataType.Append(", PurchaseOrderNumber");
                    isValid = false;
                }

                if (string.IsNullOrEmpty(careHomeDetails.SendToSAP))
                {
                    MandatoryFields.Append(", SendToSAP");
                    isValid = false;
                }
                else if (!careHomeDetails.SendToSAP.Equals(CommonConstants.Yes, StringComparison.OrdinalIgnoreCase) && !careHomeDetails.SendToSAP.Equals(CommonConstants.No, StringComparison.OrdinalIgnoreCase))
                {
                    CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.MassDataInvalidYesNoMessage, "SendToSAP"));
                    isValid = false;
                }

                if (!CommonHelper.ValidateInteger(careHomeDetails.BillTo))
                {
                    InvalidDataType.Append(", BillTo");
                    isValid = false;
                }

                if (!isValid)
                {
                    careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, globalUserId);
            }

            return isValid;
        }

        /// <summary>
        /// Determines whether [is duplicate sap care home identifier] [the specified care home details list].
        /// </summary>
        /// <param name="careHomeDetailsList">The care home details list.</param>
        /// <param name="careHomeDetails">The care home details.</param>
        /// <returns></returns>
        private bool IsDuplicateSapCareHomeId(List<MassDataCareHomeBusinessModel> careHomeDetailsList, MassDataCareHomeBusinessModel careHomeDetails)
        {
            bool isDuplicate = false;
            var data = careHomeDetailsList.Where(c => c.SAPCarehomeId == careHomeDetails.SAPCarehomeId && !string.IsNullOrEmpty(c.TransactionStatus) && !string.IsNullOrEmpty(careHomeDetails.SAPCarehomeId)).FirstOrDefault();
            if (data != null)
            {
                CustomValidation.Append(", " + CommonConstants.InvalidSapCareHomeId);
                careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                isDuplicate = true;
            }
            else if (existingCarehomeList != null && existingCarehomeList.Count > 0 && !string.IsNullOrEmpty(careHomeDetails.SAPCarehomeId))
            {
                var sapCareHomeId = CommonHelper.GetAppenedSapNumber(careHomeDetails.SAPCarehomeId);
                if (isImport && !string.IsNullOrEmpty(careHomeDetails.SAPCarehomeId))
                {
                    var careHome = existingCarehomeList.Where(x => x.SAPCareHomeNumber == sapCareHomeId).FirstOrDefault();
                    if (careHome != null)
                    {
                        CustomValidation.Append(", " + CommonConstants.InvalidSapCareHomeId);
                        careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.CurrentCulture);
                        isDuplicate = true;
                    }
                }
                else if (!string.IsNullOrEmpty(careHomeDetails.SAPCarehomeId) && CommonHelper.ValidateInteger(careHomeDetails.IndigoCarehomeId))
                {
                    var careHome = existingCarehomeList.Where(x => x.SAPCareHomeNumber == sapCareHomeId && x.CareHomeId != Convert.ToInt64(careHomeDetails.IndigoCarehomeId, CultureInfo.InvariantCulture)).FirstOrDefault();
                    if (careHome != null)
                    {
                        CustomValidation.Append(", " + CommonConstants.InvalidSapCareHomeId);
                        careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        isDuplicate = true;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(careHomeDetails.IndigoCarehomeId))
                    {
                        MandatoryFields.Append(", IndigoCarehomeId");
                        careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    }
                    else if (!CommonHelper.ValidateInteger(careHomeDetails.IndigoCarehomeId))
                    {
                        InvalidDataType.Append(", IndigoCarehomeId");
                        careHomeDetails.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    }
                }
            }

            return isDuplicate;
        }

        #endregion

        #region PostCode Matrix

        /// <summary>
        /// Masses the import post code matrix.
        /// </summary>
        /// <param name="postCodeMatrixBusinessModelList">The post code matrix business model list.</param>
        /// <returns></returns>
        private List<MassDataPostCodeMatrixBusinessModel> MassImportPostCodeMatrix(List<MassDataPostCodeMatrixBusinessModel> postCodeMatrixBusinessModelList)
        {
            ICustomerRepository customerRepository = null;
            List<MassDataPostCodeMatrixBusinessModel> returnMassDataPostCodeMatrixBusinessModel = new List<MassDataPostCodeMatrixBusinessModel>();
            List<Customer> allCustomer = new List<Customer>();
            try
            {
                if (postCodeMatrixBusinessModelList != null)
                {
                    PostcodeMatrix postcodeMatrix = null;
                    IUnitOfWork unitOfWork = null;
                    DateTime modifiedDate = DateTime.Now;
                    Customer customerToModify = null;

                    /// Get All Customer for Validation
                    customerRepository = new CustomerRepository();
                    unitOfWork = customerRepository.UnitOfWork;

                    allCustomer = customerRepository.GetAllCustomer().ToList();

                    foreach (var postCodeMatrixCode in postCodeMatrixBusinessModelList)
                    {
                        try
                        {
                            isValidGlobal = true;
                            if (this.ValidatePostCodeMatrix(postCodeMatrixCode, allCustomer, out customerToModify, out postcodeMatrix))
                            {
                                if (postcodeMatrix == null)
                                {
                                    postcodeMatrix = new PostcodeMatrix();
                                    postcodeMatrix.Round = postCodeMatrixCode.Round;
                                    postcodeMatrix.RoundID = postCodeMatrixCode.RoundId;
                                    postcodeMatrix.Postcode = postCodeMatrixCode.PostCode;
                                    postcodeMatrix.CustomerID = customerToModify.CustomerId;
                                }

                                postcodeMatrix.Monday = this.ConvertYesNoToBool(postCodeMatrixCode.Monday);
                                postcodeMatrix.Tuesday = this.ConvertYesNoToBool(postCodeMatrixCode.Tuesday);
                                postcodeMatrix.Wednesday = this.ConvertYesNoToBool(postCodeMatrixCode.Wednesday);
                                postcodeMatrix.Thursday = this.ConvertYesNoToBool(postCodeMatrixCode.Thursday);
                                postcodeMatrix.Friday = this.ConvertYesNoToBool(postCodeMatrixCode.Friday);
                                postcodeMatrix.IsActive = this.ConvertYesNoToBool(postCodeMatrixCode.IsActive);
                                postcodeMatrix.Remarks = Convert.ToString(postCodeMatrixCode.Remarks, CultureInfo.InvariantCulture);
                                postcodeMatrix.ModifiedDate = modifiedDate;
                                postcodeMatrix.ModifiedBy = Guid.Parse(globalUserId);
                                customerToModify.PostcodeMatrices.Add(postcodeMatrix);
                                customerToModify.ModifiedDate = modifiedDate;
                                customerRepository.InsertOrUpdate(customerToModify);
                                postCodeMatrixCode.TransactionStatus = CommonConstants.Success;
                            }
                            else
                            {
                                postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                                postCodeMatrixCode.ErrorMessage = this.GetErrorMessage();
                            }
                        }
                        catch (DataException e)
                        {
                            postCodeMatrixCode.ErrorMessage = e.Message;
                            postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (ArgumentException e)
                        {
                            postCodeMatrixCode.ErrorMessage = e.Message;
                            postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (FormatException e)
                        {
                            postCodeMatrixCode.ErrorMessage = e.Message;
                            postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (InvalidOperationException e)
                        {
                            postCodeMatrixCode.ErrorMessage = e.Message;
                            postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (SystemException e)
                        {
                            postCodeMatrixCode.ErrorMessage = e.Message;
                            postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (Exception e)
                        {
                            postCodeMatrixCode.ErrorMessage = e.Message;
                            postCodeMatrixCode.TransactionStatus = CommonConstants.Failed;
                            LogHelper.LogException(e, globalUserId);
                        }

                        returnMassDataPostCodeMatrixBusinessModel.Add(postCodeMatrixCode);
                    }

                    unitOfWork.Commit();
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }

                if (returnMassDataPostCodeMatrixBusinessModel.Any())
                {
                    removeColumn = new List<string> { "PostcodeMatrixID", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                    ReturnLogFilePath = CommonService.ExportToExcel(returnMassDataPostCodeMatrixBusinessModel, SCAEnums.MassDataTemplate.PostCodeMatrix.ToString(CultureInfo.InvariantCulture),
                                        massDataFileName, removeColumn, this.massChangesLogs, true);
                }
            }
            return postCodeMatrixBusinessModelList;
        }

        /// <summary>
        /// Validates the post code matrix.
        /// </summary>
        /// <param name="postCodeMatrixBusinessModel">The post code matrix business model.</param>
        /// <returns></returns>
        private bool ValidatePostCodeMatrix(MassDataPostCodeMatrixBusinessModel postCodeMatrixBusinessModel, List<Customer> allCustomer, out Customer customerToModify, out PostcodeMatrix postcodeMatrix)
        {
            string sapCustomerNumber = string.Empty;
            MandatoryFields = new StringBuilder();
            InvalidDataType = new StringBuilder();
            DatabaseValidation = new StringBuilder();
            CustomValidation = new StringBuilder();
            postcodeMatrix = null;
            customerToModify = null;
            isValidGlobal = true;

            if (!string.IsNullOrEmpty(postCodeMatrixBusinessModel.CustomerId))
            {
                sapCustomerNumber = CommonHelper.GetAppenedSapNumber(postCodeMatrixBusinessModel.CustomerId);
                customerToModify = allCustomer.Find(d => d.SAPCustomerNumber != null && d.SAPCustomerNumber.ToUpper() == sapCustomerNumber.ToUpper());
                if (customerToModify == null)
                {
                    isValidGlobal = false;
                    CustomValidation.Append("," + CommonConstants.InvalidCustomerId);
                }
                else
                {

                    // Check if its a new record,            
                    postcodeMatrix = customerToModify.PostcodeMatrices.Where(d => d.Round == postCodeMatrixBusinessModel.Round && d.RoundID == postCodeMatrixBusinessModel.RoundId).FirstOrDefault();
                    if (isImport)
                    {
                        if (postcodeMatrix != null)
                        {
                            isValidGlobal = false;
                            CustomValidation.Append(',' + CommonConstants.PostCodeMatrixExists);
                        }
                    }
                    else
                    {
                        if (postcodeMatrix == null)
                        {
                            isValidGlobal = false;
                            CustomValidation.Append(',' + CommonConstants.NewPostCodeMatrix);
                        }
                    }
                }
            }

            ValidateColumn(postCodeMatrixBusinessModel.CustomerId, "Customer ID", true, SCAEnums.DataType.NoCheck);
            ValidateColumn(postCodeMatrixBusinessModel.Round, "Round", true, SCAEnums.DataType.String, 20);
            ValidateColumn(postCodeMatrixBusinessModel.RoundId, "Round Id", true, SCAEnums.DataType.String, 50);
            ValidateColumn(postCodeMatrixBusinessModel.PostCode, "Post Code", false, SCAEnums.DataType.String, 20);
            ValidateColumn(postCodeMatrixBusinessModel.Monday, "Monday", true, SCAEnums.DataType.Bool);
            ValidateColumn(postCodeMatrixBusinessModel.Tuesday, "Tuesday", true, SCAEnums.DataType.Bool);
            ValidateColumn(postCodeMatrixBusinessModel.Wednesday, "Wednesday", true, SCAEnums.DataType.Bool);
            ValidateColumn(postCodeMatrixBusinessModel.Thursday, "Thursday", true, SCAEnums.DataType.Bool);
            ValidateColumn(postCodeMatrixBusinessModel.Friday, "Friday", true, SCAEnums.DataType.Bool);
            ValidateColumn(postCodeMatrixBusinessModel.IsActive, "IsActive", true, SCAEnums.DataType.Bool);
            return isValidGlobal;
        }

        /// <summary>
        /// Converts the yes no to bool.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// true/ false
        /// </returns>
        private bool ConvertYesNoToBool(string value)
        {
            if (value.ToUpper() == CommonConstants.Yes)
            {
                return true;
            }

            if (value.ToUpper() == CommonConstants.No)
            {
                return false;
            }

            return false;
        }

        #endregion

        #region Patient Import

        /// <summary>
        /// Import Patient Data
        /// </summary>
        /// <param name="excelFactory">Excel Query Factory</param>
        /// <param name="isImport">if set to <c>true</c> [is import].</param>
        private void ImportPatientData(ExcelQueryFactory excelFactory, bool isImport = true)
        {
            IPatientRepository patientRepository = null;
            ICustomerRepository customerRepository = null;
            IPrescriptionRepository prescriptionRepository = null;

            try
            {
                patientRepository = new PatientRepository();
                customerRepository = new CustomerRepository();
                prescriptionRepository = new PrescriptionRepository();
                IUnitOfWork unitOfWork = prescriptionRepository.UnitOfWork;

                var patientList = ReadPatientData(excelFactory);
                ///Get list details for patient
                GetListDetailsforPatient();

                if (patientList != null && patientList.Count > 0)
                {
                    objCustomer = customerRepository.GetCustomerBySAPId(CommonHelper.AppendZeroAtStart(patientList[0].CustomerID, CommonConstants.SapIdLength));
                    if (objCustomer != null)
                    {
                        string strSAPPatientId = string.Empty;

                        globalCustomerId = objCustomer.CustomerId;
                        patientLoadIds = new List<string>();
                        sapPatientIds = new List<string>();
                        List<long> patientIds = new List<long>();
                        List<string> countyCodeList = new List<string>();
                        patientList.ForEach(d =>
                        {
                            if (isImport)
                            {
                                if (!string.IsNullOrEmpty(d.LoadId) && CommonHelper.ValidateInteger(d.LoadId))
                                {
                                    if (patientLoadIds.Count > 0 && patientLoadIds.Contains(d.LoadId))
                                    {
                                        d.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.DuplicateLoadId, d.LoadId);
                                        d.TransactionStatus = CommonConstants.Failed;
                                    }
                                    else
                                    {
                                        patientLoadIds.Add(d.LoadId);
                                    }
                                }
                                else
                                {
                                    d.ErrorMessage = CommonConstants.InvalidLoadId;
                                    d.TransactionStatus = CommonConstants.Failed;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(d.IndigoPatientID) && CommonHelper.ValidateInteger(d.IndigoPatientID))
                                {
                                    patientIds.Add(Convert.ToInt64(d.IndigoPatientID, CultureInfo.InvariantCulture));
                                }
                                else
                                {
                                    d.ErrorMessage = d.ErrorMessage + " " + CommonConstants.InvalidPatientId;
                                    d.TransactionStatus = CommonConstants.Failed;
                                }

                                if (!string.IsNullOrEmpty(d.IndigoCarehomeID) && !CommonHelper.ValidateInteger(d.IndigoCarehomeID))
                                {
                                    d.ErrorMessage = d.ErrorMessage + " " + CommonConstants.InvalidCarehomeId;
                                    d.TransactionStatus = CommonConstants.Failed;
                                }
                            }

                            if (!string.IsNullOrEmpty(d.County))
                            {
                                countyCodeList.Add(d.County);
                            }
                            else
                            {
                                d.ErrorMessage = d.ErrorMessage + " " + CommonConstants.InvalidCounty;
                                d.TransactionStatus = CommonConstants.Failed;
                            }
                            // Check for valid SAP Patient Id
                            if (!string.IsNullOrEmpty(d.PatientSAPID))
                            {
                                if (CommonHelper.ValidateInteger(d.PatientSAPID))
                                {
                                    strSAPPatientId = CommonHelper.AppendZeroAtStart(d.PatientSAPID, CommonConstants.SapIdLength);
                                    if (sapPatientIds.Count > 0 && sapPatientIds.Contains(strSAPPatientId))
                                    {
                                        d.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.DuplicateSAPPatientIdinExcel, d.PatientSAPID);
                                        d.TransactionStatus = CommonConstants.Failed;
                                    }
                                    else
                                    {
                                        sapPatientIds.Add(strSAPPatientId);
                                    }
                                }
                                else
                                {
                                    d.ErrorMessage = d.ErrorMessage + " " + CommonConstants.InvalidSapPatientId;
                                    d.TransactionStatus = CommonConstants.Failed;
                                }
                            }
                        });

                        EFModel.CustomerParameter objCustomerParameter = objCustomer.CustomerParameters.FirstOrDefault();
                        if (objCustomerParameter != null)
                        {
                            existingPatientList = patientRepository.GetPatient(patientIds).ToList();
                            existingCarehomeList = objCustomer.CareHomes.ToList();
                            existingPatientListbySapId = patientRepository.GetPatientsBySAPId(sapPatientIds).ToList();
                            PopulateCountyList(countyCodeList);
                            PopulateAllCountry();
                            strRound = Convert.ToString(ConfigurationManager.AppSettings["PatientRoundFormat"], CultureInfo.InvariantCulture);
                            bool commitStatus = false;
                            foreach (var patient in patientList)
                            {
                                isValidGlobal = true;
                                if (string.IsNullOrEmpty(patient.ErrorMessage) && PatientDataBaseValidation(patient))
                                {
                                    ValidatePatient(patient, objCustomerParameter);
                                    if (string.IsNullOrEmpty(patient.ErrorMessage))
                                    {                                        
                                        InsertOrUpdatePatient(patient, prescriptionRepository);
                                        patient.Age = Convert.ToString(CommonService.GetAge(ConvertDateToMMddyyyy(patient.DateofBirth)), CultureInfo.InvariantCulture);
                                        commitStatus = true;
                                    }
                                }
                            }
                            if (commitStatus)
                            {
                                unitOfWork.Commit();
                                importStatus = true;
                                if (isImport)
                                {
                                    var addedPatientData = prescriptionRepository.GetAddedPatientEntities();
                                    addedPatientData.ForEach(patient => patientList.First(c => c.LoadId.Equals(patient.LoadId.Value.ToString(CultureInfo.InvariantCulture), StringComparison.OrdinalIgnoreCase)).IndigoPatientID = Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture));
                                }
                            }
                        }
                        else
                        {
                            patientList.ForEach(a => a.ErrorMessage = CommonConstants.CustomerParameternotfound + objCustomer.CustomerId);
                        }
                    }
                    else
                    {
                        patientList.ForEach(a => a.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCustomerId, patientList[0].CustomerID));
                    }
                    WritePatientLog(patientList);
                }
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }

                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }

                if (prescriptionRepository != null)
                {
                    prescriptionRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets the list detailsfor patient.
        /// </summary>
        private void GetListDetailsforPatient()
        {
            var patientStatusListTypeId = Convert.ToInt64(SCAEnums.ListType.PatientStatus, CultureInfo.InvariantCulture);
            var patientTypeListTypeId = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
            var communicationFormat = Convert.ToInt64(SCAEnums.ListType.CommunicationFormat, CultureInfo.InvariantCulture);
            var gender = Convert.ToInt64(SCAEnums.ListType.Gender, CultureInfo.InvariantCulture);
            var stoppedListId = Convert.ToInt64(SCAEnums.ListType.StoppedPatient, CultureInfo.InvariantCulture);
            var removedListId = Convert.ToInt64(SCAEnums.ListType.RemovedPatient, CultureInfo.InvariantCulture);
            var maleListTypeId = Convert.ToInt64(SCAEnums.ListType.MaleTitle, CultureInfo.InvariantCulture);
            var femaleListTypeId = Convert.ToInt64(SCAEnums.ListType.FemaleTitle, CultureInfo.InvariantCulture);
            string listIdforPatient = patientTypeListTypeId + "," + patientStatusListTypeId + "," + communicationFormat + "," + maleListTypeId + "," + femaleListTypeId + "," + gender + "," + stoppedListId + "," + removedListId;
            string languageId = Convert.ToString(Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            PopulateListData(listIdforPatient, languageId);
        }

        /// <summary>
        /// Write Patient Log
        /// </summary>
        /// <param name="patientList">List MassData Patient BusinessModel</param>
        private void WritePatientLog(List<MassDataPatientBusinessModel> patientList)
        {
            if (patientList != null)
            {
                removeColumn = new List<string>() { "HouseNumber", "FaxNumber", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                ReturnLogFilePath = CommonService.ExportToExcel(patientList, SCAEnums.MassDataTemplate.Patient.ToString(CultureInfo.InvariantCulture),
                                    massDataFileName, removeColumn, this.massChangesLogs, true);
            }
        }

        /// <summary>
        /// Insert Or Update Patient
        /// </summary>
        /// <param name="patient">MassData Patient BusinessModel</param>
        /// <param name="prescriptionRepository">Prescription Repository</param>
        private void InsertOrUpdatePatient(MassDataPatientBusinessModel patient, IPrescriptionRepository prescriptionRepository)
        {
            PersonalInformation objPersonalInfo = null;
            Address objAddress = null;
            EFModel.Patient objPatient = null;
            EFModel.Prescription objPrescription = null;
            EFModel.CareHome objCareHome = null;
            long patientId = 0;
            try
            {
                if (!string.IsNullOrEmpty(patient.IndigoPatientID))
                {
                    patientId = Convert.ToInt64(patient.IndigoPatientID, CultureInfo.InvariantCulture);
                    objPrescription = prescriptionRepository.GetPatientPrescription(patientId).FirstOrDefault();
                    objPatient = objPrescription.Patient;
                    objPersonalInfo = objPatient.PersonalInformation;
                    objAddress = objPersonalInfo.Address;
                }
                else
                {
                    objPatient = new EFModel.Patient();
                    objPersonalInfo = new PersonalInformation();
                    objAddress = new Address();
                    objPrescription = new Prescription();
                }
                if (!string.IsNullOrEmpty(patient.IndigoCarehomeID))
                {
                    objCareHome = objCustomer.CareHomes.Where(x => x.CareHomeId == Convert.ToInt64(patient.IndigoCarehomeID, CultureInfo.InvariantCulture)).FirstOrDefault();
                    patient.SAPCarehomeID = objCareHome.SAPCareHomeNumber;
                }
                SetPatientPresonalInformationDetails(patient, objPersonalInfo, objAddress, objCareHome);
                objPatient.PersonalInformation = objPersonalInfo;
                ///Set patient Information
                SetPatientInformation(patient, objPatient, objCareHome);
                if (isImport)
                {
                    objPrescription.ModifiedBy = guUserId;
                    objPrescription.ModifiedDate = currentDate;
                    objPatient.LoadId = Convert.ToInt64(patient.LoadId, CultureInfo.InvariantCulture);
                }

                objPrescription.Patient = objPatient;
                prescriptionRepository.InsertOrUpdate(objPrescription);

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, globalUserId);
            }
        }

        /// <summary>
        /// Set Patient Information
        /// </summary>
        /// <param name="patient">MassData Patient BusinessModel</param>
        /// <param name="objPatient">EFModel Patient</param>
        /// <param name="objPersonalInfo">Personal Information</param>
        private void SetPatientInformation(MassDataPatientBusinessModel patient, EFModel.Patient objPatient, EFModel.CareHome objCareHome)
        {
            objPatient.CustomerId = globalCustomerId;
            objPatient.ADP1 = patient.AlternateDeliveryPoint1;
            objPatient.ADP2 = patient.AlternateDeliveryPoint2;
            //ConvertDateToMMddyyyy();
            objPatient.AssessmentDate = ConvertDateToMMddyyyy(patient.AssessmentDate);
            objPatient.BillTo = Convert.ToInt64(patient.BillTo, CultureInfo.InvariantCulture);
            objPatient.DateofBirth = ConvertDateToMMddyyyy(patient.DateofBirth);
            objPatient.DeliveryDate = null;
            if (!string.IsNullOrEmpty(patient.DeliveryDate))
            {
                objPatient.DeliveryDate = ConvertDateToMMddyyyy(patient.DeliveryDate);
            }           

            // If Patient is new the assign the default round and Round Id else assign from excel sheet
            if (string.IsNullOrEmpty(patient.IndigoCarehomeID))
            {
                if (isImport)
                {
                    objPatient.Round = Convert.ToString(ConfigurationManager.AppSettings["NewPatientRound"], CultureInfo.InvariantCulture);
                    objPatient.RoundId = Convert.ToString(ConfigurationManager.AppSettings["NewPatientRoundId"], CultureInfo.InvariantCulture);
                }
                else
                {
                    objPatient.Round = patient.Round;
                    objPatient.RoundId = patient.RoundID;
                }
            }
            // Set Round and Round Id from carehome
            else
            {
                objPatient.CareHomeId = Convert.ToInt64(patient.IndigoCarehomeID, CultureInfo.InvariantCulture);
                objPatient.Round = objCareHome.Round;
                objPatient.RoundId = objCareHome.RoundId;
            }
            objPatient.DeliveryFrequency = patient.DeliveryFrequency;
            objPatient.IsDataProtected = ConvertYesNoToBool(patient.DataProtection);
            objPatient.IsSentToSAP = ConvertYesNoToBool(patient.SendtoSAP);
            objPatient.LocalId = patient.LocalID;
            objPatient.ModifiedBy = guUserId;
            objPatient.ModifiedDate = currentDate;
            objPatient.NextAssessmentDate = null;
            if (!string.IsNullOrEmpty(patient.NextAssessmentDate))
            {
                objPatient.NextAssessmentDate = ConvertDateToMMddyyyy(patient.NextAssessmentDate);
            }
            objPatient.NextDeliveryDate = null;        
            if (!string.IsNullOrEmpty(patient.NextDeliveryDate))
            {
                objPatient.NextDeliveryDate = ConvertDateToMMddyyyy(patient.NextDeliveryDate);
            }
            
            objPatient.NHSId = patient.NHSID;
            long activeStatus = Convert.ToInt64(SCAEnums.PatientStatus.Active, CultureInfo.InvariantCulture);
            long? patientStatus = GetListId(patient.PatientStatus);
            objPatient.PatientStatus = patientStatus;

            if (isImport)
            {
                objPatient.PatientType = GetListId(patient.PatientType);
            }

            objPatient.ReasonCodeID = patientStatus != activeStatus ? GetListId(patient.ReasonCode) : null;
            objPatient.SAPPatientNumber = patient.PatientSAPID;
            objPatient.Remarks = patient.Remarks;
        }

        /// <summary>
        /// Set Patient Presonal Information Details
        /// </summary>
        /// <param name="patient">MassData Patient BusinessModel</param>
        /// <param name="objPersonalInfo">Personal Information</param>
        /// <param name="objAddress">Address Model</param>
        private void SetPatientPresonalInformationDetails(MassDataPatientBusinessModel patient, PersonalInformation objPersonalInfo, Address objAddress, EFModel.CareHome objCareHome)
        {
            // Set Address Information            
            if (string.IsNullOrEmpty(patient.IndigoCarehomeID))
            {
                objAddress.AddressLine1 = patient.AddressLine1;
                objAddress.AddressLine2 = patient.AddressLine2;
                objAddress.City = patient.TownOrCity;
                objAddress.Country = patient.Country;
                objAddress.County = patient.County;
                objAddress.HouseName = patient.HouseName;
                objAddress.PostCode = patient.PostCode;
                objAddress.ModifiedDate = currentDate;
                objAddress.ModifiedBy = guUserId;
                objPersonalInfo.Address = objAddress;
            }
            else
            {
                objPersonalInfo.AddressId = objCareHome.PersonalInformation.Address.AddressId;
            }
            // Set Personal Information
            objPersonalInfo.FirstName = patient.FirstName;
            objPersonalInfo.LastName = patient.Surname;
            objPersonalInfo.Email = patient.Email;
            objPersonalInfo.FaxNumber = patient.FaxNumber;
            objPersonalInfo.MobileNumber = patient.Mobile;
            objPersonalInfo.TelephoneNumber = patient.PhoneNumber;
            objPersonalInfo.Gender = GetListId(patient.Gender);
            objPersonalInfo.TitleId = GetListId(patient.Title);
            if (isImport)
            {
                objPersonalInfo.CreatedById = guUserId;
                objPersonalInfo.CreatedDateTime = currentDate;
            }
            objPersonalInfo.ModifiedBy = guUserId;
            objPersonalInfo.ModifiedDate = currentDate;
            SetDeliveryAddress(patient, objPersonalInfo, objCareHome);
        }

        /// <summary>
        /// Sets the Delivery address.
        /// </summary>
        /// <param name="patient">The object patient business model.</param>
        /// <param name="objPersonalInformation">The object personal information.</param>
        private void SetDeliveryAddress(MassDataPatientBusinessModel patient, PersonalInformation objPersonalInformation, EFModel.CareHome objCareHome)
        {
            IAddressRepository objIAddressRepository = null;
            try
            {
                // Insert New Address then update Personal information    
                objIAddressRepository = new AddressRepository();
                IUnitOfWork addrUnitOfWork = objIAddressRepository.UnitOfWork;
                if (!string.IsNullOrEmpty(patient.IndigoCarehomeID))
                {
                    objPersonalInformation.CorrespondenceAddressId = objCareHome.PersonalInformation.AddressId;
                }
                else
                {
                    // Delivery Address was not provided then CorrespondenceAddressId will be null in Personal information table
                    if (!string.IsNullOrEmpty(patient.DeliveryPostCode)
                        && !string.IsNullOrEmpty(patient.DeliveryCounty)
                        && !string.IsNullOrEmpty(patient.DeliveryCountry)
                        && !string.IsNullOrEmpty(patient.DeliveryAddressLine1))
                    {
                        // Insert or update Delivery Address 
                        var objAddressdetails = new Address();
                        if (objPersonalInformation.CorrespondenceAddressId != null)
                        {
                            objAddressdetails = objIAddressRepository.Find(Convert.ToInt64(objPersonalInformation.CorrespondenceAddressId, CultureInfo.InvariantCulture));
                        }
                        objAddressdetails.HouseName = patient.DeliveryHouseName;
                        objAddressdetails.AddressLine1 = patient.DeliveryAddressLine1;
                        objAddressdetails.AddressLine2 = patient.DeliveryAddressLine2;
                        objAddressdetails.City = patient.DeliveryTownOrCity;
                        objAddressdetails.Country = patient.Country;
                        objAddressdetails.County = patient.DeliveryCounty;
                        objAddressdetails.PostCode = patient.DeliveryPostCode;
                        objAddressdetails.ModifiedBy = guUserId;
                        objAddressdetails.ModifiedDate = currentDate;
                        objIAddressRepository.InsertOrUpdate(objAddressdetails);
                        addrUnitOfWork.Commit();
                        objPersonalInformation.CorrespondenceAddressId = objAddressdetails.AddressId;
                    }
                    else
                    {
                        if (objPersonalInformation.CorrespondenceAddressId != null && objPersonalInformation.AddressId != objPersonalInformation.CorrespondenceAddressId)
                        {
                            objIAddressRepository.Delete(Convert.ToInt64(objPersonalInformation.CorrespondenceAddressId, CultureInfo.InvariantCulture));
                            addrUnitOfWork.Commit();
                        }
                        objPersonalInformation.CorrespondenceAddressId = null;
                    }
                }
            }
            finally
            {
                if (objIAddressRepository != null)
                {
                    objIAddressRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Get List Id
        /// </summary>
        /// <param name="listcode">list code</param>
        /// <returns>
        /// list Id
        /// </returns>
        private long? GetListId(string listcode)
        {
            var List = listModel.Where(x => x.ListCode == listcode).FirstOrDefault();
            if (List != null)
            {
                return List.ListId;
            }
            return null;
        }

        /// <summary>
        /// Patient Validation at database level
        /// </summary>
        /// <param name="patient">MassData Patient BusinessModel</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool PatientDataBaseValidation(MassDataPatientBusinessModel patient)
        {
            // Check Patient Ids existence for Update and TransactionStatus Column accordingly
            if (!isImport && !IsPatientExists(patient.IndigoPatientID))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.PatinetNotExists, patient.IndigoPatientID);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            // Check for Duplicate Patient SAPId and update TransactionStatus Column accordingly
            if (IsDuplicateSAPPatientId(patient.PatientSAPID, patient.IndigoPatientID))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.DuplicateSAPPatientIdinDatabase, patient.PatientSAPID);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            // Check Patient status existence and Update TransactionStatus Column accordingly
            else if (!IsExistsinList(patient.PatientStatus))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.PatinetStatusNotExists, patient.PatientStatus);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            // Check Patient Type existence and Update TransactionStatus Column accordingly
            else if (!IsPatientTypeExists(patient))
            {
                isValidGlobal = false;
            }

            // Check ReasonCodeID existence and Update Status Column accordingly
            else if (!IsPatientReasonCodeExists(patient.ReasonCode, patient.PatientStatus))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidReasonCode, patient.ReasonCode);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            //Check Gender existence and Update Status Column accordingly
            else if (!IsExistsinList(patient.Gender))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidGender, patient.Gender);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            //Check TitleId existence and Update Status Column accordingly
            else if (!IsTitleIdExists(patient.Title))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.TitleNotExists, patient.Title);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            // Check County existence and Update Status Column accordingly
            else if (!IsCountyExists(patient.County))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCounty, patient.County);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            // Check Country existence and Update Status Column accordingly
            else if (!IsCountryExists(patient.Country))
            {
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidCountryCode, patient.Country);
                patient.TransactionStatus = CommonConstants.Failed;
                isValidGlobal = false;
            }
            return isValidGlobal;
        }

        /// <summary>
        /// Check for duplicate SAP SAP Patient Id
        /// </summary>
        /// <param name="patientSAPID">SAP Patient Id</param>
        /// <param name="patientId">Indigo patient id</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsDuplicateSAPPatientId(string patientSAPID, string patientId)
        {
            if (!string.IsNullOrEmpty(patientSAPID) && existingPatientListbySapId.Count > 0)
            {
                if (isImport)
                {
                    var patient = existingPatientListbySapId.Where(x => x.SAPPatientNumber == patientSAPID).FirstOrDefault();
                    if (patient != null)
                    {
                        return true;
                    }
                }
                else
                {
                    var patient = existingPatientListbySapId.Where(x => x.SAPPatientNumber == patientSAPID && x.PatientId != Convert.ToInt64(patientId, CultureInfo.InvariantCulture)).FirstOrDefault();
                    if (patient != null)
                    {
                        return true;
                    }

                }
            }

            return false;
        }

        /// <summary>
        /// Check for title in list
        /// </summary>
        /// <param name="title">patient title</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsTitleIdExists(string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                long maleListTypeId = Convert.ToInt64(SCAEnums.ListType.MaleTitle, CultureInfo.InvariantCulture);
                long femaleListTypeId = Convert.ToInt64(SCAEnums.ListType.FemaleTitle, CultureInfo.InvariantCulture);
                var ListId = listModel.Where(x => x.ListCode.Equals(title, StringComparison.OrdinalIgnoreCase) && ((x.ListTypeId == maleListTypeId) || (x.ListTypeId == femaleListTypeId))).FirstOrDefault();
                if (ListId != null)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Is County Exists
        /// </summary>
        /// <param name="countyCode">county Code</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsCountyExists(string countyCode)
        {
            var county = allCountyList.Where(x => x.CountyCode == countyCode).FirstOrDefault();
            if (county != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Is Country Exists
        /// </summary>
        /// <param name="countryCode">country Code</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsCountryExists(string countryCode)
        {
            var country = allCountryList.Where(x => x.CountryCode == countryCode).FirstOrDefault();
            if (country != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Is Carehome Exists
        /// </summary>
        /// <param name="careHomeId">careHome Id</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsCarehomeExists(string careHomeId)
        {
            var careHome = existingCarehomeList.Where(x => x.CareHomeId == Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture)).FirstOrDefault();
            if (careHome != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Is PatientType Exists
        /// </summary>
        /// <param name="patient">MassData Patient BusinessModel</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsPatientTypeExists(MassDataPatientBusinessModel patient)
        {
            if (!string.IsNullOrEmpty(patient.PatientType))
            {
                var List = listModel.Where(x => x.ListCode == patient.PatientType).FirstOrDefault();
                if (List != null)
                {
                    // Check whether user changed patient type from carehomer to noncare or vise versa

                    if (!IsPatientTypeChanged(patient.IndigoPatientID, List.ListId))
                    {
                        // Check Carehome Type
                        if (CheckCarehomeType(List.ListId, patient.IndigoCarehomeID))
                        {
                            var patientTypeId = objCustomer.PatientTypeMatrices.Where(r => r.PatientType == List.ListId).FirstOrDefault();
                            if (patientTypeId != null)
                            {
                                if (!string.IsNullOrEmpty(patient.IndigoCarehomeID) && !IsCarehomeExists(patient.IndigoCarehomeID))
                                {
                                    patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.CarehomeIdNotExists, patient.IndigoCarehomeID);
                                    patient.TransactionStatus = CommonConstants.Failed;
                                    return false;
                                }

                                return true;
                            }
                        }
                        else
                        {
                            patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidPatientType, patient.PatientType);
                            patient.TransactionStatus = CommonConstants.Failed;
                            return false;
                        }

                    }
                    patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.PatientTypeChangeNotAllowed);
                    patient.TransactionStatus = CommonConstants.Failed;
                    return false;
                }
                patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidPatientType, patient.PatientType);
                patient.TransactionStatus = CommonConstants.Failed;
                return false;
            }
            patient.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.PatinetTypeNotExists, patient.PatientType);
            patient.TransactionStatus = CommonConstants.Failed;
            return false;
        }

        /// <summary>
        /// Determines whether [is patient type changed] [the specified patientid].
        /// </summary>
        /// <param name="patientid">The patientid.</param>
        /// <param name="currentPatientTypeId">The current patient type identifier.</param>
        /// <returns></returns>
        private bool IsPatientTypeChanged(string patientid, long currentPatientTypeId)
        {
            if (!isImport)
            {
                if (existingPatientList != null)
                {
                    var patientobject = existingPatientList.Where(x => x.PatientId == Convert.ToInt64(patientid, CultureInfo.InvariantCulture)).FirstOrDefault();
                    if (patientobject != null && patientobject.PatientType == currentPatientTypeId)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check Carehome Type
        /// </summary>
        /// <param name="patientTypeId">patientType Id</param>
        /// <param name="careHomeId">carehome Id</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool CheckCarehomeType(long patientTypeId, string carehomeId)
        {
            if (!string.IsNullOrEmpty(carehomeId) && (Convert.ToInt64(SCAEnums.PatientType.Hospital, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.ContinuingCare, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.Nursing, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.NursingSelfcare, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.Residential, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.ResidentialSelfcare, CultureInfo.InvariantCulture) == patientTypeId))
            {
                return true;
            }
            else if (
                string.IsNullOrEmpty(carehomeId) && (Convert.ToInt64(SCAEnums.PatientType.Bulk, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.ChildSelfCare, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.Procare, CultureInfo.InvariantCulture) == patientTypeId
                || Convert.ToInt64(SCAEnums.PatientType.SelfCare, CultureInfo.InvariantCulture) == patientTypeId))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Is Patient Exists
        /// </summary>
        /// <param name="patientId">patient Id</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsPatientExists(string patientId)
        {
            var patientobject = existingPatientList.Where(x => x.PatientId == Convert.ToInt64(patientId, CultureInfo.InvariantCulture)).FirstOrDefault();
            if (patientobject != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Is Exists in List
        /// </summary>
        /// <param name="listCode">list Code</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsExistsinList(string listCode)
        {
            if (!string.IsNullOrEmpty(listCode))
            {
                var ListId = listModel.Where(x => x.ListCode.Equals(listCode, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (ListId != null)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Is Patient ReasonCode Exists
        /// </summary>
        /// <param name="reasonCode">reason Code</param>
        /// <param name="patientStatus">patient Status</param>
        /// <returns>
        /// boolean value
        /// </returns>
        private bool IsPatientReasonCodeExists(string reasonCode, string patientStatus)
        {
            long stoppedListId = Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture);
            long listTypeId = 0;
            var PatientStatusId = listModel.Where(x => x.ListCode.Equals(patientStatus, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().ListId;
            if (PatientStatusId != Convert.ToInt64(SCAEnums.PatientStatus.Active, CultureInfo.InvariantCulture))
            {
                if (PatientStatusId > 0 && PatientStatusId == stoppedListId)
                {
                    listTypeId = Convert.ToInt64(SCAEnums.ListType.StoppedPatient, CultureInfo.InvariantCulture);
                }
                else
                {
                    listTypeId = Convert.ToInt64(SCAEnums.ListType.RemovedPatient, CultureInfo.InvariantCulture);
                }
                var List = listModel.Where(x => x.ListCode.Equals(reasonCode, StringComparison.OrdinalIgnoreCase) && x.ListTypeId == listTypeId).FirstOrDefault();
                if (List != null)
                {
                    var reasonCodeId = objCustomer.IDXCustomerReasonCodes.Where(r => r.ListID == List.ListId).FirstOrDefault();
                    if (reasonCodeId != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Validate Patient
        /// </summary>
        /// <param name="patient">MassData Patient BusinessModel</param>
        /// <param name="objCustomerParameter">Customer Parameter</param>
        /// <param name="patientRepository">Patient Repository</param>
        private void ValidatePatient(MassDataPatientBusinessModel patient, EFModel.CustomerParameter objCustomerParameter)
        {
            isValidGlobal = true;
            MandatoryFields = new StringBuilder();
            InvalidDataType = new StringBuilder();
            DatabaseValidation = new StringBuilder();
            CustomValidation = new StringBuilder();
            InvalidLength = new StringBuilder();

            ValidateColumn(patient.FirstName, "FirstName", true, SCAEnums.DataType.String, 100);
            ValidateColumn(patient.Surname, "Surname", true, SCAEnums.DataType.String, 100);
            ValidateColumn(patient.DateofBirth, "DateofBirth", true, SCAEnums.DataType.Date);
            ValidateColumn(patient.AddressLine1, "AddressLine1", true, SCAEnums.DataType.String, 80);
            ValidateColumn(patient.AddressLine1, "AddressLine2", false, SCAEnums.DataType.String, 80);
            ValidateColumn(patient.TownOrCity, "TownOrCity", true, SCAEnums.DataType.String, 64);
            ValidateColumn(patient.PostCode, "PostCode", true, SCAEnums.DataType.String, 10);
            isMandatoryGlobal = false;
            if (string.IsNullOrEmpty(patient.PhoneNumber) && objCustomerParameter.IsTelephoneMandatory)
            {
                isMandatoryGlobal = true;
            }
            if (!string.IsNullOrEmpty(patient.Email) && !CommonHelper.ValidateEmail(patient.Email))
            {
                CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidEmail, patient.Email));
            }
            ValidateColumn(patient.PhoneNumber, "PhoneNumber", isMandatoryGlobal, SCAEnums.DataType.String, 20);
            ValidateColumn(patient.Mobile, "Mobile", false, SCAEnums.DataType.String, 20);
            ValidateColumn(patient.DeliveryFrequency, "DeliveryFrequency", true, SCAEnums.DataType.Int);
            ValidateColumn(patient.DeliveryFrequency, "DeliveryFrequency", true, SCAEnums.DataType.String, 2);

            if (!isImport && string.IsNullOrEmpty(patient.IndigoCarehomeID))
            {
                ValidateColumn(patient.Round, "Round", true, SCAEnums.DataType.String, 10);
                ValidateColumn(patient.RoundID, "RoundId", true, SCAEnums.DataType.String, 4);
            }
            isMandatoryGlobal = false;
            if (string.IsNullOrEmpty(patient.AlternateDeliveryPoint1) && string.IsNullOrEmpty(patient.IndigoCarehomeID) && objCustomerParameter.IsADPMandatory)
            {
                isMandatoryGlobal = true;
            }
            ValidateColumn(patient.AlternateDeliveryPoint1, "AlternateDeliveryPoint1", isMandatoryGlobal, SCAEnums.DataType.NoCheck);

            isMandatoryGlobal = false;
            if (string.IsNullOrEmpty(patient.NHSID) && objCustomerParameter.IsNHSIDMandatory)
            {
                isMandatoryGlobal = true;
            }
            ValidateColumn(patient.NHSID, "NHSID", isMandatoryGlobal, SCAEnums.DataType.NoCheck);
            if (!string.IsNullOrEmpty(patient.NHSID) && patient.NHSID.Length < 10)
            {
                InvalidLength.Append(", NHSID");
            }
            ValidateColumn(patient.AssessmentDate, "AssessmentDate", true, SCAEnums.DataType.Date);

            isMandatoryGlobal = false;
            if (string.IsNullOrEmpty(patient.NextAssessmentDate) && objCustomerParameter.IsNextAssessmentDateMandatory)
            {
                isMandatoryGlobal = true;
            }
            ValidateColumn(patient.NextAssessmentDate, "NextAssessmentDate", isMandatoryGlobal, SCAEnums.DataType.Date);
            ValidateColumn(patient.DeliveryDate, "DeliveryDate", false, SCAEnums.DataType.Date);
            ValidateColumn(patient.NextDeliveryDate, "NextDeliveryDate", false, SCAEnums.DataType.Date);
            ValidateColumn(patient.SendtoSAP, "SendtoSAP", true, SCAEnums.DataType.Char);
            ValidateColumn(patient.DataProtection, "DataProtected", true, SCAEnums.DataType.Char);
            ValidateColumn(patient.BillTo, "BillTo", true, SCAEnums.DataType.Long);
            patient.ErrorMessage = GetErrorMessage();
            patient.TransactionStatus = RowStatus;

            if (string.IsNullOrEmpty(patient.ErrorMessage))
            {

                if (!isImport && string.IsNullOrEmpty(patient.IndigoCarehomeID) && strRound != patient.Round.Substring(0, patient.Round.Length - 2))
                {
                    CustomValidation.Append(", " + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidRoundFormat, patient.Round));

                }
                if (ConvertDateToMMddyyyy(patient.DateofBirth) > DateTime.Now.Date)
                {
                    CustomValidation.Append(", " + CommonConstants.DobGreaterThanCurrentDate);
                    isValidGlobal = false;
                }
                if (ConvertDateToMMddyyyy(patient.DateofBirth) > ConvertDateToMMddyyyy(patient.AssessmentDate))
                {
                    CustomValidation.Append(", " + CommonConstants.DobGreaterThanAssessmentDate);
                    isValidGlobal = false;
                }
                if (!string.IsNullOrEmpty(patient.NextAssessmentDate) && ConvertDateToMMddyyyy(patient.NextAssessmentDate) < ConvertDateToMMddyyyy(patient.AssessmentDate))
                {
                    CustomValidation.Append(", " + CommonConstants.NextAssessmentlessThanAssessmentDate);
                    isValidGlobal = false;
                }
                patient.ErrorMessage = GetErrorMessage();
                patient.TransactionStatus = RowStatus;
            }
        }

        /// <summary>
        /// Get Error Message
        /// </summary>
        /// <returns>
        /// string message
        /// </returns>
        public string GetErrorMessage()
        {
            string ErrorMessage = string.Empty;
            RowStatus = CommonConstants.Success;
            if (MandatoryFields != null && !string.IsNullOrEmpty(MandatoryFields.ToString()))
            {
                ErrorMessage += "[MandatoryFields:" + MandatoryFields.ToString().Remove(0, 1) + "]";
                RowStatus = CommonConstants.Failed;
            }

            if (InvalidDataType != null && !string.IsNullOrEmpty(InvalidDataType.ToString()))
            {
                ErrorMessage += " [InValidDataType:" + InvalidDataType.ToString().Remove(0, 1) + "]";
                RowStatus = CommonConstants.Failed;
            }

            if (DatabaseValidation != null && !string.IsNullOrEmpty(DatabaseValidation.ToString()))
            {
                ErrorMessage += " [DatabaseValidation:" + DatabaseValidation.ToString().Remove(0, 1) + "]";
                RowStatus = CommonConstants.Failed;
            }

            if (InvalidLength != null && !string.IsNullOrEmpty(InvalidLength.ToString()))
            {
                ErrorMessage += " [InvalidLength:" + InvalidLength.ToString().Remove(0, 1) + "]";
                RowStatus = CommonConstants.Failed;
            }

            if (CustomValidation != null && !string.IsNullOrEmpty(CustomValidation.ToString()))
            {
                ErrorMessage += " [CustomValidation:" + CustomValidation.ToString().Remove(0, 1) + "]";
                RowStatus = CommonConstants.Failed;
            }

            return ErrorMessage;
        }

        /// <summary>
        /// Read Patient Data
        /// </summary>
        /// <param name="excelFactory">Excel Query Factory</param>
        /// <returns>
        /// List of MassData Patient BusinessModel
        /// </returns>
        private List<MassDataPatientBusinessModel> ReadPatientData(ExcelQueryFactory excelFactory)
        {
            if (excelFactory != null)
            {
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.LoadId, "LoadId");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.CustomerID, "CustomerID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.IndigoPatientID, "IndigoPatientID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.PatientSAPID, "PatientSAPID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.FirstName, "FirstName");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Surname, "Surname");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Gender, "Gender");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Title, "Title");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DateofBirth, "DateofBirth");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.IndigoCarehomeID, "IndigoCarehomeID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.HouseName, "HouseName");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.AddressLine1, "AddressLine1");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.AddressLine2, "AddressLine2");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.TownOrCity, "TownOrCity");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.County, "County");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Country, "Country");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryHouseName, "DeliveryHouseName");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryAddressLine1, "DeliveryAddressLine1");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryAddressLine2, "DeliveryAddressLine2");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryTownOrCity, "DeliveryTownOrCity");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryCounty, "DeliveryCounty");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryCountry, "DeliveryCountry");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryPostCode, "DeliveryPostCode");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.PhoneNumber, "PhoneNumber");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Mobile, "Mobile");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Email, "Email");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DataProtection, "DataProtection");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryFrequency, "DeliveryFrequency");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Round, "Round");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.RoundID, "RoundID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.AlternateDeliveryPoint1, "AlternateDeliveryPoint1");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.AlternateDeliveryPoint2, "AlternateDeliveryPoint2");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.PatientStatus, "PatientStatus");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.ReasonCode, "ReasonCode");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.PatientType, "PatientType");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.NHSID, "NHSID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.LocalID, "LocalID");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.AssessmentDate, "AssessmentDate");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.NextAssessmentDate, "NextAssessmentDate");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.DeliveryDate, "DeliveryDate");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.NextDeliveryDate, "NextDeliveryDate");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.SendtoSAP, "SendtoSAP");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.BillTo, "BillTo");
                excelFactory.AddMapping<MassDataPatientBusinessModel>(x => x.Remarks, "Remarks");

                var patientList = GetNonblankRows<MassDataPatientBusinessModel>(excelFactory, patientUploadMaxLimit);

                return patientList;
            }
            return null;
        }

        #endregion

        #region Contact Person

        /// <summary>
        /// Main function to mass update contact person....
        /// </summary>
        /// <param name="contactPersonList">The contact person list.</param>
        /// <param name="massDataBusinessModel">The mass data business model.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        private List<ContactPersonBusinessModel> MassInsertUpdateContactPerson(List<ContactPersonBusinessModel> contactPersonList, MassDataBusinessModel massDataBusinessModel)
        {
            if (contactPersonList != null)
            {
                ICustomerRepository customerRepository = null;
                ICareHomeRepository careHomeRepository = null;
                IPatientRepository patientRepository = null;
                try
                {
                    customerRepository = new CustomerRepository();
                    careHomeRepository = new CareHomeRepository();
                    patientRepository = new PatientRepository();

                    bool isCustomerAvailable = false;
                    bool isPatientAvailable = false;
                    bool isCarehomeAvailable = false;

                    // Only Get the data for single Customer Id
                    foreach (var item in contactPersonList)
                    {
                        ValidateAndSetContactPerson(customerRepository, careHomeRepository, patientRepository,
                            ref isCustomerAvailable, ref isPatientAvailable, ref isCarehomeAvailable, item);
                    }

                    // Commit changes for the added entries in the respective type
                    if (isCustomerAvailable)
                    {
                        IUnitOfWork unitOfWorkCustomer = customerRepository.UnitOfWork;
                        unitOfWorkCustomer.Commit();
                    }
                    if (isCarehomeAvailable)
                    {
                        IUnitOfWork unitOfWorkCarehome = careHomeRepository.UnitOfWork;
                        unitOfWorkCarehome.Commit();
                    }
                    if (isPatientAvailable)
                    {
                        IUnitOfWork unitOfWorkPatient = patientRepository.UnitOfWork;
                        unitOfWorkPatient.Commit();
                    }

                    // Update final status
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                    return contactPersonList;
                }
                catch (DataException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (ArgumentException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (FormatException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (InvalidOperationException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (SystemException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (Exception e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                finally
                {
                    if (customerRepository != null)
                    {
                        customerRepository.Dispose();
                    }
                    if (patientRepository != null)
                    {
                        patientRepository.Dispose();
                    }
                    if (careHomeRepository != null)
                    {
                        careHomeRepository.Dispose();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Validates the and set contact person.
        /// </summary>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="patientRepository">The patient repository.</param>
        /// <param name="isCustomerAvailable">if set to <c>true</c> [is customer available].</param>
        /// <param name="isPatientAvailable">if set to <c>true</c> [is patient available].</param>
        /// <param name="isCarehomeAvailable">if set to <c>true</c> [is carehome available].</param>
        /// <param name="item">The item.</param>
        private void ValidateAndSetContactPerson(ICustomerRepository customerRepository, ICareHomeRepository careHomeRepository,
            IPatientRepository patientRepository, ref bool isCustomerAvailable, ref bool isPatientAvailable,
            ref bool isCarehomeAvailable, ContactPersonBusinessModel item)
        {
            long contactPersonTypeId = 0;
            long contactPersonType = 0;
            Guid guidUserId = Guid.Parse(globalUserId);
            bool status = false;
            var currentTimestamp = DateTime.Now;

            Model.Customer customer = new Model.Customer();
            Model.CareHome carehome = new Model.CareHome();
            Model.Patient patient = new Model.Patient();

            PersonalInformation contactPersonalInfo = null;
            ContactPerson contactInfo = null;

            try
            {
                StringBuilder sbErrorMessage = new StringBuilder();
                status = ValidateContactPerson(item, customerRepository, careHomeRepository, patientRepository, out contactPersonType, out contactPersonTypeId,
                                                ref customer, ref carehome, ref patient, out sbErrorMessage);

                // if valid the go ahead for database update / insert
                if (status)
                {
                    contactPersonalInfo = new PersonalInformation();
                    contactInfo = new ContactPerson();

                    if (!isImport)
                    {
                        // Get the parent entity i.e. Customer/CareHome/Patient and update it's ModifiedDate
                        GetContactPersonBaseEntity(customerRepository, careHomeRepository, patientRepository, customer, carehome, patient,
                            contactPersonalInfo, contactInfo, currentTimestamp, contactPersonType, item, globalUserId);
                    }

                    // Convert Business model to entity model
                    SetContactPersonInformation(item, contactPersonTypeId, contactPersonType, guidUserId, currentTimestamp, contactPersonalInfo, contactInfo);

                    // entire object gets ready and set this object into InsertOrUpdate method....
                    switch (contactPersonType)
                    {
                        case (int)SCAEnums.ObjectType.Customer:
                            {
                                customer.ContactPersons.Add(contactInfo);
                                customer.ModifiedDate = currentTimestamp;
                                customerRepository.InsertOrUpdate(customer);
                                isCustomerAvailable = true;
                                break;
                            }

                        case (int)SCAEnums.ObjectType.CareHome:
                            {
                                carehome.ContactPersons.Add(contactInfo);
                                carehome.ModifiedDate = currentTimestamp;
                                careHomeRepository.InsertOrUpdate(carehome);
                                isCarehomeAvailable = true;
                                break;
                            }

                        case (int)SCAEnums.ObjectType.Patient:
                            {
                                patient.ContactPersons.Add(contactInfo);
                                patient.ModifiedDate = currentTimestamp;
                                patientRepository.InsertOrUpdate(patient);
                                isPatientAvailable = true;
                                break;
                            }
                        default:
                            // do the default action
                            break;
                    }
                    item.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    item.ErrorMessage = Convert.ToString(sbErrorMessage, CultureInfo.InvariantCulture);
                }
            }
            catch (DataException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
        }

        /// <summary>
        /// Sets the contact person information.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="contactPersonTypeId">The contact person type identifier.</param>
        /// <param name="contactPersonType">Type of the contact person.</param>
        /// <param name="guidUserId">The unique identifier user identifier.</param>
        /// <param name="currentTimestamp">The current timestamp.</param>
        /// <param name="contactPersonalInfo">The contact personal information.</param>
        /// <param name="contactInfo">The contact information.</param>
        private static void SetContactPersonInformation(ContactPersonBusinessModel item, long contactPersonTypeId, long contactPersonType,
            Guid guidUserId, DateTime currentTimestamp,
            PersonalInformation contactPersonalInfo, ContactPerson contactInfo)
        {
            var patientId = (long?)null;
            if (!string.IsNullOrEmpty(item.IndigoPatientId))
            {
                patientId = Convert.ToInt64(item.IndigoPatientId, CultureInfo.InvariantCulture);
            }

            var carehomeId = (long?)null;
            if (!string.IsNullOrEmpty(item.IndigoCarehomeId))
            {
                carehomeId = Convert.ToInt64(item.IndigoCarehomeId, CultureInfo.InvariantCulture);
            }

            contactPersonalInfo.FirstName = item.ContactPersonFirstName;
            contactPersonalInfo.LastName = item.ContactPersonSurname;
            contactPersonalInfo.JobTitle = item.JobTitle;
            contactPersonalInfo.TelephoneNumber = item.Phone;
            contactPersonalInfo.MobileNumber = item.Mobile;
            contactPersonalInfo.Email = item.EmailId;
            contactPersonalInfo.ModifiedBy = guidUserId;
            contactPersonalInfo.ModifiedDate = currentTimestamp;
            contactPersonalInfo.CreatedDateTime = currentTimestamp;
            contactPersonalInfo.CreatedById = guidUserId;

            contactInfo.PatientId = patientId;
            contactInfo.CarehomeId = carehomeId;
            contactInfo.CustomerId = (contactPersonType == (int)SCAEnums.ObjectType.Customer) ? contactPersonTypeId : (long?)null;
            contactInfo.ModifiedBy = guidUserId;
            contactInfo.ModifiedDate = currentTimestamp;
            contactInfo.PersonalInformation = contactPersonalInfo;
            contactInfo.Remarks = item.Remarks;
        }

        /// <summary>
        /// Get existing Contact person entity..
        /// </summary>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="patientRepository">The patient repository.</param>
        /// <param name="objCustomer">The object customer.</param>
        /// <param name="objCarehome">The object carehome.</param>
        /// <param name="objPatient">The object patient.</param>
        /// <param name="contactPersonalInfo">The contact personal information.</param>
        /// <param name="contactInfo">The contact information.</param>
        /// <param name="currentTimestamp">The current timestamp.</param>
        /// <param name="contactPersonType">Type of the contact person.</param>
        /// <param name="item">The item.</param>
        private static void GetContactPersonBaseEntity(ICustomerRepository customerRepository, ICareHomeRepository careHomeRepository,
                IPatientRepository patientRepository, Model.Customer customer, Model.CareHome carehome, Model.Patient patient,
                PersonalInformation contactPersonalInfo, ContactPerson contactInfo, DateTime currentTimestamp,
                long contactPersonType, ContactPersonBusinessModel item, string userId)
        {
            try
            {
                var contactPersonId = Convert.ToInt64(item.ContactPersonId, CultureInfo.InvariantCulture);
                switch (contactPersonType)
                {
                    case (int)SCAEnums.ObjectType.Customer:
                        customer = customerRepository.GetCustomerBySAPId(item.CustomerId);
                        customer.ModifiedDate = currentTimestamp;
                        contactInfo = customer.ContactPersons.Where(q => q.ContactPersonId == contactPersonId).FirstOrDefault();
                        if (contactInfo == null)
                        {
                            item.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidContactPersonId, contactPersonId);
                            item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            contactPersonalInfo = contactInfo.PersonalInformation;
                        }
                        break;

                    case (int)SCAEnums.ObjectType.CareHome:
                        carehome = careHomeRepository.GetCareHome(Convert.ToInt64(item.IndigoCarehomeId, CultureInfo.InvariantCulture)).FirstOrDefault();
                        carehome.ModifiedDate = currentTimestamp;
                        contactInfo = carehome.ContactPersons.Where(q => q.ContactPersonId == contactPersonId).FirstOrDefault();
                        if (contactInfo == null)
                        {
                            item.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidContactPersonId, contactPersonId);
                            item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            contactPersonalInfo = contactInfo.PersonalInformation;
                        }
                        break;

                    case (int)SCAEnums.ObjectType.Patient:
                        patient = patientRepository.GetPatient(Convert.ToInt64(item.IndigoPatientId, CultureInfo.InvariantCulture)).FirstOrDefault();
                        patient.ModifiedDate = currentTimestamp;
                        contactInfo = patient.ContactPersons.Where(q => q.ContactPersonId == contactPersonId).FirstOrDefault();
                        if (contactInfo == null)
                        {
                            item.ErrorMessage = string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidContactPersonId, contactPersonId);
                            item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            contactPersonalInfo = contactInfo.PersonalInformation;
                        }
                        break;
                    default:
                        // do the default action
                        break;
                }
            }
            catch (DataException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, userId);
            }
            catch (Exception e)
            {
                item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                item.ErrorMessage = e.Message;
                LogHelper.LogException(e, userId);
            }
        }

        /// <summary>
        /// Validate contact person data...
        /// </summary>
        /// <param name="contactPerson">The contact person.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="patientRepository">The patient repository.</param>
        /// <param name="contactPersonType">Type of the contact person.</param>
        /// <param name="contactPersonTypeId">The contact person type identifier.</param>
        /// <param name="objCustomer">The object customer.</param>
        /// <param name="objCareHome">The object care home.</param>
        /// <param name="objPatient">The object patient.</param>
        /// <param name="processError">The process error.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        private bool ValidateContactPerson(ContactPersonBusinessModel contactPerson, ICustomerRepository customerRepository,
             ICareHomeRepository careHomeRepository, IPatientRepository patientRepository, out long contactPersonType, out long contactPersonTypeId,
             ref Model.Customer customer, ref Model.CareHome careHome, ref Model.Patient patient, out StringBuilder processError)
        {
            bool isValid = true;
            contactPersonType = 0;
            contactPersonTypeId = 0;
            var sapCustomerId = string.Empty;
            processError = new StringBuilder();

            if (isImport)
            {
                // ContactPersonId
                if (!string.IsNullOrEmpty(contactPerson.ContactPersonId))
                {
                    processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.ContactPersonId + CommonConstants.InvalidBlank, contactPerson.ContactPersonId));
                    isValid = false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(contactPerson.ContactPersonId))
                {
                    processError.Append(CommonConstants.Astrik + CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ContactPersonId.");
                    isValid = false;
                }
                else if (!string.IsNullOrEmpty(contactPerson.ContactPersonId) && !CommonHelper.ValidateInteger(contactPerson.ContactPersonId))
                {
                    processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.ContactPersonId + CommonConstants.InvalidNumber, contactPerson.ContactPersonId));
                    isValid = false;
                }
            }

            // First Name
            if (string.IsNullOrEmpty(contactPerson.ContactPersonFirstName))
            {
                processError.Append(CommonConstants.Astrik + CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "ContactPersonFirstName.");
                isValid = false;
            }
            else if (contactPerson.ContactPersonFirstName.Length > 200)
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, contactPerson.ContactPersonFirstName));
                isValid = false;
            }

            // Last Name
            if (!string.IsNullOrEmpty(contactPerson.ContactPersonSurname) && contactPerson.ContactPersonSurname.Length > 200)
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, contactPerson.ContactPersonSurname));
                isValid = false;
            }

            // Validate TelephoneNumber column          
            if (!string.IsNullOrEmpty(contactPerson.Phone) && !CommonHelper.ValidateInteger(contactPerson.Phone))
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Phone + CommonConstants.InvalidNumber, contactPerson.Phone));
                isValid = false;
            }
            else if (!string.IsNullOrEmpty(contactPerson.Phone) && contactPerson.Phone.Length > 20)
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, contactPerson.Phone));
                isValid = false;
            }

            // Validate Mobile Number column          
            if (!string.IsNullOrEmpty(contactPerson.Mobile) && !CommonHelper.ValidateInteger(contactPerson.Mobile))
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Mobile + CommonConstants.InvalidNumber, contactPerson.Mobile));
                isValid = false;
            }
            else if (!string.IsNullOrEmpty(contactPerson.Mobile) && contactPerson.Mobile.Length > 20)
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, contactPerson.Mobile));
                isValid = false;
            }

            // Email
            if (!string.IsNullOrEmpty(contactPerson.EmailId) && contactPerson.EmailId.Length > 100)
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, contactPerson.EmailId));
                isValid = false;
            }
            else if (!string.IsNullOrEmpty(contactPerson.EmailId) && !CommonHelper.ValidateEmail(contactPerson.EmailId))
            {
                processError.Append(CommonConstants.Astrik + string.Format(CultureInfo.InvariantCulture, CommonConstants.InvalidEmail, contactPerson.EmailId));
                isValid = false;
            }

            // If entry present in all three column - Customer, Patient and Carehome. 
            if (!string.IsNullOrEmpty(contactPerson.CustomerId) && !string.IsNullOrEmpty(contactPerson.IndigoPatientId) && !string.IsNullOrEmpty(contactPerson.IndigoCarehomeId))
            {
                processError.Append(CommonConstants.Astrik + CommonConstants.ContactPersonAll);
                isValid = false;
            } // if not a single entry present in all three column - Customer, Patient and Carehome.
            else if (string.IsNullOrEmpty(contactPerson.CustomerId) && string.IsNullOrEmpty(contactPerson.IndigoPatientId) && string.IsNullOrEmpty(contactPerson.IndigoCarehomeId))
            {
                processError.Append(CommonConstants.Astrik + CommonConstants.ContactPersonNotAll);
                isValid = false;
            }
            else
            {
                // If entry present in either of two columns.
                if ((!string.IsNullOrEmpty(contactPerson.IndigoCarehomeId)) && !(string.IsNullOrEmpty(contactPerson.IndigoPatientId)))
                {
                    // if two entries present in Patient and Carehome.
                    processError.Append(CommonConstants.Astrik + CommonConstants.InvalidPatientCarehome);
                    isValid = false;
                }
                else if (!(string.IsNullOrEmpty(contactPerson.CustomerId)) && !(string.IsNullOrEmpty(contactPerson.IndigoCarehomeId)))
                {
                    // if two entries present in Customer and Carehome.
                    processError.Append(CommonConstants.Astrik + CommonConstants.InvalidCustomerCarehome);
                    isValid = false;
                }
                else if (!(string.IsNullOrEmpty(contactPerson.CustomerId)) && !(string.IsNullOrEmpty(contactPerson.IndigoPatientId)))
                {
                    // if two entries present in Customer, Patient.
                    processError.Append(CommonConstants.Astrik + CommonConstants.InvalidPatientCustomer);
                    isValid = false;
                }
                else
                {
                    // now checking for which type of import / changes
                    if (string.IsNullOrEmpty(contactPerson.IndigoCarehomeId) && string.IsNullOrEmpty(contactPerson.IndigoPatientId))
                    {
                        // this is customer type
                        sapCustomerId = contactPerson.CustomerId.Trim().PadLeft(Convert.ToInt16(CommonConstants.SapIdLength, CultureInfo.InvariantCulture), '0');
                        if (!string.IsNullOrEmpty(sapCustomerId) && !CommonHelper.ValidateInteger(sapCustomerId))
                        {
                            processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Customer + CommonConstants.InvalidNumber, sapCustomerId));
                            isValid = false;
                        }

                        contactPersonType = (int)SCAEnums.ObjectType.Customer;
                        if (isValid)
                        {
                            customer = customerRepository.GetCustomerBySAPId(sapCustomerId);
                            if (customer == null)
                            {
                                isValid = false;
                                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidCustomerId, sapCustomerId));
                            }
                            else
                            {
                                contactPerson.CustomerId = sapCustomerId;
                                contactPersonTypeId = customer.CustomerId;

                                // check contactperson id
                                if (!isImport)
                                {
                                    var contactInfo = customer.ContactPersons.Where(q => q.ContactPersonId == Convert.ToInt64(contactPerson.ContactPersonId, CultureInfo.InvariantCulture)).FirstOrDefault();
                                    if (contactInfo == null)
                                    {
                                        isValid = false;
                                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidContactPersonId, contactPerson.ContactPersonId));
                                    }
                                }
                            }
                        }
                    }
                    else if (string.IsNullOrEmpty(contactPerson.CustomerId) && string.IsNullOrEmpty(contactPerson.IndigoCarehomeId))
                    {
                        // this is patient type
                        if (!string.IsNullOrEmpty(contactPerson.IndigoPatientId) && !CommonHelper.ValidateInteger(contactPerson.IndigoPatientId))
                        {
                            processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Patient + CommonConstants.InvalidNumber, contactPerson.IndigoPatientId));
                            isValid = false;
                        }

                        contactPersonType = (int)SCAEnums.ObjectType.Patient;
                        contactPersonTypeId = Convert.ToInt64(contactPerson.IndigoPatientId, CultureInfo.InvariantCulture);

                        if (isValid)
                        {
                            patient = patientRepository.GetPatient(long.Parse(contactPerson.IndigoPatientId, CultureInfo.InvariantCulture)).FirstOrDefault();
                            if (patient == null)
                            {
                                isValid = false;
                                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidPatientsId, contactPerson.IndigoPatientId));
                            }
                            else
                            {
                                // check contactperson id
                                if (!isImport)
                                {
                                    var contactInfo = patient.ContactPersons.Where(q => q.ContactPersonId == Convert.ToInt64(contactPerson.ContactPersonId, CultureInfo.InvariantCulture)).FirstOrDefault();
                                    if (contactInfo == null)
                                    {
                                        isValid = false;
                                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidContactPersonId, contactPerson.ContactPersonId));
                                    }
                                }
                            }
                        }
                    }
                    else if (string.IsNullOrEmpty(contactPerson.CustomerId) && string.IsNullOrEmpty(contactPerson.IndigoPatientId))
                    {
                        // this is carehome type
                        if (!string.IsNullOrEmpty(contactPerson.IndigoCarehomeId) && !CommonHelper.ValidateInteger(contactPerson.IndigoCarehomeId))
                        {
                            processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Carehome + CommonConstants.InvalidNumber, contactPerson.IndigoCarehomeId));
                            isValid = false;
                        }

                        contactPersonType = (int)SCAEnums.ObjectType.CareHome;
                        contactPersonTypeId = Convert.ToInt64(contactPerson.IndigoCarehomeId, CultureInfo.InvariantCulture);

                        if (isValid)
                        {
                            careHome = careHomeRepository.GetCareHome(long.Parse(contactPerson.IndigoCarehomeId, CultureInfo.InvariantCulture)).FirstOrDefault();
                            if (careHome == null)
                            {
                                isValid = false;
                                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidCarehomeId, contactPerson.IndigoCarehomeId));
                            }
                            else
                            {
                                // check contactperson id
                                if (!isImport)
                                {
                                    var contactInfo = careHome.ContactPersons.Where(q => q.ContactPersonId == Convert.ToInt64(contactPerson.ContactPersonId, CultureInfo.InvariantCulture)).FirstOrDefault();
                                    if (contactInfo == null)
                                    {
                                        isValid = false;
                                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidContactPersonId, contactPerson.ContactPersonId));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return isValid;
        }

        #endregion

        #region CommunicationFormat

        /// <summary>
        /// Masses the insert update communication format.
        /// </summary>
        /// <param name="communicationPreferencesList">The communication preferences list.</param>
        /// <param name="massDataBusinessModel">The mass data business model.</param>
        /// <returns></returns>
        private List<CommunicationPreferencesBusinessModel> MassInsertUpdateCommunicationFormat(List<CommunicationPreferencesBusinessModel> communicationPreferencesList, MassDataBusinessModel massDataBusinessModel)
        {
            if (communicationPreferencesList != null)
            {
                Model.CareHome carehome = new Model.CareHome();
                Model.Patient patient = new Model.Patient();
                Model.Customer customer = new Model.Customer();

                IDXPreference idxPreference = null;
                Guid guidUserId = Guid.Parse(massDataBusinessModel.UserId);
                var currentTimestamp = DateTime.Now;

                ICustomerRepository customerRepository = null;
                ICareHomeRepository careHomeRepository = null;
                IPatientRepository patientRepository = null;
                try
                {
                    customerRepository = new CustomerRepository();
                    careHomeRepository = new CareHomeRepository();
                    patientRepository = new PatientRepository();

                    bool isPatientAvailable = false;
                    bool isCarehomeAvailable = false;
                    StringBuilder sbErrorMessage = null;

                    // Only Get the data for single Customer Id
                    foreach (var item in communicationPreferencesList)
                    {
                        bool status = false;
                        sbErrorMessage = new StringBuilder();
                        status = ValidateCommunicationFormat(item, customerRepository, careHomeRepository, patientRepository,
                              ref customer, ref  carehome, ref patient, out sbErrorMessage);

                        // if valid the go ahead for database update / insert
                        if (status)
                        {
                            item.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            item.ErrorMessage = Convert.ToString(sbErrorMessage, CultureInfo.InvariantCulture);
                        }
                    }

                    var successList = communicationPreferencesList.Where(q => q.TransactionStatus == SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture)).ToList().Distinct();

                    foreach (var row in successList)
                    {
                        try
                        {
                            if (row.IsPatient)
                            {
                                // Set Communication And Marketting For Patient
                                SetCommunicationAndMarkettingForPatient(patient, idxPreference, guidUserId, currentTimestamp, patientRepository,
                                                                        ref isPatientAvailable, successList, row);
                            }
                            else
                            {
                                // Set Communication And Marketting For Carehome
                                SetCommunicationAndMarkettingForCarehome(carehome, idxPreference, guidUserId, currentTimestamp,
                                                                        careHomeRepository, ref isCarehomeAvailable, successList, row);
                            }
                        }
                        catch (DataException e)
                        {
                            row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            row.ErrorMessage = e.Message;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (ArgumentException e)
                        {
                            row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            row.ErrorMessage = e.Message;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (FormatException e)
                        {
                            row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            row.ErrorMessage = e.Message;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (InvalidOperationException e)
                        {
                            row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            row.ErrorMessage = e.Message;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (SystemException e)
                        {
                            row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            row.ErrorMessage = e.Message;
                            LogHelper.LogException(e, globalUserId);
                        }
                        catch (Exception e)
                        {
                            row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            row.ErrorMessage = e.Message;
                            LogHelper.LogException(e, globalUserId);
                        }
                    }

                    // Commit changes for the added entries in the respective type               
                    if (isCarehomeAvailable)
                    {
                        IUnitOfWork unitOfWorkCarehome = careHomeRepository.UnitOfWork;
                        unitOfWorkCarehome.Commit();
                    }
                    if (isPatientAvailable)
                    {
                        IUnitOfWork unitOfWorkPatient = patientRepository.UnitOfWork;
                        unitOfWorkPatient.Commit();
                    }

                    // Update final status
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                    return communicationPreferencesList;
                }
                catch (DataException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (ArgumentException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (FormatException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (InvalidOperationException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (SystemException e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                catch (Exception e)
                {
                    massDataBusinessModel.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    massDataBusinessModel.ErrorMessage = e.Message;
                    LogHelper.LogException(e, globalUserId);
                }
                finally
                {
                    if (customerRepository != null)
                    {
                        customerRepository.Dispose();
                    }
                    if (careHomeRepository != null)
                    {
                        careHomeRepository.Dispose();
                    }
                    if (patientRepository != null)
                    {
                        patientRepository.Dispose();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the communication and marketting for patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <param name="idxPreference">The index preference.</param>
        /// <param name="guidUserId">The unique identifier user identifier.</param>
        /// <param name="currentTimestamp">The current timestamp.</param>
        /// <param name="patientRepository">The patient repository.</param>
        /// <param name="isPatientAvailable">if set to <c>true</c> [is patient available].</param>
        /// <param name="successList">The success list.</param>
        /// <param name="row">The row.</param>
        private void SetCommunicationAndMarkettingForPatient(Model.Patient patient, IDXPreference idxPreference,
            Guid guidUserId, DateTime currentTimestamp, IPatientRepository patientRepository, ref bool isPatientAvailable,
            IEnumerable<CommunicationPreferencesBusinessModel> successList, CommunicationPreferencesBusinessModel row)
        {
            var selectedCommPreference = new List<long>();
            var selectedMarkPreference = new List<long>();
            var remarkCommunication = string.Empty;
            var remarkMarketting = string.Empty;
            try
            {
                // remove rows for patient
                patient = patientRepository.GetPatient(long.Parse(row.IndigoPatientId, CultureInfo.InvariantCulture)).FirstOrDefault();
                if (patient != null)
                {
                    isPatientAvailable = true;
                    // 1 >> Process for Communication Preferences
                    selectedCommPreference = new List<long>();
                    var communicationPrefList = patient.IDXPreferences
                                                .Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture)).ToList();
                    successList.Where(q => Convert.ToInt64(q.CommunicationPrefListId, CultureInfo.InvariantCulture) > 0 && q.IndigoPatientId == Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture)).ToList()
                                                .ForEach(q => selectedCommPreference.Add(Convert.ToInt64(q.CommunicationPrefListId, CultureInfo.InvariantCulture)));
                    var dbCommPreferencesList = communicationPrefList.Select(q => q.Preference.Value).ToList();
                    var RemovedCommPreferenceId = dbCommPreferencesList.Except(selectedCommPreference).ToList();
                    RemovedCommPreferenceId.ForEach(d =>
                    {
                        idxPreference = communicationPrefList.Find(q => q.Preference == d);
                        patient.IDXPreferences.Remove(idxPreference);
                    });

                    // Save entry in patient table for Communication preference.
                    var patientPreferences = patient.IDXPreferences;
                    foreach (var communicationId in selectedCommPreference)
                    {
                        if (!patientPreferences.Where(q => q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture) && Convert.ToInt64(q.Preference, CultureInfo.InvariantCulture) == communicationId).Any())
                        {
                            // Return remarks value                                            
                            var remarks = successList.Where(q => q.IndigoPatientId == Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture) &&
                                                q.CommunicationPrefListId == Convert.ToString(communicationId, CultureInfo.InvariantCulture)).FirstOrDefault();

                            if (remarks != null)
                            {
                                remarkCommunication = this.GetRemarksForCommunication(remarks.Remarks);

                                if (!string.IsNullOrEmpty(remarks.CommunicationPrefListId) && !string.IsNullOrEmpty(remarks.MarkettingPrefListId))
                                {
                                    remarkCommunication = remarkCommunication.Contains("|") ? remarkCommunication.Split('|')[0] : remarkCommunication;
                                }
                            }

                            // Insert in IDXPreference table
                            idxPreference = new IDXPreference();
                            idxPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture);
                            idxPreference.PatientId = patient.PatientId;
                            idxPreference.Preference = Convert.ToInt64(communicationId, CultureInfo.InvariantCulture);
                            idxPreference.ModifiedBy = guidUserId;
                            idxPreference.Remarks = remarkCommunication;
                            idxPreference.ModifiedDate = currentTimestamp;
                            patient.IDXPreferences.Add(idxPreference);

                            // Insert in Patient Table
                            patient.ModifiedDate = currentTimestamp;
                            patientRepository.InsertOrUpdate(patient);
                        }
                    }

                    // 2 >> Process for Marketting Preferences
                    selectedMarkPreference = new List<long>();
                    var MarketingPrefList = patient.IDXPreferences
                                            .Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture)).ToList();
                    successList.Where(q => Convert.ToInt64(q.MarkettingPrefListId, CultureInfo.InvariantCulture) > 0 && q.IndigoPatientId == Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture)).ToList()
                                .ForEach(q => selectedMarkPreference.Add(Convert.ToInt64(q.MarkettingPrefListId, CultureInfo.InvariantCulture)));

                    var dbPreferencesList = MarketingPrefList.Select(q => q.Preference.Value).ToList();
                    var RemovedPreferenceId = dbPreferencesList.Except(selectedMarkPreference).ToList();
                    RemovedPreferenceId.ForEach(d =>
                    {
                        idxPreference = MarketingPrefList.Find(q => q.Preference == d);
                        patient.IDXPreferences.Remove(idxPreference);
                    });

                    // Save entry in patient table for Marketting preference.
                    foreach (var markettingId in selectedMarkPreference)
                    {
                        if (!patientPreferences.Where(q => q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture) && Convert.ToInt64(q.Preference, CultureInfo.InvariantCulture) == markettingId).Any())
                        {
                            var remarks = successList.Where(q => q.IndigoPatientId == Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture) &&
                                                q.MarkettingPrefListId == Convert.ToString(markettingId, CultureInfo.InvariantCulture)).FirstOrDefault();

                            if (remarks != null)
                            {
                                remarkMarketting = GetRemarksForCommunication(remarks.Remarks);

                                if (!string.IsNullOrEmpty(remarks.CommunicationPrefListId) && !string.IsNullOrEmpty(remarks.MarkettingPrefListId))
                                {
                                    remarkMarketting = remarkMarketting.Contains("|") ? remarkMarketting.Split('|')[1] : remarkMarketting;
                                }
                            }

                            // Insert in IDXPreference table
                            idxPreference = new IDXPreference();
                            idxPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture);
                            idxPreference.PatientId = patient.PatientId;
                            idxPreference.Preference = Convert.ToInt64(markettingId, CultureInfo.InvariantCulture);
                            idxPreference.ModifiedBy = guidUserId;
                            idxPreference.Remarks = remarkMarketting;
                            idxPreference.ModifiedDate = currentTimestamp;
                            patient.IDXPreferences.Add(idxPreference);

                            // Insert in Patient Table
                            patient.ModifiedDate = currentTimestamp;
                            patientRepository.InsertOrUpdate(patient);
                        }
                    }
                }
            }
            catch (DataException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
        }

        /// <summary>
        /// Sets the communication and marketting for carehome.
        /// </summary>
        /// <param name="carehome">The carehome.</param>
        /// <param name="idxPreference">The index preference.</param>
        /// <param name="guidUserId">The unique identifier user identifier.</param>
        /// <param name="currentTimestamp">The current timestamp.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="isCarehomeAvailable">if set to <c>true</c> [is carehome available].</param>
        /// <param name="successList">The success list.</param>
        /// <param name="row">The row.</param>
        private void SetCommunicationAndMarkettingForCarehome(Model.CareHome carehome, IDXPreference idxPreference,
            Guid guidUserId, DateTime currentTimestamp, ICareHomeRepository careHomeRepository, ref bool isCarehomeAvailable,
            IEnumerable<CommunicationPreferencesBusinessModel> successList, CommunicationPreferencesBusinessModel row)
        {
            var selectedCommPreference = new List<long>();
            var selectedMarkPreference = new List<long>();
            var remarkCommunication = string.Empty;
            var remarkMarketting = string.Empty;
            try
            {
                // remove rows for carehome
                carehome = careHomeRepository.GetCareHome(long.Parse(row.IndigoCarehomeId, CultureInfo.InvariantCulture)).FirstOrDefault();

                if (carehome != null)
                {
                    isCarehomeAvailable = true;

                    // Process for Communication Preferences
                    selectedCommPreference = new List<long>();
                    var communicationPrefList = carehome.IDXPreferences
                                                .Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture)).ToList();
                    successList.Where(q => Convert.ToInt64(q.CommunicationPrefListId, CultureInfo.InvariantCulture) > 0 && q.IndigoCarehomeId == Convert.ToString(carehome.CareHomeId, CultureInfo.InvariantCulture)).ToList()
                                                .ForEach(q => selectedCommPreference.Add(Convert.ToInt64(q.CommunicationPrefListId, CultureInfo.InvariantCulture)));
                    var dbCommPreferencesList = communicationPrefList.Select(q => q.Preference.Value).ToList();
                    var RemovedCommPreferenceId = dbCommPreferencesList.Except(selectedCommPreference).ToList();
                    RemovedCommPreferenceId.ForEach(d =>
                    {
                        idxPreference = communicationPrefList.Find(q => q.Preference == d);
                        carehome.IDXPreferences.Remove(idxPreference);
                    });

                    // Save entry in carehome table for Communication preference.
                    var carehomePreferences = carehome.IDXPreferences;
                    foreach (var communicationId in selectedCommPreference)
                    {
                        if (!carehomePreferences.Where(q => q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture) && Convert.ToInt64(q.Preference, CultureInfo.InvariantCulture) == communicationId).Any())
                        {
                            // Return remarks value
                            var remarks = successList.Where(q => q.CommunicationPrefListId == Convert.ToString(communicationId, CultureInfo.InvariantCulture)
&& q.IndigoCarehomeId == Convert.ToString(carehome.CareHomeId, CultureInfo.InvariantCulture)).FirstOrDefault();

                            if (remarks != null)
                            {
                                remarkCommunication = GetRemarksForCommunication(remarks.Remarks);

                                if (!string.IsNullOrEmpty(remarks.CommunicationPrefListId) && !string.IsNullOrEmpty(remarks.MarkettingPrefListId))
                                {
                                    remarkCommunication = remarkCommunication.Contains(CommonConstants.PipeSeparator) ? remarkCommunication.Split('|')[0] : remarkCommunication;
                                }
                            }

                            // Insert in IDXPreference table
                            idxPreference = new IDXPreference();
                            idxPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture);
                            idxPreference.CarehomeId = carehome.CareHomeId;
                            idxPreference.Preference = Convert.ToInt64(communicationId, CultureInfo.InvariantCulture);
                            idxPreference.ModifiedBy = guidUserId;
                            idxPreference.Remarks = remarkCommunication;
                            idxPreference.ModifiedDate = currentTimestamp;
                            carehome.IDXPreferences.Add(idxPreference);

                            // Insert in Carehome Table
                            carehome.ModifiedDate = currentTimestamp;
                            careHomeRepository.InsertOrUpdate(carehome);
                        }
                    }

                    // Process for Communication Preferences
                    selectedMarkPreference = new List<long>();
                    var MarketingPrefList = carehome.IDXPreferences
                                            .Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture)).ToList();
                    successList.Where(q => Convert.ToInt64(q.MarkettingPrefListId, CultureInfo.InvariantCulture) > 0 && q.IndigoCarehomeId == Convert.ToString(carehome.CareHomeId, CultureInfo.InvariantCulture)).ToList()
                                                            .ForEach(q => selectedMarkPreference.Add(Convert.ToInt64(q.MarkettingPrefListId, CultureInfo.InvariantCulture)));

                    var dbPreferencesList = MarketingPrefList.Select(q => q.Preference.Value).ToList();
                    var RemovedPreferenceId = dbPreferencesList.Except(selectedMarkPreference).ToList();
                    RemovedPreferenceId.ForEach(d =>
                    {
                        idxPreference = MarketingPrefList.Find(q => q.Preference == d);
                        carehome.IDXPreferences.Remove(idxPreference);
                    });

                    foreach (var markettingId in selectedMarkPreference)
                    {
                        if (!carehomePreferences.Where(q => q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture) && Convert.ToInt64(q.Preference, CultureInfo.InvariantCulture) == markettingId).Any())
                        {
                            // Return remarks value
                            var remarks = successList.Where(q => q.MarkettingPrefListId == Convert.ToString(markettingId, CultureInfo.InvariantCulture)
&& q.IndigoCarehomeId == Convert.ToString(carehome.CareHomeId, CultureInfo.InvariantCulture)).FirstOrDefault();

                            if (remarks != null)
                            {
                                remarkMarketting = GetRemarksForCommunication(remarks.Remarks);

                                if (!string.IsNullOrEmpty(remarks.CommunicationPrefListId) && !string.IsNullOrEmpty(remarks.MarkettingPrefListId))
                                {
                                    remarkMarketting = remarkMarketting.Contains(CommonConstants.PipeSeparator) ? remarkMarketting.Split('|')[1] : remarkMarketting;
                                }
                            }

                            // Insert in IDXPreference table
                            idxPreference = new IDXPreference();
                            idxPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture);
                            idxPreference.CarehomeId = carehome.CareHomeId;
                            idxPreference.Preference = Convert.ToInt64(markettingId, CultureInfo.InvariantCulture);
                            idxPreference.ModifiedBy = guidUserId;
                            idxPreference.Remarks = remarkMarketting;
                            idxPreference.ModifiedDate = currentTimestamp;
                            carehome.IDXPreferences.Add(idxPreference);

                            // Insert in Patient Table
                            carehome.ModifiedDate = currentTimestamp;
                            careHomeRepository.InsertOrUpdate(carehome);
                        }
                    }
                }
            }
            catch (DataException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.CurrentCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
            catch (Exception e)
            {
                row.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                row.ErrorMessage = e.Message;
                LogHelper.LogException(e, globalUserId);
            }
        }


        /// <summary>
        /// Validates the communication format.
        /// </summary>
        /// <param name="objCommunicationPreferencesBusinessModel">The object communication preferences business model.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="patientRepository">The patient repository.</param>
        /// <param name="customer">The customer.</param>
        /// <param name="careHome">The care home.</param>
        /// <param name="patient">The patient.</param>
        /// <param name="processError">The process error.</param>
        /// <returns></returns>
        private bool ValidateCommunicationFormat(CommunicationPreferencesBusinessModel objCommunicationPreferencesBusinessModel, ICustomerRepository customerRepository,
            ICareHomeRepository careHomeRepository, IPatientRepository patientRepository, ref Model.Customer customer, ref Model.CareHome careHome, ref Model.Patient patient, out StringBuilder processError)
        {
            bool isValid = true;
            processError = new StringBuilder();
            var indigoCustomerId = default(long);

            var careHomeId = Convert.ToString(objCommunicationPreferencesBusinessModel.IndigoCarehomeId, CultureInfo.InvariantCulture);
            var patientId = Convert.ToString(objCommunicationPreferencesBusinessModel.IndigoPatientId, CultureInfo.InvariantCulture);
            var customerId = objCommunicationPreferencesBusinessModel.CustomerId;

            this.PopulateListData(Convert.ToString(Convert.ToInt64(SCAEnums.ListType.CommunicationFormat, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture),
                                Convert.ToString(Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture));


            if (!string.IsNullOrEmpty(objCommunicationPreferencesBusinessModel.CommunicationPreference))
            {
                // Validate List Id for Communication preference
                var listCommunication = this.listModel.Where(l => l.ListCode.Equals(objCommunicationPreferencesBusinessModel.CommunicationPreference, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (listCommunication == null)
                {
                    processError.Append(CommonConstants.Astrik + CommonConstants.InvalidCommunication);
                    isValid = false;
                }
                else
                {
                    objCommunicationPreferencesBusinessModel.CommunicationPrefListId = Convert.ToString(listCommunication.ListId, CultureInfo.InvariantCulture);
                }
            }

            if (!string.IsNullOrEmpty(objCommunicationPreferencesBusinessModel.MarketingPreference))
            {
                // Validate List Id for Marketting preference
                var listMarketting = this.listModel.Where(l => l.ListCode.Equals(objCommunicationPreferencesBusinessModel.MarketingPreference, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (listMarketting == null)
                {
                    processError.Append(CommonConstants.Astrik + CommonConstants.InvalidMarketting);
                    isValid = false;
                }
                else
                {
                    objCommunicationPreferencesBusinessModel.MarkettingPrefListId = Convert.ToString(listMarketting.ListId, CultureInfo.InvariantCulture);
                }
            }

            // chek for customer id
            if (string.IsNullOrEmpty(customerId))
            {
                processError.Append(CommonConstants.Astrik + CommonConstants.MANDATORY_FIELDS_MISSING_KEYWORD + "CustomerId.");
                isValid = false;
            }
            else if (!string.IsNullOrEmpty(customerId) && !CommonHelper.ValidateInteger(customerId))
            {
                processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Customer + CommonConstants.InvalidNumber, customerId));
                isValid = false;
            }

            if (!string.IsNullOrEmpty(customerId))
            {
                var sapCustomerId = customerId.Trim().PadLeft(Convert.ToInt16(CommonConstants.SapIdLength, CultureInfo.InvariantCulture), '0');
                customer = customerRepository.GetCustomerBySAPId(sapCustomerId);
                if (customer == null)
                {
                    isValid = false;
                    processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidCustomerId, customerId));
                }
            }

            // If entry present in either of two columns.
            if (!(string.IsNullOrEmpty(careHomeId)) && !(string.IsNullOrEmpty(patientId)))
            {
                // if two entries present in Patient and Carehome.
                processError.Append(CommonConstants.Astrik + CommonConstants.InvalidPatientCarehome);
                isValid = false;
            }
            else if ((string.IsNullOrEmpty(careHomeId)) && (string.IsNullOrEmpty(patientId)))
            {
                // if two entries present in Patient and Carehome.                
                processError.Append(CommonConstants.Astrik + CommonConstants.InvalidPatientCarehome);
                isValid = false;
            }
            else
            {
                // now checking for which type of import / changes
                if (string.IsNullOrEmpty(careHomeId))
                {
                    // this is patient type
                    objCommunicationPreferencesBusinessModel.IsPatient = true;
                    if (!string.IsNullOrEmpty(patientId) && !CommonHelper.ValidateInteger(patientId))
                    {
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Patient + CommonConstants.InvalidNumber, patientId));
                        isValid = false;
                    }
                    else if (!string.IsNullOrEmpty(patientId) && patientId.Length > 8)
                    {
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, patientId));
                        isValid = false;
                    }
                    else
                    {
                        if (isValid)
                        {
                            indigoCustomerId = customer.CustomerId;
                        }

                        patient = patientRepository.GetPatient(long.Parse(patientId, CultureInfo.InvariantCulture)).Where(q => q.CustomerId == indigoCustomerId).FirstOrDefault();
                        if (patient == null)
                        {
                            isValid = false;
                            processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidPatientsId, patientId));
                        }
                    }
                }
                else if (string.IsNullOrEmpty(patientId))
                {
                    // this is carehome type
                    objCommunicationPreferencesBusinessModel.IsPatient = false;
                    if (!string.IsNullOrEmpty(careHomeId) && !CommonHelper.ValidateInteger(careHomeId))
                    {
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.Carehome + CommonConstants.InvalidNumber, careHomeId));
                        isValid = false;
                    }
                    else if (!string.IsNullOrEmpty(careHomeId) && careHomeId.Length > 8)
                    {
                        processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidLength, careHomeId));
                        isValid = false;
                    }
                    else
                    {
                        if (isValid)
                        {
                            indigoCustomerId = customer.CustomerId;
                        }

                        careHome = careHomeRepository.GetCareHome(long.Parse(careHomeId, CultureInfo.InvariantCulture)).Where(q => q.CustomerId == indigoCustomerId).FirstOrDefault();
                        if (careHome == null)
                        {
                            isValid = false;
                            processError.Append(string.Format(CultureInfo.InvariantCulture, CommonConstants.Astrik + CommonConstants.InvalidCarehomeId, careHomeId));
                        }
                    }
                }
            }

            return isValid;
        }

        /// <summary>
        /// Gets the remarks for communication.
        /// </summary>
        /// <param name="remarks">The remarks.</param>
        /// <returns>string value</returns>
        public string GetRemarksForCommunication(string remarks)
        {
            string retVal = string.Empty;
            if (!string.IsNullOrEmpty(remarks))
            {
                if (remarks == "|")
                {
                    retVal = string.Empty;
                }
                else if (remarks.Trim().StartsWith("|", StringComparison.Ordinal))
                {
                    retVal = remarks.Trim().Substring(1, remarks.Length - 1);
                }
                else if (remarks.EndsWith("|", StringComparison.Ordinal))
                {
                    retVal = remarks.Trim().Substring(1, remarks.Length - 1);
                }
                else
                {
                    retVal = remarks.Trim();
                }
            }

            return retVal;
        }

        #endregion
    }
}
