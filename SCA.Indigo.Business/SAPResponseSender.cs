﻿//-----------------------------------------------------------------------
// <copyright file="SAPResponseSender.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Hosting;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Model;
    using System.ServiceModel;

    /// <summary>
    /// SAPResponseSender Service
    /// </summary>    
    public class SAPResponseSender : ISapSender
    {
        /// <summary>
        /// Date Format
        /// </summary>
        public const string DateFormat = "yyyyMMdd";

        /// <summary>
        /// Hour Minute Format
        /// </summary>
        public const string HourMinuteFormat = "HH:mm";

        /// <summary>
        /// SAP UserId
        /// </summary>
        private static string sapUserId;

        /// <summary>
        /// Order Id
        /// </summary>
        private static string orderIds;

        /// <summary>
        /// SAP Response Log FileName
        /// </summary>
        private static string sapResponseLogFileName = "SAPResponseLog";

        /// <summary>
        /// obj CommonService
        /// </summary>
        private static CommonService objCommonService = null;

        /// <summary> 
        /// Initializes a new instance of the <see cref="T:SAPResponseSender"/> class. 
        /// </summary> 
        public SAPResponseSender()
        {
            GetSAPUserDetails();
        }
       
        /// <summary>
        /// Get OrderDetails
        /// </summary>
        /// <param name="filters">string filters</param>
        /// <returns>List SAPOrderHeaderBusinessModel</returns>
        public SAPResponseBusinessModel GetAllCustomers(string filters)
        {
            var objSAPResponseBusinessModel = new SAPResponseBusinessModel();
            StringBuilder strMessage = new StringBuilder();
            DateTime startTime = DateTime.Now;
            try
            {
                var objCustomerList = new DBHelper().GetAllCustomers(filters);

                if (objCustomerList.Count > 0)
                {
                    var sapCustomerList = objCustomerList.Select(customer => new SAPCustomerPatientBusinessModel
                    {
                        CustomerType = customer.CustomerType,
                        FrontEndId = customer.FrontEndId,
                        SAPCustomerNo = customer.SAPCustomerNumber,
                        SalesOrg = customer.SalesOrg,
                        Distrchannel = customer.DistrChanel,
                        Division = customer.Division,
                        SAPId = customer.SAPId,
                        CareHomeId = Convert.ToString(customer.CareHomeId, CultureInfo.InvariantCulture),
                        PatientStatus = customer.PatientStatus,
                        PatientType = customer.PatientType,
                        Title = customer.Title,
                        Name1 = customer.Name,
                        Name2 = customer.CareHomeName,
                        Name3 = customer.FirstName,
                        Name4 = customer.LastName,
                        HouseNumber = customer.HouseNumber,
                        Street = customer.STREET,
                        Address2 = customer.AddressLine2,
                        Address3 = customer.AddressLine3,
                        City = customer.City,
                        Postcode = customer.PostCode,
                        County = customer.County,
                        Country = customer.Country,
                        TelephoneNumber = customer.TelephoneNumber,
                        Frequency = customer.DeliveryFrequency,
                        Round = customer.Round,
                        RoundId = customer.RoundId,
                        Sequence = customer.SEQUENCE,
                        LDDT = Convert.ToString(customer.LDDT, CultureInfo.InvariantCulture),
                        NDDT = Convert.ToString(customer.NDDT, CultureInfo.InvariantCulture),
                        DelInstruction = customer.DELINST,
                        ADP = customer.ADP
                    }).ToList();

                    // string strCustomerXml = CreateCustomerXmlstring(sapCustomerList);
                    CreateCustomerXmlstring(sapCustomerList);
                    objSAPResponseBusinessModel.ListCount = objCustomerList.Count;
                }
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                DateTime endTime = DateTime.Now;
                strMessage.AppendLine(e.Message);
                strMessage.Append(e.StackTrace);
                LogToFile(startTime, endTime, strMessage.ToString(), sapResponseLogFileName);
            }

            return objSAPResponseBusinessModel;
        }

        /// <summary>
        /// Get OrderDetails
        /// </summary>
        /// <param name="filters">string filters</param>
        /// <returns>List SAPOrderHeaderBusinessModel</returns>
        public SAPResponseBusinessModel GetOrderDetails(string filters)
        {
            var objSAPResponseBusinessModel = new SAPResponseBusinessModel();
            StringBuilder message = new StringBuilder();
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            try
            {
                objSAPResponseBusinessModel = GetOrderDetails();
                if (objSAPResponseBusinessModel.ListCount > 0)
                {                    
                    int statusCode = SendOrderDataToSapPI(objSAPResponseBusinessModel.StringXml);                    
                    message.Append("[Total Order Sent-" + objSAPResponseBusinessModel.ListCount + "]").Append(Environment.NewLine);
                    if (statusCode == 200)
                    {
                        objSAPResponseBusinessModel.SAPTransferStatus = true;
                    }

                    objSAPResponseBusinessModel.StringXml = string.Empty;
                    message.Append("[\n SAP PI HTTP Status-" + statusCode + "]");
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
                message.Append(e.Message + "\n " + e.StackTrace);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                message.Append(e.Message + "\n " + e.StackTrace);
            }

            endTime = DateTime.Now;
            LogToFile(startTime, endTime, message.ToString(), sapResponseLogFileName);
            return objSAPResponseBusinessModel;
        }

        /// <summary>
        /// Create Customer Xml string
        /// </summary>
        /// <param name="sapCustomerList">List SAPCustomer Patient BusinessModel</param>
        private void CreateCustomerXmlstring(List<SAPCustomerPatientBusinessModel> sapCustomerList)
        {            
            try
            {
                string orderTemplateFilePath = HostingEnvironment.MapPath("~/SAPFiles/CustomerPatientTemplate.xml");
                var doc = XDocument.Load(orderTemplateFilePath);
                XmlDocument xmlDoc = null;                
                Dictionary<string, string> objDictionary = null;

                int maxRecordCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxRecordCount"], CultureInfo.InvariantCulture);
                var strCustomer = new StringBuilder();

                // Get CountyList                        
                string strCustomerTemplate = string.Empty;
                var patientIdSb = new StringBuilder();
                var patientId = string.Empty;
                var carehomeIdSb = new StringBuilder();
                var carehomeId = string.Empty;
                bool isBelowMaxRecords = false;
                int recordcount = 0;

                foreach (var customer in sapCustomerList)
                {
                    recordcount++;
                    isBelowMaxRecords = true;
                  
                    objDictionary = new Dictionary<string, string>();                    

                    objDictionary.Add("CUSTOMERTYPE", customer.CustomerType);
                    objDictionary.Add("FRONTENDID", Convert.ToString(customer.FrontEndId, CultureInfo.InvariantCulture));
                    objDictionary.Add("CUSTOMERNO", customer.SAPCustomerNo);
                    objDictionary.Add("SALESORG", customer.SalesOrg);
                    objDictionary.Add("DISTRCHANNEL", customer.Distrchannel);
                    objDictionary.Add("DIVISION", customer.Division);
                    objDictionary.Add("SAPID", customer.SAPId);
                    objDictionary.Add("CAREHOMEID", customer.CareHomeId);
                    objDictionary.Add("PATSTATUS", customer.PatientStatus);
                    objDictionary.Add("PATIENTTYPE", customer.PatientType);
                    objDictionary.Add("TITLE", customer.Title);
                    objDictionary.Add("NAME1", customer.Name1);
                    objDictionary.Add("NAME2", customer.Name2);
                    objDictionary.Add("NAME3", customer.Name3);
                    objDictionary.Add("NAME4", customer.Name4);
                    objDictionary.Add("HOUSENUMBER", customer.HouseNumber);
                    objDictionary.Add("STREET", customer.Street);
                    objDictionary.Add("ADDRESS2", customer.Address2);
                    objDictionary.Add("ADDRESS3", customer.Address3);
                    objDictionary.Add("CITY", customer.City);
                    objDictionary.Add("POSTCODE", customer.Postcode);
                    objDictionary.Add("COUNTY", customer.County);
                    objDictionary.Add("COUNTRY", customer.Country);
                    objDictionary.Add("TELEPHONE", customer.TelephoneNumber);
                    objDictionary.Add("FREQUENCY", customer.Frequency);
                    objDictionary.Add("ROUND", customer.Round);
                    objDictionary.Add("ROUNDID", customer.RoundId);
                    objDictionary.Add("SEQUENCE", customer.Sequence);
                    objDictionary.Add("LDDT", customer.LDDT);
                    objDictionary.Add("NDDT", customer.NDDT);
                    objDictionary.Add("DELINST", customer.DelInstruction);
                    objDictionary.Add("ADP", customer.ADP);
                   
                    xmlDoc = new XmlDocument();
                    XmlNode xmlNode = this.CreateRootNode("ZSDHDSCUSTOMER", xmlDoc);                   
                    xmlDoc.AppendChild(xmlNode);

                    foreach (var item in objDictionary)
                    {
                        xmlNode.AppendChild(this.CreateElement(item.Key, xmlDoc, item.Value));   
                    }

                    if (customer.CustomerType == "PA"){patientIdSb.Append(customer.FrontEndId).Append(",");}else{carehomeIdSb.Append(customer.FrontEndId).Append(",");}                        
                    
                    strCustomer.Append(xmlDoc.OuterXml.ToString());

                    patientId = patientIdSb.ToString();
                    carehomeId = carehomeIdSb.ToString();

                    if (recordcount % maxRecordCount == 0)
                    {
                        strCustomerTemplate = doc.ToString();
                        strCustomerTemplate = strCustomerTemplate.Replace("<Data>Data</Data>", strCustomer.ToString());                                                
                        var status = this.SendCustomerDataToSapPI(strCustomerTemplate, recordcount);

                        if (status == 200 && (!string.IsNullOrEmpty(patientId) || !string.IsNullOrEmpty(carehomeId)))
                        {
                            UpdateSendToSAPFlag(patientId, "PA");
                            UpdateSendToSAPFlag(carehomeId, "HO");
                        }

                        recordcount = 0;
                        isBelowMaxRecords = false;
                    }
                }

                if (isBelowMaxRecords)
                {
                    strCustomerTemplate = doc.ToString();
                    strCustomerTemplate = strCustomerTemplate.Replace("<Data>Data</Data>", strCustomer.ToString());                    
                    var status =this.SendCustomerDataToSapPI(strCustomerTemplate, recordcount);
                    if (status == 200 && (!string.IsNullOrEmpty(patientId) || !string.IsNullOrEmpty(carehomeId)))
                    {
                        UpdateSendToSAPFlag(patientId, "PA");
                        UpdateSendToSAPFlag(carehomeId, "HO");
                    }

                    isBelowMaxRecords = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// CreateElement
        /// </summary>
        /// <param name="childElement"></param>
        /// <param name="xmlDoc"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private XmlNode CreateElement(string childElement, XmlDocument xmlDoc, string value)
        {
            XmlNode xmlNode = xmlDoc.CreateElement(childElement);
            xmlNode.InnerText = value;
            return xmlNode;
        }
      

        /// <summary>
        /// CreateRootNode
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private XmlNode CreateRootNode(string rootElement, XmlDocument xmlDoc)
        {
            XmlNode xmlNode = xmlDoc.CreateElement(rootElement);
            XmlAttribute attribute = xmlDoc.CreateAttribute("SEGMENT");
            attribute.Value = "1";
            xmlNode.Attributes.Append(attribute);
            return xmlNode;
        }       

        /// <summary>
        /// Update Patient Status IsSentToSAP =True once patient is sent to sap after modification
        /// </summary>        
        /// <param name="frontendId">string frontendId</param>
        /// <param name="recordType">string recordType</param>
        private void UpdateSendToSAPFlag(string frontendId, string recordType)
        {
            try
            {
                if (!string.IsNullOrEmpty(frontendId))
                {
                    frontendId = frontendId.Remove(frontendId.Length - 1);
                    new DBHelper().UpdateSendToSAPFlag(frontendId, recordType, sapUserId);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns product xml string
        /// </summary>
        /// <param name="productList">List SAPOrderDetails BusinessModel</param>
        /// <param name="orderHeader">SAP OrderHeader BusinessModel</param>
        /// <returns>string Product xml</returns>
        private string CreateDetailsXmlstring(List<SAPOrderDetailsBusinessModel> productList, SAPOrderHeaderBusinessModel orderHeader, Int64 customerId)
        {
            var strProduct = new StringBuilder();
            string productNDD = string.Empty;
            var currentDate = DateTime.Now;
            if (productList != null)
            {
                foreach (var product in productList)
                {
                    strProduct.Append("<ZSDHDSORDERITEM SEGMENT='1'>");
                    strProduct.Append("<BASEMATERIAL>" + product.BASEMATERIAL + "</BASEMATERIAL>");
                    strProduct.Append("<QUANTITY>" + product.QUANTITY + "</QUANTITY>");
                    strProduct.Append("<SALESUNIT>" + product.SALESUNIT + "</SALESUNIT>");
                    strProduct.Append("<DELIVERYDATE>" + product.DELIVERYDATE + "</DELIVERYDATE>");
                    if (!string.IsNullOrEmpty(product.NDDT))
                    {
                        strProduct.Append("<ACPPD/>");
                        strProduct.Append("<ASPPD/>");
                        strProduct.Append("<NDDT>" + product.NDDT + "</NDDT>");
                    }
                    else
                    {
                        strProduct.Append("<ACPPD>" + product.ACPPD + "</ACPPD>");
                        strProduct.Append("<ASPPD>" + product.ASPPD + "</ASPPD>");
                        // Calculate NDD for product in order which is in prescription 
                        currentDate = Convert.ToDateTime(product.DerivedNDD, CultureInfo.InvariantCulture);                       
                        productNDD = objCommonService.GetNdd(currentDate,
                                                            Convert.ToInt64(product.Frequency, CultureInfo.InvariantCulture),
                                                            customerId,
                                                            Guid.Parse(sapUserId), false).ToString(CultureInfo.InvariantCulture);                       

                        productNDD = Convert.ToDateTime(productNDD, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
                        strProduct.Append("<NDDT>" + productNDD + "</NDDT>");
                    }

                    strProduct.Append("<FREEOFFCHARGE>" + product.ITEMFOC + "</FREEOFFCHARGE>");
                    strProduct.Append("</ZSDHDSORDERITEM>");
                }
            }

            return strProduct.ToString();
        }

        #region //thism method is moved in common service
        /*
        /// <summary>
        /// CheckDateAvailabilityInHolidayProcess
        /// </summary>
        /// <param name="nextDeliveryDate"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        private static bool CheckDateAvailabilityInHolidayProcess(string nextDeliveryDate, long CustomerID)
        {            
            var objHolidayBusinessModel = new HolidayProcessAdminBusinessModels();
            objHolidayBusinessModel.CustomerId = CustomerID;
            objHolidayBusinessModel.DerivedNDD = Convert.ToString(nextDeliveryDate, CultureInfo.InvariantCulture);
            objHolidayBusinessModel.OrderCreationTime = Convert.ToString(DateTime.Now.ToString(HourMinuteFormat, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            objHolidayBusinessModel.UserId = new Guid(sapUserId);                       
            var strArrangedDate = (string)objCommonService.GetArrangedDeliveryDate(objHolidayBusinessModel);
            if (strArrangedDate != null)
            {
                ArrangedNDD = Convert.ToDateTime(strArrangedDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            return false;
        }
        */
        #endregion

        /// <summary>
        /// Create Header Xml string
        /// </summary>
        /// <param name="orderDetails">SAPOrderHeaderBusinessModel OrderDetails</param>
        /// <returns>Return XML string</returns>
        private string CreateHeaderXmlstring(SAPOrderHeaderBusinessModel orderDetails)
        {
            var strHeader = new StringBuilder();
            strHeader.Append("<ORDERTYPE>" + orderDetails.ORDERTYPE + "</ORDERTYPE>");
            strHeader.Append("<FRONTENDID>" + orderDetails.FRONTENDID + "</FRONTENDID>");
            strHeader.Append("<CUSTOMERNO>" + orderDetails.CUSTOMERNO + "</CUSTOMERNO>");
            strHeader.Append("<SALESORG>" + orderDetails.SALESORG + "</SALESORG>");
            strHeader.Append("<DISTRCHANNEL>" + orderDetails.DISTRCHANNEL + "</DISTRCHANNEL>");
            strHeader.Append("<DIVISION>" + orderDetails.DIVISION + "</DIVISION>");
            strHeader.Append("<SAPID>" + orderDetails.SAPID + "</SAPID>");
           
            if (orderDetails.CAREHOMEID > 0)
            {
                strHeader.Append("<CAREHOMEID>" + orderDetails.CAREHOMEID + "</CAREHOMEID>");
            }
            else
            {
                strHeader.Append("<CAREHOMEID></CAREHOMEID>");
            }

            strHeader.Append("<NOSERVCHARGE>" + orderDetails.NOSERVCHARGE + "</NOSERVCHARGE>");
            strHeader.Append("<ASAINDICATOR>" + orderDetails.ASAINDICATOR + "</ASAINDICATOR>");
            strHeader.Append("<EMAIL>" + orderDetails.EMAIL + "</EMAIL>");
            strHeader.Append("<BILLTO>" + orderDetails.BILLTO + "</BILLTO>");
            return strHeader.ToString();
        }

        /// <summary>
        /// Get Order header information
        /// </summary>
        /// <returns>string OrderXml</returns>
        private SAPResponseBusinessModel GetOrderDetails()
        {
            try
            {
                var orderIdsSb = new StringBuilder();
                orderIds = string.Empty;
                string orderTemplateFilePath = HostingEnvironment.MapPath("~/SAPFiles/OrderTemplate.xml");
                var doc = XDocument.Load(orderTemplateFilePath);
                var orderHeaders = new DBHelper().GetHeaderDetails();
                var objSAPResponseBusinessModel = new SAPResponseBusinessModel();

                /// if 1 or more than 1 records are available then send data as xml else nothing.
                if (orderHeaders.Count > 0)
                {
                    var orderList = orderHeaders.Select(Order => new SAPOrderHeaderBusinessModel
                    {
                        ORDERTYPE = Order.OrderType,
                        FRONTENDID = Order.OrderId,
                        CUSTOMERID = Order.CustomerId,
                        CUSTOMERNO = Order.SAPCustomerNumber,
                        SALESORG = Order.SalesOrg,
                        DISTRCHANNEL = Order.DistrChanel,
                        DIVISION = Order.Division,
                        SAPID = Order.SAPId,
                        CAREHOMEID = Convert.ToInt32(Order.CareHomeId, CultureInfo.InvariantCulture),
                        NOSERVCHARGE = Order.NOSERVCHARGE,
                        ORDERFOC = Order.ORDERFOC,
                        ASAINDICATOR = Order.ASAINDICATOR,
                        EMAIL = Order.EmailTo,
                        BILLTO = Order.BillTo,
                        ROUND = Order.Round,
                        ROUNDID = Order.RoundId,
                        POSTCODE = Order.Postcode,
                        PATIENTID = Order.PatientId,
                        PONUMBER = Order.PONumber,
                        CHINDIGOID = Convert.ToInt64(Order.CHFrontEndId, CultureInfo.InvariantCulture),
                    }).ToList();
                    int orderCount = 0;
                    var orderXmlSb = new StringBuilder();
                    string strOrderXml = string.Empty, strOrderHeader = string.Empty, strOrderDetails = string.Empty, strTeplate = string.Empty;
                    objCommonService = new CommonService();
                    foreach (var orderHeader in orderList)
                    {
                        strOrderHeader = CreateHeaderXmlstring(orderHeader);
                        var orderDetails = GetOrderItemsDetails(orderHeader);
                        if (orderDetails.ORDERITEM.Count > 0)
                        {
                            orderIdsSb.Append(",").Append(orderHeader.FRONTENDID);
                            var objCustomerParameterBusinessModel = new CustomerParameterBusinessModel();
                            objCustomerParameterBusinessModel.CustomerID = orderHeader.CUSTOMERID;

                            objCommonService.PostcodeMatrixList = objCommonService.GetPostcodeMatrixForPatient(Convert.ToString(orderHeader.CUSTOMERID, CultureInfo.InvariantCulture),
                                                                                                      Convert.ToString(orderHeader.PATIENTID, CultureInfo.InvariantCulture),
                                                                                                      Convert.ToString(orderHeader.CHINDIGOID, CultureInfo.InvariantCulture),
                                                                                                      sapUserId);

                            objCommonService.CustomerParameterList = objCommonService.GetCustomerParameter(objCustomerParameterBusinessModel);
                            
                            strOrderDetails = this.CreateDetailsXmlstring(orderDetails.ORDERITEM, 
                                                                            orderHeader,
                                                                            objCustomerParameterBusinessModel.CustomerID);
                            orderXmlSb.Append("<ZSDHDSORDER SEGMENT='1'>").Append(strOrderHeader).Append(strOrderDetails).Append("</ZSDHDSORDER>");
                            orderCount++;

                            objCustomerParameterBusinessModel.CustomerID = 0;
                            objCommonService.PostcodeMatrixList = null;
                            objCommonService.CustomerParameterList = null;
                        }
                    }

                    orderIds = orderIdsSb.ToString();
                    if (Convert.ToString(orderIds, CultureInfo.InvariantCulture).Length > 0)
                    {
                        orderIds = orderIds.Remove(0, 1);
                    }

                    strTeplate = doc.ToString();
                    strOrderXml = orderXmlSb.ToString();
                    strOrderXml = strTeplate.Replace("<ZSDHDSORDER>Data</ZSDHDSORDER>", strOrderXml);
                    StringBuilder strMessage = new StringBuilder();
                    strMessage.Append("[Total Order Sent-" + orderList.Count + "]");
                    objSAPResponseBusinessModel.StringXml = strOrderXml;
                    objSAPResponseBusinessModel.ListCount = orderCount;
                }
                else
                {
                    objSAPResponseBusinessModel.StringXml = string.Empty;
                    objSAPResponseBusinessModel.ListCount = 0;
                }

                return objSAPResponseBusinessModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Order Details based on OrderId
        /// </summary>
        /// <param name="objSAPOrderHeaderBusinessModel">SAP OrderHeader BusinessModel</param>
        /// <returns>SAPOrderHeaderBusinessModel SAPOrderHeaderBusinessModel</returns>
        private SAPOrderHeaderBusinessModel GetOrderItemsDetails(SAPOrderHeaderBusinessModel objSAPOrderHeaderBusinessModel)
        {
            try
            {
                if (objSAPOrderHeaderBusinessModel != null)
                {
                    var orderItems = new DBHelper().GetOrderItemsDetails(objSAPOrderHeaderBusinessModel.FRONTENDID);
                    var orderDetailsList = orderItems.Select(OrderDetail => new SAPOrderDetailsBusinessModel
                    {
                        BASEMATERIAL = OrderDetail.BaseMaterial,
                        QUANTITY = OrderDetail.Quantity,
                        SALESUNIT = OrderDetail.SalesUnit,
                        DELIVERYDATE = OrderDetail.DeliveryDate,
                        SAPOrderDeliveryDate = OrderDetail.SAPOrderDeliveryDate,
                        ITEMFOC = OrderDetail.ITEMFOC,
                        Frequency = OrderDetail.Frequency,
                        ACPPD = OrderDetail.ACPPD,
                        ASPPD = OrderDetail.ASPPD,
                        NDDT = OrderDetail.NDD,
                        DerivedNDD = OrderDetail.DerivedNDD
                    }).ToList();
                    objSAPOrderHeaderBusinessModel.ORDERITEM = orderDetailsList;
                    return objSAPOrderHeaderBusinessModel;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (Exception)
            {
                throw;
            }

            return null;
        }

        /// <summary>
        /// Log string message to specified Log file
        /// </summary>
        /// <param name="startTime">DateTime startTime</param>
        /// <param name="endTime">DateTime endTime</param>
        /// <param name="logMessage">string logMessage</param>
        /// <param name="logFileName">string logFileName</param>
        private void LogToFile(DateTime startTime, DateTime endTime, string logMessage, string logFileName)
        {
            // create a writer and open the file
            string strLogFilePath = HostingEnvironment.MapPath("~/SAPFiles/" + logFileName + ".txt");
            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(string.Empty);
                textWriter.WriteLine("+++++++++++ Batch Started at " + startTime + "  +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++ Batch End at " + endTime + "        +++++++++++++++");
            }
        }

        /// <summary>
        /// Log string message to specified Log file
        /// </summary>        
        /// <param name="logMessage">string logMessage</param>
        /// <param name="logFileName">string logFileName</param>
        private void LogToFile(string logMessage, string logFileName)
        {
            // create a writer and open the file
            string strLogFilePath = HostingEnvironment.MapPath("~/SAPFiles/" + logFileName + ".txt");
            File.WriteAllText(strLogFilePath, logMessage);
        }

        /// <summary>
        /// Send Customer response to SAP PI
        /// </summary>
        /// <param name="strCustomerXml">Customer's data in XML</param>
        /// <param name="record">Number of records</param>
        /// <returns>number of records affected</returns>
        private int SendCustomerDataToSapPI(string strCustomerXml, int record)
        {
            try
            {
                if (!string.IsNullOrEmpty(strCustomerXml))
                {
                    DateTime startTime = DateTime.Now;

                    strCustomerXml = "<?xml version='1.0' encoding='UTF-8' ?>" + strCustomerXml;
                    strCustomerXml = strCustomerXml.Replace("\n", string.Empty).Replace("\r", string.Empty);
                    string sysTag = Convert.ToString(ConfigurationManager.AppSettings["SAPSystemTag"], CultureInfo.InvariantCulture);
                    strCustomerXml = strCustomerXml.Replace("<SYSTEM>SY1</SYSTEM>", "<SYSTEM>" + sysTag + "</SYSTEM>");                                        
                    var serviceHelper = new SAPServiceHelper(Convert.ToString(ConfigurationManager.AppSettings["PatientInboundRequest"], CultureInfo.InvariantCulture));
                    LogOrderxml(strCustomerXml, "Customer");                    
                    var responseData = serviceHelper.PostMethodService(strCustomerXml);
                    var strMessage = new StringBuilder();
                    var statusCode = 0;

                    if (responseData != null)
                    {
                        statusCode = Convert.ToInt32(responseData.StatusCode, CultureInfo.InvariantCulture);
                    }
                    
                    strMessage.Append("[Total Patient and Carehome Sent-" + record + "]").Append(Environment.NewLine);
                    
                    strMessage.Append("[\n SAP PI HTTP Status-" + statusCode + "]");

                    DateTime endTime = DateTime.Now;
                    this.LogToFile(startTime, endTime, strMessage.ToString(), sapResponseLogFileName);

                    return statusCode;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return 0;
        }

        /// <summary>
        ///  Send Order response to SAP PI
        /// </summary>
        /// <param name="strOrderXml">Order data in XML format</param>
        /// <returns>Number of rows affected</returns>
        private int SendOrderDataToSapPI(string strOrderXml)
        {
            int responseCode = 0;

            try
            {
                if (!string.IsNullOrEmpty(strOrderXml))
                {
                    strOrderXml = "<?xml version='1.0' encoding='UTF-8' ?>" + strOrderXml;
                    strOrderXml = strOrderXml.Replace("\n", string.Empty).Replace("\r", string.Empty);
                    string sysTag = Convert.ToString(ConfigurationManager.AppSettings["SAPSystemTag"], CultureInfo.InvariantCulture);
                    strOrderXml = strOrderXml.Replace("<SYSTEM>SY1</SYSTEM>", "<SYSTEM>" + sysTag + "</SYSTEM>");
                    var serviceHelper = new SAPServiceHelper(Convert.ToString(ConfigurationManager.AppSettings["OrderInboundRequest"], CultureInfo.InvariantCulture));                    
                    this.LogOrderxml(strOrderXml, "Orders");
                    new DBHelper().UpdateSAPOrderStatus(orderIds);
                    var responseData = serviceHelper.PostMethodService(strOrderXml);                    

                    
                    if (responseData != null)
                    {
                        responseCode = Convert.ToInt32(responseData.StatusCode, CultureInfo.InvariantCulture);
                        if (responseCode == 200)
                        {
                            new DBHelper().UpdateSAPOrderStatus(orderIds);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return 0;
        }

        /// <summary>
        /// Get SAP userId
        /// </summary>
        private void GetSAPUserDetails()
        {
            sapUserId = new DBHelper().GetSAPUserDetails();
        }

        /// <summary>
        /// Log Message to log file
        /// </summary>
        /// <param name="strXml">string strXml</param>
        /// <param name="args">string args</param>
        private void LogOrderxml(string strXml, string args)
        {
            bool genareteXmlLog = false;
            if (args == "Orders")
            {
                genareteXmlLog = Convert.ToBoolean(ConfigurationManager.AppSettings["GenareteOrderXml"], CultureInfo.InvariantCulture);
                if (genareteXmlLog)
                {
                    this.LogToFile(strXml, "OrderXmlLog");
                }
            }
            else
            {
                genareteXmlLog = Convert.ToBoolean(ConfigurationManager.AppSettings["GenareteCustomerXml"], CultureInfo.InvariantCulture);
                if (genareteXmlLog)
                {
                    this.LogToFile(strXml, "PatientXmlLog");
                }
            }
        }
    }
}