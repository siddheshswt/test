﻿//----------------------------------------------------------------------------------------------
// <copyright file="Search.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Model;
    using System.ServiceModel;

    /// <summary>
    /// Search Business class
    /// </summary>    
    public class Search : ISearch
    {
        /// <summary>
        /// Get Authorised Customers
        /// </summary>
        /// <param name="userId">The user identifier</param>
        /// <returns>List&lt (DropDownDataBusinessModel) </returns>
        public List<DropDownDataBusinessModel> GetAuthorizeCustomers(string userId)
        {
            try
            {
                var customerDetails = (new DBHelper()).GetAuthoriseCustomers(Guid.Parse(userId));

                var result = customerDetails.Select(item => new DropDownDataBusinessModel()
                {
                    Value = item.CustomerId,
                    DisplayText = item.CustomerName
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Return Patient Details depending upon the search criteria
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientStatus">The patient status.</param>
        /// <param name="patientType">Type of the patient.</param>
        /// <param name="orderNo">The order no.</param>
        /// <param name ="searchId"> The Patient/Customer/Order Id to search</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="page">Page No</param>
        /// <param name="rowCount">No of Rows</param>
        /// <param name="sortColumn">Sort Column</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>List (SearchBusinessModel) .</returns>
		public List<SearchBusinessModel> GetPatientDetails(string customerId, string patientStatus, string patientType, string orderNo, string searchId, string searchText, 
                                                           string userId, string page, string rowCount, string sortColumn, string sortOrder)
        {
            List<SearchBusinessModel> result = null;
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    searchText = searchText.Replace('$', '/').Replace("'", "''");
                }

                //Code Comment For Front End implementation of Google Search Clone
                //if (string.IsNullOrEmpty(searchText))
                //{
                    var patientDetails = (new DBHelper()).DisplayPatientDetails(Convert.ToInt64(page, CultureInfo.InvariantCulture),
                                                                                Convert.ToInt64(rowCount, CultureInfo.InvariantCulture),
                                                                                searchText, customerId, patientStatus,
                                                                                Convert.ToInt64(patientType, CultureInfo.CurrentCulture),
                                                                                orderNo, searchId, Convert.ToString(userId, CultureInfo.InvariantCulture),
                                                                                sortColumn, sortOrder);

                    if (patientDetails == null)
                        return new List<SearchBusinessModel>();

                    result = patientDetails.Select(item => new SearchBusinessModel()
                    {
                        PatientId = item.PatientId,
                        TitleId = item.TitleId,
                        PatientName = item.PatientName,
                        Postcode = item.PostCode,
                        DOB = !string.IsNullOrEmpty(item.DateofBirth.ToString()) ? Convert.ToDateTime(item.DateofBirth, CultureInfo.InvariantCulture)
                                    .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
                        PatientType = item.PatientType,
                        PatientStatus = item.PatientStatus,
                        PatientCustomerId = Convert.ToInt32(item.PatientCustomer, CultureInfo.InvariantCulture),
                        DeliveryDate = !string.IsNullOrEmpty(item.DeliveryDate.ToString()) ? Convert.ToDateTime(item.DeliveryDate, CultureInfo.InvariantCulture)
                                    .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
                        OrderActivated = Convert.ToInt32(item.OrderActivated, CultureInfo.InvariantCulture),
                        HasActiveOrder = item.HasActiveOrder,
                        PatientCustomerName = item.CustomerName,
                        CareHomeName = item.CareHomeName,
                        CareHomeId = item.CareHomeId,
                        HouseName = item.HouseName,
                        HouseNumber = item.HouseNumber,
                        ADD1 = item.AddressLine1,
                        ADD2 = item.AddressLine2,
                        City = item.City,
                        Country = item.Country,
                        County = item.County,
                        TelephoneNumber = item.TelephoneNumber,
                        SAPCustomerNumber = !string.IsNullOrEmpty(item.SAPCustomerNumber) ? item.SAPCustomerNumber.TrimStart(new char[] { '0' }) : item.SAPCustomerNumber,
                        SAPPatientNumber = !string.IsNullOrEmpty(item.SAPPatientNumber) ? item.SAPPatientNumber.TrimStart(new char[] { '0' }) : item.SAPPatientNumber,
                        ObjectType = item.objectType,
                        TotalRecords = Convert.ToInt64(item.TotalRecords, CultureInfo.InvariantCulture),
                    }).ToList();

                //}
                //else
                //{
                //    searchText = searchText.Replace(" ", " AND ").Replace(",", " AND ");

                //    GoogleSearch google = new GoogleSearch();
                //    var res = google.GetGoogleSearchResults(searchText);

                //    result = res.Select(item => new SearchBusinessModel()
                //    {
                //        PatientId = item.PatientId,
                //        TitleId = item.TitleId,
                //        PatientName = item.PatientName,
                //        Postcode = item.PostCode,
                //        DOB = !string.IsNullOrEmpty(item.DateofBirth.ToString()) ? Convert.ToDateTime(item.DateofBirth, CultureInfo.InvariantCulture)
                //                    .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
                //        PatientType = item.PatientType,
                //        PatientStatus = item.PatientStatus,
                //        PatientCustomerId = Convert.ToInt32(item.CustomerId, CultureInfo.InvariantCulture),
                //        DeliveryDate = !string.IsNullOrEmpty(item.DeliveryDate.ToString()) ? Convert.ToDateTime(item.DeliveryDate, CultureInfo.InvariantCulture)
                //                    .ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture) : string.Empty,
                //        OrderActivated = item.NoOfActiveOrders > 0 ? 1 : 0,
                //        HasActiveOrder = item.NoOfActiveOrders > 0 ? true : false,
                //        PatientCustomerName = item.CustomerName,
                //        CareHomeName = item.CareHomeName,
                //        CareHomeId = item.CareHomeId,
                //        HouseName = item.HouseName,
                //        HouseNumber = item.HouseNumber,
                //        ADD1 = item.AddressLine1,
                //        ADD2 = item.AddressLine2,
                //        City = item.City,
                //        Country = item.Country,
                //        County = item.County,
                //        TelephoneNumber = item.TelephoneNumber,
                //        SAPCustomerNumber = !string.IsNullOrEmpty(item.SAPCustomerNumber) ? item.SAPCustomerNumber.TrimStart(new char[] { '0' }) : item.SAPCustomerNumber,
                //        SAPPatientNumber = !string.IsNullOrEmpty(item.SAPPatientNumber) ? item.SAPPatientNumber.TrimStart(new char[] { '0' }) : item.SAPPatientNumber,
                //        ObjectType = item.objectType,
                //        TotalRecords = Convert.ToInt64(res.Count, CultureInfo.InvariantCulture),
                //    }).ToList();
                //}

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Get Final Search Result for Grid
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List (SearchBusinessModel) .</returns>
        public List<SearchBusinessModel> GetSearchResult(string searchText, string userId)
        {
            try
            {
                if (!string.IsNullOrEmpty(searchText))
                {
                    searchText = searchText.Replace('$', '/');
                }

                 if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(userId))
                 {
                     var searchDetails = (new DBHelper()).GetSearchResults(searchText, Convert.ToString(userId, CultureInfo.InvariantCulture));
                     if (searchDetails.Count > 0)
                     {
                         return searchDetails.Select(item => new SearchBusinessModel()
                         {
                             SearchText = item.Result,
                         }).ToList();
                     }
                 }
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Get status dropdown
        /// </summary>
        /// <param name="StatusIds">ListType of Patient,carehome,customer status</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List (SearchBusinessModel) .</returns>
		public List<SearchBusinessModel> GetStatusDropDown(string statusIds, string userId)
        {
            try
            {
                var statusDropDown = (new DBHelper()).GetPatientCustomerCareHomeStatus(statusIds);
                var result = statusDropDown.Select(item => new SearchBusinessModel()
                {
                    StatusDropDownvalue = item.ListId,
                    StatusDropDownText = item.Text
                }).ToList();

                return result;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }

            return null;
        }
    }
}