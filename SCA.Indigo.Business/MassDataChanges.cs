﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Sachin
// Created          : 09-Jun-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassChangesService.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;
    using EFModel = SCA.Indigo.Model;
    using System.Collections.ObjectModel;
    using SCA.Indigo.Business.BusinessModels.MassDataImport;
    using LinqToExcel;
    using System.Text;
    using System.Configuration;
    using System.ServiceModel;
    
    public class MassDataChanges : IMassDataChanges
    {
        #region private
		private string massChangesLogs = ConfigurationManager.AppSettings["MassChangesLogs"];
        private DateTime defaultEndDate = new DateTime(2099, 12, 31);        
        private bool isImport = true;        
        private MassDataImport massDataImport = null;
        private string stringSeparator = ",";
        #endregion

        string globalUserId = string.Empty;
        DateTime currentDate;        
        private List<string> removeColumn = null;
        private string fileName = string.Empty;

        /// <summary>
        /// Maximum data limit for Mass Update
        /// </summary>
        private static string massUploadMaxLimit = ConfigurationManager.AppSettings["MassUploadMaxLimit"];

        #region OrderMassUpdate

        /// <summary>
        /// Entry point for Processing Of Uploaded Order Mass Update file
        /// </summary>
        /// <param name="excelFactory">Excel Factory Object of the file to be processed</param>
        /// <returns></returns>
        private string ProcessOrderUpdate(ExcelQueryFactory excelFactory)
        {
            var userId = globalUserId;
            currentDate = DateTime.Now;
            var filePath = string.Empty;

            try
            {
                if (excelFactory != null)
                {                    
                    var ordersToBeUpdated = massDataImport.GetNonblankRows<MassDataOrderBusinessModel>(excelFactory, massUploadMaxLimit);
                    filePath = MassUpdateOrder(ordersToBeUpdated);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            return filePath;
        }

        /// <summary>
        /// Accepts the Order Products to be updated and Calls Validation functions
        /// </summary>
        /// <param name="orderProductsToBeUpdated">List of the Order Products to be updated</param>
        /// <returns></returns>
        protected string MassUpdateOrder(List<MassDataOrderBusinessModel> orderProductsToBeUpdated)
        {
            IIDXOrderProductRepository orderProductRepository = null;
            UnitOfWork orderProductUnitOfWork = null;
            
            var validCount = 0;
            string userId = string.Empty;
            bool orderDeliveryDateInSync = false;
            bool orderFound = false;
            bool productFound = false;
            var filePath = string.Empty;            

            try
            {
                if (orderProductsToBeUpdated == null)
                {                    
                    return null;
                }
                orderProductRepository = new IDXOrderProductRepository();
                orderProductUnitOfWork = orderProductRepository.UnitOfWork;

                userId = globalUserId;
                orderProductsToBeUpdated = orderProductsToBeUpdated.OrderBy(q => q.IndigoPatientID).ThenBy(q => q.FrontEndOrderID).ToList();

                var orderIdsToUpdate = orderProductsToBeUpdated.Select(q => q.FrontEndOrderID).ToList();
                var orderProductDbEntries = orderProductRepository.GetOrderProductsByOrderIds(orderIdsToUpdate).ToList();
                IDXOrderProduct dbProduct = null;
                foreach (var productToUpdate in orderProductsToBeUpdated)
                {
                    //put try catch
                    if (ProductUpdateNonZeroValidation(productToUpdate))
                    {
                        #region Check If OrderId Exists
                        orderFound = orderProductDbEntries.Where(q => q.OrderId == productToUpdate.FrontEndOrderID).Count() > 0 ? true : false;
                        #endregion

                        #region check If Products Exist against the order
                        dbProduct = orderProductDbEntries.Where(w => w.OrderId == productToUpdate.FrontEndOrderID && w.Product.BaseMaterial == productToUpdate.BaseMaterial).FirstOrDefault();
                        productFound = dbProduct != null ? true : false;
                        #endregion

                        #region Check If Order Delivery Dates for each product in same Order Are InSync i.e. Same
                        orderDeliveryDateInSync = orderProductsToBeUpdated.Where(q => q.FrontEndOrderID == productToUpdate.FrontEndOrderID).GroupBy(w => w.DeliveryDate).Count() == 1 ? true : false;
                        #endregion

                        if (!orderFound)
                        {
                            productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            productToUpdate.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "Order with OrderId {0} Not Found", productToUpdate.FrontEndOrderID);
                            continue;
                        }
                        else if (!productFound)
                        {
                            productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            productToUpdate.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "Product id {0} Not Found", productToUpdate.NewProductBaseMaterial);
                            continue;
                        }
                        else if (!orderDeliveryDateInSync)
                        {
                            productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                            productToUpdate.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "Order Delivery Date for all products in OrderId {0} should be same", productToUpdate.FrontEndOrderID);
                            continue;
                        }
                        else
                        {
                            if (UpdateOrderProduct(productToUpdate, dbProduct, orderProductRepository))
                            {
                                #region Set Ordetr Status to Cancelled if all products in current Order are Removed
                                var count = orderProductsToBeUpdated.Where(q => q.FrontEndOrderID == dbProduct.OrderId && q.Quantity > 0).Count();
                                if (count == 0)
                                {
                                    dbProduct.Order.OrderStatus = Convert.ToInt64(SCAEnums.OrderStatus.Cancelled, CultureInfo.InvariantCulture);
                                }
                                #endregion

                                orderProductRepository.InsertOrUpdate(dbProduct);
                                validCount++;
                                ///dbProduct.Order.Patient.ModifiedDate = currentTimeStamp;//If Orders are to go into AuditLog
                            }

                        }
                    }
                }

                if (validCount > 0)
                {
                    orderProductUnitOfWork.Commit();
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (orderProductRepository != null)
                {
                    orderProductRepository.Dispose();
                }

                if (orderProductsToBeUpdated != null && orderProductsToBeUpdated.Any())
                {
                    removeColumn = new List<string>() { "UserId", "SelectedTemplate", "FilePath", "IsImport", "lngOrderType", "UILogIPAddress" };
                    filePath = CommonService.ExportToExcel(orderProductsToBeUpdated, SCAEnums.MassDataTemplate.Order.ToString(CultureInfo.InvariantCulture),
                                        fileName, removeColumn, this.massChangesLogs, true);

                }

            }
            return filePath;
        }

        /// <summary>
        /// Update Order Product
        /// </summary>
        /// <param name="productToUpdate">Order Product to be updated</param>
        /// <param name="dbProduct">Corresponding Order Product that is mentioned in previous parameter</param>
        /// <param name="orderProductRepository">Order product Repository object</param>
        /// <returns> <c>True</c> if Update is valid</returns>
        protected bool UpdateOrderProduct(MassDataOrderBusinessModel productToUpdate, IDXOrderProduct dbProduct, IIDXOrderProductRepository orderProductRepository)
        {
            bool productUpdated = false;
            bool productIdChanged = false;
            bool isNewProductValid = false;                       
            EFModel.Product newProduct = null;
            IProductRepository productRepository = null;
            ICustomerRepository customerRepository = null;
            IDXOrderProduct newOrderProductEntity = null;
			try
			{
				bool isProductDuplicated = false;

				if (productToUpdate == null)
				{
					return false;
				}

				if (dbProduct == null)
				{
					return false;
				}

				if (orderProductRepository == null)
				{
					return false;
				}

				if (!string.IsNullOrEmpty(productToUpdate.NewProductBaseMaterial) && (productToUpdate.BaseMaterial != productToUpdate.NewProductBaseMaterial))
				{
					#region Check If the NewProduct is Valid
					productIdChanged = true;
					productRepository = new ProductRepository();
					newProduct = productRepository.GetProductByBaseMaterial(productToUpdate.NewProductBaseMaterial);
					if (newProduct != null)
					{
						customerRepository = new CustomerRepository();
                        var customer = customerRepository.FindBySapId(productToUpdate.SAPCustomerID);
						if (customer == null)
						{
							productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
							productToUpdate.ErrorMessage = "Customer Not Found";
						}
						else
						{
							var customerprouct = customer.IDXCustomerProducts.Where(q => q.Product.BaseMaterial == productToUpdate.NewProductBaseMaterial).FirstOrDefault();
							if (customerprouct == null)
							{
								productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
								productToUpdate.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "New Product {0} Does not belong to the Customer", productToUpdate.NewProductBaseMaterial);
							}
							else
							{
								//if(NewProduct Already exists, set transaction status to failed)
								var existingProductsInOrder = orderProductRepository.GetOrderProductsByOrderId(productToUpdate.FrontEndOrderID).ToList();
								isProductDuplicated = existingProductsInOrder.Where(q => q.Product.BaseMaterial == productToUpdate.NewProductBaseMaterial).Count() != 0 ? true : false;
								if (isProductDuplicated)
								{
									productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
									productToUpdate.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "New Product {0} already exists in the current order", productToUpdate.NewProductBaseMaterial);
									isNewProductValid = false;
								}
								else
								{
									isNewProductValid = true;
								}
							}
						}
					}
					else
					{
						productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
						productToUpdate.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "Newly Entered Product {0} Not Found", productToUpdate.NewProductBaseMaterial);
					}
					#endregion
				}

				if (productIdChanged && isNewProductValid)
				{
					//Add new Product to repository and Remove Old Product [Set IsRemoved = False]
					dbProduct.IsRemoved = true;
					newOrderProductEntity = new IDXOrderProduct();
					newOrderProductEntity.OrderId = productToUpdate.FrontEndOrderID;
					newOrderProductEntity.ProductId = newProduct.ProductId;
					newOrderProductEntity.Quantity = productToUpdate.Quantity;
					newOrderProductEntity.IsFOC = false;
					newOrderProductEntity.ModifiedBy = Guid.Parse(globalUserId);
					newOrderProductEntity.ModifiedDate = currentDate;
					newOrderProductEntity.IsRemoved = false;
					orderProductRepository.InsertOrUpdate(newOrderProductEntity);
					productUpdated = true;
				}
				else if (!isProductDuplicated)
				{
					//Update Existing Order Product

					//Activate the current Order if Quantity of any product is made non-zero and whose OrderStatus is currently Cancelled
					if (dbProduct.Quantity == 0 && productToUpdate.Quantity > 0 && dbProduct.Order.OrderStatus == Convert.ToInt64(SCAEnums.OrderStatus.Cancelled, CultureInfo.InvariantCulture))
					{
						dbProduct.Order.OrderStatus = Convert.ToInt64(SCAEnums.OrderStatus.Activated, CultureInfo.InvariantCulture);
					}
					dbProduct.Quantity = productToUpdate.Quantity;
					dbProduct.IsRemoved = productToUpdate.Quantity == 0 ? true : false;
					DateTime deliveryDate = new DateTime();
					CommonHelper.ValidateDate(productToUpdate.DeliveryDate, out deliveryDate);
					dbProduct.Order.SAPOrderDeliveryDate = deliveryDate;
					dbProduct.ModifiedDate = currentDate;
					dbProduct.ModifiedBy = Guid.Parse(globalUserId);
					productUpdated = true;
				}
				else
				{
					productUpdated = false;
				}

				return productUpdated;
			}
			finally
			{
				if(customerRepository != null)
				{
					customerRepository.Dispose();
				}

				if(productRepository != null)
				{
					productRepository.Dispose();
				}
			}
        }

        /// <summary>
        /// Validate the updated values against Zero's or Null values
        /// </summary>
        /// <param name="productToUpdate">Product to be updated</param>
        /// <returns><c>True</c> if There are no NULL or Empty values in required fields</returns>
        private bool ProductUpdateNonZeroValidation(MassDataOrderBusinessModel productToUpdate)
        {
            bool valid = false;                      
            DateTime delDate;

            #region CustomerId Validation
            if (string.IsNullOrEmpty(productToUpdate.SAPCustomerID))
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "Customer ID is Empty";
            }                
            #endregion

            #region PatientID Validation
            else if (CommonHelper.IsNullOrEmpty(productToUpdate.IndigoPatientID))
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "Patient ID is Zero OR Empty";
            }
            #endregion

            #region ProductId Validation
            else if (string.IsNullOrEmpty(productToUpdate.BaseMaterial))
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "ProductId is Zero OR Empty";
            }
            #endregion

            #region OrderID Validation
            else if (CommonHelper.IsNullOrEmpty(productToUpdate.FrontEndOrderID))
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "Order ID was Zero OR Empty";
            }
            #endregion

            #region NextDelivery Date Validation
            else if (string.IsNullOrEmpty(productToUpdate.DeliveryDate))
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "Delivery Date is Empty";                
            }
            else if ((!CommonHelper.ValidateDate(productToUpdate.DeliveryDate, out delDate)))
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "Invalid Date Format in Delivary Date";                
            }
            #endregion

            #region Quantity Validation
            else if (productToUpdate.Quantity == null)
            {
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = "Quantity is Empty";
            }
            #endregion

            else
            {
                valid = true;
                productToUpdate.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
                productToUpdate.ErrorMessage = string.Empty;
            }
            return valid;
        }
        #endregion


        #region Prescription Mass Updates
		/// <summary>
		/// Process the prescription file upload for Mass Change
		/// </summary>
		/// <param name="excelFactory">Excel Factory with the file attached</param>
		/// <param name="isImport">checks isImport or Update</param>
		/// <returns></returns>
        private string ProcessPrescriptionUpdate(ExcelQueryFactory excelFactory, bool isImport)
        {
            string logFilePath = string.Empty;
            if (excelFactory != null)
            {             
                var prescriptionBusinessModelData = massDataImport.GetNonblankRows<MassDataPrescriptionBusinessModel>(excelFactory, massUploadMaxLimit);
                if (isImport)
                {
                    fileName = CommonConstants.PrescriptionImportTemplateFilePath;
                }
                else
                {
                    fileName = CommonConstants.PrescriptionUpdateTemplateFilePath;
                }

                var result = MassUpdatePrescription(prescriptionBusinessModelData);
                logFilePath = result;
            }

            return logFilePath;
        }

        public string ProcessMassUpdate(MassDataBusinessModel massChangesBusinessModel)
        {            
            string logFilePath = string.Empty;
			ExcelQueryFactory excelFactory = null;
            try
            {
                massDataImport = new MassDataImport();
                if (massChangesBusinessModel != null)
                {
                    globalUserId = massChangesBusinessModel.UserId;
                    isImport = massChangesBusinessModel.IsImport;
                    currentDate = DateTime.Now;
                    string templateName = string.Empty;
                    excelFactory = new ExcelQueryFactory(massChangesBusinessModel.FilePath);
                    CommonService.UILogIPAddress = massChangesBusinessModel.UILogIPAddress;
                    switch (massChangesBusinessModel.SelectedTemplate)
                    {
                        case (int)SCAEnums.MassDataTemplate.Patient:
                            //UpdatePatientData(excelFacroty);
                            break;
                        case (int)SCAEnums.MassDataTemplate.Carehome:
                            //UpdateCarehome(excelFacroty);
                            break;
                        case (int)SCAEnums.MassDataTemplate.Prescription:
                            logFilePath = this.ProcessPrescriptionUpdate(excelFactory, isImport);
                            templateName = SCAEnums.MassDataTemplate.Prescription.ToString();
                            break;
                        case (int)SCAEnums.MassDataTemplate.Order:
                            logFilePath = this.ProcessOrderUpdate(excelFactory);
                            templateName = SCAEnums.MassDataTemplate.Order.ToString();
                            break;
                        case (int)SCAEnums.MassDataTemplate.PostCodeMatrix:
                            logFilePath = massDataImport.ImportPostcodeMatrix(excelFactory);
                            templateName = SCAEnums.MassDataTemplate.PostCodeMatrix.ToString();
                            break;
						default:
							// do the default action
							break;
					}                    

                    //Create entry in MassDataLog table for uploaded template
                    massDataImport.CreateMassDataLog(templateName, isImport, logFilePath, currentDate);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, globalUserId);
                throw;
            }
			finally
			{
				if(excelFactory != null)
				{
					excelFactory.Dispose();
				}
			}
            return logFilePath;
        }

        /// <summary>
        /// MassUpdatePrescription
        /// </summary>
        /// <param name="prescriptions"></param>
        /// <returns></returns>
        protected string MassUpdatePrescription(List<MassDataPrescriptionBusinessModel> prescriptions)
        {
            IPatientRepository patientRepository = null;
            IProductRepository productRepository = null;            
            IIDXCustomerProductRepository customerProductRepository = null;            
            string logFilePath = string.Empty;
            IPatientTypeMatrixRepository patientTypeMatrixRepository = null;
			try
			{
				if (prescriptions == null)
				{
					return null;
				}
				patientRepository = new PatientRepository();
				productRepository = new ProductRepository();				
				customerProductRepository = new IDXCustomerProductRepository();
				patientTypeMatrixRepository = new PatientTypeMatrixRepository();

				var patientIdsString = prescriptions.Select(q => q.IndigoPortalPatientID).ToList();
				List<long> patientIds = new List<long>();
				long id = 0;
				patientIdsString.ForEach(q =>
				{
					if (long.TryParse(q, out id))
					{
						patientIds.Add(id);
					}
				});
				var patients = patientRepository.GetPatient(patientIds).ToList();

				var productIds = prescriptions.Where(q => !string.IsNullOrEmpty(q.NewProductBaseMaterial)).Select(s => s.NewProductBaseMaterial.Trim()).ToList();
				var oldProductIds = prescriptions.Select(q => !string.IsNullOrEmpty(q.BaseMaterial) ? q.BaseMaterial.Trim() : "").ToList();
				productIds.AddRange(oldProductIds);

				var products = productRepository.GetProductsByBaseMaterials(productIds).ToList();
				var customerProducts = customerProductRepository.GetCustomerProductsByProductId(products.Select(q => q.ProductId).ToList()).ToList();
				var customerProductsCollection = new Collection<EFModel.IDXCustomerProduct>(customerProducts);

				var patientTypeMatrixList = patientTypeMatrixRepository.GetPatientTypeMatrixByCustomerId(patients.Select(p => (long)p.CustomerId).ToList()).ToList();
				var validCount = 0;

				///validate for NDD of all prescription product
				ValidateForNewPrescriptionNdd(patients, prescriptions);

				var FirstCustomer = string.Empty;
				foreach (var item in prescriptions)
				{
					if(!string.IsNullOrEmpty(FirstCustomer) && FirstCustomer != item.SAPCustomerID)
					{
						item.ErrorMessage = "Only one Customers Patient Prescriptions can be updated at a time.";
						item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString();
						continue;
					}
					if (string.IsNullOrEmpty(item.TransactionStatus) && UpdatePrescription(item, patients, products, customerProductsCollection, patientTypeMatrixList, patientRepository))
					{
						FirstCustomer = item.SAPCustomerID;
						validCount++;
					}
				}
				if (validCount > 0)
				{
					IUnitOfWork unitOfWork = patientRepository.UnitOfWork;
					unitOfWork.Commit();
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, globalUserId);
				throw;
			}
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
                if (productRepository != null)
                {
                    productRepository.Dispose();
                }
                if (customerProductRepository != null)
                {
                    customerProductRepository.Dispose();
                }               
                if (patientTypeMatrixRepository != null)
                {
                    patientTypeMatrixRepository.Dispose();
                }

                if (prescriptions.Any())
                {
                    removeColumn = new List<string>() { "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
					if(isImport)
					{
						removeColumn.Add("NewSAPProductID");
						removeColumn.Add("NewProductName");
						removeColumn.Add("NewProductBaseMaterial");
					}

					logFilePath = CommonService.ExportToExcel(prescriptions, SCAEnums.MassDataTemplate.Prescription.ToString(CultureInfo.InvariantCulture),
									fileName, removeColumn, this.massChangesLogs, true);
                }
            }

            return logFilePath;
        }

        /// <summary>
        /// Validation for new prescription NDD
        /// </summary>
        /// <param name="patients"></param>
        /// <param name="prescriptions"></param>
		private void ValidateForNewPrescriptionNdd(List<EFModel.Patient> patients, List<MassDataPrescriptionBusinessModel> prescriptions)
		{
			foreach (var patient in patients)
			{
                var prescriptionItems = prescriptions.Where(q => q.IndigoPortalPatientID.Trim() == Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture) && string.IsNullOrEmpty(q.NewSAPProductID)).ToList();
				if (patient.DeliveryDate == null && patient.CareHome == null)
				{					
					if (prescriptionItems.Any())
					{
						if (patient.NextDeliveryDate != null)
						{
							foreach (var item in prescriptionItems)
							{
								DateTime nextDeliveryDate;
								if(CommonHelper.ValidateDate(item.NextDeliveryDate, out nextDeliveryDate) && nextDeliveryDate != patient.NextDeliveryDate)
								{								
									item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
									item.ErrorMessage = "Prescription NDD date should be same as Patient NDD for new Patient";
								}
							}
						}
						else
						{
							var NDDs = prescriptionItems.GroupBy(q => q.NextDeliveryDate).ToList();
							if (NDDs.Count > 1)
							{
								prescriptionItems.ForEach(q =>
									{
										q.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                                        q.ErrorMessage = string.Format(CultureInfo.InvariantCulture, "Prescription NDD date should be same for all the items of Patient -{0}({1})", patient.SAPPatientNumber, Convert.ToString(patient.PatientId, CultureInfo.InvariantCulture));
									});
							}
						}
					}
				}
				else if (patient.CareHome != null && patient.DeliveryDate == null)
				{
					foreach (var item in prescriptionItems)
					{
						DateTime nextDeliveryDate;
						if (CommonHelper.ValidateDate(item.NextDeliveryDate, out nextDeliveryDate) && nextDeliveryDate != patient.CareHome.NextDeliveryDate)
						{
							item.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
							item.ErrorMessage = "Prescription NDD date should be same as Patient Carehome NDD";
						}
					}

				}
			}
		}

        /// <summary>
        /// Validates the Prescription Object - Format and Mandatory data validation
        /// </summary>
        /// <param name="prescription"></param>
        /// <returns></returns>
        private bool IsValidPrescription(MassDataPrescriptionBusinessModel prescription, PatientTypeMatrix patientTypeMatrix)
        {
            if (prescription == null)
            {
                return false;
            }

            massDataImport.MandatoryFields = new StringBuilder();
            massDataImport.InvalidDataType = new StringBuilder();
            massDataImport.DatabaseValidation = new StringBuilder();
            massDataImport.CustomValidation = new StringBuilder();
			massDataImport.InvalidLength = new StringBuilder();

            bool isValid = false;          
            massDataImport.ValidateColumn(prescription.BaseMaterial, "BaseMaterial", true, SCAEnums.DataType.String, 40);
			massDataImport.ValidateColumn(prescription.SAPCustomerID, "SAPCustomerID", true, SCAEnums.DataType.String);
			massDataImport.ValidateColumn(prescription.IndigoPortalCustomerID, "IndigoPortalCustomerID", false, SCAEnums.DataType.Int);
            if (isImport || string.IsNullOrEmpty(prescription.NewProductBaseMaterial))
            {
                massDataImport.ValidateColumn(prescription.Frequency, "Frequency", true, SCAEnums.DataType.Long);
                massDataImport.ValidateColumn(prescription.NextDeliveryDate, "NextDeliveryDate", true, SCAEnums.DataType.Date);
                massDataImport.ValidateColumn(prescription.DeliveryDate, "DeliveryDate", false, SCAEnums.DataType.Date);
                massDataImport.ValidateColumn(prescription.ValidFromDate, "ValidFromDate", true, SCAEnums.DataType.Date);
                massDataImport.ValidateColumn(prescription.ValidToDate, "ValidToDate", true, SCAEnums.DataType.Date);
                massDataImport.ValidateColumn(prescription.Status, "Status", true, SCAEnums.DataType.String, 10);
                massDataImport.ValidateColumn(prescription.AssessedPadsPerDay, "AssessedPadsPerDay", true, SCAEnums.DataType.Decimal);
                massDataImport.ValidateColumn(prescription.ActualPPD, "ActualPPD", true, SCAEnums.DataType.Decimal);
                massDataImport.ValidateColumn(prescription.DQ1, "DQ1", true, SCAEnums.DataType.Long);
                massDataImport.ValidateColumn(prescription.DQ2, "DQ2", true, SCAEnums.DataType.Long);
                massDataImport.ValidateColumn(prescription.DQ3, "DQ3", true, SCAEnums.DataType.Long);
                massDataImport.ValidateColumn(prescription.DQ4, "DQ4", true, SCAEnums.DataType.Long);
                massDataImport.ValidateColumn(prescription.FlagDQ1, "FlagDQ1", true, SCAEnums.DataType.Bool);
                massDataImport.ValidateColumn(prescription.FlagDQ2, "FlagDQ2", true, SCAEnums.DataType.Bool);
                massDataImport.ValidateColumn(prescription.FlagDQ3, "FlagDQ3", true, SCAEnums.DataType.Bool);
                massDataImport.ValidateColumn(prescription.FlagDQ4, "FlagDQ4", true, SCAEnums.DataType.Bool);
                massDataImport.ValidateColumn(prescription.IsOverridden, "IsOverridden", true, SCAEnums.DataType.Bool);
                prescription.ErrorMessage = massDataImport.GetErrorMessage();
                prescription.TransactionStatus = massDataImport.RowStatus;
                if (string.IsNullOrEmpty(prescription.ErrorMessage))
                {
                    DateTime validFromDate, validToDate, nextDeliveryDate, deliverydate;
                    var frequency = Convert.ToInt64(prescription.Frequency, CultureInfo.InvariantCulture);
                    if (patientTypeMatrix != null && !(frequency >= patientTypeMatrix.Presc_Min_Freq && frequency <= patientTypeMatrix.Presc_Max_Freq))
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = "Frequency is not within the valid Range";
                    }
                    else if (patientTypeMatrix != null && frequency % patientTypeMatrix.IncrementFrequency > 0)
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = "Frequency should be multiple of " + patientTypeMatrix.IncrementFrequency.ToString();
                    }
					else if (Convert.ToDecimal(prescription.ActualPPD, CultureInfo.InvariantCulture) < Convert.ToDecimal(prescription.AssessedPadsPerDay, CultureInfo.InvariantCulture))
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = "ActualPPD should be greater or equal to AssessedPadsPerDay";
                    }
					else if ((!string.IsNullOrEmpty(prescription.NextDeliveryDate) && CommonHelper.ValidateDate(prescription.NextDeliveryDate, out nextDeliveryDate)) &&
						(!string.IsNullOrEmpty(prescription.DeliveryDate) && CommonHelper.ValidateDate(prescription.DeliveryDate, out deliverydate)) &&
						(nextDeliveryDate <= deliverydate))
					{

						prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
						prescription.ErrorMessage = "Next Delivery Date cannot be less than delivery Date";

					}
					else if ((!string.IsNullOrEmpty(prescription.ValidFromDate) && CommonHelper.ValidateDate(prescription.ValidFromDate, out validFromDate)) &&
							(!string.IsNullOrEmpty(prescription.ValidToDate) && CommonHelper.ValidateDate(prescription.ValidToDate, out validToDate)) &&
							(validFromDate > validToDate)
						)
					{

						prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
						prescription.ErrorMessage = "Valid To Date cannot be less than Valid From Date";

					}
					else if (
						(!string.IsNullOrEmpty(prescription.FlagDQ1) ? prescription.FlagDQ1.ToUpper() : "N") == "N" &&
						(!string.IsNullOrEmpty(prescription.FlagDQ2) ? prescription.FlagDQ2.ToUpper() : "N") == "N" &&
						(!string.IsNullOrEmpty(prescription.FlagDQ3) ? prescription.FlagDQ3.ToUpper() : "N") == "N" &&
						(!string.IsNullOrEmpty(prescription.FlagDQ4) ? prescription.FlagDQ4.ToUpper() : "N") == "N"
						)
					{
						prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
						prescription.ErrorMessage = "All DQ Flags cannot be empty to update the existing product / import the new product to the Prescription.";
					}
					else
					{
						var flagTrueCount = 0;
						if ((!string.IsNullOrEmpty(prescription.FlagDQ1) ? prescription.FlagDQ1.ToUpper() : "N") == "Y")
						{
							flagTrueCount++;
						}
						if ((!string.IsNullOrEmpty(prescription.FlagDQ2) ? prescription.FlagDQ2.ToUpper() : "N") == "Y")
						{
							flagTrueCount++;
						}
						if ((!string.IsNullOrEmpty(prescription.FlagDQ3) ? prescription.FlagDQ3.ToUpper() : "N") == "Y")
						{
							flagTrueCount++;
						}
						if ((!string.IsNullOrEmpty(prescription.FlagDQ4) ? prescription.FlagDQ4.ToUpper() : "N") == "Y")
						{
							flagTrueCount++;
						}

						if (flagTrueCount == 0)
						{
							prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
							prescription.ErrorMessage = "Any one DQ Flag should be set to true, to update the existing product / import the new product to the Prescription.";
						}
						else if (flagTrueCount > 1)
						{
							prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
							prescription.ErrorMessage = "Any one DQ Flag can be set to True at a time, to update the existing product / import the new product to the Prescription.";
						}
						else
						{
							isValid = true;
						}
					}
                }
            }
            else
            {
                prescription.ErrorMessage = massDataImport.GetErrorMessage();
                prescription.TransactionStatus = massDataImport.RowStatus;
                if (string.IsNullOrEmpty(prescription.ErrorMessage) && prescription.NewProductBaseMaterial == prescription.BaseMaterial)
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = CommonConstants.MassChangesSAPProductIDSame;
                }
                else if (string.IsNullOrEmpty(prescription.ErrorMessage))
                {
                    isValid = true;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Update the prescription according the business logic
        /// </summary>
        /// <param name="prescription"></param>
        /// <param name="patients"></param>
        /// <param name="products"></param>
        /// <param name="customerProductsCollection"></param>
        /// <param name="patientRepository"></param>
        /// <returns></returns>
        protected bool UpdatePrescription(MassDataPrescriptionBusinessModel prescription, List<EFModel.Patient> patients, List<EFModel.Product> products, Collection<EFModel.IDXCustomerProduct> customerProductsCollection, List<PatientTypeMatrix> patientTypeMatrixList, IPatientRepository patientRepository)
        {
            try
            {
                EFModel.Patient patient = null;
                if (patients.Where(q => Convert.ToString(q.PatientId, CultureInfo.InvariantCulture) == prescription.IndigoPortalPatientID).Any())
                {
                    patient = patients.Where(q => Convert.ToString(q.PatientId, CultureInfo.InvariantCulture) == prescription.IndigoPortalPatientID).First();
					if(patient.PatientStatus == (long)SCAEnums.PatientStatus.Removed)
					{
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
						prescription.ErrorMessage = "Cannot update the Prescription of Removed Patient.";
						return false;
					}
                }

                PatientTypeMatrix patientTypeMatrix = null;
                if (patient != null && patientTypeMatrixList.Any() && patientTypeMatrixList.Where(q => q.PatientType == patient.PatientType).Any())
                {
                    patientTypeMatrix = patientTypeMatrixList.Where(q => q.PatientType == patient.PatientType).First();
                }

                var customerProducts = customerProductsCollection.ToList();
                var modifiedBy = Guid.Parse(globalUserId);

                #region Database Entity Validation
                bool isValid = true;
                if (patient == null)
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = "Invalid Patient Id";
                    isValid = false;
                }
                else if (!string.IsNullOrEmpty(prescription.NewProductBaseMaterial) && !products.Where(q => q.BaseMaterial.Trim() == prescription.NewProductBaseMaterial.Trim()).Any())
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = CommonConstants.MassChangesNewProductNotFound;
                    isValid = false;
                }
                else if (!products.Where(q => q.BaseMaterial.Trim() == (!string.IsNullOrEmpty(prescription.BaseMaterial) ? prescription.BaseMaterial.Trim() : "")).Any())
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = CommonConstants.MassChangesOldProductNotFound;
                    isValid = false;
                }
                else if (!IsValidPrescription(prescription, patientTypeMatrix))
                {
                    isValid = false;
                }
                else if (patient.Customer.SAPCustomerNumber.ToString() != prescription.SAPCustomerID)
                    //Check SAP ID
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = "Patient does not belong to the Customer - " + prescription.SAPCustomerID;
                    isValid = false;
                }

                //check for product duplication
                if (!isValid)
                {
                    return false;
                }
                #endregion
                EFModel.Product newProduct = null;
                if (!string.IsNullOrEmpty(prescription.NewProductBaseMaterial))
                {
                    newProduct = products.Where(q => q.BaseMaterial.Trim() == prescription.NewProductBaseMaterial.Trim()).First();
                }

                if (newProduct != null && newProduct.ProductId != 0 && !customerProducts.Where(q => q.CustomerId == patient.CustomerId && q.ProductId == newProduct.ProductId).Any())
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = CommonConstants.MassChangesProductNotMappedToCustomer;
                    return false;
                }

                var patientPrescription = patient.Prescriptions.First();
                //Duplicate entry checking for new product
                if (newProduct != null && patientPrescription.IDXPrescriptionProducts.Where(q => q.ProductId == newProduct.ProductId && q.IsRemoved != true).Any())
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = CommonConstants.MassChangesDuplicateNewProduct;
                    return false;
                }

                var oldProduct = products.Where(q => q.BaseMaterial.Trim() == prescription.BaseMaterial.Trim()).First();

                List<IDXPrescriptionProduct> oldPrescriptionProducts;
                IDXPrescriptionProduct productToUpdate = null;
                if (patientPrescription.IDXPrescriptionProducts != null && patientPrescription.IDXPrescriptionProducts.Any())
                {
                    oldPrescriptionProducts = patientPrescription.IDXPrescriptionProducts.Where(q => q.ProductId == oldProduct.ProductId).ToList();
                    if (!oldPrescriptionProducts.Any() && !isImport)
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = CommonConstants.MassChangesNoMatchingPrescriptionProduct;
                        return false;
                    }
                    else if (isImport && oldPrescriptionProducts.Where(q => q.IsRemoved != true).Any())
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = "Duplicate entry - Product line item is already found in the Prescription";
                        return false;
                    }
                    if (oldPrescriptionProducts.Any())
                    {
                        productToUpdate = oldPrescriptionProducts.First();
                    }                   
                }

				if (isImport && (productToUpdate == null || (productToUpdate != null && productToUpdate.IsRemoved != true)))
				{
					productToUpdate = new IDXPrescriptionProduct();
				}


                var DQ1 = Convert.ToInt64(productToUpdate.Del1, CultureInfo.InvariantCulture);
                var DQ2 = Convert.ToInt64(productToUpdate.Del2, CultureInfo.InvariantCulture);
                var DQ3 = Convert.ToInt64(productToUpdate.Del3, CultureInfo.InvariantCulture);
                var DQ4 = Convert.ToInt64(productToUpdate.Del4, CultureInfo.InvariantCulture);
                var assessedPPD = Convert.ToInt64(productToUpdate.AssessedPadsPerDay, CultureInfo.InvariantCulture);
                var actPPD = Convert.ToDouble(productToUpdate.ActPPD, CultureInfo.InvariantCulture);
                var frequency = Convert.ToInt64(productToUpdate.Frequency, CultureInfo.InvariantCulture);
                var flagDQ1 = productToUpdate.FlagDQ1.HasValue ? productToUpdate.FlagDQ1.Value : false;
                var flagDQ2 = productToUpdate.FlagDQ2.HasValue ? productToUpdate.FlagDQ2.Value : false;
                var flagDQ3 = productToUpdate.FlagDQ3.HasValue ? productToUpdate.FlagDQ3.Value : false;
                var flagDQ4 = productToUpdate.FlagDQ4.HasValue ? productToUpdate.FlagDQ4.Value : false;

                var isCountChanged = prescription.CountChanged.ToUpper() == "Y" ? true : false;
                var activeOrders = patient.Orders.Where(q => q.OrderStatus == (long)SCAEnums.OrderStatus.Activated
                                                    && q.OrderType == (long)SCAEnums.OrderType.ZHDP
                                                    && q.IsRushFlag != true
                                                ).ToList();
                if (isImport || string.IsNullOrEmpty(prescription.NewProductBaseMaterial))
                {
                    DQ1 = Convert.ToInt64(prescription.DQ1, CultureInfo.InvariantCulture);
                    DQ2 = Convert.ToInt64(prescription.DQ2, CultureInfo.InvariantCulture);
                    DQ3 = Convert.ToInt64(prescription.DQ3, CultureInfo.InvariantCulture);
                    DQ4 = Convert.ToInt64(prescription.DQ4, CultureInfo.InvariantCulture);
                    assessedPPD = Convert.ToInt64(prescription.AssessedPadsPerDay, CultureInfo.InvariantCulture);
                    actPPD = Convert.ToDouble(prescription.ActualPPD, CultureInfo.InvariantCulture);
                    frequency = Convert.ToInt64(prescription.Frequency, CultureInfo.InvariantCulture);
                    flagDQ1 = (!string.IsNullOrEmpty(prescription.FlagDQ1) && prescription.FlagDQ1.ToUpper() == "Y") ? true : false;
                    flagDQ2 = (!string.IsNullOrEmpty(prescription.FlagDQ2) && prescription.FlagDQ2.ToUpper() == "Y") ? true : false;
                    flagDQ3 = (!string.IsNullOrEmpty(prescription.FlagDQ3) && prescription.FlagDQ3.ToUpper() == "Y") ? true : false;
                    flagDQ4 = (!string.IsNullOrEmpty(prescription.FlagDQ4) && prescription.FlagDQ4.ToUpper() == "Y") ? true : false;
                }


                if (isCountChanged && activeOrders.Any())
                {
                    prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                    prescription.ErrorMessage = CommonConstants.MassChangesHasActiveOrder;
                    return false;
                }
                else if (isCountChanged && !activeOrders.Any())
                {
                    // recaulcualte DQ1-DQ4
                    var totalDays = 4 * frequency * 7;
                    double packSize = 0;
                    if (isImport || string.IsNullOrEmpty(prescription.NewProductBaseMaterial))
                    {
                        packSize = !string.IsNullOrEmpty(oldProduct.PackSize) ? Convert.ToDouble(oldProduct.PackSize, CultureInfo.InvariantCulture) : 1;
                    }
                    else
                    {
                        packSize = !string.IsNullOrEmpty(newProduct.PackSize) ? Convert.ToDouble(newProduct.PackSize, CultureInfo.InvariantCulture) : 1;
                    }
                    var totalQuantity = Convert.ToDouble(totalDays * (assessedPPD == 0 ? 1 : assessedPPD), CultureInfo.InvariantCulture) / packSize;
                    var DQ1ToDQ4 = Math.Ceiling(totalQuantity) / 4;
                    DQ1 = Convert.ToInt64(Math.Ceiling(DQ1ToDQ4), CultureInfo.InvariantCulture);
                    DQ2 = Convert.ToInt64(Math.Floor(DQ1ToDQ4), CultureInfo.InvariantCulture);
                    DQ3 = Convert.ToInt64(Math.Floor(DQ1ToDQ4), CultureInfo.InvariantCulture);
                    DQ4 = Convert.ToInt64(Math.Floor(DQ1ToDQ4), CultureInfo.InvariantCulture);

                    double calculatedActPPD = 0;
                    while (calculatedActPPD < assessedPPD)
                    {
                        var totalDQ = DQ1 + DQ2 + DQ3 + DQ4;
                        var total = totalDQ * packSize;
                        calculatedActPPD = total / totalDays;
                        if (calculatedActPPD < assessedPPD)
                        {
                            DQ2 = DQ2 + 1;
                        }
                    };
                    actPPD = calculatedActPPD;
                    var DQTotal = DQ1 + DQ2 + DQ3 + DQ4;
                    if (DQTotal != 0)
                    {
                        double newDQ = DQTotal / 4;
                        DQ1 = Convert.ToInt64(Math.Floor(newDQ), CultureInfo.InvariantCulture);
                        var reminder = DQTotal - (DQ1 * 4);
                        if (reminder > 0)
                        {
                            DQ1 = DQ1 + 1;
                            reminder = reminder - 1;
                            if (reminder > 0)
                            {
                                DQ2 = DQ2 + 1;
                                reminder = reminder - 1;
                                if (reminder > 0)
                                {
                                    DQ3 = DQ3 + 1;
                                }
                            }
                        }
                    }
                }
                else if (!isCountChanged && activeOrders.Any())
                {
                    //update all pending orders and prescription
                    foreach (var order in activeOrders)
                    {
                        var orderProducts = order.IDXOrderProducts;
                        if (orderProducts.Where(q => q.ProductId == oldProduct.ProductId && q.IsRemoved != true).Any())
                        {
                            var productToRemove = orderProducts.Where(q => q.ProductId == oldProduct.ProductId).First();
                            var quantity = productToRemove.Quantity;
                            if ((isImport || string.IsNullOrEmpty(prescription.NewProductBaseMaterial)) && productToUpdate.IsRemoved == true)
                            {
                                productToRemove.IsRemoved = true;
                                productToRemove.Quantity = 0;
                                productToRemove.ModifiedBy = modifiedBy;
                                productToRemove.ModifiedDate = currentDate;
                            }
                            else if (!isImport && !string.IsNullOrEmpty(prescription.NewProductBaseMaterial))
                            {

                                productToRemove.IsRemoved = true;
                                productToRemove.Quantity = 0;
                                productToRemove.ModifiedBy = modifiedBy;
                                productToRemove.ModifiedDate = currentDate;

                                var newOrderProduct = new IDXOrderProduct();
                                newOrderProduct.IsRemoved = false;
                                newOrderProduct.ProductId = newProduct.ProductId;
                                newOrderProduct.Quantity = quantity;
                                newOrderProduct.IsFOC = false;
                                newOrderProduct.ModifiedDate = currentDate;
                                newOrderProduct.ModifiedBy = modifiedBy;
                                order.IDXOrderProducts.Add(newOrderProduct);
                            }
                        }
                    }
                }

                if (isImport || string.IsNullOrEmpty(prescription.NewProductBaseMaterial))
                {
                    DateTime validFromDate;
                    if (!CommonHelper.ValidateDate(prescription.ValidFromDate, out validFromDate))
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = CommonConstants.MassChangesInvalidFromDate;
                        return false;
                    }

                    DateTime validToDate;
					if (!CommonHelper.ValidateDate(prescription.ValidToDate, out validToDate))
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = CommonConstants.MassChangesInvalidToDate;
                        return false;
                    }

                    DateTime nextDeliveryDate;
                    if (!CommonHelper.ValidateDate(prescription.NextDeliveryDate, out nextDeliveryDate))
                    {
                        prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                        prescription.ErrorMessage = "Invalid Next Delivery Date for the product";
                        return false;
                    }

					if(!IsValidPrescriptionStatus(prescription.Status))
					{
						prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
						prescription.ErrorMessage = "Invalid Status";
						return false;
					}



                    productToUpdate.ProductId = oldProduct.ProductId;
                    productToUpdate.Description = oldProduct.DescriptionUI;
                    productToUpdate.ValidFromDate = validFromDate;
                    productToUpdate.ValidToDate = validToDate;
                    productToUpdate.NextDeliveryDate = nextDeliveryDate;
                    productToUpdate.Status = prescription.Status;
                    if (prescription.Status == SCAEnums.Status.Active.ToString(CultureInfo.InvariantCulture))
                    {
                        productToUpdate.IsRemoved = false;
                    }
                    else
                    {
                        productToUpdate.IsRemoved = true;
                    }
                    productToUpdate.AssessedPadsPerDay = assessedPPD;
                    productToUpdate.ActPPD = actPPD;
                    productToUpdate.IsOverridenFlag = prescription.IsOverridden.ToUpper() == "Y" ? true : false;
                    productToUpdate.Del1 = DQ1;
                    productToUpdate.Del2 = DQ2;
                    productToUpdate.Del3 = DQ3;
                    productToUpdate.Del4 = DQ4;
                    productToUpdate.FlagDQ1 = flagDQ1;
                    productToUpdate.FlagDQ2 = flagDQ2;
                    productToUpdate.FlagDQ3 = flagDQ3;
                    productToUpdate.FlagDQ4 = flagDQ4;
                    productToUpdate.Frequency = frequency;
                    productToUpdate.ModifiedDate = currentDate;
                    productToUpdate.ModifiedBy = modifiedBy;
                    productToUpdate.Remarks = prescription.Remarks;
                    if (isImport)
                    {
                        patientPrescription.IDXPrescriptionProducts.Add(productToUpdate);
                    }
                    patientPrescription.IsMassChange = true;
                    patient.ModifiedDate = currentDate;
                    patient.ModifiedBy = modifiedBy;

					if(patient.NextDeliveryDate == null)
					{
						patient.NextDeliveryDate = productToUpdate.NextDeliveryDate;
					}
                }
                else
                {

                    productToUpdate.IsRemoved = true;
                    productToUpdate.Status = SCAEnums.Status.Removed.ToString(CultureInfo.InvariantCulture);
                    productToUpdate.ModifiedBy = modifiedBy;
                    productToUpdate.ModifiedDate = currentDate;
                    productToUpdate.ValidToDate = currentDate;
                    productToUpdate.NextDeliveryDate = null;
                    productToUpdate.Remarks = prescription.Remarks;
                    //Add new product to the prescription


                    //If there is a already existing new product in the prescription with removed status on, update the same product line item.
                    IDXPrescriptionProduct newProductToUpdate;
                    if (patientPrescription.IDXPrescriptionProducts.Where(q => q.ProductId == newProduct.ProductId && q.IsRemoved == true).Any())
                    {
                        newProductToUpdate = patientPrescription.IDXPrescriptionProducts.Where(q => q.ProductId == newProduct.ProductId && q.IsRemoved == true).FirstOrDefault();
                    }
                    else
                    {
                        newProductToUpdate = new IDXPrescriptionProduct();
                    }
                    newProductToUpdate.ProductId = newProduct.ProductId;
                    newProductToUpdate.ValidFromDate = DateTime.Today;
                    newProductToUpdate.ValidToDate = defaultEndDate;
                    newProductToUpdate.Frequency = frequency;
                    newProductToUpdate.AssessedPadsPerDay = productToUpdate.AssessedPadsPerDay;
                    newProductToUpdate.FlagDQ1 = true;
                    newProductToUpdate.FlagDQ2 = false;
                    newProductToUpdate.FlagDQ3 = false;
                    newProductToUpdate.FlagDQ4 = false;
                    newProductToUpdate.Del1 = DQ1;
                    newProductToUpdate.Del2 = DQ2;
                    newProductToUpdate.Del3 = DQ3;
                    newProductToUpdate.Del4 = DQ4;
                    newProductToUpdate.AssessedPadsPerDay = assessedPPD;
                    newProductToUpdate.ActPPD = actPPD;
                    newProductToUpdate.IsOverridenFlag = productToUpdate.IsOverridenFlag;
                    newProductToUpdate.NextDeliveryDate = productToUpdate.NextDeliveryDate;
                    newProductToUpdate.ModifiedBy = modifiedBy;
                    newProductToUpdate.ModifiedDate = currentDate;
                    newProductToUpdate.Status = SCAEnums.Status.Active.ToString(CultureInfo.InvariantCulture);
                    newProductToUpdate.IsRemoved = false;
                    newProductToUpdate.Description = newProduct.DescriptionUI;
                    newProductToUpdate.Remarks = prescription.Remarks;
                    patientPrescription.IDXPrescriptionProducts.Add(newProductToUpdate);
                    patientPrescription.IsMassChange = true;
                    patient.ModifiedDate = currentDate;
                    patient.ModifiedBy = modifiedBy;

                    patientRepository.InsertOrUpdate(patient);
                }

                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Success.ToString(CultureInfo.InvariantCulture);
            }
            catch (DataException e)
            {
                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                prescription.ErrorMessage = e.Message; //Set the inner exception message
                LogHelper.LogException(e, globalUserId);
                return false;
            }
            catch (ArgumentException e)
            {
                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                prescription.ErrorMessage = e.Message; //Set the inner exception message
                LogHelper.LogException(e, globalUserId);
                return false;
            }
            catch (FormatException e)
            {
                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                prescription.ErrorMessage = e.Message; //Set the inner exception message
                LogHelper.LogException(e, globalUserId);
                return false;
            }
            catch (InvalidOperationException e)
            {
                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                prescription.ErrorMessage = e.Message; //Set the inner exception message
                LogHelper.LogException(e, globalUserId);
                return false;
            }
            catch (SystemException e)
            {
                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                prescription.ErrorMessage = e.Message; //Set the inner exception message
                LogHelper.LogException(e, globalUserId);
                return false;
            }
            catch (Exception e)
            {
                prescription.TransactionStatus = SCAEnums.MassUploadStatusType.Failed.ToString(CultureInfo.InvariantCulture);
                prescription.ErrorMessage = e.Message; //Set the inner exception message
                LogHelper.LogException(e, globalUserId);
                return false;
            }
            return true;

        }

		/// <summary>
		/// checks whether the prescription status is the valid one . i.e either Active or Removed
		/// </summary>
		/// <param name="status"></param>
		/// <returns></returns>
		private bool IsValidPrescriptionStatus(string status)
		{
			if(string.IsNullOrEmpty(status))
			{
				return false;
			}
			else
			{
                if (status.ToUpper() != SCAEnums.Status.Active.ToString(CultureInfo.InvariantCulture).ToUpper() && status.ToUpper() != SCAEnums.Status.Removed.ToString(CultureInfo.InvariantCulture).ToUpper())
				{
					return false;
				}
			}
			return true;
		}
        #endregion

        /// <summary>
        /// Download Master Mass Data Change For Carehome
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel"></param>
        /// <returns>list of details</returns>
        public MasterMassDataChangeBusinessModel DownloadMasterMassDataChange(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel)
        {
            var returnMasterMassDataChange = new MasterMassDataChangeBusinessModel();
            var contactPersonBusinessModel = new List<ContactPersonBusinessModel>();
            var communicationPreferencesBusinessModel = new List<CommunicationPreferencesBusinessModel>();
            List<ClinicalContactAnalysisMassBusinessModel> clinicalContactAnalysisData = new List<ClinicalContactAnalysisMassBusinessModel>();

            try
            {
                var sapPatientIds = string.Empty;                
                var sapIds = string.Empty;
                if (masterMassDataChangeBusinessModel != null)
                {
                    switch (masterMassDataChangeBusinessModel.DownloadTypeId)
                    {
                        case (int)SCAEnums.MassDataTemplate.Patient:                            
                            returnMasterMassDataChange.PatientDetailsList = GetPatientList(masterMassDataChangeBusinessModel);
                            break;

                        case (int)SCAEnums.MassDataTemplate.Carehome:                            
                            returnMasterMassDataChange.CarehomeDetailsList = GetCarehomeList(masterMassDataChangeBusinessModel);
                            break;

                        case (int)SCAEnums.MassDataTemplate.Prescription:                            
                            returnMasterMassDataChange.PrescriptionList = GetPrescriptionsToUpdate(masterMassDataChangeBusinessModel);
                            break;

                        case (int)SCAEnums.MassDataTemplate.PatientClinicalContacts:

                            var sapClinicalPatientIds = CommonHelper.GetAppenedSapNumber(masterMassDataChangeBusinessModel.PatientId);

                            var patientClinicalContactAnalysisInfo = (new DBHelper()).GetPatientClinicalContactAnalysisInfo(sapClinicalPatientIds, masterMassDataChangeBusinessModel.CustomerId, (int)SCAEnums.ListType.MedicalStaff);

                            patientClinicalContactAnalysisInfo.ForEach(q =>
                             {
                                 clinicalContactAnalysisData.Add(new ClinicalContactAnalysisMassBusinessModel()
                                 {
                                     PatientId = q.SAPPatientNumber,
                                     IndigoPortalPatientId = Convert.ToString(q.PatientId, CultureInfo.InvariantCulture),
                                     CustomerId = q.SAPCustomerNumber,
                                     ClinicalContactTypeId = Convert.ToString(q.ClinicalContactTypeId, CultureInfo.InvariantCulture),
                                     ClinicalContactType = q.ClincialContactType,
                                     ClinicalContactValue = q.ClincialContactValue,
                                     ClinicalContactValueId = q.ClinicalContactValueTypeId != null ? q.ClinicalContactValueTypeId : string.Empty,
                                     Remarks = q.Remarks
                                 });
                             });
                            returnMasterMassDataChange.ClinicalContactAnalysisList = clinicalContactAnalysisData;
                            break;

                        case (int)SCAEnums.MassDataTemplate.PatientAnalysisInformation:

                            sapPatientIds = CommonHelper.GetAppenedSapNumber(masterMassDataChangeBusinessModel.PatientId);

                            var patientAnalysisInfo = (new DBHelper()).GetPatientClinicalContactAnalysisInfo(sapPatientIds, masterMassDataChangeBusinessModel.CustomerId, (int)SCAEnums.ListType.AnalysisInformation);

                            patientAnalysisInfo.ForEach(q =>
                               {
                                   clinicalContactAnalysisData.Add(new ClinicalContactAnalysisMassBusinessModel()
                                   {
                                       PatientId = q.SAPPatientNumber,
                                       IndigoPortalPatientId = Convert.ToString(q.PatientId, CultureInfo.InvariantCulture),
                                       CustomerId = q.SAPCustomerNumber,
                                       AnalysisInformationTypeId = Convert.ToString(q.ClinicalContactTypeId, CultureInfo.InvariantCulture), 
                                       AnalysisInformationType = q.ClincialContactType,
                                       AnalysisInformationValue = q.ClincialContactValue,
                                       AnalysisInformationValueId = q.ClinicalContactValueTypeId != null ? q.ClinicalContactValueTypeId : string.Empty,
                                       Remarks = q.Remarks
                                   });
                               });
                            returnMasterMassDataChange.ClinicalContactAnalysisList = clinicalContactAnalysisData;
                            break;

                        case (int)SCAEnums.MassDataTemplate.CustomerClinicalContacts:
                            {                                
                                var clinicalContactMassData = (new DBHelper()).CustomerClinicalAnalysisMassData(Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture), (int)SCAEnums.ListType.MedicalStaff);

                                clinicalContactMassData.ForEach(q =>
                                    {
                                        clinicalContactAnalysisData.Add(new ClinicalContactAnalysisMassBusinessModel()
                                        {
                                            CustomerId = q.SAPCustomerNumber,
                                            ClinicalContactTypeId = Convert.ToString(q.ClinicalContactTypeId, CultureInfo.InvariantCulture), 
                                            ClinicalContactType = q.ClincialContactType,
                                            ClinicalContactValue = q.ClincialContactValue,
                                            ClinicalContactValueId = q.ClinicalContactValueTypeId != null ? q.ClinicalContactValueTypeId : string.Empty,
                                            Remarks = q.Remarks
                                        });
                                    });
                                returnMasterMassDataChange.ClinicalContactAnalysisList = clinicalContactAnalysisData;
                                break;
                            }

                        case (int)SCAEnums.MassDataTemplate.CustomerAnalysisInformation:
                            {                                
                                var clinicalContactMassData = (new DBHelper()).CustomerClinicalAnalysisMassData(Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture), (int)SCAEnums.ListType.AnalysisInformation);

                                clinicalContactMassData.ForEach(q =>
                                {
                                    clinicalContactAnalysisData.Add(new ClinicalContactAnalysisMassBusinessModel()
                                    {
                                        CustomerId = q.SAPCustomerNumber,
                                        AnalysisInformationTypeId = Convert.ToString(q.ClinicalContactTypeId, CultureInfo.InvariantCulture),
                                        AnalysisInformationType = q.ClincialContactType,
                                        AnalysisInformationValue = q.ClincialContactValue,
                                        AnalysisInformationValueId = q.ClinicalContactValueTypeId != null ? q.ClinicalContactValueTypeId : string.Empty,
                                        Remarks = q.Remarks
                                    });
                                });
                                returnMasterMassDataChange.ClinicalContactAnalysisList = clinicalContactAnalysisData;
                                break;
                            }

                        case (int)SCAEnums.MassDataTemplate.PostCodeMatrix:                            
                            returnMasterMassDataChange.PostCodeMatrixList = GetPostCodeMatrixList(masterMassDataChangeBusinessModel);
                            break;

                        case (int)SCAEnums.MassDataTemplate.CommunicationFormat:                            
                            if (!string.IsNullOrEmpty(masterMassDataChangeBusinessModel.ContactPatientCarehomeId))
                            {
                                sapIds = CommonHelper.GetAppenedSapNumber(masterMassDataChangeBusinessModel.ContactPatientCarehomeId);
                            }

                            var getCommunicationFormatForMassUpdate = (new DBHelper()).GetCommunicationFormatForMassDataChanges(
                                Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture),
                                Convert.ToInt64(masterMassDataChangeBusinessModel.ContactPersonType, CultureInfo.InvariantCulture), sapIds,
                                Convert.ToInt16(masterMassDataChangeBusinessModel.LanguageId, CultureInfo.InvariantCulture));


                            getCommunicationFormatForMassUpdate.ForEach(d =>
                            {
                                var communicationRemarks = d.Remarks;

                                if (communicationRemarks == CommonConstants.PipeSeparator)
                                {
                                    communicationRemarks = string.Empty;
                                }
                                else if (communicationRemarks.StartsWith(CommonConstants.PipeSeparator, StringComparison.InvariantCulture))
                                {
                                    communicationRemarks = communicationRemarks.Substring(1, communicationRemarks.Length - 1);
                                }
                                else if (communicationRemarks.EndsWith(CommonConstants.PipeSeparator, StringComparison.InvariantCulture))
                                {
                                    communicationRemarks = communicationRemarks.Substring(1, communicationRemarks.Length - 1);
                                }


                                communicationPreferencesBusinessModel.Add(new CommunicationPreferencesBusinessModel()
                                {
                                    CustomerId = d.CustomerId,
                                    IndigoPatientId = Convert.ToString(d.IndigoPatientId, CultureInfo.InvariantCulture),
                                    IndigoCarehomeId = Convert.ToString(d.IndigoCarehomeId, CultureInfo.InvariantCulture),
                                    CommunicationPreference = d.CommunicationPreference,
                                    MarketingPreference = d.MarkettingPreference,
                                    Remarks = communicationRemarks
                                });
                            });

                            returnMasterMassDataChange.CommunicationInfoList = communicationPreferencesBusinessModel;
                            break;

                        case (int)SCAEnums.MassDataTemplate.ContactPerson:                                                  
                            if (!string.IsNullOrEmpty(masterMassDataChangeBusinessModel.ContactPatientCarehomeId))
                            {
                                sapIds = CommonHelper.GetAppenedSapNumber(masterMassDataChangeBusinessModel.ContactPatientCarehomeId);
                            }

                            var getContactPersonForMassUpdate = (new DBHelper()).GetContactPersonsForMassDataChanges(masterMassDataChangeBusinessModel.CustomerId,
                            Convert.ToInt64(masterMassDataChangeBusinessModel.ContactPersonType, CultureInfo.InvariantCulture), sapIds);

                            var contactPersonId = 0;
                            getContactPersonForMassUpdate.ForEach(d =>
                            {                               
                                contactPersonId = (int)d.ContactPersonId;

                                contactPersonBusinessModel.Add(new ContactPersonBusinessModel()
                                {
                                    ContactPersonId = Convert.ToString(contactPersonId, CultureInfo.InvariantCulture),
                                    CustomerId = Convert.ToString(d.CustomerId, CultureInfo.InvariantCulture),
                                    IndigoPatientId = Convert.ToString(d.PatientId, CultureInfo.InvariantCulture),
                                    IndigoCarehomeId = Convert.ToString(d.CarehomeId, CultureInfo.InvariantCulture),
                                    ContactPersonFirstName = d.FirstName,
                                    ContactPersonSurname = d.LastName,
                                    JobTitle = d.JobTitle,
                                    Phone = d.TelephoneNumber,
                                    Mobile = d.MobileNumber,
                                    EmailId = d.Email,
                                    Remarks = d.Remarks
                                });
                            });

                            returnMasterMassDataChange.ContactPersonInfoList = contactPersonBusinessModel;
                            break;

                        case (int)SCAEnums.MassDataTemplate.Order:
                            returnMasterMassDataChange.OrderList = GetOrdersToUpdate(masterMassDataChangeBusinessModel);
                            break;
						default:
							// do the default action
							break;
					}
                }

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, masterMassDataChangeBusinessModel.UserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, masterMassDataChangeBusinessModel.UserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, masterMassDataChangeBusinessModel.UserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, masterMassDataChangeBusinessModel.UserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, masterMassDataChangeBusinessModel.UserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, masterMassDataChangeBusinessModel.UserId);
                throw;
            }
            return returnMasterMassDataChange;
        }

        #region Private Function

        /// <summary>
        /// Get Patient List
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel">Master MassDataChange BusinessModel</param>
        /// <returns>List of MassDataPatient BusinessModel</returns>
        private static List<MassDataPatientBusinessModel> GetPatientList(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel)
        {
            var saPatientIds = CommonHelper.GetAppenedSapNumber(masterMassDataChangeBusinessModel.PatientId);
            var patientresult = (new DBHelper()).GetPatientDetailsForMassChanges(Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture),
            masterMassDataChangeBusinessModel.PatientType, masterMassDataChangeBusinessModel.PatientStatus, saPatientIds, masterMassDataChangeBusinessModel.LanguageId);

            var patientList = patientresult.Select(patient => new MassDataPatientBusinessModel
            {
                LoadId = patient.LoadId,
                CustomerID = patient.SapCustomerId,
                IndigoPatientID = patient.PatientId,
                PatientSAPID = patient.PatientSAPID,
                FirstName = patient.FirstName,
                Surname = patient.LastName,
                Gender = patient.Gender,
                Title = patient.Title,
                DateofBirth = patient.DateofBirth,
                IndigoCarehomeID = patient.CareHomeId,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                TownOrCity = patient.City,
                County = patient.County,
                Country = patient.Country,
                HouseName = patient.HouseName,
                HouseNumber = patient.HouseNumber,
                PostCode = patient.PostCode,
                DeliveryAddressLine1 = patient.DeliveryAddressLine1,
                DeliveryAddressLine2 = patient.DeliveryAddressLine2,
                DeliveryTownOrCity = patient.DeliveryCity,
                DeliveryCounty = patient.DeliveryCounty,
                DeliveryCountry = patient.DeliveryCountry,
                DeliveryHouseName = patient.DeliveryHouseName,
                DeliveryPostCode = patient.DeliveryPostCode,
                PhoneNumber = patient.TelephoneNumber,
                Mobile = patient.MobileNumber,
                Email = patient.Email,
                DataProtection = patient.IsDataProtected,
                DeliveryFrequency = patient.DeliveryFrequency,
                Round = patient.Round,
                RoundID = patient.RoundId,
                AlternateDeliveryPoint1 = patient.ADP1,
                AlternateDeliveryPoint2 = patient.ADP2,
                PatientStatus = patient.PatientStatus,
                ReasonCode = patient.ReasonCode,
                PatientType = patient.PatientType,
                NHSID = patient.NHSId,
                LocalID = patient.LocalId,
                AssessmentDate = patient.AssessmentDate,
                NextAssessmentDate = patient.NextAssessmentDate,
                DeliveryDate = patient.DeliveryDate,
                NextDeliveryDate = patient.NextDeliveryDate,
                SendtoSAP = patient.IsSentToSAP,
                BillTo = patient.BillTo,
                Remarks = patient.Remarks
            }).ToList();

            return patientList;
        }

        /// <summary>
        /// Gets the post code matrix list.
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel">The master mass data change business model.</param>
        /// <returns></returns>
        private List<MassDataPostCodeMatrixBusinessModel> GetPostCodeMatrixList(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel)
        {
            var returnMasterMassDataChange = new MasterMassDataChangeBusinessModel();
            var postCodeMatrixBusinessModel = new List<MassDataPostCodeMatrixBusinessModel>();
            var customerIds = masterMassDataChangeBusinessModel.CustomerIdForPostCodeMatrix != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.CustomerIdForPostCodeMatrix) : string.Empty;
            var getPostCodeMatrixForMassUpdate = (new DBHelper()).GetPostCodeMatrixForMassDataChanges(customerIds);

            getPostCodeMatrixForMassUpdate.ForEach(q =>
            {
                postCodeMatrixBusinessModel.Add(new MassDataPostCodeMatrixBusinessModel()
                {
                    CustomerId = q.CustomerId,
                    PostcodeMatrixID = Convert.ToString(q.PostcodeMatrixID, CultureInfo.InvariantCulture),
                    Monday = q.Monday == true ? CommonConstants.Yes : CommonConstants.No,
                    Tuesday = q.Tuesday == true ? CommonConstants.Yes : CommonConstants.No,
                    Wednesday = q.Wednesday == true ? CommonConstants.Yes : CommonConstants.No,
                    Thursday = q.Thursday == true ? CommonConstants.Yes : CommonConstants.No,
                    Friday = q.Friday == true ? CommonConstants.Yes : CommonConstants.No,
                    RoundId = Convert.ToString(q.RoundID, CultureInfo.InvariantCulture),
                    Round = Convert.ToString(q.Round, CultureInfo.InvariantCulture),
                    PostCode = Convert.ToString(q.Postcode, CultureInfo.InvariantCulture),
                    IsActive = q.IsActive == true ? CommonConstants.Yes : CommonConstants.No,
                });
            });

            return returnMasterMassDataChange.PostCodeMatrixList = postCodeMatrixBusinessModel;
        }

        /// <summary>
        /// GetCarehomeList
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel"></param>
        /// <returns></returns>
        private List<MassDataCareHomeBusinessModel> GetCarehomeList(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel)
        {
            var returnMasterMassDataChange = new MasterMassDataChangeBusinessModel();
            var careHomeDetailsForMassUpdateBusinessModel = new List<MassDataCareHomeBusinessModel>();
            var careHomeStatus = masterMassDataChangeBusinessModel.CareHomeStatusForCareHome != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.CareHomeStatusForCareHome) : string.Empty;
            // download logic for Carehome
            var getcareHomeDetailsForMassUpdate = (new DBHelper()).GetCarehomeDetailsForMassChanges(Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture), Convert.ToInt64(masterMassDataChangeBusinessModel.OrderTypeId, CultureInfo.InvariantCulture), masterMassDataChangeBusinessModel.CarehomeId, careHomeStatus, masterMassDataChangeBusinessModel.LanguageId);

            getcareHomeDetailsForMassUpdate.ForEach(d =>
            {
                careHomeDetailsForMassUpdateBusinessModel.Add(new MassDataCareHomeBusinessModel()
                {
                    LoadId = Convert.ToString(d.LoadId, CultureInfo.InvariantCulture),
                    CustomerId = d.CustomerId,
                    IndigoCarehomeId = Convert.ToString(d.CareHomeId, CultureInfo.InvariantCulture),
                    SAPCarehomeId = d.SAPCareHomeNumber,
                    CarehomeName = d.CareHomeName,
                    HouseName = d.HouseName,
                    AddressLine1 = d.AddressLine1,
                    AddressLine2 = d.AddressLine2,
                    TownOrCity = d.TownOrCity,
                    County = d.County,
                    Country = d.Country,
                    PostCode = d.PostCode,
                    PhoneNumber = d.TelephoneNumber,
                    Mobile = d.MobileNumber,
                    Email = d.Email,
                    CarehomeStatus = d.CareHomeStatus,
                    NextDeliveryDate = d.NextDeliveryDate,
                    Round = d.Round,
                    RoundId = d.RoundId,
                    DeliveryFrequency = d.DeliveryFrequency,
                    CarehomeType = d.CareHomeType,
                    OrderType = d.OrderType,
                    PurchaseOrderNumber = Convert.ToString(d.PurchaseOrderNo, CultureInfo.InvariantCulture),
                    SendToSAP = d.IsSentToSAP,
                    BillTo = Convert.ToString(d.BillTo, CultureInfo.InvariantCulture),
                    Remarks = d.Remarks
                });
            });

            return returnMasterMassDataChange.CarehomeDetailsList = careHomeDetailsForMassUpdateBusinessModel;

        }

        /// <summary>
        /// Download Orders for Mass Update based on Selected Criteria
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel">Details of Customer/Patient and other search criteria</param>
        /// <returns>List of Orders to be Updated</returns>
        private List<MassDataOrderBusinessModel> GetOrdersToUpdate(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel)
        {
            var iCustomerId = Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture);
            var orderDownloadParams = new
            {
                customerId = iCustomerId,
                productSAPId = masterMassDataChangeBusinessModel.SAPProductIdForOrderUpdate != null ? masterMassDataChangeBusinessModel.SAPProductIdForOrderUpdate : string.Empty,
                patientIds = masterMassDataChangeBusinessModel.PatientsForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.PatientsForOrderUpdate) : string.Empty,
                careHomeIds = masterMassDataChangeBusinessModel.CareHomesForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.CareHomesForOrderUpdate) : string.Empty,
                orderIds = masterMassDataChangeBusinessModel.OrderIdsForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.OrderIdsForOrderUpdate) : string.Empty,
                patientTypes = masterMassDataChangeBusinessModel.PatientTypesForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.PatientTypesForOrderUpdate) : string.Empty,
                careHomeOrderTypes = masterMassDataChangeBusinessModel.CareHomeOrderTypesForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.CareHomeOrderTypesForOrderUpdate) : string.Empty,
                orderTypes = masterMassDataChangeBusinessModel.OrderTypesForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.OrderTypesForOrderUpdate) : string.Empty,
                patientStatus = masterMassDataChangeBusinessModel.PatientStatusForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.PatientStatusForOrderUpdate) : string.Empty,
                careHomeStatus = masterMassDataChangeBusinessModel.CareHomeStatusForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.CareHomeStatusForOrderUpdate) : string.Empty,
                orderStatus = masterMassDataChangeBusinessModel.OrderStatusForOrderUpdate != null ? string.Join(stringSeparator, masterMassDataChangeBusinessModel.OrderStatusForOrderUpdate) : string.Empty,
                orderDownloadFor = masterMassDataChangeBusinessModel.OrderDataDownloadFor,
            };

            var orders = new DBHelper().GetOrderDetailsForMassChanges(orderDownloadParams.customerId, orderDownloadParams.productSAPId, orderDownloadParams.patientIds, orderDownloadParams.careHomeIds, orderDownloadParams.orderIds, orderDownloadParams.patientTypes, orderDownloadParams.careHomeOrderTypes, orderDownloadParams.orderTypes, orderDownloadParams.patientStatus, orderDownloadParams.careHomeStatus, orderDownloadParams.orderStatus, orderDownloadParams.orderDownloadFor).ToList();

            if (orders != null)
            {
                var orderData = orders.Select(q => new MassDataOrderBusinessModel
                {
                    IndigoCustomerID = Convert.ToString(q.CustomerId, CultureInfo.InvariantCulture),
                    SAPCustomerID = q.SAPCustomerId,
                    SAPPatientID = CommonHelper.TrimLeadingZeros(q.SAPPatientId),
                    IndigoPatientID = q.PatientId,
                    IndigoCarehomeID = Convert.ToInt64(q.CareHomeId, CultureInfo.InvariantCulture),
                    SAPCarehomeID = q.SAPCareHomeId,
                    lngOrderType = q.OrderType,
                    IndigoOrderType = GetOrderTypeText(q.OrderType),
                    FrontEndOrderID = q.OrderId,
                    OldSAPProductID = CommonHelper.TrimLeadingZeros(q.SAPProductId),
                    BaseMaterial = q.BaseMaterial,
                    Quantity = q.Quantity,
                    DeliveryDate = Convert.ToDateTime(q.Deliverydate, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture)
                }).ToList();

                return orderData;
            }
            else
            {
                return new List<MassDataOrderBusinessModel>();
            }
        }

        /// <summary>
        /// Gets Order Type Text for OrderType Id
        /// </summary>
        /// <param name="orderType">OrderType Id</param>
        /// <returns>Order Type text</returns>
        private string GetOrderTypeText(long orderType)
        {
            var typeId = Convert.ToInt64(orderType);
            var typeText = string.Empty;

            if (typeId != default(long))
            {
                if (typeId == Convert.ToInt64(SCAEnums.OrderType.SAMP, CultureInfo.InvariantCulture))
                {
                    typeText = SCAEnums.OrderType.SAMP.ToString();
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDF, CultureInfo.InvariantCulture))
                {
                    typeText = SCAEnums.OrderType.ZHDF.ToString();
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDH, CultureInfo.InvariantCulture))
                {
                    typeText = SCAEnums.OrderType.ZHDH.ToString();
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDP, CultureInfo.InvariantCulture))
                {
                    typeText = SCAEnums.OrderType.ZHDP.ToString();
                }
                else if (typeId == Convert.ToInt64(SCAEnums.OrderType.ZHDR, CultureInfo.InvariantCulture))
                {
                    typeText = SCAEnums.OrderType.ZHDR.ToString();
                }
            }
            
            return typeText;
        }

        /// <summary>
        /// Download Prescriptions for Mass Update based on Selected Criteria
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel">Details of Customer/Patient and other search criteria</param>
        /// <returns>List of Prescriptions to be updated</returns>
        private static List<MassDataPrescriptionBusinessModel> GetPrescriptionsToUpdate(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel)
        {
            var prescriptionBusinessModel = new List<MassDataPrescriptionBusinessModel>();
            var getPrescriptionDetailsForMassUpdate = (new DBHelper()).DownloadPrescription(Convert.ToInt64(masterMassDataChangeBusinessModel.CustomerId, CultureInfo.InvariantCulture), Convert.ToInt64(masterMassDataChangeBusinessModel.ProductId, CultureInfo.InvariantCulture));

            getPrescriptionDetailsForMassUpdate.ForEach(q =>
            {
                prescriptionBusinessModel.Add(new MassDataPrescriptionBusinessModel()
                {
                    IndigoPortalPatientID = Convert.ToString(q.PatientInternalId, CultureInfo.InvariantCulture),
                    SAPPatientID = q.SAPPatientNumber,
                    IndigoPortalCustomerID = Convert.ToString(q.CustomerId, CultureInfo.InvariantCulture),
                    SAPCustomerID = q.SAPCustomerId,
                    SAPProductId = q.SAPProductID,
                    BaseMaterial = q.BaseMaterial,
                    ProductName = q.DescriptionUI,
                    Frequency = Convert.ToString(q.Frequency, CultureInfo.InvariantCulture),
                    DeliveryDate = Convert.ToDateTime(q.DeliveryDate, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture),
                    NextDeliveryDate = Convert.ToDateTime(q.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture),
                    ValidFromDate = Convert.ToDateTime(q.ValidFromDate, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture),
                    ValidToDate = Convert.ToDateTime(q.ValidToDate, CultureInfo.InvariantCulture).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture),
                    Status = q.Status,
                    AssessedPadsPerDay = Convert.ToString(q.AssessedPadsPerDay, CultureInfo.InvariantCulture),
                    ActualPPD = Convert.ToString(q.ActPPD, CultureInfo.InvariantCulture),
                    IsOverridden = q.IsOverridenFlag == true ? "Y" : "N",
                    DQ1 = Convert.ToString(q.DQ1, CultureInfo.InvariantCulture),
                    DQ2 = Convert.ToString(q.DQ2, CultureInfo.InvariantCulture),
                    DQ3 = Convert.ToString(q.DQ3, CultureInfo.InvariantCulture),
                    DQ4 = Convert.ToString(q.DQ4, CultureInfo.InvariantCulture),
                    FlagDQ1 = q.FlagDQ1.HasValue && q.FlagDQ1.Value ? "Y" : "N",
                    FlagDQ2 = q.FlagDQ2.HasValue && q.FlagDQ2.Value ? "Y" : "N",
                    FlagDQ3 = q.FlagDQ3.HasValue && q.FlagDQ3.Value ? "Y" : "N",
                    FlagDQ4 = q.FlagDQ4.HasValue && q.FlagDQ4.Value ? "Y" : "N",
                    NewSAPProductID = string.Empty,
                    CountChanged = "N"
                });
            });

            return prescriptionBusinessModel;
        }

        #endregion
    }
}
