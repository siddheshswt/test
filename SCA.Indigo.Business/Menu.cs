﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-07-2015
// ***********************************************************************
// <copyright file="Menu.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Business
{
    using System.Collections.Generic;
    using System.Linq;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Model;
    using System;
	using System.Globalization;
    using System.ServiceModel;

    /// <summary>
    /// Menu Class
    /// </summary>       
    public class Menu : IMenu
    {
        /// <summary>
        /// Get Menu Details
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>
        /// List (UserMenuDetailsBusinessModel);.
        /// </returns>
        public List<UserMenuDetailsBusinessModel> GetUserMenuDetails(string userName, string languageId)
        {
            long longlanguageId = Convert.ToInt64(languageId, CultureInfo.InvariantCulture);
            var userMenuDetails = (new DBHelper()).GetUserMenuDetails(userName, longlanguageId);
            return userMenuDetails.Select(item => new UserMenuDetailsBusinessModel()
            {
                MenuId = item.MenuID,
                ParentMenuId = item.ParentMenuID,
                MenuName = item.MenuName,
                ControllerName = item.ControllerName,
                ActionName = item.ActionName,
                ViewOnly= item.ViewOnly.Value,
                FullRights= item.FullRights.Value,
                HasSubMenu = item.HasSubMenu,
                ParentMenuIndex = item.ParentMenuIndex,
                DisplayIndex = item.DisplayIndex,
                IsDisplay=item.IsDisplay

            }).ToList();
        }
    }
}