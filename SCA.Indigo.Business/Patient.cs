﻿//----------------------------------------------------------------------------------------------
// <copyright file="Patient.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Infrastructure.Repositories;
    using SCA.Indigo.Model;
    using EFModel = SCA.Indigo.Model;
    using System.Text;
    using System.ServiceModel;

    /// <summary>
    /// Patient Class
    /// </summary>    
    public class Patient : IPatient
    {
        /// <summary>
        /// The date format
        /// </summary>
        public const string DateFormat = "dd/MM/yyyy";
        /// <summary>
        /// The date time format
        /// </summary>
        public const string DateTimeFormat = "dd/MM/yyyy HH:mm";

        /// <summary>
        /// The grid analysis
        /// </summary>
        private const string GridAnalysis = "Analysis";
        /// <summary>
        /// The grid medical
        /// </summary>
        private const string GridMedical = "Staff";

        /// <summary>
        /// The _user identifier
        /// </summary>
        private string globalUserId = string.Empty;

        /// <summary>
        /// Flag to check if incremental update required
        /// </summary>
        private bool isIncrementalUpdateRequired;

        /// <summary>
        /// Add new Patient note
        /// </summary>
        /// <param name="objPatientNotesBusinessModel">The object patient notes business model.</param>
        /// <returns>
        /// boolean value
        /// </returns>
        public bool AddNote(PatientNotesBusinessModel objPatientNotesBusinessModel)
        {
            IPatientNoteRepository patientNoteRepository = new PatientNoteRepository();
            try
            {
                if (objPatientNotesBusinessModel != null)
                {
                    globalUserId = objPatientNotesBusinessModel.CreatedBy;
                    UnitOfWork unitOfWork = patientNoteRepository.UnitOfWork;
                    var objPatientNote = new PatientNote()
                    {
                        PatientId = objPatientNotesBusinessModel.PatientId,
                        Note = objPatientNotesBusinessModel.NoteText,
                        CreatedBy = Guid.Parse(objPatientNotesBusinessModel.CreatedBy),
                        CreatedDate = Convert.ToDateTime(objPatientNotesBusinessModel.CreatedDate, CultureInfo.InvariantCulture)
                    };
                    patientNoteRepository.InsertOrUpdate(objPatientNote);
                    unitOfWork.Commit();
                    return true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (patientNoteRepository != null)
                {
                    patientNoteRepository.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Get carehome details based on CustomerId
        /// </summary>
        /// <param name="objCareHomeBusinessModel">The object care home business model.</param>
        /// <returns>
        /// List (CareHomeBusinessModel) .
        /// </returns>
        public List<CareHomeBusinessModel> GetCareHomeDetails(CareHomeBusinessModel objCareHomeBusinessModel)
        {
            try
            {
                if (objCareHomeBusinessModel == null)
                {
                    return null;
                }

                this.globalUserId = Convert.ToString(objCareHomeBusinessModel.LoggedInUserId, CultureInfo.InvariantCulture);
                if (objCareHomeBusinessModel.CustomerId > 0)
                {
                    ///Set carehome Id 0 when all carehome need to load
                    int carehomeId = 0;
                    var objListCareHomeBusinessModel = (new DBHelper()).GetCarehomeDetails(objCareHomeBusinessModel.CustomerId, carehomeId, this.globalUserId);
                    return objListCareHomeBusinessModel.Select(careHomedata => new CareHomeBusinessModel()
                    {
                        CareHomeId = careHomedata.CareHomeId,
                        CareHomeName = careHomedata.CareHomeName,
                        SapCareHomeNumber = !string.IsNullOrEmpty(careHomedata.SAPCareHomeNumber) ? careHomedata.SAPCareHomeNumber.TrimStart(new char[] { '0' }) : careHomedata.SAPCareHomeNumber,
                        PhoneNo = Convert.ToString(careHomedata.TelephoneNumber, CultureInfo.InvariantCulture),
                        MobileNo = Convert.ToString(careHomedata.MobileNumber, CultureInfo.InvariantCulture),
                        EmailAddress = careHomedata.Email,
                        County = careHomedata.County,
                        Country = careHomedata.Country,
                        CountyCode = careHomedata.CountyCode,
                        City = careHomedata.City,
                        HouseName = careHomedata.HouseName,
                        HouseNumber = careHomedata.HouseNumber,
                        Address1 = careHomedata.AddressLine1,
                        Address2 = careHomedata.AddressLine2,
                        PostCode = careHomedata.PostCode,
                        NextDeliveryDate = Convert.ToString(careHomedata.NextDeliveryDate, CultureInfo.InvariantCulture),
                        RoundId = careHomedata.RoundId,
                        Round = careHomedata.CareHomeRound,
                        DeliveryFrequency = careHomedata.DeliveryFrequency,
                        CareHomeStatus = careHomedata.CareHomeStatus
                    }).ToList();
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Get List of Customers in popup for MedicalStaff
        /// </summary>
        /// <param name="medicalStaffAnalysisInfoBusinessModel"></param>
        /// <returns>
        /// List (MedicalStaff AnalysisInfo BusinessModel) .
        /// </returns>
        public List<MedicalStaffAnalysisInfoBusinessModel> GetCustomerMedicalAnalysis(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel)
        {
            if (medicalStaffAnalysisInfoBusinessModel == null)
            {
                return null;
            }

            ///Get User Id
            string userId = Convert.ToString(medicalStaffAnalysisInfoBusinessModel.UserId, CultureInfo.InvariantCulture);

            IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository = null;

            ///Result Analysis Info
            List<MedicalStaffAnalysisInfoBusinessModel> clinicalContactInfo = new List<MedicalStaffAnalysisInfoBusinessModel>();

            IClinicalContactValueMappingRepository clinicalContactValueMappingRepository = null;
            try
            {
                idxCustomerMedicalStaffAnalysisInfoRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();
                clinicalContactValueMappingRepository = new ClinicalContactValueMappingRepository();
                int listId = Convert.ToInt32(medicalStaffAnalysisInfoBusinessModel.ListId, CultureInfo.InvariantCulture);
                long customerIdValue = Convert.ToInt64(medicalStaffAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture);

                if (medicalStaffAnalysisInfoBusinessModel.IsClinicalContactWithLinkage && medicalStaffAnalysisInfoBusinessModel.ListTypeId == (int)SCAEnums.ListType.MedicalStaff)
                {
                    /// Get All Clinical Contact Linkage data as per Customer Id
                    List<ClinicalContactValueMapping> allCustomerValueMapping = clinicalContactValueMappingRepository.GetAllClinicalContactValueMapping().Where(q => q.ClinicalContactRoleMapping.CustomerId == customerIdValue && q.ClinicalValueMaster.IsRemoved == false).ToList();

                    clinicalContactInfo = allCustomerValueMapping.Where(q => q.ChildListId == listId)
                        .Select(s => new MedicalStaffAnalysisInfoBusinessModel()
                                                  {
                                                      ListId = s.ChildListId,
                                                      ListTypeId = medicalStaffAnalysisInfoBusinessModel.ListTypeId,
                                                      StaffName = s.ClinicalValueMaster.ContactName,
                                                      CustomerMedicalStaffAnalysisInfoId = s.ClinicalValueMaster.ClinicalValueMasterId,
                                                      CustomerName = string.Empty,
                                                      PatientName = string.Empty
                                                  }).ToList();


                }
                else
                {
                    /// Get from IDXCustomerMedicalStaffAnalusisInfo.
                    clinicalContactInfo = idxCustomerMedicalStaffAnalysisInfoRepository.GetCustomerMedicalStaffAnalysisInfoByListID(Convert.ToInt32(listId, CultureInfo.InvariantCulture)).Where(q => q.IsRemoved == false)
                        .Where(q => q.CustomerID == customerIdValue)
                                                    .Select(s => new MedicalStaffAnalysisInfoBusinessModel()
                                                  {
                                                      ListId = s.MedicalAnalysisListID,
                                                      ListTypeId = s.ClinicalContactAnalysisMaster.TypeId,
                                                      StaffName = s.ContactName,
                                                      CustomerMedicalStaffAnalysisInfoId = s.IDXCustomerMedicalStaffAnalysisInfoID,
                                                      CustomerName = string.Empty,
                                                      PatientName = string.Empty,
                                                      IsFieldType = s.IsFieldType
                                                  }).ToList().FindAll(d => d.IsFieldType != true);
                }

                /// Insert Blank Row for default
                clinicalContactInfo = clinicalContactInfo.OrderBy(d => d.StaffName).ToList();
                var blankRow = new MedicalStaffAnalysisInfoBusinessModel { CustomerId = 0, StaffId = " ", StaffName = " ", ListId = Convert.ToInt32(listId, CultureInfo.InvariantCulture), CustomerMedicalStaffAnalysisInfoId = 0 };
                clinicalContactInfo.Insert(0, blankRow);

                return clinicalContactInfo;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {

                if (idxCustomerMedicalStaffAnalysisInfoRepository != null)
                {
                    idxCustomerMedicalStaffAnalysisInfoRepository.Dispose();
                }

                if (clinicalContactValueMappingRepository != null)
                {
                    clinicalContactValueMappingRepository.Dispose();
                }

            }

            return null;
        }

        /// <summary>
        /// Display medical staff and Analysis grid Columns
        /// </summary>
        /// <param name="gridName">Name of the grid.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns>
        /// List (MedicalStaff AnalysisInfo BusinessModel) .
        /// </returns>
        public List<MedicalStaffAnalysisInfoBusinessModel> GetMedicalAndStaffGridColumns(string gridName, int customerId)
        {
            IIDXCustomerMedicalStaffAnalysisInfoRepository medStaffRepository = null;
            try
            {
                var listTypeId = (long)SCAEnums.ListType.MedicalStaff;
                if (gridName == GridAnalysis)
                {
                    listTypeId = (long)SCAEnums.ListType.AnalysisInformation;
                }

                medStaffRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();
                var medicalStaffColumns = medStaffRepository.GetListIdByCustomerId((long)customerId).Where(d => d.IsFieldType == true);

                List<MedicalStaffAnalysisInfoBusinessModel> objMedstaffdatalist = new List<MedicalStaffAnalysisInfoBusinessModel>();

                medicalStaffColumns.ToList().ForEach(idx =>
                 {
                     var translation = idx.ClinicalContactAnalysisMaster.TranslationProxy.Translations.Where(q => q.TransProxyID == idx.ClinicalContactAnalysisMaster.TransProxyId && (q.LanguageID == (long)SCAEnums.ListType.LanguageId)).Select(w => w.TranslationType).FirstOrDefault();
                     objMedstaffdatalist.Add(
                         new MedicalStaffAnalysisInfoBusinessModel()
                         {
                             DefaultText = translation != null ? translation : idx.ClinicalContactAnalysisMaster.TranslationProxy.Text,
                             ListTypeId = idx.ClinicalContactAnalysisMaster.TypeId,
                             ListId = idx.MedicalAnalysisListID,
                             CustomerId = customerId,
                             CustomerMedicalStaffAnalysisInfoId = 0,
                             StaffName = string.Empty,
                             IsMandatory = Convert.ToBoolean(idx.IsMandatory, CultureInfo.InvariantCulture)
                         });
                 });
                objMedstaffdatalist = objMedstaffdatalist.Where(q => q.ListTypeId == listTypeId).OrderBy(d => d.DefaultText).ToList();                
                return objMedstaffdatalist;
            }
            finally
            {
                if (medStaffRepository != null)
                {
                    medStaffRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Get Clinical Contact Hierarchy For Patient Details
        /// </summary>
        /// <param name="objMedicalStaffAnalysisInfoBusinessModel">MedicalStaff Analysis Info Business Model</param>
        /// <returns>
        /// Collection of MedicalStaff Analysis Info Business Model
        /// </returns>
        private List<MedicalStaffAnalysisInfoBusinessModel> GetClinicalContactHierarchy(MedicalStaffAnalysisInfoBusinessModel objMedicalStaffAnalysisInfoBusinessModel)
        {
            if (objMedicalStaffAnalysisInfoBusinessModel == null)
            {
                return null;
            }
            IIDXPatientMedicalStaffRepository idxRepository = null;
            IClinicalContactValueMappingRepository clinicalContactValueMappingRepository = null;

            List<MedicalStaffAnalysisInfoBusinessModel> clinicalContactHierarchy = new List<MedicalStaffAnalysisInfoBusinessModel>();
            long customerId = Convert.ToInt64(objMedicalStaffAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture);
            string userId = Convert.ToString(objMedicalStaffAnalysisInfoBusinessModel.UserId, CultureInfo.InvariantCulture);

            if (customerId > 0)
            {
                try
                {
                    idxRepository = new IDXPatientMedicalStaffRepository();
                    clinicalContactValueMappingRepository = new ClinicalContactValueMappingRepository();
                    /// Get All Master List Data
                    long listTypeId = (long)SCAEnums.ListType.MedicalStaff;
                    string listType = Convert.ToString(listTypeId, CultureInfo.InvariantCulture);

                    /// Get Clinical Contact Analysis Master data
                    var clinicalContactMasterData = new DBHelper().GetClinicalContactAnalysisMaster(listType, (int)SCAEnums.OtherConstants.LanguageId);

                    /// Get Clinical Contact Linkage Data
                    var clinicalLinkageData = new DBHelper().GetClinicalContactHierarchy(customerId);

                    long? hierarchyheading = 0;
                    clinicalLinkageData.ForEach(q =>
                    {
                        if (q.HierarchyListId != hierarchyheading)
                        {
                            clinicalContactHierarchy.Add(new MedicalStaffAnalysisInfoBusinessModel()
                            {
                                DefaultText = Enum.GetName(typeof(SCAEnums.HierarchyType), q.HierarchyListId),
                                ListTypeId = 0,
                                ListId = 0,
                                PatientId = Convert.ToInt64(q.ParentListId, CultureInfo.InvariantCulture),
                                CustomerId = customerId,
                                CustomerMedicalStaffAnalysisInfoId = 0,
                                StaffName = string.Empty,
                                HierarchyListId = 0,
                                HierarchyRowNum = 0
                            });
                        }

                        clinicalContactHierarchy.Add(new MedicalStaffAnalysisInfoBusinessModel()
                        {
                            DefaultText = clinicalContactMasterData.Find(d => d.clinicalcontactanalysismasterid == q.ChildListId).ContactName,
                            ListTypeId = clinicalContactMasterData.Find(d => d.clinicalcontactanalysismasterid == q.ChildListId).TypeId,
                            ListId = clinicalContactMasterData.Find(d => d.clinicalcontactanalysismasterid == q.ChildListId).clinicalcontactanalysismasterid,
                            PatientId = Convert.ToInt64(q.ParentListId, CultureInfo.InvariantCulture),
                            CustomerId = customerId,
                            CustomerMedicalStaffAnalysisInfoId = 0,
                            StaffName = string.Empty,
                            HierarchyListId = Convert.ToInt64(q.HierarchyListId, CultureInfo.InvariantCulture),
                            HierarchyRowNum = Convert.ToInt64(q.RowNum, CultureInfo.InvariantCulture)
                        });

                        hierarchyheading = q.HierarchyListId;
                    });

                    /// Check of Existing Patient, Get all available value for the Clinical Contact types

                    if (objMedicalStaffAnalysisInfoBusinessModel.PatientId > 0)
                    {
                        /// Get all MedicalStaff Data by Patient Id
                        var clinicalData = idxRepository.GetAllPatientMedicalStaff().Where(q => q.PatientId == objMedicalStaffAnalysisInfoBusinessModel.PatientId && q.IsClinicalLinkage == true);

                        /// Get all Clinical Contact Value Mapping details
                        List<ClinicalContactValueMapping> clinicalContactValueMappings = clinicalContactValueMappingRepository.GetAllClinicalContactValueMapping()
                             .Where(q => q.ClinicalContactRoleMapping.CustomerId == customerId).ToList();

                        var objValueList = clinicalData.ToList().Select(res => new MedicalStaffAnalysisInfoBusinessModel
                         {

                             StaffName = clinicalContactValueMappings.Where(d => d.ClinicalValueMaster.ClinicalValueMasterId == res.CustomerMedicalStaffId).FirstOrDefault().ClinicalValueMaster.ContactName,
                             ListId = clinicalContactValueMappings.Where(d => d.ClinicalValueMaster.ClinicalValueMasterId == res.CustomerMedicalStaffId).FirstOrDefault().ChildListId,
                             ListTypeId = listTypeId,
                             CustomerMedicalStaffAnalysisInfoId = res.CustomerMedicalStaffId.Value,
                             CustomerId = customerId,
                             IDXPatientMedicalStaffId = res.IDXPatientMedicalStaffId
                         }).ToList();

                        /// Bind the Staff Name details with the selected Columns
                        clinicalContactHierarchy = this.MergeData(objValueList, clinicalContactHierarchy, (int)SCAEnums.ListType.MedicalStaff);
                    }
                }
                catch (DataException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (FormatException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (InvalidOperationException e)
                {
                    LogHelper.LogException(e, userId);
                }
                catch (SystemException e)
                {
                    LogHelper.LogException(e, userId);
                    throw;
                }
                catch (Exception e)
                {
                    LogHelper.LogException(e, userId);
                    throw;
                }
                finally
                {
                    if (idxRepository != null)
                    {
                        idxRepository.Dispose();
                    }
                    if (clinicalContactValueMappingRepository != null)
                    {
                        clinicalContactValueMappingRepository.Dispose();
                    }
                }
            }

            return clinicalContactHierarchy;
        }

        /// <summary>
        /// Function to get the medical analysis information
        /// </summary>
        /// <param name="objMedicalStaffAnalysisInfoBusinessModel">The medical analysis information business model.</param>
        /// <returns>
        /// List (MedicalStaff AnalysisInfo BusinessModel).
        /// </returns>
        public List<MedicalStaffAnalysisInfoBusinessModel> GetMedicalStaffAnalysisInfo(MedicalStaffAnalysisInfoBusinessModel objMedicalStaffAnalysisInfoBusinessModel)
        {
            if (objMedicalStaffAnalysisInfoBusinessModel == null)
            {
                return null;
            }

            /// Get Clinical Contact Linkage 
            if (objMedicalStaffAnalysisInfoBusinessModel.IsClinicalContactWithLinkage && objMedicalStaffAnalysisInfoBusinessModel.GridName == GridMedical)
            {
                return this.GetClinicalContactHierarchy(objMedicalStaffAnalysisInfoBusinessModel);
            }
            /// End Clinical Contact Linkage

            string userId = Convert.ToString(objMedicalStaffAnalysisInfoBusinessModel.UserId, CultureInfo.InvariantCulture);

            List<MedicalStaffAnalysisInfoBusinessModel> medicalAnalysisColumns = new List<MedicalStaffAnalysisInfoBusinessModel>();

            IIDXPatientMedicalStaffRepository idxRepository = null;

            long customerId = Convert.ToInt64(objMedicalStaffAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture);
            try
            {
                idxRepository = new IDXPatientMedicalStaffRepository();
                if (objMedicalStaffAnalysisInfoBusinessModel.PatientId != 0)
                {
                    List<MedicalStaffAnalysisInfoBusinessModel> infoColumns = new List<MedicalStaffAnalysisInfoBusinessModel>();
                    int listTypeId = 0;

                    if (objMedicalStaffAnalysisInfoBusinessModel.GridName == GridAnalysis)
                    {
                        infoColumns = this.GetMedicalAndStaffGridColumns(GridAnalysis, Convert.ToInt32(customerId, CultureInfo.InvariantCulture));
                        listTypeId = (int)SCAEnums.ListType.AnalysisInformation;
                    }
                    else
                    {
                        infoColumns = this.GetMedicalAndStaffGridColumns(GridMedical, Convert.ToInt32(customerId, CultureInfo.InvariantCulture));
                        listTypeId = (int)SCAEnums.ListType.MedicalStaff;
                    }

                    IQueryable<IDXPatientMedicalStaff> result = idxRepository.GetMedicalStaffInformation(objMedicalStaffAnalysisInfoBusinessModel.PatientId, listTypeId);

                    /// Get from IDXCustomerMedicalStaffAnalusisInfo.
                    List<IDXCustomerMedicalStaffAnalysisInfo> allClinicalContactInfo = this.GetAllCustomerMedicalInfoByCustomerId(customerId); // idxCustomerMedicalStaffAnalysisInfoRepository.GetListIdByCustomerId(customerId).ToList();

                    List<MedicalStaffAnalysisInfoBusinessModel> objDataList = new List<MedicalStaffAnalysisInfoBusinessModel>();
                    result.ToList().ForEach(d =>
                    {
                        var idxCustomerMedicalStaffAnalysisInfo = allClinicalContactInfo.Find(q => q.IDXCustomerMedicalStaffAnalysisInfoID == d.CustomerMedicalStaffId && q.ClinicalContactAnalysisMaster.TypeId == listTypeId);
                        if (idxCustomerMedicalStaffAnalysisInfo != null)
                        {
                            objDataList.Add(new MedicalStaffAnalysisInfoBusinessModel
                            {
                                DefaultText = idxCustomerMedicalStaffAnalysisInfo.ClinicalContactAnalysisMaster.TranslationProxy.Text,
                                StaffName = idxCustomerMedicalStaffAnalysisInfo.ContactName,
                                ListId = idxCustomerMedicalStaffAnalysisInfo.MedicalAnalysisListID,
                                ListTypeId = idxCustomerMedicalStaffAnalysisInfo.ClinicalContactAnalysisMaster.TypeId,
                                CustomerMedicalStaffAnalysisInfoId = idxCustomerMedicalStaffAnalysisInfo.IDXCustomerMedicalStaffAnalysisInfoID,
                                CustomerId = idxCustomerMedicalStaffAnalysisInfo.CustomerID,
                                IDXPatientMedicalStaffId = d.IDXPatientMedicalStaffId
                            });
                        }

                    });
                    medicalAnalysisColumns = this.MergeData(objDataList, infoColumns, listTypeId);
                }
                else
                {
                    return this.GetMedicalAndStaffGridColumns(objMedicalStaffAnalysisInfoBusinessModel.GridName, Convert.ToInt32(objMedicalStaffAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture));
                }

                idxRepository.Dispose();
                return medicalAnalysisColumns;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (idxRepository != null)
                {
                    idxRepository.Dispose();
                }
            }
            return null;
        }

        private List<IDXCustomerMedicalStaffAnalysisInfo> GetAllCustomerMedicalInfoByCustomerId(long customerId)
        {
            IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository = null;
            try
            {
                /// Get from IDXCustomerMedicalStaffAnalusisInfo.
                idxCustomerMedicalStaffAnalysisInfoRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();

                return idxCustomerMedicalStaffAnalysisInfoRepository.GetListIdByCustomerId(customerId).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idxCustomerMedicalStaffAnalysisInfoRepository != null)
                {
                    idxCustomerMedicalStaffAnalysisInfoRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Check whether patient has any active order
        /// </summary>
        /// <param name="patientId">Patient's Id</param>
        /// <param name="careHomeId">CareHome Id</param>
        /// <returns>
        ///   <c>True</c>If Any Order is Active
        /// </returns>
        private static bool HasAnyActiveOrder(long patientId, long careHomeId, bool isProductOrder)
        {
            IOrderRepository patientOrderRepository = null;
            try
            {
                patientOrderRepository = new OrderRepository();
                var orders = new List<EFModel.Order>();
                if (careHomeId != 0)
                {
                    orders = patientOrderRepository.GetCarehomeOrders(Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture)).ToList();
                }
                else
                {
                    orders = patientOrderRepository.GetPatientOrders(Convert.ToInt64(patientId, CultureInfo.InvariantCulture)).ToList();
                }

                var isAnyActiveOrder = false;

                if (!isProductOrder)
                {
                    if (careHomeId == 0 && patientId != 0)
                    {
                        if (orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) &&
                                               q.CareHomeId == null &&
                                               q.PatientId != null &&
                                              (bool)q.IsRushFlag == false &&
                                              (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP &&
                                              ((long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.CommunityOrder ||
                                              (long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.AutomaticOrder ||
                                              (long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.Telephonic ||
                                              (long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.Online)).Any())
                        {
                            isAnyActiveOrder = true;
                        }
                    }
                    else if (careHomeId != 0 && patientId != 0)
                    {
                        if (orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) &&
                                               q.CareHomeId != null &&
                                               q.PatientId != null && 
                                              (bool)q.IsRushFlag == false &&
                                              (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP &&
                                              ((long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.CarehomePatientOrder ||
                                              (long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.AutomaticOrder)).Any())
                        {
                            isAnyActiveOrder = true;
                        }
                    }
                    else if (careHomeId != 0)
                    {
                        if (orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) &&
                                               q.CareHomeId != null &&
                                               q.PatientId != null &&
                                              (bool)q.IsRushFlag == false &&
                                              (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP &&
                                              ((long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.CarehomePatientOrder ||
                                              (long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.AutomaticOrder)).Any())
                        {
                            isAnyActiveOrder = true;
                        }
                    }
                }
                else
                {
                    if (careHomeId != 0)
                    {
                        if (orders.Where(q => (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP) &&
                                              (bool)q.IsRushFlag == false &&
                                              (long)q.OrderType == (long)SCAEnums.OrderType.ZHDH &&
                                              (long)q.OrderCreationType == (long)SCAEnums.OrderCreationType.CarehomeProductOrder).Any())
                            isAnyActiveOrder = true;
                    }
                }

                return isAnyActiveOrder;
            }
            finally
            {
                if (patientOrderRepository != null)
                {
                    patientOrderRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets Patient's Basic Details
        /// </summary>
        /// <param name="patientId">Patient's Id</param>
        /// <param name="careHomeId"></param>
        /// <param name="userId">User's Id</param>
        /// <returns>
        /// List of PatientBusinessModel
        /// </returns>
        public List<PatientBusinessModel> GetPatientBasicDetails(string patientId, string careHomeId, string isProductOrder, string userId)
        {
            IPatientRepository patientRepository = null;
            ICareHomeRepository careHomeRepository = null;
            try
            {

                var isAnyActiveOrder = HasAnyActiveOrder(Convert.ToInt64(patientId, CultureInfo.InvariantCulture), Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture), Convert.ToBoolean(isProductOrder, CultureInfo.InvariantCulture));
                if (!string.IsNullOrWhiteSpace(careHomeId) && careHomeId != "0")
                {
                    careHomeRepository = new CareHomeRepository();
                    var careHomes = careHomeRepository.GetCareHome(Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture));
                    if (careHomes.Any())
                    {
                        var careHome = careHomes.FirstOrDefault();
                        return new List<PatientBusinessModel>()
                        { 
                            new PatientBusinessModel()
                            {
                                PatientId = 0,
                                NextDeliveryDate = careHome.NextDeliveryDate.HasValue ? Convert.ToDateTime(careHome.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture) : "",
                                PatientTypeId = null,
                                RoundId = careHome.RoundId,
                                DeliveryDate = "",
                                DeliveryFrequency =  Convert.ToString(Convert.ToInt64(careHome.DeliveryFrequency, CultureInfo.InvariantCulture) * 7, CultureInfo.InvariantCulture),
                                AnyActiveOrder = isAnyActiveOrder,
                                CareHomeStatus = careHome.CareHomeStatus,
                                CareHomeOrderType = careHome.OrderType
                            }
                        };
                    }
                }
                else if (!string.IsNullOrWhiteSpace(patientId))
                {
                    patientRepository = new PatientRepository();
                    var patients = patientRepository.GetPatient(Convert.ToInt64(patientId, CultureInfo.InvariantCulture)).ToList();

                    if (patients.Any())
                    {
                        return patients.Select(q => new PatientBusinessModel
                        {
                            PatientId = q.PatientId,
                            NextDeliveryDate = q.CareHome != null ? Convert.ToDateTime(q.CareHome.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture) : Convert.ToDateTime(q.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                            PatientTypeId = q.PatientType,
                            RoundId = q.RoundId,
                            DeliveryDate = Convert.ToDateTime(q.DeliveryDate == null ? DateTime.Today : q.DeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                            DeliveryFrequency = Convert.ToString(Convert.ToInt64(q.DeliveryFrequency, CultureInfo.InvariantCulture) * 7, CultureInfo.InvariantCulture),
                            AnyActiveOrder = isAnyActiveOrder,
                            CareHomeStatus = q.CareHome != null ? q.CareHome.CareHomeStatus : 0,
                            CareHomeOrderType = q.CareHome != null ? q.CareHome.OrderType : 0
                        }).ToList();
                    }
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }

                if (careHomeRepository != null)
                {
                    careHomeRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Get the patient details based on PatientId
        /// </summary>
        /// <param name="objPatientBusinessModel">Instance of PatientBusinessModel</param>
        /// <returns>
        /// Patient BusinessModel
        /// </returns>
        public PatientBusinessModel GetPatientDetails(PatientBusinessModel objPatientBusinessModel)
        {
            try
            {
                if (objPatientBusinessModel == null)
                {
                    return null;
                }

                var patientDetails = (new DBHelper()).GetPatientDetails(objPatientBusinessModel.PatientId);
                return patientDetails.Select(patientdata => new PatientBusinessModel()
                {
                    Title = patientdata.Title,
                    PatientId = patientdata.PatientId,
                    SAPPatientNumber = !string.IsNullOrEmpty(patientdata.SAPPatientNumber) ? patientdata.SAPPatientNumber.TrimStart(new char[] { '0' }) : patientdata.SAPPatientNumber,
                    DeliveryDate = Convert.ToString(patientdata.DeliveryDate, CultureInfo.InvariantCulture),
                    NextDeliveryDate = Convert.ToString(patientdata.NextDeliveryDate, CultureInfo.InvariantCulture),
                    PatientTypeId = patientdata.PatientType,
                    PatientStatusId = patientdata.PatientStatus,
                    ReasonCodeId = patientdata.ReasonCodeId,
                    CommunicationFormat = patientdata.CommunicationFormat,
                    TitleId = patientdata.TitleId,
                    AssessmentDate = Convert.ToString(patientdata.AssessmentDate, CultureInfo.InvariantCulture),
                    NextAssessmentDate = Convert.ToString(patientdata.NextAssessmentDate, CultureInfo.InvariantCulture),
                    RoundId = patientdata.RoundId,
                    Round = patientdata.PatientRound,
                    FirstName = patientdata.FirstName,
                    LastName = patientdata.LastName,
                    DateOfBirth = Convert.ToString(patientdata.DateofBirth, CultureInfo.InvariantCulture),
                    Age = patientdata.Age,
                    Gender = Convert.ToInt64(patientdata.Gender, CultureInfo.InvariantCulture),
                    CreatedDate = Convert.ToString(patientdata.CreatedDate, CultureInfo.InvariantCulture),
                    CreatedBy = patientdata.CreatedByName,
                    NHSId = patientdata.NHSId,
                    LocalId = patientdata.LocalId,
                    PhoneNo = Convert.ToString(patientdata.TelephoneNumber, CultureInfo.InvariantCulture),
                    MobileNo = Convert.ToString(patientdata.MobileNumber, CultureInfo.InvariantCulture),
                    EmailAddress = patientdata.Email,
                    DeliveryFrequency = patientdata.DeliveryFrequency,
                    CareHomeName = patientdata.CareHomeName,
                    CareHomeId = patientdata.CareHomeId,
                    HouseNumber = patientdata.HouseNumber,
                    HouseName = patientdata.HouseName,
                    Address1 = patientdata.AddressLine1,
                    Address2 = patientdata.AddressLine2,
                    City = patientdata.City,
                    PostCode = patientdata.PostCode,
                    County = patientdata.County,
                    Country = patientdata.Country,
                    ADP1 = patientdata.ADP1,
                    ADP2 = patientdata.ADP2,
                    SAPCustomerNumber = !string.IsNullOrEmpty(patientdata.SAPCustomerNumber) ? patientdata.SAPCustomerNumber.TrimStart(new char[] { '0' }) : patientdata.SAPCustomerNumber,
                    CareHomeNextDeliveryDate = Convert.ToString(patientdata.CareHomeNextDeliveryDate, CultureInfo.InvariantCulture),
                    CustomerId = patientdata.CustomerID,
                    AssignedCustomer = Convert.ToString(patientdata.CustomerFirstName, CultureInfo.InvariantCulture) + " " + Convert.ToString(patientdata.CustomerLastName, CultureInfo.InvariantCulture),
                    CareHomeStatus = patientdata.CareHomeStatus,
                    CorrespondenceSameAsDelivery = Convert.ToBoolean(patientdata.CorrespondenceSameAsDelivery, CultureInfo.InvariantCulture),
                    CorrespondenceCareHomeName = patientdata.CorrespondenceCareHomeName,
                    CorrespondenceAddress1 = patientdata.CorrespondenceAddress1,
                    CorrespondenceAddress2 = patientdata.CorrespondenceAddress2,
                    CorrespondenceTownOrCity = patientdata.CorrespondenceTownOrCity,
                    CorrespondencePostCode = patientdata.CorrespondencePostCode,
                    CorrespondenceCounty = patientdata.CorrespondenceCounty,
                    CorrespondenceCountry = patientdata.Country,
                    CommunicationPreferences = patientdata.CommunicationPreferences != null ? patientdata.CommunicationPreferences.Split(',') : new string[] { },
                    MarketingPreferences = patientdata.MarketingPreferences != null ? patientdata.MarketingPreferences.Split(',') : new string[] { },
                    IsDataProtected = patientdata.IsDataProtected,
                    CareHomeSAPId = string.IsNullOrEmpty(patientdata.CareHomeSAPNumber) ? string.Empty : CommonHelper.TrimLeadingZeros(patientdata.CareHomeSAPNumber),
                    RemovedStoppedDateTime = Convert.ToString(patientdata.RemovedStoppedDateTime, CultureInfo.InvariantCulture)
                }).SingleOrDefault();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, Convert.ToString(objPatientBusinessModel.UserId, CultureInfo.InvariantCulture));
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, Convert.ToString(objPatientBusinessModel.UserId, CultureInfo.InvariantCulture));
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, Convert.ToString(objPatientBusinessModel.UserId, CultureInfo.InvariantCulture));
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, Convert.ToString(objPatientBusinessModel.UserId, CultureInfo.InvariantCulture));
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, Convert.ToString(objPatientBusinessModel.UserId, CultureInfo.InvariantCulture));
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, Convert.ToString(objPatientBusinessModel.UserId, CultureInfo.InvariantCulture));
                throw;
            }

            return null;
        }

        /// <summary>
        /// Gets the Search Results in Medical Staff Popup
        /// </summary>
        /// <param name="medicalStaffAnalysisInfoBusinessModel"></param>
        /// <returns>
        /// List (MedicalStaffAnalysisInfoBusinessModel)
        /// </returns>
        public List<MedicalStaffAnalysisInfoBusinessModel> GetStaffSearchResult(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel)
        {
            ///Get User Id
            string userId = Convert.ToString(medicalStaffAnalysisInfoBusinessModel.UserId, CultureInfo.InvariantCulture);
            long listId = Convert.ToInt64(medicalStaffAnalysisInfoBusinessModel.ListId, CultureInfo.InvariantCulture);
            long customerIdValue = Convert.ToInt64(medicalStaffAnalysisInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture);

            string txtSearch = medicalStaffAnalysisInfoBusinessModel.TextSearch;
            var medicalStaff = new List<MedicalStaffAnalysisInfoBusinessModel>();

            IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository = null;
            IClinicalContactValueMappingRepository clinicalContactValueMappingRepository = null;
            try
            {
                idxCustomerMedicalStaffAnalysisInfoRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();
                clinicalContactValueMappingRepository = new ClinicalContactValueMappingRepository();

                if (medicalStaffAnalysisInfoBusinessModel.IsClinicalContactWithLinkage && medicalStaffAnalysisInfoBusinessModel.ListTypeId == (int)SCAEnums.ListType.MedicalStaff)
                {
                    /// Get All Clinical Contact Linkage data as per Customer Id
                    List<ClinicalContactValueMapping> allCustomerValueMapping = clinicalContactValueMappingRepository.GetAllClinicalContactValueMapping()
                        .Where(q => q.ClinicalContactRoleMapping.CustomerId == customerIdValue && q.ChildListId == listId && q.ClinicalValueMaster.IsRemoved == false).ToList();

                    medicalStaff = allCustomerValueMapping.Where(q =>
                        (q.ClinicalValueMaster.ContactName.ToUpper().Contains(txtSearch.ToUpper())
                        || q.ClinicalValueMaster.ClinicalValueMasterId.ToString(CultureInfo.InvariantCulture).Contains(txtSearch)))
                         .Select(s => new MedicalStaffAnalysisInfoBusinessModel()
                         {
                             ListId = s.ChildListId,
                             ListTypeId = medicalStaffAnalysisInfoBusinessModel.ListTypeId,
                             StaffName = s.ClinicalValueMaster.ContactName,
                             CustomerMedicalStaffAnalysisInfoId = s.ClinicalValueMaster.ClinicalValueMasterId,
                             CustomerName = string.Empty,
                             PatientName = string.Empty
                         }).ToList();
                }
                else
                {
                    var staffResults = idxCustomerMedicalStaffAnalysisInfoRepository.GetCustomerMedicalStaffAnalysisInfoByListID(Convert.ToInt32(listId, CultureInfo.CurrentCulture)).Where(q => q.IsRemoved == false).ToList().FindAll(d => d.IsFieldType != true);
                    if (txtSearch != "-1")
                    {
                        medicalStaff = staffResults.Where(staff => staff.CustomerID == customerIdValue
                                        && (staff.ContactName.ToUpper().Contains(txtSearch.ToUpper()) || staff.IDXCustomerMedicalStaffAnalysisInfoID.ToString(CultureInfo.InvariantCulture).Contains(txtSearch))).Select(staff => new MedicalStaffAnalysisInfoBusinessModel
                                        {
                                            StaffName = staff.ContactName,
                                            ListId = staff.MedicalAnalysisListID,
                                            CustomerId = staff.CustomerID,
                                            CustomerMedicalStaffAnalysisInfoId = staff.IDXCustomerMedicalStaffAnalysisInfoID,
                                            IsFieldType = staff.IsFieldType
                                        }).ToList().FindAll(d => d.IsFieldType != true);
                    }
                    else
                    {
                        medicalStaff = staffResults.Where(staff => staff.CustomerID == customerIdValue).Select(staff => new MedicalStaffAnalysisInfoBusinessModel
                                        {
                                            StaffName = staff.ContactName,
                                            ListId = staff.MedicalAnalysisListID,
                                            CustomerId = staff.CustomerID,
                                            CustomerMedicalStaffAnalysisInfoId = staff.IDXCustomerMedicalStaffAnalysisInfoID,
                                            IsFieldType = staff.IsFieldType
                                        }).ToList().FindAll(d => d.IsFieldType != true); ;
                    }
                }

                /// Insert the blank Row
                var blankrow = new MedicalStaffAnalysisInfoBusinessModel { CustomerId = 0, StaffName = " ", ListId = Convert.ToInt64(medicalStaffAnalysisInfoBusinessModel.ListId, CultureInfo.InvariantCulture), CustomerMedicalStaffAnalysisInfoId = 0 };
                medicalStaff.Insert(0, blankrow);

                return medicalStaff;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (idxCustomerMedicalStaffAnalysisInfoRepository != null)
                {
                    idxCustomerMedicalStaffAnalysisInfoRepository.Dispose();
                }

                if (clinicalContactValueMappingRepository != null)
                {
                    clinicalContactValueMappingRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Load the Patient notes
        /// </summary>
        /// <param name="objPatientNotesBusinessModel">The patientnote business model.</param>
        /// <returns>
        /// List (PatientNotesBusinessModel)
        /// </returns>
        public List<PatientNotesBusinessModel> LoadPatientNotes(PatientNotesBusinessModel objPatientNotesBusinessModel)
        {
            var responseNotes = new List<PatientNotesBusinessModel>();
            IUsersRepository usersRepository = null;
            try
            {
                /// Code for Customer Parameter Rules (Sample Code)
                /// ICustomerParameterRepository customParameterRepository = new CustomerParameterRepository();
                /// var objCustomerParametersRules = customParameterRepository.GetAllCustomerParameters();
                /// End
                if (objPatientNotesBusinessModel == null)
                {
                    return null;
                }

                if (objPatientNotesBusinessModel.PatientId != 0)
                {
                    usersRepository = new UsersRepository();
                    var notesList = new DBHelper().GetNoteDetailsForPatient(objPatientNotesBusinessModel.PatientId);

                    foreach (var note in notesList)
                    {
                        var objPatientNoteBusinessModel = new PatientNotesBusinessModel();
                        var mappedUserName = usersRepository.GetUsersByUserId(Convert.ToString(note.CreatedBy, CultureInfo.InvariantCulture)).Select(user => user.UserName).FirstOrDefault();
                        mappedUserName = mappedUserName == ConfigurationManager.AppSettings["DefaultUserId"].ToString(CultureInfo.InvariantCulture) ? "System" : mappedUserName;
                        if (!string.IsNullOrEmpty(mappedUserName))
                        {
                            objPatientNoteBusinessModel.PatientId = Convert.ToInt64(note.PatientId, CultureInfo.InvariantCulture);
                            objPatientNoteBusinessModel.CreatedBy = mappedUserName;
                            objPatientNoteBusinessModel.CreatedDate = note.CreatedDate.ToShortDateString();
                            objPatientNoteBusinessModel.FormattedNote = "<strong>" + mappedUserName + "   " + note.CreatedDate.ToString(DateTimeFormat, CultureInfo.InvariantCulture) + "</strong>" + Environment.NewLine + Environment.NewLine + note.Note + Environment.NewLine + Environment.NewLine;
                        }

                        responseNotes.Add(objPatientNoteBusinessModel);
                    }
                }

                return responseNotes;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (usersRepository != null)
                {
                    usersRepository.Dispose();
                }
            }
            return null;
        }

        /// <summary>
        /// function to get prescription details
        /// </summary>
        /// <param name="objPrescriptionBusinessModel">The object prescription business model.</param>
        /// <returns>
        /// List (PrescriptionBusinessModel)
        /// </returns>
        public List<PrescriptionBusinessModel> PrescriptionDetails(PrescriptionBusinessModel objPrescriptionBusinessModel)
        {
            IPrescriptionRepository prescriptionRepository = null;
            try
            {
                prescriptionRepository = new PrescriptionRepository();
                IUnitOfWork unitOfWork = prescriptionRepository.UnitOfWork;

                var prescriptionDetails = prescriptionRepository.GetPatientPrescriptions((long)objPrescriptionBusinessModel.PatientID, (bool)objPrescriptionBusinessModel.IsRemoved).ToList();
				if (prescriptionDetails.Any())
				{
					foreach (var prescriptionItem in prescriptionDetails)
					{
						if (prescriptionItem.ValidToDate < DateTime.Now && prescriptionItem.IsRemoved != true)
						{
							prescriptionItem.Status = Convert.ToString(SCAEnums.PrescriptionStatus.Removed, CultureInfo.InvariantCulture);
							prescriptionItem.IsRemoved = true;
							prescriptionItem.PrescriptionAuthorizationApprovalFlag = false;
						}
					}				

					unitOfWork.Commit();				
				}			
                return prescriptionDetails.Select(res => new PrescriptionBusinessModel
                {
                    IDXPrescriptionProductId = res.IDXPrescriptionProductId,
                    ProductId = res.Product.ProductId,
                    ProductDisplayId = CommonHelper.TrimProductId(res.Product.SAPProductID),
                    ProductName = res.Product.DescriptionUI,
                    AssessedPadsPerDay = res.AssessedPadsPerDay,
                    ActualPadsPerDay = res.ActualPadsPerDay,
                    DQ1 = res.Del1,
                    DQ2 = res.Del2,
                    DQ3 = res.Del3,
                    DQ4 = res.Del4,
                    FlagDQ1 = res.FlagDQ1,
                    FlagDQ2 = res.FlagDQ2,
                    FlagDQ3 = res.FlagDQ3,
                    FlagDQ4 = res.FlagDQ4,
                    Frequency = res.Frequency,
                    Status = res.Status,
                    ValidFromDate = Convert.ToDateTime(res.ValidFromDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                    ValidToDate = Convert.ToDateTime(res.ValidToDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                    PrescriptionAuthorizationApprovalFlag = res.PrescriptionAuthorizationApprovalFlag,
                    ProductApprovalFlag = res.ProductApprovalFlag,
                    RowStatus = "Old",
                    SalesUnit = res.Product.SalesUnit,
                    DeliveryDate = Convert.ToDateTime(res.DeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture),
                    ActualPPD = res.ActPPD,
                    IsRemoved = res.IsRemoved,
                    IsOverridden = res.IsOverridenFlag,
                    ChangeNDD = (!string.IsNullOrEmpty(Convert.ToString(res.NextDeliveryDate, CultureInfo.InvariantCulture))) ? Convert.ToDateTime(res.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture) : string.Empty,
                    PackSize = res.Product.PackSize
                }).ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (prescriptionRepository != null)
                {
                    prescriptionRepository.Dispose();
                    prescriptionRepository = null;
                }
            }

            return null;
        }

        #region Contact Info
        /// <summary>
        /// Function to get contact info
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">The object patient contact information business model.</param>
        /// <returns></returns>
        public List<PatientContactInfoBusinessModel> GetContactInfo(PatientContactInfoBusinessModel objPatientContactInfoBusinessModel)
        {
            if (objPatientContactInfoBusinessModel == null)
            {
                return null;
            }

            string userId = objPatientContactInfoBusinessModel.UserID;
            ContactPersonRepository contactPersonRepository = null;
            try
            {
                if (objPatientContactInfoBusinessModel != null)
                {
                    contactPersonRepository = new ContactPersonRepository();
                    var personalInformationDetails = new List<PatientContactInfoBusinessModel>();
                    var allContactPerson = new List<ContactPerson>();
                    if (!string.IsNullOrEmpty(objPatientContactInfoBusinessModel.PatientID))
                    {
                        long? patientId = Convert.ToInt64(objPatientContactInfoBusinessModel.PatientID, CultureInfo.InvariantCulture);
                        allContactPerson = contactPersonRepository.All.Where(x => x.PatientId == patientId).ToList();
                    }

                    if (!string.IsNullOrEmpty(objPatientContactInfoBusinessModel.CareHomeId))
                    {
                        long? careHomeId = Convert.ToInt64(objPatientContactInfoBusinessModel.CareHomeId, CultureInfo.InvariantCulture);
                        allContactPerson = contactPersonRepository.All.Where(x => x.CarehomeId == careHomeId).ToList();
                    }

                    if (!string.IsNullOrEmpty(objPatientContactInfoBusinessModel.CustomerId))
                    {
                        long? customerId = Convert.ToInt64(objPatientContactInfoBusinessModel.CustomerId, CultureInfo.InvariantCulture);
                        allContactPerson = contactPersonRepository.All.Where(x => x.CustomerId == customerId).ToList();
                    }

                    personalInformationDetails = allContactPerson.Select(q => new PatientContactInfoBusinessModel
                    {
                        PersonalInformationId = Convert.ToInt64(q.PersonalInformationId, CultureInfo.InvariantCulture),
                        FirstName = q.PersonalInformation.FirstName,
                        PatientName = q.PersonalInformation.FirstName + " " + q.PersonalInformation.LastName,
                        JobTitle = q.PersonalInformation.JobTitle,
                        TelephoneNumber = q.PersonalInformation.TelephoneNumber,
                        MobileNumber = q.PersonalInformation.MobileNumber,
                        Email = q.PersonalInformation.Email,
                        FaxNumber = q.PersonalInformation.FaxNumber,
                        ContactPersonId = q.ContactPersonId
                    }).ToList();
                    return personalInformationDetails;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (contactPersonRepository != null)
                {
                    contactPersonRepository.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Function to remove contact info
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">The patient contact information.</param>
        /// <returns>
        /// boolean value
        /// </returns>
        public bool RemoveContactInfo(PatientContactInfoBusinessModel objPatientContactInfoBusinessModel)
        {
            IContactPersonRepository contactPersonRepository = new ContactPersonRepository();
            IUnitOfWork unitOfWork = contactPersonRepository.UnitOfWork;
            try
            {
                long personalInformationID = 0;

                if (objPatientContactInfoBusinessModel != null)
                {
                    globalUserId = objPatientContactInfoBusinessModel.UserID;
                    objPatientContactInfoBusinessModel.PersonalInformationIds.ForEach(q =>
                    {
                        personalInformationID = Convert.ToInt64(q, CultureInfo.InvariantCulture);
                        var objContactPerson = contactPersonRepository.All.Where(r => r.PersonalInformationId == personalInformationID).FirstOrDefault();
                        contactPersonRepository.Delete(objContactPerson.ContactPersonId);
                        contactPersonRepository.DeletePersonalInfo(personalInformationID);
                    });
                    unitOfWork.Commit();
                    return true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (contactPersonRepository != null)
                {
                    contactPersonRepository.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Function to save patient contact info
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">The patient contact information.</param>
        /// <returns>
        /// boolean value
        /// </returns>
        public bool SaveContactInfo(PatientContactInfoBusinessModel objPatientContactInfoBusinessModel)
        {
            var contactPersonalInfo = new PersonalInformation();
            var contactInfo = new ContactPerson();

            Guid userId = Guid.Parse(objPatientContactInfoBusinessModel.UserID);
            var currentTimestamp = DateTime.Now;
            var logUserId = objPatientContactInfoBusinessModel.UserID;

            Model.Customer currentCustomer = new Model.Customer();
            Model.CareHome currentCarehome = new Model.CareHome();
            Model.Patient currentPatient = new Model.Patient();

            IContactPersonRepository contactPersonRepository = null;
            ICustomerRepository customerRepository = null;
            ICareHomeRepository careHomeRepository = null;
            IPatientRepository patientRepository = null;

            try
            {
                contactPersonRepository = new ContactPersonRepository();
                customerRepository = new CustomerRepository();
                careHomeRepository = new CareHomeRepository();
                patientRepository = new PatientRepository();

                ///Get the parent entity i.e. Customer/CareHome/Patient and update it's ModifiedDate
                #region Update Existing ContactInfo
                GetContactInfoBaseEntity(objPatientContactInfoBusinessModel, ref contactPersonalInfo, ref contactInfo, userId, currentTimestamp, ref currentCustomer, ref currentCarehome, ref currentPatient, customerRepository, careHomeRepository, patientRepository);

                #endregion

                #region Convert BusinessMode to Entity Model

                SetContactInfo(objPatientContactInfoBusinessModel, contactPersonalInfo, contactInfo, userId, currentTimestamp);

                #endregion

                if (!string.IsNullOrEmpty(objPatientContactInfoBusinessModel.CustomerId))
                {
                    var customerUnitofWork = customerRepository.UnitOfWork;
                    currentCustomer.ContactPersons.Add(contactInfo);
                    currentCustomer.ModifiedDate = currentTimestamp;
                    customerUnitofWork.Commit();
                    return true;
                }

                if (!string.IsNullOrEmpty(objPatientContactInfoBusinessModel.CareHomeId))
                {
                    var careHomeUnitofWork = careHomeRepository.UnitOfWork;
                    currentCarehome.ContactPersons.Add(contactInfo);
                    currentPatient.ModifiedDate = currentTimestamp;
                    careHomeUnitofWork.Commit();
                    return true;
                }

                if (!string.IsNullOrEmpty(objPatientContactInfoBusinessModel.PatientID))
                {
                    var patientUnitofWork = patientRepository.UnitOfWork;
                    currentPatient.ContactPersons.Add(contactInfo);
                    currentPatient.ModifiedDate = currentTimestamp;
                    patientUnitofWork.Commit();
                    return true;
                }

                return false;

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, logUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, logUserId);
                throw;
            }
            finally
            {
                if (contactPersonRepository != null)
                {
                    contactPersonRepository.Dispose();
                }

                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }

                if (careHomeRepository != null)
                {
                    careHomeRepository.Dispose();
                }

                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
            }
            return false;
        }


        /// <summary>
        /// Function to save patient contact info for copy patient
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">The patient contact information.</param>
        /// <returns>
        /// boolean value
        /// </returns>
        public void SaveContactInfoForCopyPatient(List<PatientContactInfoBusinessModel> objPatientContactInfoBusinessModel)
        {
            var logUserId = "";

            IContactPersonRepository contactPersonRepository = null;
            ICustomerRepository customerRepository = null;
            ICareHomeRepository careHomeRepository = null;
            IPatientRepository patientRepository = null;

            contactPersonRepository = new ContactPersonRepository();
            customerRepository = new CustomerRepository();
            careHomeRepository = new CareHomeRepository();
            patientRepository = new PatientRepository();
            try
            {
                ///Get the parent entity i.e. Customer/CareHome/Patient and update it's ModifiedDate
                #region Update Existing ContactInfo
                
                foreach (var _contactInfo in objPatientContactInfoBusinessModel)
                {
                    var contactPersonalInfo = new PersonalInformation();
                    var contactInfo = new ContactPerson();

                    
                    Model.Customer currentCustomer = new Model.Customer();
                    Model.CareHome currentCarehome = new Model.CareHome();
                    Model.Patient currentPatient = new Model.Patient();                    

                    Guid userId = Guid.Parse(_contactInfo.UserID);
                    var currentTimestamp = DateTime.Now;                    
                    logUserId = _contactInfo.UserID;
                    contactPersonalInfo.CreatedById = userId;
                    contactPersonalInfo.CreatedDateTime = currentTimestamp;

                    if (!string.IsNullOrEmpty(_contactInfo.PatientID))
                    {
                        long patientId = Convert.ToInt64(_contactInfo.PatientID, CultureInfo.InvariantCulture);
                        currentPatient = patientRepository.Find(patientId);
                        currentPatient.ModifiedDate = currentTimestamp;
                    }
                    //GetContactInfoBaseEntity(objPatientContactInfoBusinessModel, ref contactPersonalInfo, ref contactInfo, userId, currentTimestamp, ref currentCustomer, ref currentCarehome, ref currentPatient, customerRepository, careHomeRepository, patientRepository);

                #endregion

                    #region Convert BusinessMode to Entity Model

                    //SetContactInfo(objPatientContactInfoBusinessModel, contactPersonalInfo, contactInfo, userId, currentTimestamp);
                    
                    contactPersonalInfo.FirstName = _contactInfo.FirstName;
                    contactPersonalInfo.LastName = _contactInfo.LastName;
                    contactPersonalInfo.JobTitle = _contactInfo.JobTitle;
                    contactPersonalInfo.TelephoneNumber = _contactInfo.TelephoneNumber;
                    contactPersonalInfo.MobileNumber = _contactInfo.MobileNumber;
                    contactPersonalInfo.Email = _contactInfo.Email;
                    contactPersonalInfo.ModifiedBy = userId;
                    contactPersonalInfo.ModifiedDate = currentTimestamp;
                    contactInfo.PatientId = _contactInfo.PatientID != null ? Convert.ToInt64(_contactInfo.PatientID, CultureInfo.InvariantCulture) : (long?)null;                    
                    contactInfo.ModifiedBy = userId;
                    contactInfo.ModifiedDate = currentTimestamp;
                    contactInfo.PersonalInformation = contactPersonalInfo;
                    #endregion

                    if (!string.IsNullOrEmpty(_contactInfo.PatientID))
                    {
                        var patientUnitofWork = patientRepository.UnitOfWork;
                        currentPatient.ContactPersons.Add(contactInfo);
                        currentPatient.ModifiedDate = currentTimestamp;
                        patientUnitofWork.Commit();
                    }
                }                
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, logUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, logUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, logUserId);
                throw;
            }
            finally
            {
                if (contactPersonRepository != null)
                {
                    contactPersonRepository.Dispose();
                }

                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }

                if (careHomeRepository != null)
                {
                    careHomeRepository.Dispose();
                }

                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets the contact information base entity.
        /// </summary>
        /// <param name="patientContactInfo">The patient contact information.</param>
        /// <param name="contactPersonalInfo">The contact personal information.</param>
        /// <param name="contactInfo">The contact information.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="currentTimestamp">The current timestamp.</param>
        /// <param name="currentCustomer">The current customer.</param>
        /// <param name="currentCarehome">The current carehome.</param>
        /// <param name="currentPatient">The current patient.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="careHomeRepository">The care home repository.</param>
        /// <param name="patientRepository">The patient repository.</param>
        private static void GetContactInfoBaseEntity(PatientContactInfoBusinessModel patientContactInfo, ref PersonalInformation contactPersonalInfo, ref ContactPerson contactInfo, Guid userId, DateTime currentTimestamp, ref Model.Customer currentCustomer, ref Model.CareHome currentCarehome, ref Model.Patient currentPatient, ICustomerRepository customerRepository, ICareHomeRepository careHomeRepository, IPatientRepository patientRepository)
        {
            if (patientContactInfo.ContactPersonId != default(long))
            {
                if (!string.IsNullOrEmpty(patientContactInfo.CustomerId))
                {
                    long customerId = Convert.ToInt64(patientContactInfo.CustomerId, CultureInfo.InvariantCulture);
                    currentCustomer = customerRepository.GetCustomerById(customerId);
                    currentCustomer.ModifiedDate = currentTimestamp;
                    contactInfo = currentCustomer.ContactPersons.Where(q => q.ContactPersonId == patientContactInfo.ContactPersonId).FirstOrDefault();
                    contactPersonalInfo = contactInfo.PersonalInformation;
                }

                if (!string.IsNullOrEmpty(patientContactInfo.CareHomeId))
                {
                    long careHomeId = Convert.ToInt64(patientContactInfo.CareHomeId, CultureInfo.InvariantCulture);
                    currentCarehome = careHomeRepository.GetCareHome(careHomeId).FirstOrDefault();
                    currentCarehome.ModifiedDate = currentTimestamp;
                    contactInfo = currentCarehome.ContactPersons.Where(q => q.ContactPersonId == patientContactInfo.ContactPersonId).FirstOrDefault();
                    contactPersonalInfo = contactInfo.PersonalInformation;
                }

                if (!string.IsNullOrEmpty(patientContactInfo.PatientID))
                {
                    long patientId = Convert.ToInt64(patientContactInfo.PatientID, CultureInfo.InvariantCulture);
                    currentPatient = patientRepository.GetPatient(patientId).FirstOrDefault();
                    currentPatient.ModifiedDate = currentTimestamp;
                    contactInfo = currentPatient.ContactPersons.Where(q => q.ContactPersonId == patientContactInfo.ContactPersonId).FirstOrDefault();
                    contactPersonalInfo = contactInfo.PersonalInformation;
                }
            }
        #endregion

            #region Add new ContactInfo

            else
            {

                contactPersonalInfo.CreatedById = userId;
                contactPersonalInfo.CreatedDateTime = currentTimestamp;
                if (!string.IsNullOrEmpty(patientContactInfo.CustomerId))
                {
                    long customerId = Convert.ToInt64(patientContactInfo.CustomerId, CultureInfo.InvariantCulture);
                    currentCustomer = customerRepository.Find(customerId);
                    currentCustomer.ModifiedDate = currentTimestamp;
                }

                if (!string.IsNullOrEmpty(patientContactInfo.CareHomeId))
                {
                    long careHomeId = Convert.ToInt64(patientContactInfo.CareHomeId, CultureInfo.InvariantCulture);
                    currentCarehome = careHomeRepository.Find(careHomeId);
                    currentCarehome.ModifiedDate = currentTimestamp;
                }

                if (!string.IsNullOrEmpty(patientContactInfo.PatientID))
                {
                    long patientId = Convert.ToInt64(patientContactInfo.PatientID, CultureInfo.InvariantCulture);
                    currentPatient = patientRepository.Find(patientId);
                    currentPatient.ModifiedDate = currentTimestamp;
                }
            }
        }

        /// <summary>
        /// Sets the contact information.
        /// </summary>
        /// <param name="patientContactInfo">The patient contact information.</param>
        /// <param name="contactPersonalInfo">The contact personal information.</param>
        /// <param name="contactInfo">The contact information.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="currentTimestamp">The current timestamp.</param>
        private static void SetContactInfo(PatientContactInfoBusinessModel patientContactInfo, PersonalInformation contactPersonalInfo, ContactPerson contactInfo, Guid userId, DateTime currentTimestamp)
        {
            contactPersonalInfo.FirstName = patientContactInfo.FirstName;
            contactPersonalInfo.LastName = patientContactInfo.LastName;
            contactPersonalInfo.JobTitle = patientContactInfo.JobTitle;
            contactPersonalInfo.TelephoneNumber = patientContactInfo.TelephoneNumber;
            contactPersonalInfo.MobileNumber = patientContactInfo.MobileNumber;
            contactPersonalInfo.Email = patientContactInfo.Email;
            contactPersonalInfo.ModifiedBy = userId;
            contactPersonalInfo.ModifiedDate = currentTimestamp;
            contactInfo.PatientId = patientContactInfo.PatientID != null ? Convert.ToInt64(patientContactInfo.PatientID, CultureInfo.InvariantCulture) : (long?)null;
            contactInfo.CarehomeId = patientContactInfo.CareHomeId != null ? Convert.ToInt64(patientContactInfo.CareHomeId, CultureInfo.InvariantCulture) : (long?)null;
            contactInfo.CustomerId = patientContactInfo.CustomerId != null ? Convert.ToInt64(patientContactInfo.CustomerId, CultureInfo.InvariantCulture) : (long?)null;
            contactInfo.ModifiedBy = userId;
            contactInfo.ModifiedDate = currentTimestamp;
            contactInfo.PersonalInformation = contactPersonalInfo;
        }


            #endregion

        /// <summary>
        /// Adds the med staff analysis information.
        /// </summary>
        /// <param name="objMedicalStaffAnalysisInfoBusinessModel">The medstaff business model.</param>
        /// <returns></returns>
        public MedicalStaffAnalysisInfoBusinessModel AddMedStaffAnalysisInfo(MedicalStaffAnalysisInfoBusinessModel objMedicalStaffAnalysisInfoBusinessModel)
        {
            IIDXCustomerMedicalStaffAnalysisInfoRepository idxCustomerMedicalStaffAnalysisInfoRepository = new IDXCustomerMedicalStaffAnalysisInfoRepository();
            UnitOfWork unitOfWork = idxCustomerMedicalStaffAnalysisInfoRepository.UnitOfWork;

            try
            {
                IDXCustomerMedicalStaffAnalysisInfo MedStaff = new IDXCustomerMedicalStaffAnalysisInfo();
                if (objMedicalStaffAnalysisInfoBusinessModel == null)
                {
                    return null;

                }
                MedStaff.CustomerID = objMedicalStaffAnalysisInfoBusinessModel.CustomerId;
                MedStaff.MedicalAnalysisListID = objMedicalStaffAnalysisInfoBusinessModel.ListId;
                MedStaff.ContactName = objMedicalStaffAnalysisInfoBusinessModel.StaffName;
                MedStaff.ModifiedBy = objMedicalStaffAnalysisInfoBusinessModel.UserId;
                MedStaff.ModifiedDate = Convert.ToDateTime(objMedicalStaffAnalysisInfoBusinessModel.ModifiedDate, CultureInfo.InvariantCulture);
                MedStaff.IsFieldType = false;

                idxCustomerMedicalStaffAnalysisInfoRepository.InsertOrUpdate(MedStaff);
                unitOfWork.Commit();
                objMedicalStaffAnalysisInfoBusinessModel.CustomerMedicalStaffAnalysisInfoId = MedStaff.IDXCustomerMedicalStaffAnalysisInfoID;
                return objMedicalStaffAnalysisInfoBusinessModel;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (idxCustomerMedicalStaffAnalysisInfoRepository != null)
                    idxCustomerMedicalStaffAnalysisInfoRepository.Dispose();
            }

            return null;
        }

        /// <summary>
        /// Save Patient details for status removed and stopped
        /// </summary>
        /// <param name="objPatientBusinessModel">object of PatientBusinessModel</param>
        /// <returns>
        /// Boolean value
        /// </returns>
        public bool SavePatientDetailsRemovedStopped(PatientBusinessModel objPatientBusinessModel)
        {
            IPatientRepository patientRepository = new PatientRepository();
            IUnitOfWork unitOfWork = patientRepository.UnitOfWork;
            try
            {
                if (objPatientBusinessModel != null)
                {
                    var patient = patientRepository.Find(objPatientBusinessModel.PatientId);
                    patient.ModifiedDate = DateTime.Now;
                    patient.ReasonCodeID = objPatientBusinessModel.ReasonCodeId;

                    if (patient.PatientStatus != objPatientBusinessModel.PatientStatusId) //if only patient status is changed to stopped or removed
                        patient.RemovedStoppedDateTime = patient.ModifiedDate;

                    patient.PatientStatus = objPatientBusinessModel.PatientStatusId;
                    patient.ModifiedBy = Guid.Parse(objPatientBusinessModel.ModifiedBy);

                    KeyValuePair<EFModel.Patient, EFModel.Order> patientOrders;
                    if (string.IsNullOrEmpty(objPatientBusinessModel.Remarks))
                    {
                        patient = this.CancelOrderRemovedStopped(patient, out patientOrders);
                        patientRepository.InsertOrUpdate(patient);
                        unitOfWork.Commit();
                        if (patientOrders.Value != null && patientOrders.Key != null)
                        {
                            new Order().UpdateNddForCanceledOrder(patientOrders.Value, patientOrders.Key);
                        }
                    }
                    else
                    {                        
                        patientRepository.InsertOrUpdate(patient);
                        unitOfWork.Commit(objPatientBusinessModel.Remarks);                        
                    }
                    return true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Check if incremental update required
        /// </summary>
        /// <param name="patientId">PatientId</param>
        /// <param name="objPatientBusinessModel">PatientBusinessModel</param>
        /// <returns></returns>
        public bool CheckIncrementalUpdateRequired(long patientId,PatientBusinessModel objPatientBusinessModel)
        {
            
            IPatientRepository patientRepository = null;
            IAddressRepository addressRepository = null;
            isIncrementalUpdateRequired = false;
            try
            {
                patientRepository = new PatientRepository();
                addressRepository = new AddressRepository();
                
                if (patientId != 0)
                {
                    var oldPatientBusinessModel = patientRepository.Find(patientId);

                    var oldDOB = oldPatientBusinessModel.DateofBirth.HasValue ? oldPatientBusinessModel.DateofBirth.Value.Date : default(DateTime);
                    var newDOB = !string.IsNullOrEmpty(objPatientBusinessModel.DateOfBirth) ? CommonHelper.ConvertDateToMMddyyyy(objPatientBusinessModel.DateOfBirth)  : default(DateTime);

                    var oldNDD = oldPatientBusinessModel.NextDeliveryDate.HasValue ? oldPatientBusinessModel.NextDeliveryDate.Value.Date : default(DateTime);
                    var newNDD = !string.IsNullOrEmpty(objPatientBusinessModel.NextDeliveryDate) ? CommonHelper.ConvertDateToMMddyyyy(objPatientBusinessModel.NextDeliveryDate) : default(DateTime);

                    var oldPostcode = "";
                    var address = addressRepository.Find(oldPatientBusinessModel.PersonalInformation.CorrespondenceAddressId ?? 0);
                    if(address != null)
                        oldPostcode = !string.IsNullOrEmpty(address.PostCode) ? address.PostCode : "";                                        

                    if ((oldPatientBusinessModel.PersonalInformation.FirstName != objPatientBusinessModel.PFirstName) ||
                        (oldPatientBusinessModel.PersonalInformation.LastName != objPatientBusinessModel.PLastName) ||
                        (oldPatientBusinessModel.PersonalInformation.TelephoneNumber != objPatientBusinessModel.PTelephoneNumber) ||
                        (oldDOB != newDOB) ||
                        (oldPatientBusinessModel.PatientStatus != objPatientBusinessModel.PatientStatusId) ||
                        (oldPatientBusinessModel.PatientType != objPatientBusinessModel.PatientTypeId) ||
                        (oldNDD != newNDD) ||
                        (oldPatientBusinessModel.PersonalInformation.Address.PostCode != objPatientBusinessModel.PostCode) ||
                        (oldPostcode != objPatientBusinessModel.CorrespondencePostCode)
                        )
                    {

                        isIncrementalUpdateRequired = true;
                    }
                    else
                    {

                        isIncrementalUpdateRequired = false;
                    }


                }
                else
                {

                    isIncrementalUpdateRequired = true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, globalUserId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }
                if (addressRepository != null)
                {
                    addressRepository.Dispose();
                }
            }
            
            return isIncrementalUpdateRequired;
        }

        /// <summary>
        /// Save PatientDetails
        /// </summary>
        /// <param name="objPatientBusinessModel">Instance of PatientBusinessModel</param>
        /// <returns>
        /// Return boolean
        /// </returns>
        public PatientBusinessModel SavePatientDetails(PatientBusinessModel objPatientBusinessModel)
        {
            IPrescriptionRepository prescriptionRepository = new PrescriptionRepository();

            try
            {

                isIncrementalUpdateRequired=CheckIncrementalUpdateRequired(objPatientBusinessModel.PatientId, objPatientBusinessModel);
                bool isChangedFromCareHomeType = false;
                if (objPatientBusinessModel == null)
                {
                    return null;
                }

                List<long> patientTypes = new List<long> { (long)SCAEnums.PatientType.Bulk, (long)SCAEnums.PatientType.ChildSelfCare, (long)SCAEnums.PatientType.Procare, (long)SCAEnums.PatientType.SelfCare, (long)SCAEnums.PatientType.CommunitySelfcareResidential, (long)SCAEnums.PatientType.CommunitySelfcareNursing, (long)SCAEnums.PatientType.PaediatricSelfcare, (long)SCAEnums.PatientType.PaediatricProcare };
                List<long> careHomePatientTypes = new List<long> { (long)SCAEnums.PatientType.Hospital, (long)SCAEnums.PatientType.ContinuingCare, (long)SCAEnums.PatientType.Nursing, (long)SCAEnums.PatientType.NursingSelfcare, (long)SCAEnums.PatientType.Residential, (long)SCAEnums.PatientType.ResidentialSelfcare };

                long minFrequency = Convert.ToInt64(objPatientBusinessModel.DeliveryFrequency, CultureInfo.InvariantCulture);
                if (objPatientBusinessModel != null && objPatientBusinessModel.PrescriptionList.Any() && patientTypes.Contains(Convert.ToInt64(objPatientBusinessModel.PatientTypeId, CultureInfo.InvariantCulture)))
                {                    
                    minFrequency = Convert.ToInt64(objPatientBusinessModel.PrescriptionList.Where(x => x.Status == "Active").Min(a => a.Frequency), CultureInfo.InvariantCulture);                    
                }

                DateTime currentDateTime = DateTime.Now;

                Guid modifiedBy = Guid.Parse(objPatientBusinessModel.ModifiedBy);
                globalUserId = objPatientBusinessModel.ModifiedBy;

                #region Prescription
                var objPrescription = SetPrescription(objPatientBusinessModel, prescriptionRepository, currentDateTime, modifiedBy);
                #endregion

                if ((!objPatientBusinessModel.PAssignedCareHome.HasValue && objPatientBusinessModel.PAssignedCareHome != 0) &&
                      objPatientBusinessModel.PrescriptionList.Any() && !objPatientBusinessModel.PrescriptionList.Where(q => q.IDXPrescriptionProductId != 0).Any())
                {
                    var orderLeadTime = "0";
					var countryId = string.Empty;
					var custId = Convert.ToString(objPatientBusinessModel.CustomerId, CultureInfo.InvariantCulture);
					if(objPrescription.Patient != null)
					{
						countryId = Convert.ToString(objPrescription.Patient.Customer.CountryId, CultureInfo.InvariantCulture);
						if (objPrescription.Patient.Customer.CustomerParameters.Any())
						{
							orderLeadTime = Convert.ToString(objPrescription.Patient.Customer.CustomerParameters.FirstOrDefault().OrderLeadTime, CultureInfo.InvariantCulture);
						}
					}
					var nddClass = new NDDCalculation();
					var parametersBusinessModel = new NDDParametersBusinessModel()
					{
						CustomerId = custId,
						RoundID = objPatientBusinessModel.RoundId,
						Round = objPatientBusinessModel.Round,
						PostCode = objPatientBusinessModel.PostCode,
						UserId = objPatientBusinessModel.UserId,
						LeadTime = orderLeadTime,
						CountryId = countryId
					};
					var nextDeliveryDate = nddClass.CalculateNDD(parametersBusinessModel).ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture);
                    objPatientBusinessModel.PrescriptionList.ForEach(q => q.ChangeNDD = nextDeliveryDate);
                    objPatientBusinessModel.NextDeliveryDate = nextDeliveryDate;
                }

                long DBPatientStatus = 0;
                if (objPrescription.Patient != null && objPrescription.Patient.PatientStatus != null)
                    DBPatientStatus = Convert.ToInt64(objPrescription.Patient.PatientStatus, CultureInfo.InvariantCulture);

                #region Patient
                var objPatient = SetPatient(objPatientBusinessModel, ref isChangedFromCareHomeType, careHomePatientTypes, minFrequency, currentDateTime, modifiedBy, objPrescription);

                #endregion

                #region PersonalInformation
                var objPersonalInformation = SetPersonalInformation(objPatientBusinessModel, currentDateTime, modifiedBy, objPatient);
                #endregion

                #region AddressDetails
                var carehomeName = SetAddress(objPatientBusinessModel, isChangedFromCareHomeType, careHomePatientTypes, currentDateTime, modifiedBy, objPersonalInformation);

                #endregion

                #region IdxPrescriptionProduct

                SetPrescription(objPatientBusinessModel, currentDateTime, modifiedBy, objPrescription);

                #endregion

                #region MedicalStaffDetails
                SetPatientMedicalStaff(objPatientBusinessModel, currentDateTime, modifiedBy, objPatient);

                #endregion

                #region AnalysisDetails
                SetPatientAnalysisInfo(objPatientBusinessModel, currentDateTime, modifiedBy, objPatient);
                #endregion

                #region NurseComments

                SetPatientNurseInfo(objPatientBusinessModel, currentDateTime, modifiedBy, objPrescription);
                #endregion


                #region Communication Preferences

                SetPreferences(objPatientBusinessModel, currentDateTime, modifiedBy, objPatient);

                #endregion
                IUnitOfWork unitOfWork = prescriptionRepository.UnitOfWork;
                KeyValuePair<EFModel.Patient, EFModel.Order> patientOrders = new KeyValuePair<EFModel.Patient, EFModel.Order>();
                if (objPatient.PatientStatus == Convert.ToInt64(SCAEnums.PatientStatus.Removed, CultureInfo.InvariantCulture) || objPatient.PatientStatus == Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture))
                {
                    objPatient = this.CancelOrderRemovedStopped(objPatient, out patientOrders);

                    if (DBPatientStatus != objPatient.PatientStatus)
                        objPatient.RemovedStoppedDateTime = objPatient.ModifiedDate;                   
                }

                prescriptionRepository.InsertOrUpdate(objPrescription);
                ///Delete address if PatientType is carehome
                if (objPatientBusinessModel.PatientId > 0 && objPersonalInformation.CorrespondenceAddressId != null && objPersonalInformation.AddressId != objPersonalInformation.CorrespondenceAddressId && objPatientBusinessModel.PAssignedCareHome > 0)
                {
                    prescriptionRepository.DeleteAddress(objPersonalInformation.CorrespondenceAddressId);
                }

                unitOfWork.Commit(objPatientBusinessModel.Remarks);
                if(isIncrementalUpdateRequired)
                { 
                    var incrementalUpdateObj=new IncrementalUpdateSearchIndex();
                    incrementalUpdateObj.CreateIncrementalUpdateIndexXml(objPatient.PatientId,(long)SCAEnums.ObjectType.Patient);
                }
                string removedStoppedDateTime = string.Empty;
                long patientId = objPatient.PatientId;
                long patientstatus = Convert.ToInt64(objPatient.PatientStatus, CultureInfo.InvariantCulture);
                if ((patientstatus == Convert.ToInt64(SCAEnums.PatientStatus.Removed, CultureInfo.InvariantCulture) || patientstatus == Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture)) && (patientOrders.Value != null && patientOrders.Key != null))
                {
                    new Order().UpdateNddForCanceledOrder(patientOrders.Value, patientOrders.Key);
                }
                removedStoppedDateTime = Convert.ToString(objPatient.RemovedStoppedDateTime, CultureInfo.InvariantCulture);
                long reasonCode = Convert.ToInt64(objPatient.ReasonCodeID, CultureInfo.InvariantCulture);
                string round = Convert.ToString(objPatient.Round, CultureInfo.InvariantCulture);
                string roundId = Convert.ToString(objPatient.RoundId, CultureInfo.InvariantCulture);

                string patientName = objPatientBusinessModel.PFirstName + " " + objPatientBusinessModel.PLastName;
                string deliveryFrequency = minFrequency.ToString(CultureInfo.InvariantCulture);
                long customerId = Convert.ToInt64(objPatient.CustomerId, CultureInfo.InvariantCulture);
                string frequencyNDD = Convert.ToString(objPatient.NextDeliveryDate, CultureInfo.InvariantCulture);
                PatientBusinessModel savedPatientDetails = new PatientBusinessModel
                {
                    PatientId = patientId,
                    CareHomeName = carehomeName,
                    PatientStatusId = patientstatus,
                    ReasonCodeId = reasonCode,
                    DeliveryFrequency = deliveryFrequency,
                    PatientName = patientName,
                    Round = round,
                    RoundId = roundId,
                    PatientSaveStatus = true,
                    CustomerId = customerId,
                    RemovedStoppedDateTime = removedStoppedDateTime,
                    NextDeliveryDate = frequencyNDD
                };

                return savedPatientDetails;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (prescriptionRepository != null)
                {
                    prescriptionRepository.Dispose();
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the preferences.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="prescriptionRepository">The prescription repository.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPatient">The object patient.</param>
        private void SetPreferences(PatientBusinessModel objPatientBusinessModel, DateTime currentDateTime, Guid modifiedBy, EFModel.Patient objPatient)
        {
            DeletePreferences(objPatient, objPatientBusinessModel);
            var patientPreferences = objPatient.IDXPreferences;
            ///Create Communication Preferences List
            if (objPatientBusinessModel.CommunicationPreferences != null)
            {
                foreach (var CommunicationFormatId in objPatientBusinessModel.CommunicationPreferences)
                {
                    var communicationFormat = Convert.ToInt64(CommunicationFormatId, CultureInfo.InvariantCulture);
                    if (!patientPreferences.Where(q => q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture) && (long)q.Preference == communicationFormat).Any())
                    {
                        var objIDXPreference = new IDXPreference();
                        objIDXPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture);
                        objIDXPreference.PatientId = objPatient.PatientId;
                        objIDXPreference.Preference = Convert.ToInt64(CommunicationFormatId, CultureInfo.InvariantCulture);
                        objIDXPreference.ModifiedBy = modifiedBy;
                        objIDXPreference.ModifiedDate = currentDateTime;
                        objPatient.IDXPreferences.Add(objIDXPreference);
                    }
                }
            }
            ///Create Marketing Preferences List
            if (objPatientBusinessModel.MarketingPreferences != null)
            {
                foreach (var marketingPereferenceId in objPatientBusinessModel.MarketingPreferences)
                {
                    var marketingPereference = Convert.ToInt64(marketingPereferenceId, CultureInfo.InvariantCulture);
                    if (!patientPreferences.Where(q => q.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture) && (long)q.Preference == marketingPereference).Any())
                    {
                        var objIDXPreference = new IDXPreference();
                        objIDXPreference.PreferenceType = Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture);
                        objIDXPreference.PatientId = objPatient.PatientId;
                        objIDXPreference.Preference = Convert.ToInt64(marketingPereferenceId, CultureInfo.InvariantCulture);
                        objIDXPreference.ModifiedBy = modifiedBy;
                        objIDXPreference.ModifiedDate = currentDateTime;
                        objPatient.IDXPreferences.Add(objIDXPreference);
                    }
                }
            }
        }

        /// <summary>
        /// Deletes the preferences.
        /// </summary>
        /// <param name="objPatient">The object patient.</param>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        private void DeletePreferences(EFModel.Patient objPatient, PatientBusinessModel objPatientBusinessModel)
        {
            //Delete CommunicationPreference 
            var selectedPreference = new List<long>();
            IDXPreference idxPreference = null;
            if (objPatientBusinessModel.CommunicationPreferences != null)
            {
                selectedPreference = new List<long>();
                var CommunicationPreferenceList = objPatient.IDXPreferences.Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.CommunicationPreference, CultureInfo.InvariantCulture)).ToList();
                objPatientBusinessModel.CommunicationPreferences.ToList().ForEach(q => selectedPreference.Add(Convert.ToInt64(q, CultureInfo.InvariantCulture)));
                var dbPreferencesList = CommunicationPreferenceList.Select(q => q.Preference.Value).ToList();
                var RemovedPreferenceId = dbPreferencesList.Except(selectedPreference).ToList();
                RemovedPreferenceId.ForEach(d =>
                {
                    idxPreference = CommunicationPreferenceList.Find(q => q.Preference == d);
                    objPatient.IDXPreferences.Remove(idxPreference);
                });
            }

            ///Delete MarketingPreferences 
            if (objPatientBusinessModel.MarketingPreferences != null)
            {
                var MarketingPreferenceList = objPatient.IDXPreferences.Where(d => d.PreferenceType == Convert.ToInt64(SCAEnums.PreferenceType.MarketingPreference, CultureInfo.InvariantCulture)).ToList();
                objPatientBusinessModel.MarketingPreferences.ToList().ForEach(q => selectedPreference.Add(Convert.ToInt64(q, CultureInfo.InvariantCulture)));
                var dbPreferencesList = MarketingPreferenceList.Select(q => q.Preference.Value).ToList();
                var RemovedPreferenceId = dbPreferencesList.Except(selectedPreference).ToList();
                RemovedPreferenceId.ForEach(d =>
                {
                    idxPreference = MarketingPreferenceList.Find(q => q.Preference == d);
                    objPatient.IDXPreferences.Remove(idxPreference);
                });
            }
        }

        /// <summary>
        /// Sets the patient nurse information.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPrescription">The object prescription.</param>
        private static void SetPatientNurseInfo(PatientBusinessModel objPatientBusinessModel, DateTime currentDateTime, Guid modifiedBy, Prescription objPrescription)
        {
            if (!string.IsNullOrEmpty(objPatientBusinessModel.NurseComment.Name))
            {
                PrescriptionExtended extendedData = new PrescriptionExtended();

                extendedData.Name = objPatientBusinessModel.NurseComment.Name;
                extendedData.MobileNumber = objPatientBusinessModel.NurseComment.MobileNumber;
                extendedData.Comments = objPatientBusinessModel.NurseComment.Comments;
                extendedData.Email = objPatientBusinessModel.NurseComment.Email;
                extendedData.ModifiedDate = currentDateTime;
                extendedData.ModifiedBy = modifiedBy;
                extendedData.RoleId = objPatientBusinessModel.NurseComment.RoleId;
                objPrescription.PrescriptionExtendeds.Add(extendedData);
            }
        }

        /// <summary>
        /// Sets the patient analysis information.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPatient">The object patient.</param>
        /// <param name="Listmedicalstaffpatientanalysis">The listmedicalstaffpatientanalysis.</param>
        private static void SetPatientAnalysisInfo(PatientBusinessModel objPatientBusinessModel, DateTime currentDateTime, Guid modifiedBy, EFModel.Patient objPatient)
        {
            var objPatientanalysisdetails = new SCA.Indigo.Model.IDXPatientMedicalStaff();

            if (objPatientBusinessModel.PatientAnalysisList != null && objPatientBusinessModel.PatientAnalysisList.Any())
            {
                foreach (var item in objPatientBusinessModel.PatientAnalysisList)
                {
                    if (item.CustomerMedicalStaffAnalysisInfoId != 0)
                    {
                        if (item.IDXPatientMedicalStaffId == 0)
                        {
                            objPatientanalysisdetails = new SCA.Indigo.Model.IDXPatientMedicalStaff()
                            {
                                CustomerMedicalStaffId = item.CustomerMedicalStaffAnalysisInfoId,
                                PatientId = objPatient.PatientId,
                                ModifiedBy = modifiedBy,
                                ModifiedDate = currentDateTime
                            };
                            objPatient.IDXPatientMedicalStaffs.Add(objPatientanalysisdetails);
                            continue;
                        }

                        var updatedvalue = objPatient.IDXPatientMedicalStaffs.FirstOrDefault(e => e.IDXPatientMedicalStaffId == item.IDXPatientMedicalStaffId &&
                                                                                                  e.CustomerMedicalStaffId != item.CustomerMedicalStaffAnalysisInfoId);
                        if (updatedvalue != null)
                        {
                            updatedvalue.CustomerMedicalStaffId = item.CustomerMedicalStaffAnalysisInfoId;
                            updatedvalue.PatientId = objPatient.PatientId;
                            updatedvalue.ModifiedBy = modifiedBy;
                            updatedvalue.ModifiedDate = currentDateTime;
                        }
                    }
                    else if (item.IDXPatientMedicalStaffId != 0)
                    {
                        var updateValue = objPatient.IDXPatientMedicalStaffs.FirstOrDefault(e => e.IDXPatientMedicalStaffId == item.IDXPatientMedicalStaffId);
                        if (updateValue != null)
                            objPatient.IDXPatientMedicalStaffs.Remove(updateValue);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the patient medical staff.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPatient">The object patient.</param>
        /// <returns></returns>
        private static void SetPatientMedicalStaff(PatientBusinessModel objPatientBusinessModel, DateTime currentDateTime, Guid modifiedBy, EFModel.Patient objPatient)
        {
            var objmedicalstaffdetails = new SCA.Indigo.Model.IDXPatientMedicalStaff();

            if (objPatientBusinessModel.MedicalstaffList != null && objPatientBusinessModel.MedicalstaffList.Any())
            {
                foreach (var item in objPatientBusinessModel.MedicalstaffList)
                {
                    if (item.CustomerMedicalStaffAnalysisInfoId != 0)
                    {
                        if (item.IDXPatientMedicalStaffId == 0)
                        {
                            objmedicalstaffdetails = new SCA.Indigo.Model.IDXPatientMedicalStaff()
                            {
                                CustomerMedicalStaffId = item.CustomerMedicalStaffAnalysisInfoId,
                                PatientId = objPatient.PatientId,
                                ModifiedBy = modifiedBy,
                                ModifiedDate = currentDateTime,
                                IsClinicalLinkage = objPatientBusinessModel.IsClinicalLinkage
                            };
                            objPatient.IDXPatientMedicalStaffs.Add(objmedicalstaffdetails);
                            continue;
                        }

                        var updateValue = objPatient.IDXPatientMedicalStaffs.FirstOrDefault(e => e.IDXPatientMedicalStaffId == item.IDXPatientMedicalStaffId &&
                                          (e.CustomerMedicalStaffId != item.CustomerMedicalStaffAnalysisInfoId || e.IsClinicalLinkage != objPatientBusinessModel.IsClinicalLinkage));
                        if (updateValue != null)
                        {
                            updateValue.CustomerMedicalStaffId = item.CustomerMedicalStaffAnalysisInfoId;
                            updateValue.PatientId = objPatient.PatientId;
                            updateValue.ModifiedBy = modifiedBy;
                            updateValue.ModifiedDate = currentDateTime;
                            updateValue.IsClinicalLinkage = objPatientBusinessModel.IsClinicalLinkage;
                        }
                    }
                    else if(item.IDXPatientMedicalStaffId != 0)
                    {
                        var updateValue = objPatient.IDXPatientMedicalStaffs.FirstOrDefault(e => e.IDXPatientMedicalStaffId == item.IDXPatientMedicalStaffId);
                        if (updateValue != null)
                            objPatient.IDXPatientMedicalStaffs.Remove(updateValue);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the prescription.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPrescription">The object prescription.</param>
        private static void SetPrescription(PatientBusinessModel objPatientBusinessModel, DateTime currentDateTime, Guid modifiedBy, Prescription objPrescription)
        {
            if (objPatientBusinessModel.PrescriptionList.Count > 0)
            {
                var lstPrescriptionProduct = objPrescription.IDXPrescriptionProducts;
                List<IDXPrescriptionProduct> idxProducts = new List<IDXPrescriptionProduct>();

                foreach (var item in objPatientBusinessModel.PrescriptionList)
                {
                    var objPrescriptionDetails = new IDXPrescriptionProduct();
                    var toRemove = new IDXPrescriptionProduct();
                    var existingIdxProducts = lstPrescriptionProduct.Where(q => q.ProductId == item.ProductId);
                    if (existingIdxProducts.Any())
                    {
                        objPrescriptionDetails = existingIdxProducts.First();
                        toRemove = existingIdxProducts.First();
                    }

                    {
                        objPrescriptionDetails.ActualPadsPerDay = item.ActualPadsPerDay;
                        objPrescriptionDetails.AssessedPadsPerDay = item.AssessedPadsPerDay;
                        objPrescriptionDetails.ProductId = item.ProductId;
                        objPrescriptionDetails.Del1 = item.DQ1;
                        objPrescriptionDetails.Del2 = item.DQ2;
                        objPrescriptionDetails.Del3 = item.DQ3;
                        objPrescriptionDetails.Del4 = item.DQ4;
                        objPrescriptionDetails.Frequency = item.Frequency;
                        objPrescriptionDetails.Status = item.Status;
                        objPrescriptionDetails.ValidFromDate = DateTime.ParseExact(item.ValidFromDate, DateFormat, CultureInfo.InvariantCulture);
                        objPrescriptionDetails.ValidToDate = DateTime.ParseExact(item.ValidToDate, DateFormat, CultureInfo.InvariantCulture);
                        objPrescriptionDetails.Description = item.ProductName;
                        objPrescriptionDetails.PrescriptionAuthorizationApprovalFlag = item.PrescriptionAuthorizationApprovalFlag;
                        objPrescriptionDetails.ProductApprovalFlag = Convert.ToBoolean(item.ProductApprovalFlag, CultureInfo.InvariantCulture);
                        objPrescriptionDetails.ModifiedBy = modifiedBy;
                        objPrescriptionDetails.ModifiedDate = currentDateTime;

                        /// Delivery date will never come from front end
                        /// objPrescriptionDetails.DeliveryDate = DateTime.ParseExact(item.DeliveryDate, DateFormat, CultureInfo.InvariantCulture);
                        objPrescriptionDetails.ActPPD = item.ActualPPD;
                        objPrescriptionDetails.IsRemoved = item.IsRemoved;
                        objPrescriptionDetails.IsOverridenFlag = item.IsOverridden;
                        objPrescriptionDetails.NextDeliveryDate = (!string.IsNullOrEmpty(item.ChangeNDD)) ? DateTime.ParseExact(item.ChangeNDD, DateFormat, CultureInfo.InvariantCulture) : (DateTime?)null;
                        if (objPrescriptionDetails.IDXPrescriptionProductId == 0)
                        {
                            objPrescriptionDetails.FlagDQ1 = true;
                        }
                    }

                    idxProducts.Add(objPrescriptionDetails);
                    if (toRemove.IDXPrescriptionProductId != 0)
                    {
                        objPrescription.IDXPrescriptionProducts.Remove(toRemove);
                    }
                }

                foreach (var insertProduct in idxProducts)
                {
                    objPrescription.IDXPrescriptionProducts.Add(insertProduct);
                }
            }
        }

        /// <summary>
        /// Sets the address.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="isChangedFromCareHomeType">if set to <c>true</c> [is changed from care home type].</param>
        /// <param name="careHomePatientTypes">The care home patient types.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPersonalInformation">The object personal information.</param>
        /// <returns></returns>
        private static string SetAddress(PatientBusinessModel objPatientBusinessModel, bool isChangedFromCareHomeType, List<long> careHomePatientTypes, DateTime currentDateTime, Guid modifiedBy, PersonalInformation objPersonalInformation)
        {
            ICareHomeRepository carehomeRepository = null;
            try
            {
                /// Address will update based on patient type
                Address objAddressdetails = null;
                long? correspondenceAddressId = null;
                var carehomeName = string.Empty;
                if (objPatientBusinessModel != null && careHomePatientTypes.Contains(Convert.ToInt64(objPatientBusinessModel.PatientTypeId, CultureInfo.InvariantCulture)))
                {
                    carehomeRepository = new CareHomeRepository();
                    EFModel.CareHome carehomeDetail = carehomeRepository.GetCareHome((long)objPatientBusinessModel.PAssignedCareHome).FirstOrDefault();
                    if (carehomeDetail != null)
                    {
                        objPersonalInformation.AddressId = carehomeDetail.PersonalInformation.Address.AddressId;
                        carehomeName = carehomeDetail.CareHomeId + " " + carehomeDetail.PersonalInformation.FirstName + " " + carehomeDetail.PersonalInformation.LastName + " " + carehomeDetail.PersonalInformation.Address.PostCode;
                        if (!string.IsNullOrEmpty(carehomeDetail.SAPCareHomeNumber))
                        {
                            carehomeName += " (" + carehomeDetail.SAPCareHomeNumber + ")";
                        }
                    }
                }
                else
                {
                    if (objPersonalInformation.PersonalInformationId != 0 && !isChangedFromCareHomeType)
                    {
                        objAddressdetails = objPersonalInformation.Address;
                    }
                    else
                    {
                        objAddressdetails = new EFModel.Address();
                    }

                    objAddressdetails.HouseNumber = objPatientBusinessModel.HouseNumber;
                    objAddressdetails.HouseName = Convert.ToString(objPatientBusinessModel.HouseName, CultureInfo.InvariantCulture);
                    objAddressdetails.AddressLine1 = objPatientBusinessModel.Address1;
                    objAddressdetails.AddressLine2 = objPatientBusinessModel.Address2;
                    objAddressdetails.City = objPatientBusinessModel.City;
                    objAddressdetails.Country = objPatientBusinessModel.Country;
                    objAddressdetails.County = objPatientBusinessModel.County;
                    objAddressdetails.PostCode = objPatientBusinessModel.PostCode;
                    objAddressdetails.ModifiedBy = modifiedBy;
                    objAddressdetails.ModifiedDate = currentDateTime;
                    objPersonalInformation.Address = objAddressdetails;
                }
                SetCorrespondenceAddress(objPatientBusinessModel, objPersonalInformation);
                return carehomeName;
            }
            finally
            {
                if (carehomeRepository != null)
                {
                    carehomeRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Sets the correspondence address.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="objPersonalInformation">The object personal information.</param>
        private static void SetCorrespondenceAddress(PatientBusinessModel objPatientBusinessModel, PersonalInformation objPersonalInformation)
        {
            ///Insert New Address then update Personal information
            IAddressRepository objIAddressRepository = null;
            try
            {
                objIAddressRepository = new AddressRepository();
                IUnitOfWork addrunitOfWork = objIAddressRepository.UnitOfWork;
                if (objPatientBusinessModel.CorrespondenceSameAsDelivery)
                {
                    ///Check if Patient Type selected as a carehome
                    if (objPatientBusinessModel.PAssignedCareHome > 0)
                    {
                        objPersonalInformation.CorrespondenceAddressId = objPersonalInformation.AddressId;
                    }
                    else
                    {
                        objPersonalInformation.CorrespondenceAddressId = objPersonalInformation.Address.AddressId;
                    }
                }
                else
                {
                    /// Correspondence Address was not provided then CorrespondenceAddressId will be null in Personal information table
                    if (!string.IsNullOrEmpty(objPatientBusinessModel.CorrespondencePostCode)                        
                        && !string.IsNullOrEmpty(objPatientBusinessModel.CorrespondenceAddress1))
                    {
                        ///Insert or update Correspondence Address 
                        var objAddressdetails = new Address();
                        ///If Correspondence Address was not Delivery Address then update Correspondence Address else insert new address
                        if (objPatientBusinessModel.PatientId > 0 && objPersonalInformation.CorrespondenceAddressId != null && objPersonalInformation.AddressId != objPersonalInformation.CorrespondenceAddressId)
                        {
                            objAddressdetails = objIAddressRepository.Find(Convert.ToInt64(objPersonalInformation.CorrespondenceAddressId, CultureInfo.InvariantCulture));
                        }
                        objAddressdetails.HouseName = objPatientBusinessModel.CorrespondenceCareHomeName;
                        objAddressdetails.AddressLine1 = objPatientBusinessModel.CorrespondenceAddress1;
                        objAddressdetails.AddressLine2 = objPatientBusinessModel.CorrespondenceAddress2;
                        objAddressdetails.City = objPatientBusinessModel.CorrespondenceTownOrCity;
                        objAddressdetails.Country = objPatientBusinessModel.Country;
                        objAddressdetails.County = objPatientBusinessModel.CorrespondenceCounty;
                        objAddressdetails.PostCode = objPatientBusinessModel.CorrespondencePostCode;
                        objAddressdetails.ModifiedBy = Guid.Parse(objPatientBusinessModel.ModifiedBy);
                        objAddressdetails.ModifiedDate = DateTime.Now;

                        objIAddressRepository.InsertOrUpdate(objAddressdetails);
                        //
                        addrunitOfWork.Commit();
                        objPersonalInformation.CorrespondenceAddressId = objAddressdetails.AddressId;
                    }
                    else
                    {
                        if (objPatientBusinessModel.PatientId > 0 && objPersonalInformation.CorrespondenceAddressId != null && objPersonalInformation.AddressId != objPersonalInformation.CorrespondenceAddressId)
                        {
                            objIAddressRepository.Delete(Convert.ToInt64(objPersonalInformation.CorrespondenceAddressId, CultureInfo.InvariantCulture));
                            addrunitOfWork.Commit();
                        }
                        objPersonalInformation.CorrespondenceAddressId = null;
                    }
                }
            }
            finally
            {
                if (objIAddressRepository != null)
                {
                    objIAddressRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Sets the patient.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="isChangedFromCareHomeType">if set to <c>true</c> [is changed from care home type].</param>
        /// <param name="careHomePatientTypes">The care home patient types.</param>
        /// <param name="minFrequency">The minimum frequency.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPrescription">The object prescription.</param>
        /// <returns></returns>
        private static EFModel.Patient SetPatient(PatientBusinessModel objPatientBusinessModel, ref bool isChangedFromCareHomeType, List<long> careHomePatientTypes, long minFrequency, DateTime currentDateTime, Guid modifiedBy, Prescription objPrescription)
        {
            var objPatient = new EFModel.Patient();
            SetPatientDetails(objPatientBusinessModel, ref isChangedFromCareHomeType, careHomePatientTypes, minFrequency, currentDateTime, modifiedBy, objPrescription, ref objPatient);

            SetPatientStatusReasonCode(objPatientBusinessModel, objPatient);

            objPrescription.Patient = objPatient;
            return objPatient;
        }

        /// <summary>
        /// Sets the patient status reason code.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="objPatient">The object patient.</param>
        private static void SetPatientStatusReasonCode(PatientBusinessModel objPatientBusinessModel, EFModel.Patient objPatient)
        {
            bool isAssessedPPDviolated = objPatientBusinessModel.PrescriptionList.Where(q => q.PrescriptionAuthorizationApprovalFlag == true && (q.IsRemoved == false || q.IsRemoved == null)).Any();
            bool isProductApprovalviolated = objPatientBusinessModel.PrescriptionList.Where(q => q.ProductApprovalFlag == true && (q.IsRemoved == false || q.IsRemoved == null)).Any();
            int ActiveProductCount = objPatientBusinessModel.PrescriptionList.Count();

            if (isAssessedPPDviolated || isProductApprovalviolated)
            {
                if ((objPatientBusinessModel.PatientStatusId == Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture) && objPatientBusinessModel.ReasonCodeId != Convert.ToInt64(SCAEnums.StoppedPatientReasonCode.WaitingforApproval, CultureInfo.InvariantCulture)) || objPatientBusinessModel.PatientStatusId == Convert.ToInt64(SCAEnums.PatientStatus.Removed, CultureInfo.InvariantCulture))
                {
                    objPatient.PatientStatus = objPatientBusinessModel.PatientStatusId;
                    objPatient.ReasonCodeID = objPatientBusinessModel.ReasonCodeId;
                }
                else
                {
                    objPatient.PatientStatus = Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture);
                    objPatient.ReasonCodeID = Convert.ToInt64(SCAEnums.StoppedPatientReasonCode.WaitingforApproval, CultureInfo.InvariantCulture);
                }
            }
            else if (ActiveProductCount == 0)
            {
                if (objPatient.PatientStatus == Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture) && objPatient.ReasonCodeID == Convert.ToInt64(SCAEnums.StoppedPatientReasonCode.WaitingforApproval, CultureInfo.InvariantCulture))
                {
                    objPatient.PatientStatus = Convert.ToInt64(SCAEnums.PatientStatus.Active, CultureInfo.InvariantCulture);
                    objPatient.ReasonCodeID = null;
                }
                else
                {
                    objPatient.PatientStatus = objPatientBusinessModel.PatientStatusId;
                    objPatient.ReasonCodeID = objPatientBusinessModel.ReasonCodeId;
                }
            }
            else if (!(isAssessedPPDviolated && isProductApprovalviolated) && objPatient.PatientStatus == Convert.ToInt64(SCAEnums.PatientStatus.Stopped, CultureInfo.InvariantCulture) && objPatient.ReasonCodeID == Convert.ToInt64(SCAEnums.StoppedPatientReasonCode.WaitingforApproval, CultureInfo.InvariantCulture))
            {
                objPatient.PatientStatus = Convert.ToInt64(SCAEnums.PatientStatus.Active, CultureInfo.InvariantCulture);
                objPatient.ReasonCodeID = null;
            }
            else
            {
                objPatient.PatientStatus = objPatientBusinessModel.PatientStatusId;
                objPatient.ReasonCodeID = objPatientBusinessModel.ReasonCodeId;
            }          
        }

        /// <summary>
        /// Sets the patient details.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="isChangedFromCareHomeType">if set to <c>true</c> [is changed from care home type].</param>
        /// <param name="careHomePatientTypes">The care home patient types.</param>
        /// <param name="minFrequency">The minimum frequency.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPrescription">The object prescription.</param>
        /// <param name="objPatient">The object patient.</param>
        private static void SetPatientDetails(PatientBusinessModel objPatientBusinessModel, ref bool isChangedFromCareHomeType, List<long> careHomePatientTypes, long minFrequency, DateTime currentDateTime, Guid modifiedBy, Prescription objPrescription, ref EFModel.Patient objPatient)
        {
            if (objPrescription.PatientId != 0)
            {
                objPatient = objPrescription.Patient;
                if (careHomePatientTypes.Contains(Convert.ToInt64(objPatient.PatientType, CultureInfo.InvariantCulture)))
                {
                    isChangedFromCareHomeType = true;
                }
            }

            objPatient.PatientId = objPatientBusinessModel.PatientId;
            objPatient.DateofBirth = CommonHelper.ConvertDateToMMddyyyy(objPatientBusinessModel.DateOfBirth);

            objPatient.PatientType = objPatientBusinessModel.PatientTypeId;

            List<long> nonCareHomePatientType = new List<long> { 
                (long)SCAEnums.PatientType.Procare, 
                (long)SCAEnums.PatientType.SelfCare,
                (long)SCAEnums.PatientType.Bulk,
                (long)SCAEnums.PatientType.ChildSelfCare,
                (long)SCAEnums.PatientType.PaediatricProcare,
                (long)SCAEnums.PatientType.PaediatricSelfcare,
                (long)SCAEnums.PatientType.CommunitySelfcareResidential,
                (long)SCAEnums.PatientType.CommunitySelfcareNursing
                };

            if (objPatientBusinessModel.PatientId == 0 && (nonCareHomePatientType.Contains(Convert.ToInt64(objPatientBusinessModel.PatientTypeId, CultureInfo.InvariantCulture))))
            {
                objPatientBusinessModel.Round = Convert.ToString(ConfigurationManager.AppSettings["NewPatientRound"], CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(objPatientBusinessModel.RoundId))
                {
                    objPatientBusinessModel.RoundId = objPatientBusinessModel.RoundId;
                }
                else
                {
                    objPatientBusinessModel.RoundId = Convert.ToString(ConfigurationManager.AppSettings["NewPatientRoundId"], CultureInfo.InvariantCulture);
                }
            }

            objPatient.LastModifiedBy = objPatientBusinessModel.LastModifiedBy;
            objPatient.ContactPersonId = objPatientBusinessModel.ContactPersonId;
            objPatient.DeliveryInstructionText = objPatientBusinessModel.DeliveryInstructionText;
            objPatient.DeliveryFrequency = minFrequency.ToString(CultureInfo.InvariantCulture);
            objPatient.Round = objPatientBusinessModel.Round;
            objPatient.RoundId = objPatientBusinessModel.RoundId;
            objPatient.AlternateDeliveryPoint = objPatientBusinessModel.AlternateDeliveryPoint;
            objPatient.NHSId = objPatientBusinessModel.NHSId;
            objPatient.LocalId = objPatientBusinessModel.LocalId;
            objPatient.CustomerId = objPatientBusinessModel.CustomerId;

            objPatient.CommunicationFormat = objPatientBusinessModel.CommunicationFormat;
            objPatient.CareHomeId = objPatientBusinessModel.PAssignedCareHome == 0 ? null : objPatientBusinessModel.PAssignedCareHome; // send null if 0
            objPatient.ADP1 = objPatientBusinessModel.ADP1;
            objPatient.ADP2 = objPatientBusinessModel.ADP2;
            objPatient.BillTo = objPatientBusinessModel.BillTo;
            objPatient.ModifiedBy = modifiedBy;
            objPatient.ModifiedDate = currentDateTime;
            objPatient.IsDataProtected = objPatientBusinessModel.IsDataProtected;
            objPatient.IsSentToSAP = false;
            objPatient.AssessmentDate = CommonHelper.ConvertDateToMMddyyyy(objPatientBusinessModel.AssessmentDate);
            if (!string.IsNullOrEmpty(objPatientBusinessModel.NextAssessmentDate))
            {
                objPatient.NextAssessmentDate = CommonHelper.ConvertDateToMMddyyyy(objPatientBusinessModel.NextAssessmentDate);
            }
            if (!string.IsNullOrEmpty(objPatientBusinessModel.NextDeliveryDate))
            {
                objPatient.NextDeliveryDate = CommonHelper.ConvertDateToMMddyyyy(objPatientBusinessModel.NextDeliveryDate);
            }                  
        }

        /// <summary>
        /// Sets the prescription.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="prescriptionRepository">The prescription repository.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <returns></returns>
        private static Prescription SetPrescription(PatientBusinessModel objPatientBusinessModel, IPrescriptionRepository prescriptionRepository, DateTime currentDateTime, Guid modifiedBy)
        {
            var objPrescription = new EFModel.Prescription();
            if (objPatientBusinessModel.PatientId != 0)
            {
                var lstPrescription = prescriptionRepository.GetPatientPrescription(objPatientBusinessModel.PatientId);
                if (lstPrescription.Any())
                {
                    objPrescription = lstPrescription.First(); // Always will get one record
                }
            }

            {
                objPrescription.ModifiedBy = modifiedBy;
                objPrescription.ModifiedDate = currentDateTime;
            }

            return objPrescription;
        }

        /// <summary>
        /// Sets the personal information.
        /// </summary>
        /// <param name="objPatientBusinessModel">The object patient business model.</param>
        /// <param name="currentDateTime">The current date time.</param>
        /// <param name="modifiedBy">The modified by.</param>
        /// <param name="objPatient">The object patient.</param>
        /// <returns></returns>
        private static PersonalInformation SetPersonalInformation(PatientBusinessModel objPatientBusinessModel, DateTime currentDateTime, Guid modifiedBy, EFModel.Patient objPatient)
        {
            var objPersonalInformation = new EFModel.PersonalInformation();
            if (objPatient.PatientId != 0)
            {
                objPersonalInformation = objPatient.PersonalInformation;
            }

            objPersonalInformation.FirstName = objPatientBusinessModel.PFirstName;
            objPersonalInformation.LastName = objPatientBusinessModel.PLastName;
            objPersonalInformation.TitleId = objPatientBusinessModel.PTitleId;
            objPersonalInformation.Gender = objPatientBusinessModel.PGender;
            objPersonalInformation.TelephoneNumber = objPatientBusinessModel.PTelephoneNumber;
            objPersonalInformation.MobileNumber = objPatientBusinessModel.PMobileNumber;
            objPersonalInformation.Email = objPatientBusinessModel.PEmail;
            objPersonalInformation.ModifiedBy = modifiedBy;
            objPersonalInformation.ModifiedDate = currentDateTime;

            if (objPatient.PatientId == 0)
            {
                objPersonalInformation.CreatedById = modifiedBy;
                objPersonalInformation.CreatedDateTime = currentDateTime;
            }

            objPatient.PersonalInformation = objPersonalInformation;
            return objPersonalInformation;
        }

        /// <summary>
        /// Merge MedicalStaff and AnalysisInfo Blank columns and value assigned columns
        /// </summary>
        /// <param name="objDataColumns">The object data columns.</param>
        /// <param name="objColumns">The object columns.</param>
        /// <param name="listType">Type of the list.</param>
        /// <returns>
        /// List (MedicalStaffAnalysisInfoBusinessModel)
        /// </returns>
        private List<MedicalStaffAnalysisInfoBusinessModel> MergeData(List<MedicalStaffAnalysisInfoBusinessModel> objDataColumns, List<MedicalStaffAnalysisInfoBusinessModel> objColumns, int listType)
        {
            if (objDataColumns == null || objColumns == null)
            {
                return null;
            }
            if (listType == (int)SCAEnums.ListType.MedicalStaff)
            {
                if (objDataColumns.Any() && objColumns.Any())
                {
                    foreach (var item in objDataColumns)
                    {
                        if (objColumns.Where(m => m.ListId == item.ListId).Any())
                        {
                            var column = objColumns.Find(m => m.ListId.Equals(item.ListId));
                            column.StaffName = item.StaffName;
                            column.StaffId = item.StaffId;
                            column.CustomerMedicalStaffAnalysisInfoId = item.CustomerMedicalStaffAnalysisInfoId;
                            column.IDXPatientMedicalStaffId = item.IDXPatientMedicalStaffId;
                        }
                    }
                }
            }
            else if (listType == (int)SCAEnums.ListType.AnalysisInformation)
            {
                var staffNameSb = new StringBuilder();
                var infoIdsSb = new StringBuilder();

                var totalcount = 0;
                var counter = 0;
                char[] charsToTrim = { ',', ' ' };
                foreach (var item in objDataColumns)
                {
                    var occurCount = objDataColumns.Where(x => x.ListId == item.ListId).Count();
                    totalcount = occurCount;
                    if (objColumns.Where(m => m.ListId == item.ListId).Any())
                    {
                        if (occurCount > 1)
                        {
                            var column = objColumns.Find(m => m.ListId.Equals(item.ListId) && m.ListTypeId.Equals(item.ListTypeId));
                            staffNameSb.Append(item.StaffName).Append(", ");
                            infoIdsSb.Append(item.CustomerMedicalStaffAnalysisInfoId).Append(",");

                            column.StaffName = staffNameSb.ToString();
                            column.StaffId = item.StaffId;
                            column.CustomerMedicalStaffAnalysisInfoIds = infoIdsSb.ToString();
                            column.IDXPatientMedicalStaffId = item.IDXPatientMedicalStaffId;
                            counter++;
                            if (counter == totalcount)
                            {
                                staffNameSb.Clear();
                                infoIdsSb.Clear();
                            }
                        }
                        else
                        {
                            var column = objColumns.Find(m => m.ListId.Equals(item.ListId));
                            column.StaffName = item.StaffName;
                            column.StaffId = item.StaffId;
                            column.CustomerMedicalStaffAnalysisInfoId = item.CustomerMedicalStaffAnalysisInfoId;
                            column.IDXPatientMedicalStaffId = item.IDXPatientMedicalStaffId;
                        }
                    }
                }
                objColumns.ForEach(x => x.StaffName = x.StaffName.TrimEnd(charsToTrim));
            }

            return objColumns;
        }        

        /// <summary>
        /// Checks the product exist.
        /// </summary>
        /// <param name="PatientPrescriptionViewModel">The patient prescription view model.</param>
        /// <returns></returns>
        public bool CheckProductExist(PrescriptionBusinessModel patientPrescriptionViewModel)
        {
            if (patientPrescriptionViewModel == null)
            {
                return false;
            }

            IIDXPrescriptionProductRepository idxPrescriptionRepository = null;
            IPrescriptionRepository prescriptionRepository = null;
            var objPrescription = new EFModel.Prescription();
            try
            {
                idxPrescriptionRepository = new IDXPrescriptionProductRepository();
                prescriptionRepository = new PrescriptionRepository();
                if (patientPrescriptionViewModel.ProductId != 0)
                {
                    var patientPrescriptionList = prescriptionRepository.GetPatientPrescription(Convert.ToInt64(patientPrescriptionViewModel.PatientID, CultureInfo.InvariantCulture));
                    if (patientPrescriptionList.Any())
                    {
                        objPrescription = patientPrescriptionList.Single();
                    }

                    var prescriptionDetails = idxPrescriptionRepository.CheckProductExist(patientPrescriptionViewModel.ProductId, objPrescription.PrescriptionId);
                    if (prescriptionDetails.Any())
                    {
                        return false;
                    }

                    return true;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (prescriptionRepository != null)
                {
                    prescriptionRepository.Dispose();
                }

                if (idxPrescriptionRepository != null)
                {
                    idxPrescriptionRepository.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Get Patient Details to be Edited
        /// </summary>
        /// <param name="userType">User's type</param>
        /// <param name="roleName">User's Role Name</param>
        /// <returns>
        /// List of PatientBusinessModel
        /// </returns>
        public List<PatientBusinessModel> GetPatientEditDetails(string userType, string roleName)
        {
            IRoleRepository roleRepository = null;
            try
            {
                roleRepository = new RoleRepository();
                List<PatientBusinessModel> objNurseRole = new List<PatientBusinessModel>();

                // List<Role> EditDeatails = new List<Role>();
                var EditDetails = roleRepository.GetEditableRole(userType, roleName).ToList();
                EditDetails.ForEach(q =>
                {
                    objNurseRole.Add(new PatientBusinessModel()
                    {
                        IsMedicalInfoEditable = q.IsMedicalInfoEditable,
                        IsPatientInfoEditable = q.IsPatientInfoEditable,
                        IsPrescriptionEditable = q.IsPrescriptionEditable,
                        RoleName = roleName
                    });
                });

                return objNurseRole;
            }
            finally
            {
                if (roleRepository != null)
                {
                    roleRepository.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets the type of the customer patient.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public List<PatientBusinessModel> GetCustomerPatientType(string customerId, string userId)
        {
            IPatientTypeMatrixRepository patientRepository = null;
            IUsersRepository usersRepository = null;
            try
            {
                patientRepository = new PatientTypeMatrixRepository();
                var objPatientType = new List<PatientBusinessModel>();

                List<string> customerIds = customerId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                var cusomerIdsLong = new List<long>();

                foreach (var id in customerIds)
                {

                    if (!string.IsNullOrEmpty(id.Trim()))
                    {
                        cusomerIdsLong.Add(Convert.ToInt64(id, CultureInfo.InvariantCulture));
                    }
                }

                var patinetTypeDetails = patientRepository.GetPatientTypeMatrixByCustomerId(cusomerIdsLong);

                usersRepository = new UsersRepository();
                if (usersRepository.GetUsersByUserId(userId).Where(q => q.IsCarehomeUser).Any())
                    patinetTypeDetails = patinetTypeDetails.Where(q => Enum.IsDefined(typeof(SCAEnums.CarehomePatientType), 
                                                                       Convert.ToInt32(q.PatientType, CultureInfo.InvariantCulture))).ToList();

                patinetTypeDetails.ForEach(q =>
                {
                    objPatientType.Add(new PatientBusinessModel()
                    {
                        DisplayText = q.List.DefaultText,
                        ListId = q.List.ListId.ToString(CultureInfo.InvariantCulture),
                        ListIndex = q.List.ListIndex
                    });
                });

                var distinctTypes = objPatientType.GroupBy(q => q.ListId).Select(y => y.First());
                distinctTypes = distinctTypes.OrderBy(q => q.ListIndex);
                return distinctTypes.ToList();
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (patientRepository != null)
                {
                    patientRepository.Dispose();
                }

                if (usersRepository != null)
                    usersRepository.Dispose();

            }

            return null;

        }

        /// <summary>
        /// Loads Nurse details for given patientId
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>
        /// Nurse Comments Details
        /// </returns>
        public NurseCommentsBusinessModel LoadNurseDetails(string patientId)
        {
            IPrescriptionExtendedRepository objPrescriptionrepository = null;
            try
            {
                objPrescriptionrepository = new PrescriptionExtendedRepository();
                var iPatientId = Convert.ToInt64(patientId, CultureInfo.InvariantCulture);
                var prescriptionExtendedList = objPrescriptionrepository.All.Where(q => q.Prescription.PatientId == iPatientId).OrderByDescending(q => q.PrescriptionExtendedId);

                if (!prescriptionExtendedList.Any())
                {
                    return null;
                }

                var prescriptionExtended = prescriptionExtendedList.First();
                return new NurseCommentsBusinessModel
                {
                    Name = prescriptionExtended.Name,
                    MobileNumber = prescriptionExtended.MobileNumber,
                    Email = prescriptionExtended.Email,
                    Comments = prescriptionExtended.Comments
                };
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (objPrescriptionrepository != null)
                {
                    objPrescriptionrepository.Dispose();
                }
            }

            return null;

        }

		/// <summary>
        /// Get the patient details based on PatientId
        /// </summary>
        /// <param name="objPatientBusinessModel">Instance of PatientBusinessModel</param>
        /// <returns>
        /// Patient BusinessModel
        /// </returns>
        public string GetPatientCurrentNDD(string patientId, string userId)
		{
			IPatientRepository patientRepository = null;
			try
			{
				globalUserId = userId;
				patientRepository = new PatientRepository();
				var lPatientId = Convert.ToInt64(patientId);
				var patientNDD = patientRepository.GetPatientsNDD(lPatientId);
				if (patientNDD.HasValue)
				{
                    return patientNDD.Value.ToString(CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture);
				}				
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, this.globalUserId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, this.globalUserId);
				throw;
			}
			finally
			{
				if (patientRepository != null)
				{
					patientRepository.Dispose();
				}
			} 
			return string.Empty;		
		}

        #region CancelOrderStoppedRemovedPatient

        /// <summary>
        /// Cancels order for removed or stopped patient
        /// </summary>
        /// <param name="patient"></param>
        /// <param name="patientOrders"></param>
        /// <returns></returns>
        private EFModel.Patient CancelOrderRemovedStopped(EFModel.Patient patient, out KeyValuePair<EFModel.Patient, EFModel.Order> patientOrders)
        {
            var orders = patient.Orders.Where(q => q.OrderStatus == (long)SCAEnums.OrderStatus.Activated &&
                                                   (bool)q.IsRushFlag == false && ((long)q.OrderType == (long)SCAEnums.OrderType.ZHDH | 
                                                   (long)q.OrderType == (long)SCAEnums.OrderType.ZHDP)).ToList();

            patientOrders = new KeyValuePair<EFModel.Patient, EFModel.Order>();

            var order = orders.FirstOrDefault();
            if (order != null)
            {
                order.OrderStatus = (long)SCAEnums.OrderStatus.Cancelled;
                order.IDXOrderProducts.ToList().ForEach(q => { q.Quantity = 0; q.IsRemoved = true; q.ModifiedBy = patient.ModifiedBy; q.ModifiedDate = patient.ModifiedDate; });
                patientOrders = new KeyValuePair<EFModel.Patient, EFModel.Order>(patient, order);
            }
            return patient;
        }

        #endregion

        /// <summary>
        /// Checks wheather Patient has Active Order
        /// </summary>
        /// <param name="objPatientBusinessModel"></param>
        /// <returns></returns>
        public bool IsPatientOrderActivated(PatientBusinessModel objPatientBusinessModel)
        {
            IOrderRepository orderRepository = null;
            bool IsActiveOrder = false;
            try
            {
                if (objPatientBusinessModel != null)
                {
                    globalUserId = objPatientBusinessModel.UserId;

                    orderRepository = new OrderRepository();
                    IsActiveOrder = orderRepository.GetPatientOrders(objPatientBusinessModel.PatientId).Where(q =>
                                                                (q.OrderStatus == (long)SCAEnums.OrderStatus.Activated || q.OrderStatus == (long)SCAEnums.OrderStatus.SentToSAP)).Any();
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (orderRepository != null)
                    orderRepository.Dispose();
            }
            return IsActiveOrder;
        }

        /// <summary>
        /// Calculates NDD when new product is added in prescription
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetPatientNDDForFirstProduct(string patientId, string userId)
        {
            IPatientRepository objPatientRepository = null;

            var response = string.Empty;
            try
            {
                var responseDate = default(DateTime);
                var calculatedpatientNDD = DateTime.Now.Date;

                objPatientRepository = new PatientRepository();
                if (string.IsNullOrEmpty(patientId))
                {
                    return response;
                }
                var iPatientId = Convert.ToInt64(patientId, CultureInfo.InvariantCulture);
                var objPatient = objPatientRepository.Find(iPatientId);

                if (objPatient == null)
                {
                    return response;
                }

                var patientNDD = objPatient.NextDeliveryDate ?? default(DateTime);
                var patientType = objPatient.PatientType ?? 0;
                var careHomeNDD = objPatient.CareHome != null ? objPatient.CareHome.NextDeliveryDate ?? default(DateTime) : default(DateTime);

                switch (CheckPatientType(patientType))
                {
                    case "Selfcare":
                        responseDate = patientNDD;
                        break;
                    case "Procare":
                        if (patientNDD > calculatedpatientNDD)
                        {
                            responseDate = patientNDD;
                        }
                        else
                        {
                            var objCommon = new CommonService();

                            int OrderLeadTime = 0;
                            int countryId = objPatient.Customer.CountryId ?? 0;
                            if (objPatient.Customer.CustomerParameters.Any())
                            {
                                OrderLeadTime = objPatient.Customer.CustomerParameters.FirstOrDefault().OrderLeadTime;
                            }

                            calculatedpatientNDD = objCommon.AddWorkingDays(calculatedpatientNDD, OrderLeadTime, countryId, userId);
                            objCommon.PostcodeMatrixList = objCommon.GetPostcodeMatrixForPatient(Convert.ToString(objPatient.CustomerId, CultureInfo.InvariantCulture),
                                                                                                 Convert.ToString(objPatient.PatientId, CultureInfo.InvariantCulture),
                                                                                                 objPatient.CareHomeId != null ? Convert.ToString(objPatient.CareHomeId, CultureInfo.InvariantCulture) : "0",
                                                                                                 Convert.ToString(default(Guid), CultureInfo.InvariantCulture));

                            while (objCommon.IsHoliday(calculatedpatientNDD = objCommon.GetDateAfterPostcodeMatrix(calculatedpatientNDD), countryId, this.globalUserId))
                            {
                                calculatedpatientNDD = calculatedpatientNDD.AddDays(1);
                            }

                            responseDate = calculatedpatientNDD;
                        }
                        break;
                    case "Carehome":
                        if (patientNDD > calculatedpatientNDD)
                        {
                            responseDate = patientNDD;
                        }
                        else if (careHomeNDD != default(DateTime))
                        {
                            responseDate = careHomeNDD;
                        }
                        break;
                }

                if (responseDate != default(DateTime))
                    response = responseDate.ToString(CommonConstants.DateFormat1, CultureInfo.InvariantCulture);

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (NullReferenceException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (objPatientRepository != null)
                    objPatientRepository.Dispose();
            }
            return response;
        }

        /// <summary>
        /// Checks the patient type for NDD calculation for adding product in empty prescription
        /// </summary>
        /// <param name="patientType"></param>
        /// <returns>1 Selfcare, 2 Procare, 3 Carehome, 0 Others</returns>
        private string CheckPatientType(long patientType)
        {
            var response = string.Empty;
            switch (patientType)
            {
                case (long)SCAEnums.PatientType.SelfCare:
                case (long)SCAEnums.PatientType.PaediatricSelfcare:
                case (long)SCAEnums.PatientType.CommunitySelfcareNursing:
                case (long)SCAEnums.PatientType.CommunitySelfcareResidential:
                    response = "Selfcare";
                    break;
                case (long)SCAEnums.PatientType.Procare:
                case (long)SCAEnums.PatientType.PaediatricProcare:
                case (long)SCAEnums.PatientType.Bulk:
                    response = "Procare";
                    break;
                case (long)SCAEnums.PatientType.Residential:
                case (long)SCAEnums.PatientType.ResidentialSelfcare:
                case (long)SCAEnums.PatientType.Hospital:
                case (long)SCAEnums.PatientType.ContinuingCare:
                case (long)SCAEnums.PatientType.Nursing:
                case (long)SCAEnums.PatientType.NursingSelfcare:
                    response = "Carehome";
                    break;
            }
            return response;
        }
    }
}