﻿//----------------------------------------------------------------------------------------------
// <copyright file="AuditLog.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
	using System.ServiceModel;

    using BusinessModels;
    using Interfaces;
    using Common;
    using Common.Enums;
    using Infrastructure.Repositories;
    using Model;

    /// <summary>
    /// Audit Log details
    /// </summary>        
    public class AuditLog : IAuditLog
    {
        /// <summary>
        /// Gets Patient's details and AuditLog
        /// </summary>
        /// <param name="auditLogData"></param>
        /// <returns>
        /// Patient's Audit Logs
        /// </returns>
        public AuditLogBusinessModel GetPatientOrCareHomeAuditLog(AuditLogBusinessModel auditLogData)
        {
            IPatientRepository objPatientRepository = null;
            if (auditLogData == null)
            {
                return new AuditLogBusinessModel();
            }
            var userId = auditLogData.LoggedInUserId.ToString();
	        try
	        {
		        objPatientRepository = new PatientRepository();
		        var auditResponse = new AuditLogBusinessModel
		        {
			        Logs = GetLogs(auditLogData)
		        };
		        return auditResponse;
	        }
	        catch (DataException e)
	        {
		        LogHelper.LogException(e, userId);
	        }
	        catch (ArgumentException e)
	        {
		        LogHelper.LogException(e, userId);
	        }
	        catch (FormatException e)
	        {
		        LogHelper.LogException(e, userId);
	        }
	        catch (InvalidOperationException e)
	        {
		        LogHelper.LogException(e, userId);
	        }
	        catch (SystemException e)
	        {
		        LogHelper.LogException(e, userId);
		        throw;
	        }
	        catch (Exception e)
	        {
		        LogHelper.LogException(e, userId);
		        throw;
	        }
	        finally
	        {
		        if (objPatientRepository != null)
		        {
			        objPatientRepository.Dispose();
		        }
	        }
	        return new AuditLogBusinessModel();
        }

	    /// <summary>
	    /// Gets the logs.
	    /// </summary>
	    /// <param name="auditLogRequestData">The audit log request data.</param>
	    /// <returns>AuditLog Report Business Model</returns>
	    private static List<AuditLogReportBusinessModel> GetLogs(AuditLogBusinessModel auditLogRequestData)
	    {
		    var userId = auditLogRequestData.LoggedInUserId.ToString();
		    try
		    {

			    var tableName = auditLogRequestData.AuditLogType ==
			                    Convert.ToInt64(SCAEnums.AuditLogType.Patient, CultureInfo.InvariantCulture)
				    ? Convert.ToString(SCAEnums.AuditLogType.Patient, CultureInfo.InvariantCulture)
				    : (auditLogRequestData.AuditLogType ==
				       Convert.ToInt64(SCAEnums.AuditLogType.CareHome, CultureInfo.InvariantCulture))
					    ? Convert.ToString(SCAEnums.AuditLogType.CareHome, CultureInfo.InvariantCulture)
					    : (auditLogRequestData.AuditLogType == Convert.ToInt64(SCAEnums.AuditLogType.Users, CultureInfo.InvariantCulture))
						    ? Convert.ToString(SCAEnums.AuditLogType.Users, CultureInfo.InvariantCulture)
						    : Convert.ToString(SCAEnums.AuditLogType.Customer, CultureInfo.InvariantCulture);

			    var auditLogSpParameteres = new
			    {
				    PageNumber = auditLogRequestData.PageNumber,
				    RecordsPerPage = auditLogRequestData.RecordsPerPage,
				    AuditLogtypeId = Convert.ToInt64(auditLogRequestData.AuditLogType, CultureInfo.InvariantCulture),
				    PatientId = Convert.ToInt64(auditLogRequestData.PatientId, CultureInfo.InvariantCulture),
				    CustomerId = Convert.ToInt64(auditLogRequestData.CustomerId, CultureInfo.InvariantCulture),
				    CareHomeId = Convert.ToInt64(auditLogRequestData.CareHomeId, CultureInfo.InvariantCulture),
				    LoggedInUserId = auditLogRequestData.LoggedInUserId,
				    SearchedUserId = auditLogRequestData.SearchedUserId,
				    FromDate = Convert.ToDateTime(auditLogRequestData.Fromdate, CultureInfo.InvariantCulture),
				    ToDate = Convert.ToDateTime(auditLogRequestData.Todate, CultureInfo.InvariantCulture),
				    IsExport = auditLogRequestData.IsExport,
				    TableName = tableName
			    };

			    var objAuditLogs = (new DBHelper()).GetAuditLog(
													auditLogSpParameteres.PageNumber,
													auditLogSpParameteres.RecordsPerPage,
													auditLogSpParameteres.AuditLogtypeId,
													auditLogSpParameteres.PatientId,
													auditLogSpParameteres.CustomerId,
													auditLogSpParameteres.CareHomeId,
													auditLogSpParameteres.LoggedInUserId,
													auditLogSpParameteres.SearchedUserId,
													auditLogSpParameteres.FromDate,
													auditLogSpParameteres.ToDate,
													auditLogSpParameteres.TableName,
													auditLogRequestData.IsExport).ToList();

			    if (objAuditLogs.Any())
			    {
				    var auditLogs = objAuditLogs.Select(q => new AuditLogReportBusinessModel
				    {
					    TotalRecords = q.TotalRecords.Value,
					    Id = Convert.ToInt64(q.PrimaryKey, CultureInfo.InvariantCulture),
					    Name = q.Name,
					    Action = q.Action,
					    Field = q.ColumnName,
					    ModifiedOn = q.ModifiedDate.ToString(CommonConstants.DateTimeFormat2, CultureInfo.InvariantCulture),
					    OldValue = q.OldValue,
					    NewValue = q.NewValue,
					    ModifiedBy = q.UserName,
					    TableName = q.TableName,
					    AuditLogId = q.AuditLogId,
					    Remarks = q.Remarks
				    }).ToList();
				    return auditLogs;
			    }
			    return new List<AuditLogReportBusinessModel>();
		    }
		    catch (DataException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (ArgumentException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (FormatException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (InvalidOperationException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (SystemException e)
		    {
			    LogHelper.LogException(e, userId);
			    throw;
		    }
		    catch (Exception e)
		    {
			    LogHelper.LogException(e, userId);
			    throw;
		    }
		    return new List<AuditLogReportBusinessModel>();
	    }

	    /// <summary>
	    /// Gets the Search Results for Customer/CareHome/Patient search on AuditLog scren
	    /// </summary>
	    /// <param name="searchRequest">Object of AuditLogSearchBusinessModel containing search keyword and user Id who requested search</param>
	    /// <returns>
	    /// List of Search results including Id, SAP Id, Name of the searched Entity
	    /// </returns>
	    public List<CommonSearchBusinessModel> GetAuditLogSearchResults(CommonSearchBusinessModel searchRequest)
	    {
		    var userId = string.Empty;
		    try
		    {
			    if (searchRequest == null)
			    {
				    return new List<CommonSearchBusinessModel>();
			    }
			    var searchresults = (new DBHelper()).GetAuditLogSearchResult(searchRequest.SearchTerm, searchRequest.TableName,
				    searchRequest.UserId, searchRequest.CustomerId);
			    if (searchresults != null)
			    {
				    var searchResults = searchresults.Select(q => new CommonSearchBusinessModel
				    {
					    Id = q.Id,
					    SAPId = CommonHelper.TrimLeadingZeros(q.SAPId),
					    SearchDisplayName = q.Name,
					    SearchAutoCompleteName = q.SearchResult,
					    UserId = q.UserId ?? Guid.Empty
				    }).ToList();
				    return searchResults;
			    }
		    }
		    catch (DataException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (ArgumentException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (FormatException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (InvalidOperationException e)
		    {
			    LogHelper.LogException(e, userId);
		    }
		    catch (SystemException e)
		    {
			    LogHelper.LogException(e, userId);
			    throw;
		    }
		    catch (Exception e)
		    {
			    LogHelper.LogException(e, userId);
			    throw;
		    }
		    return new List<CommonSearchBusinessModel>();
	    }

	    /// <summary>
        /// Saves user's Remarks/Notes on AuditLog
        /// </summary>
        /// <param name="auditLogs">List of Audit Log Report Business model</param>
        /// <returns>True or False</returns>
        public bool SaveAuditLogRemarks(List<AuditLogReportBusinessModel> auditLogs)
        {
            var userId = string.Empty;            
            IAuditLogRepository objAuditLogRepository = null; 
            try
            {
                objAuditLogRepository = new AuditLogRepository();
                IUnitOfWork objUnitOfWork = objAuditLogRepository.UnitOfWork;
                var auditLogIds = auditLogs.Select(q => q.AuditLogId).ToList();
                var existingAuditList = objAuditLogRepository.GetAuditLogByAuditLogId(auditLogIds).ToList();
                foreach(var objAudit in existingAuditList)
                {
                    objAudit.Remarks = auditLogs.First(q => q.AuditLogId == objAudit.AuditLogId).Remarks.Trim();
                    objAuditLogRepository.InsertOrUpdate(objAudit);
                }
                objUnitOfWork.Commit();
                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, userId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                throw;
            }
            finally
            {
                if (objAuditLogRepository != null)
                {
                    objAuditLogRepository.Dispose();
                }
            }
            return false;
        }
    }
}
