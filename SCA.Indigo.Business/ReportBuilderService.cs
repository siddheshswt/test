﻿//----------------------------------------------------------------------------------------------
// <copyright file="ReportBuilderService.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Globalization;
	using System.Linq;

	using SCA.Indigo.Business.BusinessModels;
	using SCA.Indigo.Business.Interfaces;
	using SCA.Indigo.Common;
	using SCA.Indigo.Infrastructure.Repositories;
	using SCA.Indigo.Model;
	using System.ServiceModel;
	using System.Configuration;

	/// <summary>
	/// Report Builder Service Class
	/// </summary>    
	public class ReportBuilderService : IReportBuilder
	{
		/// <summary>
		/// Saves the report builder.
		/// </summary>
		/// <param name="reportBuilderBusinessViewModel">The report builder business view model.</param>
		/// <returns>Status </returns>
		public string SaveReportBuilder(ReportFormatBusinessModel reportBuilderBusinessViewModel)
		{
			string message = "false";
			string userId = reportBuilderBusinessViewModel.UserId;
			IReportBuilderRepository reportBuilderRepository = new ReportBuilderRepository();
			try
			{
				if (reportBuilderBusinessViewModel != null)
				{
					ReportBuilderFormat reportBuilderFormat = new ReportBuilderFormat();
					reportBuilderFormat.ReportBuilderId = reportBuilderBusinessViewModel.ReportBuilderFormatId;
					reportBuilderFormat.CustomerColumn = reportBuilderBusinessViewModel.CustomerColumn;
					reportBuilderFormat.CareHomeColumn = reportBuilderBusinessViewModel.CareHomeColumn;
					reportBuilderFormat.CustomerParameterColumn = reportBuilderBusinessViewModel.CustomerParameterColumn;
					reportBuilderFormat.OrderColumn = reportBuilderBusinessViewModel.OrderColumn;
					reportBuilderFormat.PrescriptionColumn = reportBuilderBusinessViewModel.PerscriptionColumn;
					reportBuilderFormat.ClinicalContactsColumn = reportBuilderBusinessViewModel.ClinicalContactColumn;
					reportBuilderFormat.PatientColumn = reportBuilderBusinessViewModel.PatientColumn;
					reportBuilderFormat.PatientTypeMatrixColumn = reportBuilderBusinessViewModel.PatientMatrixColumn;
					reportBuilderFormat.UserMaintainceColumn = reportBuilderBusinessViewModel.UserMaintainanceColumn;
					reportBuilderFormat.ReportFormatName = reportBuilderBusinessViewModel.ReportFormatName;
					if (reportBuilderBusinessViewModel.ReportBuilderFormatId != 0)
					{
						reportBuilderFormat.ModifiedDate = DateTime.Now;
						reportBuilderFormat.ModifiedBy = Guid.Parse(reportBuilderBusinessViewModel.UserId);
					}
					else
					{
						reportBuilderFormat.CreatedDate = DateTime.Now;
						reportBuilderFormat.CreatedBy = Guid.Parse(reportBuilderBusinessViewModel.UserId);
					}

					reportBuilderRepository.InsertOrUpdate(reportBuilderFormat);
					reportBuilderRepository.Save();
					message = "true";
					return message;
				}
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			return message;
		}

		/// <summary>
		/// Gets the report format drop down.
		/// </summary>
		/// <returns>
		/// Report Formats
		/// </returns>
		public List<ReportFormatBusinessModel> GetReportFormatDropDown()
		{
			string userId = string.Empty;
			IReportBuilderRepository reportBuilderRepository = new ReportBuilderRepository();
			List<ReportFormatBusinessModel> reportFormatBusinessModelList = new List<ReportFormatBusinessModel>();
			try
			{
				ReportFormatBusinessModel reportFormatBusinessModel;
				var responseReportbBuilder = reportBuilderRepository.All.ToList();
				foreach (var item in responseReportbBuilder)
				{
					reportFormatBusinessModel = new ReportFormatBusinessModel();
					reportFormatBusinessModel.ReportBuilderFormatId = item.ReportBuilderId;
					reportFormatBusinessModel.ReportFormatName = item.ReportFormatName;
					reportFormatBusinessModelList.Add(reportFormatBusinessModel);
				}
				return reportFormatBusinessModelList;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}

			return reportFormatBusinessModelList;
		}

		/// <summary>
		/// Gets the report format.
		/// </summary>
		/// <param name="reportFormatId">The report format identifier.</param>
		/// <returns>
		/// Report format data
		/// </returns>
		public List<ReportFormatBusinessModel> GetReportFormat(string reportFormatId)
		{
			string userId = string.Empty;
			long reportBuilderId = Convert.ToInt64(reportFormatId, CultureInfo.InvariantCulture);
			IReportBuilderRepository reportBuilderRepository = new ReportBuilderRepository();
			try
			{
				List<ReportFormatBusinessModel> reportFormatBusinessModelList = new List<ReportFormatBusinessModel>();
				ReportFormatBusinessModel reportFormatRecord = new ReportFormatBusinessModel();
				var reportFormatQuery = reportBuilderRepository.All.Where(a => a.ReportBuilderId == reportBuilderId).FirstOrDefault();
				reportFormatRecord.ReportFormatName = reportFormatQuery.ReportFormatName;
				reportFormatRecord.CustomerColumn = reportFormatQuery.CustomerColumn;
				reportFormatRecord.OrderColumn = reportFormatQuery.OrderColumn;
				reportFormatRecord.CustomerParameterColumn = reportFormatQuery.CustomerParameterColumn;
				reportFormatRecord.PatientColumn = reportFormatQuery.PatientColumn;
				reportFormatRecord.PatientMatrixColumn = reportFormatQuery.PatientTypeMatrixColumn;
				reportFormatRecord.ClinicalContactColumn = reportFormatQuery.ClinicalContactsColumn;
				reportFormatRecord.UserMaintainanceColumn = reportFormatQuery.UserMaintainceColumn;
				reportFormatRecord.CareHomeColumn = reportFormatQuery.CareHomeColumn;
				reportFormatRecord.PerscriptionColumn = reportFormatQuery.PrescriptionColumn;
				reportFormatBusinessModelList.Add(reportFormatRecord);
				return reportFormatBusinessModelList;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, userId);
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
				throw;
			}
			return null;
		}

		/// <summary>
		/// Get report data
		/// </summary>
		/// <param name="reportBuilderCriteria">Report Builder Criteria</param>
		/// <returns>
		/// report data
		/// </returns>
		public List<ReportDataBusinessModel> GetReportData(ReportBuilderCriteria reportBuilderCriteria)
		{
			var isExceptionOccurred = false;
			List<ReportDataBusinessModel> resultData = new List<ReportDataBusinessModel>();
			try
			{				
				if (reportBuilderCriteria == null)
				{
					return null;
				}
				int totalRows = 0;				
				var timeout = Convert.ToInt32(ConfigurationManager.AppSettings["ReportBuilderTimeout"], CultureInfo.InvariantCulture);
				var connectionString = ConfigurationManager.ConnectionStrings["SCAIndigoEnterpriseConn"].ConnectionString;
				var result = (new DBHelper()).GetReportData(connectionString, timeout, reportBuilderCriteria.ColumnNames, reportBuilderCriteria.CustomerIds, reportBuilderCriteria.PatientNameId
															, reportBuilderCriteria.CareHomeIds, reportBuilderCriteria.PatientDeliveryDateFrom, reportBuilderCriteria.PatientDeliveryDateTo
															, reportBuilderCriteria.OrderDateFrom, reportBuilderCriteria.OrderDateTo, reportBuilderCriteria.InvoiceDateFrom
															, reportBuilderCriteria.InvoiceDateTo
															, reportBuilderCriteria.PatientCreateDateFrom
															, reportBuilderCriteria.PatientCreateDateTo
															, reportBuilderCriteria.NextAssessmentDateFrom
															, reportBuilderCriteria.NextAssessmentDateTo
															, reportBuilderCriteria.InvoiceNumber, reportBuilderCriteria.InvoiceNumberTo
															, reportBuilderCriteria.PatientType
															, reportBuilderCriteria.PatientStatus, reportBuilderCriteria.UserDataOnly, reportBuilderCriteria.CustomerColumns
															, reportBuilderCriteria.PatientColumns, reportBuilderCriteria.PrescriptionColumns, reportBuilderCriteria.ClinicalContactsColumns
															, reportBuilderCriteria.CareHomeColumns, reportBuilderCriteria.OrderColumns, reportBuilderCriteria.UserMaintenanceColumns
															, reportBuilderCriteria.ProductIds, reportBuilderCriteria.LanguageId, reportBuilderCriteria.PageNbr, reportBuilderCriteria.PageSize
															, reportBuilderCriteria.SortColumn, reportBuilderCriteria.SortDir, reportBuilderCriteria.IsDownload, out totalRows);
				
				resultData = result.Select(item => new ReportDataBusinessModel()
				{
					CustomerId = item.CustomerId,
					CustomerStartDate = item.CustomerStartDate,
					CustomerName = item.CustomerName,
					PatientId = item.PatientId,
					SAPPatientNumber = string.IsNullOrEmpty(item.SAPPatientNumber) == true ? item.SAPPatientNumber : item.SAPPatientNumber.TrimStart(new char[] { '0' }),
					PatientStatus = item.PatientStatus,
					PatientType = item.PatientType,
					PatientNextAssessmentDate = item.PatientNextAssessmentDate,
					CareHomeId = item.CareHomeId,
					SAPCareHomeNumber = string.IsNullOrEmpty(item.SAPCareHomeNumber) == true ? item.SAPCareHomeNumber : item.SAPCareHomeNumber.TrimStart(new char[] { '0' }),
					CareHomeType = item.CareHomeType,
					CareHomeStatus = item.CareHomeStatus,
					CareHomeName = item.CareHomeName,
					OrderId = item.OrderId,
					OrderType = item.OrderType,
					OrderCreationType = item.OrderCreationType,
					SAPOrderNo = string.IsNullOrEmpty(item.SAPOrderNo) == true ? item.SAPOrderNo : item.SAPOrderNo.TrimStart(new char[] { '0' }),
					OrderDate = item.OrderDate,
					SAPOrderDeliveryDate = item.SAPOrderDeliveryDate,
					SAPActualDeliveryDate = item.SAPActualDeliveryDate,
					PODDate = item.PODDate,
					SapCustomerNumber = string.IsNullOrEmpty(item.SapCustomerNumber) == true ? item.SapCustomerNumber : item.SapCustomerNumber.TrimStart(new char[] { '0' }),
					CustomerStatus = item.CustomerStatus,
					CustomerUserName = item.CustomerUserName,
					CustomerAuthorizationPIN = item.CustomerAuthorizationPIN,
					CustomerAuthorizedUserFirstName = item.CustomerAuthorizedUserFirstName,
					CustomerAuthorizedUserLastName = item.CustomerAuthorizedUserLastName,
					CustomerAuthorizedUserJobTitle = item.CustomerAuthorizedUserJobTitle,
					CustomerAuthorizedUserTelephoneNumber = item.CustomerAuthorizedUserTelephoneNumber,
					CustomerAuthorizedUserMobileNumber = item.CustomerAuthorizedUserMobileNumber,
					CustomerAuthorizedUserEmail = item.CustomerAuthorizedUserEmail,
					PatientGender = item.PatientGender,
					PatientTitle = item.PatientTitle,
					PatientFirstName = item.PatientFirstName,
					PatientLastName = item.PatientLastName,
					PatientDateofBirth = item.PatientDateofBirth,
					PatientReasonCode = item.PatientReasonCode,
					PatientDeliveryFrequency = item.PatientDeliveryFrequency,
					PatientDeliveryAddressLine1 = item.PatientDeliveryAddressLine1,
					PatientDeliveryAddressLine2 = item.PatientDeliveryAddressLine2,
					PatientDeliveryCity = item.PatientDeliveryCity,
					PatientDeliveryCountry = item.PatientDeliveryCountry,
					PatientDeliveryHouseName = item.PatientDeliveryHouseName,
					PatientDeliveryHouseNumber = item.PatientDeliveryHouseNumber,
					PatientDeliveryPostCode = item.PatientDeliveryPostCode,
					PatientAddressAddressLine1 = item.PatientAddressAddressLine1,
					PatientAddressAddressLine2 = item.PatientAddressAddressLine2,
					PatientAddressCity = item.PatientAddressCity,
					PatientAddressCountry = item.PatientAddressCountry,
					PatientAddressHouseName = item.PatientAddressHouseName,
					PatientAddressHouseNumber = item.PatientAddressHouseNumber,
					PatientAddressPostCode = item.PatientAddressPostCode,
					PatientAssessmentDate = item.PatientAssessmentDate,
					PatientTelephoneNumber = item.PatientTelephoneNumber,
					PatientMobileNumber = item.PatientMobileNumber,
					PatientEmail = item.PatientEmail,
					PatientCommunicationFormat = item.PatientCommunicationFormat,
					PatientDeliveryDate = item.PatientDeliveryDate,
					PatientNextDeliveryDate = item.PatientNextDeliveryDate,
					PatientRoundId = item.PatientRoundId,
					PatientAdpLtc = item.PatientAdpLtc,
					PatientAdpNc = item.PatientAdpNc,
					PatientCreatedDate = item.PatientCreatedDate,
					PatientCreatedBy = item.PatientCreatedBy,
					PatientNhsId = item.PatientNhsId,
					PatientLocalId = item.PatientLocalId,
					PatientClinicalContacts = item.PatientClinicalContacts,
					PatientAnalysisInformation = item.PatientAnalysisInformation,
					PatientSAPCareHomeNumber = string.IsNullOrEmpty(item.PatientSAPCareHomeNumber) == true ? item.PatientSAPCareHomeNumber : item.PatientSAPCareHomeNumber.TrimStart(new char[] { '0' }),
					PatientCareHomeName = item.PatientCareHomeName,
					PrescriptionSapProductId = string.IsNullOrEmpty(item.PrescriptionSapProductId) == true ? item.PrescriptionSapProductId : item.PrescriptionSapProductId.TrimStart(new char[] { '0' }),
					PrescriptionProductDescription = item.PrescriptionProductDescription,
					PrescriptionFrequency = item.PrescriptionFrequency,
					PrescriptionAssessedPadsPerDay = item.PrescriptionAssessedPadsPerDay,
					PrescriptionActualPadsPerDay = item.PrescriptionActualPadsPerDay,
					PrescriptionDQ1 = item.PrescriptionDQ1,
					PrescriptionDQ2 = item.PrescriptionDQ2,
					PrescriptionDQ3 = item.PrescriptionDQ3,
					PrescriptionDQ4 = item.PrescriptionDQ4,
					PrescriptionValidFromDate = item.PrescriptionValidFromDate,
					PrescriptionValidToDate = item.PrescriptionValidToDate,
					PrescriptionStatus = item.PrescriptionStatus,
					PrescriptionDeliveryDate = item.PrescriptionDeliveryDate,
					PrescriptionNextDeliveryDate = item.PrescriptionNextDeliveryDate,
					CareHomeDeliveryFrequency = item.CareHomeDeliveryFrequency,
					CareHomeRoundId = item.CareHomeRoundId,
					CareHomeAddressLine1 = item.CareHomeAddressLine1,
					CareHomeAddressLine2 = item.CareHomeAddressLine2,
					CareHomeCity = item.CareHomeCity,
					CareHomeCountry = item.CareHomeCountry,
					CareHomeHouseName = item.CareHomeHouseName,
					CareHomeHouseNumber = item.CareHomeHouseNumber,
					CareHomePostCode = item.CareHomePostCode,
					CarehomeNextDeliveryDate = item.CarehomeNextDeliveryDate,
					CarehomePurchaseOrderNo = item.CarehomePurchaseOrderNo,
					OrderSAPDeliveryNo = item.OrderSAPDeliveryNo,
					OrderSAPProductID = string.IsNullOrEmpty(item.OrderSAPProductID) == true ? item.OrderSAPProductID : item.OrderSAPProductID.TrimStart(new char[] { '0' }),
					OrderProductDescription = item.OrderProductDescription,
					OrderQuantity = item.OrderQuantity,
					OrderInvoiceNo = item.OrderInvoiceNo,
					UserType = item.UserType,
					UserId = item.UserId,
					UserName = item.UserName,
					UserSapCustomerNo = string.IsNullOrEmpty(item.UserSapCustomerNo) == true ? item.UserSapCustomerNo : item.UserSapCustomerNo.TrimStart(new char[] { '0' }),
					UserCustomerName = item.UserCustomerName,
					UserRoleName = item.UserRoleName,
					ClinicalContactRoleName = item.ClinicalContactRoleName,
					ClinicalContactRoleId = item.ClinicalContactRoleId,
					ClinicalContactRoleValue = item.ClinicalContactRoleValue,
					ClinicalContactModifiedBy = item.ClinicalContactModifiedBy,
					ClinicalContactModifiedDate = item.ClinicalContactModifiedDate,
					ContinenceAdvisor = item.ContinenceAdvisor,
					Nurse = item.Nurse,
					NurseBase = item.NurseBase,
					HealthVisitor = item.HealthVisitor,
					HC = item.HC,
					Doctor = item.Doctor,
					GPPractice = item.GPPractice,
					CaseloadHolder = item.CaseloadHolder,
					PONumber = item.PONumber,
					TypeofIncontinence = item.TypeofIncontinence,
					MedicalCondition = item.MedicalCondition,
					School = item.School,
					Ethnicity = item.Ethnicity,
					Locality = item.Locality,
					Sector = item.Sector,
					HealthCentre = item.HealthCentre,
					BudgetCentre = item.BudgetCentre,
					IsDataProtected = item.IsDataProtected,
					RemovedStoppedDateTime = item.RemovedStoppedDateTime,
					Plant = item.Plant,
					BillingDate = item.BillingDate,
					Currency = item.Currency,
					BillingQuantity = item.BillingQuantity,
					BillingUnit = item.BillingUnit,
					DeliveryQuantity = item.DeliveryQuantity,
					DeliveryUnit = item.DeliveryUnit,
					NetValue = item.NetValue,
					VatValue = item.VatValue,
					TotalRows = totalRows
				}).ToList();
			}
			catch (System.Data.SqlClient.SqlException e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				isExceptionOccurred = true;
			}
			catch (DataException e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				isExceptionOccurred = true;
			}
			catch (ArgumentException e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				isExceptionOccurred = true;
			}
			catch (FormatException e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				isExceptionOccurred = true;
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				isExceptionOccurred = true;
			}
			catch (SystemException e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				isExceptionOccurred = true;
				throw;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, reportBuilderCriteria.UserId);
				throw;
			}
			finally
			{				
				if (isExceptionOccurred)
				{
					resultData = new List<ReportDataBusinessModel>() { new ReportDataBusinessModel() { IsExceptionOccurred = "true" } };
				}
			}

			return resultData;
		}
	}
}
