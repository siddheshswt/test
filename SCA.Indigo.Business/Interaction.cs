﻿namespace SCA.Indigo.Business
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Infrastructure.Repositories;
    using System;
    using System.Collections.Generic;
    using SCA.Indigo.Model;
    using EFModel = SCA.Indigo.Model;
    using System.Globalization;
    using System.Data;
    using System.ServiceModel;
    using System.Linq;
    using System.Text;
    using System.Configuration;
    /// <summary>
    /// 
    /// </summary>      
    public class Interaction : IInteraction
    {
        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = string.Empty;

        /// <summary>
        /// The date format
        /// </summary>
        public const string DateFormat = "dd/MM/yyyy";

        /// <summary>
        /// The automatic criteria identifier seperator
        /// </summary>
        private const char interactionReportIdSeperator = ';';       

        #region Interaction
        /// <summary>
        /// Gets the interaction summary details.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="interactionId">The interaction identifier.</param>
        /// <param name="assignedToId">The assigned to identifier.</param>
        /// <returns>
        /// List of Interaction Business Model.
        /// </returns>
        public List<InteractionBusinessModel> GetInteractionSummaryDetails(string patientId, string careHomeId, string customerId, string interactionId, string assignedToId)
        {
            List<InteractionBusinessModel> interactionslist = new List<InteractionBusinessModel>();
            InteractionBusinessModel interactionBusiness;
            long patient = Convert.ToInt64(patientId, CultureInfo.InvariantCulture);
            long customer = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
            long carehome = Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture);
            long interaction = Convert.ToInt64(interactionId, CultureInfo.InvariantCulture);
            Guid? assignedTo = null;
            if (!string.IsNullOrEmpty(assignedToId))
            {
                globalUserId = assignedToId;
                assignedTo = Guid.Parse(assignedToId);
            }

            try
            {
                var interactionSummaryDetails = (new DBHelper()).GetInteractionSummaryDetails(customer, patient, carehome, interaction, assignedTo);

                foreach (var data in interactionSummaryDetails)
                {
                    interactionBusiness = new InteractionBusinessModel();
                    interactionBusiness.InteractionId = data.InteractionId;
                    interactionBusiness.SAPCustomerId = data.SAPCustomerNumber;
                    interactionBusiness.CustomerName = data.CustomerName;
                    interactionBusiness.SAPPatientId = data.SAPPatientNumber;
                    interactionBusiness.CareHomeId = data.SAPCarehomeNumber;
                    interactionBusiness.Description = data.Description;
                    interactionBusiness.DisplayAs = data.DisplayAsAlert;
                    interactionBusiness.AssignedId = data.AssignToUserId != null ? Convert.ToString(data.AssignToUserId, CultureInfo.InvariantCulture) : null;
                    interactionBusiness.InteractionSubtypeId = Convert.ToString(data.InteractionSubTypeId, CultureInfo.InvariantCulture);
                    interactionBusiness.InteractionTypeId = Convert.ToString(data.InteractionTypeId, CultureInfo.InvariantCulture);
                    interactionBusiness.InteractionTypeCategory = data.InteractionCategory;
                    interactionBusiness.InteractionSubtypeCategory = data.InteractionSubTypeCategory;
                    interactionBusiness.CareHomeName = data.CareHomeName;
                    interactionBusiness.LogisticProviderName = data.LogisticProviderName;
                    interactionBusiness.PatientName = data.PatientName;
                    interactionBusiness.PatientId = Convert.ToString(data.PatientId, CultureInfo.InvariantCulture);
                    interactionBusiness.StatusId = data.StatusId;
                    interactionBusiness.Statustext = data.StatusDefaultText;
                    interactionBusiness.CreatedDate = data.CreatedDate;
                    interactionBusiness.InteractionId = data.InteractionId;
                    interactionBusiness.SAPCareHomeNumber = data.SAPCarehomeNumber;
                    interactionBusiness.CareHomeName = data.CareHomeName;
                    interactionBusiness.CreatedBy = data.CreatedBy;
                    interactionBusiness.CreatedDate = data.CreatedDate;
                    interactionBusiness.ModifiedBy = data.ModifiedBy;
                    interactionBusiness.ModifiedDate = data.ModifiedDate;
                    interactionBusiness.IsInteractionViewed = data.IsInteractionViewed != null ? (bool)data.IsInteractionViewed : true;
                    interactionslist.Add(interactionBusiness);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            return interactionslist;
        }

        /// <summary>
        /// Saves the interaction details.
        /// </summary>
        /// <param name="interactionBusinessModel">The interaction business model.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public bool SaveInteractionDetails(InteractionBusinessModel interactionBusinessModel)
        {
            IInteractionRepository interactionRepository = null;
            DateTime date = DateTime.Now;
            long patientId = 0;
            long carehomeId = 0;
            bool isInteractionChanged = true;
            try
            {
                interactionRepository = new InteractionRepository();
                if (interactionBusinessModel == null)
                {
                    return false;
                }

                EFModel.Interaction newInteraction = new EFModel.Interaction();
                if (interactionBusinessModel.InteractionId != 0)
                {
                    newInteraction = interactionRepository.Find(interactionBusinessModel.InteractionId);
                    isInteractionChanged = IsInteractionChanged(interactionBusinessModel, newInteraction);
                }

                globalUserId = interactionBusinessModel.UserId;
                newInteraction.InteractionId = interactionBusinessModel.InteractionId;
                newInteraction.InteractionTypeId = Convert.ToInt64(interactionBusinessModel.InteractionTypeId, CultureInfo.InvariantCulture);
                newInteraction.InteractionSubTypeId = Convert.ToInt64(interactionBusinessModel.InteractionSubtypeId, CultureInfo.InvariantCulture);
                newInteraction.DisplayAsAlert = interactionBusinessModel.DisplayAs;
                if (interactionBusinessModel.StatusId == Convert.ToInt64(SCA.Indigo.Common.Enums.SCAEnums.InteractionStatus.Completed,CultureInfo.InvariantCulture))
                {
                    if (isInteractionStatusChanged(newInteraction.StatusId, interactionBusinessModel.StatusId))
                    {
                        newInteraction.ClosedBy = Guid.Parse(interactionBusinessModel.UserId);
                        newInteraction.ClosedDate = date;
                    }
                }
                else
                {
                    newInteraction.ClosedBy = (Guid?)null;
                    newInteraction.ClosedDate = (DateTime?)null;
                }
                newInteraction.StatusId = interactionBusinessModel.StatusId;
                newInteraction.Description = interactionBusinessModel.Description;
                newInteraction.CustomerId = Convert.ToInt64(interactionBusinessModel.CustomerId, CultureInfo.InvariantCulture);
                newInteraction.OrderId = interactionBusinessModel.OrderId;
                if (isResolvedChanged(newInteraction.Resolved, interactionBusinessModel.Resolved))
                {
                    newInteraction.ResolvedBy = interactionBusinessModel.Resolved == true ? Guid.Parse(interactionBusinessModel.UserId) : (Guid?)null;
                    newInteraction.ResolvedDate = interactionBusinessModel.Resolved == true ? date : (DateTime?)null;
                }
                
                newInteraction.Resolved = interactionBusinessModel.Resolved;

                if (!string.IsNullOrEmpty(interactionBusinessModel.PatientId))
                {
                    patientId = Convert.ToInt64(interactionBusinessModel.PatientId, CultureInfo.InvariantCulture);
                    newInteraction.PatientId = Convert.ToInt64(interactionBusinessModel.PatientId, CultureInfo.InvariantCulture);
                }

                if (!string.IsNullOrEmpty(interactionBusinessModel.CareHomeId))
                {
                    carehomeId = Convert.ToInt64(interactionBusinessModel.CareHomeId, CultureInfo.InvariantCulture);
                    newInteraction.CarehomeId = Convert.ToInt64(interactionBusinessModel.CareHomeId, CultureInfo.InvariantCulture);
                }

                
                if (!string.IsNullOrEmpty(interactionBusinessModel.AssignedId))
                {
                    if (interactionBusinessModel.UserId != interactionBusinessModel.AssignedId)
                        newInteraction.IsInteractionViewed = false;
                    newInteraction.AssignToUserId = Guid.Parse(interactionBusinessModel.AssignedId);
                }

                if (newInteraction.InteractionId != 0)
                {
                    newInteraction.ModifiedDate = date;
                    newInteraction.ModifiedBy = Guid.Parse(interactionBusinessModel.UserId);
                }
                else
                {
                    newInteraction.CreatedDate = date;
                    newInteraction.CreatedBy = Guid.Parse(interactionBusinessModel.UserId);
                }

                interactionRepository.InsertOrUpdate(newInteraction);
                IUnitOfWork interactionUnitOfWork = interactionRepository.UnitOfWork;
                interactionUnitOfWork.Commit();

                if ((bool)interactionBusinessModel.DisplayAs && interactionBusinessModel.UserId != interactionBusinessModel.AssignedId && isInteractionChanged)
                {
                    sendInteractionEmail(patientId, carehomeId, interactionBusinessModel.AssignedId);
                }

                return true;
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            finally
            {
                if (interactionRepository != null)
                {
                    interactionRepository.Dispose();
                }

            }

            return false;
        }

        /// <summary>
        /// Gets the name of the logistic provider.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// Gets the Logistic Provider Name.
        /// </returns>
        public string GetLogisticProviderName(string customerId)
        {
            ICustomerRepository customerRepository = null;
            globalUserId = string.Empty;
            string logisticProviderName = string.Empty;
            try
            {
                long cId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);

                if (cId > 0)
                {
                    customerRepository = new CustomerRepository();
                    var customerDetails = customerRepository.GetCustomerById(cId);
                    logisticProviderName = customerDetails.Transport_Company;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }

            return logisticProviderName;
        }

        /// <summary>
        /// Gets the create interaction by interaction identifier.
        /// </summary>
        /// <param name="interactionId">The interaction identifier.</param>
        /// <param name="assignedToId">The assigned to identifier.</param>
        /// <returns>
        /// List of Interaction Business Model.
        /// </returns>
        public List<InteractionBusinessModel> GetCreateInteractionByInteractionId(string interactionId, string assignedToId, string userId)
        {
            IInteractionRepository interactionRepository = null;
            List<InteractionBusinessModel> interactionBusinesslist = new List<InteractionBusinessModel>();
            if (!string.IsNullOrEmpty(userId))
                this.globalUserId = userId;
            else
                this.globalUserId = "";
            try
            {
                var interaction = Convert.ToInt64(interactionId, CultureInfo.InvariantCulture);
                var assignedToGuId = Guid.Parse(assignedToId);
                var query = (new DBHelper()).GetInteractionSummaryDetails(0, 0, 0, interaction, assignedToGuId, userId);
                foreach (var obj in query)
                {
                    InteractionBusinessModel interactionBusiness = new InteractionBusinessModel();
                    interactionBusiness.InteractionTypeId = obj.InteractionTypeId.ToString(CultureInfo.InvariantCulture);
                    interactionBusiness.InteractionSubtypeId = obj.InteractionSubTypeId.ToString(CultureInfo.InvariantCulture);
                    interactionBusiness.LogisticProviderName = obj.LogisticProviderName;
                    interactionBusiness.DisplayAs = obj.DisplayAsAlert;
                    interactionBusiness.StatusId = obj.StatusId;
                    interactionBusiness.Description = obj.Description;
                    interactionBusiness.CustomerId = Convert.ToString(obj.CustomerId, CultureInfo.InvariantCulture);
                    interactionBusiness.CustomerName = obj.CustomerName;
                    interactionBusiness.SAPCustomerId = obj.SAPCustomerNumber;
                    interactionBusiness.PatientId = Convert.ToString(obj.PatientId, CultureInfo.InvariantCulture);
                    interactionBusiness.PatientName = obj.PatientName;
                    interactionBusiness.SAPPatientId = obj.SAPPatientNumber;
                    interactionBusiness.AssignedId = Convert.ToString(obj.AssignToUserId, CultureInfo.InvariantCulture);
                    interactionBusiness.CreatedBy = obj.CreatedBy;
                    interactionBusiness.CreatedDate = obj.CreatedDate;
                    interactionBusiness.ModifiedBy = obj.ModifiedBy;
                    interactionBusiness.ModifiedDate = obj.ModifiedDate;
                    interactionBusiness.CareHomeId = Convert.ToString(obj.CareHomeId, CultureInfo.InvariantCulture);
                    interactionBusiness.CareHomeName = !string.IsNullOrEmpty(obj.SAPCarehomeNumber) ? obj.CareHomeName + "(" + CommonHelper.TrimLeadingZeros(obj.SAPCarehomeNumber) + ")" : obj.CareHomeName;
                    interactionBusiness.SAPCareHomeNumber = obj.SAPCarehomeNumber;
                    interactionBusiness.Resolved = obj.Resolved;
                    interactionBusiness.ResolvedBy = obj.Resolved == true ? obj.ResolvedBy : null;
                    interactionBusiness.ResolvedDate = obj.ResolvedDate;
                    interactionBusiness.ClosedBy = obj.ClosedBy;
                    interactionBusiness.ClosedDate = obj.ClosedDate;
                    interactionBusiness.IsBtnEnabled = Convert.ToBoolean(obj.IsBtnEnabled, CultureInfo.InvariantCulture);
                    interactionBusinesslist.Add(interactionBusiness);
                }

                if (!string.IsNullOrEmpty(interactionId))
                {
                    interactionRepository = new InteractionRepository();
                    EFModel.Interaction newInteraction = new EFModel.Interaction();
                    newInteraction = interactionRepository.Find(Convert.ToInt64(interactionId, CultureInfo.InvariantCulture));

                    if (newInteraction != null ? Convert.ToString(newInteraction.AssignToUserId, CultureInfo.InvariantCulture).ToUpper(CultureInfo.InvariantCulture) == this.globalUserId.ToUpper(CultureInfo.InvariantCulture) : false)
                        newInteraction.IsInteractionViewed = true;

                    interactionRepository.InsertOrUpdate(newInteraction);

                    IUnitOfWork interactionUnitOfWork = interactionRepository.UnitOfWork;
                    interactionUnitOfWork.Commit();
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, this.globalUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, this.globalUserId);
                throw;
            }
            finally
            {
                if (interactionRepository != null)
                {
                    interactionRepository.Dispose();
                }
            }
            return interactionBusinesslist;
        }
        #endregion

        #region Interaction Report

        /// <summary>
        /// This function is use to get interaction report summary
        /// </summary>
        /// <param name="interactReportDetailsBusModel">
        /// object of InteractReportDetailsBusinessModel class
        /// </param>
        /// <returns></returns>
        public List<InteractionReportSummaryBusinessModel> GetInteractionReportSummary(InteractionReportDetailsBusinessModel interactReportDetailsBusModel)
        {
            string userId = string.Empty;
            StringBuilder carehomeIds = new StringBuilder();
            StringBuilder patientIds = new StringBuilder();
            ICareHomeRepository careHomeRepository = null;
            IPatientRepository patientRepository = null;
            if (interactReportDetailsBusModel != null)
            {
                try
                {
                    userId = interactReportDetailsBusModel.UserId;
                    Nullable<DateTime> createdDtFrom = null;
                    if (!string.IsNullOrEmpty(interactReportDetailsBusModel.CreatedDateFrom))
                    {
                        createdDtFrom = DateTime.ParseExact(interactReportDetailsBusModel.CreatedDateFrom, DateFormat, CultureInfo.InvariantCulture);
                    }
                    Nullable<DateTime> createdDateTo = null;
                    if (!string.IsNullOrEmpty(interactReportDetailsBusModel.CreatedDateTo))
                    {
                        createdDateTo = DateTime.ParseExact(interactReportDetailsBusModel.CreatedDateTo, DateFormat, CultureInfo.InvariantCulture);

                    }
                    Nullable<DateTime> modifiedDateFrom = null;
                    if (!string.IsNullOrEmpty(interactReportDetailsBusModel.ModifiedDateFrom))
                    {
                        modifiedDateFrom = DateTime.ParseExact(interactReportDetailsBusModel.ModifiedDateFrom, DateFormat, CultureInfo.InvariantCulture);

                    }
                    Nullable<DateTime> modifiedDateTo = null;
                    if (!string.IsNullOrEmpty(interactReportDetailsBusModel.ModifiedDateTo))
                    {
                        modifiedDateTo = DateTime.ParseExact(interactReportDetailsBusModel.ModifiedDateTo, DateFormat, CultureInfo.InvariantCulture);
                    }

                    #region CarehomeId
                    if (!string.IsNullOrEmpty(interactReportDetailsBusModel.CareHomeIds))
                    {
                        interactReportDetailsBusModel.CareHomeIds = interactReportDetailsBusModel.CareHomeIds.Trim();
                        var careHomesIdsList = interactReportDetailsBusModel.CareHomeIds.Split(interactionReportIdSeperator).ToList();
                        if (careHomesIdsList.Any())
                        {
                            careHomesIdsList = careHomesIdsList.Select(q => CommonHelper.AppendZeroAtStart(q.Trim(), 10)).ToList(); //SAP ID Length is 10
                            careHomeRepository = new CareHomeRepository();
                            var careHomes = careHomeRepository.GetCareHomesBySAPId(careHomesIdsList);

                            var invalidCareHomes = new List<string>();//For validation (Need to show error message in front end)
                            if (!careHomes.Any())
                            {
                                invalidCareHomes.AddRange(careHomesIdsList);
                                int count = 0;
                                foreach (var careHomesId in careHomesIdsList)
                                {
                                    carehomeIds.Append(careHomesId);
                                    if (count < careHomesIdsList.Count - 1)
                                    {
                                        carehomeIds.Append(",");
                                    }
                                    count++;
                                }
                            }
                            else
                            {
                                int count = 0;
                                foreach (var sapCarehomeId in careHomesIdsList)
                                {
                                    if (!careHomes.Where(q => q.SAPCareHomeNumber.Contains(sapCarehomeId)).Any())
                                    {
                                        invalidCareHomes.Add(sapCarehomeId);
                                        carehomeIds.Append(sapCarehomeId);
                                        if (count < careHomesIdsList.Count - 1)
                                        {
                                            carehomeIds.Append(",");
                                        }
                                    }
                                    else
                                    {
                                        carehomeIds.Append(sapCarehomeId);
                                        if (count < careHomesIdsList.Count - 1)
                                        {
                                            carehomeIds.Append(",");
                                        }
                                    }
                                    count++;
                                }
                            }
                        }
                    }
                    #endregion

                    #region Patients
                    if (!string.IsNullOrEmpty(interactReportDetailsBusModel.PatientIds))
                    {
                        interactReportDetailsBusModel.PatientIds = interactReportDetailsBusModel.PatientIds.Trim();
                        var patientIdsList = interactReportDetailsBusModel.PatientIds.Split(interactionReportIdSeperator).ToList();
                        if (patientIdsList.Any())
                        {
                            patientIdsList = patientIdsList.Select(q => CommonHelper.AppendZeroAtStart(q.Trim(), 10)).ToList(); //SAP ID Length is 10
                            patientRepository = new PatientRepository();
                            var patients = patientRepository.GetPatientsBySAPId(patientIdsList);

                            var invalidPatients = new List<string>(); //For validation (Need to show error message in front end)
                            if (!patients.Any())
                            {
                                invalidPatients.AddRange(patientIdsList);
                                int count = 0;
                                foreach (var patientId in patientIdsList)
                                {
                                    patientIds.Append(patientId);
                                    if (count < patientIdsList.Count - 1)
                                    {
                                        patientIds.Append(",");
                                    }
                                    count++;
                                }
                            }
                            else
                            {
                                int count = 0;
                                foreach (var sapPatientId in patientIdsList)
                                {
                                    if (!patients.Where(q => q.SAPPatientNumber.Contains(sapPatientId)).Any())
                                    {
                                        invalidPatients.Add(sapPatientId);
                                        patientIds.Append(sapPatientId);
                                        if (count < patientIdsList.Count - 1)
                                        {
                                            patientIds.Append(",");
                                        }
                                    }
                                    else
                                    {
                                        patientIds.Append(sapPatientId);
                                        if (count < patientIdsList.Count - 1)
                                        {
                                            patientIds.Append(",");
                                        }
                                    }
                                    count++;
                                }
                            }
                        }
                    }
                    #endregion

                    var objInteractionReport = (new DBHelper()).GetInteractionReportSumDetails(
                                                        interactReportDetailsBusModel.RowNumber,
                                                        interactReportDetailsBusModel.TotalRecords,
                                                        interactReportDetailsBusModel.IsExport,
                                                        interactReportDetailsBusModel.CustomersIds,
                                                        patientIds == null ? string.Empty : patientIds.ToString(),
                                                        carehomeIds == null ? string.Empty : carehomeIds.ToString(),
                                                        interactReportDetailsBusModel.InteractionTypeId,
                                                        interactReportDetailsBusModel.InteractionSubTypeId,
                                                        interactReportDetailsBusModel.StatusId,
                                                        interactReportDetailsBusModel.CreatedBy,
                                                        createdDtFrom,
                                                        createdDateTo,
                                                        modifiedDateFrom,
                                                        modifiedDateTo,
                                                        interactReportDetailsBusModel.SortColumn,
                                                        interactReportDetailsBusModel.SortBy,
                                                        userId
                                                        );
                    if (objInteractionReport.Any())
                    {
                        var interactionReport = objInteractionReport.Select(q => new InteractionReportSummaryBusinessModel
                        {
                            RowNumber = q.RowNumber.Value,
                            TotalRecords = q.TotalRecords.Value,
                            CustomerName = q.CustomerName,
                            CustomerId = string.IsNullOrEmpty(q.CustomerId) ? string.Empty : CommonHelper.TrimLeadingZeros(q.CustomerId),
                            PatientName = q.PatientName.ToString(),
                            PatientId = string.IsNullOrEmpty(q.PatientId) ? string.Empty : CommonHelper.TrimLeadingZeros(q.PatientId),
                            CareHomeName = q.CareHomeName,
                            CareHomeId = string.IsNullOrEmpty(q.CareHomeId) ? string.Empty : CommonHelper.TrimLeadingZeros(q.CareHomeId),
                            InteractionType = q.InteractionType,
                            SubType = q.SubType,
                            StatusId = q.StatusId,
                            CreatedOn = q.CreatedOn,
                            CreatedBy = q.CreatedBy,
                            ModifiedOn = q.ModifiedOn,
                            ModifiedBy = q.ModifiedBy,
                            Alert = q.Alert,
                            AssignedTo = q.AssignedTo,
                            Description = q.Description
                        }).ToList();
                        return interactionReport;
                    }
                    return new List<InteractionReportSummaryBusinessModel>();
                }
                catch (DataException dataException)
                {
                    LogHelper.LogException(dataException, userId);
                }
                catch (ArgumentException argumentException)
                {
                    LogHelper.LogException(argumentException, userId);
                }
                catch (FormatException formatException)
                {
                    LogHelper.LogException(formatException, userId);
                }
                catch (InvalidOperationException invalidOperationException)
                {
                    LogHelper.LogException(invalidOperationException, userId);
                }
                catch (SystemException systemException)
                {
                    LogHelper.LogException(systemException, userId);
                    throw;
                }
                catch (Exception exception)
                {
                    LogHelper.LogException(exception, userId);
                    throw;
                }
                finally
                {
                    if (patientRepository != null)
                    {
                        patientRepository.Dispose();
                    }
                    if (careHomeRepository != null)
                    {
                        careHomeRepository.Dispose();
                    }
                }
            }
            return new List<InteractionReportSummaryBusinessModel>();
        }
        #endregion

        /// <summary>
        /// Triggers Email Whenever interaction is assigned to user
        /// </summary>
        /// <param name="strPatientId"></param>
        /// <param name="strAssignedTo"></param>
        /// <returns></returns>
        private bool sendInteractionEmail(long patientId, long carehomeId, string strAssignedTo) 
        {
            UsersRepository userRepository = null;
            IPatientRepository patientRepository = null;
            ICareHomeRepository carehomeRepository = null;
            EFModel.Patient objPatient = null;
            EFModel.CareHome objCareHome = null;

            bool result = false;
            try
            {
                userRepository = new UsersRepository();

                var objUser = userRepository.GetUsersByUserId(strAssignedTo);

                if (objUser.Any())
                {
                    EmailBusinessModel email = new EmailBusinessModel();
                    email.FromEmailId = ConfigurationManager.AppSettings["EmailFrom"];
                    email.ToEmailId.Add(objUser.FirstOrDefault().GroupEmailId);
                    email.Subject = string.Format(EmailTemplate.InteractionAssignedSubject, objUser.FirstOrDefault().UserName);

                    var body = EmailTemplate.InteractionAssignedEmailBody;
                    var sapNo = "";
                    if (patientId != 0)
                    {
                        patientRepository = new PatientRepository();
                        objPatient = patientRepository.Find(patientId);

                        if (objPatient != null)
                            sapNo += string.Format(CultureInfo.InvariantCulture, EmailTemplate.InteractionPatientId, CommonHelper.TrimLeadingZeros(objPatient.SAPPatientNumber));
                        
                        if (objPatient.CareHome != null)
                            sapNo += string.Format(CultureInfo.InvariantCulture, EmailTemplate.InteractionCareHomeId, CommonHelper.TrimLeadingZeros(objPatient.CareHome.SAPCareHomeNumber));
                    }
                    else if (carehomeId != 0)
                    {
                        carehomeRepository = new CareHomeRepository();
                        objCareHome = carehomeRepository.Find(carehomeId);
                        if (objCareHome != null)
                            sapNo += string.Format(CultureInfo.InvariantCulture, EmailTemplate.InteractionCareHomeId, CommonHelper.TrimLeadingZeros(objCareHome.SAPCareHomeNumber));
                    }
                    

                    email.Body = string.Format(CultureInfo.InvariantCulture, body, objUser.FirstOrDefault().UserName, sapNo);

                    result = email.Send();
                }
            }
            catch (DataException dataException)
            {
                LogHelper.LogException(dataException, this.globalUserId);
            }
            catch (ArgumentNullException argumentNULLException)
            {
                LogHelper.LogException(argumentNULLException, this.globalUserId);
            }
            catch (ArgumentException argumentException)
            {
                LogHelper.LogException(argumentException, this.globalUserId);
            }
            catch (FormatException formatException)
            {
                LogHelper.LogException(formatException, this.globalUserId);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                LogHelper.LogException(invalidOperationException, this.globalUserId);
            }
            catch (SystemException systemException)
            {
                LogHelper.LogException(systemException, this.globalUserId);
                throw;
            }
            catch (Exception exception)
            {
                LogHelper.LogException(exception, this.globalUserId);
                throw;
            }
            finally
            {
                if (userRepository != null)
                    userRepository.Dispose();

                if (patientRepository != null)
                    patientRepository.Dispose();

                if (carehomeRepository != null)
                    carehomeRepository.Dispose();
            }
            return result;
        }

        /// <summary>
        /// is Resolved Changed
        /// </summary>
        /// <param name="changedResolvedStatus"></param>
        /// <param name="previousResolvedStatus"></param>
        /// <returns></returns>
        private bool isResolvedChanged(bool? changedResolvedStatus, bool? previousResolvedStatus)
        {
            bool isResolvedChanged = false;
            try
            {
                isResolvedChanged = previousResolvedStatus == changedResolvedStatus ? isResolvedChanged : true;                
            }
            catch (DataException dataException)
            {
                LogHelper.LogException(dataException, this.globalUserId);
            }
            catch (ArgumentNullException argumentNULLException)
            {
                LogHelper.LogException(argumentNULLException, this.globalUserId);
            }
            catch (ArgumentException argumentException)
            {
                LogHelper.LogException(argumentException, this.globalUserId);
            }
            catch (FormatException formatException)
            {
                LogHelper.LogException(formatException, this.globalUserId);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                LogHelper.LogException(invalidOperationException, this.globalUserId);
            }
            catch (SystemException systemException)
            {
                LogHelper.LogException(systemException, this.globalUserId);
                throw;
            }
            catch (Exception exception)
            {
                LogHelper.LogException(exception, this.globalUserId);
                throw;
            }
            return isResolvedChanged;
        }

        /// <summary>
        /// isInteractionStatusChanged
        /// </summary>
        /// <param name="newInteractionStatus"></param>
        /// <param name="previousInteractionStatus"></param>
        /// <returns></returns>
        private bool isInteractionStatusChanged(long newInteractionStatus, long previousInteractionStatus)
        {
            bool isInteractionChanged = false;
            try
            {
                isInteractionChanged = previousInteractionStatus == newInteractionStatus ? isInteractionChanged : true;
            }
            catch (DataException dataException)
            {
                LogHelper.LogException(dataException, this.globalUserId);
            }
            catch (ArgumentNullException argumentNULLException)
            {
                LogHelper.LogException(argumentNULLException, this.globalUserId);
            }
            catch (ArgumentException argumentException)
            {
                LogHelper.LogException(argumentException, this.globalUserId);
            }
            catch (FormatException formatException)
            {
                LogHelper.LogException(formatException, this.globalUserId);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                LogHelper.LogException(invalidOperationException, this.globalUserId);
            }
            catch (SystemException systemException)
            {
                LogHelper.LogException(systemException, this.globalUserId);
                throw;
            }
            catch (Exception exception)
            {
                LogHelper.LogException(exception, this.globalUserId);
                throw;
            }
            return isInteractionChanged;
        }

        /// <summary>
        /// Checks for any changes in interaction
        /// </summary>
        /// <param name="newInteraction"></param>
        /// <param name="oldInteraction"></param>
        /// <returns></returns>
        public bool IsInteractionChanged(InteractionBusinessModel newInteraction, EFModel.Interaction oldInteraction)
        {
            bool isChanged = false;
            Nullable<Guid> oldAssignedId = newInteraction.AssignedId == null ? (Guid?)null : Guid.Parse(newInteraction.AssignedId);

            if (oldInteraction.InteractionTypeId != Convert.ToInt64(newInteraction.InteractionTypeId, CultureInfo.InvariantCulture) ||
                oldInteraction.InteractionSubTypeId != Convert.ToInt64(newInteraction.InteractionSubtypeId, CultureInfo.InvariantCulture) ||
                oldInteraction.Resolved != newInteraction.Resolved ||
                oldInteraction.StatusId != newInteraction.StatusId ||
                !string.Equals(oldInteraction.Description.Trim(), newInteraction.Description.Trim()) ||
                oldInteraction.DisplayAsAlert != newInteraction.DisplayAs ||
                oldInteraction.AssignToUserId != oldAssignedId)
                isChanged = true;

            return isChanged;
        }
    }
}
