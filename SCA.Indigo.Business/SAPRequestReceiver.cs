﻿//-----------------------------------------------------------------------
// <copyright file="SAPRequestReceiver.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Hosting;
    using System.Xml.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Business.Interfaces;
    using SCA.Indigo.Common;
    using SCA.Indigo.Model;
    using SCA.Indigo.Model.RequestParameter;
    using System.ServiceModel;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Infrastructure.Repositories;

    /// <summary>
    /// It will handle all SAP Request
    /// </summary>    
    public class SAPRequestReceiver : ISapReceiver
    {
        /// <summary>
        /// The sap user identifier
        /// </summary>
        private static string sapUserId;

        /// <summary>
        /// The product Ids identifier
        /// </summary>
        private string productIds;

        /// <summary>
        /// The sap request log file name
        /// </summary>
        private string sapRequestLogFileName = "SAPRequestLog";

        /// <summary>
        /// The customer product log file name
        /// </summary>
        private string customerProductLogFileName = "CustomerProductLog";

        /// <summary>
        /// The sap response for success order
        /// </summary>
        private string successCode = "S";

        /// <summary>
        /// The object common service
        /// </summary>
        private CommonService objCommonService = null;

        /// <summary>
        /// Sets whether log is to be written to the text file OR not
        /// </summary>
        private bool writeOnlineOrderLog = Convert.ToBoolean(ConfigurationManager.AppSettings["GenrateOnlineOrderXmlLog"], CultureInfo.InvariantCulture);

        /// <summary>
        /// Sets whether log is to be written to the text file OR not
        /// </summary>
        private bool writeCustomerProductLog = Convert.ToBoolean(ConfigurationManager.AppSettings["GenareteCustomerProductXml"], CultureInfo.InvariantCulture);


        /// <summary>
        /// online Order Log FileName
        /// </summary>
        private string onlineOrderLogFileName = "OnlineOrdersXmlLog";

        /// <summary>
        /// online Order Log FileName
        /// </summary>
        private string customerProductXmlLogFileName = "customerProductXmlLog";


        /// <summary>
        /// Flag to check if incremental update required
        /// </summary>
        private bool isIncrementalUpdateRequired;

        /// <summary>
        /// Prevents a default instance of the <see cref="SAPRequestReceiver"/> class from being created.
        /// </summary>
        public SAPRequestReceiver()
        {
            GetSAPUserDetails();
        }

        /// <summary>
        /// One off standard orders for non-carehome patient and carehome patient
        /// </summary>
        long[] oneoffStandardOrders = new long[] {(long)SCAEnums.OrderCreationType.CommunityOneOffStandard,
                                                    (long)SCAEnums.OrderCreationType.CarehomePatientOneOffStandard};


        /// <summary>
        /// Save customer information with product
        /// </summary>
        /// <param name="objXElement">Instance of SAPParameterBusinessModel</param>
        public void SaveCustomerProductList(XElement objXElement)
        {
            // It will be used if any exception will be caught during execution
            var objSAPResultBusinessModel = new SAPCustomerProductResultBusinessModel();
            var listSAPResultBusinessModel = new List<SAPCustomerProductResultBusinessModel>();
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            SAPCustomerBusinessModel objSapCustomer = null;
            SAPProductBusinessModel objSapProduct = null;
            bool errorOccured = false;
            try
            {
                if (objXElement != null)
                {
                    var doc = new XDocument(objXElement);

                    if (this.writeCustomerProductLog)
                    {
                        LogToFile(doc.ToString(), this.customerProductXmlLogFileName);
                    }
                    LogToFile(doc.ToString(), this.customerProductXmlLogFileName);
                    var customers = from result in doc.Descendants("CUSTOMERPRODUCTLIST").Descendants("CUSTOMER")
                                    select result;

                    // Create Result message object for consumer
                    var productInserted = 0;
                    var productUpdated = 0;
                    var productFailed = 0;
                    foreach (var customer in customers.Where(customer => customer != null))
                    {
                        productInserted = 0;
                        productUpdated = 0;
                        objSapCustomer = new SAPCustomerBusinessModel();

                        objSapCustomer = SaveCustomerData(customer);
                        productIds = string.Empty;
                        if (objSapCustomer.CustomerId > 0)
                        {
                            var products = from result in doc.Descendants("PRODLIST").Descendants("PRODUCT")
                                           select result;
                            foreach (var product in products.Where(product => product != null))
                            {

                                objSapProduct = new SAPProductBusinessModel
                                {
                                    CustomerId = objSapCustomer.CustomerId,
                                    SAPProductId = product.Element("MATERIAL").Value,
                                    BaseMaterial = product.Element("BASEMATERIAL").Value,
                                    SAPProductName = product.Element("MATERIALTEXT").Value,
                                    NetValue = product.Element("NETVALUE").Value,
                                    VAT = product.Element("VAT").Value,
                                    SalesUnit = product.Element("SALESUNIT").Value,
                                    PackSize = product.Element("PACKSIZE").Value
                                };
                                // Save product details
                                SaveProductData(objSapProduct);

                                // Set value for inserted product
                                productInserted += objSapProduct.ProductInserted;
                                productUpdated += objSapProduct.ProductUpdated;
                                productFailed += objSapProduct.ProductFailed;

                            }
                            DeleteIDXCustomerProduct(objSapCustomer.CustomerId);
                        }

                        // Intialize SAPResultBusinessModel object with value
                        objSAPResultBusinessModel = new SAPCustomerProductResultBusinessModel
                        {
                            FrontEndCustomerId = objSapCustomer.CustomerId,
                            SAPCustomerNo = objSapCustomer.SAPCustomerNo,
                            ProductInserted = productInserted,
                            ProductUpdated = productUpdated,
                            ProductFailed = productFailed
                        };
                        listSAPResultBusinessModel.Add(objSAPResultBusinessModel);
                    }
                }

                // return listSAPResultBusinessModel;
            }
            catch (DataException e)
            {
                errorOccured = true;
                LogHelper.LogException(e, sapUserId);
                objSAPResultBusinessModel.ExceptionMessage = e.Message;
            }
            catch (Exception e)
            {
                errorOccured = true;
                objSAPResultBusinessModel.ExceptionMessage = e.Message;
            }

            endTime = DateTime.Now;
            if (errorOccured)
            {
                listSAPResultBusinessModel.Add(objSAPResultBusinessModel);
            }

            SaveCustomerPorductLog(listSAPResultBusinessModel, startTime, endTime);
        }

        /// <summary>
        /// Delete product from IDXCustomer Product which are not assigned to customer
        /// </summary>        
        /// <param name="customerId">long customer Id</param>        
        private void DeleteIDXCustomerProduct(long customerId)
        {
            try
            {
                if (!string.IsNullOrEmpty(productIds))
                {
                    productIds = productIds.Remove(0,1);
                    new DBHelper().DeleteIDXCustomerProduct(productIds, customerId);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Patient or Carehome SAP Id
        /// </summary>
        /// <param name="objXElement">XElement objXElement</param>
        public void CustomerResponse(XElement objXElement)
        {
            /// Initialize Loger varriable
            StringBuilder strMessage = new StringBuilder();
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            try
            {
                if (objXElement != null)
                {
                    var doc = new XDocument(objXElement);
                    var customers = from result in doc.Descendants("CUSTRESPONSE").Descendants("item")
                                    select result;

                    /// Get SAP UserDetails
                    GetSAPUserDetails();
                    foreach (var customer in customers.Where(customer => customer != null))
                    {
                        var frontendId = customer.Element("FRONTENDID");
                        var sapId = customer.Element("SAPID");
                        var customerType = customer.Element("CUSTOMERTYPE");
                        var errorCode = customer.Element("ERRORCODE");
                        var errorMessage = customer.Element("ERRORMESSAGE");

                        if (errorCode.Value == "S" && (!string.IsNullOrEmpty(frontendId.Value)) && (!string.IsNullOrEmpty(sapId.Value)))
                        {
                            var data = new DBHelper().CustomerResponse(Convert.ToInt64(frontendId.Value, CultureInfo.CurrentCulture), sapId.Value, customerType.Value);
                            strMessage.Append("[FRONTENDID=" + frontendId.Value + "]    [SAPID=" + sapId.Value + "]     [Message=" + data.ResultMessage + "] \n");
                        }
                        else
                        {
                            strMessage.Append("[FRONTENDID=" + frontendId.Value + "]    [SAPID=" + sapId.Value + "]     [Message=" + errorMessage.Value + "] \n");
                        }
                    }

                    strMessage = strMessage.Remove(strMessage.Length - 2, 2);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
                strMessage.Append(e.Message);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                strMessage.Append(e.Message);
            }

            endTime = DateTime.Now;
            LogToFile(startTime, endTime, strMessage.ToString(), sapRequestLogFileName);
        }

        /// <summary>
        /// Update Order details
        /// </summary>
        /// <param name="objXElement">XElement objXElement</param>
        public void OrderResponse(XElement objXElement)
        {
            /// Initialize Loger varriable
            StringBuilder strMessage = new StringBuilder();
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            var isCarehomeOrder = false;

            try
            {
                if (objXElement != null)
                {
                    var doc = new XDocument(objXElement);
                    var customers = from result in doc.Descendants("ORDERRESPONSE").Descendants("item")
                                    select result;

                    /// Get SAP UserDetails
                    objCommonService = new CommonService();

                    GetSAPUserDetails();
                    foreach (var customer in customers.Where(customer => customer != null))
                    {
                        var frontendId = customer.Element("FRONTENDID");
                        var orderNo = customer.Element("ORDERNO");
                        var delivery = customer.Element("DELIVERY");
                        var invoice = customer.Element("INVOICE");
                        var errorCode = customer.Element("ERRORCODE");
                        var errorMessage = customer.Element("ERRORMESSAGE");                        
                        // new fields added //
                        var baseMaterialId = customer.Element("BASEMATERIAL");
                        var actDelDate = customer.Element("ACTDELDATE");
                        var plant = customer.Element("PLANT");
                        var billingDate = customer.Element("BILLINGDATE");
                        var currency = customer.Element("CURRENCY");
                        var billingQuantity = customer.Element("BILLINGQTY");
                        var billingUnit = customer.Element("BILLINGUNIT");
                        var netValue = customer.Element("NETVALUE");
                        var vatValue = customer.Element("VATVALUE");                        
                        var podDate = customer.Element("PODDATE");
                        var deliveryQuantity = customer.Element("DELIVERYQTY");
                        var deliveryUnit = customer.Element("DELIVERYUNIT");
                        var orderItem = customer.Element("ORDERITEM");                        
                      
                        ////filled only if DELIVERY is filled                        
                       if (string.IsNullOrEmpty(delivery.Value))
                       {                           
                           actDelDate.Value = plant.Value = podDate.Value = deliveryQuantity.Value = deliveryUnit.Value = String.Empty;                             
                       }
                       else
                       {
                           deliveryQuantity.Value = deliveryQuantity.Value.Replace(",", string.Empty);
                       }                    
                       
                        //filled only if INVOICE is filled
                        if (string.IsNullOrEmpty(invoice.Value))
                        {
                            billingDate.Value = currency.Value = billingQuantity.Value = billingUnit.Value = netValue.Value = vatValue.Value = String.Empty;
                        }
                        else
                        {
                            billingQuantity.Value = billingQuantity.Value.Replace(",",string.Empty);
                            netValue.Value = netValue.Value.Replace(",", string.Empty);
                            vatValue.Value = vatValue.Value.Replace(",", string.Empty);
                        }                                        

                        if (!string.IsNullOrEmpty(frontendId.Value))
                        {
                            var data = new DBHelper().GetCustomerId(Convert.ToInt64(frontendId.Value, CultureInfo.InvariantCulture));
                            var orderTypeId = data.OrderType ?? 0;
                            var orderCreationType = data.OrderCreationType ?? 0;
                            var orderType = ((SCAEnums.OrderType)orderTypeId);
                            var patientNDD = DateTime.Now;
                            if (!string.IsNullOrEmpty(data.CustomerId) && data.SAPDeliveryDate != null)
                            {
                                var objCustomerParameterBusinessModel = new CustomerParameterBusinessModel();
                                var carehomeNDD = DateTime.Now;
                                objCustomerParameterBusinessModel.CustomerID = Convert.ToInt64(data.CustomerId, CultureInfo.InvariantCulture);
                                objCommonService.PostcodeMatrixList = objCommonService.GetPostcodeMatrixForPatient(Convert.ToString(data.CustomerId, CultureInfo.InvariantCulture),
                                                                       Convert.ToString(data.PatientId, CultureInfo.InvariantCulture),
                                                                       data.CarehomeId != null ? Convert.ToString(data.CarehomeId, CultureInfo.InvariantCulture) : "0",
                                                                       sapUserId);
                                                                
                                /// calculate & update NDD for products in order + return minimum NDD of product   
                                /// only for ZHDP i.e. community order, carehome/hospital order                                                                 
                                if (orderType == SCAEnums.OrderType.ZHDP && errorCode.Value.ToUpper() == successCode &&  Array.IndexOf(oneoffStandardOrders, orderCreationType) == -1)                                
                                //if (orderType == SCAEnums.OrderType.ZHDP && errorCode.Value.ToUpper() == successCode)
                                {
                                    patientNDD = objCommonService.UpdatePrescriptionProductNdd(Convert.ToInt64(frontendId.Value, CultureInfo.CurrentCulture), data.SAPDeliveryDate, Convert.ToInt64(data.CustomerId, CultureInfo.CurrentCulture), Guid.Parse(sapUserId), false);

                                    /// if carehome order then calculate carehome ndd
                                    if (data.CarehomeId != null)
                                    {
                                        isCarehomeOrder = true;
                                        carehomeNDD = objCommonService.GetNdd(Convert.ToDateTime(data.SAPDeliveryDate, CultureInfo.InvariantCulture), data.CarehomeFrequency, Convert.ToInt64(data.CustomerId, CultureInfo.CurrentCulture), Guid.Parse(sapUserId), false);
                                    }
                                }

                                new DBHelper().OrderResponse(
                                    Convert.ToInt64(frontendId.Value, CultureInfo.CurrentCulture), 
                                    orderNo.Value, 
                                    delivery.Value, 
                                    invoice.Value, 
                                    errorCode.Value, 
                                    errorMessage.Value,
                                    baseMaterialId.Value,
                                    patientNDD, 
                                    carehomeNDD, 
                                    isCarehomeOrder,
                                    actDelDate.Value,
                                    plant.Value,
                                    billingDate.Value,
                                    currency.Value,
                                    billingQuantity.Value,
                                    billingUnit.Value,
                                    netValue.Value,
                                    vatValue.Value,                                                                        
                                    podDate.Value,
                                    deliveryQuantity.Value,
                                    deliveryUnit.Value,
                                    orderItem.Value                                                                      
                                    );
                            }
                            else
                            {
                                errorMessage.Value = "CustomerId or DeliveryDate is empty";
                            }

                            strMessage.Append("[FRONTENDID=" + frontendId.Value + "]    [ORDERNO=" + orderNo.Value + "]     [Message=" + errorMessage.Value + "] \n");
                        }
                    }

                    strMessage = strMessage.Remove(strMessage.Length - 2, 2);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
                strMessage.Append(e.Message);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                strMessage.Append(e.Message);
            }

            endTime = DateTime.Now;
            LogToFile(startTime, endTime, strMessage.ToString(), sapRequestLogFileName, "Order");
        }

        /// <summary>
        /// Receives Request from Sap for creating new Telephonic/Online Standard Order in Indigo Application
        /// </summary>
        /// <param name="objXElement">XML document containing Patient and Customer Number for whom, Order is to be created</param>
        public void CreateSapOrders(XElement objXElement)
        {
            // Initialize Loger varriable
            StringBuilder logMessage = new StringBuilder();
            var startTime = DateTime.Now;
            var endTime = DateTime.Now;
            var userId = string.Empty;

            OnlineOrdersBusinessModel objOnlineOrder = new OnlineOrdersBusinessModel();
            List<OnlineOrdersBusinessModel> onlineOrdersList = new List<OnlineOrdersBusinessModel>();

            try
            {
                if (objXElement != null)
                {
                    var doc = new XDocument(objXElement);

                    var orders = doc.Descendants("ASAORDERS").Descendants("item").ToList();
                    char hypen = '-';
                    char slash = '/';
                    if (this.writeOnlineOrderLog)
                    {
                        LogToFile(doc.ToString(), this.onlineOrderLogFileName);
                    }

                    if (orders != null && orders.Count() != 0)
                    {
                        var emptyRecordCount = orders.Where(q => q.Element("SAPID") == null && q.Element("CUSTOMERNO") == null && q.Element("CREDATE") == null && q.Element("BIRDT") == null && q.Element("NDDT") == null).Count();
                        logMessage.AppendLine(string.Format(CultureInfo.InvariantCulture, "{0} Records Received", orders.Count()));
                        logMessage.AppendLine(string.Format(CultureInfo.InvariantCulture, "{0} Records were Empty, {1} were not Empty", emptyRecordCount, orders.Count() - emptyRecordCount));
                    }
                    else
                    {
                        logMessage.AppendLine("No Records Received");
                    }

                    objOnlineOrder = GetOnlineorderList(objOnlineOrder, onlineOrdersList, orders, hypen, slash);

                    var objOrderClass = new Order();
                    objOrderClass.GenerateOnlineTelephoneOrder(onlineOrdersList);
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, userId);
                logMessage.Append(e.Message);
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, userId);
                logMessage.Append(e.Message);
            }

            endTime = DateTime.Now;
            LogToFile(startTime, endTime, logMessage.ToString(), sapRequestLogFileName, "Online/Telephonic Order");
        }

        /// <summary>
        /// Gets online order list
        /// </summary>
        /// <param name="objOnlineOrder"></param>
        /// <param name="onlineOrdersList"></param>
        /// <param name="orders"></param>
        /// <param name="hypen"></param>
        /// <param name="slash"></param>
        /// <returns></returns>
        private OnlineOrdersBusinessModel GetOnlineorderList(OnlineOrdersBusinessModel objOnlineOrder, List<OnlineOrdersBusinessModel> onlineOrdersList, List<XElement> orders, char hypen, char slash)
        {
            foreach (var order in orders.Where(order => order != null))
            {
                objOnlineOrder = new OnlineOrdersBusinessModel();

                objOnlineOrder.SAPPatientID = order.Element("SAPID") != null ? order.Element("SAPID").Value : string.Empty;
                objOnlineOrder.SAPCustomerID = order.Element("CUSTOMERNO") != null ? order.Element("CUSTOMERNO").Value : string.Empty;
                objOnlineOrder.CreateDate = order.Element("CREDATE") != null ? order.Element("CREDATE").Value.ToString(CultureInfo.InvariantCulture).Replace(hypen, slash) : string.Empty;
                objOnlineOrder.Email = order.Element("EMAIL") != null ? order.Element("EMAIL").Value : string.Empty;
                objOnlineOrder.AllowByEmail = order.Element("ALLOWBYMAIL") != null ? order.Element("ALLOWBYMAIL").Value : string.Empty;
                objOnlineOrder.BirthDate = order.Element("BIRDT") != null ? order.Element("BIRDT").Value.ToString(CultureInfo.InvariantCulture).Replace(hypen, slash) : string.Empty;
                objOnlineOrder.NextDeliveryDate = order.Element("NDDT") != null ? order.Element("NDDT").Value.ToString(CultureInfo.InvariantCulture).Replace(hypen, slash) : string.Empty;
                objOnlineOrder.UserID = default(Guid).ToString();

                if (!(string.IsNullOrEmpty(objOnlineOrder.SAPCustomerID) && string.IsNullOrEmpty(objOnlineOrder.SAPPatientID) && string.IsNullOrEmpty(objOnlineOrder.CreateDate) && string.IsNullOrEmpty(objOnlineOrder.Email) && string.IsNullOrEmpty(objOnlineOrder.BirthDate) && string.IsNullOrEmpty(objOnlineOrder.NextDeliveryDate)))
                {
                    onlineOrdersList.Add(objOnlineOrder);
                }
            }
            return objOnlineOrder;
        }

        /// <summary>
        /// Saves the customer product log.
        /// </summary>
        /// <param name="listSAPResultBusinessModel">The list sap result business model.</param>
        /// <param name="startTime">The start time.</param>
        /// <param name="endTime">The end time.</param>
        private void SaveCustomerPorductLog(List<SAPCustomerProductResultBusinessModel> listSAPResultBusinessModel, DateTime startTime, DateTime endTime)
        {
            if (listSAPResultBusinessModel != null && listSAPResultBusinessModel.Count > 0)
            {
                StringBuilder strCustomer = new StringBuilder();
                foreach (var customerProduct in listSAPResultBusinessModel)
                {
                    strCustomer.Append("\n");
                    strCustomer.Append("[CustomerId-" + customerProduct.FrontEndCustomerId + "]  [SAPId-" + customerProduct.SAPCustomerNo + " \n");
                    strCustomer.Append("[Inserted-" + customerProduct.ProductInserted + "]    ");
                    strCustomer.Append("[Updated-" + customerProduct.ProductUpdated + "]     ");
                    strCustomer.Append("[Failed-" + customerProduct.ProductFailed + "]     ");
                    strCustomer.Append("[ErrorMessage-" + customerProduct.ExceptionMessage + "] \n");
                }

                LogToFile(startTime, endTime, strCustomer.ToString(), this.customerProductLogFileName, "CustomerProduct");
            }
        }

        /// <summary>
        /// Log string message to specified Log file
        /// </summary>
        /// <param name="startTime">DateTime startTime</param>
        /// <param name="endTime">DateTime endTime</param>
        /// <param name="logMessage">string logMessage</param>
        /// <param name="logFileName">string LogFileName</param>
        /// <param name="logFor">string logFor</param>
        private void LogToFile(DateTime startTime, DateTime endTime, string logMessage, string logFileName, string logFor = "")
        {
            // create a writer and open the file
            string strLogFilePath = HostingEnvironment.MapPath("~/SAPFiles/" + logFileName + ".txt");
            using (TextWriter textWriter = File.AppendText(strLogFilePath))
            {
                textWriter.WriteLine(string.Empty);
                textWriter.WriteLine("+++++++++++ Batch Started at " + startTime + " For " + logFor + " +++++++++++++++");
                textWriter.WriteLine(logMessage);
                textWriter.WriteLine("+++++++++++ Batch End at " + endTime + "        +++++++++++++++");
            }
        }

        /// <summary>
        /// Save individual customer details
        /// </summary>
        /// <param name="customer">instance of SAPCustomer</param>
        /// <returns>
        /// Customer details
        /// </returns>
        private SAPCustomerBusinessModel SaveCustomerData(XElement customer)
        {
            try
            {
                var customerNo = customer.Element("CUSTOMERNO");
                var salesorg = customer.Element("SALESORG");
                var distrchannel = customer.Element("DISTRCHANNEL");
                var division = customer.Element("DIVISION");
                var name = customer.Element("NAME1");
                var street = customer.Element("STREET");
                var postcode = customer.Element("POSTCODE");
                var city = customer.Element("CITY");
                var county = customer.Element("COUNTY");
                var country = customer.Element("COUNTRY");
                var haulier = customer.Element("HAULIER");

                if (customerNo != null && salesorg != null && distrchannel != null && division != null && name != null &&
                    street != null && postcode != null && city != null && county != null && country != null && haulier != null)
                {
                    var objSapCustomer = new SAPCustomerBusinessModel
                    {
                        SAPCustomerNo = customerNo.Value,
                        SalesOrg = salesorg.Value,
                        Distrchannel = distrchannel.Value,
                        Division = division.Value,
                        CustomerName = name.Value,
                        Street = street.Value,
                        Postcode = postcode.Value,
                        City = city.Value,
                        County = county.Value,
                        Country = country.Value,
                        Haulier = haulier.Value
                    };

                    var objSapCustomerDeatailsParameter = new SAPCustomerParameter
                    {
                        HouseNumber = objSapCustomer.HouseNumber,
                        AddressLine1 = objSapCustomer.Street,
                        AddressLine2 = objSapCustomer.AddressLine2,
                        City = objSapCustomer.City,
                        County = objSapCustomer.County,
                        Country = objSapCustomer.Country,
                        Postcode = objSapCustomer.Postcode,
                        HouseName = objSapCustomer.HouseName,
                        CustomerName = objSapCustomer.CustomerName,
                        TelephoneNumber = objSapCustomer.TelephoneNumber,
                        MobileNumber = objSapCustomer.MobileNumber,
                        Email = objSapCustomer.Email,
                        FaxNumber = objSapCustomer.FaxNumber,
                        CustomerNo = objSapCustomer.SAPCustomerNo,
                        SalseOrg = objSapCustomer.SalesOrg,
                        Distrchannel = objSapCustomer.Distrchannel,
                        Division = objSapCustomer.Division,
                        Haulier = objSapCustomer.Haulier,
                        ModifiedBy = sapUserId,
                        ModifiedDate = new DateTime()
                    };
                    isIncrementalUpdateRequired = CheckIncrementalUpdateRequired(objSapCustomer.CustomerId, objSapCustomer);
                    objSapCustomer.CustomerId = new DBHelper().SaveSapCustomerDetails(objSapCustomerDeatailsParameter);
                    if (isIncrementalUpdateRequired)
                    {
                        var incrementalUpdateObj = new IncrementalUpdateSearchIndex();
                        incrementalUpdateObj.CreateIncrementalUpdateIndexXml(objSapCustomer.CustomerId, (long)SCAEnums.ObjectType.Customer);
                    }
                    return objSapCustomer;
                }
            }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, sapUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                throw;
            }

            return null;
        }

        /// <summary>
        /// Save individual Product
        /// </summary>
        /// <param name="objSapProduct">Instance of SAPProduct</param>
        private void SaveProductData(SAPProductBusinessModel objSapProduct)
        {
            try
            {
                var objSaveSapCustomerProductParameter = new SAPProductParameter
                {
                    CustomerId = objSapProduct.CustomerId,
                    SAPProductId = objSapProduct.SAPProductId,
                    SAPProductName = objSapProduct.SAPProductName,
                    Salesunit = objSapProduct.SalesUnit,
                    ApprovalFlag = objSapProduct.ApprovalFlag,
                    Basematerial = objSapProduct.BaseMaterial,
                    Netvalue = Convert.ToDecimal(objSapProduct.NetValue, CultureInfo.InvariantCulture),
                    VAT = Convert.ToDecimal(objSapProduct.VAT, CultureInfo.InvariantCulture),
                    Packsize = objSapProduct.PackSize,
                    ModifiedBy = sapUserId,
                    ModifiedDate = DateTime.Now
                };
                var data = new DBHelper().SaveSapCustomerProduct(objSaveSapCustomerProductParameter);
                objSapProduct.ProductId = Convert.ToInt64(data.ProductId, CultureInfo.InvariantCulture);

                if (objSapProduct.ProductId > 0)
                {
                    productIds += "," + objSapProduct.ProductId;
                    if (objSapProduct.ProductId > 0 && data.CommondType == Convert.ToInt32(SCA.Indigo.Common.Enums.SCAEnums.SQLCommandType.Inserted, CultureInfo.InvariantCulture))
                    {
                        objSapProduct.ProductInserted = 1;
                    }
                    else if (objSapProduct.ProductId > 0 && data.CommondType == Convert.ToInt32(SCA.Indigo.Common.Enums.SCAEnums.SQLCommandType.Updated, CultureInfo.InvariantCulture))
                    {
                        objSapProduct.ProductUpdated = 1;
                    }
                }
                else
                {
                    objSapProduct.ProductFailed = 1;
                }

            }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, sapUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                throw;
            }
        }

        /// <summary>
        /// Get SAP userId
        /// </summary>
        private void GetSAPUserDetails()
        {
            sapUserId = new DBHelper().GetSAPUserDetails();
        }

        /// <summary>
        /// Log string message to specified Log file
        /// </summary>
        /// <param name="logMessage">string logMessage</param>
        /// <param name="logFileName">string LogFileName</param>
        private void LogToFile(string logMessage, string logFileName)
        {
            // create a writer and open the file
            string strLogFilePath = HostingEnvironment.MapPath("~/SAPFiles/" + logFileName + ".txt");
            File.WriteAllText(strLogFilePath, logMessage);
        }

        public bool CheckIncrementalUpdateRequired(long customerId, SAPCustomerBusinessModel objSapCustomer)
        {
            ICustomerRepository customerRepository = null;            
            isIncrementalUpdateRequired = false;
                try
                {
                    if (customerId != 0)
                    {
                        customerRepository = new CustomerRepository();
                        var customer = customerRepository.Find(customerId);
                        if (customer != null && customer.PersonalInformation != null && customer.PersonalInformation.Address != null)
                        {
                            var personalInfo = customer.PersonalInformation;
                            var address = personalInfo.Address;

                            var oldCustomerBusinessModel = customerRepository.Find(customerId);

                            if (oldCustomerBusinessModel != null && oldCustomerBusinessModel.PersonalInformation != null && oldCustomerBusinessModel.PersonalInformation.Address != null)
                            {
                                if ((oldCustomerBusinessModel.PersonalInformation.FirstName != objSapCustomer.CustomerName) ||
                                (oldCustomerBusinessModel.PersonalInformation.Address.PostCode != objSapCustomer.Postcode))
                                {

                                    isIncrementalUpdateRequired = true;
                                } 
                                else
                                {

                                    isIncrementalUpdateRequired = false;
                                }
                            }                    
                   
                        }
                    }
                    else
                    { 
                        isIncrementalUpdateRequired = true; 
                    }
                }
            catch (DataException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (ArgumentException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (FormatException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (InvalidOperationException e)
            {
                LogHelper.LogException(e, sapUserId);
            }
            catch (SystemException e)
            {
                LogHelper.LogException(e, sapUserId);
                throw;
            }
            catch (Exception e)
            {
                LogHelper.LogException(e, sapUserId);
                throw;
            }
            finally
            {
                if (customerRepository != null)
                {
                    customerRepository.Dispose();
                }
            }            
            return isIncrementalUpdateRequired;
        }
        
    }
}