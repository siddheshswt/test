﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 12-16-2014
// ***********************************************************************
// <copyright file="Startup.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI
{
    using Microsoft.AspNet.SignalR;
    using Owin;
    using System.Configuration;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // ConfigureAuth(app);
            GlobalHost.DependencyResolver.UseSqlServer(ConfigurationManager.ConnectionStrings["SCAIndigoEnterpriseConn"].ConnectionString);
            app.MapSignalR();
        }
    }
}
