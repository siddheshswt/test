/// <autosync enabled="true" />
/// <reference path="bootstrap.js" />
/// <reference path="common.js" />
/// <reference path="custome-inputs.js" />
/// <reference path="datepicker.js" />
/// <reference path="expand.js" />
/// <reference path="fileinput.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="jquery-1.2.6.min.js" />
/// <reference path="jquery-ui-1.10.0.js" />
/// <reference path="jquery-ui.js" />
/// <reference path="jquery.jqgrid.js" />
/// <reference path="jquery.mousewheel.js" />
/// <reference path="jquery.multiselect.js" />
/// <reference path="jquery.session.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="modernizr.custom.js" />
/// <reference path="mootools-1.2-core.js" />
/// <reference path="mootools-1.2-more.js" />
/// <reference path="placeholders.min.js" />
/// <reference path="print.js" />
/// <reference path="respond.js" />
/// <reference path="respond.src.js" />
/// <reference path="respond.src.js.js" />
/// <reference path="scrollbar.js" />
/// <reference path="pie/pie_ie678.js" />
/// <reference path="pie/pie_ie678_uncompressed.js" />
/// <reference path="pie/pie_ie9.js" />
/// <reference path="pie/pie_ie9_uncompressed.js" />
/// <reference path="tab/jquery.pwstabs-1.2.1.js" />
/// <reference path="thickbox/jquery-2.1.1.min.js" />
/// <reference path="thickbox/thickbox-compressed.js" />
/// <reference path="thickbox/thickbox.js" />
/// <reference path="js/grid/medicalstaff.js" />
/// <reference path="js/grid/prouductlist.js" />
/// <reference path="js/validations/attachment.js" />
/// <reference path="js/validations/auditlog.js" />
/// <reference path="js/validations/automaticorder.js" />
/// <reference path="js/validations/carehome.js" />
/// <reference path="js/validations/clinicalcontactmapping.js" />
/// <reference path="js/validations/common.js" />
/// <reference path="js/validations/contactinfo.js" />
/// <reference path="js/validations/customerinformation.js" />
/// <reference path="js/validations/customerparameter.js" />
/// <reference path="js/validations/formvalidation.js" />
/// <reference path="js/validations/holidayprocessadmin.js" />
/// <reference path="js/validations/home.js" />
/// <reference path="js/validations/interaction.js" />
/// <reference path="js/validations/mapusercustomer.js" />
/// <reference path="js/validations/mapusertorole.js" />
/// <reference path="js/validations/massdatachanges.js" />
/// <reference path="js/validations/massdataimport.js" />
/// <reference path="js/validations/medicalstaffanalysisinfo.js" />
/// <reference path="js/validations/medicalstaffeditpopup.js" />
/// <reference path="js/validations/notes.js" />
/// <reference path="js/validations/order.js" />
/// <reference path="js/validations/patientdetails.js" />
/// <reference path="js/validations/patientinfo.js" />
/// <reference path="js/validations/patientnotes.js" />
/// <reference path="js/validations/patienttypematrix.js" />
/// <reference path="js/validations/postcodechecker.js" />
/// <reference path="js/validations/prescription.js" />
/// <reference path="js/validations/prescriptionauthorization.js" />
/// <reference path="js/validations/productlist.js" />
/// <reference path="js/validations/productorders.js" />
/// <reference path="js/validations/recentorders.js" />
/// <reference path="js/validations/reportbuilder.js" />
/// <reference path="js/validations/sampleorder.js" />
/// <reference path="js/validations/sampleproductlist.js" />
/// <reference path="js/validations/search.js" />
/// <reference path="js/validations/uploadexcel.js" />
/// <reference path="js/validations/CarehomeMaintanceReport.js" />
/// <reference path="js/validations/InteractionReport.js" />
/// <reference path="js/validations/Notification.js" />
/// <reference path="js/validations/ResetUnlockUsers.js" />
/// <reference path="jquery.signalr-2.2.0.min.js" />
/// <reference path="js/validations/carehomeusers.js" />
/// <reference path="select-with-search.js" />
/// <reference path="jquery.multiselect.filter.min.js" />
