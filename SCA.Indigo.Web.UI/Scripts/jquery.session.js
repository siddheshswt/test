﻿$jqSession = jQuery.noConflict();
var sess_pollInterval = 60000;
var sess_expirationMinutes = 20;
var sess_warningMinutes = 3;
var sess_intervalID;
var sess_lastActivity;

function initSession() {
    sess_lastActivity = new Date();
    sessSetInterval();
    $jqSession(document).bind('keypress.session', function (ed, e) {
        sessKeyPressed(ed, e);
    });

    $jqSession(document).mousedown(function (ed, e) {
        sessKeyPressed(ed, e);
    });
}

function sessSetInterval() {
    sess_intervalID = setInterval('sessInterval()', sess_pollInterval);
}

function sessClearInterval() {
    clearInterval(sess_intervalID);
}

function sessKeyPressed(ed, e) {
    sess_lastActivity = new Date();
}

function sessLogOut() {
    var protocol = window.location.protocol;
    var hostname = window.location.hostname;
    var redirectUrl = 'Account/Logout';    
    window.location.href = protocol + '//' + hostname + '/' + redirectUrl;
}

function sessInterval() {
    var now = new Date();
    //get milliseconds of differneces
    var diff = now - sess_lastActivity;
    //get minutes between differences
    var diffMins = (diff / 1000 / 60);
    //if (diffMins >= sess_warningMinutes) {
    if (diffMins >= sess_expirationMinutes) {                               	
        $jqSession('#j_lightbox_session').show();        
    }
}
