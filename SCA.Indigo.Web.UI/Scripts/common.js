window.history.forward();
var $jqComm = jQuery.noConflict();
//custom selectbox
$jqComm(document).ready(function () {
    $jqComm(".custom-select-2").each(function () {
        $jqComm(this).wrap("<span class='select-wrapper'></span>");
        $jqComm(this).after("<span class='holder'></span>");
    });
    $jqComm(".custom-select-2").change(function () {
        var selectedOption = $jqComm(this).find(":selected").text();
        $jqComm(this).next(".holder").text(selectedOption);
    }).trigger('change');
});
//END custom selectbox


//$jqComm(window).scroll(function (event) {
//    var win_height = $jqComm("#container").height();
//    //alert(win_height);
//    $jqGrid("#data_loader").css('height', win_height);
//    $jqGrid("#loader").css('height', win_height);
//});


$jqComm(document).ready(function () {
    $jqComm(".j_lightbox_click").click(function () {
        $jqComm("#j_lightbox").show();
    });

    $jqComm(".j_lightbox_close").click(function () {
        $jqComm("#j_lightbox").hide();
    });

    $jqComm(document).keydown(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            $jqComm("#j_lightbox").hide();
        }
    });
});

//notification
$jqComm(document).ready(function (e) {
    $jqComm(document).on('click', '.notification-icon', function () {
        $jqComm("#inner_notification").fadeToggle("slow", "linear");
        $jqComm(".notification-icon").toggleClass('active-drop');
    });
    $jqComm(document).on('click', '#container', ".notify_cell", function () {
        $jqComm("#inner_notification").hide();
        $jqComm(".notification-icon").removeClass('active-drop');
    });

});
//End notification

$jqComm(document).on('click', '.notification-icon', function () {
    var totalitems = $jqComm("#inner_notification .notify_cell").length;
    var scrollval = $jqComm('#inner_notification .notify_cell').height();
    var scrollval = scrollval + 15;
    var totalheight = (totalitems * scrollval) - ($jqComm("#inner_notification").height());
 
    if (totalitems >= 6) {
        $jqComm("#notify_arrow_down").show();
    }
    else {
        $jqComm("#notify_arrow_down").hide();
    }

    $jqComm(document).on("click", "#notify_arrow_down", function () {
        var currentscrollval = $jqComm('#notification_body').scrollTop();
        
        $jqComm('#notification_body').scrollTop(scrollval + currentscrollval);

        // hide/show buttons
        if(currentscrollval == totalheight) {
            $jqComm(this).hide();
         }
         else {
            $jqComm("#notify_arrow_up").show();
         }
    });
    $jqComm(document).on("click", "#notify_arrow_up", function () {
        var currentscrollval = parseInt($jqComm('#notification_body').scrollTop());
         
        $jqComm('#notification_body').scrollTop(currentscrollval - scrollval);
         
         // hide/show buttons
         if((scrollval+currentscrollval) == scrollval) {
             $jqComm(this).hide();
         }
         else {
             $jqComm("#notify_arrow_down").show();
         }
    });
});

//end notification

$jqComm(window).load(function () {
    $jqComm(".span_expnd a").addClass("open");
    $jqComm(".care_home_maintenance_block").css('display', 'block');
    $jqComm(".audit_log").css('display', 'block');
    $jqComm(".interaction_summary_alert").css('display', 'block');
    $jqComm(".report_builder_cust_name_select_2").css('display', 'block');
 });



$jqComm(document).ready(function () {
    $jqComm("#txtpatsearch").click(function () {
        $jqComm("#txtpatsearch").attr('placeholder', '');
    });
    $jqComm("#txtpatsearch").focusout(function () {
        $jqComm("#txtpatsearch").attr('placeholder', 'Type to search');
    });

    $jqComm("#orderNo").click(function () {
        $jqComm("#orderNo").attr('placeholder', '');
    });
    $jqComm("#orderNo").focusout(function () {
        $jqComm("#orderNo").attr('placeholder', 'Order No.');
    });

    $jqComm("#searchID").click(function () {
        $jqComm("#searchID").attr('placeholder', '');
    });
    $jqComm("#searchID").focusout(function () {        
        $jqComm("#searchID").attr('placeholder', $jqComm("#hdnPlaceholderSearchId").val());        
    });


});

//custom input
//$jqComm(document).ready(function(){
//$jqComm(function() {
// $jqComm('input').ezMark();
// });
//});
//END custom input

//Search Pannel
$jqComm(document).ready(function () {
    var docked = 0;

    $jqComm("#dock li ul").height($jqComm(window).height());

    $jqComm("#dock .dock").click(function () {
        $jqComm(this).parent().parent().addClass("docked").removeClass("free");

        docked += 1;
        var dockH = ($jqComm(window).height()) / docked
        var dockT = 0;

        $jqComm("#dock li ul.docked").each(function () {
            $jqComm(this).height(dockH).css("top", dockT + "px");
            dockT += dockH;
        });
        $jqComm(this).parent().find(".dock").show();
        $jqComm(this).hide();

        if (docked > 0) {
            //$jqComm("#container").css("margin-left", "450px");
            //$jqComm("#container").css("width", "840px");
        }
        else {
            //$jqComm("#container").css("margin-left", "auto");
        }      
    });

    $jqComm("#dock .dock").click(function () {
        $jqComm(this).parent().parent().addClass("free").removeClass("docked").animate({ left: "-1173px" }, 1000).height($jqComm(window).height()).css("top", "0px");
        $jqComm("#search_dock_bg").css('display', 'none');
        $jqComm(".docking_button a").removeClass('active');
         $jqComm(this).parent().find(".dock").hide();
         $jqComm(this).parent().find(".undock").show();

        docked = docked - 1;
        var dockH = ($jqComm(window).height()) / docked
        var dockT = 0;

        $jqComm("#dock li ul.docked").each(function () {
            $jqComm(this).height(dockH).css("top", dockT + "px");
            $jqComm("#search_dock_bg").css('display', 'none');
            dockT += dockH;
        });
        $jqComm(this).parent().find(".dock").show();
        $jqComm(this).hide();

        
    });

    $jqComm(".docking_button").click(function () {
        $jqComm("ul.dock").animate({ left: "40px" }, 1000);
            $jqComm("#search_dock_bg").css('display', 'block');
            $jqComm("ul.dock").removeClass('free');
            $jqComm("ul.dock").addClass('docked');
            $jqComm(".docking_button a").addClass('active');
            $jqComm(this).parent().find(".dock").show();
            $jqComm(this).parent().find(".undock").hide();
    });

    //$jqComm("#dock").hover(function () {
    //    $jqComm("ul.dock").animate({ left: "40px" }, 500);
    //    $jqComm("#search_dock_bg").css('display', 'block');
    //}, function () {
    //    $jqComm("ul.free").animate({ left: "-1170px" }, 500);
    //    $jqComm("#search_dock_bg").css('display', 'none');

    //});
});


////Header Animation
$jqComm(function () {
    var lastScroll = 0;
    $jqComm(window).scroll(function (event) {
        var st = $jqComm(window).scrollTop();
        if (st >= 50) {
            $jqComm("header").css('height', '100px');
            $jqComm("#header_inner").css('height', '101px');
            $jqComm(".sca-logo").css('width', '16%');
            $jqComm(".sca-logo").css('padding-top', '12px');
            $jqComm(".header-right").css('right', '20px');
            $jqComm("#search_pannel").css('top', '106px');
            $jqComm("nav").css('right', '10px');
            $jqComm(".search_form_main_btns_1").css("margin-top", "-30px");
            $jqComm(".search_form_main_btns_1").css("border-bottom", "2px solid #ccc");
        }
        else {
            $jqComm("header").css('height', '115px');
            $jqComm("#header_inner").css('height', '116px');
            $jqComm(".sca-logo").css('width', '17%');
            $jqComm(".sca-logo").css('padding-top', '20px');
            $jqComm(".header-right").css('right', '13px');
            $jqComm("#search_pannel").css('top', '121px');
            $jqComm("nav").css('right', '0px');
            $jqComm(".search_form_main_btns_1").css("margin-top", "-10px");
            $jqComm(".search_form_main_btns_1").css("border-bottom", "0px solid #eee");
        }
        lastScroll = st;
    });
});
////End Header Animation


////Scroll Top
$jqComm.noConflict();
$jqComm(document).ready(function () {
    $jqComm(window).scroll(function () {
        if ($jqComm(this).scrollTop() > 250) {
            $jqComm('.scrollup').fadeIn();
        } else {
            $jqComm('.scrollup').fadeOut();
        }
    });

    $jqComm('.scrollup').click(function () {
        $jqComm("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
////End Scroll Top

///Loader
$jqComm(document).ready(function () {
    $jqComm("#loader").show();
    $jqComm("body").css('overflow', 'hidden');
});
$jqComm(window).load(function () {
    $jqComm("#loader").hide();
    $jqComm("body").css('overflow', 'auto');
});

/// scrollbar
(function ($jqComm) {
    $jqComm(window).load(function (e) {

        //$jqComm(".ui-jqgrid-bdiv").mCustomScrollbar();
        $jqComm(".mCustomScrollbar").mCustomScrollbar();

        //$("#gview_jqPatientPrescription .ui-jqgrid-bdiv").mCustomScrollbar("destroy");
        //$(".mCustomScrollbar").mCustomScrollbar("disable", true);
        //$(".mCustomScrollbar").mCustomScrollbar("scrollTo", { y:"0", x:"0"} );
        //$(".mCustomScrollbar").mCustomScrollbar({ axis: "yx", setTop: "0px", setLeft: "0px" });

        // Detected IE Browser
        var Browser = {
            IsIe: function () {
                return navigator.appVersion.indexOf("MSIE") != -1;
            },
            Navigator: navigator.appVersion,
            Version: function () {
                var version = 999;
                if (navigator.appVersion.indexOf("MSIE") != -1)
                    version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                return version;
            }
        };

        if (Browser.IsIe() && Browser.Version() <= 9) {
            $jqComm(".mCustomScrollbar").mCustomScrollbar("scrollTo", { y: "0", x: "0" });
            $jqComm(".mCustomScrollbar").mCustomScrollbar({ axis: "yx", setTop: "0px", setLeft: "0px" });
            //$(".ui-jqgrid .ui-jqgrid-bdiv").mCustomScrollbar({ axis: "yx", setTop: "0px", setLeft: "0px" });
            //$(".mCustomScrollbar").mCustomScrollbar("scrollTo", "left", { timeout: 1000 });

        }
        // End Detecting IE Browser
    });
})(jQuery);
///End Scrollbar


///Accordian
$jqComm(document).ready(function ($jqComm) {
    $jqComm('input').ezMark();
    $jqComm(".heading-1").toggler();
    //$jqComm(".heading-001").toggler();
    $jqComm("#inner-page").expandAll({ trigger: ".heading-1.collaps", ref: ".heading-1.collaps" });
});
///End Accordian

///Multiselect Fillter
$jqComm(document).ready(function ($jqComm) {
    //$jqComm("select").multiselect().multiselectfilter();
});
///End Multiselect Fillter

function ValidationGridPageInput(e, val) {
    if (val == '0' && e.which == 96) {
        $jqComm('.ui-pg-input').val('');
        return false;
    }
    return true;
}

// Prevent the backspace key from navigating back.
// prevent backspace if focus is on button and textbox
$(document).on("keydown", function (e) {
    //if (e.which === 8 && !$(e.target).is("input, textarea")) {

    //    e.preventDefault();
    //}

    if (e.which === 8) {
        if ($(document.activeElement).hasClass('btnFocus') || !$(e.target).is("input, textarea")) {
            e.preventDefault();
        }
    }
});

function ShowErrorMessage(id) {
    $jqGrid('#' + id).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        closeOnEscape: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralOk").val().toString(),
            "id": "hdnGeneralOk",
            click: function () {
                $jqGrid('#' + id).dialog('close');
                $jqGrid(".ui-widget-overlay").hide();
            },
        }]
    });

    $jqGrid('#' + id).dialog('open');
}

function HandleAjaxError(x, e, jsFileName, jsMethodName) {  
    if (x.status === 0) {
        //alert('You are offline!!\n Please Check Your Network. ' + x.reponseText);
        x.responseText = 'You are offline!!\n Please Check Your Network. ';
    }
    else if (x.status == 404) {
        //alert('Requested URL not found.');
        x.responseText = 'Requested URL not found.';
    } else if (x.status == 500) {
        //alert('Internal Server Error.');
        x.responseText = 'Requested URL not found.';
    } else if (e == 'parsererror') {
        //alert('Error.\nParsing JSON Request failed.');
        x.responseText = 'Error.\nParsing JSON Request failed.';
    } else if (e == 'timeout') {
        //alert('Request Time out.');
        x.responseText = 'Request Time out.';
    } else {
        //alert('Unknow Error.\n' + x.responseText);
        x.responseText = 'Unknown Error.\n';
    }
    //We can log error to exception log also
    var error = {
        "ErrorStatus": x.status,
        "ErrorMessage": x.responseText,
        "JsFileName": jsfilename,
        "JsMethodName": jsMethodName
    };

    $.ajax({
        url: '/Common/LogException',
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        cache: false,
        data: JSON.stringify(error),
        success: function (results) {

        }
    });
}