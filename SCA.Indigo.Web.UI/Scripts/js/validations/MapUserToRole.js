﻿$jqGrid = jQuery.noConflict();

var MapUserToRoleObj = new MapUserToRole();
MapUserToRole.prototype.GeneralYes = "hdnGeneralYes";
MapUserToRole.prototype.GeneralNo = "hdnGeneralNo";
MapUserToRole.prototype.GeneralOk = "hdnGeneralOk";

function MapUserToRole() { }

$jqGrid(document).ready(function () {
    LoadRoleToUser();
    RegisterMapUserToRoleGrid();
    //Hide the new role added span
    $jqGrid("#newRoleTextBox").parent().hide();

    DisableRolesToUserDropDown();

    // Check for ViewOnly
    //linkId: - Menu Link Id, element: - Main Div of the Form
    CommonScriptObj.DisableFormControls("frm_MapUserRole");

    // Clear Patient Sesion
    CommonScriptObj.ClearPatientSession();
});

$jqGrid("#sapcustomerid").keypress(function (evt) {
    evt = evt || window.event;
    if (evt.keyCode == 13) {
        SearchUserRolemapping();
        return false;
    }
});

$jqGrid("#userid").keypress(function (evt) {
    evt = evt || window.event;
    if (evt.keyCode == 13) {
        SearchUserRolemapping();
        return false;
    }
});

function DisplayAddNewRoleSection() {
    $jqGrid("#DDLRoles").parent().hide();
    $jqGrid("#newRoleTextBox").parent().show();
    $jqGrid("#lnkAddNewRole").hide();

    // Reset and Disable Grid
    $jqGrid("#jqMapRoleMenu").addClass("btn_disable");
    $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").attr("disabled", "disabled");
    $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").attr('readonly', 'true');
    $jqGrid(".viewOnly").children().prop("checked", false);
    $jqGrid(".fullRights").children().prop("checked", false);
    $('#DDLRoles').val($('#DDLRoles option').eq(0).val());
    $("#btnSaveRole").addClass("btn_disable");

    return false;
}

function DisplayErrorMessage(messageToDisplay) {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";
    errorMsg += errorMsgUl + errorMsgLi + messageToDisplay + errorMsgLiclose + errorMsgUlclose;
    $jqGrid("#divErrorMessage").html(errorMsg);
    ShowErrorMessage('divErrorMessage');
    return false;
}

function SaveNewRole() {
    var newRoleName = $jqGrid("#newRoleTextBox").val().trim();
    var isExternal = $jqGrid("#externalUserType").is(":checked");
    var isSCA = $jqGrid("#ScaUserType").is(":checked");
    if (newRoleName.length == 0 || (!isSCA && !isExternal)) {
        DisplayErrorMessage($jqGrid("#hdnNewRoleErrorMessage").val());
        return false;
    }

    // Validate and Save Role 
    $jqGrid.ajax({
        url: '/UserAdmin/SaveNewRole',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        cache: false,
        data: JSON.stringify({
            roleName: newRoleName, isSCA: isSCA
        }),
        success: function (data) {
            if (data) {
                // Load the Role Dropdown 
                // Update the Role Text in PopUp
                $("#divRoleFunctionalitySucess").text($("#hdnNewRoleAddedMessage").val().replace("{0}", newRoleName));

                $jqGrid('#divRoleFunctionalitySucess').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#" + MapUserToRoleObj.GeneralOk).val().toString(),
                        "id": MapUserToRoleObj.GeneralOk,
                        click: function () {
                            $jqGrid('#divRoleFunctionalitySucess').dialog('close');
                            // Refresh the Page;
                            window.location.href = "/UserAdmin/MapUserRoles";
                        },
                    }]
                });
                $jqGrid('#divRoleFunctionalitySucess').dialog('open');
                $jqGrid("#newRoleTextBox").val('');
                return false;
            }
            else {
                // Display the Validation Message
                DisplayErrorMessage($jqGrid("#hdnNewRoleAlreadyExistErrorMessage").val());
            }
        }
    });
}

// Load Role To Functionality
function LoadRoleTofunctionality() {
    var disableMapRoleMenuTitle = $jqGrid("#hdnDisableMapRoleMenuTitle").val();
    $jqGrid("#data_loader").show();
    $jqGrid("body").css('overflow', 'hidden');
    roleId = $("#DDLRoles :selected").val();
       if (roleId == "") {
        roleId = 0;
    }

     // Enable Headers 
    $("#jqMapRoleMenu_ViewOnly").find("div input").removeClass("btn_disable");
    $("#jqMapRoleMenu_ViewOnly").find("div input").removeAttr("disabled");
    $("#jqMapRoleMenu_ViewOnly").find("div input").removeAttr("readonly");

    $("#jqMapRoleMenu_FullRights").find("div input").removeClass("btn_disable");
    $("#jqMapRoleMenu_FullRights").find("div input").removeAttr("disabled");
    $("#jqMapRoleMenu_FullRights").find("div input").removeAttr("readonly");

    $jqGrid("#jqMapRoleMenu").removeClass("btn_disable");
    $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").removeAttr("disabled");
    $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").removeAttr('readonly');

    $jqGrid.ajax({
        url: '/UserAdmin/GetRoleToFunctionality',
            type: 'GET',
            contentType: "application/json",
        dataType: 'json',
        data: {
        roleId: roleId },
            cache : false,
            async: false,
                success: function(responseData) {
            $jqGrid("#jqMapRoleMenu").jqGrid('clearGridData');
                $jqGrid("#jqMapRoleMenu").jqGrid('setGridParam', {
            datatype: 'local', data : responseData
            }).trigger('reloadGrid');

            if(roleId == "0") {
                $jqGrid("#jqMapRoleMenu").addClass("btn_disable");
                $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").attr("disabled", "disabled");
                $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").attr('readonly', 'true');

                $("#jqMapRoleMenu_ViewOnly").find("div input").addClass("btn_disable");
                $("#jqMapRoleMenu_ViewOnly").find("div input").attr("disabled", "disabled");
                $("#jqMapRoleMenu_ViewOnly").find("div input").attr("disabled", "disabled");

                $("#jqMapRoleMenu_FullRights").find("div input").addClass("btn_disable");
                $("#jqMapRoleMenu_FullRights").find("div input").attr("disabled", "disabled");
                $("#jqMapRoleMenu_FullRights").find("div input").attr("disabled", "disabled");
             }
            // Disable the Map role menu for Admin only
            var disableMenus = $(".disableMapRoleMenu").filter(function (index) { return $(this).text() == "true";
            });
            disableMenus.parent().find("[type='checkbox']").attr("disabled", "disabled");
            disableMenus.parent().find("[type='checkbox']").attr("readonly", "true");
            disableMenus.parent().children().attr("title", disableMapRoleMenuTitle);
            $jqComm('#jqMapRoleMenu input[type="checkbox"]').ezMark();
            RegisterRoleToFunctionalityGrid();
        }
    });

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
    return false;
}

// Save Role To Functionality
function SaveRoleMenuMapping() {
    // Validate the Selection.
    var errmsg = ValidateRoleMenuMapping();

    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        $jqGrid("#divErrorMessage").html(errmsg);
        ShowErrorMessage('divErrorMessage');
        return false;
    }

    // Process for the Role Menu Mapping
    var roleMenuMappingList = [];
    var mapRoleMenuGridData = $jqGrid("#jqMapRoleMenu").jqGrid('getRowData');

    for (var i = 0; i < mapRoleMenuGridData.length; i++) {

        var row = mapRoleMenuGridData[i];

        var rolemenuMapping = {
            IDXRoleMenuID: row.IDXRoleMenuID,
            SubMenuId: row.SubMenuId,
            MenuId: row.MenuId,
            MenuName: row.MenuName,
            ViewOnly: row.ViewOnly,
            FullRights: row.FullRights,
        }

        roleMenuMappingList.push(rolemenuMapping);
    }

    roleId = $("#DDLRoles :selected").val();
    var roleText = $("#DDLRoles :selected").text();

    // Submit the RoleMenuMapping details.

    $jqGrid('#divErrorMessage').hide();
    $.ajax({
        url: '/UserAdmin/SaveRoleToFunctionality',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        cache: false,
        data: JSON.stringify({
            roleMenuMappingList: roleMenuMappingList, roleId: roleId
        }),
        success: function (data) {
            // Update the Role Text in PopUp
            $("#divRoleFunctionalitySucess").text($("#hdnresRoleFunctionalityMappingMessage").val().replace("{0}", roleText));
            LoadRoleTofunctionality();
            $jqGrid('#divRoleFunctionalitySucess').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#" + MapUserToRoleObj.GeneralOk).val().toString(),
                    "id": MapUserToRoleObj.GeneralOk,
                    click: function () {
                        $jqGrid('#divRoleFunctionalitySucess').dialog('close');
                    },
                }]
            });
            $jqGrid('#divRoleFunctionalitySucess').dialog('open');
            return false;
        }
    });
}

// Validate Role To Functionality
function ValidateRoleMenuMapping() {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";

    errorMsg = errorMsgUl;
    if ($jqGrid("#DDLRoles :selected").val() == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnerrorSelectRole").val() + errorMsgLiclose;
    }
    return errorMsg + errorMsgUlclose;
}

// Reset Selections
function ResetRoleMenu() {
    var resetMessage = $jqGrid("#hdnUserAdminResetMessage").val();
    $jqGrid('#divRoleUserSucess').html(resetMessage);

    $jqGrid('#divRoleUserSucess').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + MapUserToRoleObj.GeneralYes).val().toString(),
            "id": MapUserToRoleObj.GeneralYes,
            click: function () {
                DisableRoleMenuGrid();

                $jqGrid("#DDLRoles").parent().show();
                $jqGrid("#newRoleTextBox").parent().hide();
                $jqGrid("#newRoleTextBox").val('');
                $jqGrid("#lnkAddNewRole").show();
            },
        }, {
            text: $jqGrid("#" + MapUserToRoleObj.GeneralNo).val().toString(),
            "id": MapUserToRoleObj.GeneralNo,
            click: function () {
                $jqGrid('#divRoleUserSucess').dialog('close');
                return false;
            },
        }]
    });
    $jqGrid('#divRoleUserSucess').dialog('open');
    return false;
}

function DisableRoleMenuGrid() {
    $jqGrid('#divRoleUserSucess').dialog('close');
    $jqGrid("#jqMapRoleMenu").addClass("btn_disable");
    $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").attr("disabled", "disabled");
    $jqGrid("#jqMapRoleMenu").find("[type='checkbox']").attr('readonly', 'true');
    $jqGrid(".viewOnly input").prop("checked", false).closest("div").removeClass("ez-checked");
    $jqGrid(".fullRights input").prop("checked", false).closest("div").removeClass("ez-checked");
    $('#DDLRoles').val($('#DDLRoles option').eq(0).val());
    $("#btnSaveRole").removeClass("btn_disable");
    return false;
}

// Map User to Role Section

// Select All
function SelectAllUserRoleMapping(obj) {
    // Enable the Dropdown
    if (obj.checked) {
        $jqGrid('.check input').filter(function (index) { return $(this).attr("disabled") != "disabled";}).prop("checked", true).closest("div").addClass("ez-checked");
        EnableRolesToUserDropDown();
        return false;
    }
    else {
        $jqGrid('.check input').filter(function (index) { return $(this).attr("disabled") != "disabled"; }).prop("checked", false).closest("div").removeClass("ez-checked");
        DisableRolesToUserDropDown();
        return false;
    }
}

// Load Role to User
function LoadRoleToUser() {    
    $jqGrid("#data_loader").show();
    $jqGrid("body").css('overflow', 'hidden');

    var selectedrole = $("#DDLRolesToUser :selected").val();
    if (selectedrole == "" || selectedrole == undefined) {
        selectedrole = 0;
    }

    var sapCustomerId = $jqGrid.trim($jqGrid("#sapcustomerid").val());
    if (sapCustomerId == "" || sapCustomerId == undefined) {
        sapCustomerId = "NULL";
    }

    var userid = $jqGrid.trim($jqGrid("#userid").val());
    if (userid == "" || userid == undefined) {
        userid = "NULL";
    }

    $jqGrid("#cbox").closest("div").removeClass("ez-checked");
    $jqGrid.ajax({
        url: '/UserAdmin/GetRoleToUser',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        data: { userName: userid, roleId: selectedrole, sapCustomerId: sapCustomerId },
        cache: false,
        async: false,
        success: function (responseData) {
            $jqGrid("#cbox").prop('checked', false);
            $jqGrid("#cbox").removeAttr("disabled");
            $jqGrid("#cbox").removeAttr('readonly');
            $jqGrid("#jqMapUserRole").jqGrid('clearGridData');
            $jqGrid("#jqMapUserRole").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');

            //if (selectedrole == "0") {
            //    $jqGrid("#cbox").attr("disabled", "disabled");
            //    $jqGrid("#cbox").attr('readonly', 'true');
            //    $jqGrid("#jqMapUserRole").find("[type='checkbox']").attr("disabled", "disabled");
            //    $jqGrid("#jqMapUserRole").find("[type='checkbox']").attr('readonly', 'true');
            //}
            
            $jqComm('#jqMapUserRole input[type="checkbox"]').ezMark();
            RegisterMapUserToRoleGrid();
        }
    });

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');

    return false;
}

// Reset MapUserRole
function ResetMapUserRole() {    
    var resetMessage = $jqGrid("#hdnUserAdminResetMessage").val();
    $jqGrid('#divRoleUserSucess').html(resetMessage);

    $jqGrid('#divRoleUserSucess').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + MapUserToRoleObj.GeneralYes).val().toString(),
            "id": MapUserToRoleObj.GeneralYes,
            click: function () {
                $jqGrid('#divRoleUserSucess').dialog('close');

                $jqGrid("#userid").val('');
                $jqGrid("#sapcustomerid").val('');
                $('#DDLRolesToUser').val($('#DDLRolesToUser option').eq(0).val());
                // Reload the Screen                
                LoadRoleToUser();
                DisableRolesToUserDropDown();
                return false;
            },
        }, {
            text: $jqGrid("#" + MapUserToRoleObj.GeneralNo).val().toString(),
            "id": MapUserToRoleObj.GeneralNo,
            click: function () {
                $jqGrid('#divRoleUserSucess').dialog('close');
            },
        }]
    });
    $jqGrid('#divRoleUserSucess').dialog('open');
    return false;
}

// Save User to Role Mapping
function SaveUserRoleMapping() {
    // Validate the Selection.
    var errmsg = ValidateUserRoleMapping();

    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        $jqGrid("#divErrorMessage").html(errmsg);
        ShowErrorMessage('divErrorMessage');
        return false;
    }

    // Process for the Role Menu Mapping
    var userRoleMappingList = [];
    var mapUserRoleGridData = $jqGrid("#jqMapUserRole").jqGrid('getRowData');

    for (var i = 0; i < mapUserRoleGridData.length; i++) {

        var row = mapUserRoleGridData[i];

        var userRoleMapping = {
            IsRoleAssigned: row.IsRoleAssigned,
            IDXUserRoleId: row.IDXUserRoleId,
            UserID: row.UserID,
            RoleID: row.RoleID,
        }

        userRoleMappingList.push(userRoleMapping);
    }

    roleId = $("#DDLRolesToUser :selected").val();
    var rolesToUserText = $("#DDLRolesToUser :selected").text();

    // Submit the RoleMenuMapping details.

    $jqGrid('#divErrorMessage').hide();
    $.ajax({
        url: '/UserAdmin/SaveRoleToUser',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        cache: false,
        data: JSON.stringify({ userRoleMappingList: userRoleMappingList, roleId: roleId }),
        success: function (data) {
            $("#divRoleUserSucess").text($("#hdnresRoleUserMappingMessage").val().replace("{0}", rolesToUserText));
            $jqGrid('#divRoleUserSucess').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#" + MapUserToRoleObj.GeneralOk).val().toString(),
                    "id": MapUserToRoleObj.GeneralOk,
                    click: function () {
                        $jqGrid('#divRoleUserSucess').dialog('close');
                        // Reloading after saving changes
                        LoadRoleToUser();
                        return false;
                    },
                }]
            });
            $jqGrid('#divRoleUserSucess').dialog('open');
            $jqGrid(".ui-dialog-titlebar-close").hide();
            return false;
        }
    });

    return false;
}

// Validate Users To Role mapping
function ValidateUserRoleMapping() {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";

    errorMsg = errorMsgUl;
    if ($jqGrid("#DDLRolesToUser :selected").val() == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnerrorSelectRole").val() + errorMsgLiclose;
    }

    var isUserSelected = $jqGrid('.check').find(".ez-checked").length > 0;

    if (!isUserSelected) {
        errorMsg += errorMsgLi + 'Please select users to Update' + errorMsgLiclose;
    }

    return errorMsg + errorMsgUlclose;
}

// Search User to Role mapping
function SearchUserRolemapping() {    
    LoadRoleToUser();
}

// Build checkbox along with id
function DisableCheckBox(cellvalue, options, rowobject) {
    return "{disabled: false } ";
}

function DisableRolesToUserDropDown() {
    // Disable the Dropdown
    $jqGrid("#DDLRolesToUser").addClass("btn_disable");
    // Disable the Dropdown
    $jqGrid("#DDLRolesToUser").attr("disabled", "disabled");
    $jqGrid("#DDLRolesToUser").attr("readonly", "true");
    $jqGrid("#cbox").closest("div").removeClass("ez-checked");
    // Reset DropDown value
    $jqGrid("#DDLRolesToUser").val('');
}

function EnableRolesToUserDropDown() {
    $jqGrid("#DDLRolesToUser").removeClass("btn_disable");
    $jqGrid("#DDLRolesToUser").removeAttr("disabled");
    $jqGrid("#DDLRolesToUser").removeAttr("readonly");
}

// Select All View Only Check box
function SelectAllViewOnlyCheckBox(obj) {
    if (obj.checked) {
        $jqGrid('.viewOnly input').filter(function(index) {
return $(this).attr("disabled") != "disabled"; }).prop("checked",true).closest("div").addClass("ez-checked");
    }
    else {
       $jqGrid('.viewOnly input').filter(function(index) {
return $(this).attr("disabled") != "disabled";
       }).prop("checked",false).closest("div").removeClass("ez-checked");
       }
    return false;
}

    // Select All edit Check box
    function SelectAllEditCheckBox(obj) {
    if (obj.checked) {
            $jqGrid('.fullRights input').filter(function(index) {
    return $(this).attr("disabled") != "disabled"; }).prop("checked",true).closest("div").addClass("ez-checked");
    }
    else {
        $jqGrid('.fullRights input').filter(function(index) {
    return $(this).attr("disabled") != "disabled";
        }).prop("checked",false).closest("div").removeClass("ez-checked");
    }
    return false;
}

    // Register the Click evetn
  function RegisterRoleToFunctionalityGrid() {

      // Check for Select All ViewOnly and Full Rights
      FullRightsSelectAllCheckEvent();

    //Handle the Select All View Only checkbox
    $jqGrid(".viewOnly input").click(function() {
        ViewOnlySelectAllCheckEvent();
        });

      //Handle the Select All Full Rights checkbox
    $jqGrid(".fullRights input").click(function() {
        // Check for Select All functionality
        FullRightsSelectAllCheckEvent();
    });
  }

  function ViewOnlySelectAllCheckEvent()
  {
      // Check for Select All functionality
      var totalViewOnlyValidCheckboxes = $jqGrid(".viewOnly input").filter(function (index) { return $(this).attr("disabled") != "disabled"; }).parent(".ez-checked").length;
      if (totalViewOnlyValidCheckboxes > 0) {

          if (totalViewOnlyValidCheckboxes == $(".viewOnly input").filter(function (index) { return $(this).attr("disabled") != "disabled"; }).length) {
              $jqGrid("#viewonly_chkbox").prop("checked", true).closest("div").addClass("ez-checked");
          }
          else {
              $jqGrid("#viewonly_chkbox").prop("checked", false).closest("div").removeClass("ez-checked");
          }
      }
  }

  function FullRightsSelectAllCheckEvent() {
      if ($jqGrid(".fullRights input").parent("div .ez-checked").length == $jqGrid(".fullRights :checkbox").length) {
          $jqGrid("#fullRights_chkbox").prop("checked", true).closest("div").addClass("ez-checked");
      }
      else {
          $jqGrid("#fullRights_chkbox").prop("checked", false).closest("div").removeClass("ez-checked");
      }
  }

  function RegisterMapUserToRoleGrid()
  {
        // When page refresh
      if ($("input:checkbox[class=ez-hide]:checked").length == 0) {
          DisableRolesToUserDropDown();
      }
      // Map User Role CheckBox event
      $jqGrid(".check input").click(function () {          
          if ($("input:checkbox[class=ez-hide]:checked").length == 0) {
              // Disable the Dropdown
              DisableRolesToUserDropDown();              
          }
          else {
              // Enable the Dropdown
              EnableRolesToUserDropDown();

              // Check for Select All functionality              
              var numberOfChecked = $jqGrid(".check :checked").length;
              var totalCheckboxes = $jqGrid(".check :checkbox").length;
              if (numberOfChecked == totalCheckboxes) {
                  $jqGrid("#cbox").prop("checked", false).closest("div").addClass("ez-checked");
              }
              else {
                  $jqGrid("#cbox").prop("checked", false).closest("div").removeClass("ez-checked");
              }
          }
      });
}