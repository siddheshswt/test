﻿$jqGrid = jQuery.noConflict();
function Search() { }

var SearchObj = new Search();

Search.prototype.NameAsHyperLinkFormater = function (cellvalue, options, rowobject) {
    if (cellvalue == null) {
        cellvalue = "";
        return "";
    }
    
    return "<a onclick='HyperLinkClicked(" + options['rowId'] + ")' class='searchresult_a'>" + cellvalue + "</a>"
}

$jqGrid(document).ready(function () {
    var isRedirect = $jqGrid('#hdnisRedirect').val();
    if (isRedirect == "Order") {
        LoadSearchDataFromOrder();
    }

    $('#DDLPatientType option').remove();
    var optionhtml = "<option>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
    $("#DDLPatientType").append(optionhtml);


    $jqGrid("#txtpatsearch").autocomplete({
        minLength: 3,
        source: function (request, response) {
            $jqGrid.getJSON('/Search/DisplaySearchResult', { term: request.term }, function (data) {
                $.ajaxSetup({ cache: false });
                response($jqGrid.map(data, function (el) {
                    return {
                        label: el.SearchText,
                        value: el.SearchText
                    };
                }));
            });
        },
        select: function (event, ui) {
            var googleTypedtext = ui.item.label;
            $jqGrid(".ui-autocomplete").hide();
            loadPatientdetails(googleTypedtext);
        }
    });
       
    $jqGrid("#txtpatsearch").keypress(function (e) {
        $jqGrid(".ui-autocomplete").hide()
        var charCode = (e.which) ? e.which : e.keyCode;

        if (charCode == 13) {            
            loadPatientdetails(null);
        }

        if (isInValidSpecialCharacter(charCode) && !isApostrophe(charCode)) {
            return false;
        }
        else
            return true;
    });
        
    $jqGrid("#orderNo").keypress(function (evt) {
        evt = evt || window.event;
        if (isNumeric(evt.keyCode)) {
            return true;
        }
        else if (evt.keyCode == 13) {
            loadPatientdetails(null);
        }
        else {
            return false;
        }
    });

    $jqGrid("#searchID").keypress(function (evt) {
        evt = evt || window.event;
        if (isNumeric(evt.keyCode)) {
            return true;
        }
        else if (evt.keyCode == 13) {
            loadPatientdetails(null);
        }
        else {
            return false;
        }
    });

    $jqGrid("#DDLCustomer").keypress(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13) {
            evt.preventDefault();
            loadPatientdetails(null);
        }
    });

    $jqGrid("#DDLPatientStatus").keypress(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13) {
            evt.preventDefault();
            loadPatientdetails(null);
        }
    });

    $jqGrid("#DDLPatientType").keypress(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13) {
            evt.preventDefault();
            loadPatientdetails(null);
        }
    });

    $jqGrid("#btnPatientSearch").click(function () {        
        if ($jqGrid("#btnPatientSearch").hasClass('disabled'))
            return false;
        $jqGrid(".ui-autocomplete").hide();
        loadPatientdetails(null);
    });

    function loadPatientdetails(googleTypedtext) {
        var searchText = 'NULL';
        var customerId = 'NULL';
        var patientStatus = 'NULL';
        var patientType = -1;
        var orderNo = 'NULL';

        $jqGrid(".ui-autocomplete").hide();
        var isValidInput = false;

        // SearchId
        var searchID = 'NULL';
        if ($jqGrid("#searchID").val() != "" && $jqGrid("#searchID").val() != $jqGrid("#hdnPlaceholderSearchId").val()) {
            searchID =$.trim($jqGrid("#searchID").val());
            isValidInput = true;
        }

        if (searchID == 'NULL') {

            // OrderNumber
            if ($jqGrid("#orderNo").val() != "" && $jqGrid("#orderNo").val() != "Order No.") {
                orderNo = $.trim($jqGrid("#orderNo").val());
                isValidInput = true;
            }

            if (orderNo == 'NULL') {

                // Search Text
                if ($jqGrid("#txtpatsearch").val() != "" && $jqGrid("#txtpatsearch").val() != "Type to search") {
                    searchText = $jqGrid("#txtpatsearch").val();
                    isValidInput = true;
                }

                // Customer Dropdown
                if ($jqGrid("#DDLCustomer").val() != "" && $jqGrid("#DDLCustomer").val() != null) {
                    var customerId = $jqComm("#DDLCustomer").multiselect("getChecked").map(function () {
                        return this.value;
                    }).get();
                    isValidInput = true;
                }

                // PatientStaus Dropdown
                if ($jqGrid("#DDLPatientStatus").val() != "" && $jqGrid("#DDLPatientStatus").val() != null) {
                    var patientStatus = $jqComm("#DDLPatientStatus").multiselect("getChecked").map(function () {
                        return this.value;
                    }).get();
                    isValidInput = true;
                }

                // PatientType Dropdown
                if ($jqGrid("#DDLPatientType").val() != "" && $jqGrid("#DDLPatientType").val() != $("#hdnresddlPateintTypeDefaultValue").val()) {
                    patientType = $jqGrid("#DDLPatientType").val();
                    isValidInput = true;
                }
            }
        }

        if (googleTypedtext != null) {
            searchText = googleTypedtext;
            //customerId = 'NULL';
            //patientStatus = 'NULL';
            //patientType = -1;
            //orderNo = 'NULL';
            //searchID = 'NULL';
            //isValidInput = true;
        }

        if (!isValidInput) {
            var errorMsg = "";
            var errorMsgUl = "<ul class='pop_error_ul'>";
            var errorMsgUlclose = "</ul>";
            var errorMsgLi = "<li>";
            var errorMsgLiclose = "</li>";
            errorMsg = errorMsgUl;
            errorMsg += errorMsgLi + $jqGrid("#hdnerrorMsg").val() + errorMsgLiclose;
            errorMsg += errorMsgUlclose;
            $("#divErrorMessage").html(errorMsg);
            ShowHideError();
            return false;
        }

        $jqGrid("#data_loader").show();
        $jqGrid("body").css('overflow', 'hidden');

        var searchParam = {
            CustomerId: customerId.toString(),
            PatientStatus: patientStatus.toString(),
            PatientType: patientType,
            OrderNo: orderNo,
            searchID: searchID,
            SearchText: searchText
        };

        LoadSearchData(searchParam);

        return false;
    }

    function checkBox() {

        if ($jqGrid('#chkSelectall').is(':checked')) {
            $jqGrid(":checkbox").each(function () {

                //loop through each checkbox
                $jqGrid(":checkbox").prop("checked", true); //select all checkboxes with class "checkbox"               
            });
        } else {
            $jqGrid(":checkbox").each(function () {
                $jqGrid(":checkbox").prop("checked", false); //deselect all checkboxes with class "checkbox"
            });
        }
    }


    $jqGrid('#btnClearSearchResults').click(function () {
        if ($jqGrid("#btnClearSearchResults").children().hasClass('disabled'))
            return false;

        $jqGrid.ajax({
            url: '/Search/ClearSearchParamFromSession',
            async: false,
            cache: false,
            success: function (Data) {
            }
        });
        window.location.href = "/Search/SearchData";
    });

    $jqGrid("#DDLCustomer").on('change', function (e) {
        var customerIds = $jqComm("#DDLCustomer").multiselect("getChecked").map(function () {
            return this.value;
        }).get();
       
        var customerName = $("#DDLCustomer option:selected").text();
        if (customerName != $("#hdnresddlCUstomerDefaultValue").val() && customerName != "" && customerName != undefined) {
            LoadPatinetTypeDDL(customerIds);
        }
        else {
            $jqGrid('#DDLPatientType option').remove();
            var optionhtml = "<option>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
            $jqGrid("#DDLPatientType").append(optionhtml);
        }
    });

    $jqGrid("#btnBackSearch").click(function () {        
        if (((window.isMedicalInfoEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.staff_select_mode .medicalAnalysis_nformation_table .DisablePages').hasClass('disable_div'))) ||
   ((window.isPatientInfoEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.patientinfo_fields .DisablePages').hasClass('disable_div'))) ||
   (window.isPrescriptionEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.prescription_nformation_table .DisablePages').hasClass('disable_div'))) {
            if (window.patientId != 0 && window.patientId != undefined) {
                $jqGrid('#divBackPrompt').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                            BackToHomepage();
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divBackPrompt').dialog('open');
            }
            else {
                $jqGrid('#divBackPrompt').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                            BackToHomepage();
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divBackPrompt').dialog('open');
            }
        }
        else {
            BackToHomepage();
            return true;
        }
    });

    if ($jqGrid("#hdnIsCalledFromSearchPage").length > 0) {
           if($jqGrid("#hdnIsCalledFromSearchPage").val().toLowerCase() == "true") {
            if($jqGrid("#hdnCustomer").val() != "") {
                var SelectedArray = $jqGrid("#hdnCustomer").val().split(',');
                $jqGrid("#DDLCustomer").val(SelectedArray);
                $jqGrid("#DDLCustomer").multiselect('refresh');
                $jqGrid("#DDLCustomer").change();
                }

            if($jqGrid("#hdnSearchStatus").val() != "") {
                var SelectedArray = $jqGrid("#hdnSearchStatus").val().split('|');
                $jqGrid("#DDLPatientStatus").val(SelectedArray);
                $jqGrid("#DDLPatientStatus").multiselect('refresh');
                }

            if ($jqGrid("#hdnSearchOrderNumber").val() != "") {
                $jqGrid("#orderNo").val($jqGrid("#hdnSearchOrderNumber").val());
                }

            if ($jqGrid("#hdnSearchId").val() != "") {
                $jqGrid("#searchID").val($jqGrid("#hdnSearchId").val());
                }

            if ($jqGrid("#hdnTypeToSearch").val() != "") {
                $jqGrid("#txtpatsearch").val($jqGrid("#hdnTypeToSearch").val());
                }

            setTimeout(function() {
            if($jqGrid("#hdnSearchType").val() != "") {
                $jqGrid("#DDLPatientType").val($jqGrid("#hdnSearchType").val());
                }

             $jqGrid("#btnPatientSearch").click();
             }, 500);
             }

             }

             });

function LoadSearchData(searchParam) {
    $jqGrid("#jqPatientSearchResults").jqGrid('clearGridData');
    $jqGrid("#jqPatientSearchResults").jqGrid('setGridParam', {
        postData: {
            customerId: searchParam.CustomerId, patientStatus: searchParam.PatientStatus, patientType: searchParam.PatientType, orderNo: searchParam.OrderNo, searchID: searchParam.searchID, searchText: searchParam.SearchText
        }, datatype: 'json'
    }).trigger('reloadGrid');
                    $jqComm("#data_loader").hide();
            $jqComm("body").css('overflow', 'auto');
}

function LoadSearchDataFromOrder() {
    
    var searchParam = $jqComm("#hdnSearchParam").val();
    if (searchParam == "") {
        return;
    }
    var searchData = searchParam.split(";");
    searchParam = {
        CustomerId: searchData[0],
        PatientStatus: searchData[1],
        PatientType: searchData[2],
        OrderNo: searchData[3],
        searchID: searchData[4],
        SearchText: searchData[5]
    };
    BindSearchParam(searchParam);
    LoadSearchData(searchParam);
}

function BindSearchParam(searchParam) {    
    if (searchParam != "" && searchParam != undefined) {        
        //Make an array
        var customerArray = new Array();
        if (searchParam.customerId != "" && searchParam.customerId != undefined) {
            customerArray = searchParam.CustomerId.split(",");
            // Set the value
            $jqGrid('#DDLCustomer').val(customerArray);
            // Then refresh
            $jqGrid('#DDLCustomer').multiselect("refresh");
        }

        //Make an array
        var statusArray = new Array();
        if (searchParam.PatientStatus != "" && searchParam.PatientStatus != undefined) {
            statusArray = searchParam.PatientStatus.split(",");
            // Set the value
            $jqGrid('#DDLPatientStatus').val(statusArray);
            // Then refresh
            $jqGrid('#DDLPatientStatus').multiselect("refresh");
        }


        patientTypeSelectedVal = searchParam.PatientType;
        if (patientTypeSelectedVal.length > 0) {
            // Load PatientType
            LoadPatinetTypeDDL(searchParam.CustomerId);
        }
        // Set the value
        $jqGrid('#DDLPatientType').val(patientTypeSelectedVal);
        //bind orderId AND searchText

    }

}

function LoadPatinetTypeDDL(selectedCustomers) {

    if (selectedCustomers != $("#hdnresddlCUstomerDefaultValue").val()) {
        $jqGrid.ajax({
            url: '/Search/GetCustomerPatientType',
            type: 'GET',
            cache: false,
            async: false,
            data: { customerId: selectedCustomers.toString() },
            success: function (responseData) {                
                if (responseData != false) {
                    $('#DDLPatientType option').remove();
                    var optionhtml = "<option>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
                    $("#DDLPatientType").append(optionhtml);
                    $.each(responseData, function (i) {
                        optionhtml = "";
                        optionhtml = '<option value="' + responseData[i].ListId + '">' + responseData[i].DisplayText + '</option>';
                        $("#DDLPatientType").append(optionhtml);
                    });
                }
                else {
                    $('#DDLPatientType option').remove();
                    var optionhtml = "<option>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
                    $("#DDLPatientType").append(optionhtml);
                }
            }
        });
    }
}

function SetSearchSessionForNavigation() {    
    var customer = "";
    if ($jqGrid("#DDLCustomer").val() != "" && $jqGrid("#DDLCustomer").val() != null) {
        customer = "," + $jqGrid("#DDLCustomer").multiselect("getChecked").map(function () {
            return this.value;
        }).get();
    }

    customer = customer.substring(1, customer.length);

    var searchStatus = $jqGrid("#DDLPatientStatus").val();
    var patientStatus = "";
    if (searchStatus != null) {
        for (var i = 0; i < searchStatus.length ; i++) {
         patientStatus = patientStatus + searchStatus[i]+ "|";
}
}


    //searchStatus = searchStatus.substring(1, searchStatus.length);

   var patientType = $jqGrid("#DDLPatientType").val();
   var orderNubmer = $jqGrid("#orderNo").val();
   var searchId = $jqGrid("#searchID").val();
   var url = $jqGrid("#hdnSetSessionUrl").val();
   var typeToSearch = $jqGrid("#txtpatsearch").val();
   $jqGrid.ajax({
       url: url,
       async: false,
       cache: false,
       type: "Post",
           data: {
           Customer: customer,
           SearchStatus: patientStatus,
           TypeToSearch: typeToSearch,
           PatientType: patientType,
           OrderNubmer: orderNubmer,
           SearchId: searchId
        },
            success: function (data)
       {

        }
    });
}

function SingleClickOnSearchGrid(rowid)
{
    SetSearchSessionForNavigation();
    var rowData = $jqGrid("#jqPatientSearchResults").getRowData(rowid);
    SetPatientLoadParameter(rowData, true);
    clickevent = "OnCellSelect";
    $jqGrid.ajax({
        url: '/Patient/SetClickEventInSession',
        type: 'GET',
        async: false,
        data: { redirectEvent: clickevent },
        cache: false,
        success: function (Data) {
            return true;
        }
    });
    return true;
}

function HyperLinkClicked(rowId) {
    
    /// Add loader
    $jqGrid("#data_loader").show();
    $jqGrid(".docking_button a").removeClass('active');
    $jqGrid("body").css('overflow', 'hidden');
    //$jqComm(".prescription_nformation_table").mCustomScrollbar();
    var rowData = $jqGrid("#jqPatientSearchResults").getRowData(rowId);
    SetSearchSessionForNavigation();
    SingleClickOnSearchGrid(rowId);
    setTimeout(function () {
        var ObjectType = rowData['ObjectType'];
        //PatientId can have PatientId,CareHomeId and CustomerId
        var patientId = rowData['PatientId'];
        if (ObjectType == "10113") {

            // Check for the Patient View Details ViewOnly or Full Rights
            var viewPatientElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnViewPatientMenuText").val() });
            // Sub menu available
            if (viewPatientElement.length > 0) {
                // Get the Full Rights or View Only Property
                var isFullRights = $jqGrid(viewPatientElement).attr("isfullrights");
                var isViewonly = $jqGrid(viewPatientElement).attr("isviewonly");
                if (isViewonly == "True" && isFullRights == "False") {
                    setTimeout(function () {
                        $jqGrid("#divSearchScreenPatientDetails").addClass("read_only_div");
                        $jqGrid("#divSearchScreenPatientDetails").attr('disabled', 'disabled');
                        $jqGrid("#divSearchScreenPatientDetails").attr('readonly', 'true');
                    }, 200);
                }
            }
            else {

                // SubMenu not available. Need to show error message.
                CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnViewPatientAllowMessage").val(), 'pop_error_ul');
                return false;
            }

            SetPatientLoadParameter(rowData, false);
            window.SelectedPatientStatus = rowData['PatientStatus'];
            LoadSearchPatientDetails();
            $jqGrid(".cust_pat_details").show();

            var stringToHtml = $jqGrid.parseHTML(rowData['PatientName']);
            var patientName = $jqGrid(stringToHtml).text();


            var customerName = rowData['PatientCustomerName'];
            var sapCustomerNumber = rowData['SAPCustomerNumber'];
            CommonScriptObj.PatientId = patientId;
            CommonScriptObj.CustomerId = customerId;
            CommonScriptObj.SAPPatientNumber = rowData["SAPPatientNumber"];
            CommonScriptObj.PatientName = patientName;
            CommonScriptObj.CareHomeId = "0";
            CommonScriptObj.CustomerName = rowData["PatientCustomerName"];
            CommonScriptObj.SAPCustomerNumber = rowData["SAPCustomerNumber"];
            clickevent = "OndblClickRow";

            $jqGrid.ajax({
                url: '/Patient/SetClickEventInSession',
                type: 'GET',
                async: false,
                data: { redirectEvent: clickevent },
                cache: false,
                success: function (Data) {
                    return true;
                }
            });

            var displayCustomerName = customerId + " " + customerName;
            if (sapCustomerNumber != null || sapCustomerNumber != "" || sapCustomerNumber != undefined)
                displayCustomerName += "(" + sapCustomerNumber + ")";

            var displayPatientName = patientId + "/" + patientName;

            $jqGrid("#lblCustomerName").text(displayCustomerName);            
            $jqGrid("#lblPatientName").text(displayPatientName);

            /// Disable the Functionality as per the Role.
            setTimeout(function () {
                GetPatientSubModuleAccess();
            }, 200);
        }
        {
            RedirectToPage(ObjectType, patientId);
        }
    }, 300);

    /// Remove loader.
    /// Currently kept for 3 seconds interval as it will definately take more than 3 seconds for multi user.
    setTimeout(function () {
        $jqComm("#data_loader").hide();
        $jqComm("body").css('overflow', 'auto');
    }, 1000);
}

function RedirectToPage(Type, ObjectId) {
    if (Type == "10114") {
        // Check for Carehome and Customer details
        var viewCarehomeElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnViewCarehomeDetailsMenuText").val() });
        if (viewCarehomeElement.length > 0) {
            window.location.href = "/CareHome/GetCareHome/";
        }
        else {
            // SubMenu not available. Need to show error message.
            CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnViewCarehomeDetailsAllowMessage").val(), 'pop_error_ul');
            return false;
        }
    }
    else if (Type == "10115") {
        // Check for Carehome and Customer details
        var viewCustomerElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnViewCustomerDetailsMenuText").val() });
        if (viewCustomerElement.length > 0) {
            window.location.href = "/Customer/CustomerInformation/";
        }
        else {
            // SubMenu not available. Need to show error message.
            CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnViewCustomerDetailsAllowMessage").val(), 'pop_error_ul');
            return false;
        }
    }
}