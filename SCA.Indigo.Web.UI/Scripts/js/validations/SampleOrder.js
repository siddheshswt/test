﻿$jqGrid = jQuery.noConflict();
var jsfilename = "SampleOrder.js";
var sampleProductCount = 0;
//Reset error message
function ResetErrorMessage() {
    $jqGrid('#MsgSelectProductsToRemoveSample').hide();
}

//Apply styles for all when page gets load
function ApplyStyle() {
    var flag = $jqGrid('#hdnIsEditFlag').val();
    if (flag == "true") {
        $jqGrid("#txtSampleFirstName").addClass("bg_ddd");
        $jqGrid("#txtSampleLastName").addClass("bg_ddd");
        SetDropdownValue('ddlSampleCounty', $jqGrid('#hdnCountyValue').val());
    }
    else {
        $jqGrid("#txtSampleFirstName").removeClass("bg_ddd");
        $jqGrid("#txtSampleLastName").removeClass("bg_ddd");
    }
}

// Show sample pop up
function showPopupSamplePostCode(id) {
    var postCode = $jqGrid("#" + id).val();
    $jqGrid('#MsgSelectProductsToRemoveSample').hide();
    $jqGrid("#jqPostCodeSearchResults").jqGrid('clearGridData');
    showPopupPostCode("SampleOrder", postCode);
}

// Set dropdown value
function SetDropdownValue(ddlid, text) {
    var ddl = document.getElementById(ddlid);
    var selectedflag = false;
    for (var i = 0; i < ddl.options.length; i++) {
        if (ddl.options[i].text.toLowerCase() == text.toLowerCase()) {
            ddl.options[i].setAttribute('selected', 'selected');
            selectedflag = true;
            ddl.selectedIndex = i;
        }
        else {
            ddl.options[i].removeAttribute('selected');
        }
    }
    if (!selectedflag) {
        ddl.selectedIndex = 0;
    }
}

// Remove sample rows
function removeSampleRows(cellvalue, options, rowobject) {
    return "<input type='checkbox' class='chkRemoveClass' id=" + options['rowId'] + "_" + options.colModel["name"] + "_" + rowobject["Quantity"] + ">";
}

// Remove sample order
function removeFromSampleOrder(clearAll, isFirstTimeLoad) {
    $jqGrid('#MsgSelectProductsToRemoveSample').hide();
    var isRemoved = '';
    var rowIds = [];
    if (clearAll == true) {
        $jqGrid(".chkRemoveClass").each(function (rowId) {
            var Trid = $jqGrid(this).closest('tr').prop('id');
            rowIds.push(Trid);
        });
    }
    else {
        $jqGrid(".chkRemoveClass").each(function (rowId) {
            if ($jqGrid(this).is(':checked')) {
                var Trid = $jqGrid(this).closest('tr').prop('id');
                rowIds.push(Trid);
            }

        });
    }

    var reveseIds = (rowIds.reverse());
    if (reveseIds.length == 0 && (!isFirstTimeLoad)) {
        $jqGrid('#MsgSelectProductsToRemoveSample').show();

    }
    else {
        $jqGrid('#MsgSelectProductsToRemoveSample').hide();
        $.each(reveseIds, function (index) {
            var rowId = reveseIds[index];
            $jqGrid('#jqGetSampleOrder').jqGrid('delRowData', rowId);
        })
    }
}

// Save Sample Order
function SaveSampleOrderInfo() {
    $jqGrid('#MsgSelectProductsToRemoveSample').hide();
    var errmsg = ValidateSampleOrderInfo();
    $jqGrid("#divErrorMessage").html(errmsg);

    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        ShowHideError();
        return false;
    }
    else {
        var totalRecordCount = $jqGrid("#jqGetSampleOrder").jqGrid('getGridParam', 'records');
        if (totalRecordCount < 1) {
            $jqGrid("#divErrorMessage").html($jqGrid("#hdnValidProductListCount").val());
            ShowHideError();
            return false;
        }

        var objOrderList = [];
        var gridRows = $jqGrid("#jqGetSampleOrder").jqGrid('getRowData');
        var ids = $jqGrid("#jqGetSampleOrder").jqGrid('getDataIDs');

        var checkFlag = false;
        var invalidQuantity = [];

        for (var i = 0; i < gridRows.length; i++) {
            var rowId = ids[i];
            var row = gridRows[i];

            var isValidProductId = $jqGrid("#" + rowId + "_ProductID").val();

            if (isValidProductId == undefined) {
                isValidProductId = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowId, 'ProductID');
            }

            var isValidHelixProductId = $jqGrid("#" + rowId + "_HelixProductId").val();

            if (isValidHelixProductId == undefined) {
                isValidHelixProductId = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowId, 'HelixProductId');
            }

            var isValidDescription = $jqGrid("#" + rowId + "_Description").val();

            if (isValidDescription == undefined) {
                isValidDescription = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowId, 'Description');
            }

            var isValidQuantity = $jqGrid("#" + rowId + "_Quantity").val();
            if (isValidQuantity == undefined) {
                isValidQuantity = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowId, 'Quantity');
            }

            if (parseInt($.trim(isValidQuantity)) == "NaN" || parseInt($.trim(isValidQuantity)) == 0 || $.trim(isValidQuantity) == "") {
                checkFlag = true;
                invalidQuantity.push(isValidHelixProductId);
            }

            var objOrders = {
                ProductID: isValidProductId,
                HelixProductId: isValidHelixProductId,
                DescriptionUI: isValidDescription,
                MaxQuantity: isValidQuantity
            }
            objOrderList.push(objOrders);
        }

        if (invalidQuantity.length > 0) {
            validateQuantity(invalidQuantity);
            return false;
        }

        var selectedVal = $jqGrid("#ddlSampleCounty option:selected").val();
        var selectedCountyText = $.trim($jqGrid("#ddlSampleCounty option:selected").text());
        if (selectedVal == $jqGrid("#hdnresddlSampleCountyDefaultValue").val())
            selectedVal = "";
        if (selectedCountyText == $jqGrid("#hdnresddlSampleCountyDefaultValue").val())
            selectedCountyText = "";

        var SampleOrderPatientBusinessModel = {
            "FirstName": $.trim($jqGrid('#txtSampleFirstName').val()),
            "LastName": $.trim($jqGrid('#txtSampleLastName').val()),
            "Address1": $.trim($jqGrid('#txtAddress1').val()),
            "Address2": $.trim($jqGrid('#txtAddress2').val()),
            "TownOrCity": $.trim($jqGrid('#txtTownCity').val()),
            "PostCode": $.trim($jqGrid('#txtPostCode').val()),
            "SampleHouseNumber": $.trim($jqGrid('#txtSampleHouseNumber').val()),
            "SampleHouseName": $.trim($jqGrid('#txtSampleHouseName').val()),
            "County": selectedVal,
            "CountyText": selectedCountyText,
            "NoteText": $.trim($jqGrid('#txtSampleOrderNote').val().trim())
        }

        if (objOrderList.length > 0) {
            $jqGrid("#data_loader").show();
            $jqGrid("body").css('overflow', 'hidden');

            // Call Helix service and return invalid data            
            $jqGrid.ajax({

                url: '/Order/CheckSampleOrders',
                type: 'POST',
                contentType: "application/json",
                dataType: 'json',
                data: JSON.stringify({ objSampleOrderPatientBusinessModel: SampleOrderPatientBusinessModel, objProductList: objOrderList }),
                success: function (data) {
                    $(".prepended").remove();

                    // if Only unavailable items shows on the screen then return with Ok message
                    if (data.AvailableItems.length == 0 && data.NotAvailableItems.length > 0) {
                        $('#lblNoItemsPresent').append('<span class="prepended">' + data.ReturnMessage);
                        $jqGrid('#divNoItemsPresent').dialog({
                            autoOpen: false,
                            modal: true,
                            closeOnEscape: false,
                            width: 'auto',
                            resizable: false,
                            buttons: {
                                'Ok': function () {
                                    $jqGrid('#divNoItemsPresent').dialog('close');
                                }
                            }
                        });
                        $jqComm("#data_loader").hide();
                        $jqComm("body").css('overflow', 'auto');

                        $jqGrid('#divNoItemsPresent').dialog('open');
                        $jqGrid(".ui-dialog-titlebar-close").hide();
                        return false;
                    }

                    // if available and unavailable items shows on the screen..
                    if (data.AvailableItems.length > 0 && data.NotAvailableItems.length > 0) {
                        $('#lblInvalidOutputMessage').append('<span class="prepended">' + data.ReturnMessage);
                        $jqGrid('#divInvalidOutputMessage').dialog({
                            autoOpen: false,
                            modal: true,
                            closeOnEscape: false,
                            width: 'auto',
                            resizable: false,
                            buttons: [{
                                text: $jqGrid("#hdnGeneralYes").val().toString(),
                                "id": "hdnGeneralYes",
                                click: function () {
                                    //Send Order
                                    data.YesToSave = true;
                                    sendOrderToHelix(data);
                                    $jqGrid('#divInvalidOutputMessage').dialog('close');                    
                                },
                            }, {
                                text: $jqGrid("#hdnGeneralNo").val().toString(),
                                "id": "hdnGeneralNo",
                                click: function () {
                                    $jqGrid('#divInvalidOutputMessage').dialog('close');
                                },
                            }]
                        });
                        $jqComm("#data_loader").hide();
                        $jqComm("body").css('overflow', 'auto');

                        $jqGrid('#divInvalidOutputMessage').dialog('open');
                        $jqGrid(".ui-dialog-titlebar-close").hide();
                        return false;
                    }
                    
                    // if all items are available on the screen..
                    if (data.AvailableItems.length > 0 && data.NotAvailableItems.length == 0) {
                        $jqGrid('#divValidOutputMessage').dialog({
                            autoOpen: false,
                            modal: true,
                            closeOnEscape: false,
                            width: 'auto',
                            resizable: false,
                            buttons: [{
                                text: $jqGrid("#hdnGeneralYes").val().toString(),
                                "id": "hdnGeneralYes",
                                click: function () {
                                    $jqGrid('#divValidOutputMessage').dialog('close');
                                    //Send Order
                                    data.YesToSave = true;
                                    sendOrderToHelix(data);
                                },
                            }, {
                                text: $jqGrid("#hdnGeneralNo").val().toString(),
                                "id": "hdnGeneralNo",
                                click: function () {
                                    $jqGrid('#divValidOutputMessage').dialog('close');
                                },
                            }]
                        });

                        $jqComm("#data_loader").hide();
                        $jqComm("body").css('overflow', 'auto');

                        $jqGrid('#divValidOutputMessage').dialog('open');
                        $jqGrid(".ui-dialog-titlebar-close").hide();
                        return false;
                    }
                }
            });
            return false;
        }
    }
}

//Start function sendOrderToHelix : for sending sample orders to helix service.
function sendOrderToHelix(objSampleOrder) {
    $jqGrid.ajax({
        url: '/Order/CheckSampleOrders',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify({ objSampleOrderPatientBusinessModel: objSampleOrder }),
        success: function (data) {
            // If Helix Service return false.         
            if (data.HelixOrderID.toLowerCase() == 'invalid') {
                if (data.HelixResponseCode.length > 0)
                {
                    $jqGrid('#divHelixResponse').removeClass('display_none');
                    $jqGrid('#lblResponseCodeValue').text("");
                    $jqGrid('#lblResponseMessageValue').text("");
                    $jqGrid('#lblResponseCodeValue').text(data.HelixResponseCode);
                    $jqGrid('#lblResponseMessageValue').text(data.HelixMessage);
                }
                else {
                    $jqGrid('#divHelixResponse').addClass('display_none');
                }
                $jqGrid('#divHelixIssue').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        'Ok': function () {
                            $jqGrid('#divHelixIssue').dialog('close');
                        }
                    }
                });
                $jqComm("#data_loader").hide();
                $jqComm("body").css('overflow', 'auto');

                $jqGrid('#divHelixIssue').dialog('open');
                $jqGrid(".ui-dialog-titlebar-close").hide();
                return false;
            }
            else {
                //Save data in the SCA Indigo
                SendOrder(objSampleOrder, data.AvailableItems, data.HelixOrderID);
            }
        }
    });
}
//End Function sendOrderToHelix


// Send order once helix service return success response
function SendOrder(SampleOrderPatientBusinessModel, objOrderList, objHelixProductId) {
    $jqGrid('#divErrorMessage').hide();
    $.ajax({
        url: '/Order/SaveSampleOrderDetails',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        data: JSON.stringify({ objSampleOrderPatientBusinessModel: SampleOrderPatientBusinessModel, objProductList: objOrderList, objHelixId: objHelixProductId }),
        success: function (data) {
            $(".prepended").remove();

            $jqGrid('#divOutputMessage').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        if ($jqGrid('#hdnIsEditFlag').val() == "true") {
                            window.location.href = "/Patient/ViewDetails";
                        }
                        else {
                            window.location.href = "/Order/SampleOrder";
                        }
                        $jqGrid('#divOutputMessage').dialog('close');
                    },
                }]
            });
            $jqGrid('#divOutputMessage').dialog('open');
            $jqGrid(".ui-dialog-titlebar-close").hide();
            return false;
        }
    });

}

// Validate sample order before saving
function ValidateSampleOrderInfo() {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";

    errorMsg = errorMsgUl;

    if ($.trim($jqGrid("#txtSampleFirstName").val()) == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnreserrormsgFirstName").val() + errorMsgLiclose;
    }

    if ($.trim($jqGrid("#txtSampleLastName").val()) == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnreserrormsgLastName").val() + errorMsgLiclose;
    }

    if ($.trim($jqGrid("#txtAddress1").val()) == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnreserrormsgAddress1").val() + errorMsgLiclose;
    }

    if ($.trim($jqGrid("#txtTownCity").val()) == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnreserrormsgCity").val() + errorMsgLiclose;
    }

    if ($.trim($jqGrid("#txtPostCode").val()) == "") {
        errorMsg += errorMsgLi + $jqGrid("#hdnreserrormsgPostCode").val() + errorMsgLiclose;
    }


    return errorMsg + errorMsgUlclose;
}

function validateQuantity(invalidQuantity) {
    $(".prepended").remove();
    $('#lblInvalidQuantity').append('<span class="prepended"> for ' + invalidQuantity.join(","));

    $jqGrid('#divErrorInvalidQuantity').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralOk").val().toString(),
            "id": "hdnGeneralOk",
            click: function () {
                $jqGrid('#divErrorInvalidQuantity').dialog('close');
            },
        }]
    });
    $jqGrid('#divErrorInvalidQuantity').dialog('open');
    return false;
}

function ShowHeaderForSampleOrder() {
    var flag = $jqGrid('#hdnIsEditFlag').val();
    if (flag == "true") {
        $jqGrid(".samp_pat_details").show();
        $("#divAutohide").css("height", "0"); //60
        var customerName = $jqGrid('#hdnCustomerName').val();
        var sapcustomerNumber = $jqGrid('#hdnSAPCustomerNumber').val();
        var sappatientNumber = $jqGrid('#hdnSAPPatientNumber').val();
        var patientName = $jqGrid('#hdnPatientName').val();

        if (sapcustomerNumber != "")
        sapcustomerNumber = parseInt(sapcustomerNumber);
        if (customerName.indexOf(sapcustomerNumber.toString()) == -1)
            customerName += " (" + sapcustomerNumber + ")";

        $jqGrid("#lblCustomerName").text('');
        $jqGrid("#lblPatientName").text('');

        $jqGrid("#lblCustomerName").text(customerName);
        $jqGrid("#lblPatientName").text(patientName);
    }
    else {
        $("#divAutohide").css("height", "2");
        $jqGrid(".samp_pat_details").hide();
    }
}

$jqGrid("#btnSampleLetter").click(function () {
    GenerateSampleLetter();
});

//Start Function GenerateSampleLetter*********************************************************************
function GenerateSampleLetter() {
    $jqGrid('#MsgSelectProductsToRemoveSample').hide();
    var errmsg = ValidateSampleOrderInfo();
    $jqGrid("#divErrorMessage").html(errmsg);

    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        ShowHideError();
        return false;
    }
    else {
        getSampleOrderPatientBusinessModel();
    }
}
//End Function GenerateSampleLetter***********************************************************************


function getSampleOrderPatientBusinessModel() {
    var objOrderList = [];
    var invalidQuantity = [];

    var allGriddata = $jqGrid("#jqGetSampleOrder").jqGrid("getGridParam", "data");
    if (allGriddata.length < 1) {
        $jqGrid("#divErrorMessage").html($jqGrid("#hdnValidSampleLetterProductListCount").val());
        ShowHideError();
        return false;
    } else {
        for (var i = 0; i < allGriddata.length; i++) {
            var rowdata = allGriddata[i];
            var productId = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowdata.id, 'ProductID');
            var helixProductId = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowdata.id, 'HelixProductId');
            var description = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowdata.id, 'Description');
            var quantity = $jqGrid("#" + rowdata.id + "_Quantity").val();
            if (quantity == undefined) {
                quantity = $jqGrid('#jqGetSampleOrder').jqGrid('getCell', rowdata.id, 'Quantity');
            }
            if (parseInt($.trim(quantity)) == "NaN" || parseInt($.trim(quantity)) == 0 || $.trim(quantity) == "") {
                invalidQuantity.push(helixProductId);
            }
            var objOrders = {
                ProductID: productId,
                HelixProductId: helixProductId,
                DescriptionUI: description,
                MaxQuantity: quantity
            }
            objOrderList.push(objOrders);
        }
    }
    if (invalidQuantity.length > 0) {
        validateQuantity(invalidQuantity);
        return false;
    }
    else {
        var selectedVal = $jqGrid("#ddlSampleCounty option:selected").val();
        var selectedCountyText = $.trim($jqGrid("#ddlSampleCounty option:selected").text());
        if (selectedVal == $jqGrid("#hdnresddlSampleCountyDefaultValue").val())
            selectedVal = "";
        if (selectedCountyText == $jqGrid("#hdnresddlSampleCountyDefaultValue").val())
            selectedCountyText = "";
        if (objOrderList.length > 0) {
            var sampleOrderPatientBusinessModel = {
                "Title": $.trim($jqGrid('#hdTitle').val()),
                "FirstName": $.trim($jqGrid('#txtSampleFirstName').val()),
                "LastName": $.trim($jqGrid('#txtSampleLastName').val()),
                "Address1": $.trim($jqGrid('#txtAddress1').val()).toUpperCase(),
                "Address2": $.trim($jqGrid('#txtAddress2').val()).toUpperCase(),
                "TownOrCity": $.trim($jqGrid('#txtTownCity').val()).toUpperCase(),
                "PostCode": $.trim($jqGrid('#txtPostCode').val()).toUpperCase(),
                "SampleHouseNumber": $.trim($jqGrid('#txtSampleHouseNumber').val()).toUpperCase(),
                "SampleHouseName": $.trim($jqGrid('#txtSampleHouseName').val()).toUpperCase(),
                "County": selectedVal,
                "CountyText": selectedCountyText,
                "SampleProductBusinessModel": objOrderList
            }
            $jqGrid.ajax({
                url: '/Order/GenerateSampleLetter',
                type: 'POST',
                contentType: "application/json",
                dataType: 'json',
                data: JSON.stringify({ sampleOrderPatientBusinessModel: sampleOrderPatientBusinessModel }),
                success: function (data) {
                    $jqGrid("#data_loader").hide();
                    if (data == "" || data == null) {
                        $jqGrid("#divErrorMessage").html($jqGrid("#hdnErrorCreateSampleLetter").val());
                        ShowHideError();
                        return false;
                    }
                    else {
                        window.open(data);
                    }
                },
                error: function (x, e) {
                    HandleAjaxError(x, e, jsfilename, "getSampleOrderPatientBusinessModel");
                }
            });
        }
    }
    return false;
}

//START : Restrict Special Characters in textbox *****************
$jqGrid(function () {
    $('#txtSampleOrderNote').on("input", function () {
        var noteText = $(this).val();
        var regex = /['"]/gi;
        if (regex.test(noteText)) {
            $(this).val(noteText.replace(regex, ''));
        }
    });
});
//END ************************************************************