﻿$jqGrid = jQuery.noConflict();

// Ready event
$jqGrid(document).ready(function () {
    // sample button click event
    $jqGrid("#btnSampleSearch").click(function () {    
        loadSampleProductdetails();
    });

    // text serach key event
    $jqGrid("#txtSamplesearchproduct").keydown(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13) {
            loadSampleProductdetails();
        }
    });

    // load sample products
    function loadSampleProductdetails() {
        var searchText = $jqGrid('#txtSamplesearchproduct').val();
        if (searchText == "")
            searchText = "blank";
        $jqGrid.getJSON('/Search/DisplaySampleProductDetails',
        {
            SearchText: searchText
        }, function (data) {            
            $jqGrid("#jqSampleProductList").jqGrid('clearGridData');
            $jqGrid("#jqSampleProductList").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
        })
        .fail(function () {  // This one checks for data Exist & also whe there is a exception                                  
            $jqGrid("#jqSampleProductList").jqGrid('clearGridData');
            $jqGrid("#jqSampleProductList").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
        });
    }
});

// Show sample order products
function showPopupSampleProduct(id) {    
    window.gridid = "";
    window.gridid = "jqGetSampleOrder";    
    $jqGrid('#MsgSelectProductsToRemoveSample').hide();

    if ($jqGrid('#txtSamplesearchproduct').val() != "" && $jqGrid('#txtSamplesearchproduct').val() != undefined) {
        $jqGrid('#txtSamplesearchproduct').val('');
    }

    $jqGrid('#divSampleProductList').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: "Sample Product List",
    });
    $jqGrid('#divSampleProductList').dialog('open');

    $jqGrid.ajax({

        url: '/' + $jqGrid("#hdnPatientController").val() + '/' + $jqGrid("#hdnSampleProductDetails").val(),
        type: 'GET',
        cache: false,
        data: { page: 1, rows: 50 },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {            
            $jqGrid("#jqSampleProductList").jqGrid('clearGridData');
            $jqGrid("#jqSampleProductList").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
        }
    });
}







