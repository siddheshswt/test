﻿$jq = jQuery.noConflict();
var ReportBuilderObj = new ReportBuilder();
function ReportBuilder() { }

ReportBuilder.prototype.CustomerColumns = "";
ReportBuilder.prototype.PrescriptionColumns = "";
ReportBuilder.prototype.PatientColumns = "";
ReportBuilder.prototype.CareHomeColumns = "";
ReportBuilder.prototype.OrderColumns = "";
ReportBuilder.prototype.UserMaintenanceColumns = "";
ReportBuilder.prototype.ClinicalContactsColumns = "";
ReportBuilder.prototype.FieldNameText = "";
ReportBuilder.prototype.FieldCustomerColumns = "ddlCustomerColumns";
ReportBuilder.prototype.FieldPrescriptionColumns = "ddlPrescriptionColumns";
ReportBuilder.prototype.FieldCareHomeColumns = "ddlCareHomeColumns";
ReportBuilder.prototype.FieldPatientColumns = "ddlPatientColumns";
ReportBuilder.prototype.FieldOrderColumns = "ddlOrderColumns";
ReportBuilder.prototype.FieldUserMaintenanceColumns = "ddlUserMaintenanceColumns";
ReportBuilder.prototype.FieldClinicalContactsColumns = "ddlClinicalContactsColumns";
ReportBuilder.prototype.FieldSelectedFieldsName = "txtSelectedFieldsName";
ReportBuilder.prototype.ReportFormatDropDown = "ddlReportFormats";
ReportBuilder.prototype.FieldCustomersReport = "ddlCustomersReport";
ReportBuilder.prototype.FieldCareHomeReport = "ddlCareHomeReport";
ReportBuilder.prototype.ReportPatitentTypeDropDown = "ddlPatientTypeListReport";
ReportBuilder.prototype.ReportPatitentStatusDropDown = "ddlPatientStatusListReport";
ReportBuilder.prototype.ReportProductsDropDown = "ddlCustomerProducts";
ReportBuilder.prototype.ReportBuilderErrorBox = "divReportBuilderErrorMessage";
ReportBuilder.prototype.RequiredColumnsMessage = "hdnresRequiredColumns";
ReportBuilder.prototype.RequiredCarehomeOrPatientId = "hdnresRequiredCarehomeOrPatientId";
ReportBuilder.prototype.RequiredCustomer = "hdnresRequiredCustomer";
ReportBuilder.prototype.RequiredFieldMessage = "hdnresRequiredFieldName";
ReportBuilder.prototype.RequiredUserColumns = "hdnresRequiredUserMaintenanceFiels";
ReportBuilder.prototype.GetReportFormatAction = "GetReportFormatAction";
ReportBuilder.prototype.NoRecords = "hdnresNoRecords";
ReportBuilder.prototype.ReportBuilderPager="jqReportGridPager";
ReportBuilder.prototype.ReportBuilderViewModel = [];
ReportBuilder.prototype.ReportBuilderId = "";
ReportBuilder.prototype.CreateColumnModelArray = [];
ReportBuilder.prototype.SelectedColumns = "";
ReportBuilder.prototype.SelectedColumnsHeaders = "";
ReportBuilder.prototype.hdnEnableUserTable = "hdnEnableUserTable";
ReportBuilder.prototype.jqReportGridPager_center = "jqReportGridPager_center";
ReportBuilder.prototype.JQReportGrid = "jqReportGrid";
ReportBuilder.prototype.hdnReportBuilderPageNo = "hdnReportBuilderPageNo";
ReportBuilder.prototype.GenerateReportAction = "GenerateReportAction";
ReportBuilder.prototype.SelectField = "hdnresSelectField";
ReportBuilder.prototype.chkUserData = "chkUserData";
ReportBuilder.prototype.txtPatientNameId = "txtPatientNameId";
ReportBuilder.prototype.dtpPatientDeliveryDateFrom = "dtpPatientDeliveryDateFrom";
ReportBuilder.prototype.dtpPatientDeliveryDateTo = "dtpPatientDeliveryDateTo";
ReportBuilder.prototype.dtpOrderDateFrom = "dtpOrderDateFrom";
ReportBuilder.prototype.dtpOrderDateTo = "dtpOrderDateTo";
ReportBuilder.prototype.dtpPatientCreateDateFrom = "dtpPatientCreateDateFrom";
ReportBuilder.prototype.dtpPatientCreateDateTo = "dtpPatientCreateDateTo";
ReportBuilder.prototype.dtpNextAssessmentDateFrom = "dtpNextAssessmentDateFrom";
ReportBuilder.prototype.dtpNextAssessmentDateTo = "dtpNextAssessmentDateTo";
ReportBuilder.prototype.dtpInvoiceDateFrom = "dtpInvoiceDateFrom";
ReportBuilder.prototype.dtpInvoiceDateTo = "dtpInvoiceDateTo";
ReportBuilder.prototype.txtInvoiceNumberReport = "txtInvoiceNumberReport";
ReportBuilder.prototype.txtInvoiceNumberToReport = "txtInvoiceNumberToReport";
ReportBuilder.prototype.divUserDataOnly = "divUserDataOnly";
ReportBuilder.prototype.divUser = "divUser";
ReportBuilder.prototype.GetProductsAction = "GetProductsAction";
ReportBuilder.prototype.GetCareHomeAction = "GetCareHomeAction";
ReportBuilder.prototype.SaveReportFormatAction = "SaveReportFormatAction";
ReportBuilder.prototype.DownloadReportAction = "DownloadReportAction";
ReportBuilder.prototype.NoReportData = "hdnresErrorNoExcel";
ReportBuilder.prototype.PatientDeliveryDateCheck = "hdnresPatientDeliveryDateCheck";
ReportBuilder.prototype.OrderDateCheck = "hdnresOrderDateCheck";
ReportBuilder.prototype.ReportFormatExists = "hdnresReportFormatExists";
ReportBuilder.prototype.InvalidDeliveryDateFrom = "hdnresInvalidDeliveryDateFrom";
ReportBuilder.prototype.InvalidDeliveryDateTo = "hdnresInvalidDeliveryDateTo";
ReportBuilder.prototype.InvalidOrderDateFrom = "hdnresInvalidOrderDateFrom";
ReportBuilder.prototype.InvalidOrderDateTo = "hdnresInvalidOrderDateTo";
ReportBuilder.prototype.InvoiceDateCheck = "hdnresInvoiceDateCheck";
ReportBuilder.prototype.InvalidInvoiceDateFrom = "hdnresInvalidInvoiceDateFrom";
ReportBuilder.prototype.InvalidInvoiceDateTo = "hdnresInvalidInvoiceDateTo";
ReportBuilder.prototype.PatientCreateDateCheck = "hdnresPatientCreateDateCheck";
ReportBuilder.prototype.InvalidPatientCreateDateFrom = "hdnresInvalidPatientCreateDateFrom";
ReportBuilder.prototype.InvalidPatientCreateDateTo = "hdnresInvalidPatientCreateDateTo";
ReportBuilder.prototype.NextAssessmentDateCheck = "hdnresNextAssessmentDateCheck";
ReportBuilder.prototype.InvalidNextAssessmentDateFrom = "hdnresInvalidNextAssessmentDateFrom";
ReportBuilder.prototype.InvalidNextAssessmentDateTo = "hdnresInvalidNextAssessmentDateTo";
ReportBuilder.prototype.InvalidInvoiceNo = "hdnresInvalidInvoiceNo";
ReportBuilder.prototype.InvoiceNoCheck = "hdnresInvoiceNoCheck";
ReportBuilder.prototype.ReportFormatCreated = "hdnresReportFormatCreated";
ReportBuilder.prototype.ReportFormatUpdated = "hdnresReportFormatUpdated";
ReportBuilder.prototype.btnPrint = "btnPrint";
ReportBuilder.prototype.DivReportBuilder = "divReportBuilder";
ReportBuilder.prototype.PrescriptionOrOrder = "hdnresPrescriptionOrOrder";
ReportBuilder.prototype.hdnresReportTimeOut = "hdnresReportTimeOut";
ReportBuilder.prototype.ReportFormatName = "";
ReportBuilder.prototype.CustomerIds = "";
ReportBuilder.prototype.CareHomeIds = "";
ReportBuilder.prototype.PatientNameId = "";
ReportBuilder.prototype.DeliveryDateFrom = "";
ReportBuilder.prototype.DeliveryDateTo = "";
ReportBuilder.prototype.OrderDateFrom = "";
ReportBuilder.prototype.OrderDateTo = "";
ReportBuilder.prototype.InvoiceDateFrom = "";
ReportBuilder.prototype.InvoiceDateTo = "";
ReportBuilder.prototype.PatientCreateDateFrom = "";
ReportBuilder.prototype.PatientCreateDateTo = "";
ReportBuilder.prototype.NextAssessmentDateFrom = "";
ReportBuilder.prototype.NextAssessmentDateTo = "";
ReportBuilder.prototype.InvoiceNo = "";
ReportBuilder.prototype.InvoiceNoTo = "";
ReportBuilder.prototype.UserDataOnly = false;
ReportBuilder.prototype.PatientType = '';
ReportBuilder.prototype.PatientStatus = '';
ReportBuilder.prototype.ProductIds = '';
ReportBuilder.prototype.PageNo = 1;
ReportBuilder.prototype.Rows = 5;
ReportBuilder.prototype.SortColumn = '';
ReportBuilder.prototype.SortOrder = '';

ReportBuilder.prototype.GenerateReport = function () {
    ReportBuilderObj.SetReportData();
    if (ReportBuilderObj.UserDataOnly && ReportBuilderObj.UserMaintenanceColumns.length == 0) {
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.RequiredUserColumns).val(), 'pop_error_ul');
    }
    else {
        if (ReportBuilderObj.SelectedColumns.length > 0) {

            var isValid = ReportBuilderObj.ValidateReportCriteria();
            if (isValid) {
                var deliveryDateFrom;
                var deliveryDateTo;
                var OrderDateFrom;
                var OrderDateTo;
                var InvoiceDateFrom;
                var InvoiceDateTo;
                var PatientCreateDateFrom;
                var PatientCreateDateTo;
                var NextAssessmentDateFrom;
                var NextAssessmentDateTo;
                if (ReportBuilderObj.DeliveryDateFrom != '') {
                    deliveryDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.DeliveryDateFrom);
                }

                if (ReportBuilderObj.DeliveryDateTo != '') {
                    deliveryDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.DeliveryDateTo);
                }

                if (ReportBuilderObj.OrderDateFrom != '') {
                    OrderDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.OrderDateFrom);
                }

                if (ReportBuilderObj.OrderDateTo != '') {
                    OrderDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.OrderDateTo);
                }

                if (ReportBuilderObj.InvoiceDateFrom != '') {
                    InvoiceDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.InvoiceDateFrom);
                }

                if (ReportBuilderObj.InvoiceDateTo != '') {
                    InvoiceDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.InvoiceDateTo);
                }

                if (ReportBuilderObj.PatientCreateDateFrom != '') {
                    PatientCreateDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.PatientCreateDateFrom);
                }

                if (ReportBuilderObj.PatientCreateDateTo != '') {
                    PatientCreateDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.PatientCreateDateTo);
                }

                if (ReportBuilderObj.NextAssessmentDateFrom != '') {
                    NextAssessmentDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.NextAssessmentDateFrom);
                }

                if (ReportBuilderObj.NextAssessmentDateTo != '') {
                    NextAssessmentDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.NextAssessmentDateTo);
                }
                 
                $jq("#data_loader").show();
                $jq('.form_main_btns').addClass('btn_disable')
                var reportBuilderViewModel = {
                    ColumnNames: ReportBuilderObj.SelectedColumns,
                    PatientNameId: ReportBuilderObj.PatientNameId,
                    CustomerIds: ReportBuilderObj.CustomerIds,
                    CareHomeIds: ReportBuilderObj.CareHomeIds,
                    PatientDeliveryDateFrom: deliveryDateFrom,
                    PatientDeliveryDateTo: deliveryDateTo,
                    OrderDateFrom: OrderDateFrom,
                    OrderDateTo: OrderDateTo,
                    InvoiceDateFrom: InvoiceDateFrom,
                    InvoiceDateTo: InvoiceDateTo,
                    PatientCreateDateFrom: PatientCreateDateFrom,
                    PatientCreateDateTo: PatientCreateDateTo,
                    NextAssessmentDateFrom: NextAssessmentDateFrom,
                    NextAssessmentDateTo: NextAssessmentDateTo,
                    InvoiceNumber: ReportBuilderObj.InvoiceNo,
                    InvoiceNumberTo: ReportBuilderObj.InvoiceNoTo,
                    UserDataOnly: ReportBuilderObj.UserDataOnly,
                    FieldCustomerColumns: ReportBuilderObj.CustomerColumns,
                    FieldPatientColumns: ReportBuilderObj.PatientColumns,
                    FieldPrescriptionColumns: ReportBuilderObj.PrescriptionColumns,
                    FieldClinicalContactsColumns: ReportBuilderObj.ClinicalContactsColumns,
                    FieldCareHomeColumns: ReportBuilderObj.CareHomeColumns,
                    FieldOrderColumns: ReportBuilderObj.OrderColumns,
                    FieldUserMaintenanceColumns: ReportBuilderObj.UserMaintenanceColumns,
                    PatientStatus: ReportBuilderObj.PatientStatus,
                    PatientType: ReportBuilderObj.PatientType,
                    ProductIds: ReportBuilderObj.ProductIds,
                }

                ReportBuilderObj.BindColumnsToJqGrid();
                $jq("#" + ReportBuilderObj.JQReportGrid).jqGrid('clearGridData');
                 $jq("#" + ReportBuilderObj.JQReportGrid).jqGrid('setGridParam',
                {
                    postData: {
                        reportBuilderViewModel: reportBuilderViewModel
                    }, datatype: 'json'                    
                }).trigger('reloadGrid');

                $jqComm("#data_loader").hide();
                $jq(".ui-autocomplete").hide();
                $jqComm("body").css('overflow', 'auto');
                $jq('.form_main_btns').removeClass('btn_disable')
            }
        }
        else {
            $jqComm("#data_loader").hide();
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.SelectField).val(), 'pop_error_ul');
        }
    }
}

$jq(document).ready(function () {

    $jq("#" + ReportBuilderObj.FieldCustomersReport).on('change', function (e) {
        $jq("#" + ReportBuilderObj.ReportProductsDropDown).hide().empty();
        $jq("#" + ReportBuilderObj.FieldCareHomeReport).hide().empty();

        var customerIds = "," + $jqComm("#" + ReportBuilderObj.FieldCustomersReport).multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        customerIds = customerIds.substring(1, customerIds.length)

        if (customerIds.length > 0) {
            $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).multiselect('enable');
            LoadCustomerPatientType(customerIds, ReportBuilderObj.ReportPatitentTypeDropDown)
            ReportBuilderObj.GetProducts(customerIds);
            ReportBuilderObj.GetCareHome(customerIds);
        }
        else {
            $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).empty();
            $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).multiselect("disable");
            $jq("#" + ReportBuilderObj.ReportProductsDropDown).multiselect("disable");
            $jq("#" + ReportBuilderObj.FieldCareHomeReport).multiselect("disable");
            ReportBuilderObj.GetProducts("0");
            ReportBuilderObj.GetCareHome("0");
        }
    });

    $jq("#" + ReportBuilderObj.ReportFormatDropDown).on('change', function (e) {        
        ReportBuilderObj.ReportFormatChangeEvent();
    });

    setTimeout(function () {
        ///Check for ViewOnly 
        ///Element: Main div of the form
        CommonScriptObj.DisableFormControls(ReportBuilderObj.DivReportBuilder);
        $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).multiselect("uncheckAll");
        $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).multiselect("disable");
        if ($jq('#' + ReportBuilderObj.hdnEnableUserTable).val() == false || $jq('#' + ReportBuilderObj.hdnEnableUserTable).val() == "False") {
            $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).multiselect("disable");
            $jq('#' + ReportBuilderObj.chkUserData).addClass('btn_disable');
            $jq('#' + ReportBuilderObj.divUser).hide();
            $jq('#' + ReportBuilderObj.divUserDataOnly).hide();
        }

        $jq('#' + ReportBuilderObj.btnPrint).addClass("btn_disable");
        $jq('#' + ReportBuilderObj.btnPrint).attr('disabled', 'disabled');
        $jq("#" + ReportBuilderObj.ReportProductsDropDown).multiselect("disable");
        $jq("#" + ReportBuilderObj.FieldCareHomeReport).multiselect("disable");
    }, 500);

    /// Check for ViewOnly 
    /// Element: - Main Div of the Form
    CommonScriptObj.DisableFormControls("frm_ReportBuilder");
    
    $jq('#' + ReportBuilderObj.txtInvoiceNumberReport).change(function () {        
        $jq('#' + ReportBuilderObj.txtInvoiceNumberToReport).val('');
        $jq('#' + ReportBuilderObj.txtInvoiceNumberToReport).val($jq('#' + ReportBuilderObj.txtInvoiceNumberReport).val());
    });

    $jq('#' + ReportBuilderObj.dtpInvoiceDateFrom).change(function () {        
        $jq('#' + ReportBuilderObj.dtpInvoiceDateTo).val('');
        $jq('#' + ReportBuilderObj.dtpInvoiceDateTo).val($jq('#' + ReportBuilderObj.dtpInvoiceDateFrom).val());
    });
    
    $jq('#' + ReportBuilderObj.dtpPatientCreateDateFrom).change(function () {
        $jq('#' + ReportBuilderObj.dtpPatientCreateDateTo).val('');
        $jq('#' + ReportBuilderObj.dtpPatientCreateDateTo).val($jq('#' + ReportBuilderObj.dtpPatientCreateDateFrom).val());
    });

    $jq('#' + ReportBuilderObj.dtpNextAssessmentDateFrom).change(function () {
        $jq('#' + ReportBuilderObj.dtpNextAssessmentDateTo).val('');
        $jq('#' + ReportBuilderObj.dtpNextAssessmentDateTo).val($jq('#' + ReportBuilderObj.dtpNextAssessmentDateFrom).val());
    });
});

//START******************LoadCustomerPatientType*************************************************
function LoadCustomerPatientType(selectedCustomers, ddlId) {
    ddlId = "#" + ddlId;
    $jqGrid(ddlId).empty();
    if (selectedCustomers != $("#hdnresddlCUstomerDefaultValue").val()) {
        $jqGrid.ajax({
            url: '/ReportBuilder/GetPatientTypeOnCustomerId',
            type: 'GET',
            cache: false,
            async: false,
            data: { customerIds: selectedCustomers.toString() },
            success: function (responseData) {
                if (responseData != false) {
                    $jqGrid.each(responseData, function (key, option) {
                        $jqGrid(ddlId).append($jqGrid('<option></option>').val(option.Value).html(option.Text));
                    });
                    $jqGrid(ddlId).multiselect('refresh');
                    $jqGrid('.ui-corner-all input[type="checkbox"]').ezMark();
                }
            },
            error: function (x, e) {
                HandleAjaxError(x, e, jsfilename, "LoadCustomerPatientType");
            }
        });
    }
}
//END******************LoadCustomerPatientType*************************************************

ReportBuilder.prototype.ReportFormatChangeEvent = function ()
{
    $jq('#' + ReportBuilderObj.chkUserData).prop("checked", false);
    ReportBuilderObj.ReportBuilderId = $jq("#" + ReportBuilderObj.ReportFormatDropDown).val();
    if (ReportBuilderObj.ReportBuilderId != "") {
        $jq("#data_loader").show();
        $jq.ajax({
            url: $jq("#" + ReportBuilderObj.GetReportFormatAction).val(),
            type: 'GET',
            cache: false,
            async: false,
            data: { reportFormatId: ReportBuilderObj.ReportBuilderId },
            success: function (responsedata) {
                /// bind the multiselect check box here 
                ReportBuilderObj.SetSelectFieldValues(responsedata);
                ReportBuilderObj.SetReportData();
                ReportBuilderObj.BindColumnsToJqGrid();
                $jqComm('.ui-corner-all input[type="checkbox"]').ezMark();
                $jq("#data_loader").hide();
            },
            error: function () {
                $jq("#data_loader").hide();
            }   
        });
    }
    else {
        ReportBuilderObj.UnCheckAllSelectFields();
    }
}

ReportBuilder.prototype.SetSelectFieldValues = function (data)
{
    ReportBuilderObj.UnCheckAllSelectFields();
    if (data.CustomerColumn != undefined && data.CustomerColumn.length > 0) {
        SelectedArray = [];
        SelectedArray = data.CustomerColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldCustomerColumns).val(SelectedArray);
        $jq("#" + ReportBuilderObj.FieldCustomerColumns).multiselect('refresh');
    }

    if (data.OrderColumn != undefined && data.OrderColumn.length > 0) {
        OrderColumn = [];
        OrderColumn = data.OrderColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldOrderColumns).val(OrderColumn);
        $jq("#" + ReportBuilderObj.FieldOrderColumns).multiselect('refresh');
    }

    if (data.PatientColumn != undefined && data.PatientColumn.length > 0) {
        SelectedArray = [];
        SelectedArray = data.PatientColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldPatientColumns).val(SelectedArray);
        $jq("#" + ReportBuilderObj.FieldPatientColumns).multiselect('refresh');
    }

    if (data.PerscriptionColumn != undefined && data.PerscriptionColumn.length > 0) {
        SelectedArray = [];
        SelectedArray = data.PerscriptionColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).val(SelectedArray);
        $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).multiselect('refresh');
    }

    if (data.UserMaintainanceColumn != undefined && data.UserMaintainanceColumn.length > 0) {
        SelectedArray = [];
        SelectedArray = data.UserMaintainanceColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).val(SelectedArray);
        $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).multiselect('refresh');
    }

    if (data.CareHomeColumn != undefined && data.CareHomeColumn.length > 0) {
        SelectedArray = [];
        SelectedArray = data.CareHomeColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldCareHomeColumns).val(SelectedArray);
        $jq("#" + ReportBuilderObj.FieldCareHomeColumns).multiselect('refresh');
    }

    if (data.ClinicalContactColumn != undefined && data.ClinicalContactColumn.length > 0) {
        SelectedArray = [];
        SelectedArray = data.ClinicalContactColumn.split(',');
        $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).val(SelectedArray);
        $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).multiselect('refresh');
    }

    $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val(data.ReportFormatName);
    ReportBuilderObj.ReportFormatName = data.ReportFormatName;
}

ReportBuilder.prototype.ValidateReportFormatSave = function ()
{    
    var customerColumns = $jq("#" +ReportBuilderObj.FieldCustomerColumns).val() == null;
    var prescriptionColumns = $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).val() == null;
    var careHomeColumns = $jq("#" + ReportBuilderObj.FieldCareHomeColumns).val() == null;
    var patientColumns = $jq("#"+ReportBuilderObj.FieldPatientColumns).val() == null;
    var orderColumns = $jq("#" + ReportBuilderObj.FieldOrderColumns).val() == null;
    var userMaintenanceColumns = $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).val() == null;
    var clinicalContactsColumns = $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).val() == null;
    var fieldNameText = $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val() == "" || $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val() == null;

    var isValid = true;
    /// if all are true then throw the error message
    if (customerColumns && prescriptionColumns && careHomeColumns &&
        patientColumns && orderColumns && userMaintenanceColumns && clinicalContactsColumns)
    {
        /// at least one column should be selected
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.RequiredColumnsMessage).val(), 'pop_error_ul');
        return false;
    }

    if (!prescriptionColumns && !orderColumns) {
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" +ReportBuilderObj.PrescriptionOrOrder).val(), 'pop_error_ul');
        return false;
    }

    if (fieldNameText)
    {
        /// Report format name is required
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.RequiredFieldMessage).val(), 'pop_error_ul');
        return false;        
    }

    if ((ReportBuilderObj.ReportBuilderId == "" || ReportBuilderObj.ReportBuilderId == "0") || ReportBuilderObj.ReportFormatName.trim() != $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val().trim()) {
        $jq("#" + ReportBuilderObj.ReportFormatDropDown + " > option").each(function () {
            if (this.text.trim() == $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val().trim()) {
                CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.ReportFormatExists).val(), 'pop_error_ul');
                isValid = false;
            }
        });
    }

    return isValid;
}

ReportBuilder.prototype.ReportBuilderSave = function ()
{
    if (ReportBuilderObj.ValidateReportFormatSave())
    {        
        $jq("#data_loader").show();
        ReportBuilderObj.SetReportData();
        var message = '';
        if (ReportBuilderObj.ReportFormatName.trim() != $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val().trim() && ReportBuilderObj.ReportBuilderId != "" && ReportBuilderObj.ReportBuilderId != "0")
        {
            ReportBuilderObj.ReportBuilderId = 0;
            message = $jq('#' + ReportBuilderObj.ReportFormatCreated).val();
        }
        else if (ReportBuilderObj.ReportFormatName.trim() == $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val().trim())
        {
            message = $jq('#' + ReportBuilderObj.ReportFormatUpdated).val();
        }
        else {
            message = $jq('#' + ReportBuilderObj.ReportFormatCreated).val();
        }

        $jq.ajax({
            url: $jq('#' + ReportBuilderObj.SaveReportFormatAction).val(),
            type: 'POST',
            cache: false,
            async: false,
            data: {
                FieldCustomerColumns: ReportBuilderObj.CustomerColumns,
                FieldPrescriptionColumns: ReportBuilderObj.PrescriptionColumns,
                FieldCareHomeColumns: ReportBuilderObj.CareHomeColumns,
                FieldPatientColumns: ReportBuilderObj.PatientColumns,
                FieldOrderColumns: ReportBuilderObj.OrderColumns,
                FieldUserMaintenanceColumns: ReportBuilderObj.UserMaintenanceColumns,
                FieldClinicalContactsColumns:ReportBuilderObj.ClinicalContactsColumns,
                FieldReportFormatName: $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val(),
                ReportBuilderId: ReportBuilderObj.ReportBuilderId
            },
            success: function () {
                $jq("#data_loader").hide();
                $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).html(message);
                $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: "Ok",
                        "id": "btnYes",
                        click: function () {
                            window.location.reload();
                        },
                    }]
                });
                $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).dialog('open');
            },
            error: function () {
                $jq("#data_loader").hide();
            }
        });
    }
}

ReportBuilder.prototype.SetReportData = function ()
{    
    ReportBuilderObj.CustomerIds = '';
    ReportBuilderObj.CareHomeIds = '';
    ReportBuilderObj.PatientStatus = '';
    ReportBuilderObj.PatientType = '';
    ReportBuilderObj.ProductIds = '';
    ReportBuilderObj.PatientNameId = $jq('#' + ReportBuilderObj.txtPatientNameId).val();
    ReportBuilderObj.DeliveryDateFrom = $jq('#' + ReportBuilderObj.dtpPatientDeliveryDateFrom).val();
    ReportBuilderObj.DeliveryDateTo = $jq('#' + ReportBuilderObj.dtpPatientDeliveryDateTo).val();
    ReportBuilderObj.OrderDateFrom = $jq('#' + ReportBuilderObj.dtpOrderDateFrom).val();
    ReportBuilderObj.OrderDateTo = $jq('#' + ReportBuilderObj.dtpOrderDateTo).val();
    ReportBuilderObj.InvoiceDateFrom = $jq('#' + ReportBuilderObj.dtpInvoiceDateFrom).val();
    ReportBuilderObj.InvoiceDateTo = $jq('#' + ReportBuilderObj.dtpInvoiceDateTo).val();
    ReportBuilderObj.PatientCreateDateFrom = $jq('#' + ReportBuilderObj.dtpPatientCreateDateFrom).val();
    ReportBuilderObj.PatientCreateDateTo = $jq('#' + ReportBuilderObj.dtpPatientCreateDateTo).val();
    ReportBuilderObj.NextAssessmentDateFrom = $jq('#' + ReportBuilderObj.dtpNextAssessmentDateFrom).val();
    ReportBuilderObj.NextAssessmentDateTo = $jq('#' + ReportBuilderObj.dtpNextAssessmentDateTo).val();
    ReportBuilderObj.InvoiceNo = $jq('#' + ReportBuilderObj.txtInvoiceNumberReport).val();
    ReportBuilderObj.InvoiceNoTo = $jq('#' + ReportBuilderObj.txtInvoiceNumberToReport).val();
    ReportBuilderObj.UserDataOnly = $jq('#' + ReportBuilderObj.chkUserData).is(':checked');

    if ($jq("#" + ReportBuilderObj.FieldCustomersReport).val() != "" && $jq("#" + ReportBuilderObj.FieldCustomersReport).val() != null) {
        ReportBuilderObj.CustomerIds = "," + $jq("#" + ReportBuilderObj.FieldCustomersReport).multiselect("getChecked").map(function () {
            return this.value;
        }).get();
    }

    ReportBuilderObj.CustomerIds = ReportBuilderObj.CustomerIds.substring(1, ReportBuilderObj.CustomerIds.length)

    if ($jq("#" + ReportBuilderObj.FieldCareHomeReport).val() != "" && $jq("#" + ReportBuilderObj.FieldCareHomeReport).val() != null) {
        ReportBuilderObj.CareHomeIds = "," + $jq("#" + ReportBuilderObj.FieldCareHomeReport).multiselect("getChecked").map(function () {
            return this.value;
        }).get();
    }

    ReportBuilderObj.CareHomeIds = ReportBuilderObj.CareHomeIds.substring(1, ReportBuilderObj.CareHomeIds.length);

    if ($jq("#" + ReportBuilderObj.ReportPatitentStatusDropDown).val() != "" && $jq("#" + ReportBuilderObj.ReportPatitentStatusDropDown).val() != null) {
        ReportBuilderObj.PatientStatus = "," + $jq("#" + ReportBuilderObj.ReportPatitentStatusDropDown).multiselect("getChecked").map(function () {
            return this.value;
        }).get();
    }

    ReportBuilderObj.PatientStatus = ReportBuilderObj.PatientStatus.substring(1, ReportBuilderObj.PatientStatus.length);

    if ($jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).val() != "" && $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).val() != null) {
        ReportBuilderObj.PatientType = "," + $jq("#" + ReportBuilderObj.ReportPatitentTypeDropDown).multiselect("getChecked").map(function () {
            return this.value;
        }).get();
    }

    ReportBuilderObj.PatientType = ReportBuilderObj.PatientType.substring(1, ReportBuilderObj.PatientType.length);

    if ($jq("#" + ReportBuilderObj.ReportProductsDropDown).val() != "" && $jq("#" + ReportBuilderObj.ReportProductsDropDown).val() != null) {
        ReportBuilderObj.ProductIds = "," + $jq("#" + ReportBuilderObj.ReportProductsDropDown).multiselect("getChecked").map(function () {
            return this.value;
        }).get();
    }

    ReportBuilderObj.ProductIds = ReportBuilderObj.ProductIds.substring(1, ReportBuilderObj.ProductIds.length);

    ReportBuilderObj.SelectedColumns = "";
    ReportBuilderObj.SelectedColumnsHeaders = "";
    ReportBuilderObj.CustomerColumns = "";
    ReportBuilderObj.PatientColumns = "";
    ReportBuilderObj.PrescriptionColumns = "";
    ReportBuilderObj.CareHomeColumns = "";
    ReportBuilderObj.OrderColumns = "";
    ReportBuilderObj.UserMaintenanceColumns = "";
    ReportBuilderObj.ClinicalContactsColumns = "";

    if (!ReportBuilderObj.UserDataOnly) {
        if ($jq("#" + ReportBuilderObj.FieldCustomerColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldCustomerColumns).val() != null) {
            ReportBuilderObj.CustomerColumns = "," + $jq("#" + ReportBuilderObj.FieldCustomerColumns).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            ReportBuilderObj.SelectedColumns += ReportBuilderObj.CustomerColumns;
        }

        if (ReportBuilderObj.CustomerColumns.length > 0) {
            ReportBuilderObj.CustomerColumns = ReportBuilderObj.CustomerColumns.substring(1, ReportBuilderObj.CustomerColumns.length)
        }

        if ($jq("#" + ReportBuilderObj.FieldCustomerColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldCustomerColumns).val() != null) {
            ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldCustomerColumns).multiselect("getChecked").map(function () {
                return this.title;
            }).get();
        }

        if ($jq("#" + ReportBuilderObj.FieldPatientColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldPatientColumns).val() != null) {
            ReportBuilderObj.PatientColumns = "," + $jq("#" + ReportBuilderObj.FieldPatientColumns).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            ReportBuilderObj.SelectedColumns += ReportBuilderObj.PatientColumns;
        }

        if (ReportBuilderObj.PatientColumns.length > 0) {
            ReportBuilderObj.PatientColumns = ReportBuilderObj.PatientColumns.substring(1, ReportBuilderObj.PatientColumns.length)
        }

        if ($jq("#" + ReportBuilderObj.FieldPatientColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldPatientColumns).val() != null) {
            ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldPatientColumns).multiselect("getChecked").map(function () {
                return this.title;
            }).get();
        }

        if ($jq("#" + ReportBuilderObj.FieldPrescriptionColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).val() != null) {
            ReportBuilderObj.PrescriptionColumns = "," + $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            ReportBuilderObj.SelectedColumns += ReportBuilderObj.PrescriptionColumns;
        }

        if (ReportBuilderObj.PrescriptionColumns.length > 0) {
            ReportBuilderObj.PrescriptionColumns = ReportBuilderObj.PrescriptionColumns.substring(1, ReportBuilderObj.PrescriptionColumns.length)
        }

        if ($jq("#" + ReportBuilderObj.FieldPrescriptionColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).val() != null) {
            ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).multiselect("getChecked").map(function () {
                return this.title;
            }).get();
        }

        if ($jq("#" + ReportBuilderObj.FieldCareHomeColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldCareHomeColumns).val() != null) {
            ReportBuilderObj.CareHomeColumns = "," + $jq("#" + ReportBuilderObj.FieldCareHomeColumns).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            ReportBuilderObj.SelectedColumns += ReportBuilderObj.CareHomeColumns;
        }

        if (ReportBuilderObj.CareHomeColumns.length > 0) {
            ReportBuilderObj.CareHomeColumns = ReportBuilderObj.CareHomeColumns.substring(1, ReportBuilderObj.CareHomeColumns.length)
        }

        if ($jq("#" + ReportBuilderObj.FieldCareHomeColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldCareHomeColumns).val() != null) {
            ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldCareHomeColumns).multiselect("getChecked").map(function () {
                return this.title;
            }).get();
        }

        if ($jq("#" + ReportBuilderObj.FieldOrderColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldOrderColumns).val() != null) {
            ReportBuilderObj.OrderColumns = "," + $jq("#" + ReportBuilderObj.FieldOrderColumns).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            ReportBuilderObj.SelectedColumns += ReportBuilderObj.OrderColumns;
        }

        if (ReportBuilderObj.OrderColumns.length > 0) {
            ReportBuilderObj.OrderColumns = ReportBuilderObj.OrderColumns.substring(1, ReportBuilderObj.OrderColumns.length)
        }

        if ($jq("#" + ReportBuilderObj.FieldOrderColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldOrderColumns).val() != null) {
            ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldOrderColumns).multiselect("getChecked").map(function () {
                return this.title;
            }).get();
        }

        if ($jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).val() != null) {
            ReportBuilderObj.ClinicalContactsColumns = "," + $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            ReportBuilderObj.SelectedColumns += ReportBuilderObj.ClinicalContactsColumns;
        }

        if (ReportBuilderObj.ClinicalContactsColumns.length > 0) {
            ReportBuilderObj.ClinicalContactsColumns = ReportBuilderObj.ClinicalContactsColumns.substring(1, ReportBuilderObj.ClinicalContactsColumns.length)
        }

        if ($jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).val() != null) {
            ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).multiselect("getChecked").map(function () {
                return this.title;
            }).get();
        }
    }

    if ($jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).val() != null) {
        ReportBuilderObj.UserMaintenanceColumns = "," + $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        ReportBuilderObj.SelectedColumns += ReportBuilderObj.UserMaintenanceColumns;
    }

    if (ReportBuilderObj.UserMaintenanceColumns.length > 0) {
        ReportBuilderObj.UserMaintenanceColumns = ReportBuilderObj.UserMaintenanceColumns.substring(1, ReportBuilderObj.UserMaintenanceColumns.length)
    }

    if ($jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).val() != "" && $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).val() != null) {
        ReportBuilderObj.SelectedColumnsHeaders += "," + $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).multiselect("getChecked").map(function () {
            return this.title;
        }).get();
    }

    if (ReportBuilderObj.SelectedColumns.length > 0) {
        ReportBuilderObj.SelectedColumns = ReportBuilderObj.SelectedColumns.substring(1, ReportBuilderObj.SelectedColumns.length)
    }

    if (ReportBuilderObj.SelectedColumnsHeaders.length > 0) {
        ReportBuilderObj.SelectedColumnsHeaders = ReportBuilderObj.SelectedColumnsHeaders.substring(1, ReportBuilderObj.SelectedColumnsHeaders.length)
    }
}

ReportBuilder.prototype.UnCheckAllSelectFields = function ()
{
    $jq("#" + ReportBuilderObj.FieldCareHomeColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldCustomerColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldOrderColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldPatientColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldPrescriptionColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldUserMaintenanceColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldClinicalContactsColumns).multiselect("uncheckAll");
    $jq("#" + ReportBuilderObj.FieldSelectedFieldsName).val('');
}

ReportBuilder.prototype.ReportBuilderUndo = function ()
{
    window.location.reload();
}

ReportBuilder.prototype.BindColumnsToJqGrid = function (rowCount)
{
    ReportBuilderObj.CreateColumnModel();

    $jq("#" + ReportBuilderObj.JQReportGrid).jqGrid("GridUnload")

    $jq("#" + ReportBuilderObj.JQReportGrid).jqGrid({
        url: $jq('#' + ReportBuilderObj.GenerateReportAction).val(),
        datatype: "local",
        mtype: "POST",
        colNames: ReportBuilderObj.SelectedColumnsHeaders.split(','),
        colModel: ReportBuilderObj.CreateColumnModelArray,
        height: '135',
        width: '895',
        toppager: false,
        jsonReader: { repeatitems: false },
        viewrecords: true,
        ignoreCase: true,
        sortname: "",
        sortorder: "",
        rowNum: 10,
        rowList: [10, 20, 50, 100],
        emptyrecords: $jq("#" + ReportBuilderObj.NoRecords).val(),
        pager: $jq("#" + ReportBuilderObj.ReportBuilderPager),
        shrinkToFit: false,
        loadError: function (xhr, textStatus, errorThrown)
        {            
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.hdnresReportTimeOut).val(), 'pop_error_ul');
        },
        loadComplete: function () {
            var page = $jq("#" + ReportBuilderObj.JQReportGrid).jqGrid('getGridParam', 'page');
            $jq("#" + ReportBuilderObj.hdnReportBuilderPageNo).val(page);
            
        },
        gridComplete: function () {
            $jq('.ui-paging-info').css("text-align", "left");
            $jq('#' + ReportBuilderObj.ReportBuilderPager + '_left').hide();
            var rowcount = $jq(this).jqGrid('getGridParam', 'records');
            if (rowcount == 0) {
                $jq('#' + ReportBuilderObj.ReportBuilderPager + '_center').hide();
                $jq('#' + ReportBuilderObj.btnPrint).addClass("btn_disable");
                $jq('#' + ReportBuilderObj.btnPrint).attr('disabled', 'disabled');

            } else {
                $jq('#' + ReportBuilderObj.ReportBuilderPager + '_center').show();
                $jq('#' + ReportBuilderObj.btnPrint).removeClass("btn_disable")
                $jq('#' + ReportBuilderObj.btnPrint).removeAttr("disabled")
            }
        },
        onPaging: function (pgButton) {
        },
        onSortCol: function (index, iCol, sortOrder) {
            $jq("#" + ReportBuilderObj.JQReportGrid).jqGrid('setGridParam', { page: $jq("#" + ReportBuilderObj.hdnReportBuilderPageNo).val() });
        }
    });
}

ReportBuilder.prototype.CreateColumnModel = function () {
    var data = ReportBuilderObj.SelectedColumns.split(',');
    ReportBuilderObj.CreateColumnModelArray = [];
    for (var i = 0 ; i < data.length; i++) {
        if (data[i].toLowerCase().indexOf('date') != -1) {
            ReportBuilderObj.CreateColumnModelArray.push({ name: data[i], index: data[i], width: 100, classes: 'align_left padding_left_10', formatter: 'date', formatoptions: { srcformat: 'm/d/y H:i:s', newformat: 'd/m/Y' } });
        }
        else{
            ReportBuilderObj.CreateColumnModelArray.push({ name: data[i], index: data[i], width: 100, classes: 'align_left padding_left_10' });
        }
    }
}

ReportBuilder.prototype.DownloadReport = function () {
    ReportBuilderObj.SetReportData();
    if (ReportBuilderObj.UserDataOnly && ReportBuilderObj.UserMaintenanceColumns.length == 0) {
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.RequiredUserColumns).val(), 'pop_error_ul');
    }
    else {
        if (ReportBuilderObj.SelectedColumns.length > 0) {

            var isValid = ReportBuilderObj.ValidateReportCriteria();
            if (isValid) {
                $jq("#data_loader").show();

                var deliveryDateFrom;
                var deliveryDateTo;
                var OrderDateFrom;
                var OrderDateTo;
                var InvoiceDateFrom;
                var InvoiceDateTo;
                var PatientCreateDateFrom;
                var PatientCreateDateTo;
                var NextAssessmentDateFrom;
                var NextAssessmentDateTo;
                if (ReportBuilderObj.DeliveryDateFrom != '') {
                    deliveryDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.DeliveryDateFrom);
                }

                if (ReportBuilderObj.DeliveryDateTo != '') {
                    deliveryDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.DeliveryDateTo);
                }

                if (ReportBuilderObj.OrderDateFrom != '') {
                    OrderDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.OrderDateFrom);
                }

                if (ReportBuilderObj.OrderDateTo != '') {
                    OrderDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.OrderDateTo);
                }

                if (ReportBuilderObj.InvoiceDateFrom != '') {
                    InvoiceDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.InvoiceDateFrom);
                }

                if (ReportBuilderObj.InvoiceDateTo != '') {
                    InvoiceDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.InvoiceDateTo);
                }

                if (ReportBuilderObj.PatientCreateDateFrom != '') {
                    PatientCreateDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.PatientCreateDateFrom);
                }

                if (ReportBuilderObj.PatientCreateDateTo != '') {
                    PatientCreateDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.PatientCreateDateTo);
                }

                if (ReportBuilderObj.NextAssessmentDateFrom != '') {
                    NextAssessmentDateFrom = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.NextAssessmentDateFrom);
                }

                if (ReportBuilderObj.NextAssessmentDateTo != '') {
                    NextAssessmentDateTo = CommonScriptObj.DateTextToMMDDYYYY(ReportBuilderObj.NextAssessmentDateTo);
                }

                var url = $jq('#' + ReportBuilderObj.DownloadReportAction).val();
                $jq.ajax({
                    dataType: 'json',
                    type: "POST",
                    async: false,
                    cache: false,
                    url: url,
                    data: {
                        ColumnNames: ReportBuilderObj.SelectedColumns,
                        PatientNameId: ReportBuilderObj.PatientNameId,
                        CustomerIds: ReportBuilderObj.CustomerIds,
                        CareHomeIds: ReportBuilderObj.CareHomeIds,
                        PatientDeliveryDateFrom: deliveryDateFrom,
                        PatientDeliveryDateTo: deliveryDateTo,
                        OrderDateFrom: OrderDateFrom,
                        OrderDateTo: OrderDateTo,
                        InvoiceDateFrom: InvoiceDateFrom,
                        InvoiceDateTo: InvoiceDateTo,
                        PatientCreateDateFrom: PatientCreateDateFrom,
                        PatientCreateDateTo: PatientCreateDateTo,
                        NextAssessmentDateFrom: NextAssessmentDateFrom,
                        NextAssessmentDateTo: NextAssessmentDateTo,
                        InvoiceNumber: ReportBuilderObj.InvoiceNo,
                        InvoiceNumberTo: ReportBuilderObj.InvoiceNoTo,
                        UserDataOnly: ReportBuilderObj.UserDataOnly,
                        FieldCustomerColumns: ReportBuilderObj.CustomerColumns,
                        FieldPatientColumns: ReportBuilderObj.PatientColumns,
                        FieldPrescriptionColumns: ReportBuilderObj.PrescriptionColumns,
                        FieldClinicalContactsColumns: ReportBuilderObj.ClinicalContactsColumns,
                        FieldCareHomeColumns: ReportBuilderObj.CareHomeColumns,
                        FieldOrderColumns: ReportBuilderObj.OrderColumns,
                        FieldUserMaintenanceColumns: ReportBuilderObj.UserMaintenanceColumns,
                        ColumnHeaders: ReportBuilderObj.SelectedColumnsHeaders,
                        PatientStatus: ReportBuilderObj.PatientStatus,
                        PatientType: ReportBuilderObj.PatientType,
                        ProductIds: ReportBuilderObj.ProductIds
                    },
                    success: function (reportPath) {
                        if (reportPath == "") {
                            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.NoReportData).val(), 'pop_error_ul');
                        }
                        else {
                            var path = reportPath;
                            window.open(path, 'ReportBuilder');
                        }

                        $jq("#data_loader").hide();
                    },
                    error: function (data) {
                        $jqComm("#data_loader").hide();
                    }
                });
            }
        }
        else {
            $jqComm("#data_loader").hide();
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.SelectField).val(), 'pop_error_ul');
        }
    }
}

ReportBuilder.prototype.PrintGrid = function () {
    $jq('#' + ReportBuilderObj.JQReportGrid).print({
    });
}

ReportBuilder.prototype.ConfirmUserDataOnly = function () {
    if ($jq('#' + ReportBuilderObj.chkUserData).is(':checked')) {
        var confirm = $jq('#hdnresConfirmUserDataOnly').val();
        $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).html(confirm);
        $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: "Yes",
                "id": "btnYes",
                click: function () {
                    $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).dialog('close');
                    return false;
                },
            }, {
                text: "No",
                "id": "btnNo",
                click: function () {
                    $jq('#' + ReportBuilderObj.chkUserData).prop("checked", false);
                    $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).dialog('close');
                    $jq("#chkUserData").parent(".ez-checkbox").removeClass("ez-checked");
                    return false;
                },
            }]
        });
        $jq('#' + ReportBuilderObj.ReportBuilderErrorBox).dialog('open');
        return false;
    }
}

ReportBuilder.prototype.ValidateReportCriteria = function () {
    if (ReportBuilderObj.CustomerIds.length == 0 && !ReportBuilderObj.UserDataOnly) {
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.RequiredCustomer).val(), 'pop_error_ul');
        return false;
    }

    if (ReportBuilderObj.CareHomeIds.length != 0 && ReportBuilderObj.PatientNameId != "") {
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.RequiredCarehomeOrPatientId).val(), 'pop_error_ul');
        return false;
    }

    if (ReportBuilderObj.PrescriptionColumns.length > 0 && ReportBuilderObj.OrderColumns.length > 0) {
        CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.PrescriptionOrOrder).val(), 'pop_error_ul');
        return false;
    }

    var deliveryDateFrom;
    var deliveryDateTo;
    if (ReportBuilderObj.DeliveryDateFrom != '') {
        deliveryDateFrom = CommonScriptObj.isValidDate(ReportBuilderObj.DeliveryDateFrom);
        if (deliveryDateFrom != false) {
            deliveryDateFrom = Date.parse(deliveryDateFrom)
        }
        else
        {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidDeliveryDateFrom).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.DeliveryDateTo != '') {
        deliveryDateTo = CommonScriptObj.isValidDate(ReportBuilderObj.DeliveryDateTo);
        if (deliveryDateTo != false) {
            deliveryDateTo = Date.parse(deliveryDateTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidDeliveryDateTo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.DeliveryDateFrom != '' && ReportBuilderObj.DeliveryDateTo != '') {
        if (deliveryDateFrom > deliveryDateTo) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.PatientDeliveryDateCheck).val(), 'pop_error_ul');
            return false;
        }
    }

    var orderDateFrom;
    var orderDateTo;
    if (ReportBuilderObj.OrderDateFrom != '') {
        orderDateFrom = CommonScriptObj.isValidDate(ReportBuilderObj.OrderDateFrom);
        if (orderDateFrom != false) {
            orderDateFrom = Date.parse(orderDateFrom)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidOrderDateFrom).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.OrderDateTo != '') {
        orderDateTo = CommonScriptObj.isValidDate(ReportBuilderObj.OrderDateTo);
        if (orderDateTo != false) {
            orderDateTo = Date.parse(orderDateTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidOrderDateTo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.OrderDateFrom != '' && ReportBuilderObj.OrderDateTo != '') {
        if (orderDateFrom > orderDateTo) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.OrderDateCheck).val(), 'pop_error_ul');
            return false;
        }
    }

    var invoiceDateFrom;
    var invoiceDateTo;
    if (ReportBuilderObj.InvoiceDateFrom != '') {
        invoiceDateFrom = CommonScriptObj.isValidDate(ReportBuilderObj.InvoiceDateFrom);
        if (invoiceDateFrom != false) {
            invoiceDateFrom = Date.parse(invoiceDateFrom)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidInvoiceDateFrom).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.InvoiceDateTo != '') {
        invoiceDateTo = CommonScriptObj.isValidDate(ReportBuilderObj.InvoiceDateTo);
        if (invoiceDateTo != false) {
            invoiceDateTo = Date.parse(invoiceDateTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidInvoiceDateTo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.InvoiceDateFrom != '' && ReportBuilderObj.InvoiceDateTo != '') {
        if (invoiceDateFrom > invoiceDateTo) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvoiceDateCheck).val(), 'pop_error_ul');
            return false;
        }
    }


    var patientCreateDateFrom;
    var patientCreateDateTo;
    if (ReportBuilderObj.PatientCreateDateFrom != '') {
        patientCreateDateFrom = CommonScriptObj.isValidDate(ReportBuilderObj.PatientCreateDateFrom);
        if (patientCreateDateFrom != false) {
            patientCreateDateFrom = Date.parse(patientCreateDateFrom)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidPatientCreateDateFrom).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.PatientCreateDateTo != '') {
        patientCreateDateTo = CommonScriptObj.isValidDate(ReportBuilderObj.PatientCreateDateTo);
        if (patientCreateDateTo != false) {
            patientCreateDateTo = Date.parse(patientCreateDateTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidPatientCreateDateTo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.PatientCreateDateFrom != '' && ReportBuilderObj.PatientCreateDateTo != '') {
        if (patientCreateDateFrom > patientCreateDateTo) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.PatientCreateDateCheck).val(), 'pop_error_ul');
            return false;
        }
    }


    var nextAsessmentDateFrom;
    var nextAsessmentDateTo;
    if (ReportBuilderObj.NextAssessmentDateFrom != '') {
        nextAsessmentDateFrom = CommonScriptObj.isValidDate(ReportBuilderObj.NextAssessmentDateFrom);
        if (nextAsessmentDateFrom != false) {
            nextAsessmentDateFrom = Date.parse(nextAsessmentDateFrom)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidNextAssessmentDateFrom).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.NextAssessmentDateTo != '') {
        nextAsessmentDateTo = CommonScriptObj.isValidDate(ReportBuilderObj.NextAssessmentDateTo);
        if (nextAsessmentDateTo != false) {
            nextAsessmentDateTo = Date.parse(nextAsessmentDateTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidNextAssessmentDateTo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.NextAssessmentDateFrom != '' && ReportBuilderObj.NextAssessmentDateTo != '') {
        if (nextAsessmentDateFrom > nextAsessmentDateTo) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.NextAssessmentDateCheck).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.InvoiceNo != '') {
        if ($jq.isNumeric(ReportBuilderObj.InvoiceNo) == false){
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidInvoiceNo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.InvoiceNoTo != '') {
        if ($jq.isNumeric(ReportBuilderObj.InvoiceNoTo) == false) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvalidInvoiceNo).val(), 'pop_error_ul');
            return false;
        }
    }

    if (ReportBuilderObj.InvoiceNo != '' && ReportBuilderObj.InvoiceNoTo != '') {
        if (parseInt(ReportBuilderObj.InvoiceNoTo) < parseInt(ReportBuilderObj.InvoiceNo)) {
            CommonScriptObj.DisplayErrorMessage(ReportBuilderObj.ReportBuilderErrorBox, $jq("#" + ReportBuilderObj.InvoiceNoCheck).val(), 'pop_error_ul');
            return false;
        }
    }

    return true;
}

ReportBuilder.prototype.GetProducts = function (customerIds) {
    var url = $jq('#' + ReportBuilderObj.GetProductsAction).val();
    $jq.ajax({
        dataType: 'json',
        type: "GET",
        url: url,
        async: false,
        cache: false,
        data: {
            "customerIds": customerIds
        },
        success: function (data) {
            $jq("#" + ReportBuilderObj.ReportProductsDropDown).multiselect('enable')
            $.each(data, function (key, option) {
                $("#" + ReportBuilderObj.ReportProductsDropDown).append($jq('<option></option>').val(option.Value).html(option.Text));
            });

            $jq("#" + ReportBuilderObj.ReportProductsDropDown).multiselect('refresh');
            $jqComm('.ui-corner-all input[type="checkbox"]').ezMark();
        },
        error: function (result) {
        }
    });
}

ReportBuilder.prototype.GetCareHome = function (customerIds) {
    var url = $jq('#' + ReportBuilderObj.GetCareHomeAction).val();
    $jq.ajax({
        dataType: 'json',
        type: "GET",
        url: url,
        async: false,
        cache: false,
        data: {
            "customerIds": customerIds
        },
        success: function (data) {
            $jq("#" + ReportBuilderObj.FieldCareHomeReport).multiselect('enable')
            $.each(data, function (key, option) {
                $("#" + ReportBuilderObj.FieldCareHomeReport).append($jq('<option></option>').val(option.Value).html(option.Text));
            });

            $jq("#" + ReportBuilderObj.FieldCareHomeReport).multiselect('refresh');
            $jqComm('.ui-corner-all input[type="checkbox"]').ezMark();
            if (customerIds == "0")
                $jq("#" +ReportBuilderObj.FieldCareHomeReport).multiselect('disable');
        },
        error: function (result) {
        }
    });
}

ReportBuilder.prototype.UncheckUserDataOnly = function () {
    $jq('#' + ReportBuilderObj.chkUserData).prop("checked", false);
    $jq("#chkUserData").parent(".ez-checkbox").removeClass("ez-checked");
}






