﻿$jqGrid = jQuery.noConflict();

var AuditLogObj = new AuditLog();

//Id's from PatientInfo/CareHome Maintenence screen
AuditLog.prototype.btnPatientAuditLog = "btnPatientAuditLog";
AuditLog.prototype.btnCareHomeAuditLog = "btnCareHomeAuditLog";

AuditLog.prototype.hdnPatientId = "PatientId";
AuditLog.prototype.hdnCustomerId = "ddlCustomer";
AuditLog.prototype.hdnCareHomeId = "Assignedcarehome";

AuditLog.prototype.hdnPatientFirstName = "FirstName";
AuditLog.prototype.hdnPatientLastName = "LastName";
AuditLog.prototype.patientScreenCareHomeName = "CareHomeName";
AuditLog.prototype.hdnPatientSAPCustomerNumber = "hdnPatientSAPCustomerNumber";
AuditLog.prototype.hdnPatientCareHomeSAPId = "hdnPatientCareHomeSAPId";
AuditLog.prototype.SAPPatientNumber = "SAPPatientNumber";
AuditLog.prototype.hdnCareHomeSAPCustomerNumber = "hdnSAPCustomerNumber";



//AuditLog screen Id's

AuditLog.prototype.txtCustomerSearch = "txtCustomerSearch";
AuditLog.prototype.txtcareHomeSearch = "txtcareHomeSearch";
AuditLog.prototype.txtPatientSearch = "txtPatientSearch";
AuditLog.prototype.txtUsernameSearch = "txtUsernameSearch";
AuditLog.prototype.dateAuditLogFromDate = "dateAuditLogFromDate";
AuditLog.prototype.dateAuditLogToDate = "dateAuditLogToDate";

AuditLog.prototype.txtCustomerDisplay = "txtCustomerDisplay";
AuditLog.prototype.txtcareHomeDisplay = "txtcareHomeDisplay";
AuditLog.prototype.txtPatientDisplay = "txtPatientDisplay";
AuditLog.prototype.txtUsernameDisplay = "txtUsernameDisplay";
AuditLog.prototype.lblUserName = "lblUserName";

AuditLog.prototype.gridAuditLog = "jqAuditLog";

AuditLog.prototype.btnViewAuditLog = "btnViewAuditLog";
AuditLog.prototype.btnDownloadAuditLog = "btnDownloadAuditLog";
AuditLog.prototype.btnExportToExcel = "btnExportToExcel";
AuditLog.prototype.btnAuditLogReset = "btnAuditLogReset";
AuditLog.prototype.btnSaveRemarks = "btnSaveRemarks";
AuditLog.prototype.btnExitAuditPopup = "btnExitAuditPopup";

AuditLog.prototype.hdnCurrentUserType = "hdnCurrentUserType";
AuditLog.prototype.hdnCurrentUserRole = "hdnCurrentUserRole";
AuditLog.prototype.SCAuserType = "SCAuserType";
AuditLog.prototype.ExtuserType = "ExtuserType";

AuditLog.prototype.AdminUserRole = "AdminUserRole";
AuditLog.prototype.KeyMemberUserRole = "KeyMemberUserRole";
AuditLog.prototype.hdnPatientAuditLogType = "hdnPatientAuditLogType";
AuditLog.prototype.hdnCustomAuditLogType = "hdnCustomAuditLogType";
AuditLog.prototype.hdnCareHomeAuditLogType = "hdnCareHomeAuditLogType";
AuditLog.prototype.hdnCustomerAuditLogType = "hdnCustomerAuditLogType";
AuditLog.prototype.hdnUserAuditLogType = "hdnUserAuditLogType";

AuditLog.prototype.hdnPatientTable = "hdnPatientTable";
AuditLog.prototype.hdnCustomerTable = "hdnCustomerTable";
AuditLog.prototype.hdnCareHomeTable = "hdnCareHomeTable";
AuditLog.prototype.hdnUsersTable = "hdnUsersTable";


AuditLog.prototype.divAuditLogValidation = "divAuditLogValidation";
AuditLog.prototype.divAuditLogPartial = "divAuditLogPartial";
//divAuditLogValidation
AuditLog.prototype.divValidationMsgText = "divValidationMsgText";
AuditLog.prototype.classBtnDisable = "btn_disable";
AuditLog.prototype.classErrorMessage = "validation_msg";


AuditLog.prototype.hdnErrorSelectCustomer = "hdnErrorSelectCustomer";
AuditLog.prototype.hdnErrorSelectCustomerORUser = "hdnErrorSelectCustomerORUser";
AuditLog.prototype.hdnErrorSelectPatient = "hdnErrorSelectPatient";
AuditLog.prototype.hdnErrorDate = "hdnErrorDate";
AuditLog.prototype.hdnErrorNoAuditLogs = "hdnErrorNoAuditLogs";
AuditLog.prototype.hdnErrorCannotExport = "hdnErrorCannotExport";
AuditLog.prototype.hdnErrorInvalidFromDate = "hdnErrorInvalidFromDate";
AuditLog.prototype.hdnErrorInvalidToDate = "hdnErrorInvalidToDate";
AuditLog.prototype.hdnMsgresGrdNoChanges = "hdnMsgresGrdNoChanges";
AuditLog.prototype.hdnMsgRemarksSaved = "hdnMsgRemarksSaved";
AuditLog.prototype.hdnErrorMsgRemarkSave = "hdnErrorMsgRemarkSave";


var customerId = 0;
var patientId = 0;
var careHomeId = 0;
var auditSearchUserId = '';
var fromDate = '';
var toDate = '';
var isExport = false;
var IsAuditLogPopupClosed = true;


var patientAuditLogType = $jqGrid('#' + AuditLogObj.hdnPatientAuditLogType + '').val();
var careHomeAuditLogType = $jqGrid('#' + AuditLogObj.hdnCareHomeAuditLogType + '').val();
var customerAuditLogType = $jqGrid('#' + AuditLogObj.hdnCustomerAuditLogType + '').val();;
var customAuditLogType = $jqGrid('#' + AuditLogObj.hdnCustomAuditLogType + '').val();
var userAuditLogType = $jqGrid('#' + AuditLogObj.hdnUserAuditLogType + '').val();


var patientTable = $jqGrid('#' + AuditLogObj.hdnPatientTable + '').val();
var customerTable = $jqGrid('#' + AuditLogObj.hdnCustomerTable + '').val();
var careHomeTable = $jqGrid('#' + AuditLogObj.hdnCareHomeTable + '').val();
var usersTable = $jqGrid('#' + AuditLogObj.hdnUsersTable + '').val();


var SCAuserType = $jqGrid('#' + AuditLogObj.SCAuserType + '').val();
var ExtuserType = $jqGrid('#' + AuditLogObj.ExtuserType + '').val();
var AdminUserRole = $jqGrid('#' + AuditLogObj.AdminUserRole + '').val();
var KeyMemberUserRole = $jqGrid('#' + AuditLogObj.KeyMemberUserRole + '').val();
var loggedInUserType = $jqGrid('#' + AuditLogObj.hdnCurrentUserType + '').val();
var loggedInUserRole = $jqGrid('#' + AuditLogObj.hdnCurrentUserRole + '').val();

var _auditLogType = customAuditLogType;
var KeyUser = "Key User";

function AuditLog() { }

$jqGrid(document).ready(function () {
         
    if (loggedInUserType != SCAuserType)
    {     
        AuditLogObj.HideUserSelection();
    }

    //if (loggedInUserRole != AdminUserRole && loggedInUserRole != KeyMemberUserRole && loggedInUserRole != KeyUser)
    //{        
    //    AuditLogObj.HideAuditLogButtons();
    //}
    $jqGrid('#' + AuditLogObj.btnExitAuditPopup).hide();

    // Load the Date Picker
    $jqGrid('.AuditLogCalenderImg').datepicker({
        yearRange: "-100:+100",
        dateFormat: 'dd/mm/yy',        
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        maxDate: '0',
        showOn: 'both',
        firstDay: 1,
    });

    $jqGrid('#' + AuditLogObj.txtPatientSearch + '').on('keyup', function (e) {
    

        $jqGrid('#' + AuditLogObj.btnAuditLogReset + '').removeClass(AuditLogObj.classBtnDisable);
        var charCode = (e.which) ? e.which : e.keyCode;        
        if (charCode == 8) {            
            AuditLogObj.ClearPatientName();
        }

        if ($jqGrid('#' + AuditLogObj.txtPatientSearch + '').val().length != 0) {
            $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').addClass(AuditLogObj.classBtnDisable);
            $jqGrid("#" + AuditLogObj.txtcareHomeSearch).attr("disabled", "disabled");
        }
        else {
            $jqGrid('#' + AuditLogObj.txtPatientDisplay + '').val('');
            $jqGrid("#" + AuditLogObj.txtcareHomeSearch + '').removeAttr("disabled");
            $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').removeClass(AuditLogObj.classBtnDisable);
        }
       
        if ($jqGrid('#' + AuditLogObj.txtCustomerSearch + '').val() == '' || customerId == 0) {            
            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorSelectCustomer + '').val());
            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
            AuditLogObj.ClearPatient();            
            return false;

        }

    });

    $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').on('keyup', function (e) {        
        $jqGrid('#' + AuditLogObj.btnAuditLogReset + '').removeClass(AuditLogObj.classBtnDisable);

        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8) {
            AuditLogObj.ClearCareHomeName();
        }

        if ($jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').val() != '') {
            $jqGrid('#' + AuditLogObj.txtPatientSearch + '').addClass(AuditLogObj.classBtnDisable);
            $jqGrid("#" + AuditLogObj.txtPatientSearch).attr("disabled", "disabled");
        }
        else {
            $jqGrid('#' + AuditLogObj.txtcareHomeDisplay + '').val('');
            $jqGrid("#" + AuditLogObj.txtPatientSearch + '').removeAttr("disabled");
            $jqGrid('#' + AuditLogObj.txtPatientSearch + '').removeClass(AuditLogObj.classBtnDisable);
        }       
        if ($jqGrid('#' + AuditLogObj.txtCustomerSearch + '').val() == '' || customerId == 0) {
            
            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorSelectCustomer + '').val());
            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
            AuditLogObj.ClearCareHome();
            $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').focus();
            return false;
        }

    });

    $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').on('keyup', function (e) {        
        if ($jqGrid('#' + AuditLogObj.txtUsernameSearch + '').val() != '')
        {
            AuditLogObj.ClearUser();
        }
        $jqGrid('#' + AuditLogObj.btnAuditLogReset + '').removeClass(AuditLogObj.classBtnDisable);
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8 || $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').val() == '') {            
            // AuditLogObj.ClearCustomer();
            AuditLogObj.ClearCustomerName();
            AuditLogObj.ClearCareHome();
            AuditLogObj.ClearPatient();
            AuditLogObj.ClearUser();
        }
    });

    $jqGrid('#' + AuditLogObj.txtUsernameSearch + '').on('keyup', function (e) {
        $jqGrid('#' + AuditLogObj.btnAuditLogReset + '').removeClass(AuditLogObj.classBtnDisable);
        AuditLogObj.ClearCareHome();
        AuditLogObj.ClearPatient();
        AuditLogObj.ClearCustomer();
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8) {
            AuditLogObj.ClearUser();
        }

    });
        // Check for ViewOnly 
    //linkId: - Menu Link Id, element: - Main Div of the Form
    CommonScriptObj.DisableFormControls("frm_ViewAuditLog");

    
});//End Of Document.ready()

$jqGrid('#' + AuditLogObj.btnPatientAuditLog + '').click(function () {    
     patientId = $jqGrid('#'+ AuditLogObj.hdnPatientId +'').text();     
    _auditLogType = patientAuditLogType;
    AuditLogObj.OpenAuditLogPopup();
    $jqComm(".audit_log").css('display', 'none');
    AuditLogObj.HideUserSelection();
    $jqGrid('#' + AuditLogObj.btnExitAuditPopup).show();
    isExport = false;
    AuditLogObj.GetAuditLog();

    // Handle the View only and Full Rights
    if (PatientInfoObj.AuditLogAccess == "0") {
        // Set AuditLog View Only
        $jqGrid(".actionbtn").addClass("btn_disable");
    }
});

$jqGrid('#' + AuditLogObj.btnCareHomeAuditLog + '').click(function () {    
    careHomeId = $jqGrid('#hdnCareHomeId').val();

    _auditLogType = careHomeAuditLogType;
    AuditLogObj.OpenAuditLogPopup();
    $jqComm(".audit_log").css('display', 'none');
    AuditLogObj.HideUserSelection();
    $jqGrid('#' + AuditLogObj.btnExitAuditPopup).show();
    isExport = false;
    AuditLogObj.GetAuditLog();

    // Handle the View only and Full Rights
    if (CareHomeObj.AuditLogAccess == "0") {
        // Set AuditLog View Only
        $jqGrid(".actionbtn").addClass("btn_disable");
    }
});

AuditLog.prototype.GetPatientOrCareHomeAuditLog = function (Id) {     
    $jqGrid("#jqAuditLog").jqGrid('setGridParam', { postData: { Id: Id, fromDate: $jqGrid('#'+ AuditLogObj.dateAuditLogFromDate +'').val(), toDate: $jqGrid('#'+ AuditLogObj.dateAuditLogToDate +'').val(), auditLogType: _auditLogType }, datatype: 'json' }).trigger('reloadGrid');
    AuditLogObj.setAuditLogDataForPatientCareHome();
}

AuditLog.prototype.OpenAuditLogPopup = function () {
    $jqGrid('#'+AuditLogObj.divAuditLogPartial).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: '80%',
        height:'540px',
        resizable: true,
        title: 'Audit log',
        overflow: 'auto'
        });

            $jqGrid('#'+AuditLogObj.divAuditLogPartial).dialog('open');    
            $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').removeAttr('placeholder');
            $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').removeAttr('placeholder');
            $jqGrid('#' + AuditLogObj.txtPatientSearch + '').removeAttr('placeholder');
            IsAuditLogPopupClosed = false;
            window.IsAuditLogFromMenu = true;
}

$jqGrid('#' + AuditLogObj.divAuditLogPartial).on('dialogbeforeclose', function (event) {
    $(this).siblings(".ui-dialog-titlebar").find("button").blur();
    if (!IsAuditLogPopupClosed) {
        event.preventDefault();
        AuditLogObj.CloseAuditLogPopup();
    }
})

AuditLog.prototype.setAuditLogDataForPatientCareHome = function () {
    
            var careHomeId = '';
            var careHomeName = '';

            var customerId = '';
            var customerName = '';

            if (_auditLogType == patientAuditLogType) {        
                       
                careHomeId = $jqGrid('#' + AuditLogObj.hdnPatientCareHomeSAPId + '').val();                
                careHomeName = $jqGrid('#' + AuditLogObj.patientScreenCareHomeName + '').val();
                customerId     =    $jqGrid('#' + AuditLogObj.hdnCustomerId + '').val();
                customerName = $jqGrid('#' + AuditLogObj.hdnCustomerId + ' option:selected').text();

                customerName = customerName.substring(0, customerName.indexOf("("));
                $jqGrid('#' + AuditLogObj.txtPatientSearch + '').val($jqGrid('#' + AuditLogObj.SAPPatientNumber + '').text());
                $jqGrid('#' + AuditLogObj.txtPatientDisplay + '').val(($jqGrid('#' + AuditLogObj.hdnPatientFirstName + '').val()) + ' ' + ($jqGrid('#' + AuditLogObj.hdnPatientLastName + '').val()));
                $jqGrid('#' +AuditLogObj.txtCustomerSearch + '').val($jqGrid('#' + AuditLogObj.hdnPatientSAPCustomerNumber + '').val());
                $jqGrid('#' +AuditLogObj.txtCustomerDisplay + '').val(customerName);

        }
        else if(_auditLogType == careHomeAuditLogType) {                
                
                customerId = $jqGrid('#' + AuditLogObj.hdnCareHomeSAPCustomerNumber + '').val();            
                customerName = $('#' + AuditLogObj.hdnCustomerId + ' option:selected').text();
                careHomeId = $jqGrid('#lblCareHomeId').text();//This is SAP CareHome Number from CareHome maintenance screen            
                careHomeName = $jqGrid('#txtCareHomeName').val();
                customerName = customerName.substring(0, customerName.indexOf("("));

                $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').val(customerId);
                $jqGrid('#' + AuditLogObj.txtCustomerDisplay + '').val(customerName);
        }

            $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').addClass(AuditLogObj.classBtnDisable);
            $jqGrid("#" + AuditLogObj.txtCustomerSearch).attr("disabled", "disabled");            

            $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').addClass(AuditLogObj.classBtnDisable);
            $jqGrid("#" + AuditLogObj.txtcareHomeSearch).attr("disabled", "disabled");
            $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').val(careHomeId);
            $jqGrid('#' + AuditLogObj.txtcareHomeDisplay + '').val(careHomeName);

            $jqGrid('#' + AuditLogObj.txtPatientSearch + '').addClass(AuditLogObj.classBtnDisable);
            $jqGrid("#" + AuditLogObj.txtPatientSearch).attr("disabled", "disabled");

            $jqGrid('#' + AuditLogObj.txtUsernameSearch + '').addClass(AuditLogObj.classBtnDisable);            
      
}

$jqGrid('#' + AuditLogObj.btnExportToExcel + '').click(function () {    
    if (AuditLogPageClick(this)) {
        isExport = true;
        var data = AuditLogObj.GetAuditLog();
    }
});

$jqGrid('#' + AuditLogObj.txtCustomerSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: customerTable, customerId: 0 }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {                
                return {                    
                    label: el.SearchAutoCompleteName,                    
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    customerId: el.Id
                };
            }));
        });
    },
    select: function (event, ui) {        
        customerId = ui.item.customerId;        
        $jqGrid('#' + AuditLogObj.txtCustomerDisplay + '').val(ui.item.label.substring(0, ui.item.label.indexOf("(")));        
        $jqGrid(".ui-autocomplete").hide();        
    }
});

$jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {   
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: careHomeTable, customerId: customerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });            
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,                    
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    careHomeId : el.Id
                };                
            }));
        });
    },
    select: function (event, ui) {        
        careHomeId = ui.item.careHomeId;        
        $jqGrid('#' + AuditLogObj.txtcareHomeDisplay + '').val(ui.item.label.substring(0, ui.item.label.indexOf("(")));        
        $jqGrid(".ui-autocomplete").hide();
    }
});


$jqGrid('#' + AuditLogObj.txtPatientSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: patientTable, customerId: customerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,                    
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    patientId: el.Id
                };
            }));
        });
    },
    select: function (event, ui) {        
        patientId = ui.item.patientId;        
        $jqGrid('#' + AuditLogObj.txtPatientDisplay + '').val(ui.item.label.substring(0, ui.item.label.indexOf("(")));
        $jqGrid(".ui-autocomplete").hide();
    }
});


$jqGrid('#' + AuditLogObj.txtUsernameSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: usersTable, customerId: customerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId,
                    userId: el.UserId
                };
            }));
        });
    },
    select: function (event, ui) {
        auditSearchUserId = ui.item.userId;
        $jqGrid('#' + AuditLogObj.txtUsernameDisplay + '').val(ui.item.label);
        $jqGrid(".ui-autocomplete").hide();
    }
});

AuditLog.prototype.ClearPatient = function () {
    $jqGrid('#' + AuditLogObj.txtPatientSearch + '').val('');
    $jqGrid('#' + AuditLogObj.txtPatientDisplay + '').val('');
    patientId = 0;
}

AuditLog.prototype.ClearPatientName = function () {    
    $jqGrid('#' + AuditLogObj.txtPatientDisplay + '').val('');
    patientId = 0;
}

AuditLog.prototype.ClearCareHome = function () {    
    $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').val('');
    $jqGrid('#' + AuditLogObj.txtcareHomeDisplay + '').val('');
    careHomeId = 0;
}

AuditLog.prototype.ClearCareHomeName = function () {    
    $jqGrid('#' + AuditLogObj.txtcareHomeDisplay + '').val('');
    careHomeId = 0;
}

AuditLog.prototype.ClearCustomer = function () {
    $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').val('');
    $jqGrid('#' + AuditLogObj.txtCustomerDisplay + '').val('');
    customerId = 0;
}

AuditLog.prototype.ClearCustomerName = function () {  
    $jqGrid('#' + AuditLogObj.txtCustomerDisplay + '').val('');
    customerId = 0;
}

AuditLog.prototype.ClearUser = function () {
    $jqGrid('#' + AuditLogObj.txtUsernameSearch + '').val('');
    $jqGrid('#' + AuditLogObj.txtUsernameDisplay + '').val('');
    auditSearchUserId = '';
}

AuditLog.prototype.HideUserSelection = function () {

    $jqGrid('#' + AuditLogObj.txtUsernameSearch + '').hide();
    $jqGrid('#' + AuditLogObj.txtUsernameDisplay + '').hide();
    $jqGrid('#' + AuditLogObj.lblUserName + '').hide();    
}

$jqGrid('#' + AuditLogObj.btnViewAuditLog + '').click(function () {
    if (AuditLogPageClick(this)) {
        isExport = false;
        AuditLogObj.GetAuditLog();
    }
});

AuditLog.prototype.GetAuditLog = function () {
    
    var audittype = customAuditLogType;
    if (_auditLogType == customAuditLogType)
    {
        $jqGrid('#'+ AuditLogObj.btnAuditLogReset +'').removeClass(AuditLogObj.classBtnDisable);
        if (auditSearchUserId != '')
        {
            audittype = userAuditLogType;
        }
        else if(patientId == 0 && careHomeId == 0)
        {
            audittype = customerAuditLogType;
        }
        else if ( patientId == 0)
        {
            audittype = careHomeAuditLogType;
        }
        else {
            audittype = patientAuditLogType;
        }
    }
    else {
        audittype = _auditLogType;
    }    
    fromDate = $jqGrid('#' + AuditLogObj.dateAuditLogFromDate + '').val();
    toDate = $jqGrid('#' + AuditLogObj.dateAuditLogToDate + '').val();

    if (fromDate != "")
    {        
        if (!CommonScriptObj.isValidDate(fromDate)) {            
            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorInvalidFromDate + '').val());
            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
            return false;
        }
        else {
               fromDate = CommonScriptObj.isValidDate(fromDate);            
        }
    }

    if (toDate != "")
    {        
        if (!CommonScriptObj.isValidDate(toDate)) {            
            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorInvalidToDate + '').val());
            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
            return false;
        }

    else{
            toDate = CommonScriptObj.isValidDate(toDate);
    }
}
    var isSearchCriteriaValid = AuditLogObj.ValidateSearchCriteria();
    if (isSearchCriteriaValid) {
        if (!isExport)
        {
            $jqGrid("#jqAuditLog").jqGrid('setGridParam', { postData: { customerId: customerId, careHomeId: careHomeId, patientId: patientId, fromDate: fromDate, toDate: toDate, auditLogType: audittype, isExport: isExport, auditSearchUserId: auditSearchUserId }, datatype: 'json' }).trigger('reloadGrid');
        if (_auditLogType != customAuditLogType) {
            AuditLogObj.setAuditLogDataForPatientCareHome();
        }
        }
        else {
            $.ajax({
                url: '/AuditLog/GetAuditLog',
                    type: 'GET',
                    contentType: "application/json",
                    dataType: 'json',
                    cache:false,
                    async: false,
                    data: { page: 1, rows: 100, customerId: customerId, careHomeId: careHomeId, patientId: patientId, fromDate: fromDate, toDate: toDate, auditLogType: audittype, isExport: isExport, auditSearchUserId: auditSearchUserId },
                    success: function (data) {
                        if (data == "-1") {                            
                            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorCannotExport + '').val());
                            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
                        }
                        else {
                            window.open(data, '_AuditLog');
                        }
                    },
                    error: function (xhr) {
                        alert('Ajax status code : ' + xhr.Status + ' and ' + xhr.statusText);
                    }
                });
}
    }
    else {        
    }
}

AuditLog.prototype.ValidateSearchCriteria = function () {

    var isSearchCriteriaValid = true;
    if (_auditLogType ==  customAuditLogType) {
        if (loggedInUserType == SCAuserType) {
            if (customerId == 0 && auditSearchUserId == '') {               
                $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorSelectCustomerORUser + '').val());
                $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
                isSearchCriteriaValid = false;
                return isSearchCriteriaValid;
            }
        }
        else if (customerId == 0) {            
            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorSelectCustomer + '').val());
            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();

            isSearchCriteriaValid = false;
            return isSearchCriteriaValid;
        }
        
        if (loggedInUserType != SCAuserType && patientId == 0 && careHomeId == 0) {            
            $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorSelectPatient + '').val());
            $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
            isSearchCriteriaValid = false;
            return isSearchCriteriaValid;
        }       
    }    
    var fromDate = $jqGrid('#'+ AuditLogObj.dateAuditLogFromDate +'').val();
    var objFromDate = $jqGrid.datepicker.parseDate("dd/mm/yy", fromDate);
    var toDate = $jqGrid('#' + AuditLogObj.dateAuditLogToDate + '').val();
    var objToDate = $jqGrid.datepicker.parseDate("dd/mm/yy", toDate);      
    if (objToDate != null &&  objFromDate > objToDate)
    {        
        $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorDate + '').val());
        $jqGrid('#'+ AuditLogObj.divAuditLogValidation ).show();
        isSearchCriteriaValid = false;
        return isSearchCriteriaValid;
    }
    return isSearchCriteriaValid;
}

AuditLog.prototype.HideAuditLogButtons = function () {

    $jqGrid('#' + AuditLogObj.btnPatientAuditLog + '').hide();
    $jqGrid('#' + AuditLogObj.btnCareHomeAuditLog + '').hide();
}

AuditLog.prototype.Reset = function () {

    if (_auditLogType != patientAuditLogType && _auditLogType !=  careHomeAuditLogType)
    {        
        $jqGrid('#' + AuditLogObj.txtCustomerSearch + '').val('');
        $jqGrid('#' + AuditLogObj.txtCustomerDisplay + '').val('');
        customerId = 0;

        $jqGrid('#' + AuditLogObj.txtcareHomeSearch + '').val('');
        $jqGrid('#' + AuditLogObj.txtcareHomeDisplay + '').val('');
        careHomeId = 0;

        $jqGrid('#' + AuditLogObj.txtPatientSearch + '').val('');
        $jqGrid('#' + AuditLogObj.txtPatientDisplay + '').val('');
        patientId = 0;
    }        
    $jqGrid('#' + AuditLogObj.txtUsernameSearch + '').val('');
    $jqGrid('#' + AuditLogObj.txtUsernameDisplay + '').val('');
    auditSearchUserId = '';    

    $jqGrid('#' + AuditLogObj.dateAuditLogFromDate + '').val('');
    $jqGrid('#' + AuditLogObj.dateAuditLogToDate + '').val('');
    
    $jqGrid("#" +  AuditLogObj.gridAuditLog +'').jqGrid('clearGridData');
}

$jqGrid('#' + AuditLogObj.btnAuditLogReset + '').click(function () {
    if (AuditLogPageClick(this)) {
        AuditLogObj.Reset();
    }
});

$jqGrid('#' + AuditLogObj.btnSaveRemarks).click(function () {
    $jqGrid("#" + AuditLogObj.gridAuditLog).jqGrid('editCell', 0, 0, false);
    var changedcells = $jqGrid('#' + AuditLogObj.gridAuditLog).getChangedCells('dirty');
    var auditLogRemarksList = [];

    if (changedcells == null || changedcells.length == 0) {
        $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnMsgresGrdNoChanges).val());
        $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
    }
    else {

        changedcells.forEach(function (e) {
            var row = $jqGrid('#' + AuditLogObj.gridAuditLog).jqGrid('getRowData', e.id);
            row['Remarks'] = e.Remarks;
            var auditLogObject = {
                AuditLogId: row['AuditLogId'],
                Remarks: e.Remarks
            }
            auditLogRemarksList.push(row);
        });

        $jqGrid.ajax({
            url: '/AuditLog/SaveRemarks',
            type: 'POST',
            contentType: "application/json",
            async: false,
            data: JSON.stringify(auditLogRemarksList),
            success: function (data) {
                if (data.toLowerCase() == "true") {
                    $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnMsgRemarksSaved).val());
                    $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
                }
                else {
                    $jqGrid('#' + AuditLogObj.divValidationMsgText).html($jqGrid('#' + AuditLogObj.hdnErrorMsgRemarkSave).val());
                    $jqGrid('#' + AuditLogObj.divAuditLogValidation).show();
                }
            }
        });
    }

    var gridData = $jqGrid('#' + AuditLogObj.gridAuditLog).jqGrid('getRowData');
    $jqGrid('#' + AuditLogObj.gridAuditLog).jqGrid('setGridParam', { datatype: 'local', data: gridData }).trigger('reloadGrid');

});

$jqGrid('#' + AuditLogObj.btnExitAuditPopup).click(function () {
    AuditLogObj.CloseAuditLogPopup();
});

AuditLog.prototype.CloseAuditLogPopup = function () {
    if (CheckAuditLogRemarkChanges()) {
        IsAuditLogPopupClosed = true;
        window.IsAuditLogFromMenu = false;
        $jqGrid('#' + AuditLogObj.divAuditLogPartial).dialog('close');
    }
}

function CheckAuditLogRemarkChanges(e) {
    $jqGrid('#jqAuditLog').jqGrid('editCell', 0, 0, false);
    var changedcells = $jqGrid('#jqAuditLog').getChangedCells('dirty');
    var auditLogRemarksList = [];

    if (changedcells != null && changedcells.length != 0) {
        $jqGrid("#divValidationConfirmRemarkField").dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            resizable: false,
            closeOnEscape: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralYes").val(),
                click: function () {
                    $jqGrid(this).dialog("close");
                    window.IsAuditLogFromMenu = false;
                    try {
                        e.click();
                        return;
                    }
                    catch(err) { }
                    try {
                        IsAuditLogPopupClosed = true;
                        $jqGrid('#' + AuditLogObj.divAuditLogPartial).dialog('close');
                    }
                    catch(err) { }
                },
            },
            {
                text: $jqGrid("#hdnGeneralNo").val(),
                click: function () {
                    $jqGrid(this).dialog("close");
                },
            }
            ]
        });
        $jqGrid("#divValidationConfirmRemarkField").dialog('open');
        return false;
    }
    return true;
}

function AuditLogPageClick(e) {
    var isChanged = true;
    if (window.IsAuditLogFromMenu == true) {
        isChanged = CheckAuditLogRemarkChanges(e);
    }
    window.IsAuditLogFromMenu = true;
    IsAuditLogPopupClosed = false;
    return isChanged;
}