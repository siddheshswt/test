﻿$jqGrid = jQuery.noConflict();

var SearchText = "";

$jqGrid(document).ready(function () {

    $jqGrid("#btnUserSearch").click(function () {
        LoadSearchData();
    });        

    $jqGrid("#lnkClearSearchResul").click(function () {
        window.location.reload(true);
});

    $jqGrid("#txtUserSearch").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;

        if (charCode == 13) {
            LoadSearchData();
        }

        if (isInValidSpecialCharacter(charCode))
            return false;
        else
            return true;
    });
});

function NameAsHyperLinkFormater(cellvalue, options, rowobject) {
    if (cellvalue == null) {
        cellvalue = "";
        return "";
    }
    if (cellvalue == 'Reset')
        return "<a onclick='ResetUserId(" + options['rowId'] + ")' class='searchresult_a'>" + cellvalue + "</a>"
    else
        return "<a onclick='LockUnlockUserId(" + options['rowId'] + ")' class='searchresult_a'>" + cellvalue + "</a>"
}

function ResetUserId(rowId) {
    var rowData = $jqGrid("#jqResetUnlockUsersResult").getRowData(rowId);
    $jqGrid("#lblUsername").html(rowData.UserName);
    $jqGrid("#UserId").val(rowData.UserId);

    if (rowData.IsAccountLocked != "N")
    {
        CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", $jqGrid(hdnresResetUnlockResetInvalidMsg).val(), 'pop_error_ul');
}
    else if ($jqGrid("#hdnCurrentUserId").val() == rowData.UserId)
    {
        CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", $jqGrid(hdnresResetUnlockCurrentUserIdValidation).val(), 'pop_error_ul');
    }
    else
    {
        
    
    var carehomeReport = $jqGrid("#divResetPassword").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 900,
        height: 600,
        resizable: true,
        title: "Reset Password",
        open: function () {
        },
    });

    carehomeReport.dialog('open');
    }
}

function LockUnlockUserId(rowId) {

    var rowData = $jqGrid("#jqResetUnlockUsersResult").getRowData(rowId);
    var isAccountLocked = false;
    var successmsg = "";

    if (rowData.IsAccountLocked == 'Y') {
        $jqGrid('#lblValidationMessage').text($jqGrid('#hdnresUnlockMessage').val().replace("{0}", rowData.UserName));
        successmsg = $jqGrid("#hdnresUnlockSuccessMessage").val().replace("{0}", rowData.UserName);
        isAccountLocked = true
    }
    else {
        $jqGrid('#lblValidationMessage').text($jqGrid('#hdnresLockMessage').val().replace("{0}", rowData.UserName));
        successmsg = $jqGrid("#hdnresLockSuccessMessage").val().replace("{0}", rowData.UserName);
    }

    if ($jqGrid("#hdnCurrentUserId").val() == rowData.UserId) {
        CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", $jqGrid(hdnresResetUnlockCurrentUserIdValidation).val(), 'pop_error_ul');
    }
    else {
        $jqGrid('#divConfirmBox').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralYes").val().toString(),
            "id": "hdnGeneralYes",
            click: function () {
                    $jqGrid('#divConfirmBox').dialog('close');

                    var url = $jqGrid('#hdnUnlockResetAction').val();
                    $jqGrid.ajax({
                        cache: false,
                        type: "GET",
                        url: url,
                        data: { userIdTOLockUnlock: rowData.UserId, isLocked: isAccountLocked },
                        async: false,
                        cache: false,
                        success: function (data) {
                            if (data == true) {
                                CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", successmsg, 'pop_error_ul');                                
                                LoadSearchData();
                            }
                            else {
                                CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", $jqGrid(hdnresUnlockLockErrorMessage).val(), 'pop_error_ul');
                            }
                        },
                    });

            },
        }, {
            text: $jqGrid("#hdnGeneralNo").val().toString(),
            "id": "hdnGeneralNo",
            click: function () {
                $jqGrid('#divConfirmBox').dialog('close');
            },
        }]
    });
        $jqGrid('#divConfirmBox').dialog('open');
    }
}


function SetData() {
    SearchText = $jqComm("#txtUserSearch").val();
}

function LoadSearchData() {
    SetData();
    $jqGrid("#jqResetUnlockUsersResult").jqGrid('clearGridData');
    $jqGrid("#jqResetUnlockUsersResult").jqGrid('setGridParam', {
        postData: {
            searchText: SearchText
        }, datatype: 'json'
    }).trigger('reloadGrid');
    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
}

function ResetPasswordFields() {
    $jqGrid("#NewPassword").val("");
    $jqGrid("#ConfirmPassword").val("");
}

function ResetPasswordPopMessage(event) {
    var Username = $jqGrid("#lblUsername").html();
    var UserId = $jqGrid("#UserId").val();
    var NewPassword = $jqGrid("#NewPassword").val();
    var ConfirmPassword = $jqGrid("#ConfirmPassword").val();
    var successmsg = $jqGrid("#hdnResetSuccessMessage").val().replace("{0}", Username)
    var Unsuccessmsg = $jqGrid("#hdnResetUnSuccessMessage").val().replace("{0}", Username);

    $jqGrid.ajax({
        cache: false,
        type: "POST",       
        url: "/Account/ResetPassword",
        data: { NewPassword: NewPassword, ConfirmPassword: ConfirmPassword, UserId: UserId, Username: Username },       
        async: false,
        cache: false,       
        success: function (data) {
            event.preventDefault();
            if (data == true) {
                $jqGrid('#divResetPassword').dialog('close');
                ResetPasswordFields();
                CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", successmsg, 'pop_error_ul');
            }
            else if (data == false) {
                ResetPasswordFields();
                CommonScriptObj.DisplayErrorMessage("divResetUnlockErrorMessage", Unsuccessmsg, 'pop_error_ul');
            }
        },
    });
}
