﻿$jqGrid = jQuery.noConflict();

$jqGrid(document).ready(function () {
    window.IsremovedVar = 1;
    $('#show_removed').click(function () {
        var grid = $jqGrid("#jqPatientPrescription"), ids = grid.jqGrid('getDataIDs'), i, l = ids.length;
        if ($("#show_removed").is(':checked')) {
            for (var i = 0; i < l; i++) {
                $jqGrid("#jqPatientPrescription #" + (ids[i])).show();
            }
        }
        else {
            for (var i = 0; i < l; i++) {
                if ($jqGrid("#jqPatientPrescription #" + ids[i] + "_IsRemoved").val() == 'removed') {
                    $jqGrid("#jqPatientPrescription #" + ids[i]).hide();
                }
            }
        }
    });
});