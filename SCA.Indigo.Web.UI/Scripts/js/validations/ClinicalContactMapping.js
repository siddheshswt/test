﻿$jqGrid = jQuery.noConflict();

//// Object declaration
var ClinicalContactMappingObj = new ClinicalContactMapping();

// Div used
ClinicalContactMapping.prototype.ClinicalContactMappingDialog = "divClinicalContactMapping";
ClinicalContactMapping.prototype.ClinicalRoleMappingSucessDialog = "divRoleMappingSuccess";
ClinicalContactMapping.prototype.ErrorMessage = "divClinicalContactErrorMessage";
ClinicalContactMapping.prototype.ClinicalContactRoleMappingUndoDialog = "divRoleMapplingUndo";
ClinicalContactMapping.prototype.CheckClinicalContactClass = "chkClinicalContactClass";
ClinicalContactMapping.prototype.AddEditSection = "divAddEditSection";
ClinicalContactMapping.prototype.CustomerController = "hdnCustomerController";
ClinicalContactMapping.prototype.OkCancelPopUp = "Clinicalcontact_ok_cancel_pop";
ClinicalContactMapping.prototype.ClinicalContactRoleMappingTabChanged = "divRoleMapplingTabChanged";

// url used
ClinicalContactMapping.prototype.GetClinicalContactValueMappingUrl = "hdnActionMethodGetClinicalContactValueMapping";
ClinicalContactMapping.prototype.GetClinicalContactRoleMappingUrl = "hdnActionMethodGetClinicalContactRoleMapping";
ClinicalContactMapping.prototype.SaveRoleMappingUrl = "hdnActionMethodSaveClinicalRoleMapping";

// Action Method used
ClinicalContactMapping.prototype.ActionMethodSaveClinicalContactValue = "hdnActionMethodSaveClinicalContactValue";
ClinicalContactMapping.prototype.ActionMethodGetParentValueForSelectedRoleType = "hdnActionMethodGetParentValueForSelectedRoleType";
ClinicalContactMapping.prototype.ActionMethodClinicalContactMapping = "hdnActionMethodClinicalContactMapping";
ClinicalContactMapping.prototype.ActionMethodRefreshRoleTypeDropDown = "hdnActionMethodRefreshRoleTypeDropDown";
ClinicalContactMapping.prototype.ActionRemoveClinicalContactValue = "hdnActionRemoveClinicalContactValue";

// grid used
ClinicalContactMapping.prototype.ClinicalContactValueMappingGrid = "jqClinicalContactValueMapping";
ClinicalContactMapping.prototype.ClinicalContactRoleMappingGrid = "jqClinicalContactRoleMapping";

// textbox used
ClinicalContactMapping.prototype.RoleValueTextBox = "txtRoleValue";
ClinicalContactMapping.prototype.RoleTypeTextBox = "txtRoleType";
ClinicalContactMapping.prototype.RoleMappingTextBox = "txtRoleMapping";

// Dropdown
ClinicalContactMapping.prototype.HierarchyDropDown = "ddlHierarchy";
ClinicalContactMapping.prototype.ChildDropDown = "ddlChild";
ClinicalContactMapping.prototype.ParentDropDown = "ddlParent";
ClinicalContactMapping.prototype.RoleDropdown = "ddlMedicalStaffType";
ClinicalContactMapping.prototype.ParentValueDropDown = "ddlParentValue";

// Buttons..
ClinicalContactMapping.prototype.AddEditValueButton = "btnAddClinicalContactEditValue";
ClinicalContactMapping.prototype.ClinicalRoleMappingRemoveButton = "btnClinicalContactRoleRemove";

// Hidden fields..
ClinicalContactMapping.prototype.FormTitle = "hdnClinicalContactFormTitle";
ClinicalContactMapping.prototype.RemoveErrorMessage = "hdnClinicalContactRemove";
ClinicalContactMapping.prototype.ClinicalSaveMessage = "hdnClinicalSaveMessage";
ClinicalContactMapping.prototype.ChildValueMessage = "hdnChildValueMessage";
ClinicalContactMapping.prototype.RoleBlankMessage = "hdnRoleBlankMessage";
ClinicalContactMapping.prototype.RecordAlreadyMessage = "hdnRecordAlreadyMessage";
ClinicalContactMapping.prototype.EditButtonText = "hdnEditButtonText";
ClinicalContactMapping.prototype.AddButtonText = "hdnAddButtonText";
ClinicalContactMapping.prototype.ClinicalSaveNoRecord = "hdnClinicalSaveNoRecord";
ClinicalContactMapping.prototype.NoClinicalContactValueFound = "hdnNoClinicalContactValueFound";
ClinicalContactMapping.prototype.GeneralYes = "hdnGeneralYes";
ClinicalContactMapping.prototype.GeneralNo = "hdnGeneralNo";
ClinicalContactMapping.prototype.GeneralOk = "hdnGeneralOk";
ClinicalContactMapping.prototype.SelectRoleMessage = "hdnSelectRole";
ClinicalContactMapping.prototype.RoleMappingId = "hdnRoleMappingId";
ClinicalContactMapping.prototype.ParentName = "hdnParentName";
ClinicalContactMapping.prototype.ChildParentExistMessage = "hdnChildParentExist";
ClinicalContactMapping.prototype.MandetoryRoleMappingMessage = "hdnMandetoryRoleMappingMessage";
ClinicalContactMapping.prototype.NoRecordFoundMessage = "hdnNoRecordFound";
ClinicalContactMapping.prototype.UndoMessage = "hdnUndoMessage";
ClinicalContactMapping.prototype.IsTrackDirty = false;
ClinicalContactMapping.prototype.SelectParentMessage = "hdnSelectParentMessage";
ClinicalContactMapping.prototype.SelectParentLabel = "hdnSelectParentLabel";
ClinicalContactMapping.prototype.SelectRoleType = "hdnSelectRoleType";
ClinicalContactMapping.prototype.ParentValueCheck = "hdnParentValueCheck";
ClinicalContactMapping.prototype.ChildExist = "hdnChildExist";
ClinicalContactMapping.prototype.PreviousDropDownValue = "hdnPreviousDropDownValue";
ClinicalContactMapping.prototype.RemoveClinicalContactValue = "hdnRemoveClinicalContactValue";

//Hierarchy Hidden Fields
ClinicalContactMapping.prototype.Hierarchy1Code = "hdnHierarchy1Code";
ClinicalContactMapping.prototype.Hierarchy2Code = "hdnHierarchy2Code";
ClinicalContactMapping.prototype.Hierarchy3Code = "hdnHierarchy3Code";
ClinicalContactMapping.prototype.Hierarchy4Code = "hdnHierarchy4Code";
ClinicalContactMapping.prototype.InvalidHirarchyLinkageMessage = "hdnInvalidHirarchyLinkageMessage";
ClinicalContactMapping.prototype.InvalidChildParentLinkageMessage = "hdnInvalidChildParentLinkageMessage";

ClinicalContactMapping.prototype.RemoveContactValueLinkageWithChild = "hdnRemoveContactValueLinkageWithChild";
//// Form declaration
function ClinicalContactMapping() { }

// Show clinical contact pop up
ClinicalContactMapping.prototype.ShowClinicalContactMapping = function () {
    ClinicalContactMappingObj.IsTrackDirty = false;
    ClinicalContactMappingObj.CancelClinicalContact();
    ClinicalContactMappingObj.UndoClinicalContactValueMapping();

    var url = '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.ActionMethodClinicalContactMapping).val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var popupTitle = $jqGrid("#" + ClinicalContactMappingObj.FormTitle) != null ? $jqGrid("#" + ClinicalContactMappingObj.FormTitle).val() : "";


    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactMappingDialog).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        draggable: false,
        title: popupTitle,
    });
    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactMappingDialog).dialog('open');
    $jqGrid(".disable_div").remove();
    $jqGrid.ajax({
        url: url,
        type: 'GET',
        cache: false,
        data: {
            customerId: customerId
        },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {

        }
    });
    return false;
}

// Show Hide Remove Option on the Grid Cell Select
ClinicalContactMapping.prototype.DisplayRemoveOption = function (rowId) {
    var rowData = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).getRowData(rowId);
    if (rowData.ClinicalContactRoleMappingId == "0") {
        // Display the Remove Option
        $("#" + ClinicalContactMappingObj.ClinicalRoleMappingRemoveButton).show();
    }
    else {
        // Hide the Remove Option
        $("#" + ClinicalContactMappingObj.ClinicalRoleMappingRemoveButton).hide();
    }
    return false;
}

// Remove selected Role Mapping
ClinicalContactMapping.prototype.RemoveRoleMapping = function () {
    var rowId = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('getGridParam', 'selrow');
    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('delRowData', rowId);
    return false;
}

// if click on OK - pop up, this will retrieve data on selected dropdown value.
ClinicalContactMapping.prototype.OKClicked = function () {
    ClinicalContactMappingObj.IsTrackDirty = false;
    ClinicalContactMappingObj.GetClinicalContactValueMappingOnChange();
    $("#" + ClinicalContactMappingObj.OkCancelPopUp).hide();
}

// if click on Cancel - pop up, this will stay on the same page.
ClinicalContactMapping.prototype.CancelClicked = function () {
    var text = ClinicalContactMappingObj.GetDropdownValue(ClinicalContactMappingObj.RoleDropdown, $("#" + ClinicalContactMappingObj.PreviousDropDownValue).val());
    ClinicalContactMappingObj.SetDropdownValue(ClinicalContactMappingObj.RoleDropdown, text);
    $("#" + ClinicalContactMappingObj.OkCancelPopUp).hide();
}

// Undo Clinical Contact Mapping data
ClinicalContactMapping.prototype.UndoClinicalContactValueMapping = function () {
    $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown).val($jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option").eq(0).val());
    ClinicalContactMappingObj.IsTrackDirty = false;
    ClinicalContactMappingObj.GetClinicalContactValueMappingOnChange();
    return false;
}

//Save clinical contact value
ClinicalContactMapping.prototype.SaveClinicalContactValueMapping = function () {
    var gridRows = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getRowData');
    var ids = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getDataIDs');
    var gridLength = gridRows.length;
    if (gridLength == 0) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.ClinicalSaveNoRecord).val(), 'pop_error_ul');
        return false;
    }

    //// Check any record gets updated or not.    
    if (!ClinicalContactMappingObj.IsTrackDirty) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.NoRecordFoundMessage).val(), 'pop_error_ul');
        return false;
    }

    var contactValuesList = [];
    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = ids[i];
        var isChecked = $jqGrid("#" + rowId + "_Remove").val();
        if (isChecked == undefined) {
            isChecked = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getCell', rowId, "_Remove");
        }

        var objValueList = {
            ClinicalContactRoleMappingId: row["ClinicalContactRoleMappingId"],
            ClinicalContactValueMappingId: row["ClinicalContactValueMappingId"],
            ChildListId: row["ChildListId"],
            ChildValue: row["ChildValue"],
            ParentValue: row["ParentValue"],
            ParentValueId: row["ParentValueId"],
            IsRemoved: isChecked
        }

        contactValuesList.push(objValueList);
    }

    $jqGrid.ajax({
        url: '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.ActionMethodSaveClinicalContactValue).val(),
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        cache: false,
        data: JSON.stringify({ objClinicalContactValues: contactValuesList }),
        async: false,
        success: function (retData) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.ClinicalSaveMessage).val(), 'msgSuccess');
            ClinicalContactMappingObj.IsTrackDirty = false;
            ClinicalContactMappingObj.GetClinicalContactValueMappingOnChange();
            return false;
        } // end succes function
    });
}

// Add clinical contact value in the grid..
ClinicalContactMapping.prototype.AddClinicalContactValueInGrid = function () {
    if ($jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val() == "") {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.ChildValueMessage).val(), 'pop_error_ul');
        return false;
    }

    if ($jqGrid("#" + ClinicalContactMappingObj.RoleTypeTextBox).val() == "") {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.RoleBlankMessage).val(), 'pop_error_ul');
        return false;
    }

    var ddl = document.getElementById(ClinicalContactMappingObj.ParentValueDropDown);
    if (ddl.options.length > 1 && $.trim($jqGrid("#" + ClinicalContactMappingObj.ParentValueDropDown + " option:selected").text()) == $.trim($jqGrid("#" + ClinicalContactMappingObj.SelectParentLabel).val())) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.SelectParentMessage).val(), 'pop_error_ul');
        return false;
    }

    var parentValue = "";
    var parentValueId = 0;
    if ($jqGrid("#" + ClinicalContactMappingObj.ParentValueDropDown + " option:selected").val() != 0) {
        parentValue = $.trim($jqGrid("#" + ClinicalContactMappingObj.ParentValueDropDown + " option:selected").text());
        parentValueId = $jqGrid("#" + ClinicalContactMappingObj.ParentValueDropDown + " option:selected").val();
    }

    var gridRows = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getRowData');
    var gridLength = gridRows.length;

    var isMappingValueExist = $(".childValueMapping").filter(function (index) { return $.trim($(this).text().toLowerCase()) == $.trim($jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val().toLowerCase()); }).length;

    ClinicalContactMappingObj.IsTrackDirty = true;
    var isEditmode = $jqGrid("#" + ClinicalContactMappingObj.AddEditValueButton).val() == $jqGrid("#" + ClinicalContactMappingObj.EditButtonText).val();

    if (isEditmode) {

        var selectedRowId = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getGridParam', 'selrow');
        if (isMappingValueExist > 0) {
            var existingValueRowId = 0;
            var existingValueElement = $(".childValueMapping").filter(function (index) { return $.trim($(this).text().toLowerCase()) == $.trim($jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val().toLowerCase()); });
            if (existingValueElement.length > 0) {
                existingValueRowId = $jqGrid(existingValueElement).parent().attr("id");
            }
            if (existingValueRowId != selectedRowId) {
                $(".disable_pop").show();
                $(".ui-dialog-titlebar-close").addClass('btn_disable');
                CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.RecordAlreadyMessage).val(), 'pop_error_ul');
                return false;
            }
        }

        // Check for the Current edit Item
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('setCell', selectedRowId, 'ChildValue', $jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val());
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('setCell', selectedRowId, 'ParentValue', parentValue);
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('setCell', selectedRowId, 'ParentValueId', parentValueId);
    }
    else {
        if (isMappingValueExist > 0) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.RecordAlreadyMessage).val(), 'pop_error_ul');
            return false;
        }

        var datarow = {
            Remove: 0,
            ClinicalContactRoleMappingId: $jqGrid("#" + ClinicalContactMappingObj.RoleMappingId).val(),
            ClinicalContactValueMappingId: 0,
            ChildListId: $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").val(),
            ChildValue: $jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val(),
            ParentValue: parentValue,
            HierarchyName: $jqGrid("#" + ClinicalContactMappingObj.RoleMappingTextBox).val(),
            ParentListName: $jqGrid("#" + ClinicalContactMappingObj.ParentName).val(),
            ParentValueId: parentValueId
        };

        var gridrowdata = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getRowData');
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('addRowData', gridrowdata.length + 1, datarow, "first");
    }

    $jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val('');
    $jqGrid("#" + ClinicalContactMappingObj.ParentValueDropDown).val($("#" + ClinicalContactMappingObj.ParentValueDropDown + " option").eq(0).val());
    return false;
}

// Cancel enter values in the Add child section
ClinicalContactMapping.prototype.CancelClinicalContact = function () {
    $jqGrid("#" + ClinicalContactMappingObj.AddEditSection)[0].style.display = "none";
    $jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val('');
    $jqGrid("#" + ClinicalContactMappingObj.AddEditValueButton).val($jqGrid("#" + ClinicalContactMappingObj.AddButtonText).val());
}

// Show Clinical Contact Value Mapping master data
ClinicalContactMapping.prototype.ShowClinicalContactEditValue = function (flag) {
    // Blank the Hidden field value
    $jqGrid("#" + ClinicalContactMappingObj.ParentName).val('');
    $jqGrid("#" + ClinicalContactMappingObj.RoleMappingId).val('');

    var url = '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.ActionMethodGetParentValueForSelectedRoleType).val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var childId = $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").val() == "" ? 0 : $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").val();
    var selectedRole = $.trim($jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").text());

    if (childId == 0) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.SelectRoleType).val(), 'pop_error_ul');
        return false;
    }

    $jqGrid.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        async: false,
        cache: false,
        data: {
            customerId: customerId, childId: childId
        },
        cache: false,
        success: function (retData) {
            if (retData.Count == 0) {
                $(".disable_pop").show();
                $(".ui-dialog-titlebar-close").addClass('btn_disable');
                var alertMesssage = $jqGrid("#" + ClinicalContactMappingObj.ParentValueCheck).val().replace('{0}', retData.ParentListName).replace('{1}', selectedRole);
                CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, alertMesssage, 'pop_error_ul');
                return false;
            }

            $jqGrid("#" + ClinicalContactMappingObj.AddEditSection)[0].style.display = "";
            $jqGrid("#" + ClinicalContactMappingObj.RoleTypeTextBox).val(selectedRole);
            $jqGrid("#" + ClinicalContactMappingObj.RoleMappingTextBox).val(retData.HierarchyName);
            $jqGrid("#" + ClinicalContactMappingObj.RoleMappingId).val(retData.ClinicalContactRoleMappingId);
            $jqGrid("#" + ClinicalContactMappingObj.ParentName).val(retData.ParentListName);

            if (retData.Count == 1) {
                // Check for Top Parent
                if (retData.ParentLinkItems[0].Value == 0) {
                    // This is the Top Parent.
                    // No need to fill any value in Parent Drop down                    
                    return false;
                }
            }
            // Load dropdown
            ClinicalContactMappingObj.LoadDropDownData(true, retData.ParentLinkItems);
            if (flag) {
                var selectedRowId = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getGridParam', 'selrow');
                var isChildValue = $jqGrid("#" + selectedRowId + "_ChildValue").val();
                if (isChildValue == undefined) {
                    isChildValue = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getCell', selectedRowId, "ChildValue");
                }

                var isParentValue = $jqGrid("#" + selectedRowId + "_ParentValue").val();
                if (isParentValue == undefined) {
                    isParentValue = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getCell', selectedRowId, "ParentValue");
                }

                $jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val(isChildValue);
                $jqGrid("#" + ClinicalContactMappingObj.AddEditValueButton).val($jqGrid("#" + ClinicalContactMappingObj.EditButtonText).val());
                ClinicalContactMappingObj.SetDropdownValue(ClinicalContactMappingObj.ParentValueDropDown, $.trim(isParentValue));
            }
        } // end succes function
    });
}

// Load dropdown data in the Add child section
ClinicalContactMapping.prototype.LoadDropDownData = function (flag, list) {
    var items = [];
    if (flag) {
        // Fill the Parent Drop down with retData.ClinicalContactValueMappingList
        items.push("<option value=" + 0 + ">" + $jqGrid("#" + ClinicalContactMappingObj.SelectParentLabel).val() + "</option>");
        for (var i = 0; i < list.length; i++) {
            items.push("<option value=" + list[i].Value + ">" + list[i].Text + "</option>");
        }
    }
    else {
        items.push("<option value=" + 0 + ">" + $jqGrid("#" + ClinicalContactMappingObj.SelectParentLabel).val() + "</option>");
        $jqGrid("#" + ClinicalContactMappingObj.RoleValueTextBox).val('');
        $jqGrid("#" + ClinicalContactMappingObj.AddEditSection)[0].style.display = "none";
    }
    $jqGrid("#" + ClinicalContactMappingObj.ParentValueDropDown).html(items.join(' '));
}

// get dropdown value by passing dropdown id and value
ClinicalContactMapping.prototype.GetDropdownValue = function (ddlid, value) {
    var ddl = document.getElementById(ddlid);
    var val;
    for (var i = 0; i < ddl.options.length; i++) {
        if (ddl.options[i].value == value) {
            val = ddl.options[i].text;
        }
    }
    return $.trim(val);
}

// Set dropedown value
ClinicalContactMapping.prototype.SetDropdownValue = function (ddlid, text) {
    var ddl = document.getElementById(ddlid);
    var selectedflag = false;
    for (var i = 0; i < ddl.options.length; i++) {
        if (ddl.options[i].text.toLowerCase() == text.toLowerCase()) {
            ddl.options[i].setAttribute('selected', 'selected');
            selectedflag = true;
            ddl.selectedIndex = i;
        }
        else {
            ddl.options[i].removeAttribute('selected');
        }
    }
    if (!selectedflag) {
        ddl.selectedIndex = 0;
    }
}

// Remove rows
ClinicalContactMapping.prototype.RemoveValueRows = function (cellvalue, options, rowobject) {
    return "<input type='checkbox' class='" + ClinicalContactMappingObj.CheckClinicalContactClass + "' id=" + options['rowId'] + "_" + options.colModel["name"] + " onchange=ClinicalContactMappingObj.RemoveRecordOnSelect(this)>";
}

// Select or deselect all checkbox
ClinicalContactMapping.prototype.RemoveRecordOnSelect = function (checkbox) {
    var gridRows = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getRowData');
    var ids = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getDataIDs');
    var rowId = (checkbox.id).split("_")[0];

    var isChildValueId = $jqGrid("#" + rowId + "_ChildValueId").val();
    if (isChildValueId == undefined) {
        isChildValueId = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getCell', rowId, "ChildValueId");
    }

    var isChildValue = $jqGrid("#" + rowId + "_ChildValue").val();
    if (isChildValue == undefined) {
        isChildValue = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('getCell', rowId, "ChildValue");
    }

    // check if Parent value is already associate with Child Value.
    var isChlildExist = ClinicalContactMappingObj.CheckForAssociationWithCustomer(isChildValueId, true);
    if (isChlildExist) {
        var messageToDisplay = $jqGrid("#" + ClinicalContactMappingObj.RemoveContactValueLinkageWithChild).val().replace('{0}', isChildValue);
        $jqGrid(checkbox).prop("checked", false);
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, messageToDisplay, 'pop_error_ul');
        return false;
    }

    // check if child value is already used by customer or not..
    var count = ClinicalContactMappingObj.CheckForAssociationWithCustomer(isChildValueId, false);
    if (count) {
        var messageToDisplay = $jqGrid("#" + ClinicalContactMappingObj.RemoveClinicalContactValue).val().replace('{0}', isChildValue);
        $jqGrid(checkbox).prop("checked", false);
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, messageToDisplay, 'pop_error_ul');
        return false;
    }

    if (checkbox.checked) {
        $jqGrid("#" + checkbox.id).val('true');
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.RemoveErrorMessage).val(), 'pop_error_ul');
    }
    else {
        $jqGrid("#" + checkbox.id).val('false');
    }
    return false;
}

// Refresh Dropdown on Role Type Dropdown change
ClinicalContactMapping.prototype.RefreshRoleTypeDropDown = function () {
    var url = '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.ActionMethodRefreshRoleTypeDropDown).val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();

    $jqGrid.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {
            customerId: customerId
        },
        async: false,
        cache: false,
        success: function (retData) {
            var items = [];
            if (retData.MedicalStaffMasterList.length > 0) {
                $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('clearGridData');
                // Fill the Role type Drop down with retData.MedicalStaffMasterList
                items.push("<option value=" + 0 + ">" + $jqGrid("#" + ClinicalContactMappingObj.SelectRoleMessage).val() + "</option>");
                for (var i = 0; i < retData.MedicalStaffMasterList.length; i++) {
                    items.push("<option value=" + retData.MedicalStaffMasterList[i].Value + ">" + retData.MedicalStaffMasterList[i].Text + "</option>");
                }
                $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown).html(items.join(' '));
            }
            ClinicalContactMappingObj.CancelClinicalContact();
            // end outer else condition
        } // end succes function
    });
}

// retrieve data on selected value
ClinicalContactMapping.prototype.GetClinicalContactValueMappingOnChange = function () {
    // Load dropdown
    ClinicalContactMappingObj.LoadDropDownData(false, '');

    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var selectedVal = $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").val() == "" ? 0 : $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").val();
    $jqGrid("#" + ClinicalContactMappingObj.RoleTypeTextBox).val('');
    $jqGrid("#" + ClinicalContactMappingObj.RoleMappingTextBox).val('');

    if ($.trim($jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").text()) == $.trim($jqGrid("#" + ClinicalContactMappingObj.SelectRoleMessage).val())) {
        selectedVal = 0;
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('clearGridData');
        return false;
    }

    // Load Grid data
    $jqGrid.ajax({
        url: '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.GetClinicalContactValueMappingUrl).val(),
        type: 'GET',
        dataType: 'json',
        data: {
            customerId: customerId, roleType: selectedVal
        },
        async: false,
        cache: false,
        success: function (retData) {
            if (retData.Count == 0) {
                $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('clearGridData');
            }
            else {
                $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('clearGridData');
                $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactValueMappingGrid).jqGrid('setGridParam',
                        {
                            datatype: 'local', data: retData.ClinicalContactValueMappingList
                        }).trigger('reloadGrid');

                ClinicalContactMappingObj.CancelClinicalContact();

                // Register click event for checkbox..
                $jqGrid(".isSelected").click(function (id) {
                    ClinicalContactMappingObj.IsTrackDirty = true;
                });
            }// end outer else condition
        } // end succes function
    });
}

// Get Clinical Value mapping grid data
ClinicalContactMapping.prototype.GetClinicalContactValueMapping = function () {
    var ddl = $("#" + ClinicalContactMappingObj.RoleDropdown);
    var previous = ddl.data('previous');
    ddl.data('previous', ddl.val());

    if (previous == '') {
        previous = 0;
    }

    $("#" + ClinicalContactMappingObj.PreviousDropDownValue).val(previous);
    if (ClinicalContactMappingObj.IsTrackDirty) {
        $("#" + ClinicalContactMappingObj.OkCancelPopUp).show();
    }
    else {
        ClinicalContactMappingObj.GetClinicalContactValueMappingOnChange();
    }
    return false;
}

// Get parent data list
ClinicalContactMapping.prototype.GetParentDataList = function () {
    var gridRowsRoleMapping = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('getRowData');
    var gridLength = gridRowsRoleMapping.length;

    var RoleMappingList = [];
    for (var i = 0; i < gridRowsRoleMapping.length; i++) {
        var row = gridRowsRoleMapping[i];
        if (row["ChildListId"] == $jqGrid("#" + ClinicalContactMappingObj.RoleDropdown + " option:selected").val()) {
            var objRoleMappingList = {
                Hierarchy: row["Hierarchy"],
                ClinicalContactRoleMappingId: row["ClinicalContactRoleMappingId"],
                HierarchyListId: row["HierarchyListId"],
                ChildListId: row["ChildListId"],
                ParentListId: row["ParentListId"],
                IsParentExist: CheckParentExist(row["ParentListId"])
            }
            RoleMappingList.push(objRoleMappingList);
        }
    }

    return RoleMappingList;
}

// Get Clinical Role mapping grid data
ClinicalContactMapping.prototype.GetClinicalContactRoleMapping = function () {

    if ($("#" + CustomerInformationObj.ShowCustomerViewAlert).val() == "True") {
        return false;
    }

    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    $jqGrid.ajax({
        url: '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.GetClinicalContactRoleMappingUrl).val(),
        type: 'GET',
        dataType: 'json',
        contentType: "application/json",
        data: {
            customerId: customerId
        },
        async: false,
        cache: false,
        success: function (retData) {
            $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('clearGridData');
            $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('setGridParam',
                {
                    datatype: 'local', data: retData.ClinicalContactRoleMappingList
                }).trigger('reloadGrid');
        } // end succes function
    });
    return false;
}

// This will check / uncheck checkbox values.
ClinicalContactMapping.prototype.ClearGridData = function (flag, cssId) {
    if (flag) {
        $jqGrid("." + cssId).prop("checked", true);
    } else {
        $jqGrid("." + cssId).prop("checked", false);
    }
}

//This will reset clinical role page
ClinicalContactMapping.prototype.UndoRoleMapping = function () {
    ClinicalContactMappingObj.ClearRoleMappingDropdown();
    var ChangesDone = $(".hierarchy").filter(function (index) { return $(this).text() }).parent().find(".IsNewRow").filter(function (index) { return $(this).text() == "true" }).length;
    if (ChangesDone > 0) {
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).html($jqGrid("#" + ClinicalContactMappingObj.UndoMessage).val());

        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#" + ClinicalContactMappingObj.GeneralYes).val().toString(),
                "id": ClinicalContactMappingObj.GeneralYes,
                click: function () {
                    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog('close');
                    ClinicalContactMappingObj.GetClinicalContactRoleMapping();
                    return false;
                },
            }, {
                text: $jqGrid("#" + ClinicalContactMappingObj.GeneralNo).val().toString(),
                "id": ClinicalContactMappingObj.GeneralNo,
                click: function () {
                    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog('close');
                },
            }]
        });
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog('open');
    }
    return false;
}

//This will add row in grid after validating
ClinicalContactMapping.prototype.AddRoleMapping = function () {

    if ($jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown + " option:selected").val() == ""
        || $jqGrid("#" + ClinicalContactMappingObj.ChildDropDown + " option:selected").val() == ""
        || $jqGrid("#" + ClinicalContactMappingObj.ParentDropDown + " option:selected").val() == "") {
        $jqGrid(".disable_pop").show();
        $jqGrid(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.MandetoryRoleMappingMessage).val(), 'pop_error_ul');
        return false;
    }

    var selectedChild = $jqGrid("#" + ClinicalContactMappingObj.ChildDropDown + " option:selected").val();
    var selectedChildText = $jqGrid("#" + ClinicalContactMappingObj.ChildDropDown + " option:selected").text();

    var selectedparent = $jqGrid("#" + ClinicalContactMappingObj.ParentDropDown + " option:selected").val();
    var selectedparentText = $jqGrid("#" + ClinicalContactMappingObj.ParentDropDown + " option:selected").text();

    var selectedHierarchy = $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown + " option:selected").val();
    var selectedHierarchyText = $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown + " option:selected").text();

    var clildexist = $(".child").filter(function (index) { return $(this).text() == selectedChild }).length;
    if (clildexist > 0) {
        $jqGrid(".disable_pop").show();
        $jqGrid(".ui-dialog-titlebar-close").addClass('btn_disable');
        var childExistErrorMessage = $jqGrid("#" + ClinicalContactMappingObj.ChildExist).val().replace("{0}", "'" + selectedChildText + "'");
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, childExistErrorMessage, 'pop_error_ul');
        return false;
    }

    if (selectedChild == selectedparent) {
        $jqGrid(".disable_pop").show();
        $jqGrid(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.ChildParentExistMessage).val(), 'pop_error_ul');
        return false;
    }

    // Check if Selected child and parent relation is already exist as vice versa
    if ($(".child").filter(function (index) { return $(this).text() == selectedparent }).parent().find(".parent").filter(function (index) { return $(this).text() == selectedChild }).length > 0) {
        // Display Message : Parent and Child already exist
        $jqGrid(".disable_pop").show();
        $jqGrid(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.InvalidChildParentLinkageMessage).val(), 'pop_error_ul');
        return false;
    }

    var gridRowsRoleMapping = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('getRowData');
    var dataRow = { ClinicalContactRoleMappingId: 0, HierarchyListId: selectedHierarchy, Hierarchy: selectedHierarchyText, ChildListId: selectedChild, Child: selectedChildText, ParentListId: selectedparent, Parent: selectedparentText, IsNewRow: true };
    var gridLength = gridRowsRoleMapping.length;
    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('addRowData', gridLength + 1, dataRow, "first");

    // Clear the Dropdown selection
    ClinicalContactMappingObj.ClearRoleMappingDropdown();

    return false;
}

// Clear role mapping dropdown.
ClinicalContactMapping.prototype.ClearRoleMappingDropdown = function () {
    $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown).val($("#" + ClinicalContactMappingObj.HierarchyDropDown + 'option').eq(0).val());
    $jqGrid("#" + ClinicalContactMappingObj.ChildDropDown).val($("#" + ClinicalContactMappingObj.ChildDropDown + 'option').eq(0).val());
    $jqGrid("#" + ClinicalContactMappingObj.ParentDropDown).val($("#" + ClinicalContactMappingObj.ParentDropDown + 'option').eq(0).val());
    return false;
}

//This will save all records in grid
ClinicalContactMapping.prototype.SaveRoleMapping = function () {
    // Validate the Hierarchy Linkage for all Hierarchy.
    // Check for Hierarchy1 
    var hierarchy1Code = $jqGrid("#" + ClinicalContactMappingObj.Hierarchy1Code).val();
    var hierarchy2Code = $jqGrid("#" + ClinicalContactMappingObj.Hierarchy2Code).val();
    var hierarchy3Code = $jqGrid("#" + ClinicalContactMappingObj.Hierarchy3Code).val();
    var hierarchy4Code = $jqGrid("#" + ClinicalContactMappingObj.Hierarchy4Code).val();

    var hierarchy1Status = ClinicalContactMappingObj.ValidateByHierarchy(hierarchy1Code);
    var hierarchy2Status = ClinicalContactMappingObj.ValidateByHierarchy(hierarchy2Code);
    var hierarchy3Status = ClinicalContactMappingObj.ValidateByHierarchy(hierarchy3Code);
    var hierarchy4Status = ClinicalContactMappingObj.ValidateByHierarchy(hierarchy4Code);
    var message = $jqGrid("#" + ClinicalContactMappingObj.InvalidHirarchyLinkageMessage).val();

    var isLinkageFail = false;
    if (!hierarchy1Status) {
        message = message.replace('{0}', $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown).find("option[value=" + hierarchy1Code + "]").text());
        isLinkageFail = true;
    }

    if (!hierarchy2Status) {
        message = message.replace('{0}', $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown).find("option[value=" + hierarchy2Code + "]").text());
        isLinkageFail = true;
    }
    if (!hierarchy3Status) {
        message = message.replace('{0}', $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown).find("option[value=" + hierarchy3Code + "]").text());
        isLinkageFail = true;
    }
    if (!hierarchy4Status) {
        message = message.replace('{0}', $jqGrid("#" + ClinicalContactMappingObj.HierarchyDropDown).find("option[value=" + hierarchy4Code + "]").text());
        isLinkageFail = true;
    }
    if (isLinkageFail) {
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, message, 'pop_error_ul');
        return false;
    }

    var gridRowsRoleMapping = $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingGrid).jqGrid('getRowData');
    var gridLength = gridRowsRoleMapping.length;
    if (gridLength == 0) {
        CommonScriptObj.DisplayErrorMessage(ClinicalContactMappingObj.ErrorMessage, $jqGrid("#" + ClinicalContactMappingObj.NoRecordFoundMessage).val(), 'pop_error_ul');
        return false;
    }

    var RoleMappingList = [];
    for (var i = 0; i < gridRowsRoleMapping.length; i++) {
        var row = gridRowsRoleMapping[i];
        var objRoleMappingList = {
            ClinicalContactRoleMappingId: row["ClinicalContactRoleMappingId"],
            CustomerId: $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val(),
            HierarchyListId: row["HierarchyListId"],
            ChildListId: row["ChildListId"],
            ParentListId: row["ParentListId"],
            IsParentExist: ClinicalContactMappingObj.CheckParentExist(row["ParentListId"])
        }
        RoleMappingList.push(objRoleMappingList);
    }
    $jqGrid.ajax({
        url: '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/' + $("#" + ClinicalContactMappingObj.SaveRoleMappingUrl).val(),
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        cache: false,
        data: JSON.stringify({
            objClinicalRoleMapping: RoleMappingList
        }),
        async: false,
        success: function (retData) {
            $jqGrid("#" + ClinicalContactMappingObj.ClinicalRoleMappingSucessDialog).dialog({
                autoOpen: false,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#" + ClinicalContactMappingObj.GeneralOk).val().toString(),
                    "id": ClinicalContactMappingObj.GeneralOk,
                    click: function () {
                        $jqGrid("#" + ClinicalContactMappingObj.ClinicalRoleMappingSucessDialog).dialog('close');
                        ClinicalContactMappingObj.GetClinicalContactRoleMapping();
                        ClinicalContactMappingObj.RefreshRoleTypeDropDown();
                        return false;
                    },
                }]
            });
            $jqGrid("#" + ClinicalContactMappingObj.ClinicalRoleMappingSucessDialog).dialog('open');
            return false;
        } // end succes function
    });
}

//this will check where selected parent is present in grid or not
ClinicalContactMapping.prototype.CheckParentExist = function (selectedparent) {
    var clildexist = $jqGrid(".child").filter(function (index) { return $(this).text() == selectedparent }).length;
    if (clildexist > 0) {
        return true;
    }
    return false;
}

//this will check whether previous tab has some unsaved data 
ClinicalContactMapping.prototype.CheckUnsavedData = function () {
    var ChangesDone = $(".hierarchy").filter(function (index) { return $(this).text() }).parent().find(".IsNewRow").filter(function (index) { return $(this).text() == "true" }).length;
    if (ChangesDone > 0) {
        return true;
    }
    return false;
}

//this will validate linkage for hierarchy
ClinicalContactMapping.prototype.ValidateByHierarchy = function (hierarchyId) {
    var clildexist = $(".hierarchy").filter(function (index) { return $(this).text() == hierarchyId }).length;
    if (clildexist > 0) {
        var difference = [];
        var allParents = $(".hierarchy").filter(function (index) { return $(this).text() == hierarchyId }).parent().find(".parent").map(function (index) { return $(this).text(); }).get();
        var allChilds = $(".child").map(function (index) { return $(this).text(); }).get();

        jQuery.grep(allParents, function (el) {
            if (jQuery.inArray(el, allChilds) == -1) difference.push(el);
        });

        return difference.length < 2;
    }
    else {
        return true;
    }

    return false;
}

//this will fire onclick on ok button after clinicalvaluemapping tab change
ClinicalContactMapping.prototype.ClinicalValueTabOkClick = function () {
    ClinicalContactMappingObj.SaveRoleMapping();
    $jqGrid("#" + ClinicalContactMappingObj.ClinicalRoleMappingSucessDialog).dialog('close');
    ClinicalContactMappingObj.GetClinicalContactRoleMapping();
    ClinicalContactTabObj.ClinicalTabChangedOkClick();
    ClinicalTabChangedOkClick();
    $("#divRoleMapplingTabChanged").hide();
}

//this will fire onclick on Cancel button after clinicalvaluemapping tab change
ClinicalContactMapping.prototype.ClinicalValueTabCancelClick = function () {
    $("#divRoleMapplingTabChanged").hide();
    return false;
}

//Check If Clinical Contact Value Associated With Customer
ClinicalContactMapping.prototype.CheckForAssociationWithCustomer = function (childValueId, checkForChildLinkage) {
    var url = '/' + $jqGrid("#" + CustomerParameterObj.CustomerController).val() + '/' + $jqGrid("#" + ClinicalContactMappingObj.ActionRemoveClinicalContactValue).val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var count;
    $jqGrid.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {
            customerId: customerId, CustomerMedicalStaffAnalysisInfoId: childValueId, CheckForChildLinkage: checkForChildLinkage
        },
        async: false,
        cache: false,
        success: function (retData) {
            count = retData;
            // end outer else condition
        } // end succes function
    });

    return count;
}

// Close Pop up the Clinical Contact Role Mapping: Handle the Exit Button logic
ClinicalContactMapping.prototype.CloseClinicalRoleMappingScreen = function () {
    if (ClinicalContactMappingObj.CheckUnsavedData()) {

        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).html($jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingTabChanged).find("label").html());

        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#" + ClinicalContactMappingObj.GeneralYes).val().toString(),
                "id": ClinicalContactMappingObj.GeneralYes,
                click: function () {
                    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog('close');
                    ClinicalContactMappingObj.GetClinicalContactRoleMapping();
                    ClosePopup(ClinicalContactMappingObj.ClinicalContactMappingDialog);
                    return false;
                },
            }, {
                text: $jqGrid("#" + ClinicalContactMappingObj.GeneralNo).val().toString(),
                "id": ClinicalContactMappingObj.GeneralNo,
                click: function () {
                    $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog('close');
                },
            }]
        });
        $jqGrid("#" + ClinicalContactMappingObj.ClinicalContactRoleMappingUndoDialog).dialog('open');
    }
    else {
        $jqGrid('#divClinicalContactMapping').dialog('close'); // Issue No. #1571026 BUG with Customer parameter Exit button
    }
}