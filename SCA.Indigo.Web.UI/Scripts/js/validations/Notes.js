﻿$jqGrid = jQuery.noConflict();
var jsfilename = "Notes.js";
var allGriddata = $jqGrid('#jqNotes').jqGrid('getGridParam', 'data');
var isDirty = false;
var isForceClose = false;
function showPopupPatientNotes() {
    $jqGrid("#txtNote").val('');
    var NoteTypeId = '';
    if (isCopyDetails == true) {
        if ($('#hdnPatientId').val() == '') {
            NoteTypeId = $('#PatientId').text();
        }
        else {
            NoteTypeId = $('#hdnPatientId').val();
        }
    }
    else {
        NoteTypeId = $jqGrid("#PatientId").text();
    }    
    showPopupNotes(NoteTypeId);
}

//Start SaveAlert Function************************************************************//
function SaveAlert() {
    var selectedNotes = [];
    var objNote = [];
    var allGriddata = $jqGrid('#jqNotes').jqGrid('getGridParam', 'data');
    if (allGriddata.length == 0) {
        $jqGrid('#divErrorNoteForAlert').hide();
        $jqGrid('#divErrorMsgForAlert').hide();
        $jqGrid('#divErrorNote').show();
        return false;
    } else {
        $(".chkDisplayAsAlert").each(function () {
            var isChecked = $jqGrid(this).is(':checked');
            var noteId = $(this).attr('id').replace("note_", "");
            if (isChecked) {
                objNote.push({
                    "NoteId": noteId,
                    "IsDisplayAsAlert": isChecked
                });
                selectedNotes.push(noteId);
            }
            else if (isChecked == false) {
                var rowData = $.map(allGriddata, function (item) { if (item.IsDisplayAsAlert == true && item.NoteId == noteId) { return item; } });
                if (rowData.length > 0) {
                    objNote.push({
                        "NoteId": noteId,
                        "IsDisplayAsAlert": isChecked
                    });
                }
            }
        });
        if (objNote.length > 0) {
            if (selectedNotes.length >= 0 && selectedNotes.length < 4) {
                $jqGrid.ajax({
                    url: '/Common/SaveAlert',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(objNote),
                    cache: false,
                    async: false,
                    success: function (data) {
                        if (data != null && data == 1) {
                            $jqGrid('#divErrorNoteForAlert').hide();
                            $jqGrid('#divErrorMsgForAlert').hide();
                            $jqGrid('#divErrorNote').hide();
                            $jqGrid('#divSaveAlertSuccess').dialog({
                                autoOpen: false,
                                modal: true,
                                closeOnEscape: false,
                                width: 'auto',
                                resizable: false,
                                buttons: [{
                                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                                    "id": "hdnGeneralOk",
                                    click: function () {
                                        if ($jqGrid("#txtNote").val() == "") {
                                            isDirty = false;
                                            isForceClose = false;
                                        }
                                        $jqGrid('#divSaveAlertSuccess').dialog('close');
                                        LoadNotes();
                                    },
                                }]
                            });
                            $jqGrid('#divSaveAlertSuccess').dialog('open');
                        }
                    },
                    error: function (x, e) {
                        HandleAjaxError(x, e, jsfilename, "SaveAlert");
                    }
                });
            }
            else {
                $jqGrid('#divErrorNote').hide();
                $jqGrid('#divErrorMsgForAlert').hide();
                $jqGrid('#divErrorNoteForAlert').show();
                return false;
            }
        }
        else {
            $jqGrid('#divErrorNote').hide();
            $jqGrid('#divErrorNoteForAlert').hide();
            $jqGrid('#divErrorMsgForAlert').show();
            return false;
        }
    }
    return true;
}
//End SaveAlert Function************************************************************//

function showPopupNotes(NoteTypeId) {
    window.IsNotesPopupOpen = true;
    isDirty = false;
    isForceClose = false;
    $jqGrid("#txtNote").val('');
    $jqGrid("#divSelectedNotePopUp").fadeOut("200");
    var title = $jqGrid("#hdnNoteTitle").val();
    $jqGrid("#hdnNoteTypeId").val(NoteTypeId);
    var dd = $jqGrid("#divNotesPopup").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: title
    });
    dd.dialog('open');    
    $jqGrid("#jqNotes").jqGrid('clearGridData');
    LoadNotes();
    $jqGrid('#divErrorNote').hide();
    $jqGrid('#divErrorNoteForAlert').hide();
}
/*
20132	Patient Note
20133	Order Note
20134	Prescription Note
20135	CareHome Note
*/
//NoteType- Type of note. e.g. Patient Note,OrderNote,Carehome Note
function SaveNote() {
    $jqGrid("#divSelectedNotePopUp").fadeOut("200");
    var txtNote = $jqGrid("#txtNote").val();
    var NoteType = $jqGrid("#hdnNoteType").val();
    var NoteTypeId = $jqGrid("#hdnNoteTypeId").val();
    $jqGrid('#divErrorNoteForAlert').hide();
    $jqGrid('#divErrorMsgForAlert').hide();
    if (txtNote.trim() == "") {
        $jqGrid('#divErrorNote').show();
        return false;
    } else {

        var objNote = {
            "NoteType": NoteType,
            "NoteTypeId": NoteTypeId,
            "NoteText": txtNote
        };        
        $jqGrid('#divErrorNote').hide();
        $jqGrid.ajax({
            url: '/Common/AddNotes',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(objNote),
            cache: false,
            async: false,
            success: function (data) {
                isDirty = false;
                isForceClose = false;
                $jqGrid("#txtNote").val('');
                $jqGrid('#divErrorNote').hide();
                $jqGrid('#divErrorNoteForAlert').hide();
                LoadNotes();
            }
        });
    }
    return true;
}

/*
20132	Patient Note
20133	Order Note
20134	Prescription Note
20135	CareHome Note
*/
//NoteType- Type of note. e.g. Patient Note,OrderNote,Carehome Note
function LoadNotes() {    
    var NoteType = $("#hdnNoteType").val();
    var NoteTypeId = $("#hdnNoteTypeId").val();
    $jqGrid.ajax({
        url: '/Common/GetNotes',
        type: 'GET',
        cache: false,
        data: { page: 1, rows: 50, noteType: NoteType, noteTypeId: NoteTypeId},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (responseData) {            
            $jqGrid("#jqNotes").jqGrid('clearGridData');
            $jqGrid("#jqNotes").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');

            // View Only implementation: Care Home
            if ($("#hdnNoteType").val() == $jqGrid("#hdnCareHomeNoteTypeId").val()) {

                if (CareHomeObj.NotesAccess == "0") {
                    // Set Notes View Only
                    //$jqGrid("#notesMainButton").addClass("read_only_div");
                    $jqGrid("#btnAddNote").addClass("btn_disable");
                    
                }
            }

            // View Only implementation: Patient
            if ($("#hdnNoteType").val() == $jqGrid("#hdnPatientNoteTypeId").val()) {

                if (PatientInfoObj.NotesAccess == "0") {

                    // Set Notes View Only
                    //$jqGrid("#notesMainButton").addClass("read_only_div");
                    $jqGrid("#btnAddNote").addClass("btn_disable");
                }
            }
        }
    });
}

//**************************************Load JQGrid for jqAlertNotes table**************************************
function LoadJQGridAlertNotes() {
    $jqGrid("#jqAlertNotes").jqGrid({
        datatype: "local",
        colModel: [
            {
                name: 'FormattedNote', index: 'FormattedNote', editable: false, hidden: false, classes: 'pnote_class', align: 'left', width: '840'
            }
        ],
        height: '143',
        width: 'auto',
        toppager: false,
        jsonReader: {
            repeatitems: false
        },
        viewrecords: true,
        autowidth: true,
        gridComplete: function () {
            $jqGrid('#jqgh_jqAlertNotes_FormattedNote').hide();
            $jqGrid("#jqAlertNotes_FormattedNote").hide();
            $jqGrid('#divjqAlertNotesTablePager').hide();
            $jqGrid('.ui-pg-input').bind('keyup', function (e) {
                var val = $jqGrid(this).val().trim();
                ValidationGridPageInput(e, val);
            });
        },
        ondblClickRow: function (rowid, options, rowObject, object) {
            var rowData = object.currentTarget.rows[rowid];
            var title_val = $jqGrid(rowData).find('td').html();
            $jqGrid("#divSelectedAlertNotePopUp #noteAlertText").html(title_val);
            $jqGrid("#divSelectedAlertNotePopUp").fadeIn("200");
            $jqGrid("#noteAlertText").scrollTop(0);
        },
    });
}
//**************************************END********************************************************************

//******************START LoadAlertNotes function**************************************************************
function LoadAlertNotes(noteType, noteTypeId) {
    $("#divSelectedAlertNotePopUp").hide();
    if ($("#hdnSessionDoNotShow").val() == "false") {
        $jqGrid.ajax({
            url: '/Common/GetNotes',
            type: 'GET',
            cache: false,
            data: { page: 0, rows: 0, noteType: noteType, noteTypeId: noteTypeId, alertNotes: true },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (responseData) {
                if (responseData != null && responseData.length > 0) {
                    var dd = $jqGrid("#divAlertNotePopUp").dialog({
                        autoOpen: false,
                        modal: true,
                        closeOnEscape: false,
                        width: 'auto',
                        resizable: true,
                        title: $jqGrid("#hdnNotesAlertHeader").val()
                    });
                    dd.dialog('open');
                    $jqGrid("#jqAlertNotes").jqGrid('clearGridData');
                    $jqGrid("#jqAlertNotes").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
                    $jqGrid("#chkDoNotShow").prop("checked", true).parent(".ez-checkbox").addClass("ez-checked");
                    ChkDoNotShowAlert();
                }
            },
            error: function (x, e) {
                HandleAjaxError(x, e, jsfilename, "LoadAlertNotes");
            }
        });
    }
}
//******************END LoadAlertNotes function**************************************************************

//**************START:ExitNoteText*******
function ExitNoteText() {
    $jqGrid(".note_pop").fadeOut("200");
   
}
//******************END******************

//START*****************************************************************
$jqGrid(function () {
    $jqGrid("#divNotesPopup").on('dialogbeforeclose', function (event) {
        $(this).siblings(".ui-dialog-titlebar").find("button").blur();
        if (!isForceClose) {
            event.preventDefault();
            ExitWarningMessage("divNotesPopup");
        }
    });
});
//END*******************************************************************