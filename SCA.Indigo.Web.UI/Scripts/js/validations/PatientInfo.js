﻿$jqGrid = jQuery.noConflict();
var jsfilename = "PatientInfo.js";
var isSaveSuccess = 'false';
var value = "False";
var valTrue = "TRUE";
var isPatientInfoEditable = "True";
var isMedicalInfoEditable = "True";
var isPrescriptionEditable = "True";
var PreviousPatientType = "";
var previousPatientStatus = "";
var leadTime = 0;
window.isPatientInfoEditable = true;
window.isMedicalInfoEditable = true;
window.isPrescriptionEditable = true;
window.PrescriptionEditedbyNurse = false;
var HouseName = "";
var PatientNDD = "";
var ShowPPDAlertBeforeSave = false;
var MaxPPDValue = 0;
var customerCurrentStatus = $jqGrid("#hdnActiveCustomer").val();
var ReasonCode = "";
var ExistingPatientStatus = "";
var ExistingReasonCode = "";
var CarehomeData = "";
var carehomeValue = "Carehome";
var nonCarehomeValue = "NonCarehome";
var PatientInfoObj = new PatientInfoDetails();
function PatientInfoDetails() { };
PatientInfoDetails.prototype.AnalysisInfoAccess = "";
PatientInfoDetails.prototype.ClinicalContactAccess = "";
PatientInfoDetails.prototype.ContactInfoAccess = "";
PatientInfoDetails.prototype.InteractionAccess = "";
PatientInfoDetails.prototype.AttachmentAccess = "";
PatientInfoDetails.prototype.NotesAccess = "";
PatientInfoDetails.prototype.RecentOrdersAccess = "";
PatientInfoDetails.prototype.SampleOrdersAccess = "";
PatientInfoDetails.prototype.AuditLogAccess = "";
PatientInfoDetails.prototype.NoAccessMessage = "No Access";
PatientInfoDetails.prototype.BtnBackButton = "btnBackButton";
PatientInfoDetails.prototype.ExistClinicalContact = "";
PatientInfoDetails.prototype.ExistAnalysisInfo = "";
var userType = "SCA";
var isCopyDetails = false;
var isCustomerChanged = false;
window.IsDirtyRead = false;
window.countOfMandatoryAnalysisField = 0;
window.isFromCarehomeChangePopup = false;
window.isPatientTypeChanged = false;
window.existingdbndd = "";
var existingdbPatientType = "";
var RecalcuateNDDForProCare = false;
var IsForceDisplayNDD = false;
if ($jqGrid('#hdnNonCareHomePatientType').val() != "" && $jqGrid('#hdnNonCareHomePatientType').val() != undefined) {
    var NonCarehome = $jqGrid('#hdnNonCareHomePatientType').val().split(",");
}

if ($jqGrid('#hdnProcare').val() != "" && $jqGrid('#hdnProcare').val() != undefined) {
    var Procare = $jqGrid('#hdnProcare').val().split(",");
}

if ($jqGrid('#hdnSelfcare').val() != "" && $jqGrid('#hdnSelfcare').val() != undefined) {
    var Selfcare = $jqGrid('#hdnSelfcare').val().split(",");
}

$jqGrid(document).ready(function () {
    $jqGrid('#divPatientInfoDisplay input').change(function () {
        PatientHasChanged();
    });

    $jqGrid('#divPatientInfoDisplay textarea').change(function () {
        PatientHasChanged();
    });

    $jqGrid('#divPatientInfoDisplay select').change(function () {
        PatientHasChanged();
    });

    ReasonCode = $jqGrid("#ddlReasonCode").val();

    var minFrequency = 0;
    var minFrequencyDate;
    var NotRemovedCount = 0;

    HideNurseCommentsDiv();//to be shown only when the role is Nurse
    DisableNurseComments();


    $jqGrid("#btnSearchCarehome").click(function () {
        LoadCarehomeDetails();
    });

    $jqGrid("#txtSearchCarehome").keypress(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13) {
            LoadCarehomeDetails();
        }

    });

    function LoadCarehomeDetails() {
        var customer_ID = $("#ddlCustomer").val();
        var searchText = $jqGrid('#txtSearchCarehome').val();
        if (searchText == "")
            searchText = "blank";
        $jqGrid.ajax({
            url: '/Patient/SearchCarehomeDetails',
            type: 'GET',
            cache: false,
            data: { searchText: searchText, CustomerId: customer_ID },
            async: false,
            success: function (data) {
                $jqGrid("#jqCareHomeResults").jqGrid('clearGridData');
                $jqGrid("#jqCareHomeResults").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
            }
        });//end ajax call
    }

    //Adding a new Patient
    if (window.patientId == null || window.patientId == undefined || window.patientId == 0) {
        $jqGrid("#SAPPatientNumber").text('');
        $jqGrid("#DeliveryFrequency").val(0);
        MakeAddressReadOnlyExceptCounty();
        SetSameAsDeliveryCheckBox();
        $jqGrid.ajax({
            url: '/Patient/LoadDropDowns',
            type: 'GET',
            async: false,
            cache: false,
            success: function (data) {
                FillDropDown(data);
                $jqGrid("#hdnuserType").val(data.UserType);
            }
        });
    }

    $jqGrid.ajax({
        url: '/Patient/GetPatientSectionsEditAccess',
        type: 'GET',
        cache: false,
        async: false,
        success: function (responseData) {
            //Nurse role CR                 
            window.RoleName = responseData.RoleName;
            if (responseData != null || responseData != undefined) {

                if (responseData.IsPatientInfoEditable.toString().toUpperCase() == valTrue.toUpperCase()) {
                    isPatientInfoEditable = value;
                    window.isPatientInfoEditable = false;
                    $jqGrid('.patientinfo_fields .DisablePages').addClass("disable_div");
                    $jqGrid('.patientinfo_fields .disable_msg').css('display', 'block');
                }

                if (responseData.IsMedicalInfoEditable.toString().toUpperCase() == valTrue.toUpperCase()) {
                    window.isMedicalInfoEditable = false;
                    isMedicalInfoEditable = value;
                    $jqGrid('.staff_select_mode .medicalAnalysis_nformation_table .DisablePages').addClass("disable_div");
                    $jqGrid('.analysis_information .medicalAnalysis_nformation_table .DisablePages').addClass("disable_div");

                    $jqGrid('.staff_select_mode .disable_msg').css('display', 'block');
                    $jqGrid('.analysis_information .disable_msg').css('display', 'block');

                }

                if (responseData.IsPrescriptionEditable.toString().toUpperCase() == valTrue.toUpperCase()) {
                    isPrescriptionEditable = value;
                    window.isPrescriptionEditable = false;
                    $jqGrid('.prescription_nformation_table .DisablePages').addClass('disable_div');
                    $jqGrid('.prescription_nformation .disable_msg').css('display', 'block');
                    HideNurseCommentsDiv();
                    DisablePrescriptionButtons();

                }
                if (isPatientInfoEditable.toString().toUpperCase() == value.toUpperCase() &&
                     isMedicalInfoEditable.toString().toUpperCase() == value.toUpperCase() &&
                        isPrescriptionEditable.toString().toUpperCase() == value.toUpperCase()) {

                    $jqGrid('#btnDetailSave').attr('disabled', 'disabled');
                    $jqGrid('#btnDetailSave').addClass('btn_disable');
                }

                if (responseData.IsPrescriptionEditable.toString().toUpperCase() != valTrue.toUpperCase() && window.RoleName == $jqGrid('#hdnNurseRoleName').val()) {

                    ShowNurseCommentsDiv();

                }
            }
        }
    });
    /// Moved this code to document ready (if patient id is null.. i.e in case of new patient)    
    //$jqGrid("#SAPPatientNumber").text('');    
    ////$jqGrid("#DeliveryFrequency").val(0);
    $jqGrid('#CareHomeName').addClass("bg_ddd");
    $("#CareHomeName").attr('readonly', 'true');
    if (!$('#hdnIsredirectFromApproval').val() == 1 && $('#ddlCustomer').val() == "-1") {
        DisableDetails();
    }

    $jqGrid('#Country').val($('#hdnDefaultCountry').val());
    $jqGrid('#Country').attr('disabled', 'disabled');

    if ($jqGrid("#hdnuserType").val().toUpperCase() != userType.toUpperCase()) {
        $jqGrid("#spnMandatory").hide();
        $jqGrid('#RoundId').attr('disabled', 'disabled');
    }
    else if ($jqGrid("#hdIsCarehomePatient").val().toUpperCase() == valTrue.toUpperCase()) {   
            $jqGrid("#spnMandatory").hide();
            $jqGrid('#RoundId').attr('disabled', 'disabled');
    }
    else if ((jQuery.inArray($jqGrid("#ddlPatientType").val(), NonCarehome)) == -1) {
        $jqGrid("#spnMandatory").hide();
        $jqGrid('#RoundId').attr('disabled', 'disabled');
    }
    else {
        $jqGrid("#RoundId").removeAttr('disabled', 'disabled');
        $jqGrid('#RoundId').removeClass('bg_ddd');
        $jqGrid("#spnMandatory").show();
    }
    $jqGrid('#DeliveryFrequency').attr('disabled', 'disabled');
    if ($jqGrid("#hdnDetailsPage").val() != undefined && (isPrescriptionEditable.toString().toUpperCase() != value.toString().toUpperCase())) {

    }
    else {

        EnablePatientInfoButtons();
    }

    ///if from prescription authorization
    if ($jqGrid("#hdnIsredirectFromApproval").val() == 1) {
        EnablePatientInfoButtons();
        
        setTimeout(function () {
            $(".staff_select_mode").attr('style', '');
            $(".analysis_information").attr('style', '');
        }, 2000);

        $("#spanMedicalstaff").trigger("click");
        $("#spanPatientanalysis").trigger("click");
    }

    $jqGrid('#NHSId').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $jqGrid("#NHSId").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (isNumeric(charCode))
            return true;
        else
            return false;
    });

    $jqGrid('#txtNurseMobile').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $jqGrid("#txtNurseMobile").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (isNumeric(charCode))
            return true;
        else
            return false;
    });

    $jqGrid('.NoCopyPaste').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $jqGrid(".Numeric").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (isNumeric(charCode))
            return true;
        else
            return false;
    });

    $jqGrid("#AssessmentDate").blur(function (e) {
        var Assesmentdate = $jqGrid(this).val();
        var data = Assesmentdate.split("/");
        if (Assesmentdate != "" && $jqGrid.trim(data[0]).length > 0 && $jqGrid.trim(data[1]).length > 0 && $jqGrid.trim(data[2]).length > 0) {
            setNextAssesmentDate(Assesmentdate);
        }
    });

    $jqGrid(".upper").blur(function (e) {
        if ($(this).val() != "" || $(this).val() != undefined) {
            this.value = this.value.toString().toUpperCase();
        }
    });
          

    $('#ddlReasonCode').attr('disabled', 'disabled');

    if (!$('#hdnIsredirectFromApproval').val() == 1) {
        $("#PatientId").text('');
    }

    $jqGrid('.CalenderImg').datepicker({
        yearRange: "-115:+0",
        dateFormat: 'dd/mm/yy',
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'both',
        maxDate: '0',
        firstDay: 1

    });
    $jqGrid('#NextAssessmentDate').datepicker({
        yearRange: "-115:+10",
        dateFormat: 'dd/mm/yy',
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'button',
        firstDay: 1
    });
    $jqGrid('#AssessmentDate').datepicker({
        yearRange: "-115:+10",
        dateFormat: 'dd/mm/yy',
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'button',
        maxDate: '0',
        firstDay: 1,
        onSelect: function () {
            var date = $(this).val();
            setNextAssesmentDate(date);
            PatientHasChanged();
        }
    });

    GetPatientSession();
    SetCommonObjects();

    //Bind on blur function on change of delivery address    
    $jqGrid("#CareHomeName").blur(function (e) {
        CopyDeliveryAddress(false);
    });
    $jqGrid("#Address1").blur(function (e) {
        CopyDeliveryAddress(false);
    });
    $jqGrid("#Address2").blur(function (e) {
        CopyDeliveryAddress(false);
    });
    $jqGrid("#TownOrCity").blur(function (e) {
        CopyDeliveryAddress(false);
    });

    $jqGrid("#ddlCounty").on('change', function (e) {
        CopyDeliveryAddress(false);
    });

    $jqGrid("#PostCode").blur(function (e) {
        CopyDeliveryAddress(false);
    });

    $jqGrid("#btnCorrespondencePostCode").click(function (e) {
        var PostCode = $jqGrid("#CorrespondencePostCode").val();
        showPopupPostCode("DeliveryAddress", PostCode);
    });

    $jqGrid("#btnPostCode").click(function (e) {
        var PostCode = $jqGrid("#PostCode").val();
        showPopupPostCode("PatientAddress", PostCode);
    });

    $jqGrid("#btnBackButton").click(function (e) {
        if (((window.isMedicalInfoEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.staff_select_mode .medicalAnalysis_nformation_table .DisablePages').hasClass('disable_div'))) ||
       ((window.isPatientInfoEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.patientinfo_fields .DisablePages').hasClass('disable_div'))) ||
       (window.isPrescriptionEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.prescription_nformation_table .DisablePages').hasClass('disable_div'))) {
            if (window.patientId != 0 && window.patientId != undefined) {
                $jqGrid('#divBackPrompt').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                            BackToHomepage();
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divBackPrompt').dialog('open');
            }
            else {
                $jqGrid('#divBackPrompt').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                            BackToHomepage();
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divBackPrompt').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divBackPrompt').dialog('open');
            }
        }
        else {
            BackToHomepage();
            return true;
        }
    });

    //Fill careHome details if carehome ID available in query string
    FillCareHomeDetails();


    // Get Functional module View Only and Full Rights. Commented On 4th Aug to avoid repeated Call
    // GetPatientSubModuleAccess();

    GetPrescriptionNDDRelatedDetails($("#ddlCustomer").val(), $("#Assignedcarehome").val() == "" ? 0 : $("#Assignedcarehome").val(), $("#PatientId").text() == "" ? 0 : $("#PatientId").text());
});//end of ready function

// Get Functional module View Only and Full Rights 
function GetPatientSubModuleAccess() {
    // No Need to check for Add New patient 
    $jqGrid.ajax({
        url: '/Patient/GetPatientSectionsEditAccess',
        type: 'GET',
        cache: false,
        async: false,
        success: function (responseData) {
            // Set the Interaction, Contact Info, Attachment, Notes and Recent Orders Edit access as per Role.
            if (responseData.AnalysisInfoAccess.length > 0) {
                PatientInfoObj.AnalysisInfoAccess = responseData.AnalysisInfoAccess;
            }
            else {
                // Disable the Button
                $jqGrid("#btnAddPatient").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            if (responseData.ClinicalContactAccess.length > 0) {
                PatientInfoObj.ClinicalContactAccess = responseData.ClinicalContactAccess;
            }
            else {
                // Disable the Button
                $jqGrid("#btnAddPatient").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            if (responseData.ContactInfoAccess.length > 0) {
                PatientInfoObj.ContactInfoAccess = responseData.ContactInfoAccess;
            }
            else {
                // Disable the Contact Info Button
                $jqGrid("#btnContactInfo").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            //Check for Interaction
            if (responseData.InteractionAccess.length > 0) {
                PatientInfoObj.InteractionAccess = responseData.InteractionAccess;
            }
            else {
                // disable the Interaction Button
                $jqGrid("#btnInteractionsummary").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            // Chcek for Attachment
            if (responseData.AttachmentAccess.length > 0) {
                PatientInfoObj.AttachmentAccess = responseData.AttachmentAccess;
            }
            else {
                // disable the Attachment Button
                $jqGrid("#btnAttachment").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            // Chcek for Notes
            if (responseData.NotesAccess.length > 0) {
                PatientInfoObj.NotesAccess = responseData.NotesAccess;
            }
            else {
                // disable the Notes Button
                $jqGrid("#btnNotes").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            // Chcek for Recent Orders 
            if (responseData.RecentOrdersAccess.length > 0) {
                PatientInfoObj.RecentOrdersAccess = responseData.RecentOrdersAccess;
            }
            else {
                // disable the Recent Orders Button
                $jqGrid("#btnRecentOrder").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            // Chcek for Recent Orders 
            if (responseData.SampleOrderAccess.length > 0) {
                PatientInfoObj.SampleOrdersAccess = responseData.SampleOrderAccess;
            }
            else {
                // disable the Recent Orders Button
                $jqGrid("#btnSampleOrder").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

            // Chcek for AuditLog
            if (responseData.AuditLogAccess.length > 0) {
                PatientInfoObj.AuditLogAccess = responseData.AuditLogAccess;
            }
            else {
                // disable the Recent Orders Button
                $jqGrid("#btnPatientAuditLog").addClass("btn_disable").attr("title", PatientInfoObj.NoAccessMessage);
            }

        }
    });
}

function CustomerChange(customerid, bflagSearch) {
    if (customerid == "-1") {
        if ($(".spn_NHS:has(span)")) {
            $(".spn_NHS").children("span").remove();
        }
        if ($(".spn_ADP1:has(span)")) {
            $(".spn_ADP1").children("span").remove();
        }
        if ($(".spn_TelephoneNo:has(span)")) {
            $(".spn_TelephoneNo").children("span").remove();
        }
        if ($(".spn_NextAssesmentDate:has(span)")) {
            $(".spn_NextAssesmentDate").children("span").remove();
        }
        $jqGrid("#DeliveryFrequency").val(0);
        $jqGrid('#RoundId').val('');
        $jqGrid("#ddlCommunicationFormat").multiselect("uncheckAll");
        $jqGrid("#ddlMarketingPreference").multiselect("uncheckAll");
        DisableDetails();
        ClearCorrespondenceAddress();
    }
    if (isCopyDetails == false) {
    setCustomerParameter(customerid, true);
    LoadPatinetTypeDropDown(customerid);
    }

    if (customerid != "-1" && $("#hdnAssignedCustomer").val() == "") {
        EnableDetails();
    }
    else {
        if (isCopyDetails == false) {
        MakeAddressEmpty();
        disablePostcodeCarehome();
    }
    }

    // if (window.patientId == null || window.patientId == 0 || window.patientId == undefined) {
    var isDetailsPage = window.location.href.indexOf("Details") > 0 ? true : false;
    var isCopyDetailsPage = window.location.href.indexOf("CopyDetails") > 0 ? true : false;

     ToggleMedicalInfo();

    $('#spanMedicalstaff').click(function () {
        if ($("#jqMedicalStaffAnalysis tr").length == 0 || isCustomerChanged == true) {
            isCustomerChanged = false;
            LoadMedicalAnalysisandStaffInfo(false);
    }
    });
    $('#spanPatientanalysis').click(function () {
        if($("#jqPatientAnalysis tr").length == 0 || isCustomerChanged == true) {
            isCustomerChanged = false;
            LoadMedicalAnalysisandStaffInfo(true);
        } 
    });

    if (isDetailsPage || isCopyDetailsPage || ($("#PatientId").text() == null || $("#PatientId").text() == 0 || $("#PatientId").text() == undefined)) {
        //LoadMedicalAnalysisandStaffInfo();        
       
        if (isCopyDetails == false) {
        LoadPrescriptionGrid();
    }
    }
    if (isCopyDetails == false) {
    clearNDDRoundID();
}
}

function ToggleMedicalInfo()
{
    $jqGrid('.staff_select_mode.staff_select_mode_collapse.collapse').css('display', 'none');
    $jqGrid('.analysis_information.analysis_information_collapse.collapse').css('display', 'none');
}

$jqGrid("#ddlCustomer").on('change', function (e) {

    isCustomerChanged = true;
    $jqGrid(".prescription_nformation_table").removeClass('mCS_destroyed');
    //setTimeout(function(){
    //    $jqGrid(".prescription_nformation_table").mCustomScrollbar();
    //}, 1000);
    if (isCopyDetails == false) {
    $jqGrid("#jqPatientPrescription").jqGrid('clearGridData');
    }
    else if (isCopyDetails == true) {
        LoadPatinetTypeDropDown($(this).val());
    }
    var customerid = $(this).val();    
    CheckCustomerStatus(customerid);
    if (customerCurrentStatus == $("#hdnBlockedCustomer").val()) {
        CommonScriptObj.DisplayErrorMessage("divErrorMessage", $("#hdnresErrorMessageBlockedCustomer").val(), 'pop_error_ul');
        $jqGrid("#ddlCustomer").val(-1);
    }
    CustomerChange(customerid, false);
});

function FillCareHomeDetails() {

    var careHomeId = $("#hdnCareHomeId").val();
    if (careHomeId > 0) {
        $jqGrid.ajax({
            url: '/Patient/GetCareHomeById',
            type: 'GET',
            async: false,
            data: { careHomeId: careHomeId },
            cache: false,
            success: function (data) {
                $("#ddlCustomer").val(data.CustomerId);
                CustomerChange(data.CustomerId, false);
                $("#Address1").val('');
                $("#Address2").val('');
                $("#TownOrCity").val('');
                $("#PostCode").val('');
                $("#PhoneNo").val('');
                $("#EmailAddress").val('');
                $("#MobileNo").val('');
                $("#CareHomeName").val('');
                $("#PhoneNo").val(data.TelephoneNumber);
                $("#MobileNo").val(data.MobileNumber);
                $("#EmailAddress").val(data.EmailAddress);
                $("#CareHomeName").val(data.CareHomeName.toUpperCase());
                $("#Address1").val(data.Address1.toUpperCase());
                if (data.Address2 != null) {
                    $("#Address2").val(data.Address2.toUpperCase());
                }
                if (data.TownOrCity != null) {
                    $("#TownOrCity").val(data.TownOrCity.toUpperCase());
                }
                $("#ddlCounty").val(data.County);
                $("#PostCode").val(data.PostCode.toUpperCase());
                $("#Assignedcarehome").val(data.CareHomeId);
                var round = data.Round == undefined || data.Round == null ? '' : data.Round;
                var roundId = data.RoundId == undefined || data.RoundId == null ? '' : data.RoundId;
                var displayRound = "";
                if (round != "" || roundId != "") {
                        displayRound = round.slice(-2) + "/" + roundId;
                }
                $("#RoundId").val(displayRound);
                $("#DeliveryFrequency").val(data.DeliveryFrequency);
                $("#hdnCarehomePatientNDD").val(data.NextDeliveryDate);
                $jqGrid('#lblCareHome').text($jqGrid('#hdnlblCareHomeName').val());
                $jqGrid('#lbldelCareHome').text($jqGrid('#hdnlblCareHomeName').val());
                if (data.CommunicationFormat > 0) {
                    $("#ddlCommunicationFormat").val(data.CommunicationFormat);
                }
                $("#hdnSelectedCarehomeStatus").val(data.CareHomeStatus);
                // Make Addess Field Disable                                                
                MakeAddressReadOnly();
                $jqGrid('#btnCarehome').addClass('btn_disable');
                $jqGrid('#btnCarehome').attr('disabled', 'disabled');
                $("#ddlCustomer").prop("disabled", true);
                $("#ddlCustomer").addClass("bg_ddd");
                CopyDeliveryAddress(false);
                //Set Carehome frequency
                window.FrequencyMultipleOf = $("#DeliveryFrequency").val();
                SetCareHomePrescriptionFrequency(true);
            }
        });

        // Get Clinical Contact & Analysis Info
        //setTimeout(function () {
        //    ReloadMedicalStaffGrid();
        //}, 2000);
    }
}

function MakeAddressEmpty() {
    $("#HouseNumber").val('');
    $("#HouseName").val('');
    $("#Address1").val('');
    $("#Address2").val('');
    $("#TownOrCity").val('');
    $("#PostCode").val('');
    $("#ddlCounty").removeAttr('disabled');
    $("#ddlCounty").prop("disabled", false);
    $('#ddlCounty').prop('selectedIndex', 0);
    $("#PhoneNo").val('');
    $("#EmailAddress").val('');
    $("#MobileNo").val('');
    $("#CareHomeName").val('');
    ClearCorrespondenceAddress();
    $("#hdnSelectedCarehomeStatus").val('');
}

function MakeRoundIdEmpty()
{
    $("#RoundId").val('');
    if ($jqGrid("#hdnuserType").val().toUpperCase() == userType.toUpperCase()) {
        $jqGrid("#RoundId").removeAttr('disabled', 'disabled');
        $jqGrid('#RoundId').removeClass('bg_ddd');
        $jqGrid("#spnMandatory").show();
    }
    $("#DeliveryFrequency").val('0');
}
function ConvertDateToDDMMYYYY(dateText) {
    dateText = new Date(dateText);
    var dd = ("0" + dateText.getDate()).slice(-2);
    var mm = ("0" + (dateText.getMonth() + 1)).slice(-2);
    var yyyy = dateText.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
}

function DateTextToMMDDYYYY(dateText) {
    var mmddyyyDate = new Date();
    var ddmmyyyy = dateText.split("/");
    mmddyyyDate.setDate(ddmmyyyy[0]);
    mmddyyyDate.setMonth(ddmmyyyy[1] - 1);
    mmddyyyDate.setFullYear(ddmmyyyy[2]);
    var dd = ("0" + mmddyyyDate.getDate()).slice(-2);
    var mm = ("0" + (mmddyyyDate.getMonth() + 1)).slice(-2);
    var yyyy = mmddyyyDate.getFullYear();
    return mm + '/' + dd + '/' + yyyy;
}

function setNextAssesmentDate(Assesmentdate) {
    $("#NextAssessmentDate").val('');
    var newDate = CalculateDate(Assesmentdate);
    var NextAssesmentdateMandetory = $("#hdnNextAssesmentDateMandetory").val();
    var NextAssesmentdateincrement = $("#hdnNextAssesmentDate").val();
    if (NextAssesmentdateincrement != "") {
        $("#NextAssessmentDate").val(ConvertDateToDDMMYYYY(newDate.setMonth(newDate.getMonth() + parseInt(NextAssesmentdateincrement))));
    }
}

function GetViewPatientAccess() {
    $jqGrid.ajax({
        url: '/Patient/GetViewPatientAccess',
        type: 'GET',
        cache: false,
        success: function (result) {
            if (result == true) {
                DisableEditButton();
            }
            else {
                EnableEditButton();
            }
        }
    });
}

$jqGrid("#ddlReasonCode").on('change', function () {
    var patientCopied = $("#hdnPatientCopiedReasonCode").val();
    var CarehomeRemove = $("#hdnCareHomeRemovedReasonCode").val();
    var CarehomeStopped = $("#hdnCareHomeStoppedReasonCode").val();
    var CareHomeWaitingForApproval = $("#hdnCareHomeWaitingForApproval").val();
    var selectedResonCode = $jqGrid("#ddlReasonCode option:selected").val();

    if (selectedResonCode == CarehomeRemove || selectedResonCode == CarehomeStopped || selectedResonCode == CareHomeWaitingForApproval || selectedResonCode == patientCopied) {
        $('#ddlReasonCode').val(ReasonCode);
    }
});

$("#ddlPatientType").on('focus', function () {
    PreviousPatientType = $("#ddlPatientType").val();
}).change(function () {
    window.isPatientTypeChanged = true;
    var deafultPatientType = $("#ddlPatientType option:first").val();
    var current = $("#ddlPatientType").val();
    var isRedirectFromCarehome = false;
    //Check whether user came from carehome Add Patient button click, If yes then don't show Carehome popup 
    if ($("#hdnCareHomeId").val() > 0) {
        isRedirectFromCarehome = true;
        window.isPatientTypeChanged = false;
    }

    if ((jQuery.inArray(deafultPatientType, NonCarehome)) == -1)
    {
        window.IsCarehomeOldPatientType = true;
    }

    if (current != deafultPatientType) {
        if ((jQuery.inArray(current, NonCarehome)) == -1) {
            $jqGrid('#hdIsCarehomePatient').val(true);
            $jqGrid("#spnMandatory").hide();

            MakeAddressReadOnly();
            var isChecked = $("#chkSameAsDelivery").prop('checked');
            if (!isChecked) {
                $("#divCorrespondenceAddress .patientinfo_form .ez-checkbox").addClass("ez-checked");
                $("#chkSameAsDelivery").prop('checked', true);
            }

            //Check whether user came from carehome Add Patient button click, If yes then don't show Carehome popup 
            if (!isRedirectFromCarehome) {
                MakeAddressEmpty();
                EnableCarehomeButtons();
                ShowCareHomePopup();
            }
            SetAdpMandatoryBasedonPatientType($("#hdnADPMandetory").val(), carehomeValue);
        }
        else {
            window.isFromCarehomeChangePopup = false;
            $jqGrid('#hdIsCarehomePatient').val(false);
            SetAdpMandatoryBasedonPatientType($("#hdnADPMandetory").val(), nonCarehomeValue);
            //Check whether user came from carehome Add Patient button click, If yes then on selection of non carehome PatientType show warning message
            if (isRedirectFromCarehome) {
                CommonScriptObj.DisplayErrorMessage("divsaveError", $("#hdnNonCarehomePatientTypeWarning").val(), 'pop_error_ul');
                $('#ddlPatientType').val("-1");
                return
            }
                //Clear Address field only when Previous PatientType was Carehome patient type
            else if ((jQuery.inArray(PreviousPatientType, NonCarehome)) == -1) {
                MakeAddressEmpty();
                disableCarehome(true);
                MakeRoundIdEmpty();
                $jqGrid("#RoundId").val($jqGrid("#hdnDisplayDefaultRound").val());
            }
            MakeAddressEditable();
        }
        LoadPatientTypeMatrix(current);
        RecalcuateNDDForProCare = false;
        SetPatientNDD(true);
     //   SetCareHomePrescriptionFrequency(false);
        GetPrescriptionNDDRelatedDetails($("#ddlCustomer").val(), $("#Assignedcarehome").val() == "" ? 0 : $("#Assignedcarehome").val(), $("#PatientId").text() == "" ? 0 : $("#PatientId").text());
    }
    else {
        MakeAddressEmpty();
        disablePostcodeCarehome();
        MakeAddressReadOnly();
        MakeRoundIdEmpty();        
    }
});


function SetAdpMandatoryBasedonPatientType(args, value) {    
    if (args.length == 0) {
        args = "false";
    }

    if (value == carehomeValue) {
        if ($(".spn_ADP1:has(span)")) {
            $(".spn_ADP1").children("span").remove();
        }
    }
    else {
        if (args == "true") {
            if ($(".spn_ADP1:has(span)")) {
                $(".spn_ADP1").children("span").remove();
            }
            $(".spn_ADP1").append('<span id="spn_ADP1Mandetory">*</span>');
            $("#hdnADPMandetory").val("true");
        }
        else {
            if ($(".spn_ADP1:has(span)")) {
                $(".spn_ADP1").children("span").remove();
            }
            $("#hdnADPMandetory").val("false");
        }
    }

}
function LoadPatientTypeMatrix(patientTypeEnum) {

    var customerId = $("#ddlCustomer").val();
    $.ajax({
        async: false,
        cache: false,
        url: '/Patient/LoadPatientTypeMatrixAction',
        //type: 'POST',
        data: {
            patientTypeEnum: patientTypeEnum, customerId: customerId
        },
        success: function (data) {
            window.PrescMinFrequency = data.PrescMinFrequency;
            window.PrescMaxFrequency = data.PrescMaxFrequency;
            window.DefaultFrequency = data.DefaultFrequency;
            window.IncrementFrequency = data.IncrementFrequency;
            // Set Multiple Of Frequency when user not came from carehome page
            if ($("#hdnCareHomeId").val() < 1 && $jqGrid('#hdIsCarehomePatient').val() != "true")               
            {
                window.FrequencyMultipleOf = data.IncrementFrequency;
            }
            window.IsPrescriptionApprovalRequired = data.IsPrescriptionApprovalRequired;
            return false;
        }
    });
}//end

function disablePostcode() {
    $jqGrid('#PostCode').addClass('bg_ddd');
    $jqGrid('#PostCode').attr('disabled', 'disabled');
    $jqGrid('#btnPostCode').addClass('btn_disable');
    $jqGrid('#btnPostCode').attr('disabled', 'disabled');
}

function disableCarehome(clearCareHome) {
    $jqGrid('#PostCode').removeClass('bg_ddd');
    $jqGrid('#PostCode').removeAttr('disabled');
    $jqGrid('#PostCode').removeAttr('readonly');
    $jqGrid('#btnPostCode').removeClass('btn_disable');
    $jqGrid('#btnPostCode').removeAttr('disabled');
    $jqGrid('#btnPostCode').removeClass('bg_ddd');

    if (clearCareHome == true) {
        $jqGrid('#CareHomeName').val('');
    }
    $jqGrid("#Assignedcarehome").val('');
    $jqGrid('#btnCarehome').addClass('btn_disable');
    $jqGrid('#btnCarehome').attr('disabled', 'disabled');
    $jqGrid('#lblCareHome').text($jqGrid('#hdnlblHouseName').val());
    $jqGrid('#lbldelCareHome').text($jqGrid('#hdnlblHouseName').val());
}

function EnableCarehomeButtons() {
    $jqGrid('#lblCareHome').text($jqGrid('#hdnlblCareHomeName').val());
    $jqGrid('#lbldelCareHome').text($jqGrid('#hdnlblCareHomeName').val());
    $jqGrid('#btnCarehome').removeClass('btn_disable');
    $jqGrid('#btnCarehome').removeAttr('disabled');
}

function MakeAddressReadOnly() {

    $("#Address1").addClass("bg_ddd");
    $("#Address2").addClass("bg_ddd");
    $("#TownOrCity").addClass("bg_ddd");
    $("#PhoneNo").addClass("bg_ddd");
    $("#MobileNo").addClass("bg_ddd");
    $('#CareHomeName').addClass("bg_ddd");
    $('#PostCode').addClass('bg_ddd');

    $("#Address1").attr('readonly', 'true');
    $("#Address2").attr('readonly', 'true');
    $("#TownOrCity").attr('readonly', 'true');
    $("#ddlCounty").prop("disabled", true);
    $("#PhoneNo").attr('readonly', 'true');
    $("#MobileNo").attr('readonly', 'true');
    $("#CareHomeName").attr('readonly', 'true');
    $("#PostCode").attr('readonly', 'true');
    $jqGrid('#btnPostCode').attr('disabled', 'disabled');
    $jqGrid('#btnPostCode').addClass('bg_ddd');

    $('#chkSameAsDelivery').addClass('bg_ddd');
    $("#chkSameAsDelivery").attr('readonly', 'true');
    $('#chkSameAsDelivery').attr('disabled', 'disabled');
    $("#lnkSameAsDelivery").attr('readonly', 'true');
    $('#lnkSameAsDelivery').attr('disabled', 'disabled');

}
function MakeAddressReadOnlyExceptCounty() {
    $("#HouseNumber").addClass("bg_ddd");
    $("#HouseName").addClass("bg_ddd");
    $("#Address1").addClass("bg_ddd");
    $("#Address2").addClass("bg_ddd");
    $("#TownOrCity").addClass("bg_ddd");
    $("#PhoneNo").addClass("bg_ddd");
    $("#MobileNo").addClass("bg_ddd");
    $('#ddlCounty').attr('readonly', 'true');
    $("#ddlCounty").addClass("bg_ddd");

    $("#Address1").attr('readonly', 'true');
    $("#Address2").attr('readonly', 'true');
    $("#TownOrCity").attr('readonly', 'true');
    $("#PhoneNo").attr('readonly', 'true');
    $("#MobileNo").attr('readonly', 'true');


    $('#chkSameAsDelivery').addClass('bg_ddd');
    $("#chkSameAsDelivery").attr('readonly', 'true');
    $('#chkSameAsDelivery').attr('disabled', 'disabled');
    $("#lnkSameAsDelivery").attr('readonly', 'true');
    $('#lnkSameAsDelivery').attr('disabled', 'disabled');
    if ($("#hdnCareHomeId").val() < 1) {
        $jqGrid('#btnCarehome').removeClass('btn_disable');
        $jqGrid('#btnCarehome').removeAttr('disabled');
    }
}

function MakeAddressEditable() {
    $('#CareHomeName').removeAttr('readonly');
    $('#CareHomeName').removeAttr('disabled');
    $('#Address1').removeAttr('readonly');
    $('#Address1').removeAttr('disabled');
    $('#Address2').removeAttr('readonly');
    $('#Address2').removeAttr('disabled');
    $('#TownOrCity').removeAttr('readonly');
    $('#TownOrCity').removeAttr('disabled');
    $("#ddlCounty").prop("disabled", false);
    $('#ddlCounty').removeAttr('readonly');
    $('#ddlCounty').removeClass("bg_ddd");
    $('#PhoneNo').removeAttr('readonly');
    $('#PhoneNo').removeAttr('disabled');
    $('#MobileNo').removeAttr('disabled');
    $('#MobileNo').removeAttr('readonly');

    $("#CareHomeName").removeClass("bg_ddd");
    $("#Address1").removeClass("bg_ddd");
    $("#Address2").removeClass("bg_ddd");
    $("#TownOrCity").removeClass("bg_ddd");
    $("#County").removeClass("bg_ddd");
    $("#PhoneNo").removeClass("bg_ddd");
    $("#MobileNo").removeClass("bg_ddd");

    $('#chkSameAsDelivery').removeClass('bg_ddd');
    $("#chkSameAsDelivery").removeAttr('readonly');
    $('#chkSameAsDelivery').removeAttr('disabled');
    $("#lnkSameAsDelivery").removeAttr('readonly');
    $('#lnkSameAsDelivery').removeAttr('disabled');


}
var currentPatientStatus;
$jqGrid("#ddlPatientStatus").on('focus', function () {
    currentPatientStatus = "";
    previousPatientStatus = "";
    previousPatientStatus = $jqGrid("#ddlPatientStatus option:selected").text();
}).change(function () {
    currentPatientStatus = $jqGrid("#ddlPatientStatus option:selected").text();
    var PatientStatus = $(this).val();
    if (PatientStatus != $jqGrid("#hdnActivePatientStatus").val() && PatientStatus != "-1") {
        $('#ddlReasonCode').removeAttr('disabled');
        $jqGrid('#ddlReasonCode').removeClass('btn_disable');
    }
    else {
        $('#ddlReasonCode').attr('disabled', 'disabled');
    }
    var adate = new Date();
    var currentDate = (adate.getMonth() + 1) + "/" + adate.getDate() + "/" + adate.getFullYear() + "";
    $("#RemovedStoppedDateTime").text(ConvertDateToDDMMYYYY(currentDate));
    LoadReasonCode(PatientStatus, "");
    var changedpatientStatus = $(this).val();

    //    SharePoint issue #100 
    var newCalculatedNDD;
    if (previousPatientStatus == "Removed" && currentPatientStatus == "Active") {
    // RecalculateNDDforNewCommunityProcarePatient - HDUK-319
         newCalculatedNDD =  NDDForNewCommunityPatient();
         if(newCalculatedNDD == undefined || newCalculatedNDD == "")
         {
             if (jQuery.inArray($("#ddlPatientType").val(), Procare) >= 0) {
                 NDDForCommunityProcarePatient();
             }
         }
         }
         else if (previousPatientStatus == "Stopped" && currentPatientStatus == "Active") {
        if (jQuery.inArray($("#ddlPatientType").val(), Procare) >= 0) {
            NDDForCommunityProcarePatient();
        }
        else
        {
        SetPatientNDD(true);
    }
}
});

function NDDForCommunityProcarePatient()
{
 var roundAndRoundId = $("#RoundId").val().split('/');
    var round = $("#hdnDefaultRound").val() +roundAndRoundId[0];
    var roundId = roundAndRoundId[1].toUpperCase();

    $jqGrid.ajax({
        url: '/Patient/GetNDDForCommunityProcarePatient',
        type: 'GET',
        cache: false,
        async: false,
        data: {
            round: round, roundId: roundId, postCode: $('#PostCode').val()
        },
            success: function (newNDD) {
                        if (newNDD != "" && newNDD != undefined) {
                            ShowUpdatedPatientNDD(newNDD, true);
                            UpdateActivePrescriptionNDD(newNDD, true);
                       return newNDD;
            }
    },
                               error: function (x, e) {
                HandleAjaxError(x, e, jsfilename, "NDDForNewCommunityPatient");
                }
                });
                }


$jqGrid("#ddlGender").on('change', function () {
    disableTitle();
    var Gender = $("#ddlGender option:selected").val();
    if (Gender != -1) {
        enableTitle();
    }
    LoadTitle(Gender, "");
});

//show the updated NDD at patient level
function ShowUpdatedPatientNDD(newNDD, isPastDateCheckRequired) {
    var isDateChange = true;
    var patientNDD = CommonScriptObj.DateTextToMMDDYYYY(newNDD);
 
    if (isPastDateCheckRequired)
    {
        var currentNDD = CommonScriptObj.DateTextToMMDDYYYY($jqGrid("#hdnNextDeliverydate").val());
        if(currentNDD == undefined)
        {
            currentNDD = $jqGrid('#DeliveryDate').text();
        }
        if (new Date(currentNDD) >= new Date(patientNDD))
        {
           isDateChange = false;
        }
    }

    if (isDateChange)
    {
        $jqGrid("#hdnNextDeliverydate").val(newNDD);
        window.ChangeNDD = ConvertDateToDDMMYYYY(patientNDD);
        $jqGrid('#DeliveryDate').text(window.ChangeNDD);
        IsForceDisplayNDD = true;
    }
}

//update all the active prescription NDD with the new NDD
function UpdateActivePrescriptionNDD(newNDD, isPastDateCheckRequired) {
    var activeProducts = 0;
     var prescriptionNDD = CommonScriptObj.DateTextToMMDDYYYY(newNDD);
    finalPrescNDD = ConvertDateToDDMMYYYY(prescriptionNDD);

    var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
    var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');

    for (var i = 0; i < gridRowsprescription.length; i++) {
        var row = gridRowsprescription[i];
        var rowId = ids[i];

        if ($jqGrid(row["IsRemoved"]).val() == 'removed') {
            continue;
        }
        activeProducts = activeProducts + 1;

         if (isPastDateCheckRequired )
         {    
             var curentPrescNdd = CommonScriptObj.DateTextToMMDDYYYY($jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val());
             if(new Date(curentPrescNdd) < new Date(prescriptionNDD))
            {
                 $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(finalPrescNDD);
            }
            }
            else
         {
            $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(finalPrescNDD);
        }
        }
            //Set the Change NDD buttun disabled if all product are removed
    if (activeProducts == 0)
        {
        DisableChangeNDD(true);
        PatientNDD = "";
        $jqGrid('#DeliveryDate').text('');
    }
}

function NDDForNewCommunityPatient() {
    var roundAndRoundId = $("#RoundId").val().split('/');
    var round = $("#hdnDefaultRound").val() + roundAndRoundId[0];
    var roundId = roundAndRoundId[1].toUpperCase();
    var calculatedNDD = undefined;
    $jqGrid.ajax({
        url: '/Patient/GetNDDForNewCommunityPatient',
        type: 'GET',
        cache: false,
        async: false,
        data: {
            patientId: $jqGrid("#PatientId").text(), patientType: $("#ddlPatientType option:selected").val(), round: round, roundId: roundId, postCode: $('#PostCode').val()
        },
        success: function (newNDD) {
            if (newNDD != "" && newNDD != undefined) {
                ShowUpdatedPatientNDD(newNDD, false);
                UpdateActivePrescriptionNDD(newNDD, false);
                calculatedNDD = newNDD;
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "NDDForNewCommunityPatient");
        }
    });
    return calculatedNDD;
}

function disableTitle() {
    $jqGrid('#ddlTitle').attr('disabled', 'disabled');
    $jqGrid('#ddlTitle').attr('readonly', true);
}


function enableTitle() {
    $jqGrid('#ddlTitle').removeAttr('disabled');
    $jqGrid('#ddlTitle').attr('readonly', false);
}

function disablePostcodeCarehome() {
    $jqGrid('#PostCode').addClass('bg_ddd');
    $jqGrid('#PostCode').attr('disabled', 'disabled');
    $jqGrid('#btnPostCode').addClass('btn_disable');
    $jqGrid('#btnPostCode').attr('disabled', 'disabled');

    $jqGrid('#CareHomeName').val('');
    $jqGrid("#Assignedcarehome").val('');
    $jqGrid('#btnCarehome').addClass('btn_disable');
    $jqGrid('#btnCarehome').attr('disabled', 'disabled');
}

function ReloadMedicalStaffGrid() {
    $jqGrid("#jqPatientAnalysis").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
    $jqGrid("#jqMedicalStaffAnalysis").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

    $jqGrid('.staff_select_mode.staff_select_mode_collapse.collapse').css('display', 'none');
    $jqGrid('.analysis_information.analysis_information_collapse.collapse').css('display', 'none');
}

function setCustomerParameter(customerid, flagsearch) {

    $("#hdnNHSIDMandetory").val('');
    $("#hdnADPMandetory").val('');
    $("#hdnNextAssesmentDate").val('');
    $("#hdnNextAssesmentDateMandetory").val('');
    $("#hdnTelephoneMandetory").val('');
    $jqGrid.ajax({
        url: '/Patient/SetCustomerSession',
        type: 'GET',
        cache: false,
        async: false,
        data: {
            customerid: customerid
        },
        success: function (customerparameter) {
            leadTime = customerparameter[0].OrderLeadTime;
            window.countOfMandatoryAnalysisField = customerparameter[0].CountOfMandatoryAnalysisField;
            window.CustomerParamets = customerparameter;
            if (customerparameter != null && customerparameter != "" && customerparameter != undefined) {
                if (customerparameter[0].IsNHSIDMandatory == true) {
                    if ($(".spn_NHS:has(span)")) {
                        $(".spn_NHS").children("span").remove();
                    }
                    $(".spn_NHS").append('<span id="spn_NHSMandetory">*</span>');
                }
                else {
                    if ($(".spn_NHS:has(span)")) {
                        $(".spn_NHS").children("span").remove();
                    }
                }
                if (customerparameter[0].IsADPMandatory == true) {
                    if ($(".spn_ADP1:has(span)")) {
                        $(".spn_ADP1").children("span").remove();
                    }
                    $(".spn_ADP1").append('<span id="spn_ADP1Mandetory">*</span>');
                }
                else {
                    if ($(".spn_ADP1:has(span)")) {
                        $(".spn_ADP1").children("span").remove();
                    }
                }

                if (customerparameter[0].IsTelephoneMandatory == true) {
                    $("#lblPhoneNo").html($("#lblPhoneNo").html().replace("*", "") + "*");
                }
                if (customerparameter[0].IsNextAssessmentDateMandatory == true) {
                    if ($(".spn_NextAssesmentDate:has(span)")) {
                        $(".spn_NextAssesmentDate").children("span").remove();
                    }
                    $(".spn_NextAssesmentDate").append('<span id="spn_NextAssesmentDateMandetory">*</span>');
                }
                else {
                    if ($(".spn_NextAssesmentDate:has(span)")) {
                        $(".spn_NextAssesmentDate").children("span").remove();
                    }
                }

                $("#hdnNHSIDMandetory").val(customerparameter[0].IsNHSIDMandatory);
                $("#hdnADPMandetory").val(customerparameter[0].IsADPMandatory);
                $("#hdnTelephoneMandetory").val(customerparameter[0].IsTelephoneMandatory);
                $("#hdnNextAssesmentDateMandetory").val(customerparameter[0].IsNextAssessmentDateMandatory);
                $("#hdnNextAssesmentDate").val(customerparameter[0].NextAssessmentDate);
                $('#RoundId').val('');
                // if (flagsearch == true) {

                $('#RoundId').val(customerparameter[0].RoundID);
                if (customerparameter[0].IsNDDAllow == true) {
                    DisableChangeNDD(false);
                    //$jqGrid('#changeNDDPrescription').show();
                }
                else {
                    DisableChangeNDD(true);
                    //$jqGrid('#changeNDDPrescription').hide();
                }
                //}
                var AssesmentDate = $("#AssessmentDate").val();
                if (AssesmentDate != "") {
                    setNextAssesmentDate(AssesmentDate);
                }

                // Set the ClinicalContact with Linkage
                $jqGrid("#hdnIsClinicalContactWithLinkage").val(customerparameter[0].IsClinicalContactWithLinkage);
            }

        }
    });
}

function TodaysDate() {
    var calToDate = new Date();
    var dd = ("0" + calToDate.getDate()).slice(-2);
    var mm = ("0" + (calToDate.getMonth() + 1)).slice(-2);
    var yyyy = calToDate.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
}

function LoadReasonCode(Patientstatus, ReasonCode) {
    var customerId = $jqGrid("#ddlCustomer").val();
    $jqGrid.getJSON('/Patient/LoadReasonCode', {
        Patientstatus: Patientstatus, customerId: customerId
    }, function (data) {
        $('#ddlReasonCode option').remove();
        var optionhtml = "<option>" + $("#hdnresddlReasonCodeDefaultValue").val() + "</option>";
        $("#ddlReasonCode").append(optionhtml);
        $.each(data, function (i) {
            if (ReasonCode == data[i].ListId)
                optionhtml = '<option value="' + data[i].ListId + '" selected>' + data[i].DisplayText + '</option>';
            else
                optionhtml = '<option value="' + data[i].ListId + '">' + data[i].DisplayText + '</option>';

            $("#ddlReasonCode").append(optionhtml);
        });
    });
}
function LoadTitle(Gender, Title) {
    $jqGrid.getJSON('/Patient/LoadTitle', {
        Gender: Gender
    }, function (data) {
        $('#ddlTitle option').remove();
        var optionhtml = "<option>" + $("#hdnresddlTitleDefaultValue").val() + "</option>";
        $("#ddlTitle").append(optionhtml);
        $.each(data, function (i) {
            if (Title == data[i].ListId)
                optionhtml = '<option value="' + data[i].ListId + '" selected>' + data[i].DisplayText + '</option>';
            else
                optionhtml = '<option value="' + data[i].ListId + '">' + data[i].DisplayText + '</option>';
            $("#ddlTitle").append(optionhtml);
        });
    });
}

function EnableDetails() {

    $("#ddlPatientType").removeAttr('disabled');
    $('#ddlPatientType').removeClass("bg_ddd");

    $("#ddlPatientStatus").removeAttr('disabled');
    $('#ddlPatientStatus').removeClass("bg_ddd");

    $("#ddlGender").removeAttr('disabled');
    $('#ddlGender').removeClass("bg_ddd");

    $("#ddlTitle").removeAttr('disabled');
    $('#ddlTitle').removeClass("bg_ddd");

    $("#FirstName").removeAttr('disabled');
    $('#FirstName').removeClass("bg_ddd");

    $("#LastName").removeAttr('disabled');
    $('#LastName').removeClass("bg_ddd");

    $("#DateOfBirth").removeAttr('disabled');
    $('#DateOfBirth').removeClass("bg_ddd");

    $("#AssessmentDate").removeAttr('disabled');
    $('#AssessmentDate').removeClass("bg_ddd");

    $("#NextAssessmentDate").removeAttr('disabled');
    $('#NextAssessmentDate').removeClass("bg_ddd");

    $("#EmailAddress").removeAttr('disabled');
    $('#EmailAddress').removeClass("bg_ddd");

    $("#NHSId").removeAttr('disabled');
    $('#NHSId').removeClass("bg_ddd");

    $("#PhoneNo").removeAttr('disabled');
    $('#PhoneNo').removeClass("bg_ddd");

    $("#ADP1").removeAttr('disabled');
    $('#ADP1').removeClass("bg_ddd");

    $jqGrid("#ddlCommunicationFormat").multiselect("enable");
    $jqGrid("#ddlMarketingPreference").multiselect("enable");

    $("#ddlCounty").removeAttr('readonly');

    $("#ADP2").removeAttr('disabled');
    $('#ADP2').removeClass("bg_ddd");

    $("#delAddress").removeAttr('disabled');
    $('#delAddress').removeClass("bg_ddd");

    $("#corAddress").removeAttr('disabled');
    $('#corAddress').removeClass("bg_ddd");

    $("#MobileNo").removeAttr('disabled');
    $('#MobileNo').removeClass("bg_ddd");

    $("#LocalId").removeAttr('disabled');
    $('#LocalId').removeClass("bg_ddd");
}

function DisableDetails() {

    $jqGrid('#btnCarehome').addClass('btn_disable');
    $jqGrid('#btnCarehome').attr('disabled', 'disabled');

    $("#CareHomeName").addClass('readonly', 'true');
    $('#HouseNumber').attr('readonly', 'true');
    $('#HouseName').attr('readonly', 'true');
    $('#Address1').attr('readonly', 'true');
    $('#Address2').attr('readonly', 'true');
    $('#TownOrCity').attr('readonly', 'true');
    $('#PostCode').attr('readonly', 'true');
    $("#ddlCounty").attr('disabled', 'disabled');
    $('#ddlCounty').addClass("bg_ddd");

    $("#CareHomeName").addClass("bg_ddd");
    $('#HouseNumber').addClass("bg_ddd");
    $('#HouseName').addClass("bg_ddd");
    $('#Address1').addClass("bg_ddd");
    $('#Address2').addClass("bg_ddd");
    $('#TownOrCity').addClass("bg_ddd");
    $('#PostCode').addClass("bg_ddd");
    $('#btnPostCode').addClass("bg_ddd");
    $jqGrid('#btnPostCode').attr('disabled', 'disabled');

    $("#ddlPatientType").attr('disabled', 'disabled');
    $('#ddlPatientType').addClass("bg_ddd");

    $("#ddlPatientStatus").attr('disabled', 'disabled');
    $('#ddlPatientStatus').addClass("bg_ddd");

    $("#ddlGender").attr('disabled', 'disabled');
    $('#ddlGender').addClass("bg_ddd");

    $("#ddlTitle").attr('disabled', 'disabled');
    $('#ddlTitle').addClass("bg_ddd");

    $("#FirstName").attr('disabled', 'disabled');
    $('#FirstName').addClass("bg_ddd");

    $("#LastName").attr('disabled', 'disabled');
    $('#LastName').addClass("bg_ddd");

    $("#DateOfBirth").attr('disabled', 'disabled');
    $('#DateOfBirth').addClass("bg_ddd");

    $("#AssessmentDate").attr('disabled', 'disabled');
    $('#AssessmentDate').addClass("bg_ddd");

    $("#NextAssessmentDate").attr('disabled', 'disabled');
    $('#NextAssessmentDate').addClass("bg_ddd");

    $("#EmailAddress").attr('disabled', 'disabled');
    $('#EmailAddress').addClass("bg_ddd");

    $("#NHSId").attr('disabled', 'disabled');
    $('#NHSId').addClass("bg_ddd");

    $("#PhoneNo").attr('disabled', 'disabled');
    $('#PhoneNo').addClass("bg_ddd");

    $("#ADP1").attr('disabled', 'disabled');
    $('#ADP1').addClass("bg_ddd");

    $("#ADP2").attr('disabled', 'disabled');
    $('#ADP2').addClass("bg_ddd");

    $("#delAddress").attr('disabled', 'disabled');
    $('#delAddress').addClass("bg_ddd");

    $("#corAddress").attr('disabled', 'disabled');
    $('#corAddress').addClass("bg_ddd");

    $("#MobileNo").attr('disabled', 'disabled');
    $('#MobileNo').addClass("bg_ddd");

    $("#LocalId").attr('disabled', 'disabled');
    $('#LocalId').addClass("bg_ddd");
}

function CheckCustomerStatus(customerId) {
    if (customerId != "-1") {
        $jqGrid.ajax({
            url: '/Customer/GetCustomerDetails',
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            data: {
                customerId: customerId
            },
            cache: false,
            async: false,
            success: function (responseData) {
                if (responseData.CustomerBusinessModel != null) {
                    var cusotmerDetails = responseData.CustomerBusinessModel;
                    customerCurrentStatus = cusotmerDetails.Status;
                    return false;
                }
            }
        });
    }
    return false;
}

function ValidatePatientInfo(PrescriptionEditedbyNurse) {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li style='width: 350px;'>";
    var errorMsgLiclose = "</li>";
    errorMsg = errorMsgUl;
    if (document.getElementById('ddlCustomer').selectedIndex < 1) {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCustomer").val() + errorMsgLiclose;
    }
    if (document.getElementById('ddlGender').selectedIndex < 1) {

        errorMsg += errorMsgLi + $("#hdnreserrormsgGender").val() + errorMsgLiclose;
    }
    if (document.getElementById('ddlTitle').selectedIndex < 1) {

        errorMsg += errorMsgLi + $("#hdnreserrormsgTitle").val() + errorMsgLiclose;
    }
    if ($.trim($("#FirstName").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgFirstName").val() + errorMsgLiclose;
    }
    if ($.trim($("#LastName").val()) == "") {

        errorMsg += errorMsgLi + $("#hdnreserrormsgLastName").val() + errorMsgLiclose;
    }
    if ($("#DateOfBirth").val() == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgDateofBirth").val() + errorMsgLiclose;
    }
    if ($("#DateOfBirth").val() != "") {
        var data = $jqGrid("#DateOfBirth").val().split("/");
        var fromYear = 1900;
        var invalidDate = "Invalid Date";
        if (data.length == 3) {
            var day = data[0];
            var month = data[1];
            var year = data[2];
            if (year.length != 4) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidDOB").val() + errorMsgLiclose;
            }
            else if (parseInt(year) < parseInt(fromYear)) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidDOB").val() + errorMsgLiclose;
            }
            else {
                var newDate = month + "/" + day + "/" + year;
                var parsedDate = new Date(newDate);
                var newMonth = parsedDate.getMonth() + 1;
                if (newMonth != month) {
                    errorMsg += errorMsgLi + $("#hdnresErrormsgValidDOB").val() + errorMsgLiclose;
                }
                else if (invalidDate == new Date(newDate)) {
                    errorMsg += errorMsgLi + $("#hdnresErrormsgValidDOB").val() + errorMsgLiclose;
                }
                else {
                    if (month.length == 1) {
                        appendZeroToMonth = '0' + month.toString();
                    }
                    else {
                        appendZeroToMonth = month;
                    }
                    if (day.length == 1) {
                        appendZeroToDay = '0' + day.toString();
                    }
                    else {
                        appendZeroToDay = day;
                    }
                    $("#DateOfBirth").val(appendZeroToDay + "/" + appendZeroToMonth + "/" + year);
                }
            }
        }
        else {
            errorMsg += errorMsgLi + $("#hdnresErrormsgValidDOB").val() + errorMsgLiclose;
        }
    }
    if (document.getElementById('ddlPatientType').selectedIndex < 1) {

        errorMsg += errorMsgLi + $("#hdnreserrormsgPatientType").val() + errorMsgLiclose;
    }
    var patientType = $("#ddlPatientType option:selected").text();
    if ((patientType == $("#hdnNursingPatient").val() || patientType == $("#hdnNursingSelfcarePatient").val() || patientType == $("#hdnResidentialPatient").val() || patientType == $("#hdnResidentialSelfcarePatient").val() || patientType == $("#hdnHospitalPatient").val() || patientType == $("#hdnHospitalSelfcarePatient").val()) && ($("#CareHomeName").val() == '')) {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCarehome").val() + errorMsgLiclose;
    }
    if (document.getElementById('ddlPatientStatus').selectedIndex < 1) {
        errorMsg += errorMsgLi + $("#hdnreserrormsgPatientStatus").val() + errorMsgLiclose;
    }
    if ($("#ddlPatientStatus option:selected").text() != $("#hdnActivePatient").val()) {
        if (document.getElementById('ddlReasonCode').selectedIndex < 1) {
            errorMsg += errorMsgLi + $("#hdnreserrormsgReasonCode").val() + errorMsgLiclose;
        }
    }
    if ($("#AssessmentDate").val() == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgAssessmentDate").val() + errorMsgLiclose;
    }
    if ($("#AssessmentDate").val() != "") {
        var Assdate = "";
        var data = $jqGrid("#AssessmentDate").val().split("/");
        var invalidDate = "Invalid Date";
        if (data.length == 3) {
            var day = data[0];
            var month = data[1];
            var year = data[2];
            var newDate = month + "/" + day + "/" + year;
            var parsedDate = new Date(newDate);
            var newMonth = parsedDate.getMonth() + 1;

            if (year.length != 4) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidAssDate").val() + errorMsgLiclose;
            }
            else if (newMonth != month) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidAssDate").val() + errorMsgLiclose;
            }
            else if (invalidDate == new Date(newDate)) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidAssDate").val() + errorMsgLiclose;
            }
            else {
                if (month.length == 1) {
                    appendZeroToMonth = '0' + month.toString();
                }
                else {
                    appendZeroToMonth = month;
                }
                if (day.length == 1) {
                    appendZeroToDay = '0' + day.toString();
                }
                else {
                    appendZeroToDay = day;
                }
                $jqGrid("#AssessmentDate").val(appendZeroToDay + "/" + appendZeroToMonth + "/" + year);

                Assdate = appendZeroToMonth + "/" + appendZeroToDay + "/" + year;
            }
            if (Assdate != "") {
                var EnteredAssdate = new Date(Assdate);

                var TodaysDate = new Date();

                if (EnteredAssdate > TodaysDate) {
                    errorMsg += errorMsgLi + $("#hdnresErrorMessageAssTodaysDate").val() + errorMsgLiclose;
                }
            }
        }
        else {
            errorMsg += errorMsgLi + $("#hdnresErrormsgValidAssDate").val() + errorMsgLiclose;
        }
    }
    if ($("#hdnNextAssesmentDateMandetory").val() == "true" && $("#NextAssessmentDate").val() == "") {
        errorMsg += errorMsgLi + $("#hdnresErrormsgValidNextAssDate").val() + errorMsgLiclose;
    }

    if ($("#NextAssessmentDate").val() != "") {

        var AssessmentDate = CalculateDate($('#AssessmentDate').val());//new Date($('#AssessmentDate').val());
        var NextAssessmentDate = CalculateDate($('#NextAssessmentDate').val())
        if (AssessmentDate > NextAssessmentDate) {
            errorMsg += errorMsgLi + $("#hdnreserrormsgNextAssesmentdate").val() + errorMsgLiclose;
        }
    }

    if ($("#NextAssessmentDate").val() != "") {
        var data = $jqGrid("#NextAssessmentDate").val().split("/");
        var invalidDate = "Invalid Date";
        if (data.length == 3) {
            var day = data[0];
            var month = data[1];
            var year = data[2];
            var newDate = month + "/" + day + "/" + year;
            var parsedDate = new Date(newDate);
            var newMonth = parsedDate.getMonth() + 1;

            if (year.length != 4) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidNextAssDate").val() + errorMsgLiclose;
            }
            else if (newMonth != month) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidNextAssDate").val() + errorMsgLiclose;
            }
            else if (invalidDate == new Date(newDate)) {
                errorMsg += errorMsgLi + $("#hdnresErrormsgValidNextAssDate").val() + errorMsgLiclose;
            }
            else {
                if (month.length == 1) {
                    appendZeroToMonth = '0' + month.toString();
                }
                else {
                    appendZeroToMonth = month;
                }
                if (day.length == 1) {
                    appendZeroToDay = '0' + day.toString();
                }
                else {
                    appendZeroToDay = day;
                }
                $jqGrid("#NextAssessmentDate").val(appendZeroToDay + "/" + appendZeroToMonth + "/" + year);
            }

        }
        else {
            errorMsg += errorMsgLi + $("#hdnresErrormsgValidNextAssDate").val() + errorMsgLiclose;
        }
    }


    if ($("#hdnNHSIDMandetory").val() == "true" && ($.trim($("#NHSId").val()) == "" || $.trim($("#NHSId").val()).length != 10)) {
        errorMsg += errorMsgLi + $("#hdnreserrormsNHSID").val() + errorMsgLiclose;
    }
    //Not mandetory but if provided should be of 10 digit
    if ($("#hdnNHSIDMandetory").val() != "true" && ($.trim($("#NHSId").val()).length > 0 && $.trim($("#NHSId").val()).length < 10)) {
        errorMsg += errorMsgLi + $("#hdnreserrormsNHSID").val() + errorMsgLiclose;
    }

    //Required field validation based on Marketting Preferences
    //var SelectedValue = $jqGrid("#ddlCommunicationFormat").val();
    var SelectedValue = $jqGrid("#ddlCommunicationFormat").multiselect('getChecked').map(function (index) { return $(this).attr('value'); })

    if ((jQuery.inArray($("#hdnCommFormateText").val(), SelectedValue) != -1) && $.trim($("#MobileNo").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsMobile").val() + errorMsgLiclose;
    }

    if ((($("#hdnTelephoneMandetory").val() == "true") || (jQuery.inArray($("#hdnCommFormateTelephoneNo").val(), SelectedValue) != -1)) && $.trim($("#PhoneNo").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsTelephone").val() + errorMsgLiclose;
    }

    if ((jQuery.inArray($("#hdnCommFormateEmail").val(), SelectedValue) != -1) && $.trim($("#EmailAddress").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgEmailAddress").val() + errorMsgLiclose;
    }

    if ($.trim($("#EmailAddress").val()) != "") {
        if (!ValidateEmail($("#EmailAddress").val())) {
            errorMsg += errorMsgLi + $("#hdnreserrormsgEmailAddress").val() + errorMsgLiclose;
        }
    }

    // End
    if ($.trim($("#TownOrCity").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCity").val() + errorMsgLiclose;
    }

    if ($.trim($("#Address1").val()) == "") {

        errorMsg += errorMsgLi + $("#hdnresErrorMessageAddress1").val() + errorMsgLiclose;
    }


    if ($.trim($("#Country").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCountry").val() + errorMsgLiclose;
    }

    if ($.trim($("#PostCode").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgPostCode").val() + errorMsgLiclose;
    }
        
    if ($("#ddlPatientType").val() == -1 || jQuery.inArray($("#ddlPatientType").val(), NonCarehome) != -1)
    {
        if ($("#hdnADPMandetory").val() == "true" && $.trim($("#ADP1").val()) == "") {
            errorMsg += errorMsgLi + $("#hdnreserrormsADP").val() + errorMsgLiclose;
        }
    }
    
    if ($("#DateOfBirth").val() != "" && $("#AssessmentDate").val() != "") {
        var AssessmentDate = CalculateDate($('#AssessmentDate').val());
        var DateOfBirth = CalculateDate($('#DateOfBirth').val());
        if (DateOfBirth > AssessmentDate) {
            errorMsg += errorMsgLi + $("#hdnreserrormsDOBAssDate").val() + errorMsgLiclose;
        }
    }

    if ($("#DateOfBirth").val() != "" && $("#NextAssessmentDate").val() != "") {
        var NextAssessmentDate = CalculateDate($('#NextAssessmentDate').val());
        var DateOfBirth = CalculateDate($('#DateOfBirth').val());
        if (DateOfBirth > NextAssessmentDate) {
            errorMsg += errorMsgLi + $("#hdnreserrormsDOBNextAssDate").val() + errorMsgLiclose;
        }
    }

    errorMsg = ValidateCorrespondenceAddress(errorMsg, errorMsgLi, errorMsgLiclose);

    // Validate the Medical analysis 
    var mandateFields = $(".mandate").filter(function (index) { return $(this).text() === "true"; });

    var totalMandatoryCount = window.countOfMandatoryAnalysisField;
    if (totalMandatoryCount > 0) {
        var totalFilledCount = $(mandateFields).filter(function (index) { return $(this).parent().find(".staffName").text().trim().length > 0 }).length;
        if (totalMandatoryCount != totalFilledCount) {
            errorMsg += errorMsgLi + $("#hdnMandatoryClinicalContacts").val() + errorMsgLiclose;
        }
    }
    // End

    // Start validation for RoundId field
    if ($jqGrid("#hdnuserType").val().toUpperCase() == userType.toUpperCase() && (jQuery.inArray($jqGrid("#ddlPatientType").val(), NonCarehome)) != -1) {
        var roundid = $.trim($("#RoundId").val());
        if (roundid == "") {
            errorMsg += errorMsgLi + $("#hdnreserrormsgRoundIdVal").val() + errorMsgLiclose;
            isErrorOccured = true;
        }
        else if (roundid.indexOf("/") >= 0) {
            var roundAndRoundId = roundid.split('/');
            if (roundAndRoundId.length >= 1 && roundAndRoundId.length <= 2) {
                var round = roundAndRoundId[0];
                if ($.trim(round).length != 2 || !$.isNumeric(round) || round != "90") {
                    errorMsg += errorMsgLi + $("#hdnreserrormsgValidRoundChar").val() + errorMsgLiclose;
                    isErrorOccured = true;
                }
                else if (roundAndRoundId[1] == "" || roundAndRoundId[1] == undefined) {
                    errorMsg += errorMsgLi + $("#hdnreserrormsgValidRoundChar").val() + errorMsgLiclose;
                    isErrorOccured = true;
            }
            }
            else {
                errorMsg += errorMsgLi + $("#hdnreserrormsgValidRoundChar").val() + errorMsgLiclose;
                isErrorOccured = true;
            }
        }
        else {
            errorMsg += errorMsgLi + $("#hdnreserrormsgValidRoundChar").val() + errorMsgLiclose;
            isErrorOccured = true;
        }
    }
    // End validation for RoundId field


    if (PrescriptionEditedbyNurse == true) {
        var NursevalidationMsg = "";
        NursevalidationMsg = validateNurseComments();
        errorMsg += NursevalidationMsg;
    }


    return errorMsg + errorMsgUlclose;

}//validate patientInfo

function CalculateDate(oldData) {
    var newData = oldData.split("/");
    var day = newData[0];
    var month = newData[1];
    var year = newData[2];
    var newDate = month + "/" + day + "/" + year;
    var parsedDate = new Date(newDate);
    return parsedDate;

}

function FillDropDown(data) {

    $('#ddlPatientType option').remove();
    var optionhtml = "<option value='-1'>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
    $("#ddlPatientType").append(optionhtml);
    $.each(data.ddlPatientType, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlPatientType[i].Value + '">' + data.ddlPatientType[i].Text + '</option>';
        $("#ddlPatientType").append(optionhtml);
    });

    //PatientStatus dropdown
    $('#ddlPatientStatus option').remove();
    optionhtml = "";
    optionhtml = "<option  value='-1'>" + $("#hdnresddlPateintStatusDefaultValue").val() + "</option>";
    $("#ddlPatientStatus").append(optionhtml);
    $.each(data.ddlPatientStatus, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlPatientStatus[i].Value + '">' + data.ddlPatientStatus[i].Text + '</option>';
        $("#ddlPatientStatus").append(optionhtml);
    });

    //ReasonCode dropdown
    $('#ddlReasonCode option').remove();
    optionhtml = "";
    optionhtml = "<option>" + $("#hdnresddlReasonCodeDefaultValue").val() + "</option>";
    $("#ddlReasonCode").append(optionhtml);
    $.each(data.ddlReasonCode, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlReasonCode[i].Value + '">' + data.ddlReasonCode[i].Text + '</option>';
        $("#ddlReasonCode").append(optionhtml);
    });


    // Gender Dropdown
    $('#ddlGender option').remove();
    optionhtml = "";
    optionhtml = '<option value="' + -1 + '">' + $("#hdnresddlGenderDefaultValue").val() + '</option>';
    $("#ddlGender").append(optionhtml);
    $.each(data.ddlGender, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlGender[i].Value + '">' + data.ddlGender[i].Text + '</option>';
        $("#ddlGender").append(optionhtml);
    });


    // Marketing Preference dropdown
    $('#ddlMarketingPreference option').remove();
    optionhtml = "";
    //optionhtml = "<option>" + $("#hdnresddlCommunicationFormatDefaultValue").val() + "</option>";
    $("#ddlMarketingPreference").append(optionhtml);
    $.each(data.ddlCommunicationFormat, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlCommunicationFormat[i].Value + '">' + data.ddlCommunicationFormat[i].Text + '</option>';
        $("#ddlMarketingPreference").append(optionhtml);
    });

    $jqGrid("#ddlMarketingPreference").multiselect({
        noneSelectedText: $("#hdnresddlCommunicationFormatDefaultValue").val(),
        click: function (event, ui) {
            SetNoCommunicationValidation(ui, "ddlMarketingPreference");
        }
    });

    // CommunicationFormat dropdown
    $('#ddlCommunicationFormat option').remove();
    optionhtml = "";
    //optionhtml = "<option>" + $("#hdnresddlCommunicationFormatDefaultValue").val() + "</option>";
    $("#ddlCommunicationFormat").append(optionhtml);
    $.each(data.ddlCommunicationFormat, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlCommunicationFormat[i].Value + '">' + data.ddlCommunicationFormat[i].Text + '</option>';
        $("#ddlCommunicationFormat").append(optionhtml);
    });

    $jqGrid("#ddlCommunicationFormat").multiselect({
        noneSelectedText: $("#hdnresddlCommunicationFormatDefaultValue").val(),
        click: function (event, ui) {
            SetNoCommunicationValidation(ui, "ddlCommunicationFormat");
        }
    });

    // Customer dropdown    
    $('#ddlCustomer option').remove();
    optionhtml = "";
    optionhtml = "<option  value='-1'>" + $("#hdnresddlCUstomerDefaultValue").val() + "</option>";
    $("#ddlCustomer").append(optionhtml);
    $.each(data.ddlCustomer, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlCustomer[i].Value + '">' + data.ddlCustomer[i].Value + " " + data.ddlCustomer[i].Text + '</option>';
        $("#ddlCustomer").append(optionhtml);
    });

    if (data.DropDownSelectedvalue.CustomerSelectedValue != null && data.DropDownSelectedvalue.CustomerSelectedValue != 0) {
        $('#ddlCustomer').val(data.DropDownSelectedvalue.CustomerSelectedValue);
        var patientId = data.PatientId;
        var bflagSearch = false;
        if (patientId != null || patientId != 0) {
            bflagSearch = true;
        }
        CustomerChange(data.DropDownSelectedvalue.CustomerSelectedValue, bflagSearch);
    }
    $('#ddlPatientStatus').val($('#ddlPatientStatus option').eq(1).val());

    // County dropdown
    $('#ddlCounty option').remove();
    optionhtml = "";
    optionhtml = "<option>" + $("#hdnresddlCountyDefaultValue").val() + "</option>";
    $("#ddlCounty").append(optionhtml);
    $.each(data.ddlCounty, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlCounty[i].Value + '">' + data.ddlCounty[i].Text + '</option>';
        $("#ddlCounty").append(optionhtml);
    });

    // Correspondence County dropdown
    $('#ddlCorrespondenceCounty option').remove();
    optionhtml = "";
    optionhtml = "<option>" + $("#hdnresddlCountyDefaultValue").val() + "</option>";
    $("#ddlCorrespondenceCounty").append(optionhtml);
    $.each(data.ddlCounty, function (i) {
        optionhtml = "";
        optionhtml = '<option value="' + data.ddlCounty[i].Value + '">' + data.ddlCounty[i].Text + '</option>';
        $("#ddlCorrespondenceCounty").append(optionhtml);
    });
}

function LoadPatinetTypeDropDown(customerId) {
    if (customerId != "-1") {
        $jqGrid.ajax({
            url: '/Patient/GetCustomerPatientType',
            type: 'GET',
            cache: false,
            data: {
                customerId: customerId
            },
            async: false,
            success: function (responseData) {
                if (responseData != false) {
                    $('#ddlPatientType option').remove();
                    var optionhtml = "<option value='-1'>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
                    $("#ddlPatientType").append(optionhtml);
                    $.each(responseData, function (i) {
                        optionhtml = "";
                        optionhtml = '<option value="' + responseData[i].ListId + '">' + responseData[i].DisplayText + '</option>';
                        $("#ddlPatientType").append(optionhtml);
                    });
                }
                else {
                    $('#ddlPatientType option').remove();
                    var optionhtml = "<option value='-1'>" + $("#hdnresddlPateintTypeDefaultValue").val() + "</option>";
                    $("#ddlPatientType").append(optionhtml);
                }
                ClearAddress();
                DisableAddress();
                ClearCorrespondenceAddress();
                DisableCorrespondenceAddress();
            }
        });
    }
}

function SavePatientDetailsRemovedStopped(btnID) {
    var selectedPatientStatus = $("#ddlPatientStatus").val();
    if (selectedPatientStatus == $("#hdnStoppedReasonCodeID").val() || selectedPatientStatus == $("#hdnRemovedReasonCodeID").val()) {
        if (selectedPatientStatus == $("#hdnRemovedReasonCodeID").val()) {
            $('#lblPatientStatusRemoved').show();
            $('#lblPatientStatusStopped').hide();
            $('#lblsaveSuccessRemoved').show();
            $('#lblsaveSuccessStopped').hide();
        }
        if (selectedPatientStatus == $("#hdnStoppedReasonCodeID").val()) {
            $('#lblPatientStatusStopped').show();
            $('#lblPatientStatusRemoved').hide();
            $('#lblsaveSuccessStopped').show();
            $('#lblsaveSuccessRemoved').hide();
        }
        SavePatientDetail(btnID);
    }
    else {
        return;
    }
}//end function

function RemovedStoppedSuccess(selectedPatientStatus, patientID) {

    //window.SelectedPatientStatus = $("#ddlPatientStatus option:selected").text();
    if (patientId == "") {
        patientId = patientID;
    }

    if (selectedPatientStatus == $("#hdnRemovedReasonCodeID").val()) {
        $jqGrid('#lblsaveSuccessRemoved').text(function () {
            return $(this).text().replace("#text", patientId);
        });
    }
    if (selectedPatientStatus == $("#hdnStoppedReasonCodeID").val()) {
        $jqGrid('#lblsaveSuccessStopped').text(function () {
            return $(this).text().replace("#text", patientId);
        });
    }
    $jqGrid('#divsaveSucessRemovedStopped').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralOk").val().toString(),
            "id": "hdnGeneralOk",
            click: function () {
                $jqGrid.ajax({
                    url: '/Patient/LoadPatientDetails',
                    type: 'POST',
                    cache: false,
                    data: {
                        patientId: patientId, customerId: customerId, PatientName: PatientName, CustomerName: CustomerName, SAPCustomerNumber: SAPCustomerNumber, SAPPatientNumber: SAPPatientNumber
                    },
                    success: function (data) {
                        EnableDetails();
                        SetPatientDetails(data);
                        $jqGrid('.collapse').css('display', 'block');
                        $jqGrid('.heading-1 a').addClass('open');
                        $jqComm(".staff_select_mode_collapse").css('display', 'none');
                        $jqComm(".analysis_information_collapse").css('display', 'none');
                        $jqComm("#search_dock_bg").css('display', 'none');
                        DisablePatientInfoPage();
                        window.patientId = patientId;
                        window.customerId = customerId;
                        window.patientName = PatientName;
                        window.customerName = CustomerName;
                        window.CustomerParamets = data.CustomerParameters;
                        if (data.CustomerParameters != null && data.CustomerParameters[0].IsNDDAllow == true) {
                            DisableChangeNDD(false);
                            // $jqGrid('#changeNDDPrescription').show();
                        }
                        else {

                            DisableChangeNDD(true);
                            // $jqGrid('#changeNDDPrescription').hide();
                        }
                        $jqGrid("#jqPatientAnalysis").trigger('reloadGrid');
                        $jqGrid("#jqMedicalStaffAnalysis").trigger('reloadGrid');
                        $jqGrid("#jqPatientPrescription").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                        DisableSaveButton();
                        EnableEditButton();
                        DisablePrescriptionButtons();
                        //EnableCancelButton();
                        DisableCancelButton();
                        //DisableChangeNDD(true);
                        //$jqGrid('#changeNDDPrescription').addClass("btn_disable");
                        //$jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
                    }
                });//ajax                                        
                ShowCustomerPatientName();

                $jqGrid('#divsaveSucessRemovedStopped').dialog('close');
                DisablePatientInfoPage();
                DisableSaveButton();
                EnableCancelButton();
                EnableEditButton();
            },
        }]
    });//dailog
    $jqGrid('#divsaveSucessRemovedStopped').dialog('open');
    return false;
}

function DisablePatientStatus() {
    $jqGrid('#ddlReasonCode').addClass('btn_disable');
    $jqGrid('#ddlReasonCode').attr('disabled', 'disabled');
    $jqGrid('#ddlPatientStatus').addClass('btn_disable');
    $jqGrid('#ddlPatientStatus').attr('disabled', 'disabled');
}

function EnablePatientStatus() {

    $jqGrid('#ddlPatientStatus').removeClass('btn_disable');
    $jqGrid('#ddlPatientStatus').removeAttr('disabled');
}

function IsValidPrescriptionData() {
    var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
    var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');

    //var Assespadsperday = 0;
    //var Startdate = 0;
    //var enddate = 0;
    //var frequency = 0;

    for (var i = 0; i < gridRowsprescription.length; i++) {
        var rowId = ids[i];
        var row = gridRowsprescription[i];

        if ($jqGrid(row["IsRemoved"]).val() == 'removed')
            continue;

        //DQ1: ($jqGrid("#" + rowId + "_DQ1").val() != "" && $jqGrid("#" + rowId + "_DQ1").val() != undefined) ? $jqGrid("#" + rowId + "_DQ1").val() : row["DQ1"],
        //DQ2: ($jqGrid("#" + rowId + "_DQ2").val() != "" && $jqGrid("#" + rowId + "_DQ2").val() != undefined) ? $jqGrid("#" + rowId + "_DQ2").val() : row["DQ2"],
        //DQ3: ($jqGrid("#" + rowId + "_DQ3").val() != "" && $jqGrid("#" + rowId + "_DQ3").val() != undefined) ? $jqGrid("#" + rowId + "_DQ3").val() : row["DQ3"],
        //DQ4: ($jqGrid("#" + rowId + "_DQ4").val() != "" && $jqGrid("#" + rowId + "_DQ4").val() != undefined) ? $jqGrid("#" + rowId + "_DQ4").val() : row["DQ4"],

        var isValidAssessedPadsPerDay = $jqGrid("#" + rowId + "_AssessedPadsPerDay").val();
        if (isValidAssessedPadsPerDay == undefined) {
            isValidAssessedPadsPerDay = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'AssessedPadsPerDay');
        }

        if ((isValidAssessedPadsPerDay == "" || isValidAssessedPadsPerDay == 0 || isValidAssessedPadsPerDay == "0" || isValidAssessedPadsPerDay == undefined) &&
            !($jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) && !($jqGrid("#" + rowId + "_IsRemoved").is(':checked'))) {
            $jqGrid("#divAssessedPadError").text('Please enter Assessed PPD for Product - ' + row["ProductDisplayId"]);
            $jqGrid('#divAssessedPadError').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divAssessedPadError').dialog('close');
                    },
                }]
            });
            $jqGrid('#divAssessedPadError').dialog('open');
            return false;
        }

        var isValidFrequency = $jqGrid("#" + rowId + "_Frequency").val();
        if (isValidFrequency == undefined) {
            isValidFrequency = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'Frequency');
        }
        if (isValidFrequency == "" || isValidFrequency == 0 || isValidFrequency == "0" || isValidFrequency == undefined) {
            $jqGrid("#divfrequencyError").text('Please enter Frequency for Product - ' + row["ProductDisplayId"]);
            $jqGrid('#divfrequencyError').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divfrequencyError').dialog('close');
                    },
                }]
            });
            $jqGrid('#divfrequencyError').dialog('open');
            return false;
        }

        var isValidFromDate = $jqGrid("#" + rowId + "_ValidFromDate").val();
        if (isValidFromDate == undefined) {
            isValidFromDate = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ValidFromDate');
        }
        if (isValidFromDate == "" || isValidFromDate == undefined) {
            $jqGrid("#divstartDateError").text('Please enter Valid From Date for Product - ' + row["ProductDisplayId"]);
            $jqGrid('#divstartDateError').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divstartDateError').dialog('close');
                    },
                }]
            });
            $jqGrid('#divstartDateError').dialog('open');
            return false;

        }

        if (!CommonScriptObj.isValidDate(isValidFromDate)) {
            $jqGrid('#divPrescValidationMsgText').html("Invalid From Date in Prescription for Product  " + row["ProductDisplayId"]);
            $jqGrid('#divPrescriptionValidation').show();
            return false;
        }

        var isValidToDate = $jqGrid("#" + rowId + "_ValidToDate").val();
        if (isValidToDate == undefined) {
            isValidToDate = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ValidToDate');
        }
        if (isValidToDate == "" || isValidToDate == undefined) {
            $jqGrid("#divendDateError").text('Please enter Valid To Date for Product - ' + row["ProductDisplayId"]);
            $jqGrid('#divendDateError').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divendDateError').dialog('close');
                    },
                }]
            });
            $jqGrid('#divendDateError').dialog('open');

            return false;
        }

        if (!CommonScriptObj.isValidDate(isValidToDate)) {
            $jqGrid('#divPrescValidationMsgText').html("Invalid To Date in Prescription for Product  " + row["ProductDisplayId"]);
            $jqGrid('#divPrescriptionValidation').show();
            return false;
        }

        if (new Date(DateTextToMMDDYYYY(isValidFromDate)) > new Date(DateTextToMMDDYYYY(isValidToDate))) {
            $jqGrid("#divendDateError").text('Valid To date must be greater than Valid From date for ' + row["ProductDisplayId"]);
            $jqGrid('#divendDateError').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divendDateError').dialog('close');
                    },
                }]
            });
            $jqGrid('#divendDateError').dialog('open');
            return false;
        }

        var isInvalidQuantity = false;
        var quantityMsg = "Please enter ";
        var isDQ1Zero = $jqGrid("#" + rowId + "_DQ1").val();
        if (isDQ1Zero == undefined) {
            isDQ1Zero = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ1');
        }
        var isDQ2Zero = $jqGrid("#" + rowId + "_DQ2").val();
        if (isDQ2Zero == undefined) {
            isDQ2Zero = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ2');
        }
        var isDQ3Zero = $jqGrid("#" + rowId + "_DQ3").val();
        if (isDQ3Zero == undefined) {
            isDQ3Zero = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ3');
        }
        var isDQ4Zero = $jqGrid("#" + rowId + "_DQ4").val();
        if (isDQ4Zero == undefined) {
            isDQ4Zero = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ4');
        }

        if (isDQ1Zero == "" || isDQ1Zero == 0 || isDQ1Zero == "0" || isDQ1Zero == undefined) {
            quantityMsg = quantityMsg + 'Del1,';
            isInvalidQuantity = true;

        }
        if (isDQ2Zero == "" || isDQ2Zero == 0 || isDQ2Zero == "0" || isDQ2Zero == undefined) {
            quantityMsg = quantityMsg + ' Del2,';
            isInvalidQuantity = true;
        }
        if (isDQ3Zero == "" || isDQ3Zero == 0 || isDQ3Zero == "0" || isDQ3Zero == undefined) {
            quantityMsg = quantityMsg + ' Del3,';
            isInvalidQuantity = true;
        }
        if (isDQ4Zero == "" || isDQ4Zero == 0 || isDQ4Zero == "0" || isDQ4Zero == undefined) {
            quantityMsg = quantityMsg + ' Del4 ,';
            isInvalidQuantity = true;
        }
        quantityMsg = quantityMsg.slice(0, -1);
        quantityMsg = quantityMsg + '  for Product - ';
        if (isInvalidQuantity) {
            $jqGrid("#divQuantityError").text(quantityMsg + row["ProductDisplayId"] + ', or increase the product frequency.');
            $jqGrid('#divQuantityError').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divQuantityError').dialog('close');
                    },
                }]
            });
            $jqGrid('#divQuantityError').dialog('open');
            return false;
        }
    }

    return true;
}


function SavePatientDetails(btnID) {
    var isNurseCommentvalidationReqd = false;

    if ((btnID == "btnSave" || btnID == "btnDetailSave") && !$jqGrid("#ddlPatientStatus").is(':disabled')) {
        if ($("#ddlPatientStatus option:selected").index() != 1 && $("#ddlPatientStatus option:selected").index() != 0) {
            if ($("#ddlReasonCode option:selected").index() == 0) {
                $jqGrid('#divErrorReasonCodeNotSelected').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                        "id": "hdnGeneralOk",
                        click: function () {
                            $jqGrid('#divErrorReasonCodeNotSelected').dialog('close');
                        },
                    }]
                });//end dailog
                $jqGrid('#divErrorReasonCodeNotSelected').dialog('open');
                return false;
            }
            else {
                if (isCopyDetails == false || isCopyDetails == undefined || isCopyDetails == "") {
                SavePatientDetailsRemovedStopped(btnID);
                return false;
            }
        }
    }
    }
    SavePatientDetail(btnID);
}//end of function

    function SavePatientDetail(btnID) {
        //--Checking for patient status and show warning popup
    if (isCopyDetails == true) {
        var selectedPatientStatus = $("#ddlPatientStatus").val();
        if (selectedPatientStatus == $("#hdnStoppedReasonCodeID").val() || selectedPatientStatus == $("#hdnRemovedReasonCodeID").val()) {
            if (selectedPatientStatus == $("#hdnRemovedReasonCodeID").val()) {
                $('#lblPatientStatusRemoved').show();
                $('#lblPatientStatusStopped').hide();
                $('#lblsaveSuccessRemoved').show();
                $('#lblsaveSuccessStopped').hide();
        }
            if (selectedPatientStatus == $("#hdnStoppedReasonCodeID").val()) {
                $('#lblPatientStatusStopped').show();
                $('#lblPatientStatusRemoved').hide();
                $('#lblsaveSuccessStopped').show();
                $('#lblsaveSuccessRemoved').hide();
        }

            if (ExistingPatientStatus != $("#ddlPatientStatus").val() || ExistingReasonCode != $jqGrid("#ddlReasonCode").val()) {
                $jqGrid('#divSavePatientRemovedStopped').dialog({
                        autoOpen: false,
                        modal: true,
                        closeOnEscape: false,
                        width: 'auto',
                        resizable: false,
                        buttons: [{
                                text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                                click: function () {
                            $jqGrid('#divSavePatientRemovedStopped').dialog('close');
                            return true;
                        },
                        }, {
                                text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                                click: function () {
                            $jqGrid('#divSavePatientRemovedStopped').dialog('close');
                            return false;
                        },
                        }]
                });//end dailog
                $jqGrid('#divSavePatientRemovedStopped').dialog('open');
                return false;
        }
    }
    }

    isSaveSuccess = 'false';
    var EditablerowsPrescription = window.EditableRows;
    var patientPrescriptionList = [];

    // validate Prescription

   // if (window.isPatientTypeChanged) {        
        if (!validatePrescriptionFrequency(true)) {          
            return false;
        }
  //  }

    if (!Calculate()) {
        return false;
    }
    CompareActualPPDwithMaxPPD(btnID);

    if (!IsValidPrescriptionData()) {
        return false;
    }



    var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
    var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');
    var ValidPrescription = false;
    var minFrequency = -1;
    var frequencyNDD = '';
    for (var i = 0; i < gridRowsprescription.length; i++) {
        var row = gridRowsprescription[i];
        var rowId = ids[i];
        ValidPrescription = true;
        var actualPPD = $jqGrid("#" + rowId + "_ActualPPD").val();
        if (actualPPD == undefined) {
            actualPPD = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ActualPPD');
        }

        var assessedPadsPerDay = $jqGrid("#" + rowId + "_AssessedPadsPerDay").val();
        if (assessedPadsPerDay == undefined) {
            assessedPadsPerDay = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'AssessedPadsPerDay');
        }

        var description = $jqGrid("#" + rowId + "_ProductName").val();
        if (description == undefined) {
            description = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ProductName');
        }

        var frequency = $jqGrid("#" + rowId + "_Frequency").val();
        if (frequency == undefined) {
            frequency = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'Frequency');
        }

        var changeNDD = $jqGrid("#" + rowId + "_ChangeNDD").val();
        if (changeNDD == undefined) {
            changeNDD = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ChangeNDD');
        }
       
        if ($jqGrid("#" + rowId + "_IsRemoved").is(':checked'))
        {
            var rowData = $jqGrid('#jqPatientPrescription').jqGrid('getRowData', rowId);
            if (rowData["Status"] != "Removed")
            {
                $('#' + rowId + "_IsRemoved").attr('checked', false);
            }
        }

        if (!$jqGrid("#" + rowId + "_IsRemoved").is(':checked')) {
            if (minFrequency == -1) {
                minFrequency = parseInt(frequency);
                frequencyNDD = changeNDD;
            }
            else {
                if (minFrequency > parseInt(frequency)) {
                    minFrequency = parseInt(frequency);
                    frequencyNDD = changeNDD;
                }
                else if (minFrequency == parseInt(frequency)) {
                    if (Date.parse(CommonScriptObj.DateTextToMMDDYYYY(frequencyNDD)) < Date.parse(CommonScriptObj.DateTextToMMDDYYYY(changeNDD))) {
                        frequencyNDD = changeNDD;
                    }
                }
            }
        }
        

        var prescriptionObj = {
            IDXPrescriptionProductId: row["IDXPrescriptionProductId"],
            ProductId: row["ProductId"],
            ProductDisplayId: row["ProductDisplayId"],
            DeliveryDate: ($jqGrid("#" + rowId + "_DeliveryDate").val() != "" && $jqGrid("#" + rowId + "_DeliveryDate").val() != undefined) ? $jqGrid("#" + rowId + "_DeliveryDate").val() : row["DeliveryDate"],
            ProductName: description != '' ? description : row["ProductName"],
            AssessedPadsPerDay: assessedPadsPerDay != '' ? assessedPadsPerDay : row["AssessedPadsPerDay"],
            IsOverriddenFlag: $jqGrid("#" + rowId + "_IsOverriddenFlag").prop('checked'),
            ActualPadsPerDay: ($jqGrid("#" + rowId + "_ActualPadsPerDay").val() != "" && $jqGrid("#" + rowId + "_ActualPadsPerDay").val() != undefined) ? $jqGrid("#" + rowId + "_ActualPadsPerDay").val() : row["ActualPadsPerDay"],
            DQ1: ($jqGrid("#" + rowId + "_DQ1").val() != "" && $jqGrid("#" + rowId + "_DQ1").val() != undefined) ? $jqGrid("#" + rowId + "_DQ1").val() : row["DQ1"],
            DQ2: ($jqGrid("#" + rowId + "_DQ2").val() != "" && $jqGrid("#" + rowId + "_DQ2").val() != undefined) ? $jqGrid("#" + rowId + "_DQ2").val() : row["DQ2"],
            DQ3: ($jqGrid("#" + rowId + "_DQ3").val() != "" && $jqGrid("#" + rowId + "_DQ3").val() != undefined) ? $jqGrid("#" + rowId + "_DQ3").val() : row["DQ3"],
            DQ4: ($jqGrid("#" + rowId + "_DQ4").val() != "" && $jqGrid("#" + rowId + "_DQ4").val() != undefined) ? $jqGrid("#" + rowId + "_DQ4").val() : row["DQ4"],
            Status: ($jqGrid("#" + rowId + "_Status").val() != "" && $jqGrid("#" + rowId + "_Status").val() != undefined) ? $jqGrid("#" + rowId + "_Status").val() : row["Status"],
            SalesUnit: row["SalesUnit"],
            ValidFromDate: ($jqGrid("#" + rowId + "_ValidFromDate").val() != "" && $jqGrid("#" + rowId + "_ValidFromDate").val() != undefined) ? $jqGrid("#" + rowId + "_ValidFromDate").val() : row["ValidFromDate"],
            ValidToDate: ($jqGrid("#" + rowId + "_ValidToDate").val() != "" && $jqGrid("#" + rowId + "_ValidToDate").val() != undefined) ? $jqGrid("#" + rowId + "_ValidToDate").val() : row["ValidToDate"],
            DeliveryDate: ($jqGrid("#" + rowId + "_DeliveryDate").val() != "" && $jqGrid("#" + rowId + "_DeliveryDate").val() != undefined) ? $jqGrid("#" + rowId + "_DeliveryDate").val() : row["DeliveryDate"],
            ChangeNDD: ($jqGrid("#" + rowId + "_ChangeNDD").val() != "" && $jqGrid("#" + rowId + "_ChangeNDD").val() != undefined) ? $jqGrid("#" + rowId + "_ChangeNDD").val() : row["ChangeNDD"],
            RowStatus: ($jqGrid("#" + rowId + "_RowStatus").val() != "" && $jqGrid("#" + rowId + "_RowStatus").val() != undefined) ? $jqGrid("#" + rowId + "_RowStatus").val() : row["RowStatus"],
            ApprovalFlag: ($jqGrid("#" + rowId + "_ApprovalFlag").val() != undefined) ? $jqGrid("#" + rowId + "_ApprovalFlag").val() : row["ApprovalFlag"],
            PrescriptionAuthorizationApprovalFlag: row["PrescriptionAuthorizationApprovalFlag"],
            ProductApprovalFlag: row['ProductApprovalFlag'],
            ActualPPD: actualPPD != '' ? actualPPD : row["ActualPPD"],
            Frequency: ($jqGrid("#" + rowId + "_Frequency").val() != "" && $jqGrid("#" + rowId + "_Frequency").val() != undefined) ? $jqGrid("#" + rowId + "_Frequency").val() : row["Frequency"],
            IsRemoved: ($jqGrid("#" + rowId + "_IsRemoved").is(':checked') == true) ? true : false
        }
        patientPrescriptionList.push(prescriptionObj);
    }

    var medicalStaffdetailsList = [];
    var gridRowsmedicalstaff = $jqGrid("#jqMedicalStaffAnalysis").jqGrid('getRowData');
    for (var i = 0; i < gridRowsmedicalstaff.length; i++) {
        var row = gridRowsmedicalstaff[i];
        var medicalstaffObj = {
            ListTypeId: row["ListTypeId"],
            ListId: row["ListId"],
            Description: row["DefaultText"],
            hdnDefaultText: $jqGrid("#" + i + "").val(),
            Text: row["StaffName"],
            CustomerMedicalStaffAnalysisId: row["CustomerMedicalStaffAnalysisInfoId"],
            RowsStatus: row["RowsStatus"],
            CustomerId: row["CustomerId"],
            IdxPatientMedicalStaffId: row["IDXPatientMedicalStaffId"]
        }
        medicalStaffdetailsList.push(medicalstaffObj);
    }

    var isClinicalContactChanged = true;

    var patientanlysisdetailsList = [];
    var gridRowspatientanalysis = $jqGrid("#jqPatientAnalysis").jqGrid('getRowData');
    for (var i = 0; i < gridRowspatientanalysis.length; i++) {
        var row = gridRowspatientanalysis[i];
        var strCommaSeparatedAnalysisId = row["CustomerMedicalStaffAnalysisInfoId"];
        if (strCommaSeparatedAnalysisId != null && strCommaSeparatedAnalysisId != undefined && strCommaSeparatedAnalysisId != "") {
            var arrCommaSeparatedAnalysisId = strCommaSeparatedAnalysisId.split(',');
            for (var k = 0; k < arrCommaSeparatedAnalysisId.length; k++) {
                var patientanlysisObj = {
                    ListTypeId: row["ListTypeId"],
                    ListId: row["ListId"],
                    Description: row["DefaultText"],
                    hdnDefaultText: $jqGrid("#" + i + "").val(),
                    Text: row["StaffName"],
                    CustomerMedicalStaffAnalysisId: arrCommaSeparatedAnalysisId[k],
                    RowsStatus: row["RowsStatus"],
                    CustomerId: row["CustomerId"],
                    IdxPatientMedicalStaffId: row["IDXPatientMedicalStaffId"]
                }
                patientanlysisdetailsList.push(patientanlysisObj);
            }
        }
    }

    var isAnalysisInfoChanged = true;

    var errmsg = ValidatePatientInfo(window.PrescriptionEditedbyNurse);
    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        CommonScriptObj.DisplayErrorMessage("divErrorMessage", errmsg, 'pop_error_ul');
    }
    else {
        var current = $("#ddlPatientType").val();
        //-----To check the careHome Status and then Patient Status Can be changed 
        if ((jQuery.inArray(current, NonCarehome)) == -1) {
            if ($('#ddlPatientStatus option:selected').val() === $("#hdnActivePatientStatus").val()) {
                if (isCarehomeActive() === false) {
                    StatusError = $("#hdnStoppedStatusError").val();
                    CommonScriptObj.DisplayErrorMessage("divErrorMessage", StatusError, 'pop_error_ul');
                    LoadReasonCode(CarehomeData.DropDownSelectedvalue.PatientStatusSelectedValue, CarehomeData.DropDownSelectedvalue.ReasonCodeSelectedValue);
                    $('#ddlPatientStatus').val(CarehomeData.DropDownSelectedvalue.PatientStatusSelectedValue).change();
                    $jqGrid('#ddlReasonCode').removeAttr('disabled');
                    return false;
                }
            }
        }
        //-----------END----------------------//

        //Check if prescription is having valid row
        if (!ValidPrescription) {
            $("#DeliveryDate").text('');

        }

        var roundAndRoundId = $("#RoundId").val().split('/');
        var round = $("#hdnDefaultRound").val() + roundAndRoundId[0];
        var roundId = roundAndRoundId[1].toUpperCase();

        var arrCommunicationFormat = [];
        $($jqGrid("#ddlCommunicationFormat").multiselect('getChecked').map(function (index) { return $(this).attr('value'); })).each(
            function (e, item) {
                arrCommunicationFormat.push(item);
            });
        var arrMarketingPreferences = [];
        $($jqGrid("#ddlMarketingPreference").multiselect('getChecked').map(function (index) { return $(this).attr('value'); })).each(
            function (e, item) {
                arrMarketingPreferences.push(item);
            });

        var county = null;
        if ($('#ddlCounty').val() != $('#hdnresddlCountyDefaultValue').val() &&  document.getElementById('ddlCounty').selectedIndex >= 1) {
            county = $('#ddlCounty').val();
        }
        var correspondenceCounty = null;
        if ($("#ddlCorrespondenceCounty").val() != $('#hdnresddlCountyDefaultValue').val() && document.getElementById('ddlCorrespondenceCounty').selectedIndex >=1) {
            correspondenceCounty = $("#ddlCorrespondenceCounty").val();
        }

        var finalPatientNDD = window.ChangeNDD;
        if (!IsForceDisplayNDD)
        {
            finalPatientNDD = frequencyNDD;
        }      
        var jsonData = {
            "CustomerId": $("#ddlCustomer").val(),
            "PatientId": $("#PatientId").text(),
            "SAPPatientNumber": $.trim($("#SAPPatientNumber").text()),
            //"NextDeliveryDate": $("#DeliveryDate").text(),
            "NextDeliveryDate": finalPatientNDD,
            "PatientTypeId": $('#ddlPatientType').val(),
            "PatientStatusId": $('#ddlPatientStatus').val(),
            "AssessmentDate": $('#AssessmentDate').val(),
            "ReasonCodeId": $('#ddlReasonCode').val(),
            "NextAssessmentDate": $('#NextAssessmentDate').val(),
            "DeliveryFrequency": $.trim($('#DeliveryFrequency').val()),
            //"RoundId": $.trim($('#RoundId').val()),
            "RoundId": roundId,
            "Round": round,
            "DateOfBirth": $("#DateOfBirth").val(),
            "NHSId": $.trim($('#NHSId').val()),
            "LocalId": $.trim($("#LocalId").val()),
            "CommunicationFormat": $('#ddlCommunicationFormat').val(),
            "HouseNumber": $.trim($('#HouseNumber').val()),
            "HouseName": $.trim($('#CareHomeName').val()),
            "Address1": $.trim($('#Address1').val()),
            "Address2": $.trim($('#Address2').val()),
            "TownOrCity": $.trim($('#TownOrCity').val()),
            "PostCode": $.trim($('#PostCode').val()),
            "ADP1": $.trim($('#ADP1').val()),
            "ADP2": $.trim($('#ADP2').val()),
            "Country": $.trim($('#Country').val()),
            "County": county,
            "OldPatientStatus": $("#hdnPatientStatus").val(),
            "OldPatientType": $("#hdnPatientType").val(),
            "TitleId": $('#ddlTitle').val(),
            "Gender": $("#ddlGender").val(),
            "FirstName": $.trim($('#FirstName').val()),
            "LastName": $.trim($('#LastName').val()),
            "TelephoneNumber": $.trim($('#PhoneNo').val()),
            "MobileNumber": $.trim($('#MobileNo').val()),
            "Email": $('#EmailAddress').val(),
            "AssignedCareHome": $("#Assignedcarehome").val(),
            "AssignedCustomer": $("#ddlCustomer option:selected").text(),
            //Set Correspondence Address 
            "CorrespondenceSameAsDelivery": $("#chkSameAsDelivery").prop('checked'),
            "CorrespondenceCareHomeName": $("#CorrespondenceCareHomeName").val(),
            "CorrespondenceAddress1": $("#CorrespondenceAddress1").val(),
            "CorrespondenceAddress2": $("#CorrespondenceAddress2").val(),
            "CorrespondenceTownOrCity": $("#CorrespondenceTownOrCity").val(),
            "CorrespondenceCounty": correspondenceCounty,
            "CorrespondencePostCode": $("#CorrespondencePostCode").val(),
            //Set Communication Preferences
            "CommunicationPreferences": arrCommunicationFormat,
            "MarketingPreferences": arrMarketingPreferences,
            "IsDataProtected": $("#chkIsDataProtected").prop('checked'),
            "IsClinicalLinkage": $jqGrid("#hdnIsClinicalContactWithLinkage").val(),
            "IsClinicalContactChanged": isClinicalContactChanged,
            "IsAnalysisInfoChanged": isAnalysisInfoChanged,
            "ExistingPatientNDD": DateTextToMMDDYYYY(window.existingdbndd),
            "RecalcuateNDDForProCare": RecalcuateNDDForProCare
        };

        var NurseCommentdata = {
            "Name": $jqGrid('#txtNurseName').val(),
            "MobileNumber": $jqGrid('#txtNurseMobile').val(),
            "Email": $jqGrid('#txtNurseEmail').val(),
            "Comments": $jqGrid('#txtNurseComments').val()
        };

        var Patientdata = JSON.stringify(jsonData);

        if (ShowPPDAlertBeforeSave) {
            var selectedPatientStatus = $("#ddlPatientStatus").val();
            var selectedReasonCode = $("#ddlReasonCode").val();
            if (selectedPatientStatus != $jqGrid("#hdnActivePatientStatus").val() && !$jqGrid("#ddlPatientStatus").is(':disabled') && selectedReasonCode != $jqGrid("#hdnWaitingForApproval").val() && (selectedPatientStatus == $jqGrid("#hdnStoppedPatient").val() || selectedPatientStatus == $("#hdnRemovedReasonCodeID").val())) {
                SaveInformation(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
            }
            else {
                $jqGrid('#divMaxPadPerDayError').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid(this).dialog('close');
                        SaveInformation(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid(this).dialog('close');
                        },
                    }]
                });
                $jqGrid('#divMaxPadPerDayError').html('');
                $jqGrid('#divMaxPadPerDayError').append($jqGrid("#hdnErrorMaxPerDay").val() + ' ' + MaxPPDValue);
                $jqGrid('#divMaxPadPerDayError').append('<br><br>');
                $jqGrid('#divMaxPadPerDayError').append($jqGrid("#hdnPatientSaveConfirmMessage").val());
                $jqGrid('#divMaxPadPerDayError').dialog('open');
            }
        }
        else {
            SaveInformation(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
        }                
    }
    GetPrescriptionNDDRelatedDetails($("#ddlCustomer").val(),
                                    $("#Assignedcarehome").val() == "" ? 0: $("#Assignedcarehome").val(),
                                    $("#PatientId").text() == "" ? 0: $("#PatientId").text());
    ClearPatientChanged();
}

    function SaveInformation(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails) {
    
    //*******************
    var selectedPatientStatus = $("#ddlPatientStatus").val();
    if (ExistingPatientStatus != "" && ExistingPatientStatus != $("#ddlPatientStatus").val() && jsonData['PatientId'] != "") {
        if (selectedPatientStatus == $("#hdnActiveReasonCodeID").val()) {
            $jqGrid('#lblPatientStatusActive').show();
            $jqGrid('#divSavePatientActive').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralYes").val().toString(),
                    "id": "hdnGeneralYes",
                    click: function () {
                        $jqGrid('#divSavePatientActive').dialog('close');
                            SavePatientInfo(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
                    },
                }, {
                    text: $jqGrid("#hdnGeneralNo").val().toString(),
                    "id": "hdnGeneralNo",
                    click: function () {
                        $jqGrid('#divSavePatientActive').dialog('close');
                        return false;
                    },
                }]
            });//end dailog
            $jqGrid('#divSavePatientActive').dialog('open');
            return false;
        }
        else {
                SavePatientInfo(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
        }
    }
    else {
            SavePatientInfo(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata,isCopyDetails);
    }
    //*******************
}

    function SavePatientInfo(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails) {
        var isProductExist;
        var patientStatus;
        if (isCopyDetails == true) {
            isProductExist = CheckPrescriptionProductExitForCustomers($('#hdnPatientId').val(), $('#ddlCustomer').val(), patientPrescriptionList);
            if (isProductExist > 0) {
                return false;
            }
        }
        patientStatus= $("#ddlPatientStatus").val();
        if (patientStatus == $("#hdnStoppedReasonCodeID").val() || patientStatus == $("#hdnRemovedReasonCodeID").val()) {
            if (ExistingPatientStatus != $("#ddlPatientStatus").val() || ExistingReasonCode != $jqGrid("#ddlReasonCode").val()) {
                $jqGrid('#divSavePatientRemovedStopped').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid('#divSavePatientRemovedStopped').dialog('close');
                            SaveAllPatientDetails(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divSavePatientRemovedStopped').dialog('close');
                            return false;
                        },
                    }]
                });//end dailog
                $jqGrid('#divSavePatientRemovedStopped').dialog('open');
                return false;
            }
            else {
                SaveAllPatientDetails(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
            }
        }
        else {
            SaveAllPatientDetails(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails);
        }
    }

    function SaveAllPatientDetails(jsonData, patientPrescriptionList, medicalStaffdetailsList, patientanlysisdetailsList, NurseCommentdata, isCopyDetails) {
        $.ajax({
            url: '/Patient/SavePatientDetails',
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            data: JSON.stringify({ objPatientdetails: jsonData, objPrescriptionDetails: patientPrescriptionList, objMedicalstaffdetails: medicalStaffdetailsList, objPatientanlysisdetails: patientanlysisdetailsList, objNurseComment: NurseCommentdata, isCopyDetailCall: isCopyDetails }),
            success: function (results) {
                var patient = parseInt(results.PatientId);
                var patientStatusId = parseInt(results.PatientStatusId);
                var reasonCodeId = parseInt(results.ReasonCodeId);
                var selectedPatientStatus = $("#ddlPatientStatus").val();
                $("#PatientId").text(patient);
                if (selectedPatientStatus != $jqGrid("#hdnActivePatientStatus").val() && !$jqGrid("#ddlPatientStatus").is(':disabled')) {
                    if ((reasonCodeId != $jqGrid("#hdnWaitingForApproval").val() && patientStatusId == $jqGrid("#hdnStoppedPatient").val()) || patientStatusId == $("#hdnRemovedReasonCodeID").val()) {
                        RemovedStoppedSuccess(patientStatusId, patient);
                    }
                }
                else {
                    ExistingPatientStatus = patientStatusId;
                    ExistingReasonCode = reasonCodeId;
                    var deliveryFrequency = results.Frequency;
                    var frequencyNDD = results.FrequencyNDD;
                    var round = results.Round == results.Round == undefined || results.Round == null ? '' : results.Round;
                    var roundId = results.RoundId == results.RoundId == undefined || results.RoundId == null ? '' : results.RoundId;
                    $("#hdnRound").val(round);
                    $("#hdnRoundId").val(roundId);
                    $("#hdnNextDeliverydate").val(frequencyNDD);
                    $("#DeliveryDate").text(frequencyNDD);
                    if (isCopyDetails == true) {
                        $jqGrid('#btnContactInfo').removeClass("btn_disable")
                        $jqGrid('#btnNotes').removeClass("btn_disable")
                    }
                    if (results.savestatus == true) {
                        isSaveSuccess = 'true';
                        if (window.btnApproveClicked == true) {
                            return false;
                        }
                        $jqGrid('#divsaveSucess').dialog({
                            autoOpen: false,
                            modal: true,
                            closeOnEscape: false,
                            width: 'auto',
                            resizable: false,
                            buttons: [{
                                text: $jqGrid("#hdnGeneralOk").val().toString(),
                                "id": "hdnGeneralOk",
                                click: function () {
                                    RecalcuateNDDForProCare = false;
                                    window.existingdbndd = frequencyNDD;
                                    existingdbPatientType = $jqGrid("#ddlPatientType").val();
                                    ToggleMedicalInfo();
                                    if (isCopyDetails == true) {
                                        window.location = $("#hdnRedirectToViewPatient").val();
                                    }
                                    $jqGrid("#PatientId").text(patient);
                                    $jqGrid('#ddlPatientStatus').val(patientStatusId);

                                    if (reasonCodeId == $jqGrid("#hdnWaitingForApproval").val()) {
                                        $jqGrid("#RemovedStoppedDateTime").text(results.RemovedStoppedDateTime);
                                        LoadReasonCode(patientStatusId, reasonCodeId);
                                    }
                                    else if (patientStatusId == $jqGrid("#hdnActivePatientStatus").val()) {
                                        $('#ddlReasonCode').val($('#ddlReasonCode option').eq(0).val());
                                    }
                                    if (patientStatusId == $jqGrid("#hdnActivePatientStatus").val()) {
                                        window.PatientStatus = $("#hdnActivePatient").val();
                                        //$jqGrid("#RemovedStoppedDateTime").text('');
                                        $jqGrid("#RemovedStoppedDateTime").text(results.RemovedStoppedDateTime);
                                        $jqGrid('#ddlPatientStatus').removeClass('btn_disable');
                                        $jqGrid('#ddlPatientStatus').removeAttr('disabled');
                                    }
                                    else
                                        window.PatientStatus = "stopped/removed";
                                    window.patientId = patient;
                                    window.customerId = $("#ddlCustomer").val();
                                    window.patientName = $('#FirstName').val() + $('#LastName').val();
                                    window.customerName = $("#ddlCustomer option:selected").text();
                                    ShowCustomerPatientName();

                                    $jqGrid('#ddlCustomer').attr('disabled', 'disabled');
                                    $jqGrid("#DeliveryFrequency").val(deliveryFrequency);
                                    DisablePatientInfoPage();

                                    DisableSaveButton();
                                    DisableCancelButton();
                                    EnableEditButton();
                                    ClearNurseComments();
                                    DisableNurseComments();
                                    //// Set New Patient Id and Customer Id to Common Object 
                                    SetCommonObjects();
                                    $jqGrid('#divsaveSucess').dialog('close');

                                    if (window.isPatientInfoEditable.toString().toUpperCase() != value.toUpperCase()) {
                                        EnablePatientInfoButtons();
                                    }

                                    DisablePrescriptionButtons();
                                    $jqGrid("#jqPatientPrescription").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                    $jqGrid('#txtPIN').val('');
                                        $jqGrid('#RoundId').val(round.slice(-2) + "/" + roundId);
                                    //Set carehome Id 0 so PatientType change validation will be set to normal Add patient validation
                                    //Note: hdnCareHomeId is only used to identify user has come from Carehome screen                                                                 
                                    if ($("#hdnCareHomeId").val() > 0 && $('#hdnisRedirectFromProductOrder').val() == "1") {
                                        ClearIsRedirectFromProductOrderSession();
                                        window.location.href = "/Order/ProductOrders";
                                    }
                                    else if ($("#hdnCareHomeId").val() > 0) {
                                        window.location.href = "/CareHome/GetCareHome";
                                    }
                                },
                            }]
                        });
                        $jqGrid('#divsaveSucess').dialog('open');
                        $(".ui-dialog-titlebar-close").hide();
                        $jqGrid("#lblsuccess").text('');
                        var sucessMessage = $("#hdnPatientSaveMessage").val();
                        sucessMessage = sucessMessage.replace("#text", patient);
                        if (reasonCodeId == $jqGrid("#hdnWaitingForApproval").val() && patientStatusId == $jqGrid("#hdnStoppedPatient").val()) {
                            sucessMessage += $jqGrid('#hdnMessageProductsSentForApproval').val();
                            sucessMessage += "</br>";
                            sucessMessage += $jqGrid('#hdnMessagePatientStopped').val();
                            // STATUS IS STOPPED AND RESON IS WAITING FOR APPROVAL. SHOULD NOT ALLOW FOR STATUS CHANGE.
                            $jqGrid('#ddlPatientStatus').addClass('btn_disable');
                            $jqGrid('#ddlPatientStatus').attr('disabled', 'disabled');
                        }
                        $jqGrid("#lblsuccess").append(sucessMessage);
                        if ($jqGrid('#hdnIsredirectFromApproval').val() != "" || $jqGrid('#hdnIsredirectFromApproval').val() != "undefined") {
                            if (patientStatusId == $jqGrid("#hdnActivePatientStatus").val() && $jqGrid('#hdnIsredirectFromApproval').val() == 1) {
                                HideApprovalDiv();
                            }
                        }

                        $jqGrid("#jqPatientAnalysis").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                        $jqGrid("#jqMedicalStaffAnalysis").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                    }
                    else if (results.savestatus == false) {
                        isSaveSuccess = 'false';
                        $jqGrid('#divsaveError').dialog({
                            autoOpen: false,
                            modal: true,
                            closeOnEscape: false,
                            width: 'auto',
                            resizable: false,
                            buttons: [{
                                text: $jqGrid("#hdnGeneralOk").val().toString(),
                                "id": "hdnGeneralOk",
                                click: function () {
                                    $jqGrid('#divsaveError').dialog('close');
                                },
                            }]
                        });
                        $jqGrid('#divsaveError').dialog('open');
                    }
                }
            }
        });
    }

    function ClearIsRedirectFromProductOrderSession() {
        $jqGrid.ajax({
            url: '/Patient/ClearIsRedirectFromProductOrderSession',
            type: 'GET',
            cache: false,
            success: function () {
                return true;
            }
        });
    }

    function SetDropDownData(data) {
        FillDropDown(data);
        $("#hdnPatientType").val('');
        $("#hdnPatientStatus").val('');

        //PatientStatus dropdown
        $("#ddlPatientStatus").val(data.DropDownSelectedvalue.PatientStatusSelectedValue);
        ExistingPatientStatus = data.DropDownSelectedvalue.PatientStatusSelectedValue;
        if (data.DropDownSelectedvalue.PatientStatusSelectedValue != $("#hdnActivePatientValue").val()) {
            $('#ddlReasonCode').removeAttr('disabled');
            //$('#RemovedStoppedDateTime').text(data.RemovedStoppedDateTime);
        }
        else {
            $('#ddlReasonCode').attr('disabled', 'disabled');
            //$('#RemovedStoppedDateTime').text('');
        }
        $('#RemovedStoppedDateTime').text(data.RemovedStoppedDateTime);
        //ReasonCode dropdown
        $("#ddlReasonCode").val(data.DropDownSelectedvalue.ReasonCodeSelectedValue);
        ExistingReasonCode = data.DropDownSelectedvalue.ReasonCodeSelectedValue;

        // Gender Dropdown
        $("#ddlGender").val(data.DropDownSelectedvalue.GenderSelectedValue);

        // Communication Format dropdown    
        var SelectedArray = [];
        if (data.CommunicationPreferences != undefined && data.CommunicationPreferences.length > 0) {
            SelectedArray = data.CommunicationPreferences;
            $jqGrid("#ddlCommunicationFormat").val(SelectedArray);
            $jqGrid("#ddlCommunicationFormat").multiselect('refresh');
            $jqGrid('input[type="radio"], .ui-corner-all input[type="checkbox"]').ezMark();
            $jqGrid('#jqPatientSearchResults tr td div').removeClass('ez-checkbox');
            $jqGrid('#jqPatientSearchResults tr td div input[type="checkbox"]').removeClass('ez-hide');
        }


        // Marketing Preferences dropdown    
        if (data.MarketingPreferences != undefined && data.MarketingPreferences.length > 0) {
            SelectedArray = data.MarketingPreferences;
            $jqGrid("#ddlMarketingPreference").val(SelectedArray);
            $jqGrid("#ddlMarketingPreference").multiselect('refresh');
            $jqGrid('input[type="radio"], .ui-corner-all input[type="checkbox"]').ezMark();
            $jqGrid('#jqPatientSearchResults tr td div').removeClass('ez-checkbox');
            $jqGrid('#jqPatientSearchResults tr td div input[type="checkbox"]').removeClass('ez-hide');
        }

        //CustomerDropdown    
        $("#ddlCustomer").val(data.DropDownSelectedvalue.CustomerSelectedValue);
        $('#ddlCustomer').attr('disabled', 'disabled');
        $("#hdnAssignedCustomer").val(data.DropDownSelectedvalue.CustomerSelectedValue);

        var Gender = $("#ddlGender option:selected").val();
        var Title = data.DropDownSelectedvalue.TitleSelectedValue;
        LoadTitle(Gender, Title);

        ////Title drop down
        $("#ddlTitle").val(data.DropDownSelectedvalue.TitleSelectedValue);

        var PatientStatus = data.DropDownSelectedvalue.PatientStatusSelectedValue;
        var ReasonCode = data.DropDownSelectedvalue.ReasonCodeSelectedValue;
        LoadReasonCode(PatientStatus, ReasonCode);
        if ($("#ddlPatientStatus").val() == $("#hdnStoppedReasonCodeID").val() && (ReasonCode == $jqGrid("#hdnWaitingForApproval").val() || hasActiveOrder == "true")) {
            //$jqGrid('#ddlPatientStatus').addClass('btn_disable');
            $jqGrid('#ddlReasonCode').attr('disabled', 'disabled');
            $jqGrid('#ddlPatientStatus').attr('disabled', 'disabled');
        }
        else {
            $jqGrid('#ddlPatientStatus').removeClass('btn_disable');
            $jqGrid('#ddlPatientStatus').removeAttr('disabled');
            //$jqGrid('#ddlReasonCode').attr('disabled', 'disabled');
        }

        var customerId = data.DropDownSelectedvalue.CustomerSelectedValue;
        setCustomerParameter(customerId, false);
        // County dropdown -- This should be after LoadPatinetTypeDropDown
        if (data.DropDownSelectedvalue.CountySelectedValue == null) {
            $("#ddlCounty").val($('#hdnresddlCountyDefaultValue').val());
        }
        else {
            $("#ddlCounty").val(data.DropDownSelectedvalue.CountySelectedValue);
        }
            //PatientType dropdown    
        $("#ddlPatientType").val(data.DropDownSelectedvalue.PatientTypeSelectedValue);

        var OldPatientType = $("#ddlPatientType").val();
        EnableDisableAddress(OldPatientType);

        LoadPatientTypeMatrix($("#ddlPatientType").val());

        $("#hdnPatientType").val($("#ddlPatientType option:selected").text());
        $("#hdnPatientStatus").val($("#ddlPatientStatus option:selected").text());
    }

    function EnableDisableAddress(PatientType) {
        //non carehome patient postcode unable   

        if (jQuery.inArray(PatientType, NonCarehome) != -1) {
            disableCarehome(false);
            MakeAddressEditable();
        }
        else {
            disablePostcode();
            MakeAddressReadOnlyExceptCounty();
        }
    }


    // This method will fill the patient detials returned from patient controller in form json data 
    function SetPatientDetails(data) {
        
        var current = data.DropDownSelectedvalue.PatientTypeSelectedValue;
        var houseName = "";
        if ((jQuery.inArray(current.toString(), NonCarehome)) != -1) {
            $jqGrid('#lblCareHome').text($jqGrid('#hdnlblHouseName').val());
            $jqGrid('#CareHomeName').removeClass("bg_ddd");
            $jqGrid('#CareHomeName').removeAttr('readonly');
            if (data.HouseName != undefined && data.HouseName != "") {
                houseName = data.HouseName.toString().toUpperCase();
            }
        }
        else {
            $jqGrid('#lblCareHome').text($jqGrid('#hdnlblCareHomeName').val());
            $jqGrid('#CareHomeName').addClass("bg_ddd");
            $("#CareHomeName").attr('readonly', 'true');
            if (data.CareHomeName != undefined && data.CareHomeName != "") {
                houseName = data.CareHomeName.toString().toUpperCase();
            }
    }
    if (isCopyDetails == true) {
        var dt = new Date();
        var currentDate = ConvertDateToDDMMYYYY(dt);
        $("#PatientId").text('');
        $("#CreatedDate").text(currentDate);
        }
    else {
        $("#PatientId").text(data.PatientId);
        $("#CreatedDate").text(data.CreatedDate);
        $("#CreatedBy").text(data.CreatedBy);
    }
        SetDropDownData(data);
        $("#SAPPatientNumber").text('');
        if (data.SAPPatientNumber != null) {
        if (isCopyDetails == true) {
            LoadPatinetTypeDropDown($('#ddlCustomer').val());
            $("#ddlCounty").val(data.DropDownSelectedvalue.CountySelectedValue);
            $("#ddlPatientType").val(data.DropDownSelectedvalue.PatientTypeSelectedValue);
            $("#SAPPatientNumber").text('');
            $('#ddlCustomer').removeAttr('disabled');
            $('#RoundId').removeAttr('disabled');
            //--------------Checking if prescription is set then frequency will be filled
            //    if (data.NextDeliveryDate == null || data.NextDeliveryDate == undefined || data.NextDeliveryDate == "") { $("#DeliveryFrequency").val(''); }
        }
        else {
            $("#SAPPatientNumber").text(data.SAPPatientNumber);
            //$("#DeliveryFrequency").val(data.DeliveryFrequency);
    }
        }
        var dataRound = "";
        var dataRoundId = "";
        var displayRound = "";
        if (data.Round != undefined && data.Round != "") {
            dataRound = data.Round;
        }
        if (data.RoundId != undefined && data.RoundId != "") {
        if (isCopyDetails == true) {
            dataRoundId = 'COPY';
        }
        else {
            dataRoundId = data.RoundId;
        }
    }

        $("#hdnRound").val(dataRound);
        $("#hdnRoundId").val(dataRoundId);
        if (dataRound != "" || dataRoundId != "") {
            displayRound = dataRound.slice(-2) + "/" + dataRoundId;

    }
        $("#CareHomeName").val(houseName);
        $("#hdnDeliverydate").val(data.DeliveryDate);
        $("#hdnNextDeliverydate").val(data.NextDeliveryDate);
        window.existingdbndd = data.NextDeliveryDate;
        window.ChangeNDD = data.NextDeliveryDate;
        RecalcuateNDDForProCare = false;
        existingdbPatientType = data.DropDownSelectedvalue.PatientTypeSelectedValue;
        if (HasActivePrescriptionProduct()) {
            $("#DeliveryDate").text(data.NextDeliveryDate);        
        }
        $("#AssessmentDate").val(data.AssessmentDate);
        $("#NextAssessmentDate").val(data.NextAssessmentDate);
        $("#FirstName").val(data.FirstName);
        $("#LastName").val(data.LastName);
        $("#RoundId").val(displayRound);
        //$("#RoundId").val(data.RoundId);
        $("#DateOfBirth").val(data.DateOfBirth);

        $("#NHSId").val(data.NHSId);
        $("#LocalId").val(data.LocalId);
        $("#PhoneNo").val(data.PhoneNo);
        $("#MobileNo").val(data.MobileNo);
        $("#EmailAddress").val(data.EmailAddress);
        $("#ContactPersonName").val(data.ContactPersonName);
        $("#DeliveryFrequency").val(data.DeliveryFrequency);
        $("#HouseNumber").val(data.HouseNumber);
        $("#HouseName").val(data.HouseName);
        $("#Address1").val(data.Address1);
        $("#Address2").val(data.Address2);
        $("#TownOrCity").val(data.TownOrCity);
        $("#PostCode").val(data.PostCode);
        $("#Country").val(data.Country);
        $("#ADP1").val(data.ADP1);
        $("#ADP2").val(data.ADP2);
        $("#Assignedcarehome").val(data.CareHomeId);
        $('#hdnCarehomePatientNDD').val(data.CareHomeNextDeliveryDate);

        if (window.PatientStatus != $("#hdnActivePatient").val()) {
            $jqGrid('#divPrescription').addClass("disable_div");
            if (!$('#hdnIsredirectFromApproval').val() == 1) {
                DisablePrescriptionButtons();
            }
        }
        if ((data.PatientId != 0 || data.PatientId != undefined) && (window.isPatientInfoEditable.toString().toUpperCase() != value.toString().toUpperCase())) {
            EnablePatientInfoButtons();
        }

        var PatientType = $("#ddlPatientType").val();

        //If Patient Type carehome then use default frequency
        if (jQuery.inArray(PatientType, NonCarehome) == -1) {
            window.FrequencyMultipleOf = $("#DeliveryFrequency").val();
            SetAdpMandatoryBasedonPatientType(false, carehomeValue);
        }
        //Set Correspondence Address     
        document.getElementById("chkSameAsDelivery").checked = data.CorrespondenceSameAsDelivery;
        if (data.CorrespondenceSameAsDelivery) {
            $("#divCorrespondenceAddress .patientinfo_form .ez-checkbox").addClass("ez-checked");
        }
        else {
            $("#divCorrespondenceAddress .patientinfo_form .ez-checkbox").removeClass("ez-checked");
            EnableCorrespondenceAddress(false);
        }

        $("#CorrespondenceCareHomeName").val(data.CorrespondenceCareHomeName);
        $("#CorrespondenceAddress1").val(data.CorrespondenceAddress1);
        $("#CorrespondenceAddress2").val(data.CorrespondenceAddress2);
        $("#CorrespondenceTownOrCity").val(data.CorrespondenceTownOrCity);
        if (data.DropDownSelectedvalue.CountySelectedValue == null) {
            $("#ddlCounty").val($('#hdnresddlCountyDefaultValue').val());
        }

        if (data.CorrespondenceCounty == null) {
            $("#ddlCorrespondenceCounty").val($('#hdnresddlCountyDefaultValue').val());
        }
        else {
            $("#ddlCorrespondenceCounty").val(data.CorrespondenceCounty);
        }
        
        $("#CorrespondencePostCode").val(data.CorrespondencePostCode);

        document.getElementById("chkIsDataProtected").checked = data.IsDataProtected;
        if (data.IsDataProtected) {
            $("#divDataProtected .ez-checkbox").addClass("ez-checked");
        }
        SetMandatoryBasedOnMarketingPreferences();

        $jqGrid('#hdnPatientSAPCustomerNumber').val(data['SAPCustomerNumber']);
        $jqGrid('#hdnPatientCareHomeSAPId').val(data['CareHomeSAPId']);
        SetCommonObjects();
    if (isCopyDetails == true) {
        //ReloadMedicalStaffGrid();
        }

    LoadPrescriptionGrid();

    LoadJQGridAlertNotes();
    if ($("#hdnSessionDoNotShow").val() == "false") {
        var noteType = $("#hdnNoteType").val();
        var noteTypeId = window.patientId;
        LoadAlertNotes(noteType, noteTypeId);
        }

    }

    function SetViewOnly(element) {
        $jqGrid("#" + element).addClass("btn_disable");
        $jqGrid("#" + element).attr('disabled', 'disabled');
        $jqGrid("#" + element).attr('readonly', 'true');
        DisableEditButton();
        DisableCancelButton();
        $("#hdnIsCarehomeRemoved").val("true")
    }
    // Set CareHome details selcted from CareHome Popup
    function SetCareHomeDetails(data) {
        PatientHasChanged();
        $("#Address1").val('');
        $("#Address2").val('');
        $("#TownOrCity").val('');
        $("#PostCode").val('');
        $("#PhoneNo").val('');
        $("#MobileNo").val('');
        $("#EmailAddress").val('');
        $("#MobileNo").val('');
        $("#CareHomeName").val('');
        $("#ddlCounty").val('');
        $("#Assignedcarehome").val('');
        $("#RoundId").val('');
        $("#DeliveryFrequency").val('');
        $("#hdnCarehomePatientNDD").val('');

        $("#Address1").val(data['Address1'].toUpperCase());
        $("#Address2").val(data['Address2'].toUpperCase());
        $("#TownOrCity").val(data['City'].toUpperCase());
        $("#PostCode").val(data['PostCode'].toUpperCase());
        $("#PhoneNo").val(data['PhoneNo']);
        $("#MobileNo").val(data['MobileNo']);
        $("#EmailAddress").val(data['EmailAddress']);
        $("#CareHomeName").val(data['CareHomeName'].toUpperCase());
        $("#ddlCounty").val(data['CountyCode']);
        $("#Assignedcarehome").val(data['CareHomeId']);

        var round = data['Round'] == undefined || data['Round'] == null ? '' : data['Round'];
        var roundId = data['RoundId'] == undefined || data['RoundId'] == null ? '' : data['RoundId'];
        var displayRound = "";

        $("#hdnRound").val(round);
        $("#hdnRoundId").val(roundId);
        if (round != "" || roundId != "") {
            displayRound = round.slice(-2) + "/" + roundId;
            if ($jqGrid("#hdnuserType").val().toUpperCase() == userType.toUpperCase()) {
                $jqGrid('#RoundId').attr('disabled', 'disabled');
            }
        }
        
        $("#RoundId").val(displayRound);
        $("#DeliveryFrequency").val(data['DeliveryFrequency']);
        $("#hdnCarehomePatientNDD").val(data['NextDeliveryDate']);
        $("#hdnSelectedCarehomeStatus").val(data['CareHomeStatus']);

        //Set patient NDD after change in carehome
        SetPatientNDD(true);
        window.FrequencyMultipleOf = $("#DeliveryFrequency").val();
        window.IsCarehomePatientType = true;
      //  SetCareHomePrescriptionFrequency(true);

        //Make Address Field Read only
        MakeAddressReadOnly();
        //Copy Delivery Address to Correspondence Address

        CopyDeliveryAddress();
    }

    function SetCareHomePrescriptionFrequency(IsCarehome) {

        var FrequencyMultipleOf = 0;
        if (window.FrequencyMultipleOf != null && window.FrequencyMultipleOf != undefined) {
            FrequencyMultipleOf = parseInt(window.FrequencyMultipleOf);
        }
        var patientTypeMinFrequency = window.PrescMinFrequency;
        var patientTypeMaxFrequency = window.PrescMaxFrequency;
        var defaultFrequency = window.DefaultFrequency;
        var currentFrequency = 0;
        var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
        var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');
        for (var i = 0; i < gridRowsprescription.length; i++) {
            var row = gridRowsprescription[i];
            var rowId = ids[i];

            if ($jqGrid(row["IsRemoved"]).val() == 'removed')
                continue;

            if (IsCarehome) {
                currentFrequency = FrequencyMultipleOf;
            }
            else {
                currentFrequency = defaultFrequency;
            }
            $jqGrid("#" + rowId + "_Frequency").val(currentFrequency);
        }
    }

    function ShowCareHomePopup() {
        if ($("#ddlCustomer").val() == "-1") {
            $jqGrid("#jqCareHomeResults").jqGrid('clearGridData');
            var dd = $jqGrid("#divCareHome").dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: true,
                title: $jqGrid('#hdnAddChangeCareHomeTitle').val()
            });
            dd.dialog('open');
        }
        else {
            var page = 0;
            var rows = 50;
            $jqGrid("#jqCareHomeResults").jqGrid('clearGridData');
            var customer_ID = $("#ddlCustomer").val();
            $jqGrid.ajax({

                url: '/Patient/GetCareHomeDetails',
                type: 'GET',
                cache: false,
                data: {
                    page: page, rows: rows, CustomerId: customer_ID
                },
                contentType: 'application/json',
                dataType: "json",
                async: false,
                success: function (responseData) {
                    $jqGrid("#jqCareHomeResults").jqGrid('clearGridData');
                    $jqGrid("#jqCareHomeResults").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
                },
                error: function (x, e) {
                        HandleAjaxError(x, e, jsfilename, "CheckPrescriptionProductExitForCustomers");
                        return false;
                }
                });
                var dd = $jqGrid("#divCareHome").dialog({
                        autoOpen: false,
                        modal: true,
                        closeOnEscape: false,
                        width: 'auto',
                        resizable: true
                        });
                dd.dialog('open');
        }
    }

    //Post code checker code has been placed inside Postcodechecker.js file
    // Since it will be a common function


    $jqGrid("#btnApprove").click(function ApprovePrescription() {
        window.btnApproveClicked = true;
        var authPIN = $jqGrid.trim($jqGrid('#txtPIN').val());
        if (authPIN == '' || authPIN == $jqGrid("#hdnEnterPINPlaceholder").val()) {
            $jqGrid("#prescriptionMessage").html($jqGrid("#hdnmsgEnterPIN").val());
            $jqGrid('#prescriptionMessage').show();
            return false;
        }
        else {
            $jqGrid.ajax({
                url: '/' + $jqGrid("#hdnAuthorizeControllername").val() + '/' + $jqGrid("#hdnActionNameGetPIN").val(),
                type: 'GET',
                cache: false,
                data: {
                    PIN: authPIN
                },
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result == true) {
                        SavePatientDetails();
                        if (isSaveSuccess == 'true') {
                            SaveAndApprove();
                        }
                        $jqGrid('#txtPIN').val('');
                    }
                    else {
                        window.btnApproveClicked = false;
                        $jqGrid("#prescriptionMessage").html($jqGrid("#hdnmsgWrongPIN").val());
                        $jqGrid('#prescriptionMessage').show();
                    }
                }

            });
        }
    });

    function SaveAndApprove() {

        var PatientId = $jqGrid("#hdnPatientId").val();
        var PatientName = $jqGrid("#hdnPatientName").val();
        var CustomerId = $jqGrid("#hdnCustomerId").val();
        var CustomerName = $jqGrid("#hdnCustomerName").val();
        var PrescriptionNote = $.trim($jqGrid('#txtPrescriptionNote').val());

        $jqGrid.ajax({
            url: '/' + $jqGrid("#hdnAuthorizeControllername").val() + '/' + $jqGrid("#hdnActionNameApprovePrescription").val(),
            type: 'POST',
            cache: false,
            data: {
                PatientId: PatientId, PrescriptionNote: PrescriptionNote
            },
            dataType: "json",
            success: function (data) {
                if (data == true) {

                    DisableSaveButton();
                    DisableEditButton();
                    DisableCancelButton();

                    $jqGrid('#divApprovalDialog').html($jqGrid("#hdnmsgPrescriptionApproved").val());
                    $jqGrid('#divApprovalDialog').dialog({
                        autoOpen: false,
                        modal: true,
                        closeOnEscape: false,
                        width: 'auto',
                        resizable: false,
                        title: $jqGrid("#hdnmsgPrescriptionApproved").val(),
                        buttons: [{
                            text: $jqGrid("#hdnGeneralOk").val().toString(),
                            "id": "hdnGeneralOk",
                            click: function () {
                                ClearNurseComments();
                                window.location.href = "/Authorize/Prescription";
                                $jqGrid('#divApprovalDialog').dialog('close');
                                $jqGrid.ajax({
                                    url: '/Authorize/GetPrescription',
                                    type: 'GET',
                                    cache: false,
                                    dataType: "json",
                                    data: {
                                        page: 1, rows: 50
                                    },
                                    success: function (data) {

                                    }
                                });
                            },
                        }]
                    });
                    $jqGrid('#divApprovalDialog').dialog('open');
                    $jqGrid('#txtPIN').val('');
                    $jqGrid('#prescriptionMessage').hide();
                    HideApprovalDiv();
                    $jqGrid.ajax({
                        url: '/' + $jqGrid("#hdnPatientController").val() + '/' + $jqGrid("#hdnActionClearCache").val(),
                        type: 'GET',
                        cache: false,
                        dataType: "json",
                        success: function (data) {
                            if (data == false)
                                alert("Session variables not cleared");
                        }
                    });
                }
                else {
                    $jqGrid('#divApprovalDialog').html($jqGrid("#hdnmsgPrescriptionNotApproved").val());
                    $jqGrid('#divApprovalDialog').dialog({
                        autoOpen: false,
                        modal: true,
                        width: 'auto',
                        resizable: false,
                        title: $jqGrid("#hdnmsgPrescriptionNotApproved").val(),
                        buttons: [{
                            text: $jqGrid("#hdnGeneralOk").val().toString(),
                            "id": "hdnGeneralOk",
                            click: function () {
                                $jqGrid('#divApprovalDialog').dialog('close');
                                DisablePatientInfoPage();

                                DisableSaveButton();
                                EnableEditButton();
                                EnablePatientInfoButtons();
                                window.btnApproveClicked = false;
                            },
                        }]
                    });
                    $jqGrid('#divApprovalDialog').dialog('open');
                    $jqGrid('#txtPIN').val('');
                    $jqGrid('#prescriptionMessage').hide();
                }
            }

                });
            }

    function CompareActualPPDwithMaxPPDRemoved(rowId) {
        var rowData = $jqGrid("#jqPatientPrescription").jqGrid('getRowData', rowId);
        var MaxPadPerDay = 0;
        var actualPPDValue = 0;
        var gridid = $jqGrid("#jqPatientPrescription"), ids = gridid.jqGrid('getDataIDs'), i, l = ids.length;
        for (var i = 1; i <= l; i++) {
            if (!$jqGrid("#" + i + "_IsRemoved").is(":checked")) {
                rowActualPPDValue = parseFloat($jqGrid("#" + i + "_ActualPPD").val());
                actualPPDValue += parseFloat(rowActualPPDValue);
                actualPPDValue = parseFloat(actualPPDValue.toFixed(2));
            }
        }
        var CustomerParamets = window.CustomerParamets;
        if (CustomerParamets != null) {
            MaxPadPerDay = CustomerParamets[0].MaxPadPerDay;

            if (MaxPadPerDay != 0) {
                if (parseFloat(MaxPadPerDay) < actualPPDValue) {
                    for (var i = 1; i <= l; i++) {
                        if (!$jqGrid("#" + i + "_IsRemoved").is(":checked")) {
                            $jqGrid('#jqPatientPrescription').jqGrid('setCell', i, 'PrescriptionAuthorizationApprovalFlag', 'true');
                        }
                    }
                }
                else if (!(window.IsPrescriptionApprovalRequired)) {
                    for (var i = 1; i <= l; i++) {
                        if (!$jqGrid("#" + i + "_IsRemoved").is(":checked") && $jqGrid('#jqPatientPrescription').jqGrid('getCell', i, 'ProductApprovalFlag').toString().toUpperCase() != valTrue) {
                            $jqGrid('#jqPatientPrescription').jqGrid('setCell', i, 'PrescriptionAuthorizationApprovalFlag', 'false');
                        }
                    }
                }
            }
        }
    }

    function DisableChangeNDD(isDisable) {
        if (isDisable == true) {
            $jqGrid('#changeNDDPrescription').addClass("btn_disable");
            $jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
            $jqGrid('#btnPrescriptionCalculate').addClass("btn_disable");
            $jqGrid('#btnPrescriptionCalculate').attr('disabled', 'disabled');
        }
        else {
            $jqGrid('#btnPrescriptionCalculate').removeClass("btn_disable");
            $jqGrid('#btnPrescriptionCalculate').removeAttr('disabled', 'disabled');
            $.ajax({
                url: '/Order/IsNddAllowed',
                contentType: "application/json",
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data == true && isDisable == false) {
                        $jqGrid('#changeNDDPrescription').removeClass("btn_disable");
                        $jqGrid('#changeNDDPrescription').removeAttr("disabled");

                    }
                    else {
                        $jqGrid('#changeNDDPrescription').addClass("btn_disable");
                        $jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
                    }
                }
            });
        }
    }

    function SetPatientNDD(RecalculateNDD) {
        RecalcuateNDDForProCare = false;
        NotRemovedCount = 0;
        var PatientType = $("#ddlPatientType").val();
        if (jQuery.inArray(PatientType, NonCarehome) != -1) {
            var ExistingPatientNDD = $jqGrid("#hdnNextDeliverydate").val();
            var newNDD = "";
            var addedDays = "";
            var adate = new Date();
            var currentdateDDMM = (adate.getMonth() + 1) + "/" + adate.getDate() + "/" + adate.getFullYear() + "";
            //alert("getting date");
            $jqGrid.ajax({
                url: '/Patient/AddWorkingDays',
                type: 'GET',
                cache: false,
                async: false,
                data: {
                    date: currentdateDDMM, noOfDays: leadTime
                },
                success: function (response) {
                    addedDays = response.substr(0, 10);
                }
            });

            if (ExistingPatientNDD != "") {
                var Existingdate = new Date(currentdateDDMM);
                var calculatedDate = new Date(addedDays);
                //alert("new logic2");
                if (Existingdate < calculatedDate) {
                    if (RecalculateNDD) {
                        newNDD = ConvertDateToDDMMYYYY(addedDays);
                    }
                    else {
                        newNDD = ExistingPatientNDD;
                    }
                }
                else {
                    newNDD = ExistingPatientNDD;
                }
            }
            $jqGrid("#hdnNextDeliverydate").val(newNDD);

            if (parseInt(PatientType) != existingdbPatientType && $jqGrid("#PatientId").text() != "" && ExistingPatientNDD != "") {

                if (jQuery.inArray(PatientType, Selfcare) >= 0) {
                    IsForceDisplayNDD = true;
                    $jqGrid("#hdnNextDeliverydate").val(window.existingdbndd);
                }

                if (jQuery.inArray(PatientType, Procare) >= 0)
                {
                    IsForceDisplayNDD = true;
                    RecalcuateNDDForProCare = true;
                    var roundAndRoundId = $("#RoundId").val().split('/');
                    var round = $("#hdnDefaultRound").val() + roundAndRoundId[0];
                    var roundId = roundAndRoundId[1].toUpperCase();

                    $jqGrid.ajax({
                        url: $jqGrid("#GetNDDForProcarePatient").val(),
                        type: 'GET',
                        cache: false,
                        data: { customerId: $("#ddlCustomer").val(), round: round, roundId: roundId, postCode: $('#PostCode').val(), pateintNDD: DateTextToMMDDYYYY(window.existingdbndd) },
                        async: false,
                        success: function (response) {
                            addedDays = response.substr(0, 10);
                        },
                        error: function (x, e) {
                            HandleAjaxError(x, e, jsfilename, "SetPatientNDD");
                        }
                    });
                    var calculatedDate = new Date(addedDays);
                    $jqGrid("#hdnNextDeliverydate").val(ConvertDateToDDMMYYYY(calculatedDate));
                }
            }
        }

        var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
        var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');
        for (var i = 0; i < gridRowsprescription.length; i++) {
            var row = gridRowsprescription[i];
            var rowId = ids[i];

            if ($jqGrid(row["IsRemoved"]).val() == 'removed')
                continue;
            NotRemovedCount = NotRemovedCount + 1;
            //If valid active prescription available and PatientType changed from carehome to noncarehome or vise versa then recalculate the NDD for all prescription product
            if (RecalculateNDD) {
                DisplayNDD(rowId);
            }
        }
        //Set the Change NDD buttun disabled if all product are removed    
        if (NotRemovedCount == 0) {

            DisableChangeNDD(true);
            //$jqGrid('#changeNDDPrescription').addClass('btn_disable');
            //$jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');

            //If non care home then remove NDD without saving to hidden field else save in hidded field        
            PatientNDD = "";
            $jqGrid('#DeliveryDate').text('');
            $jqGrid("#hdnNextDeliverydate").val('');
        }
        IsForceDisplayNDD = false;
    }

//TODO : This function requires clean up
    function DisplayNDD(rowId) {
        var PatientType = $("#ddlPatientType").val();

        if (IsForceDisplayNDD)
        {
            var ExistingPatientNDD = $jqGrid("#hdnNextDeliverydate").val();
            PatientNDD = CommonScriptObj.DateTextToMMDDYYYY(ExistingPatientNDD);
            window.ChangeNDD = ConvertDateToDDMMYYYY(PatientNDD);
            if (jQuery.inArray(PatientType, Selfcare) >= 0) {
                $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val($jqGrid('#jqPatientPrescription #' + rowId + '_hdnChangeNDD').val());
            }
            else {
                $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(window.ChangeNDD);
            }

            $jqGrid('#DeliveryDate').text(window.ChangeNDD);
            SetChangeButtonEnable();
        }
        else if (window.isFromCarehomeChangePopup) {
            window.ChangeNDD = $jqGrid('#hdnCarehomePatientNDD').val();
            $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(window.ChangeNDD);
            $jqGrid('#DeliveryDate').text(window.ChangeNDD);
            SetChangeButtonEnable();
        }
        else if (jQuery.inArray(PatientType, NonCarehome) == -1 && ($jqGrid("#PatientId").text() == "" || window.existingdbndd == "")) {
            window.ChangeNDD = $jqGrid('#hdnCarehomePatientNDD').val();
            $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(window.ChangeNDD);
            $jqGrid('#DeliveryDate').text(window.ChangeNDD);
            SetChangeButtonEnable();
        }
        else {
            //Patient is new Patient then calculate NDD logic else Use Existing Patient NDD
            var LDD = $jqGrid("#hdnDeliverydate").val();
            var ExistingPatientNDD = $jqGrid("#hdnNextDeliverydate").val();

            if (LDD != undefined && LDD != "" && ExistingPatientNDD != "") {
                PatientNDD = CommonScriptObj.DateTextToMMDDYYYY(ExistingPatientNDD);
                window.ChangeNDD = ConvertDateToDDMMYYYY(PatientNDD);
                $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(window.ChangeNDD);

                $jqGrid('#DeliveryDate').text(window.ChangeNDD);
                SetChangeButtonEnable();
            }
            else {
                //For new patient if there is atleast one valid prescription then take NDD from PatientNDD varriable else send to controller for calculation
                if (PatientNDD != "" && PatientNDD != undefined) {
                    window.ChangeNDD = ConvertDateToDDMMYYYY(PatientNDD);
                    $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(window.ChangeNDD);
                    $jqGrid('#DeliveryDate').text(window.ChangeNDD);
                    SetChangeButtonEnable();
                }
                else {
                    var roundAndRoundId = $("#RoundId").val().split('/');
                    var round = $("#hdnDefaultRound").val() + roundAndRoundId[0];
                    var roundId = roundAndRoundId[1].toUpperCase();

                    $jqGrid.ajax({
                        url: '/Patient/GetPatientNDD',
                        type: 'GET',
                        cache: false,
                        data: { isPatientTypeChanged: window.isPatientTypeChanged, round: round, roundId: roundId, postCode: $('#PostCode').val(), reCalculate: false },
                        async: false,
                        success: function (response) {
                            window.isPatientTypeChanged = false;
                            if (response != null || response != undefined) {
                                PatientNDD = response.substr(0, 10);
                                window.ChangeNDD = ConvertDateToDDMMYYYY(PatientNDD);
                                $jqGrid('#jqPatientPrescription #' + rowId + '_ChangeNDD').val('').val(window.ChangeNDD);
                                $jqGrid('#DeliveryDate').text(window.ChangeNDD);
                                SetChangeButtonEnable();
                            }
                        }
                    });
                }
            }
        }
    }
    function SetChangeButtonEnable() {
        //Set the Change NDD buttun enablled if there is product with valid NDD
        if (!$jqGrid('#divPrescription').hasClass('disable_div')) {
            DisableChangeNDD(false);
        }
    }

    function clearNDDRoundID() {
        $jqGrid('#DeliveryDate').val('');
        $jqGrid('#RoundId').val('');
        $jqGrid('#DeliveryFrequency').val('0');
    }

    function CalculatePrescriptionValues() {
        if (!validatePrescriptionFrequency(false)) {
            return false;
        }

        if (window.isPrescriptionEditable.toString().toUpperCase() != value.toUpperCase() && window.RoleName == $jqGrid('#hdnNurseRoleName').val()) {
            window.PrescriptionEditedbyNurse = true;
            EnableNurseComments();
        }
        var isCalculated = Calculate();
        if (isCalculated) {
            PatientHasChanged();
            CompareActualPPDwithMaxPPD('btnPrescriptionCalculate');
        }
        $jqGrid('#btnPrescriptionCalculate').focus();
    }

    function validatePrescriptionFrequency(isFromSave) {       
        var errorProductIds = "";
        var isCarehome = false;
        if ($jqGrid('#hdIsCarehomePatient').val() == "true") {
            isCarehome = true;
            if ($("#CareHomeName").val() == '') {
                if (!isFromSave) {
                    $jqGrid('#divPrescValidationMsgText').html($("#hdnreserrormsgCarehome").val());
                    $jqGrid('#divPrescriptionValidation').show();
                    return false;
                }
            }
        }
        
        var FrequencyMultipleOf = 0;
        if (window.FrequencyMultipleOf != null && window.FrequencyMultipleOf != undefined) {
            FrequencyMultipleOf = parseInt(window.FrequencyMultipleOf);
        }
        var patientTypeMinFrequency = window.PrescMinFrequency;
        var patientTypeMaxFrequency = window.PrescMaxFrequency;
        var defaultFrequency = window.DefaultFrequency;
        var currentFrequency = 0;
        var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
        var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');      
        
        for (var i = 0; i < gridRowsprescription.length; i++) {
            var row = gridRowsprescription[i];
            var rowId = ids[i];

            if ($jqGrid(row["IsRemoved"]).val() == 'removed')
                continue;

            if (isCarehome) {
                currentFrequency = FrequencyMultipleOf;
            }
            else {
                currentFrequency = defaultFrequency;
            }
            var prescFrequency = $jqGrid("#" + rowId + "_Frequency").val();
            var remainder = parseInt(prescFrequency) % parseInt(FrequencyMultipleOf);
            if (isCarehome) {
                if (parseInt(prescFrequency) >= parseInt(currentFrequency) && parseInt(remainder) == 0) {
                    $jqGrid('#jqPatientPrescription').setCell(rowId, 'Frequency', '', { 'background': 'none', 'color': '#000' });
                    continue;
                }
                else {
                    errorProductIds += row["ProductDisplayId"] + ", ";                 
                    $jqGrid('#jqPatientPrescription').setCell(rowId, 'Frequency', '', { 'background': 'Red', 'color': '#000' });
                }
            }
            else {
                
                if (parseInt(prescFrequency) == parseInt(currentFrequency) ||
                    (parseInt(prescFrequency) >= parseInt(patientTypeMinFrequency)
                        && parseInt(remainder) == 0
                        && parseInt(prescFrequency) <= parseInt(patientTypeMaxFrequency)))
                {
                    $jqGrid('#jqPatientPrescription').setCell(rowId, 'Frequency', '', { 'background': 'none', 'color': '#000' });
                    continue;
                }
                else {
                    errorProductIds += row["ProductDisplayId"] + ", ";                  
                    $jqGrid('#jqPatientPrescription').setCell(rowId, 'Frequency', '', { 'background': 'Red', 'color': '#000' });
                }
            }
        }
        var errorMessage = "";
        if (errorProductIds != "") {
            var errorMessage = "Please enter new Frequency for Product Id(s)  " + errorProductIds.slice(0, -1).slice(0,-1);
            if (isCarehome) {
                errorMessage += ".<br/>New Frequency should be greater than or equal to " + currentFrequency + " in steps of " + FrequencyMultipleOf + "\n";
            }
            else {
                errorMessage += ".<br/>New Frequency should be between " + patientTypeMinFrequency + " and " + patientTypeMaxFrequency + " in steps of " + FrequencyMultipleOf;
            }

            $jqGrid('#divPrescValidationMsgText').html(errorMessage);
            $jqGrid('#divPrescriptionValidation').show();
            return false
        }
        return true;
    }

    function Calculate(isValidate) {
        var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
        var gridid = $jqGrid("#jqPatientPrescription"), ids = gridid.jqGrid('getDataIDs'), rowId, rowCount = ids.length;
        for (var i = 0; i < gridRowsprescription.length; i++) {
            var rowId = ids[i];
            var row = gridRowsprescription[i];

            if ($jqGrid(row["IsRemoved"]).val() == 'removed')
                continue;

            //DQ1: ($jqGrid("#" + rowId + "_DQ1").val() != "" && $jqGrid("#" + rowId + "_DQ1").val() != undefined) ? $jqGrid("#" + rowId + "_DQ1").val() : row["DQ1"],
            //DQ2: ($jqGrid("#" + rowId + "_DQ2").val() != "" && $jqGrid("#" + rowId + "_DQ2").val() != undefined) ? $jqGrid("#" + rowId + "_DQ2").val() : row["DQ2"],
            //DQ3: ($jqGrid("#" + rowId + "_DQ3").val() != "" && $jqGrid("#" + rowId + "_DQ3").val() != undefined) ? $jqGrid("#" + rowId + "_DQ3").val() : row["DQ3"],
            //DQ4: ($jqGrid("#" + rowId + "_DQ4").val() != "" && $jqGrid("#" + rowId + "_DQ4").val() != undefined) ? $jqGrid("#" + rowId + "_DQ4").val() : row["DQ4"],

            var isValidAssessedPadsPerDay = $jqGrid("#" + rowId + "_AssessedPadsPerDay").val();
            if (isValidAssessedPadsPerDay == undefined) {
                isValidAssessedPadsPerDay = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'AssessedPadsPerDay');
            }

            if ((isValidAssessedPadsPerDay == "" || isValidAssessedPadsPerDay == 0 || isValidAssessedPadsPerDay == "0" || isValidAssessedPadsPerDay == undefined) &&
                !($jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) && !($jqGrid("#" + rowId + "_IsRemoved").is(':checked'))) {
                $jqGrid("#divAssessedPadError").text('Please enter Assessed PPD for Product - ' + row["ProductDisplayId"]);
                $jqGrid('#divAssessedPadError').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                        "id": "hdnGeneralOk",
                        click: function () {
                            $jqGrid('#divAssessedPadError').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divAssessedPadError').dialog('open');
                return false;
            }
        }



        for (var rowId = 1; rowId <= rowCount; rowId++) {
            if (!$jqGrid("#" + rowId + "_IsRemoved").is(":checked")) {

                var Freq = $jqGrid("#" + rowId + "_Frequency").val();
                if (Freq == undefined) {
                    Freq = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'Frequency');
                }

                var PackSize = $jqGrid("#" + rowId + "_PackSize").val();
                if (PackSize == undefined) {
                    PackSize = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'PackSize');
                }
                var PPD = $jqGrid("#" + rowId + "_AssessedPadsPerDay").val();
                if (PPD == undefined) {
                    PPD = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'AssessedPadsPerDay');
                }

                var DQ1ToDQ4 = 0.0;
                var ActPPD = 0.0;
                var Totalpieces = 0.0;
                var totalpacks = 0.0;
                var roundedPPD = PPD;
                if (roundedPPD == "" || roundedPPD == "0") {
                    roundedPPD = parseInt(1);
                }
                var totalQuantity = 0.0;
                totalQuantity = parseFloat(Freq) * 7 * 4 * roundedPPD / parseFloat(PackSize);
                DQ1ToDQ4 = (Math.ceil(totalQuantity)) / 4;


                var DQ1 = $jqGrid("#" + rowId + "_DQ1").val();
                if (DQ1 == undefined) {
                    DQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ1');
                }
                if (DQ1 == '') {
                    DQ1 = Math.ceil(parseFloat(DQ1ToDQ4));
                }

                var DQ2 = $jqGrid("#" + rowId + "_DQ2").val();
                if (DQ2 == undefined) {
                    DQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ2');
                }
                if (DQ2 == '') {
                    DQ2 = Math.floor(parseFloat(DQ1ToDQ4));
                }

                var DQ3 = $jqGrid("#" + rowId + "_DQ3").val();
                if (DQ3 == undefined) {
                    DQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ3');
                }
                if (DQ3 == '') {
                    DQ3 = Math.floor(parseFloat(DQ1ToDQ4));
                }

                var DQ4 = $jqGrid("#" + rowId + "_DQ4").val();
                if (DQ4 == undefined) {
                    DQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ4');
                }
                if (DQ4 == '') {
                    DQ4 = Math.floor(parseFloat(DQ1ToDQ4));
                }

                if (!$jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) {
                    DQ1 = Math.ceil(parseFloat(DQ1ToDQ4));
                    DQ2 = Math.floor(parseFloat(DQ1ToDQ4));
                    DQ3 = Math.floor(parseFloat(DQ1ToDQ4));
                    DQ4 = Math.floor(parseFloat(DQ1ToDQ4));
                }


                var iDQ1 = parseFloat(DQ1);
                var iDQ2 = parseFloat(DQ2);
                var iDQ3 = parseFloat(DQ3);
                var iDQ4 = parseFloat(DQ4);

                if (PPD == "" || PPD == "0" || $jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) {
                    var totalDQ = iDQ1 + iDQ2 + iDQ3 + iDQ4;
                    var total = totalDQ * parseFloat(PackSize);
                    var totalDays = 4 * (parseFloat(Freq) * 7);
                    ActPPD = total / totalDays;
                }
                else {

                    while (ActPPD < PPD) {
                        var totalDQ = iDQ1 + iDQ2 + iDQ3 + iDQ4;
                        var total = totalDQ * parseFloat(PackSize);
                        var totalDays = 4 * (parseFloat(Freq) * 7);
                        ActPPD = total / totalDays;
                        if (ActPPD < PPD) {
                            iDQ2 = iDQ2 + parseFloat(1);
                        }
                    };
                    var iDQTotal = iDQ1 + iDQ2 + iDQ3 + iDQ4;
                    if (iDQTotal != 0) {
                        DQ1 = Math.floor(iDQTotal / 4);
                        var reminder = iDQTotal - (DQ1 * 4);
                        if (reminder > 0) {
                            DQ1 = DQ1 + 1;
                            reminder = reminder - 1;
                            if (reminder > 0) {
                                DQ2 = DQ2 + 1;
                                reminder = reminder - 1;
                                if (reminder > 0) {
                                    DQ3 = DQ3 + 1;
                                }
                            }
                        }
                    }
                }

                $jqGrid("#jqPatientPrescription").jqGrid('saveRow', rowId, false, 'clientArray');

                $jqGrid('#jqPatientPrescription').jqGrid('restoreRow', rowId);
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ1', DQ1);
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ2', DQ2);
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ3', DQ3);
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ4', DQ4);
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ActualPPD', ActPPD.toFixed(2));
                if (PPD == "") {
                    $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'AssessedPadsPerDay', undefined);
                }
                else {
                    $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'AssessedPadsPerDay', PPD);
                }

                if (!$jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) {
                    var cmDQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ1');
                    var cmDQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ2');
                    var cmDQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ3');
                    var cmDQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ4');
                    var cmAssessedPPD = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'AssessedPadsPerDay');

                    cmDQ1.editable = false;
                    cmDQ2.editable = false;
                    cmDQ3.editable = false;
                    cmDQ4.editable = false;
                    cmAssessedPPD.editable = true;
                    $jqGrid('#jqPatientPrescription').jqGrid('editRow', rowId, true, null, null, 'clientArray');
                    //cmDQ1.editable = true;
                    //cmDQ2.editable = true;
                    //cmDQ3.editable = true;
                    //cmDQ4.editable = true;
                }
                else if (window.gridid != "jqCommunityOrders" && window.gridid != "jqOneOffOrders") {
                    var cmDQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ1');
                    var cmDQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ2');
                    var cmDQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ3');
                    var cmDQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ4');
                    var cmAssessedPPD = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'AssessedPadsPerDay');

                    cmDQ1.editable = true;
                    cmDQ2.editable = true;
                    cmDQ3.editable = true;
                    cmDQ4.editable = true;
                    cmAssessedPPD.editable = false;
                    $jqGrid('#jqPatientPrescription').jqGrid('editRow', rowId, true, null, null, 'clientArray');
                    //cmDQ1.editable = false;
                    //cmDQ2.editable = false;
                    //cmDQ3.editable = false;
                    //cmDQ4.editable = false;
                }
                Totalpieces = PPD * ((12 * 7) * 4);
                totalpacks = Totalpieces / PackSize;
            }
        }
        return true;
    }


    function CheckPrescriptionAuthorizationApproval(e) {
        var row = $jqGrid(e.target).closest('tr.jqgrow');
        var rowId = row.attr('id');
        var rowData = $jqGrid("#jqPatientPrescription").jqGrid('getRowData', rowId);
        var IsCustomerApprovalReq = false;
        var IsPrescriptionApprovalRequired = false;
        var AssessedPadsPerDay = e.target.value;
        window.AssessedPadsPerDay = AssessedPadsPerDay;
        var CustomerParamets = window.CustomerParamets;
        IsPrescriptionApprovalRequired = window.IsPrescriptionApprovalRequired;
        if (CustomerParamets != null) {
            IsCustomerApprovalReq = CustomerParamets[0].IsCustomerApprovalReq;
            if (IsPrescriptionApprovalRequired == true) {
                $jqGrid("#" + rowId + "_RowStatus").val('');
                $jqGrid("#" + rowId + "_RowStatus").val('Edited');

                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'PrescriptionAuthorizationApprovalFlag', 'true');
            }
            else {
                if (rowData['ProductApprovalFlag'].toString().toUpperCase() != valTrue) {
                    $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'PrescriptionAuthorizationApprovalFlag', 'false');
                }
            }
        }
    }

    function CheckNurseRoleLogin() {
        if (window.isPrescriptionEditable.toString().toUpperCase() != value.toUpperCase() && window.RoleName == $jqGrid('#hdnNurseRoleName').val()) {
            window.PrescriptionEditedbyNurse = true;
            EnableNurseComments();
        }
    }

    function CompareActualPPDwithMaxPPD(btnId) {
        var MaxPadPerDay = 0;
        var actualPPDValue = 0;
        var gridid = $jqGrid("#jqPatientPrescription"), ids = gridid.jqGrid('getDataIDs'), rowId, rowCount = ids.length;
        for (var rowId = 1; rowId <= rowCount; rowId++) {
            if (!$jqGrid("#" + rowId + "_IsRemoved").is(":checked")) {
                rowActualPPDValue = parseFloat($jqGrid("#" + rowId + "_ActualPPD").val());
                actualPPDValue += parseFloat(rowActualPPDValue);
                actualPPDValue = parseFloat(actualPPDValue.toFixed(2));
            }
        }
        var CustomerParamets = window.CustomerParamets;
        if (CustomerParamets != null) {
            MaxPadPerDay = CustomerParamets[0].MaxPadPerDay;        
            if (MaxPadPerDay != undefined && MaxPadPerDay != null && MaxPadPerDay != 0) {
                if (parseFloat(MaxPadPerDay) < actualPPDValue) {
                    for (var rowId = 1; rowId <= rowCount; rowId++) {
                        if (!$jqGrid("#" + rowId + "_IsRemoved").is(":checked")) {
                            $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'PrescriptionAuthorizationApprovalFlag', 'true');
                        }
                    }
                }
                else if (!(window.IsPrescriptionApprovalRequired)) {
                    for (var rowId = 1; rowId <= rowCount; rowId++) {
                        if (!$jqGrid("#" + rowId + "_IsRemoved").is(":checked") && $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ProductApprovalFlag').toString().toUpperCase() != valTrue) {
                            $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'PrescriptionAuthorizationApprovalFlag', 'false');
                        }
                    }
                }
                if (actualPPDValue > parseFloat(MaxPadPerDay)) {
                    if (btnId == "btnPrescriptionCalculate") {
                        $jqGrid('#divMaxPadPerDayError').dialog({
                            autoOpen: false,
                            modal: true,
                            closeOnEscape: false,
                            width: 'auto',
                            resizable: false,
                            buttons: {
                                "Ok": function () {
                                    $jqGrid('#divMaxPadPerDayError').dialog('close');
                                }
                            }
                        });
                        $jqGrid('#divMaxPadPerDayError').html('');
                        $jqGrid('#divMaxPadPerDayError').append($jqGrid("#hdnErrorMaxPerDay").val() + ' ' + MaxPadPerDay);
                        $jqGrid('#divMaxPadPerDayError').dialog('open');
                    }
                    else if (btnId == "btnSave" || btnId == "btnDetailSave") {
                        ShowPPDAlertBeforeSave = true;
                        MaxPPDValue = MaxPadPerDay;
                    }
                }
                else {
                    ShowPPDAlertBeforeSave = false;
                }
            }
        }
    }


    function MakeDQ1ToDQ2Editable(rowId) {
        var isOverriddenFlagChecked = $jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked');
        if ($jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked') && window.gridid != "jqCommunityOrders" && window.gridid != "jqOneOffOrders") {
            $jqGrid("#" + rowId + "_" + '_DQ1', event.target).focus(); //added by jitendra to keep the focus on grid row
        }
        else {
            var DQ1 = $jqGrid("#" + rowId + "_DQ1").val();
            if (DQ1 == undefined) {
                DQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ1');
            }
            var DQ2 = $jqGrid("#" + rowId + "_DQ2").val();
            if (DQ2 == undefined) {
                DQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ2');
            }
            var DQ3 = $jqGrid("#" + rowId + "_DQ3").val();
            if (DQ3 == undefined) {
                DQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ3');
            }
            var DQ4 = $jqGrid("#" + rowId + "_DQ4").val();
            if (DQ4 == undefined) {
                DQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ4');
            }
        }

        var actualPPD = $jqGrid("#" + rowId + "_ActualPPD").val();
        if (actualPPD == undefined) {
            actualPPD = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ActualPPD');
        }

        var assessedPadsPerDay = $jqGrid("#" + rowId + "_AssessedPadsPerDay").val();
        if (assessedPadsPerDay == undefined) {
            assessedPadsPerDay = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'AssessedPadsPerDay');
        }

        var frequency = $jqGrid("#" + rowId + "_Frequency").val();
        if (frequency == undefined) {
            frequency = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'Frequency');
        }

        var validFromDate = $jqGrid("#" + rowId + "_ValidFromDate").val();
        if (validFromDate == undefined) {
            validFromDate = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ValidFromDate');
        }

        var validToDate = $jqGrid("#" + rowId + "_ValidToDate").val();
        if (validToDate == undefined) {
            validToDate = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ValidToDate');
        }

        var productDescription = $jqGrid("#" + rowId + "_Description").val();
        if (productDescription == undefined) {
            productDescription = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'ProductName');
        }

        window.AssessedPadsPerDay = assessedPadsPerDay;
        window.ActualPPD = actualPPD;
        window.Frequency = frequency;
        window.ValidFromDate = validFromDate;
        window.ValidToDate = validToDate;
        window.ProductDescription = productDescription;

        var gridAssessedPPD = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'AssessedPadsPerDay');
        gridAssessedPPD.editable = true;

        $jqGrid("#jqPatientPrescription").jqGrid('saveRow', rowId, false, 'clientArray');
        $jqGrid('#jqPatientPrescription').jqGrid('restoreRow', rowId);
        if ($jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked') && window.gridid != "jqCommunityOrders" && window.gridid != "jqOneOffOrders") {
            $jqGrid('#jqPatientPrescription').setColProp('DQ1', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ2', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ3', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ4', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('AssessedPadsPerDay', { editable: false });
            $jqGrid('#jqPatientPrescription').jqGrid('editRow', rowId, true);
        }
        else {

            if (window.gridid == "jqCommunityOrders" || window.gridid == "jqOneOffOrders") {
                $jqGrid('#jqPatientPrescription').setColProp('AssessedPadsPerDay', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('IsOverriddenFlag', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('ActualPPD', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('Frequency', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('ValidFromDate', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('ValidToDate', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('DeliveryDate', { editable: false });
                $jqGrid('#jqPatientPrescription').setColProp('ChangeNDD', { editable: false });
            }

            $jqGrid('#jqPatientPrescription').setCell(rowId, 'DQ1', '', { editable: '0' });
            $jqGrid('#jqPatientPrescription').setCell(rowId, 'DQ2', '', { editable: '0' });
            $jqGrid('#jqPatientPrescription').setCell(rowId, 'DQ3', '', { editable: '0' });
            $jqGrid('#jqPatientPrescription').setCell(rowId, 'DQ4', '', { editable: '0' });
        }

        if (window.AssessedPadsPerDay != undefined) {
            if (isOverriddenFlagChecked)
                $jqGrid('#jqPatientPrescription #' + rowId + '_AssessedPadsPerDay').val('').val(window.AssessedPadsPerDay);
            else
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'AssessedPadsPerDay', window.AssessedPadsPerDay);
        }
        if (window.Frequency != undefined) {
            if (isOverriddenFlagChecked)
                $jqGrid('#jqPatientPrescription #' + rowId + '_Frequency').val('').val(window.Frequency);
            else
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'Frequency', window.Frequency);
        }
        if (window.ValidFromDate != undefined) {
            if (isOverriddenFlagChecked)
                $jqGrid('#jqPatientPrescription #' + rowId + '_ValidFromDate').val('').val(window.ValidFromDate);
            else
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ValidFromDate', window.ValidFromDate);
        }
        if (window.ValidToDate != undefined) {
            if (isOverriddenFlagChecked)
                $jqGrid('#jqPatientPrescription #' + rowId + '_ValidToDate').val('').val(window.ValidToDate);
            else
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ValidToDate', window.ValidToDate);
        }
        if (window.ActualPPD != undefined) {
            if (isOverriddenFlagChecked)
                $jqGrid('#jqPatientPrescription #' + rowId + '_ActualPPD').val('').val(window.ActualPPD);
            else
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ActualPPD', window.ActualPPD);
        }
        if (productDescription != undefined) {
            if (isOverriddenFlagChecked)
                $jqGrid('#jqPatientPrescription #' + rowId + '_Description').val('').val(window.ProductDescription);
            else
                $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ProductName', window.ProductDescription);
        }

        if (!$jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked') && window.gridid != "jqCommunityOrders" && window.gridid != "jqOneOffOrders") {
            PrescriptionCalculationForRow(rowId);
        }
    }

    // DO NOT DELETE COMMENTED CODE. THIS IS REQUIRED FOR VARIFICATION IN CASE OF ANY ISSUE WITH CALCULATE BUTTON -Siddhesh-25/12/2015
    function PrescriptionCalculationForRow(rowId) {
        var rowData = $jqGrid("#jqPatientPrescription").jqGrid('getRowData', rowId);

        var Freq = $jqGrid("#" + rowId + "_Frequency").val();
        if (Freq == undefined) {
            Freq = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'Frequency');
        }

        var PackSize = $jqGrid("#" + rowId + "_PackSize").val();
        if (PackSize == undefined) {
            PackSize = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'PackSize');
        }
        var PPD = $jqGrid("#" + rowId + "_AssessedPadsPerDay").val();
        if (PPD == undefined) {
            PPD = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'AssessedPadsPerDay');
        }

        //var DQ1ToDQ4 = 0.0;
        //var ActPPD = 0.0;
        //var Totalpieces = 0.0;
        //var totalpacks = 0.0;
        //var roundedPPD = PPD;
        //if (roundedPPD == "" || roundedPPD == "0") {
        //    roundedPPD = parseInt(1);
        //}
        //var totalQuantity = 0.0;
        //totalQuantity = parseFloat(Freq) * 7 * 4 * roundedPPD / parseFloat(PackSize);
        //DQ1ToDQ4 = (Math.ceil(totalQuantity)) / 4;


        var DQ1 = $jqGrid("#" + rowId + "_DQ1").val();
        if (DQ1 == undefined) {
            DQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ1');
        }
        //if (DQ1 == '') {
        //    DQ1 = Math.ceil(parseFloat(DQ1ToDQ4));
        //}

        var DQ2 = $jqGrid("#" + rowId + "_DQ2").val();
        if (DQ2 == undefined) {
            DQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ2');
        }
        //if (DQ2 == '') {
        //    DQ2 = Math.floor(parseFloat(DQ1ToDQ4));
        //}

        var DQ3 = $jqGrid("#" + rowId + "_DQ3").val();
        if (DQ3 == undefined) {
            DQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ3');
        }
        //if (DQ3 == '') {
        //    DQ3 = Math.floor(parseFloat(DQ1ToDQ4));
        //}

        var DQ4 = $jqGrid("#" + rowId + "_DQ4").val();
        if (DQ4 == undefined) {
            DQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getCell', rowId, 'DQ4');
        }
        //if (DQ4 == '') {
        //    DQ4 = Math.floor(parseFloat(DQ1ToDQ4));
        //}

        //if (!$jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) {
        //    DQ1 = Math.ceil(parseFloat(DQ1ToDQ4));
        //    DQ2 = Math.floor(parseFloat(DQ1ToDQ4));
        //    DQ3 = Math.floor(parseFloat(DQ1ToDQ4));
        //    DQ4 = Math.floor(parseFloat(DQ1ToDQ4));
        //}


        //var iDQ1 = parseFloat(DQ1);
        //var iDQ2 = parseFloat(DQ2);
        //var iDQ3 = parseFloat(DQ3);
        //var iDQ4 = parseFloat(DQ4);

        //if (PPD == "" || PPD == "0" || $jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) {
        //    var totalDQ = iDQ1 + iDQ2 + iDQ3 + iDQ4;
        //    var total = totalDQ * parseFloat(PackSize);
        //    var totalDays = 4 * (parseFloat(Freq) * 7);
        //    ActPPD = total / totalDays;
        //}
        //else {

        //    while (ActPPD < PPD) {
        //        var totalDQ = iDQ1 + iDQ2 + iDQ3 + iDQ4;
        //        var total = totalDQ * parseFloat(PackSize);
        //        var totalDays = 4 * (parseFloat(Freq) * 7);
        //        ActPPD = total / totalDays;
        //        if (ActPPD < PPD) {
        //            iDQ2 = iDQ2 + parseFloat(1);
        //        }
        //    };
        //    var iDQTotal = iDQ1 + iDQ2 + iDQ3 + iDQ4;
        //    if (iDQTotal != 0) {
        //        DQ1 = Math.floor(iDQTotal / 4);
        //        var reminder = iDQTotal - (DQ1 * 4);
        //        if (reminder > 0) {
        //            DQ1 = DQ1 + 1;
        //            reminder = reminder - 1;
        //            if (reminder > 0) {
        //                DQ2 = DQ2 + 1;
        //                reminder = reminder - 1;
        //                if (reminder > 0) {
        //                    DQ3 = DQ3 + 1;
        //                }
        //            }
        //        }
        //    }
        //}

        $jqGrid("#jqPatientPrescription").jqGrid('saveRow', rowId, false, 'clientArray');



        $jqGrid('#jqPatientPrescription').jqGrid('restoreRow', rowId);
        $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ1', DQ1);
        $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ2', DQ2);
        $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ3', DQ3);
        $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'DQ4', DQ4);
        //$jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ActualPPD', ActPPD.toFixed(2));
        //$jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'AssessedPadsPerDay', PPD);    
        if (!$jqGrid("#" + rowId + "_IsOverriddenFlag").is(':checked')) {
            var cmDQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ1');
            var cmDQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ2');
            var cmDQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ3');
            var cmDQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ4');
            var cmAssessedPPD = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'AssessedPadsPerDay');

            cmDQ1.editable = false;
            cmDQ2.editable = false;
            cmDQ3.editable = false;
            cmDQ4.editable = false;
            cmAssessedPPD.editable = true;
            $jqGrid('#jqPatientPrescription').jqGrid('editRow', rowId, true, null, null, 'clientArray');
            //cmDQ1.editable = true;
            //cmDQ2.editable = true;
            //cmDQ3.editable = true;
            //cmDQ4.editable = true;
        }
        else if (window.gridid != "jqCommunityOrders" && window.gridid != "jqOneOffOrders") {
            var cmDQ1 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ1');
            var cmDQ2 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ2');
            var cmDQ3 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ3');
            var cmDQ4 = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'DQ4');
            var cmAssessedPPD = $jqGrid('#jqPatientPrescription').jqGrid('getColProp', 'AssessedPadsPerDay');

            cmDQ1.editable = true;
            cmDQ2.editable = true;
            cmDQ3.editable = true;
            cmDQ4.editable = true;
            cmAssessedPPD.editable = false;
            $jqGrid('#jqPatientPrescription').jqGrid('editRow', rowId, true, null, null, 'clientArray');
            //cmDQ1.editable = false;
            //cmDQ2.editable = false;
            //cmDQ3.editable = false;
            //cmDQ4.editable = false;
        }


        //Totalpieces = PPD * ((12 * 7) * 4);
        //totalpacks = Totalpieces / PackSize;
    }

    var patientId = "";
    var customerId = "";
    var PatientName = "";
    var CustomerName = "";
    var SAPCustomerNumber = "";
    var SAPPatientNumber = "";
    var IsOrderActived = "";
    var PatientStatus = "";
    var carehomeId = "";
    var carehomeName = "";
    var postCode = "";
    var hasActiveOrder = "";
var DoNotShowAlert = "";

    function SetPatientLoadParameter(rowData, SetSessionData) {
    $("#hdnSessionDoNotShow").val("false");
        var patientName = "";
        try {
            patientName = $jqGrid.parseHTML(rowData['PatientName']);
            patientName = $jqGrid(patientName).text();
        }
        catch (err) {
            patientName = rowData['PatientName'];
        }
        patientId = rowData['PatientId'];
        customerId = rowData['PatientCustomerId'];
        PatientName = patientName; //rowData['PatientName'];
        CustomerName = rowData['PatientCustomerName'];
        SAPCustomerNumber = rowData['SAPCustomerNumber'];
        SAPPatientNumber = rowData['SAPPatientNumber'];
        IsOrderActived = rowData['OrderActivated'];
        PatientStatus = rowData['PatientStatus'];
        carehomeId = rowData['CareHomeId'];
        carehomeName = rowData['CareHomeName'];
        postCode = rowData['Postcode'];
        hasActiveOrder = rowData['HasActiveOrder'];
        if (SetSessionData) {
            //if (customerId != "" && PatientName != "" && CustomerName != "" && SAPCustomerNumber != "") {
            $jqGrid.ajax({
                url: '/Patient/SetPatientSession',
                type: 'POST',
                cache: false,
                data: {
                    patientId: patientId, customerId: customerId, PatientName: PatientName, CustomerName: CustomerName, SAPCustomerNumber: SAPCustomerNumber, SAPPatientNumber: SAPPatientNumber, IsOrderActived: IsOrderActived, PatientStatus: PatientStatus, CareHomeID: carehomeId, CareHomeName: carehomeName, PostCode: postCode
                },
                success: function (data) {

                }
            });
            //}
        }
    }


    function ShowCustomerPatientName() {
        $jqGrid(".cust_pat_details").show();
        var customerName = $("#ddlCustomer option:selected").text();

        var sappatientNumber = $jqGrid("#SAPPatientNumber").text();
        var patientid = "";
        if ($jqGrid("#PatientId").text()=="") {
            patientid = patientId;
        } else {
            patientid = $jqGrid("#PatientId").text();
        }
        var patientName = patientid + " " + $jqGrid("#FirstName").val() + " " + $jqGrid("#LastName").val();
        if (sappatientNumber != "")
            patientName += " (" + sappatientNumber + ")";


        $jqGrid("#lblCustomerName").text('');
        $jqGrid("#lblPatientName").text('');

        $jqGrid("#lblCustomerName").text(customerName);
        $jqGrid("#lblPatientName").text(patientName);
    }


    function LoadSearchPatientDetails() {
        $jqComm("ul.dock").addClass("free").animate({ left: "-1170px" }, 500);
        $jqComm("a.dock").css("display", "inline");
        $jqComm("a.undock").css("display", "none");
        $jqGrid.ajax({
            url: '/Patient/LoadPatientDetails',
            type: 'POST',
            cache: false,
            data: {
                patientId: patientId, customerId: customerId, PatientName: PatientName, CustomerName: CustomerName, SAPCustomerNumber: SAPCustomerNumber, SAPPatientNumber: SAPPatientNumber
            },
            success: function (data) {
                CarehomeData = data;
                EnableDetails();
                SetPatientDetails(data);
                $jqGrid('.collapse').css('display', 'block');
                $jqGrid('.heading-1 a').addClass('open');
                $jqComm(".staff_select_mode_collapse").css('display', 'none');
                $jqComm(".analysis_information_collapse").css('display', 'none');
                $jqComm("#search_dock_bg").css('display', 'none');
                DisablePatientInfoPage();
                window.patientId = patientId;
                window.customerId = customerId;
                window.patientName = PatientName;
                window.customerName = CustomerName;
                window.CustomerParamets = data.CustomerParameters;
                if (data.CustomerParameters != null && data.CustomerParameters[0].IsNDDAllow == true) {
                    DisableChangeNDD(false);
                    // $jqGrid('#changeNDDPrescription').show();
                }
                else {
                    DisableChangeNDD(true);
                    //    $jqGrid('#changeNDDPrescription').hide();
                }
                $jqGrid("#jqPatientAnalysis").trigger('reloadGrid');
                $jqGrid("#jqMedicalStaffAnalysis").trigger('reloadGrid');
                $jqGrid("#jqPatientPrescription").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                DisableSaveButton();
                EnableEditButton();
                DisablePrescriptionButtons();
                DisableCancelButton();
                //EnableCancelButton();

                //DisableChangeNDD(true);
                //$jqGrid('#changeNDDPrescription').addClass("btn_disable");
                //$jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
                //Show customer and Patient name on top

                ShowCustomerPatientName();
                // Set Form View Only for Removed carehome 
                //if (data.CareHomeStatus == $("#hdnCareHomeRemovedStatus").val()) {
                //SetViewOnly("divPatientInfoDisplay");
                //}
                $jqGrid('#hdIsCarehomePatient').val(data.IsCarehomePatient);
                if ($jqGrid("#hdIsCarehomePatient").val().toUpperCase() == valTrue.toUpperCase()) {
                //if ((jQuery.inArray($jqGrid("#ddlPatientType").val(), NonCarehome)) == -1) {
                    $jqGrid('#RoundId').attr('disabled', 'disabled');
                }
                else {
                    $jqGrid("#RoundId").removeAttr('disabled', 'disabled');
                    $jqGrid('#RoundId').removeClass('bg_ddd');
                    $jqGrid("#spnMandatory").show();
                }
                GetPrescriptionNDDRelatedDetails($("#ddlCustomer").val(), $("#Assignedcarehome").val() == "" ? 0 : $("#Assignedcarehome").val(), $("#PatientId").text() == "" ? 0 : $("#PatientId").text());
            }

        });
    }

    function GetPatientSession() {
        var PageUrl = window.location.href;
        if (PageUrl.indexOf("Patient/ViewDetails") != -1) {

            $jqGrid.ajax({
                url: '/Patient/GetPatientSession',
                type: 'GET',
                cache: false,
                async: false,
                success: function (data) {
                    if (data != null && data != undefined & data != "") {
                        window.patientId = data.patientId;
                        window.customerId = data.customerid;
                        window.PatientName = data.PatientName;
                        window.CustomerName = data.CustomerName;
                        window.SAPCustomerNumber = data.SAPCustomerNumber;
                        window.SAPPatientNumber = data.SAPPatientNumber;
                        window.IsOrderActived = data.IsOrderActived;
                        window.PatientStatus = data.PatientStatus;

                        if (window.patientId > 0 && window.customerId != "" && window.PatientName != "" && window.CustomerName != "") {

                            LoadSearchPatientDetails();
                            // Check for ViewOnly 
                            //linkId: - Menu Link Id, element: - Main Div of the Form
                            CommonScriptObj.DisableFormControls("divPatientMenuDetails");

                        }
                    }
                    else {
                        $jqGrid('#divViewPatientError').show();
                        //
                        //if (!$jqGrid("#PatientDetails").hasClass("btn_disable"))
                        //{
                        //    $jqGrid('#divViewPatientError').show();
                        //}
                    }
                }
            });
        }
        else {
            // This is for Add Patient Details
            //linkId: - Menu Link Id, element: - Main Div of the Form
            CommonScriptObj.DisableFormControls("PatientDetails");
        }

        // Get Patient Details Access
        GetPatientSubModuleAccess();
    }

    function HideNurseCommentsDiv() {
        //$jqGrid('#divNurseDetails').hide();
    }

    function ShowNurseCommentsDiv() {
        $jqGrid('#divNurseDetails').show();
    }

    function validateNurseComments() {


        var errorMsg = "";
        var newline = "</br>";
        var errorMsgUlclose = "</ul>";
        var errorMsgLi = "<li>";

        var errorMsgLiclose = "</li>";


        if ($.trim($jqGrid('#txtNurseName').val()).length == 0 || $.trim($jqGrid('#txtNurseName').val()) == $jqGrid('#hdnPlcHolderNurseName').val()) {
            errorMsg += errorMsgLi + $("#hdnErrormsgNurseName").val() + errorMsgLiclose;
        }

        if ($.trim($jqGrid('#txtNurseMobile').val()).length == 0 || $.trim($jqGrid('#txtNurseMobile').val()) == $jqGrid('#hdnPlcHolderNurseMobile').val()) {
            errorMsg += errorMsgLi + $("#hdnErrormsgNurseMobile").val() + errorMsgLiclose;
        }

        if ($.trim($jqGrid('#txtNurseEmail').val()).length == 0 || $.trim($jqGrid('#txtNurseEmail').val()) == $jqGrid('#hdnPlcHolderNurseEmail').val()) {
            errorMsg += errorMsgLi + $("#hdnErrormsgNurseEmail").val() + errorMsgLiclose;
        }


        else {
            if (!ValidateEmail($("#txtNurseEmail").val())) {
                errorMsg += errorMsgLi + $("#hdnErrormsgNurseValidEmail").val() + errorMsgLiclose;
            }
        }

        if ($.trim($jqGrid('#txtNurseComments').val()).length == 0 || $.trim($jqGrid('#txtNurseComments').val()) == $jqGrid('#hdnPlcHolderNurseComments').val()) {
            errorMsg += errorMsgLi + $("#hdnErrormsgNurseComments").val() + errorMsgLiclose;
        }

        return errorMsg;
    }

    function ClearNurseComments() {

        $jqGrid('#txtNurseName').val('');
        $jqGrid('#txtNurseMobile').val('');
        $jqGrid('#txtNurseEmail').val('');
        $jqGrid('#txtNurseComments').val('');
    }

    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function (match) {
            return match.charAt(0).toUpperCase() + match.substr(1).toLowerCase();
        });
    }

    $jqGrid('#txtNurseName').keydown(function () {
        $jqGrid('#txtNurseName').val(toTitleCase($jqGrid('#txtNurseName').val()));
    })


    function DisableNurseComments() {

        $jqGrid('#txtNurseName').attr('disabled', 'disabled');
        $jqGrid('#txtNurseMobile').attr('disabled', 'disabled');
        $jqGrid('#txtNurseEmail').attr('disabled', 'disabled');
        $jqGrid('#txtNurseComments').attr('disabled', 'disabled');

        $jqGrid('#txtNurseName').addClass('btn_disable');
        $jqGrid('#txtNurseMobile').addClass('btn_disable');
        $jqGrid('#txtNurseEmail').addClass('btn_disable');
        $jqGrid('#txtNurseComments').addClass('btn_disable');

    }

    function EnableNurseComments() {

        $jqGrid('#txtNurseName').removeClass('btn_disable');
        $jqGrid('#txtNurseMobile').removeClass('btn_disable');
        $jqGrid('#txtNurseEmail').removeClass('btn_disable');
        $jqGrid('#txtNurseComments').removeClass('btn_disable');

        $jqGrid('#txtNurseName').removeAttr('disabled');
        $jqGrid('#txtNurseMobile').removeAttr('disabled');
        $jqGrid('#txtNurseEmail').removeAttr('disabled');
        $jqGrid('#txtNurseComments').removeAttr('disabled');
    }

    function DisablePatientInfoPage() {
        $jqGrid('.DisablePages').addClass("disable_div");
    }

    function EnablePatientInfoPage() {
        $jqGrid('.DisablePages').removeClass("disable_div");
    if (isCopyDetails == true) { $("#ddlCustomer").removeAttr("disabled"); }
    }

    function SetSameAsDeliveryCheckBox() {

        var isChecked = $("#chkSameAsDelivery").prop('checked');
        if (isChecked) {
            $("#divCorrespondenceAddress .patientinfo_form .ez-checkbox").removeClass("ez-checked");
            $("#chkSameAsDelivery").prop('checked', false);
        }
        else {
            $("#divCorrespondenceAddress .patientinfo_form .ez-checkbox").addClass("ez-checked");
            $("#chkSameAsDelivery").prop('checked', true);
        }
        CopyDeliveryAddress(true);
    }

    function CopyDeliveryAddress(IsClearCorrespondenceAddress) {

        var isChecked = $("#chkSameAsDelivery").prop('checked');
        if (isChecked) {
            SetDeliveryAddressMandatory("", true);
            DisableCorrespondenceAddress();
        }
        else {
            EnableCorrespondenceAddress(IsClearCorrespondenceAddress);
            SetDeliveryAddressMandatory("", false);
        }
    }

    function EnableCorrespondenceAddress(IsClearCorrespondenceAddress) {
        if (IsClearCorrespondenceAddress) {
            ClearCorrespondenceAddress();
        }
        $("#CorrespondenceCareHomeName").removeClass("bg_ddd");
        $("#CorrespondenceAddress1").removeClass("bg_ddd");
        $("#CorrespondenceAddress2").removeClass("bg_ddd");
        $("#CorrespondenceTownOrCity").removeClass("bg_ddd");
        $("#ddlCorrespondenceCounty").removeClass("bg_ddd");
        $("#CorrespondencePostCode").removeClass("bg_ddd");
        $("#btnCorrespondencePostCode").removeClass('btn_disable');

        $("#CorrespondenceCareHomeName").removeAttr('disabled');
        $("#CorrespondenceAddress1").removeAttr('disabled');
        $("#CorrespondenceAddress2").removeAttr('disabled');
        $("#CorrespondenceTownOrCity").removeAttr('disabled');
        $("#ddlCorrespondenceCounty").removeAttr('disabled');
        $("#CorrespondencePostCode").removeAttr('disabled');
        $("#btnCorrespondencePostCode").removeAttr('disabled');

    }

    function DisableCorrespondenceAddress() {

        $("#CorrespondenceCareHomeName").val($("#CareHomeName").val());
        $("#CorrespondenceAddress1").val($("#Address1").val());
        $("#CorrespondenceAddress2").val($("#Address2").val());
        $("#CorrespondenceTownOrCity").val($("#TownOrCity").val());
        $("#ddlCorrespondenceCounty").val($("#ddlCounty").val());
        $("#CorrespondencePostCode").val($("#PostCode").val());

        $("#CorrespondenceCareHomeName").addClass("bg_ddd");
        $("#CorrespondenceAddress1").addClass("bg_ddd");
        $("#CorrespondenceAddress2").addClass("bg_ddd");
        $("#CorrespondenceTownOrCity").addClass("bg_ddd");
        $("#ddlCorrespondenceCounty").addClass("bg_ddd");
        $("#CorrespondencePostCode").addClass("bg_ddd");
        $("#btnCorrespondencePostCode").addClass("btn_disable");

        $("#CorrespondenceCareHomeName").attr('disabled', 'disabled');
        $("#CorrespondenceAddress1").attr('disabled', 'disabled');
        $("#CorrespondenceAddress2").attr('disabled', 'disabled');
        $("#CorrespondenceTownOrCity").attr('disabled', 'disabled');
        $("#ddlCorrespondenceCounty").attr('disabled', 'disabled');
        $("#CorrespondencePostCode").attr('disabled', 'disabled');
        $("#btnCorrespondencePostCode").attr('disabled', 'disabled');
    }

    function ClearCorrespondenceAddress() {
        $("#CorrespondenceCareHomeName").val("");
        $("#CorrespondenceAddress1").val("");
        $("#CorrespondenceAddress2").val("");
        $("#CorrespondenceTownOrCity").val("");
        $('#ddlCorrespondenceCounty').prop('selectedIndex', 0);
        $("#CorrespondencePostCode").val("");

    }

    function ClearAddress() {
        $("#CareHomeName").val("");
        $("#Address1").val("");
        $("#Address2").val("");
        $("#TownOrCity").val("");
        $("#ddlCounty").val("");
        $('#ddlCounty').prop('selectedIndex', 0);
        $("#PostCode").val("");
    }

    function DisableAddress() {

        $jqGrid('#btnCarehome').addClass('btn_disable');
        $jqGrid('#btnCarehome').attr('disabled', 'disabled');

        $("#CareHomeName").addClass("bg_ddd");
        $("#Address1").addClass("bg_ddd");
        $("#Address2").addClass("bg_ddd");
        $("#TownOrCity").addClass("bg_ddd");
        $("#ddlCounty").addClass("bg_ddd");
        $("#PostCode").addClass("bg_ddd");
        $("#btnPostCode").addClass("btn_disable");

        $("#CareHomeName").attr('disabled', 'disabled');
        $("#Address1").attr('disabled', 'disabled');
        $("#Address2").attr('disabled', 'disabled');
        $("#TownOrCity").attr('disabled', 'disabled');
        $("#ddlCounty").attr('disabled', 'disabled');
        $("#PostCode").attr('disabled', 'disabled');
        $("#btnPostCode").attr('disabled', 'disabled');
    }

    function ValidateCorrespondenceAddress(errorMsg, errorMsgLi, errorMsgLiclose) {
        var Correspondence = "Correspondence ";
        var CorrespondenceAddress1 = $("#CorrespondenceAddress1").val();
        var CorrespondenceTownOrCity = $("#CorrespondenceTownOrCity").val();
        var ddlCorrespondenceCounty = document.getElementById('ddlCorrespondenceCounty').selectedIndex;
        var CorrespondencePostCode = $("#CorrespondencePostCode").val();
        var CorrespondencePostCode = $("#CorrespondencePostCode").val();
        //var SelectedValue = $jqGrid("#ddlCommunicationFormat").val();
        var SelectedValue = $jqGrid("#ddlCommunicationFormat").multiselect('getChecked').map(function (index) { return $(this).attr('value'); })
        var CheckForCorrespondenceAddress = false;
        if (jQuery.inArray($("#hdnCommFormatePost").val(), SelectedValue) != -1) {
            CheckForCorrespondenceAddress = true;
        }

        if ((CorrespondenceAddress1 != "" || CorrespondenceTownOrCity != "" || ddlCorrespondenceCounty > 0 || CorrespondencePostCode != "") || CheckForCorrespondenceAddress) {
            if ($.trim(CorrespondenceAddress1) == "") {

                errorMsg += errorMsgLi + $("#hdnreserrormsCorrespondenceAddress1").val() + errorMsgLiclose;
            }

            if ($.trim(CorrespondenceTownOrCity) == "") {

                errorMsg += errorMsgLi + $("#hdnreserrormsCorrespondenceTownOrCity").val() + errorMsgLiclose;
            }


            if ($.trim(CorrespondencePostCode) == "") {

                errorMsg += errorMsgLi + $("#hdnreserrormsCorrespondencePostCode").val() + errorMsgLiclose;
            }
        }
        return errorMsg;
    }

    var NoCommunicationChecked = false;
    function SetNoCommunicationValidation(ui, dropdown) {
        var NoCommunicationId = $("#hdnCommFormateNoCommunication").val();
        var ul = $jqGrid(".ui-multiselect-checkboxes input").filter(function (index) { return $(this).attr("name") == 'multiselect_' + dropdown; }).closest(".ui-multiselect-checkboxes");
        if (NoCommunicationChecked == false && ui.value == NoCommunicationId) {
            // Get the Communication Format Dropdown       

            $jqGrid(".ui-multiselect-checkboxes input").filter(function (index) { return $(this).attr("name") == 'multiselect_' + dropdown; }).filter('input').closest("div").removeClass("ez-checked");
            //$jqGrid(ul).filter('input').prop('checked', false);

            $jqGrid(".ui-multiselect-checkboxes input").filter(function (index) { return $(this).attr("name") == 'multiselect_' + dropdown; }).filter('input').prop('checked', false);

            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").addClass("ez-checked");
            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', true);
            NoCommunicationChecked = true;
        }

        else if (NoCommunicationChecked && ui.value == NoCommunicationId) {

            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', false);
            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").removeClass("ez-checked");
            NoCommunicationChecked = false;
        }
        else if (ui.checked && ui.value != NoCommunicationId) {
            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").removeClass("ez-checked");
            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', false);

            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == ui.value }).parent(".ez-checkbox").addClass("ez-checked");
            $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == ui.value }).prop('checked', true);

            NoCommunicationChecked = false;
        }
    }

    function SetNoCommunicationOnCheckAll(flag, dropdwon) {

        var NoCommunicationId = $("#hdnCommFormateNoCommunication").val();
        if (flag) {
            dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").removeClass("ez-checked");
            dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', false);
            NoCommunicationChecked = false;
        }
        else {
            dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").addClass("ez-checked");
            dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', true);
            NoCommunicationChecked = true;
        }
    }
    function SetMandatoryBasedOnMarketingPreferences() {

        //var SelectedValue = $jqGrid("#ddlCommunicationFormat").val();
        var SelectedValue = $jqGrid("#ddlCommunicationFormat").multiselect('getChecked').map(function (index) { return $(this).attr('value'); })

        if (jQuery.inArray($("#hdnCommFormateTelephoneNo").val(), SelectedValue) != -1) {
            $("#lblPhoneNo").html($("#lblPhoneNo").html().replace("*", "") + "*");
        }
        else if ($("#hdnTelephoneMandetory").val() == "false") {
            $("#lblPhoneNo").html($("#lblPhoneNo").html().replace("*", ""));
        }

        if (jQuery.inArray($("#hdnCommFormateText").val(), SelectedValue) != -1) {
            $("#lblMobileNo").html($("#lblMobileNo").html().replace("*", "") + "*");
        }
        else {
            $("#lblMobileNo").html($("#lblMobileNo").html().replace("*", ""));
        }

        if (jQuery.inArray($("#hdnCommFormateEmail").val(), SelectedValue) != -1) {
            $("#lblEmailAddress").html($("#lblEmailAddress").html().replace("*", "") + "*");
        }
        else {
            $("#lblEmailAddress").html($("#lblEmailAddress").html().replace("*", ""));
        }
        SetDeliveryAddressMandatory(SelectedValue, false);
    }

    function SetDeliveryAddressMandatory(SelectedValue, IsDeliverySameAsPatient) {
        if (jQuery.inArray($("#hdnCommFormatePost").val(), SelectedValue) != -1 || IsDeliverySameAsPatient) {
            //$("#lbldelCareHome").html($("#lbldelCareHome").html().replace("*", "") + "*");
            $("#lblAddress1").html($("#lblAddress1").html().replace("*", "") + "*");
            $("#lblTownOrCity").html($("#lblTownOrCity").html().replace("*", "") + "*");
            $("#lblPostCode").html($("#lblPostCode").html().replace("*", "") + "*");
        }
        else {
            //$("#lbldelCareHome").html($("#lbldelCareHome").html().replace("*", ""));
            $("#lblAddress1").html($("#lblAddress1").html().replace("*", ""));
            $("#lblTownOrCity").html($("#lblTownOrCity").html().replace("*", ""));
            $("#lblPostCode").html($("#lblPostCode").html().replace("*", ""));
        }
    }

    function SetCommonObjects() {
        var customername = $("#ddlCustomer option:selected").text().split('(');
        var sapCustomerName = "";
        if (typeof (customername[1]) != "undefined") {
            sapCustomerName = customername[1].split(')');
        }

        CommonScriptObj.SAPCustomerNumber = sapCustomerName[0];
        CommonScriptObj.CustomerName = customername[0];
        CommonScriptObj.PatientId = $jqGrid("#PatientId").text();
        CommonScriptObj.CustomerId = $("#ddlCustomer").val();
        CommonScriptObj.PatientName = $('#FirstName').val() + " " + $('#LastName').val();
        CommonScriptObj.SAPPatientNumber = $("#SAPPatientNumber").text();
        CommonScriptObj.SAPCareHomeNumber = "";
        CommonScriptObj.CareHomeName = "";
        $jqGrid("#hdnCreateInteractionPatientId").val(CommonScriptObj.PatientId);
        $jqGrid("#hdnCreateInteractionCustomerId").val(CommonScriptObj.CustomerId);
    }

    function NavigateToSampleOrderMenu() {
        // Check for Sample order Menu avaibility
        //var viewSampleOrderElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnSampleOrderMenuText").val() });
        //if (viewSampleOrderElement.length == 0) {

        //    // SubMenu not available. Need to show error message.
        //    CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnSampleOrderMenuTextMessage").val(), 'pop_error_ul');
        //    return false;
        //}

        var viewSampleOrderElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnSampleOrderMenuText").val() });
        if (PatientInfoObj != null) {
            if (PatientInfoObj.SampleOrdersAccess == "0") {

                // SubMenu not available. Need to show error message.
                CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnSampleOrderMenuTextMessage").val(), 'pop_error_ul');
                return false;
            }
        }

        // Navigate to Sample Order
        var navigateURL = $jqGrid("#hdnNavigateSampleOrderMenuText").val();
        window.location.href = navigateURL;
    }

    function BackToHomepage() {
        $jqGrid.ajax({
            url: '/Patient/GetHomePageFlag',
            type: 'POST',
            dataType: 'Json',
            data: {},
            success: function (data) {
                window.location.href = data;
            }
        });
    }

    function GetPrescriptionNDDRelatedDetails(customerId, carehomeId, patientId) {
        $jqGrid.ajax({
            url: '/Patient/GetPrescriptionNDDRelatedDetails',
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            data: { customerId: customerId, carehomeId: carehomeId, patientId: patientId },
            cache: false,
            async: false,
            success: function (responseData) {
                $jqGrid("#hdnPostcodeMatrix").val(encodeURIComponent(JSON.stringify(responseData)));
            }
        });
    }

function isCarehomeActive()
{
        var careHomeStoppedStatus = parseInt($("#hdnCareHomeStoppedStatus").val());
        var careHomeRemovedStatus = parseInt($("#hdnCareHomeRemovedStatus").val());
        var careHomeActiveStatus = parseInt($("#hdnCareHomeActiveStatus").val());
        var selectedCareHomeStatus = "";
        if ($("#hdnSelectedCarehomeStatus").val() === "") {
            selectedCareHomeStatus = CarehomeData.CareHomeStatus;
        }
        else {
            selectedCareHomeStatus = $("#hdnSelectedCarehomeStatus").val();
        }

        if (parseInt(selectedCareHomeStatus) === careHomeActiveStatus) {
            return true;
        }
        else {
        return false;
    }
}

    //Prescription 1 Changes - Phase 3 - Release 1 - Sprint 1 

function DeletePrescriptionItems() {
        var rowIds = $jqGrid('#jqPatientPrescription').jqGrid('getDataIDs');
        var idsToRemove = [];
        var rowIndexToDisable = [];
        window.TotalProductCount = rowIds.length;
        window.RemovedProductCount = 0;
        window.RemovedProductsInDB = 0;
        for (var i = 1; i <= rowIds.length; i++) {
            var rowData = $jqGrid('#jqPatientPrescription').jqGrid('getRowData', i);
            if ($jqGrid("#" + i + "_IsRemoved").is(':checked')) {
                if (rowData["Status"] != "Removed")
                    {
                    rowIndexToDisable.push(i);
                }
                if (rowData["IDXPrescriptionProductId"]!= undefined && rowData["IDXPrescriptionProductId"] != '' && rowData["IDXPrescriptionProductId"]!= 0) {
                    idsToRemove.push(rowData["IDXPrescriptionProductId"]);
                    window.RemovedProductsInDB++;
                }
                window.RemovedProductCount++;
            }
    }

        if (rowIndexToDisable.length > 0)
        {
            $jqGrid("#divRemovePrescription").dialog({
                    resizable: false,
                    height: 140,
                    modal: true,
                    closeOnEscape: false,
                    buttons: [{
                            text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        PatientHasChanged();
                        $jqGrid(this).dialog("close");
                        $jqGrid(".ui-dialog-titlebar-close").hide();
                        DisableRemovedPrescriptionItems(rowIndexToDisable);
                        CompareActualPPDwithMaxPPDRemoved();
                        if (window.isPrescriptionEditable.toString().toUpperCase() != value.toUpperCase() && window.RoleName == $jqGrid('#hdnNurseRoleName').val()) {
                            window.PrescriptionEditedbyNurse = true;
                            EnableNurseComments();
                        }

                        if (window.RemovedProductsInDB >= 1 && window.TotalProductCount == window.RemovedProductCount)
                        {
                            window.IsReassessment = true;
                        }
                    },
                    },
                    {
                            text: $jqGrid("#hdnGeneralCancel").val().toString(),
                    "id": "hdnGeneralCancel",
                            click: function () {
                        for (var i = 0; i < rowIndexToDisable.length; i++)
                        {
                            var rowDetails = $jqGrid('#jqPatientPrescription').jqGrid('getRowData', rowIndexToDisable[i]);
                            $('#' + rowIndexToDisable[i] + "_IsRemoved").attr('checked', false);
                        }
                        $jqGrid(this).dialog("close");
                    },
                    }]
            });
    }
}

    function DisableRemovedPrescriptionItems(rowIndexs)
    {
        for (var i = 0; i < rowIndexs.length; i++) {
            var rowId = rowIndexs[i];
            var rowDataToDisable = $jqGrid('#jqPatientPrescription').jqGrid('getRowData', rowId);
            $('#' + rowId + "_IsRemoved").val('removed');
            $('#' + rowId + "_RowStatus").val('Old');
            $('#' + rowId + "_IsRemoved").attr('checked', true);
            $('#' + rowId + "_IsRemoved").attr('disabled', 'disabled');
            $jqGrid('#jqPatientPrescription').setColProp('AssessedPadsPerDay', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ1', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ2', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ3', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DQ4', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('ActualPPD', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('Frequency', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('ValidFromDate', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('ValidToDate', { editable: true });
            $jqGrid('#jqPatientPrescription').setColProp('DeliveryDate', { editable: true });
            $jqGrid('#jqPatientPrescription').jqGrid('saveRow', rowId);
            $jqGrid('#jqPatientPrescription').jqGrid('restoreRow', rowId);
            $jqGrid('#jqPatientPrescription').setColProp('AssessedPadsPerDay', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('IsOverriddenFlag', { editable: false });
            $jqGrid('#' + rowId + '_IsOverriddenFlag').attr('disabled', 'disabled');
            $jqGrid('#jqPatientPrescription').setColProp('ActualPPD', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('Frequency', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('ValidFromDate', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('ValidToDate', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('DeliveryDate', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('DQ1', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('DQ2', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('DQ3', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('DQ4', { editable: false });
            $jqGrid('#jqPatientPrescription').setColProp('ActualPadsPerDay', { editable: false });
            $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'ValidToDate', TodaysDate());
            $jqGrid('#jqPatientPrescription').jqGrid('setCell', rowId, 'Status', "Removed");
            if (!$("#show_removed").is(':checked'))
                $jqGrid("#jqPatientPrescription #" + rowId).hide();
            var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
    }
        SetPatientNDD(false);
}

function RemoveCheckBoxCheck(rowid)
{
        if ($('#' + rowid + "_IsRemoved").attr('checked') == 'checked') {
            $('#' + rowid + "_IsRemoved").removeAttr('checked');
        }
        else {
            $('#' + rowid + "_IsRemoved").attr('checked', 'checked');
    }
}

function CheckPrescriptionProductExitForCustomers(patientId, customerId, objPrescriptionDetails) {
        var isProductExist;
        $jqGrid.ajax({
                url: '/Patient/CheckPrescriptionProductExistForCustomer',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ objPrescriptionDetails: objPrescriptionDetails, patientId: patientId, customerId: customerId }),
                cache: false,
                async: false,
                success: function (responseData) {
                if (responseData.length > 0) {
                    CommonScriptObj.DisplayErrorMessage("divErrorMessage", "Product " + responseData + " not available", 'pop_error_ul');
                    isProductExist = responseData.length;
                }
                else {
                    isProductExist = responseData.length;
                }
        },
                error: function (x, e) {
                HandleAjaxError(x, e, jsfilename, "CheckPrescriptionProductExitForCustomers");
            return false;
        }
        });
        return isProductExist;
}

    function EnableCorrespondenceAddressForCopyPatient() {
        if (isCopyDetails == true) {
            $("#CareHomeName").removeClass("bg_ddd");
            $("#CareHomeName").removeAttr("disabled", "disabled");
            $("#CareHomeName").attr("readonly", false);
            $("#Address1").removeClass("bg_ddd");
            $("#Address1").removeAttr("disabled", "disabled");
            $("#Address1").attr("readonly", false);
            $("#Address2").removeClass("bg_ddd");
            $("#Address2").removeAttr("disabled", "disabled");
            $("#Address2").attr("readonly", false);
            $("#TownOrCity").removeClass("bg_ddd");
            $("#TownOrCity").removeAttr("disabled", "disabled");
            $("#TownOrCity").attr("readonly", false);
            $("#ddlCounty").removeClass("bg_ddd");
            $("#ddlCounty").removeAttr("disabled", "disabled");
            $("#ddlCounty").attr("readonly", false);
            $("#PostCode").removeClass("bg_ddd");
            $("#PostCode").removeAttr("disabled", "disabled");
            $("#PostCode").attr("readonly", false);
        }
    }

function HasActivePrescriptionProduct() {
        var gridRowsprescription = $jqGrid("#jqPatientPrescription").jqGrid('getRowData');
        var ids = $jqGrid("#jqPatientPrescription").jqGrid('getDataIDs');
        var isValid = false;
        for (var i = 0; i < gridRowsprescription.length; i++) {
            var rowId = ids[i];
            var row = gridRowsprescription[i];

            if (row["Status"] == 'Removed' || row["Status"] == 'removed') {
                continue;
            }
            else {
                isValid = true;
                break;
            }
        }
        return isValid;
            }

    function CheckPatientDetailsChanges(e) {
        if (window.IsDirtyRead == true) {
            $jqGrid("#divValidationConfirmField").dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                resizable: false,
                closeOnEscape: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralYes").val(),
                    click: function () {
                        $jqGrid(this).dialog("close");
                        if (e.id != "LogOff")
                            ClearPatientChanged();
                        window.IsPatientChanged = false;
                            
                        try {
                            e.click();
                        }
                        catch (err) { }
                    },
                },
                {
                    text: $jqGrid("#hdnGeneralNo").val(),
                    click: function () {
                        $jqGrid(this).dialog("close");
                    },
                }
                ]
            });
            $jqGrid("#divValidationConfirmField").dialog('open');
            return false;
        }
        return true;
    }

function PatientHasChanged() {
        window.IsDirtyRead = true;   
    }

function ClearPatientChanged() {
        window.IsDirtyRead = false;
    }