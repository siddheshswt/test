﻿$jqGrid('#btnAdd').click(function () {
    ShowAddContactInfo();
});

function AddNewDetails() {
    var isError = 0;
    alphaSepecialCharacterRegex = /^[ A-Za-z_@./'#&+-\s]*$/;
    var firstName = $('#txtFirstName').val();
    var lastName = $('#txtLastName').val();
    var relation = $('#txtJobTitle').val();

    if (!alphaSepecialCharacterRegex.test(firstName)) {
        $('#errorFirstName').html($('#hdnreserrormsgFirstNameInvalid').val());
        isError = 1;
    }
    if ($.trim($('#txtFirstName').val()) == '') {
        $('#errorFirstName').html($('#hdnreserrormsgFirstName').val());
        isError = 1;
    }
    if ($('#txtEmail').val() != '') {
        var isvalid = ValidateEmail($("#txtEmail").val())
        if (!isvalid) {
            $('#errorEmail').html($('#hdnreserrormsgEmailAddress').val());
            isError = 1;
        }
    }
    if (isError == 1) {
        isError = 0;
        return false;
    }
    else {
        var contactType = $('#hdnContactType').val();
        var contactTypeId = $('#hdnContactTypeId').val();
        var ContactPersonId = $('#hdnContactPersonId').val()

        var objContactInfo = {
            "PersonalInformationId": $('#hdnPersonalInformationId').val(),
            "FirstName": $.trim($('#txtFirstName').val()),
            "LastName": $.trim($('#txtLastName').val()),
            "JobTitle": $.trim($('#txtJobTitle').val()),
            "TelephoneNumber": $.trim($('#txtTelephone').val()),
            "MobileNumber": $.trim($('#txtMobile').val()),
            "Email": $.trim($('#txtEmail').val()),
            "PatientID": $.trim($jqGrid("#hdnContactPatientId").val()),
            "CustomerId": $.trim($jqGrid("#hdnContactCustomerId").val()),
            "CareHomeId": $.trim($jqGrid("#hdnContactCarehomeId").val()),
            "ContactType": contactType,
            "ContactTypeId": contactTypeId,
            "ContactPersonId": (ContactPersonId != null && ContactPersonId != undefined) ? ContactPersonId : 0
        };
        $jqGrid.ajax({
            url: '/Patient/SaveContactInfoDetails',
            type: 'POST',
            cache: false,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(objContactInfo),
            async: false,
            success: function (status) {
                if (status.status) {
                    $jqGrid("#JqPatientContactInfo").jqGrid('clearGridData');
                    $jqGrid("#JqPatientContactInfo").jqGrid('setGridParam', { postData: { contactType: contactType, contactTypeId: contactTypeId }, datatype: 'json' }).trigger('reloadGrid');
                    $jqGrid('#tblPatientContactInfo').hide();
                }
            }
        });//end ajax call            
    }
    return true;
}

function SaveButtonEnable() {
    $('#btnSaveContactInfo').removeAttr('disabled');
    $('#btnSaveContactInfo').removeClass('btn_disable');
}

function AddButtonDisable() {
    $('#btnAdd').attr('disabled', 'disabled');
    $('#btnAdd').addClass('btn_disable');
}

function AddButtonEnable() {
    $('#btnAdd').removeAttr('disabled');
    $('#btnAdd').removeClass('btn_disable');
}

function ShowAddContactInfo() {
    $('#btnAdd').attr('disabled', 'disabled');
    $('#btnAdd').addClass('btn_disable');
    $('#btnSaveContactInfo').removeAttr('disabled');
    $('#btnSaveContactInfo').removeClass('btn_disable');
    $('.errorPatientContactInfo').empty();
    $('.patientContactInfoField').val('');
    $jqGrid('#tblPatientContactInfo').show();
    $('#hdnContactPersonId').val('0');
}

function HideAddContactInfo() {
    $('#btnAdd').removeAttr('disabled');
    $('#btnAdd').removeClass('btn_disable');
    $('#btnSaveContactInfo').attr('disabled', 'disabled');
    $('#btnSaveContactInfo').addClass("btn_disable");
    $('.errorPatientContactInfo').empty();
    $('.patientContactInfoField').val('');
    $jqGrid('#tblPatientContactInfo').hide();
    $('#hdnPersonalInformationId').val("0");
}

function OpenEditMode(rowData) {
    ShowAddContactInfo();
    //Fill Contact Details    
    var lastname = "";
    $('#hdnPersonalInformationId').val(rowData['PersonalInformationId']);
    var PatientName = rowData['PatientName'].split(" ");
    $('#txtFirstName').val($.trim(PatientName[0]));
    for (var i = 1; i < PatientName.length; i++) {
        lastname += ltrim(PatientName[i] + " ");
    }
    $('#txtLastName').val(lastname);
    $('#txtJobTitle').val(rowData['JobTitle']);
    $('#txtEmail').val(rowData['Email']);
    $('#txtTelephone').val(rowData['TelephoneNumber']);
    $('#txtMobile').val(rowData['MobileNumber']);
    $('#hdnContactPersonId').val(rowData['ContactPersonId']);    
    
}

function RemoveDetails() {
    var contactType = $('#hdnContactType').val();
    var contactTypeId = $('#hdnContactTypeId').val();
    $jqGrid.ajax({
        url: '/Patient/RemoveContactInfoDetails',
        type: 'POST',
        cache: false,
        data: { personalInformationIds: removeIDs },
        async: false,
        success: function (status) {
            if (status.status) {
                $jqGrid("#JqPatientContactInfo").jqGrid('clearGridData');
                $jqGrid("#JqPatientContactInfo").jqGrid('setGridParam', { postData: { contactType: contactType, contactTypeId: contactTypeId }, datatype: 'json' }).trigger('reloadGrid');
                $jqGrid("#divContactErrorMessage").html($('#hdnPatientContactInfoRemoved').val());
                ShowMessage();
                return false;
            }
        }
    });
}


function ShowMessage() {
    $jqGrid("#j_lightbox_error_ContactInfo").show();
    $jqGrid(".j_lightbox_error_close").click(function () {
        $jqGrid("#j_lightbox_error_ContactInfo").hide();
    });

    $jqGrid(".j_lightbox_error_ok_ContactInfo").click(function () {
        $jqGrid("#j_lightbox_error_ContactInfo").hide();
    });
    return false;
}
$jqGrid('#btnSaveContactInfo').click(function () {   
    if (removeIDs.length > 0) {
        RemoveDetails();
        removeIDs = [];
        HideAddContactInfo();
    }
    else {
        var isSaveed = AddNewDetails()
        if (isSaveed) {
            HideAddContactInfo();
        }
    }

});

function countChecked(checkbox) {

    var numberOfChecked = $jqGrid('.chkRemoveClass:checked').length;
    var totalCheckboxes = $('.chkRemoveClass:checkbox').length;
    if (numberOfChecked == totalCheckboxes)
        $jqGrid("#chkRemovePatientContactInfo").prop("checked", true);
    else
        $jqGrid("#chkRemovePatientContactInfo").prop("checked", false);

    if (checkbox.checked) {
        HideAddContactInfo();
        ShowRemoveWarningMessage();
    }
}

function ShowRemoveWarningMessage() {    
    $("#contact_info_ok_cancel_pop").show();
    removeIDs = [];
    AddButtonDisable();     
}

function WarningOkClick() {
    $jqGrid('.chkRemoveClass').each(function () {
        if ($jqGrid(this).is(':checked')) {
            var rowID = $(this).closest("tr").attr('id');
            var personalInformationID = $(this).attr('id');
            removeIDs.push(personalInformationID);
            $jqGrid('#JqPatientContactInfo').jqGrid('delRowData', rowID);
            SaveButtonEnable();
        }
    });
    $("#contact_info_ok_cancel_pop").hide();
}

function WarningCancelClick() {
    $jqGrid(".chkRemoveClass").prop("checked", false);
    $jqGrid("#chkRemovePatientContactInfo").prop("checked", false);
    $("#chkRemovePatientContactInfo").parent().removeClass("ez-checked");
    AddButtonEnable();
    $("#contact_info_ok_cancel_pop").hide();
}



function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (match) {
        return match.charAt(0).toUpperCase() + match.substr(1).toLowerCase();
    });
}

function ShowPopupContactInfo(contactType) {
    removeIDs = [];
    $('#chkRemovePatientContactInfo').prop('checked', false);
    $("#chkRemovePatientContactInfo").parent().removeClass("ez-checked");
    var contactTypeId = '';
    var patientId = '';
    var customerId = '';
    var carehomeId = '';    
    if (contactType == $('#hdnContactTypePatient').val()) {
        if (isCopyDetails == true) {
            if ($('#hdnPatientId').val() == '') {
                patientId = $('#PatientId').text();
            }
            else {
                patientId = $('#hdnPatientId').val();
            }
        }
        else {
            patientId = $('#PatientId').text();
        }
        $jqGrid("#hdnContactPatientId").val(patientId);
    }
    else if (contactType == $('#hdnContactTypeCareHome').val()) {
        carehomeId = $('#hdnCareHomeId').val();
        $jqGrid("#hdnContactCarehomeId").val(carehomeId);

    }
    else if (contactType == $('#hdnContactTypeCustomer').val()) {
        //  contactTypeId = $("#ddlCustomerInfomation option:selected").val();
        customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
        $jqGrid("#hdnContactCustomerId").val(customerId);
    }
   
    $('#hdnContactPersonId').val('0');

    $jqGrid('#divPatientContactInfoPopup').dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        closeOnEscape: false,
        resizable: true,
        title: $jqGrid('#hdnresContactInfoTitle').val()
    });
    $jqGrid('#divPatientContactInfoPopup').dialog('open');
    $jqGrid("#JqPatientContactInfo").jqGrid('setGridParam', { postData: { patientId: patientId, careHomeId: carehomeId, customerId: customerId }, datatype: 'json' }).trigger('reloadGrid');
    HideAddContactInfo();

    // View Only implementation as per Contact Type
    if (contactType == $('#hdnContactTypePatient').val()) {

        $jqGrid("#contactInfoMainButton").removeClass("read_only_div");
        if (PatientInfoObj.ContactInfoAccess == "0") {

            // Set Contact Info View Only
           // $jqGrid("#contactInfoMainButton").addClass("read_only_div");
            $jqGrid("#btnAdd").addClass("btn_disable");
            $jqGrid("#btnSaveContactInfo").addClass("btn_disable");
            $jqGrid("#JqPatientContactInfo").addClass('btn_disable');
            $("#JqPatientContactInfo_Remove").addClass('btn_disable');

        }
    } else if (contactType == $('#hdnContactTypeCareHome').val()) {

        // View Only implementation as per Contact Type

        $jqGrid("#contactInfoMainButton").removeClass("read_only_div");
        if (CareHomeObj.ContactInfoAccess == "0") {

            // Set Contact Info View Only
            //$jqGrid("#contactInfoMainButton").addClass("read_only_div");
             $jqGrid("#btnAdd").addClass("btn_disable");
             $jqGrid("#btnSaveContactInfo").addClass("btn_disable");
             $jqGrid("#JqPatientContactInfo").addClass('btn_disable');
             $("#JqPatientContactInfo_Remove").addClass('btn_disable');
        }
    } else if (contactType == $('#hdnContactTypeCustomer').val()) {

        // View Only implementation as per Contact Type
        $jqGrid("#contactInfoMainButton").removeClass("read_only_div");
        if (CustomerInformationObj.ContactInfoAccess == "0") {

            // Set Contact Info View Only
            // $jqGrid("#contactInfoMainButton").addClass("read_only_div");
             $jqGrid("#btnAdd").addClass("btn_disable");
             $jqGrid("#btnSaveContactInfo").addClass("btn_disable");
             $jqGrid("#JqPatientContactInfo").addClass('btn_disable');
             $("#JqPatientContactInfo_Remove").addClass('btn_disable');
        }
    }
}