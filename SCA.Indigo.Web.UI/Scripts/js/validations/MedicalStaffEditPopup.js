﻿$jqGrid = jQuery.noConflict();

var MedicalStaffEditPopupObj = new MedicalStaffEditPopup();

function MedicalStaffEditPopup() { }

$jqGrid(document).ready(function () {
    $jqGrid(".firstupper").blur(function (e) {        
        $(this).val($(this).val().slice(0, 1).toUpperCase() + $(this).val().slice(1));
    });
   
    $("#Popupsearch").keypress(function(event) {
        if (event.which == 13) {            
            loadSearchDetails();
        }               
    });      

    $jqGrid("#MedicalStaff_Popup_Search_button").click(function () {
        loadSearchDetails();
        return;
    });

    function loadSearchDetails() {        
        ResetWaterMarkValue($jqGrid('#Popupsearch')[0].id, $jqGrid('#hdnWaterMark').val());
        $jqGrid.getJSON('/Patient/DisplayMedicalStaffSearchResult', {
            txtSearch: ($jqGrid('#Popupsearch').val()), ListId: window.ListId, customerId: function () {
                var customerId = $jqGrid('#ddlCustomer').val();
                customerId = parseInt(customerId) || -1;
                return customerId;
            }, listTypeId: $jqGrid("#hdnSelectedListTypeId").val()
        }, function (data) {
            $jqGrid("#jqMedicalStaff").jqGrid('clearGridData');
            $jqGrid("#jqMedicalStaff").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
        });
    }

    $('#btnAddPatient').click(function () {
        var placeHolder = $jqGrid('#hdnPlaceholder').val();        
        
        staffName = $.trim($jqGrid('#txtName').val());

        ListId = window.ListId;
          
        if (staffName.length == 0 || staffName == placeHolder) {
            $jqGrid("#responseMessage").addClass('field-validation-error');
            $jqGrid("#responseMessage").removeClass('msgSuccess');
            $jqGrid("#responseMessage").html($jqGrid('#hdnStaffNameMessage').val());
            $jqGrid('#txtName').val('');
        }            
        if (staffName.length != 0 && staffName != placeHolder) {
            saveMasterData(staffName, ListId);
        }
         $jqGrid("#responseMessage").show();
           
    });

    $('#btnAddToGrid').click(function () {
        var selectedIds = "";
        var selectedStaffNames = "";
        var numberOfChecked = $jqGrid('.chkSelectClass:checked').length;
        if (numberOfChecked == 0) {
            $('#divErrorSelectRecord').show();
            return false;
        }
        var gridid = $jqGrid("#jqMedicalStaff"), ids = gridid.jqGrid('getDataIDs'), i, l = ids.length;
        for (var i = 2; i <= l; i++) {
            var rowData = $jqGrid("#jqMedicalStaff").jqGrid('getRowData', i);
            var customerMedicalStaffAnalysisInfoId = rowData["CustomerMedicalStaffAnalysisInfoId"];
            var staffName = rowData["StaffName"];
            var isChecked = $jqGrid("#Select_" +i).is(":checked");
            if (isChecked) {
                selectedIds += customerMedicalStaffAnalysisInfoId + ",";
                selectedStaffNames += staffName + ", ";
            }
        }
        var selectedListTypeId = $jqGrid("#hdnSelectedListTypeId").val();
        var selectedRowId = $jqGrid("#hdnSelectedRowId").val();
        if (selectedListTypeId == 2) {
            $('#jqPatientAnalysis #' + selectedRowId + '').find('td:eq(4)').html(selectedStaffNames.slice(0, -2));
            $('#jqPatientAnalysis #' + selectedRowId + '').find('td:eq(5)').html(selectedIds.slice(0, -1));
        }
        $jqGrid('#divMedStaffPopup').dialog('close');
    });
    });//ready

    function countCheckPatientMedicalStaff() {
        var numberOfChecked = $jqGrid('.chkSelectClass:checked').length;
        var totalCheckboxes = $('.chkSelectClass:checkbox').length;
        if (numberOfChecked == totalCheckboxes) {
            $jqGrid("#chkMedicalStaffSelectAll").prop("checked", true);
            $("#chkMedicalStaffSelectAll").parent(".ez-checkbox").addClass("ez-checked");
        }
        else {
            $jqGrid("#chkMedicalStaffSelectAll").prop("checked", false);
            $("#chkMedicalStaffSelectAll").parent(".ez-checkbox").removeClass("ez-checked");
        }
    }

MedicalStaffEditPopup.prototype.UpdateForLinkageClinicalContact = function()
{
    // Check if Clinical Contact is in Linkage Mode, Hide the Add new Clinical Contact Section in Medical Staff Edit Pop Up
    var isClinicalContactLinkage = $jqGrid("#hdnIsClinicalContactWithLinkage").val();
    var selectedListTypeId = $jqGrid("#hdnSelectedListTypeId").val();
    if (selectedListTypeId == "1" && isClinicalContactLinkage =="true") {
        $jqGrid("#btnAddPatient").hide();
        $jqGrid("#txtName").hide();
    }
    else {
        $jqGrid("#btnAddPatient").show();
        $jqGrid("#txtName").show();
    }
    return false;
}

MedicalStaffEditPopup.prototype.BindPatientDetailsClinicalAnalysiInfo = function (rowDataPopup) {
    try {
        PatientHasChanged();
    }
    catch (err) { }
    var isClinicalContactLinkage = $jqGrid("#hdnIsClinicalContactWithLinkage").val();
    var textToDisplay = rowDataPopup['StaffName'];
    var custMedStaffId = rowDataPopup['CustomerMedicalStaffAnalysisInfoId'];
    if (custMedStaffId == " ") {
        custMedStaffId = 0;
    }

    var selectedHierarchyId = $jqGrid("#hdnSelectedHierarchyId").val();
    var selectedListTypeId = $jqGrid("#hdnSelectedListTypeId").val();
    var selectedRowId = $jqGrid("#hdnSelectedRowId").val();

    if (selectedListTypeId == 1 ) {
        if (isClinicalContactLinkage == "true") {

            // Clear all the Existing Values
            $(".hierarchyListId").filter(function (index) {return $(this).text() == selectedHierarchyId}).parent().find('td:eq(4)').html('');
            $(".hierarchyListId").filter(function (index) { return $(this).text() == selectedHierarchyId }).parent().find('td:eq(5)').html('');

            if (custMedStaffId > 0) {
                // With Linkage Clinical Contact
                MedicalStaffEditPopupObj.BindHierarchyLinkData(custMedStaffId);
                $('#jqMedicalStaffAnalysis #' + selectedRowId + '').find('td:eq(4)').html(textToDisplay);
                $('#jqMedicalStaffAnalysis #' + selectedRowId + '').find('td:eq(5)').html(custMedStaffId);
            }
            return false;
        }
        else {
            // Non Linkage Clinical Contact
            $('#jqMedicalStaffAnalysis #' + selectedRowId + '').find('td:eq(4)').html(textToDisplay);
            $('#jqMedicalStaffAnalysis #' + selectedRowId + '').find('td:eq(5)').html(custMedStaffId);
        }
    } else {
        $('#jqPatientAnalysis #' + selectedRowId + '').find('td:eq(4)').html(textToDisplay);
        $('#jqPatientAnalysis #' + selectedRowId + '').find('td:eq(5)').html(custMedStaffId);
    }
}

MedicalStaffEditPopup.prototype.BindHierarchyLinkData = function (staffId) {
     var customerId = $jqGrid('#ddlCustomer').val();
    customerId = parseInt(customerId) || -1;
    var selectedRowId = $jqGrid("#hdnSelectedRowId").val();
    var selectedHierarchyId = $jqGrid("#hdnSelectedHierarchyId").val();
    // Ajax Call
    $jqGrid.ajax({
        url: '/Patient/GetHierarchyLinkData',
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        cache: false,
        data: JSON.stringify({ customerId: customerId, infoId: staffId, hierarchyId: selectedHierarchyId }),
        async: false,
        success: function (retData) {
            var selectedRowNum = parseInt(selectedRowId);
            for (var i = 0; i < retData.length; i++) {
                selectedRowNum = selectedRowNum + 1;
                if ($('#jqMedicalStaffAnalysis #' + selectedRowNum + '').find(".hierarchyListId").text() == selectedHierarchyId) {
                    $('#jqMedicalStaffAnalysis #' + selectedRowNum + '').find('td:eq(4)').html(retData[i].DisplayText);
                    $('#jqMedicalStaffAnalysis #' + selectedRowNum + '').find('td:eq(5)').html(retData[i].Value);
            }
            }

            return false;
        } // end succes function
    });
}

MedicalStaffEditPopup.prototype.StoreGridData = function (typeId) {

    var existingData = [];
    if (typeId == 1) {

        var gridRowsmedicalstaff = $jqGrid("#jqMedicalStaffAnalysis").jqGrid('getRowData');
        for (var i = 0; i < gridRowsmedicalstaff.length; i++) {
            var row = gridRowsmedicalstaff[i];
            var medicalstaffObj = {
                ListTypeId: row["ListTypeId"],
                ListId: row["ListId"],
                CustomerMedicalStaffAnalysisId: row["CustomerMedicalStaffAnalysisInfoId"],
            }
            existingData.push(medicalstaffObj);
        }
        PatientInfoObj.ExistClinicalContact = existingData;
    }
    else {
        var gridAnalysis = $jqGrid("#jqPatientAnalysis").jqGrid('getRowData');
        for (var i = 0; i < gridAnalysis.length; i++) {
            var row = gridAnalysis[i];
            var medicalstaffObj = {
                ListTypeId: row["ListTypeId"],
                ListId: row["ListId"],
                CustomerMedicalStaffAnalysisId: row["CustomerMedicalStaffAnalysisInfoId"],
            }
            existingData.push(medicalstaffObj);
        }
        PatientInfoObj.ExistAnalysisInfo = existingData;
    }
}