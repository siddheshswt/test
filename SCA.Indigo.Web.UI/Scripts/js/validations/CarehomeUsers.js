﻿$jqGrid = jQuery.noConflict();
var jsfilename = "CarehomeUsers.js";

$jqGrid(document).ready(function () {
    $jqGrid("#ddlCarehome").multiselect({ noneSelectedText: $jqGrid("#hdnresDefaultddlCarehome").val() });

    var careHomeId = $jqGrid("#ddlCarehome").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqGrid("#ddlUsers").multiselect({ noneSelectedText: $jqGrid("#hdnresDefaultddlUser").val() });

    var userId = $jqGrid("#ddlUsers").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqGrid("#ddlCustomer").on('change', function (e) {
        var customerId = $jqGrid("#ddlCustomer").val();

        $jqGrid("#ddlUsers").empty();
        $jqGrid("#ddlUsers").multiselect('refresh');
        $jqGrid("#ddlUsers").multiselect("disable");

        if (customerId.length == 0) {
            $jqGrid("#ddlCarehome").empty();
            $jqGrid("#ddlCarehome").multiselect('refresh');
            $jqGrid("#ddlCarehome").multiselect("disable");
        }
        else {
            LoadDDL(customerId, "ddlCarehome");
        }
    });

    $jqGrid("#ddlCarehome").on('change', function (e) {
        var customerId = $jqGrid("#ddlCustomer").val();

        var careHomeId = $jqGrid("#ddlCarehome").multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        if (careHomeId.length == 0) {
            $jqGrid("#ddlUsers").empty();
            $jqGrid("#ddlUsers").multiselect('refresh');
            $jqGrid("#ddlUsers").multiselect("disable");
        }
        else {
            LoadDDL(customerId, "ddlUsers");
        }
    });

    $jqGrid("#btnSubmit").on('click', function (e) {
        var customerId = $jqGrid("#ddlCustomer").val();

        var carehomeId = $jqGrid("#ddlCarehome").multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        var userId = $jqGrid("#ddlUsers").multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        if (customerId == "") {
            CommonScriptObj.DisplayErrorMessage("divMapUserCarehomeErrorMessage", $jqComm("#hdnresCustomerValidationMsg").val(), 'pop_error_ul');
            return false;
        }
        else if (carehomeId.length == 0) {
            CommonScriptObj.DisplayErrorMessage("divMapUserCarehomeErrorMessage", $jqComm("#hdnresCarehomeValidationMsg").val(), 'pop_error_ul');
            return false;
        }
        else if (userId.length == 0) {
            CommonScriptObj.DisplayErrorMessage("divMapUserCarehomeErrorMessage", $jqComm("#hdnresUserValidationMsg").val(), 'pop_error_ul');
            return false;
        }

        $jqGrid('#divSaveCarehomeUsers').dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralYes").val().toString(),
                "id": "hdnGeneralYes",
                click: function () {
                    saveCarehomeUsers();
                    $jqGrid('#divSaveCarehomeUsers').dialog('close');
                },
            }, {
                text: $jqGrid("#hdnGeneralNo").val().toString(),
                "id": "hdnGeneralNo",
                click: function () {
                    $jqGrid('#divSaveCarehomeUsers').dialog('close');
                    return false;
                },
            }]
        });
        $jqGrid('#divSaveCarehomeUsers').dialog('open');
        return false;
    });

    $jqGrid("#btnUndo").on('click', function (e) {
        setTimeout(function () {
            window.location.reload(true);
        }, 100);
    });

    CommonScriptObj.DisableFormControls("frm_MapUserCarehome");
});

function LoadDDL(valueId, elementId) {
    elementId = "#" + elementId;

    $jqGrid(elementId).empty();
    $jqGrid(elementId).multiselect("enable");

    var typeId = 0
    if (elementId == "#ddlCarehome")
        typeId = 1;
    else
        typeId = 2;

    if (typeId != 0) {
        var url = $jqGrid('#hdnLoadDDLAction').val();
        $jqGrid.ajax({
            cache: false,
            type: "GET",
            url: url,
            data: { valueId: valueId.toString(), typeId: typeId },
            async: false,
            cache: false,
            success: function (data) {
                $jqGrid.each(data, function (key, option) {
                    $jqGrid(elementId).append($jqGrid('<option></option>').val(option.Value).html(option.Text));
                });
            },
            error: function (x, e) {
                HandleAjaxError(x, e, jsfilename, "LoadDDL");
            }
        });
    }

    $jqGrid(elementId).multiselect('refresh');
    $jqGrid('.ui-corner-all input[type="checkbox"]').ezMark();
}

function saveCarehomeUsers()
{
    var customerId = $jqGrid("#ddlCustomer").val();

    var careHomeId = $jqGrid("#ddlCarehome").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    var userId = $jqGrid("#ddlUsers").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    var objUserCarehome = { CustomerId: customerId.toString(), CarehomeId: careHomeId.toString(), UserIds: userId.toString() }

    var url = $jqGrid('#hdnSubmitAction').val();
    $jqGrid.ajax({
        cache: false,
        type: "POST",
        url: url,
        data: JSON.stringify({ objUserCarehomeBusinessModel: objUserCarehome }),
        async: false,
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (data) {
            if (data == 1)
            {
                $jqGrid('#divSaveCarehomeUsersSuccess').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                        "id": "hdnGeneralOk",
                        click: function () {
                            $jqGrid('#divSaveCarehomeUsersSuccess').dialog('close');
                            $jqGrid("#btnUndo").trigger("click");
                        }
                    }]
                });
                $jqGrid('#divSaveCarehomeUsersSuccess').dialog('open');
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "LoadDDL");
        }
    });
}