﻿var $jqComm = jQuery.noConflict();
var jsfilename = "Notification.js";

function ClearNotifications(type) {
    sessionStorage.session = undefined;
    $jqComm.connection.hub.start().done(function () {
        $jqComm.connection.signalR.server.clearNotification($jqComm("#hdnUserId").val());
    });
    ShowNotification();
}

function ShowNotification() {
    var divNotification = $jqComm("#notification");
    var divinner_Notification = $jqComm("#notification_body");
    var html = "";
    var counthtml = "";
   

    if (sessionStorage.session != undefined && sessionStorage.session != "undefined") {
        var session = JSON.parse(sessionStorage.session);

        if (session.InteractionCount > 0) {
            html += "<div class='notify_cell'>" +
                        "<a href='/Home/Welcome' onclick='ClearNotifications('NOTIFICATIONCLICK'); return MenuClick(this);'>" +
                            "<span><i class='fa fa-envelope-o'></i></span>" +
                            "<span>" + session.InteractionCount + " " + $jqComm("#hdnresNotificationInteractionAssigned").val() + "</span>" +
                        "</a>" +
                    "</div>";

            counthtml = "<span id='allCount'>" + session.InteractionCount + "</span> ";
            $jqGrid("#allCount").remove();
            $jqGrid(".notification-icon").append(counthtml);
            $jqGrid(".notification-icon").removeAttr("disabled");
        }
    }
    else {
        $jqGrid(".notification-icon").attr("disabled", "true");
    }
    divinner_Notification.hide();
    divinner_Notification.empty();

    divinner_Notification.append(html);

}

function LogOutFromSignalR() {
    $jqComm.connection.signalR.server.disconnect($jqComm("#hdnUserId").val());
}

function SendInteractionNotificationToUser(userId) {
    $jqComm.connection.signalR.server.sendNotificationForInteraction(userId);
}

$jqComm(document).ready(function () {
    $jqComm(function () {
        ShowNotification();
    });

    $jqComm(function () {

        var app = $jqComm.connection.signalR;

        app.client.sendNotification = function (notification) {
            sessionStorage.setItem("session", notification);
            ShowNotification();
        }

        // Start the connection.
        $jqComm.connection.hub.start().done(function () {
            session = null
            if (sessionStorage.session != undefined && sessionStorage.session != "undefined")
                session = sessionStorage.getItem("session");            
            app.server.register($jqComm("#hdnUserId").val(), $jqComm.connection.hub.id, session);
        });
    });
});