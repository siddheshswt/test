﻿$jqGrid = jQuery.noConflict();
var HomeScriptObj = new HomeScript();
function HomeScript() { }

HomeScript.prototype.JqInteractionSummaryHome="JqInteractionSummaryHome";

$jqGrid(document).ready(function () {

    // Show Hide the Interaction Grid
    $jqGrid(".home-interaction-div").click(function () { $jqGrid(".homeInteraction").hide(); });

    $jqGrid("#jqgh_jqInteractionSummaryHome_CustomerName").bind("click", function () {

    });

    // Get for Interaction Alert
    HomeScriptObj.GetInteactionAlert();

});

HomeScript.prototype.ShowAsLink = function (cellvalue, options, rowobject) {
    if (cellvalue == null) {
        cellvalue = "";
        return "";
    }
    var rowId = options['rowId'];
    return "<a  id = InteractionId" + options['rowId'] + " onclick='HomeScriptObj.GetInteracionIdToShowCreateInteraction(" + rowId + ")'>" + cellvalue + "</a>"
}

HomeScript.prototype.GetInteactionAlert = function () {
    $jqGrid(".homeInteraction").hide();
    $jqGrid("#JqInteractionSummaryHome").jqGrid('clearGridData');
    $jqGrid.ajax({
        url: '/Patient/GetInteractionSummaryDetails',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        data: {
            HomePageAlert: '1'
        },
        cache: false,
        async: false,
        success: function (responseData) {
            $jqGrid(".homeInteraction").show();
            $jqGrid("#jqInteractionSummaryHome").jqGrid('clearGridData');
            if (responseData.Interactions != null)
            {
                $jqGrid("#jqInteractionSummaryHome").jqGrid('setGridParam', { datatype: 'local', data: responseData.Interactions }).trigger('reloadGrid');
            }
        }
    });
}

HomeScript.prototype.GetInteracionIdToShowCreateInteraction = function (rowId)
{    
    var rowData = $jqGrid("#jqInteractionSummaryHome").getRowData(rowId);
    var interactionId = rowData["InteractionId"];
    InteractionObj.ShowCreateInteraction(true, interactionId, true, 'Home');
}