﻿$jqGrid = jQuery.noConflict();
$jqGrid(document).ready(function () {
    var IsOrderActivated;
    var valFalse = "FALSE";
    var valTrue = "TRUE";

    if (window.patientId == 0 || window.patientId == undefined || window.isPatientInfoEditable.toString().toUpperCase() == valFalse) {

        DisablePatientInfoButtons();
    }
    if ($jqGrid('#hdnOrderSavedFromOneOff').val() == "True" || window.redirectPage == $jqGrid('#hdnOrderPage').val()) {

        HideApprovalDiv();
        DisablePatientInfoPage();

        DisableCancelButton();
        DisableSaveButton();
        EnableEditButton();

        DisablePrescriptionButtons();
    }
    else {

        DisableEditButton();
        if ($jqGrid("#hdnIsredirectFromApproval").val() == 1) {

        }
    }
   
    DisableViewPatientDetailsStatus();

    $jqGrid(".BtnEditEnable").click(function () {
        EnablePatientInfoPage();
        DisableEditButton();
        EnableSaveButton();
        EnableCancelButton();
        EnablePatientInfoButtons();
        EnablePrescriptionButtons();
        EnableCorrespondenceAddressForCopyPatient();

        if (window.isPatientInfoEditable.toString().toUpperCase() == valTrue && !($jqGrid('.patientinfo_fields .DisablePages').hasClass("disable_div"))) {

            EnablePatientInfoButtons();
        }

        if (window.RoleName != "" && window.RoleName != undefined) {
            if (window.RoleName == "Nurse") {
                DisablePatientInfoPage();
            }
        }

        //if a particular user role has access to PatientInfo, make it editable
        if (window.isPatientInfoEditable.toString().toUpperCase() == valTrue) {
            //patient info page is editable
            $jqGrid('.patientinfo_fields .DisablePages').removeClass("disable_div");
        }
        else
        {
            $jqGrid('.patientinfo_fields .DisablePages').addClass("disable_div");
            DisablePatientInfoButtons();
        }

        if (window.isMedicalInfoEditable.toString().toUpperCase() == valTrue) {
            //Patient analysis and contact info are editable
            $jqGrid('.staff_select_mode .medicalAnalysis_nformation_table .DisablePages').removeClass("disable_div");
            $jqGrid('.analysis_information .medicalAnalysis_nformation_table .DisablePages').removeClass("disable_div");
        }
        else {
            $jqGrid('.staff_select_mode .medicalAnalysis_nformation_table .DisablePages').addClass("disable_div");
            $jqGrid('.analysis_information .medicalAnalysis_nformation_table .DisablePages').addClass("disable_div");

            $jqGrid('.staff_select_mode .disable_msg').css('display', 'block');
            $jqGrid('.analysis_information .disable_msg').css('display', 'block');
        }

        if (window.isPrescriptionEditable.toString().toUpperCase() == valTrue) {
            //Prescription is editable
            $jqGrid('.prescription_nformation_table .DisablePages').removeClass('disable_div');
            EnablePrescriptionButtons();
        }
        else {
            $jqGrid('.prescription_nformation_table .DisablePages').addClass('disable_div');
            DisablePrescriptionButtons();
        }

        //if (window.isPrescriptionEditable.toString().toUpperCase() == valFalse) {
        //    DisablePrescriptionButtons();
        //}

        //if (window.isPatientInfoEditable.toString().toUpperCase() == valFalse) {
        //    DisablePatientInfoButtons();
        //}


        if (window.isPrescriptionEditable.toString().toUpperCase() == valTrue) {
            $jqGrid.ajax({
                url: '/Patient/GetOrderActivatedSession',
                async: false,
                cache: false,
                success: function (response) {
                    IsOrderActivated = response;
                }
            })

            if (IsOrderActivated == "True") {

                //Set Patient Type dropdown and Carehome button disabled if order is active
                $jqGrid('#divPrescription').addClass("disable_div");

                $("#ddlPatientType").attr('disabled', 'disabled');
                $jqGrid('#btnCarehome').addClass('btn_disable');
                $jqGrid('#btnCarehome').attr('disabled', 'disabled');

                DisablePrescriptionButtons();
                $jqGrid('#divOrderActivated').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                        "id": "hdnGeneralOk",
                        click: function () {
                            $jqGrid('#divOrderActivated').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divOrderActivated').dialog('open');
            }
            else if (($jqGrid("#ddlPatientStatus").val() == $jqGrid("#hdnStoppedPatient").val() && $jqGrid("#ddlReasonCode").val() == $jqGrid("#hdnWaitingForApproval").val()) //stopped & waiting for approval
                        && ($jqGrid("#hdnIsredirectFromApproval").val() != 1)
                        && isPrescriptionEditable.toString().toUpperCase() == valTrue.toUpperCase())//Prescription is accessible for current role
            {
                $jqGrid('#divPrescription').addClass("disable_div");
                DisablePrescriptionButtons();

                $jqGrid('#divPrescriptionApprovalPopupMsg').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                            $jqGrid('#divPrescription').removeClass("disable_div");
                            EnablePrescriptionButtons();
                            $jqGrid('#divPrescriptionApprovalPopupMsg').dialog('close');
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divPrescriptionApprovalPopupMsg').dialog('close');
                        },
                    }]
                });

                $jqGrid('#divPrescriptionApprovalPopupMsg').dialog('open');
            }
            else {
                EnablePrescriptionButtons();
            }
        }

        if (window.countOfMandatoryAnalysisField > 0) {
            if ($("#jqMedicalStaffAnalysis tr").length == 0)
                LoadMedicalAnalysisandStaffInfo(false);

            if($("#jqPatientAnalysis tr").length == 0)
                LoadMedicalAnalysisandStaffInfo(true);
        }
    });

    $jqGrid(".BtnExitSearch").click(function () {

        if (((window.isMedicalInfoEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.staff_select_mode .medicalAnalysis_nformation_table .DisablePages').hasClass('disable_div'))) ||
            ((window.isPatientInfoEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.patientinfo_fields .DisablePages').hasClass('disable_div'))) ||
            (window.isPrescriptionEditable.toString().toUpperCase() == valTrue) && !($jqGrid('.prescription_nformation_table .DisablePages').hasClass('disable_div'))) {
            if (window.patientId != 0 && window.patientId != undefined) {
                $jqGrid('#divCancelPrompt').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                                    $jqGrid('#divCancelPrompt').dialog('close');
                                    CancelFromSearch();
                            try {
                                ClearPatientChanged();
                            } catch (err) { }
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divCancelPrompt').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divCancelPrompt').dialog('open');
            }
            else {
                $jqGrid('#divCancelPrompt').dialog({
                    autoOpen: false,
                    modal: true,
                    closeOnEscape: false,
                    width: 'auto',
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralYes").val().toString(),
                        "id": "hdnGeneralYes",
                        click: function () {
                                    $jqGrid('#divCancelPrompt').dialog('close');
                                    CancelFromAddPatient();
                        },
                    }, {
                        text: $jqGrid("#hdnGeneralNo").val().toString(),
                        "id": "hdnGeneralNo",
                        click: function () {
                            $jqGrid('#divCancelPrompt').dialog('close');
                        },
                    }]
                });
                $jqGrid('#divCancelPrompt').dialog('open');
            }
        }
        else {
            return true;
        }
    });    
  
});//End of Document.ready 

function DisableViewPatientDetailsStatus()
{
    if (window.patientId > 0) {

        //Check for the Patient View Details ViewOnly or Full Rights
        var viewPatientElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnViewPatientMenuText").val() });
        // Sub menu available
        if (viewPatientElement.length == 0) {
            // SubMenu not available. Need to show error message.
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnViewPatientAllowMessage").val(), 'pop_error_ul');
            return false;
        }
        else {
            var fullRights = $jqGrid(viewPatientElement).attr(ISFULLRIGHTS);
            var viewOnly = $jqGrid(viewPatientElement).attr(ISVIEWONLY);
            if (viewOnly == "True" && fullRights == "False") {
                setTimeout(function () {
                    $jqGrid("#divPatientMenuDetails").addClass("read_only_div");
                    $jqGrid("#divPatientMenuDetails").attr('disabled', 'disabled');
                    $jqGrid("#divPatientMenuDetails").attr('readonly', 'true');
                }, 200);
            }
        }
    }
}

function CancelFromSearch() {    
    if (window.patientId != 0 && window.patientId != undefined) {
        $jqGrid.ajax({
            url: '/Patient/LoadPatientDetails',
            type: 'POST',
            cache: false,
            async: false,
            data: { patientId: window.patientId, customerId: window.customerId, PatientName: window.patientName, CustomerName: window.customerName },
            success: function (data) {
                SetPatientDetails(data);

                EnableEditButton();
                DisableSaveButton();
                DisableCancelButton();
                ClearNurseComments();
                DisableNurseComments();
                $jqGrid('.collapse').css('display', 'block');
                $jqGrid('.heading-1 a').addClass('open');
                DisablePatientInfoPage();

                EnablePatientInfoButtons();

                window.CustomerParamets = data.CustomerParameters;
                ReloadMedicalStaffGrid();
                $jqGrid("#jqPatientPrescription").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                $jqGrid('#btnSave').addClass("btn_disable");
                $jqGrid('#btnDownSave').addClass("btn_disable");
                if ($jqGrid("#hdnIsredirectFromApproval").val() == 1) {
                    $jqGrid('#txtPIN').val('');
                    $jqGrid("#prescriptionMessage").hide();
                }

            }
        });
        ShowCustomerPatientName();
    }


}


function CancelFromAddPatient() {   
    window.location.href = "/Patient/Details";
}

function EnablePatientInfoButtons() {
    if (isCopyDetails == false || isCopyDetails == "" || isCopyDetails == undefined) { // Setting the length to zero so not to enable button's for copy patient
        if (PatientInfoObj.AttachmentAccess.length != 0) {
            $jqGrid('#btnAttachment').removeClass("btn_disable");
            $jqGrid('#btnAttachment').removeAttr("disabled");
        }
        if (PatientInfoObj.ContactInfoAccess.length != 0) {
            $jqGrid('#btnContactInfo').removeClass("btn_disable");
            $jqGrid('#btnContactInfo').removeAttr("disabled");
        }
        if (PatientInfoObj.NotesAccess.length != 0) {
            $jqGrid("#btnNotes").removeClass("btn_disable");
            $jqGrid('#btnNotes').removeAttr("disabled");
        }
        if (PatientInfoObj.RecentOrdersAccess.length != 0) {
            $jqGrid("#btnRecentOrder").removeClass("btn_disable");
            $jqGrid('#btnRecentOrder').removeAttr("disabled");
        }
        if (PatientInfoObj.SampleOrdersAccess.length != 0) {
            $jqGrid('#btnSampleOrder').removeClass("btn_disable");
            $jqGrid('#btnSampleOrder').removeAttr('disabled');
        }
        if (PatientInfoObj.InteractionAccess.length != 0) {
            $jqGrid('#btnInteractionsummary').removeClass("btn_disable");
            $jqGrid('#btnInteractionsummary').removeAttr("disabled");
        }
        if (PatientInfoObj.AuditLogAccess.length != 0) {
            $jqGrid('#btnPatientAuditLog').removeClass("btn_disable");
            $jqGrid('#btnPatientAuditLog').removeAttr('disabled', 'disabled');
        }
    }
}

function DisablePatientInfoButtons() {

    $jqGrid('#btnAttachment').addClass("btn_disable");
    $jqGrid('#btnAttachment').attr('disabled', 'disabled');
    $jqGrid('#btnContactInfo').addClass("btn_disable");
    $jqGrid('#btnContactInfo').attr('disabled', 'disabled');
    $jqGrid("#btnNotes").addClass("btn_disable");
    $jqGrid('#btnNotes').attr('disabled', 'disabled');
    $jqGrid("#btnRecentOrder").addClass("btn_disable");
    $jqGrid('#btnRecentOrder').attr('disabled', 'disabled');
    $jqGrid('#btnSampleOrder').addClass("btn_disable");
    $jqGrid('#btnSampleOrder').attr('disabled', 'disabled');
    $jqGrid("#btnInteractionsummary").addClass("btn_disable");
    $jqGrid('#btnInteractionsummary').attr('disabled', 'disabled');
    $jqGrid('#btnPatientAuditLog').addClass("btn_disable");
    $jqGrid('#btnPatientAuditLog').attr('disabled', 'disabled');   
}

function EnablePrescriptionButtons() {

    $jqGrid('#btnAddProduct').removeClass("btn_disable");
    $jqGrid('#btnAddProduct').removeAttr("disabled");

    DisableChangeNDD(false);
    //   $jqGrid('#changeNDDPrescription').removeClass("btn_disable");
    //  $jqGrid('#changeNDDPrescription').removeAttr("disabled");

}

function DisablePrescriptionButtons() {

    $jqGrid('#btnAddProduct').addClass('btn_disable');
    $jqGrid('#btnAddProduct').attr('disabled', 'disabled');

    DisableChangeNDD(true);
    //  $jqGrid('#changeNDDPrescription').addClass('btn_disable');
    //  $jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
}

function EnableSaveButton() {
    $jqGrid('.ClassSavePatient').removeClass('btn_disable');
    $jqGrid('.ClassSavePatient').removeAttr('disabled');

}

function DisableSaveButton() {

    $jqGrid('.ClassSavePatient').addClass("btn_disable");
    $jqGrid('.ClassSavePatient').attr('disabled', 'disabled');

}

function EnableEditButton() {
    //Enable Edit Button only when carehome not removed 
    var isRemoved = $("#hdnIsCarehomeRemoved").val();
    if (isRemoved == "false") {
        $jqGrid('.BtnEditEnable').removeClass("btn_disable");
        $jqGrid('.BtnEditEnable').removeAttr('disabled');
    }
}

function DisableEditButton() {

    $jqGrid('.BtnEditEnable').addClass("btn_disable");
    $jqGrid('.BtnEditEnable').attr('disabled', 'disabled');
}

function EnableCancelButton() {

    $jqGrid('.BtnExitSearch').removeClass("btn_disable");
    $jqGrid('.BtnExitSearch').removeAttr('disabled');
}

function DisableCancelButton() {

    $jqGrid('.BtnExitSearch').addClass("btn_disable");
    $jqGrid('.BtnExitSearch').attr('disabled', 'disabled');

}


function ShowApprovalDiv() {
    $jqGrid("#divPrescriptionApprovalbtn").show();
}

function HideApprovalDiv() {

    $jqGrid("#divPrescriptionApprovalbtn").hide();
}

function BackToHomepage()
{
    $jqGrid.ajax({
        url: '/Patient/GetHomePageFlag',
        type: 'POST',
        dataType: 'Json',
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            window.location.href = data;
        }
    });
}