﻿$jqGrid = jQuery.noConflict();
function showPopupRecentOrders(recentOrderType) {
    var searchId = '';
    if (recentOrderType == $('#hdnRecentOrderTypePatient').val())
        searchId = $('#PatientId').text();
    else if (recentOrderType == $('#hdnContactTypeCareHome').val())
        searchId = $('#hdnCareHomeId').val();
    else if (recentOrderType == $('#hdnContactTypeCustomer').val())
        searchId = 0;
    $jqGrid("#jqPatientRecentOrderInfoPopup").jqGrid('clearGridData');
    $jqGrid('#divPatientRecentOrdersPopup').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: $jqGrid('#hdnresRecentOrderTitle').val()
    });
    $jqGrid('#divPatientRecentOrdersPopup').dialog('open');  
    $jqGrid("#jqPatientRecentOrderInfoPopup").jqGrid('clearGridData');
    $jqGrid("#jqPatientRecentOrderInfoPopup").jqGrid('setGridParam', { postData: { searchId: searchId, recentOrderType: recentOrderType }, datatype: 'json', sortname: 'OrderDate', sortorder: 'desc' }).trigger('reloadGrid');
}