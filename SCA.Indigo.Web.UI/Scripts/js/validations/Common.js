﻿$jqGrid = jQuery.noConflict();
var ISFULLRIGHTS = "isfullrights";
var ISVIEWONLY = "isviewonly";
var carehomePage = "Carehome";
var detailsPage = "Details";
var searchPage = "Search";
var jsfilename = "Common.js";

CommonScriptObj = new CommonScript();
function CommonScript() { }
CommonScriptObj.PatientId = "";
CommonScriptObj.CustomerId = "";
CommonScriptObj.SAPCustomerNumber = "";
CommonScriptObj.SAPPatientNumber = "";
CommonScriptObj.SAPCareHomeNumber = "";
CommonScriptObj.PatientName = "";
CommonScriptObj.CareHomeName = "";
CommonScriptObj.CareHomeId = "";
CommonScriptObj.CustomerName = "";
CommonScriptObj.LogisticProviderName = "";
CommonScriptObj.Description = "";
CommonScriptObj.DisplayAsAlert = "";
CommonScriptObj.InteractionTypeId = "";
CommonScriptObj.InteractionSubTypeId = "";
CommonScriptObj.StatusId = "";
CommonScriptObj.InteractionId = "";

//Enable Disable Control
CommonScript.prototype.EnableDisableFileUpload = function (dropDownId, fileControlId, buttonId) {
    var ddlUpload = $jqGrid("#" + dropDownId).val();
    CommonScriptObj.clear_upload(fileControlId);
    $jqGrid("#" + fileControlId).val('');

    $jqGrid("#" + buttonId).addClass("btn_disable");
    $jqGrid("#" + buttonId).attr('disabled', 'disabled');

    if (ddlUpload > 0) {
        $jqGrid("#" + fileControlId).removeClass("btn_disable");
        $jqGrid("#" + fileControlId).removeAttr("disabled");
    }
    else {
        $jqGrid("#" + fileControlId).addClass("btn_disable");
        $jqGrid("#" + fileControlId).attr('disabled', 'disabled');
    }
}

$jqGrid(document).ready(function () {
    $jqGrid(".j_lightbox_click").click(function () {
        $jqGrid("#j_lightbox").show();
    });

    $jqGrid(".j_lightbox_close").click(function () {
        $jqGrid("#j_lightbox").hide();
    });

    $jqGrid(document).keydown(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            $jqGrid("#j_lightbox").hide();
        }
    });
});

function ResetWaterMarkValue(id, waterMarkVal) {
    var val = $jqGrid("#" + id).val();
    if (val == waterMarkVal || val == undefined || val == null || val == 'undefined') {
        $jqGrid("#" + id).val('');
    }
    return false;
}

function ShowHideError() {
    $jqGrid("#j_lightbox_error").show();
    $jqGrid(".j_lightbox_error_close").click(function () {
        $jqGrid("#j_lightbox_error").hide();
    });
    $jqGrid(".j_lightbox_error_ok").click(function () {
        $jqGrid("#j_lightbox_error").hide();
    });
}

function ClosePopup(id) {    
    if (window.gridid == "jqPatientPrescription") {
        $jqGrid('#hdnFromAddProduct').val("0");
    }
    if (id == "divNotesPopup") {
        ExitWarningMessage(id);
    } else {
        $jqGrid('#' + id).dialog('close');
    }
}

function ExitWarningMessage(id, e) {
    if (isDirty) {
        $jqGrid("#divValidationConfirmBeforeExit").dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            resizable: false,
            closeOnEscape: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralYes").val(),
                click: function () {
                    $jqGrid(this).dialog("close");
                    window.IsNotesPopupOpen = false;
                    try {
                        e.click();
                        return;
                    }
                    catch (err) { }
                    try {
                        isForceClose = true;
                        $jqGrid('#' + id).dialog('close');
                    }
                    catch (err) { }
                },
            },
            {
                text: $jqGrid("#hdnGeneralNo").val(),
                click: function () {
                    $jqGrid(this).dialog("close");
                },
            }
            ]
        });
        $jqGrid("#divValidationConfirmBeforeExit").dialog('open');
        return false;
    } else {
        isForceClose = true;
        $jqGrid('#' + id).dialog('close');
        return true;
    }
}

function HideLightBoxErrorPopup(id) {
    $jqGrid('#' + id).hide();
}

//use this function to restrict special characters that WCF rest service do not accept in parameter string
function isInValidSpecialCharacter(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 35 //#
        || charCode == 37 //%
        || charCode == 38 //&
        || charCode == 39 //'
        || charCode == 42 //*
        || charCode == 43 //+
        || charCode == 58 //:
        || charCode == 60 //<
        || charCode == 62 //>
        || charCode == 63 //?
        || charCode == 92 //\
        ) {        
        return true;
    }
    else {        
        return false;
    }   
}

function isApostrophe(evt)
{ 
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 39)
    {
        return true;
    }
    else
    {
       return false;
    }

}

//Use this function to Validate Numeric input to textbax
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 32)
        return true;
    if (charCode > 32 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

//allow 0 to 9 only & backspace  & delete
function isNumeric(charCode) {
    if ((charCode >= 48 && charCode <= 57) || charCode == 8) {
        return true;
    }
    else {
        return false;
    }
}

//allow 0 to 9 only & backspace  & delete & dot
function isDecimal(charCode) {
    if ((charCode >= 48 && charCode <= 57) || charCode == 8 || charCode == 46) {
        return true;
    }
    else {
        return false;
    }
}

//Use this function to Validate Alpha input to textbox
function isAlphaKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
        return true;
    else
        return false;
}


//Method to check validate email address
function ValidateEmail(email) {
    var re = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    return re.test(email);
}


function selectUnselectall(classId, childclass) {
    $jqGrid('#' + classId).click(function (event) {
        if ($jqGrid('#' + childclass).is(":checked")) {
            $jqGrid('.' + childclass).prop("checked", true);
        } else {
            $jqGrid('.' + childclass).prop("checked", false);
        }

    });
}

// This Function will disable all controls in the given element as per Viewonly property given in linkid
CommonScript.prototype.DisableFormControls = function (element) {    
    var activeMenus = $jqGrid(".active_menu");
    var linkEle;
    var fullRights;
    var viewOnly;
    var menuText = $jqGrid("#hdnCurrentActionClicked").val();    
    linkEle = $(".active_menu").filter(function (index) { return $(this).text() === menuText; });
    fullRights = $jqGrid(linkEle).attr(ISFULLRIGHTS);
    viewOnly = $jqGrid(linkEle).attr(ISVIEWONLY);
    $jqGrid("#hdnIsViewOnly").val(viewOnly);
    $jqGrid("#hdnIsFullRights").val(fullRights);
    if (viewOnly == "True" && fullRights == "False") {
        setTimeout(function () {
            $jqGrid("#" + element).addClass("read_only_div");
            $jqGrid("#" + element).attr('disabled', 'disabled');
            $jqGrid("#" + element).attr('readonly', 'true');
        }, 200);
    }
}


function SetMatchedValueAsSelected(ddlid, text) {
    var ddl = document.getElementById(ddlid);
    var selectedflag = false;
    for (var i = 0; i < ddl.options.length; i++) {
        if (ddl.options[i].text.toLowerCase() == text.toLowerCase()) {
            ddl.options[i].setAttribute('selected', 'selected');
            selectedflag = true;
        }
        else {
            ddl.options[i].removeAttribute('selected');
        }
    }
    if (!selectedflag) {
        ddl.selectedIndex = 0;
    }
}

// This will build generic message format
CommonScript.prototype.DisplayErrorMessage = function (divId, messageToDisplay, cssClass) {
    //pop_error_ul
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='" + cssClass + "'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";
    errorMsg += errorMsgUl + errorMsgLi + messageToDisplay + errorMsgLiclose + errorMsgUlclose;
    $jqGrid("#" + divId).html(errorMsg);
    CommonScriptObj.OpenDialog(divId);
    return false;
}

// This will open message in common dialog
CommonScript.prototype.OpenDialog = function (id) {
    var dialogOptions = {
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        closeOnEscape: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralOk").val().toString(),
            "id": "hdnGeneralOk",
            click: function () {
                $jqGrid(this).dialog('destroy');
                $(".disable_pop").hide();
                $(".ui-dialog-titlebar-close").removeClass('btn_disable');
                if ($("#hdnIsReloadCarehome").val() == "true") {
                    $("#hdnIsReloadCarehome").val(false);
                    window.location.href = "/CareHome/GetCareHome";
                }
            },
        }]
    };
    $jqGrid('#' + id).dialog(dialogOptions).dialog('open');
}

// SSO- Integration : Handle the Session Expired button click event & LogOff button Click event
CommonScript.prototype.RedirectToSessionExpiredOrLogout = function () {
    var isInternalUser = localStorage.getItem("IsInternalUser");
    if(isInternalUser == undefined || isInternalUser == "")
    {
        isInternalUser = "true";
    }   
    $jqSession('#j_lightbox_session').hide();
    window.location.href = "/Account/SessionExpiredOrLogOut?isInternalUser=" + isInternalUser;
}

$jqGrid(".btnsessionexpired").click(function () {
    CommonScriptObj.RedirectToSessionExpiredOrLogout();
});

$jqGrid(".btnsessionLogOff").click(function () {
    CommonScriptObj.RedirectToSessionExpiredOrLogout();
});


//Get Query string data from url
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function GetHolidayList() {
    var holidayDaysList = [];
    $jqGrid.ajax({
        url: '/Common/GetHolidayList',
        type: 'GET',
        cache: false,
        async: false,
        success: function (responseData) {                       
            if (responseData.length > 0)
                for (var i = 0; i < responseData.length; i++) {
                    holidayDaysList.push(ConvertDateToMMDDYYYY(new Date(responseData[i].HolidayDateStringFormat)));
                }

        }
    });
    return holidayDaysList;
}

function ConvertDateToMMDDYYYY(dateText) {
    dateText = new Date(dateText);
    var dd = ("0" + dateText.getDate()).slice(-2);
    var mm = ("0" + (dateText.getMonth() + 1)).slice(-2);
    var yyyy = dateText.getFullYear();
    return mm + '/' + dd + '/' + yyyy;
}//end function

function DisableChangeNDD(isDisable) {
    if (isDisable == true) {
        $jqGrid('#changeNDDPrescription').addClass("btn_disable");
        $jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
        $jqGrid('#btnPrescriptionCalculate').addClass("btn_disable");
        $jqGrid('#btnPrescriptionCalculate').attr('disabled', 'disabled');
    }
    else {
        $jqGrid('#btnPrescriptionCalculate').removeClass("btn_disable");
        $jqGrid('#btnPrescriptionCalculate').removeAttr('disabled', 'disabled');
        $.ajax({
            url: '/Order/IsNddAllowed',
            contentType: "application/json",
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data == true && isDisable == false) {
                    $jqGrid('#changeNDDPrescription').removeClass("btn_disable");
                    $jqGrid('#changeNDDPrescription').removeAttr("disabled");

                }
                else {
                    $jqGrid('#changeNDDPrescription').addClass("btn_disable");
                    $jqGrid('#changeNDDPrescription').attr('disabled', 'disabled');
                }
            }
        });
    }
}

// Used to fill the data in Recent Order grid on Interaction summary Popup
CommonScript.prototype.CallRecentOrderPopup = function (recentOrderType) {
    var data = $jqGrid("#hdnInteractionRecentOrderValue").val();
    var searchId = '';
    if (recentOrderType == $('#hdnRecentOrderTypePatient').val()) {
        searchId = $('#PatientId').text();
    }
    else if (recentOrderType == $('#hdnContactTypeCareHome').val()) {
        searchId = $('#hdnCareHomeId').val();
    }
    else if (recentOrderType == $('#hdnContactTypeCustomer').val()) {
        searchId = 0;
    }
    $jqGrid("#jqPatientInteractionRecentOrderInfoPopup").jqGrid('clearGridData');
    $jqGrid("#jqPatientInteractionRecentOrderInfoPopup").jqGrid('setGridParam', { postData: { searchId: searchId, recentOrderType: recentOrderType }, datatype: 'json', sortname: 'OrderDate', sortorder: 'desc' }).trigger('reloadGrid');
    return false;
}

CommonScript.prototype.ClearPatientSession = function () {
    $jqGrid.ajax({
        url: '/Common/ClearPatientSession',
        type: 'GET',
        async: false,
        cache: false,
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {
            return false;
        },
        error: function (data) {

        }
    });

    return false;
}

//Function to have first character upper
$jqGrid(".firstCharUpper").blur(function (e) {
    if ($(this).val() != "" || $(this).val() != undefined) {
        $(this).val($(this).val().slice(0, 1).toString().toUpperCase() + $(this).val().slice(1).toString());
    }
});


CommonScript.prototype.DateTextToMMDDYYYY = function (dateText) {
    var mmddyyyDate = new Date();
    var ddmmyyyy = dateText.split("/");
    mmddyyyDate.setDate(ddmmyyyy[0]);
    mmddyyyDate.setMonth(ddmmyyyy[1] - 1);
    mmddyyyDate.setFullYear(ddmmyyyy[2]);
    var dd = ("0" + mmddyyyDate.getDate()).slice(-2);
    var mm = ("0" + (mmddyyyDate.getMonth() + 1)).slice(-2);
    var yyyy = mmddyyyDate.getFullYear();
    return mm + '/' + dd + '/' + yyyy;
}

//Returns the date in mm/dd/yyyy format if the input parameter is a Valid Date else returns false
CommonScript.prototype.isValidDate = function (dateText) {
    dateText = dateText.split("/");
    var fromYear = 1900;
    var invalidDate = "Invalid Date";
    if (dateText.length == 3) {
        var day = dateText[0];
        var month = dateText[1];
        var year = dateText[2];
            if (year.length != 4) {
                return false;
            }
            else if (parseInt(year) < parseInt(fromYear)) {
                return false;
            }
            else {
                    var newDate = month + "/" +day + "/" +year;
                    var parsedDate = new Date(newDate);
                    var newMonth = parsedDate.getMonth() +1;
                    if (newMonth != month) {
                        return false;
                    }
                    else if (invalidDate == new Date(newDate)) {
                        return false;
                    }
                    else
                    {
                            if (month.length == 1) {
                                appendZeroToMonth = '0' +month.toString();
                            }
                            else {
                                appendZeroToMonth = month;
                            }
                            if (day.length == 1) {
                                appendZeroToDay = '0' +day.toString();
                            }
                            else {
                                appendZeroToDay = day;
                            }
                            return (appendZeroToMonth + "/" +appendZeroToDay + "/" +year);
                    }
              }       
    }
    else {
        return false;
    }
}

function ConvertDateFormat(dateText) {
    if (dateText instanceof Date && !isNaN(dateText.valueOf())) {
        dateText = new Date(dateText);
        var dd = ("0" + dateText.getDate()).slice(-2);
        var mm = ("0" + (dateText.getMonth() + 1)).slice(-2);
        var yyyy = dateText.getFullYear();
        return dd + '/' + mm + '/' + yyyy;
    }
    else {
        var calDate = new Date();
        var dateText = dateText.split("/");
        calDate.setDate(dateText[0]);
        calDate.setMonth(dateText[1] - 1);
        calDate.setFullYear(dateText[2]);
        calDate.setDate(calDate.getDate());
        var dd = ("0" + calDate.getDate()).slice(-2);
        var mm = ("0" + (calDate.getMonth() + 1)).slice(-2);
        var yyyy = calDate.getFullYear();
        return dd + '/' + mm + '/' + yyyy;
    }
}//end function

function rtrim(str) {
    return str.replace(/\s+$/, "");
}
function ltrim(str) {
    return str.replace(/^\s+/, "");
}


function rtrim(str) {
    return str.replace(/\s+$/, "");
}
function ltrim(str) {
    return str.replace(/^\s+/, "");
}

//Empty body method for multiselect
//function SetNoCommunicationOnCheckAll(flag, dropdwon) { }

// Used to clear the file upload control in IE Browser
CommonScript.prototype.clear_upload = function (id) {
    var element = document.getElementById(id);
    element.innerHTML = element.innerHTML;
}

//****************Start Function : ChkDoNotShowAlert*********************************
function ChkDoNotShowAlert() {
    var page = $jqGrid("#hdnAlertNotePage").val();
    if (page == detailsPage || page == searchPage) {
        var urlvalue = '/Patient/SetSessionForDoNotStop';
        AjaxcallForDoNotShowAlert(urlvalue);
    } else if (page == carehomePage) {
        var urlvalue = '/Carehome/SetSessionForDoNotStop';
        AjaxcallForDoNotShowAlert(urlvalue);
    }
}
//****************End Function : ChkDoNotShowAlert***********************************

//****************Start Function : AjaxcallForDoNotShowAlert*********************************
function AjaxcallForDoNotShowAlert(urlvalue) {
    $jqGrid.ajax({
        url: urlvalue,
        type: 'POST',
        cache: false,
        data: {
            displayAsAlert: $jqGrid("#chkDoNotShow").is(':checked')
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "AjaxcallForDoNotShowAlert");
        }
    });
}
//****************End Function : AjaxcallForDoNotShowAlert***********************************
