﻿
$jqGrid = jQuery.noConflict();

var AttachmentObj = new Attachment();
function Attachment() { }

Attachment.prototype.FileData = new FormData();
Attachment.prototype.RemoveAllAttachment = 'chkRemoveAllAttachment';
Attachment.prototype.RemoveAttachmentRecord = "divAttachmentProcess";
Attachment.prototype.ErrorMessageForCustomerInformation = "divAttachmentError";
Attachment.prototype.GeneralOk = "hdnGeneralOk";
Attachment.prototype.AttachmentGrid = "jqAttachmentGird";
Attachment.prototype.isTrackDirty = false;
Attachment.prototype.GeneralCancel = "hdnGeneralCancel";
Attachment.prototype.ArrayAttachmentId = [];
Attachment.prototype.SelectedFilesCount = 0;
Attachment.prototype.AttachmentFileControl = "AttachmentFileControl";
Attachment.prototype.RemoveAllAttachmentChecked = false;
Attachment.prototype.Jqgh_jqAttachmentGird_Remove = "jqgh_jqAttachmentGird_Remove";
Attachment.prototype.FileNotFound = "hdnAttachmentFileNotFound";

$jqGrid(document).ready(function () {

    $jqGrid("#" + AttachmentObj.RemoveAllAttachment).bind('click', function (e) {        
        AttachmentObj.RemoveAllAttachmentChecked = $jqGrid("#" + AttachmentObj.RemoveAllAttachment).is(':checked');
        setTimeout(function () {            
            AttachmentObj.ArrayAttachmentId = [];
            if (AttachmentObj.RemoveAllAttachmentChecked) {
                $jqGrid(".chkRemoveClass").unbind('onchange');
                $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", true);
                $jqGrid(".chkRemoveClass").prop("checked", true);                
                AttachmentObj.ShowRemoveWarningMessage();
                AttachmentObj.ArrayAttachmentId = [];
                return false;
            }
            else {
                $jqGrid(".chkRemoveClass").bind('onchange');
                $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", false);
                $jqGrid(".chkRemoveClass").prop("checked", false);
                AttachmentObj.ArrayAttachmentId = [];
                return false;
            }
        }, 500);
    });

    AttachmentObj.BindKeyUpEvent(AttachmentObj.AttachmentFileControl);

    setTimeout(function () {
        $jqGrid("#" + AttachmentObj.Jqgh_jqAttachmentGird_Remove).addClass("widht: 30px; padding-left: 36px;");
        $jqGrid("#btnUpload").addClass("btn_disable");
        $jqGrid('#btnUpload').attr('disabled', 'disabled');
        $jqGrid("#jqAttachmentGirdTablePager_center").hide();
    }, 500);
});

// click event of upload button
Attachment.prototype.UploadFile = function ()
{    
    $jqGrid("#data_loader").show();
    if (AttachmentObj.ValidateAttachment()) {
        AttachmentObj.SetTypeIdAndAttachmentType();
        $jqGrid.ajax({
            type: "POST",
            url: '/Attachment/UploadAttachment',
            contentType: false,
            processData: false,
            cache: false,
            async: false,
            data: AttachmentObj.FileData,
            success: function (result) {
                if (result === "true") {
                    AttachmentObj.RefreshAttachmentGrid();
                    $jqGrid("#data_loader").hide();
                }
                else {
                    AttachmentObj.DisplayAlertMessage(result);
                    $jqGrid("#data_loader").hide();
                }
                AttachmentObj.FileData = new FormData();
                AttachmentObj.DisableUploadButton();
            },
            error: function (xhr, status, p3, p4) {
                $jqGrid("#data_loader").hide();
            }
        });
    }
}

// change event of file upload control
Attachment.prototype.FileUploadChange = function (e)
{    
    var files = e.files;
    AttachmentObj.SelectedFilesCount = files.length;
    if (AttachmentObj.SelectedFilesCount > 0) {
        $jqGrid("#btnUpload").removeClass("btn_disable");
        $jqGrid('#btnUpload').removeAttr("disabled");
    }
    else {
        $jqGrid("#btnUpload").addClass("btn_disable");
        $jqGrid('#btnUpload').attr('disabled', 'disabled');
    }
    if (AttachmentObj.ValidateAttachment()) {
        var fileCount = $jqGrid("#hdnFileCount").val()
        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    AttachmentObj.FileData.append("file" + x, files[x]);
                }                
            } else {
                alert("This file format not supported by HTML5.0");
            }
        }
        
    }
}

// Ajax call to refresh the jqgrid
Attachment.prototype.RefreshAttachmentGrid = function ()
{    
    $jqGrid.ajax({
        url: '/Attachment/DisplayAttachment',
        type: 'GET',
        cache: false,
        data: {
            TypeId: $jqGrid("#hdnTypeId").val(), AttachmentType: $("#AttachmentTypeId").val()
        },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {            
            if (responseData != null) {                
                $jqGrid("#jqAttachmentGird").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
            }
        },
        error: function (data) {

        }
    });
}   

// formatter for checkbox in jqGrid
Attachment.prototype.RemoveAttachment = function (cellvalue, options, rowobject) {    
    return "<input type='checkbox' class='chkRemoveClass' id=" + options['rowId'] + " onchange=AttachmentObj.CountChecked(this)>";
}

 //Select or deselect all checkbox
Attachment.prototype.CountChecked = function (checkbox) {    
    var numberOfChecked = $jqGrid('.chkRemoveClass:checked').length;
    var totalCheckboxes = $('.chkRemoveClass:checkbox').length;
    if (numberOfChecked == totalCheckboxes) {
        $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", true);
    }
    else {
        $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", false);
    }

    if (checkbox.checked) {        
        AttachmentObj.ShowRemoveWarningMessage();
    }
    return false;
}

Attachment.prototype.ShowRemoveWarningMessage = function () {
    $jqGrid('#' + AttachmentObj.RemoveAttachmentRecord).show();
    
}

// Ajax call to remove records form grid
Attachment.prototype.RemoveRecordAttachment = function ()
{    
    $jqGrid.ajax({
        url: '/Attachment/RemoveAttachment',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        data: JSON.stringify(AttachmentObj.ArrayAttachmentId),       
        async: false,
        cache: false,
        success: function (responseData) {            
            AttachmentObj.RefreshAttachmentGrid();
        },
        error: function (data) {

        }
    });

}

// Used to do validation required for upload (size and file count)
Attachment.prototype.ValidateAttachment = function ()
{    
    //// File Count
    var existingFileCount = $("#" + AttachmentObj.AttachmentGrid).children().children().length -1 ;
    if (AttachmentObj.SelectedFilesCount + existingFileCount > 7) {              
        AttachmentObj.DisplayAlertMessage($jqGrid("#hdnFileCountErrorMessage").val());
        AttachmentObj.SelectedFilesCount = 0;
        AttachmentObj.DisableUploadButton();
        return false;
    }    
    var fileControl = $jqGrid("#AttachmentFileControl");
    var maxSize = $jqGrid("#hdnAppConfigMaxSixe").val() * 1024 * 1024;
    var maxSizeMessage = $jqGrid("#hdnFilzSize").val().replace('{0}', $jqGrid("#hdnAppConfigMaxSixe").val());
    for (var i = 0; i < AttachmentObj.SelectedFilesCount; i++) {
        if ($("#AttachmentFileControl")[0].files[i].size > maxSize) {
            AttachmentObj.DisplayAlertMessage(maxSizeMessage);
            AttachmentObj.DisableUploadButton();
            return false;
        }
    }
    return true;
}

// Message popup
Attachment.prototype.DisplayAlertMessage = function (message)
{    
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='field-validation-error' >";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";
    errorMsg = errorMsgUl;

    errorMsg += message + errorMsgLiclose;
    var errmsg = errorMsg + errorMsgUlclose;
    if ($jqGrid("#divAttachmentError").length > 0) {
        $jqGrid("#divAttachmentError").html(errmsg);
        if (errorMsg != "" && errorMsg != "<ul class='field-validation-error'>") {
            AttachmentObj.AttachmentShowHideError();
            return false;
        }
        else {
            return true;
        }
    }
    else
    {
        $jqGrid("#divAttachmentError").html(errmsg);
        if (errorMsg != "" && errorMsg != "<ul class='field-validation-error'>") {
            AttachmentObj.AttachmentShowHideError();
            return false;
        }
        else {
            return true;
        }
    }
   

}

// Used to reset the upload button and upload control
Attachment.prototype.DisableUploadButton = function ()
{    
    $jqGrid('#AttachmentFileControl').val('');
    clear_html("AttachmentFileUploadControl");
    AttachmentObj.BindKeyUpEvent(AttachmentObj.AttachmentFileControl);
    $jqGrid("#btnUpload").addClass("btn_disable");
    $jqGrid('#btnUpload').attr('disabled', 'disabled');
}

// Adding link in jqGrid formatter
Attachment.prototype.ShowMessage = function (cellvalue, options, rowobject) {    
    var download = $jqGrid("#hdnDownload").val();
    return "<a  id = AttachmentId" + options['rowId'] + " onclick='AttachmentObj.DownloadAttachment(" + options['rowId'] + ")'>" + download + "</a>"
}

// Download attachment code
Attachment.prototype.DownloadAttachment = function (rowId)
{    
    var rowData = $jqGrid("#jqAttachmentGird").getRowData(rowId);
    var attachmentId = rowData["AttachmentId"];
    var attachmentIdList = [];
    attachmentIdList.push(attachmentId);

    $jqGrid.ajax({
        url: '/Attachment/DownloadAttachmentResult',
        type: 'POST',
        cache: false,
        data: JSON.stringify(attachmentIdList),
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {
            
            if (responseData.length > 0) {
                
                if (responseData[0] == "") {
                    AttachmentObj.DisplayAlertMessage($jqGrid("#" + AttachmentObj.FileNotFound).val());
                }
                else {
                    var realatvieFilePath = responseData[0];
                    var localHost = "../";
                    if ($jqGrid("#AttachmentTypeId").val() === "10113") {
                        window.open(realatvieFilePath, '_blank');
                    }

                    if ($jqGrid("#AttachmentTypeId").val() === "10114" || $jqGrid("#AttachmentTypeId").val() === "10115") {
                        window.open(realatvieFilePath, '_blank');;
                    }
                }
            }
        },
        error: function (data) {
        }
        });
}

// Used to open Attachment Popup Screen
Attachment.prototype.ShowAttachmentPopup = function (parentFormName) 
{
    $jqGrid('#divAttachmentPopup').dialog({
        autoOpen: false,
        modal: true,
        width: '824px',
        hight:'324px',
        closeOnEscape: false,
        data: { ParentFormName: parentFormName },
        resizable: true,
        title: $jqGrid('#hdnresAttachmentTitle').val(),
        open: function () {
            if (parentFormName != '') {
                $("#AttachmentTypeId").val(parentFormName);
            }
            
            AttachmentObj.FileData.append("AttachmentType", $("#AttachmentTypeId").val());
            if ($jqGrid("#AttachmentTypeId").val() === $jqGrid("#hdnAttachmentPatientType").val()) {
                var patientId = $jqGrid("#PatientId").text();
                $jqGrid("#hdnTypeId").val(patientId);
                var sappatientNumber = $jqGrid("#SAPPatientNumber").text();
                var patientName = patientId + " " + $jqGrid("#FirstName").val() + " " + $jqGrid("#LastName").val();
                if (sappatientNumber != "") {
                    patientName += " (" + sappatientNumber + ")";
                }
                $jqGrid("#lblAttachmentTypeName").text('');
                $jqGrid("#lblAttachmentTypeName").text(patientName);
                AttachmentObj.FileData.append("TypeId", $jqGrid("#PatientId").text());
                
                if (PatientInfoObj.AttachmentAccess == "0") {                    
                    $jqGrid("#AttachmentFileUploadControl").addClass("read_only_div");
                }
            }

            if ($jqGrid("#AttachmentTypeId").val() === $jqGrid("#hdnAttachmentCareHomeType").val())
            {
                var careHomeId = $("#hdnCareHomeId").val();                                
                var sapCarehomeId = $jqGrid("#lblCareHomeId").val();
                var careHomeName = $jqGrid("#txtCareHomeName").val();
                $jqGrid("#hdnTypeId").val(careHomeId);

                var careHomeLabel = careHomeId + " " + careHomeName;
                if (sapCarehomeId != "")
                {
                    careHomeLabel += "(" + sapCarehomeId + ")";
                }
                $jqGrid("#lblAttachmentTypeName").text('');
                $jqGrid("#lblAttachmentTypeName").text(careHomeLabel);
                AttachmentObj.FileData.append("TypeId", careHomeId);
                
                if (CareHomeObj.AttachmentAccess == "0") {
                    $jqGrid("#AttachmentFileUploadControl").addClass("read_only_div");
                }
            }
            
            if ($jqGrid("#AttachmentTypeId").val() === $jqGrid("#hdnAttachmentCustomerType").val()) {
                var customerId = $("#hdnCurrentCustomerId").val();
                var sapCustomerId = $jqGrid("#txtSAPCustomerNumber").val();
                var customerName = $jqGrid("#txtCustomerName").val();
                $jqGrid("#hdnTypeId").val(customerId);

                var customerLabel = customerId + " " + customerName;
                if (sapCarehomeId != "") {
                    careHomeLabel += "(" + sapCustomerId + ")";
                }
                $jqGrid("#lblAttachmentTypeName").text('');
                $jqGrid("#lblAttachmentTypeName").text(customerLabel);
                AttachmentObj.FileData.append("TypeId", customerId);
                                
                if (CustomerInformationObj.AttachmentAccess == "0") {
                    $jqGrid("#AttachmentFileUploadControl").addClass("read_only_div");
                }
            }
        }
    });

    $jqGrid('#divAttachmentPopup').dialog('open');

    $jqGrid.ajax({
        url: '/Attachment/DisplayAttachment',
        type: 'GET',
        cache: false,
        data: {
            TypeId: $jqGrid("#hdnTypeId").val(), AttachmentType: $("#AttachmentTypeId").val()
        },
        contentType: 'application/json',
        dataType: "json",
        async: false,
                success: function (responseData) {
                function clear_html(id) {
            $('#'+id).html($('#'+id).html());
            }
            if (responseData != null) {
                $jqGrid("#jqAttachmentGird").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
            }
        },
        error: function (data) {
        }
    });
}

Attachment.prototype.AttachmentOkButon = function ()
{
    $jqGrid('.chkRemoveClass').each(function () {
        if ($jqGrid(this).is(':checked')) {
            var rowId = $(this).closest("tr").attr('id');
            var rowData = $jqGrid("#jqAttachmentGird").jqGrid('getRowData', rowId);
            AttachmentObj.ArrayAttachmentId.push(rowData.AttachmentId);
        }
    });
    AttachmentObj.RemoveRecordAttachment();
    AttachmentObj.ArrayAttachmentId = [];
    setTimeout(function () {        
        $jqGrid("#jqAttachmentGird").jqGrid('clearGridData');
        $("#chkRemoveAllAttachment").parent().removeClass("ez-checked");
        $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", false);
        AttachmentObj.RefreshAttachmentGrid();
        $jqGrid('#' + AttachmentObj.RemoveAttachmentRecord).hide();
    }, 500);
    AttachmentObj.isTrackDirty = true;

    $jqGrid('#' + AttachmentObj.RemoveAttachmentRecord).hide();

    var numberOfChecked = $jqGrid('.chkRemoveClass:checked').length;
    if (numberOfChecked == 0) {
        $("#chkRemoveAllAttachment").parent().removeClass("ez-checked");
        $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", false);
    }
    $jqGrid('#' + AttachmentObj.RemoveAttachmentRecord).hide();
}

// Remove Cancel button 
Attachment.prototype.AttachmentCancelButon = function () {        
    $jqGrid(".chkRemoveClass").prop("checked", false);
    $("#chkRemoveAllAttachment").parent().removeClass("ez-checked");
    $jqGrid("#" + AttachmentObj.RemoveAllAttachment).prop("checked", false);
    $jqGrid('#' + AttachmentObj.RemoveAttachmentRecord).hide();
}

// Set TypeId and Attachment Type
Attachment.prototype.SetTypeIdAndAttachmentType = function ()
{    
    AttachmentObj.FileData.append("AttachmentType", $("#AttachmentTypeId").val());
    if ($jqGrid("#AttachmentTypeId").val() === "10113")
    {
        AttachmentObj.FileData.append("TypeId", $jqGrid("#PatientId").text());
    }

    if ($jqGrid("#AttachmentTypeId").val() === "10114")
    {
        AttachmentObj.FileData.append("TypeId", $("#hdnCareHomeId").val());
    }
    if ($jqGrid("#AttachmentTypeId").val() === "10115")
    {
        AttachmentObj.FileData.append("TypeId", $("#hdnCurrentCustomerId").val());
    }
}

// Exit button
Attachment.prototype.AttachmentExit = function () {
    AttachmentObj.DisableUploadButton();
    ClosePopup('divAttachmentPopup');
}

Attachment.prototype.AttachmentShowHideError = function (){    
    $jqGrid("#divAttachmentError_lightbox").show();
    $jqGrid(".j_lightbox_error_close").click(function () {
        $jqGrid("#divAttachmentError_lightbox").hide();
    });
    $jqGrid(".j_lightbox_error_ok").click(function () {
        $jqGrid("#divAttachmentError_lightbox").hide();
    });
}

Attachment.prototype.BindKeyUpEvent = function (id)
{
    $("#" + id).on("keyup", function (e) {        
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8 || charCode == 46) {
            $("#" + AttachmentObj.AttachmentFileControl).val('');
        }

        if ($("#" + AttachmentObj.AttachmentFileControl).val() == '') {
            $jqGrid("#btnUpload").addClass("btn_disable");
            $jqGrid('#btnUpload').attr('disabled', 'disabled');
        }

    });
}

// Used to clear the file upload control in IE Browser
function clear_html(id)
{   
    var element = document.getElementById(id);
	element.innerHTML = element.innerHTML;

}

