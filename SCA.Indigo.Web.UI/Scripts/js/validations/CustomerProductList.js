﻿$jqGrid = jQuery.noConflict();
var jsfilename = "CustomerProductList.js";
var CustomerProductListObj = new CustomerProductList();

CustomerProductList.prototype.BtnCustomerProductList = "btnCustomerProductList";
CustomerProductList.prototype.DivCustomerProducts = "divCustomerProducts";
CustomerProductList.prototype.ProductListTitle = "hdnProductListTitle";
CustomerProductList.prototype.BtnSaveCustomerProducts = "btnSaveCustomerProducts";
CustomerProductList.prototype.BtnExitCustomerProductPopup = "btnExitCustomerProductPopup";
CustomerProductList.prototype.CustomerController = "hdnCustomerController";
CustomerProductList.prototype.ActionLoadCustomerProductList = "hdnActionLoadCustomerProductList";
CustomerProductList.prototype.ActionSaveCustomerProductList = "hdnActionSaveCustomerProductList";
CustomerProductList.prototype.jqCustomerProductList = "jqCustomerProductList";
CustomerProductList.prototype.divCPLSaveSuccess = "divCPLSaveSuccess";
CustomerProductList.prototype.divCPLOverrideError = "divCPLOverrideError";
CustomerProductList.prototype.divCPLSaveNoChanges = "divCPLSaveNoChanges";
CustomerProductList.prototype.hdnCPLOverrideError = "hdnCPLOverrideError";

function CustomerProductList() { }

function IsActive(cellvalue, options, rowobject) {
    var controlStr = "<input type='checkbox'  id= '" + options['rowId'] + "_IsActive' ";

    if (rowobject['IsActive'] == true || rowobject['IsActive'] == "1") {
        controlStr = controlStr + "checked = 'checked' ";
    }
    controlStr = controlStr + "/>";
    return controlStr;
}

function IsDisplayed(cellvalue, options, rowobject) {
    var controlStr = "<input type='checkbox' id= '" + options['rowId'] + "_IsDisplayed' ";

    if (rowobject['IsDisplayed'] == true || rowobject['IsDisplayed'] == "1") {
        controlStr = controlStr + "checked = 'checked' ";
    }
    controlStr = controlStr + "/>";
    return controlStr;
}

function IsApprovalRequired(cellvalue, options, rowobject) {
    var controlStr = "<input type='checkbox' id= '" + options['rowId'] + "_IsApprovalRequired' ";

    if (rowobject['IsApprovalRequired'] == true || rowobject['IsApprovalRequired'] == "1") {
        controlStr = controlStr + "checked = 'checked' ";
    }
    controlStr = controlStr + "/>";
    return controlStr;
}

function IsProductOverriden(cellvalue, options, rowobject) {
    var controlStr = "<input type='checkbox' " + " onchange=OnChangeIsOverride(" + options['rowId'] + ") id= '" + options['rowId'] + "_IsOverride' ";
    if (rowobject['IsOverride'] == true || rowobject['IsOverride'] == "1") {
        controlStr = controlStr + "checked = 'checked' ";
    }
    controlStr = controlStr + "/>";
    return controlStr;
}

function IsDisplayForSample(cellvalue, options, rowobject) {
    var controlStr = "<input type='checkbox' id= '" + options['rowId'] + "_IsDisplayForSample' ";

    if (rowobject['IsDisplayForSample'] == true || rowobject['IsDisplayForSample'] == "1") {
        controlStr = controlStr + "checked = 'checked' ";
    }
    controlStr = controlStr + "/>";
    return controlStr;
}

function OnChangeIsOverride(rowId) {    
    var rowData = $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).jqGrid('getRowData', rowId);        
    $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).jqGrid('saveRow', rowId, false, 'clientArray');
    $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).jqGrid('restoreRow', rowId);

    if ($jqGrid("#" + rowId + "_IsOverride").is(':checked')) {
        $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).setColProp('Frequency', { editable: true });
        $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).setColProp('Quantity', { editable: true });
        $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).jqGrid('editRow', rowId, true);
    }
}

CustomerProductList.prototype.ShowPopupCustomerProductList = function (e) {
    var title = $jqGrid("#" + CustomerProductListObj.ProductListTitle) != null ? $jqGrid("#" + CustomerProductListObj.ProductListTitle).val() : "";    
    
    $jqGrid("#" + CustomerProductListObj.DivCustomerProducts).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: '960px',     
        resizable: true,
        title: title
    });
    $jqGrid("#" + CustomerProductListObj.DivCustomerProducts).dialog('open');
    $jqGrid(".disable_div").remove();
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();    
    CustomerProductListObj.LoadCustomerProductList(customerId);
}

CustomerProductList.prototype.LoadCustomerProductList = function (customerId) {     
    $jqGrid.ajax({
        url: '/' + $jqGrid("#" + CustomerProductListObj.CustomerController).val() + '/' + $jqGrid("#" + CustomerProductListObj.ActionLoadCustomerProductList).val(),
        type: 'GET',
        cache: false,
        data: {
            customerId: customerId
        },
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (responseData) {
            if (responseData.length > 0) {
                $jqGrid("#lblCustomerName").html( responseData[0]["CustomerId"] + "  " + responseData[0]["CustomerName"]);
            }            
            $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).jqGrid('clearGridData');
            $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
        },
        error: function (x, e)
        {
            HandleAjaxError(x, e, jsfilename, "LoadCustomerProductList");
            return false;
        }
    });
}

$jqGrid('#' + CustomerProductListObj.BtnExitCustomerProductPopup).click(function () {
    CustomerProductListObj.CloseCustomerProductPopup();
});

CustomerProductList.prototype.CloseCustomerProductPopup = function () {
    $jqGrid('#' + CustomerProductListObj.DivCustomerProducts).dialog('close');
}



CustomerProductList.prototype.SaveCustomerProductList = function (e) {
    var gridData = $jqGrid("#" + CustomerProductListObj.jqCustomerProductList).getGridParam('data');
    var isError = false;
    var productsToSave = [];
    var errorMessage = "";
    for (var i = 0; i < gridData.length; i++) {
        var rowData = gridData[i];
        var rowId = i + 1;

        if ($jqGrid("#" + rowId + "_IsOverride").prop('checked') == true &&
            ($jqGrid("#" + rowId + "_Frequency").val() == "" || $jqGrid("#" + rowId + "_Frequency").val() == undefined || $jqGrid("#" + rowId + "_Frequency").val() == 0 ||
                $jqGrid("#" + rowId + "_Quantity").val() == "" || $jqGrid("#" + rowId + "_Quantity").val() == undefined || $jqGrid("#" + rowId + "_Quantity").val() == 0)) {
            isError = true;
            errorMessage += rowData["BaseMaterial"] + ",";

        }
        if (isError) {
            continue;
        }
        var productDetails = {
            IdxCustomerProductId: rowData["IdxCustomerProductId"],
            IsDisplayed: $jqGrid("#" + rowId + "_IsDisplayed").prop('checked'),
            IsApprovalRequired: $jqGrid("#" + rowId + "_IsApprovalRequired").prop('checked'),
            IsOverride: $jqGrid("#" + rowId + "_IsOverride").prop('checked'),
            IsActive: $jqGrid("#" + rowId + "_IsActive").prop('checked'),
            IsDisplayForSample: $jqGrid("#" + rowId + "_IsDisplayForSample").prop('checked'),
            Frequency: ($jqGrid("#" + rowId + "_Frequency").val() != "" && $jqGrid("#" + rowId + "_Frequency").val() != undefined) ? $jqGrid("#" + rowId + "_Frequency").val() : rowData["Frequency"],
            Quantity: ($jqGrid("#" + rowId + "_Quantity").val() != "" && $jqGrid("#" + rowId + "_Quantity").val() != undefined) ? $jqGrid("#" + rowId + "_Quantity").val() : rowData["Quantity"]
        }

        productsToSave.push(productDetails);
    }

    if (isError) {
        errorMessage = $jqGrid("#" + CustomerProductListObj.hdnCPLOverrideError).val() + " " + errorMessage.slice(0, -1);
        $jqGrid("#lbldCPLOverrideError").html(errorMessage);
        $jqGrid("#" + CustomerProductListObj.divCPLOverrideError).show();
        return false;
    }

    $jqGrid.ajax({
        url: '/' + $jqGrid("#" + CustomerProductListObj.CustomerController).val() + '/' + $jqGrid("#" + CustomerProductListObj.ActionSaveCustomerProductList).val(),
        type: 'POST',
        cache: false,
        data: JSON.stringify(productsToSave),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (responseData) {
            if (responseData == 1) {
                $jqGrid("#" + CustomerProductListObj.divCPLSaveSuccess).show();
            }
            else if(responseData == 2) {
                $jqGrid("#" + CustomerProductListObj.divCPLSaveNoChanges).show();
            }
            else {
                $jqGrid("#" + CustomerProductListObj.divCPLSaveError).show();
            }
        },
        error: function (x, e) {
            $jqGrid("#" + CustomerProductListObj.divCPLSaveError).show();
            HandleAjaxError(x, e, jsfilename, "SaveCustomerProductList");
        }
    });
}

$jqGrid('#' + CustomerProductListObj.BtnSaveCustomerProducts).click(function () {
    CustomerProductListObj.SaveCustomerProductList();
});
