﻿$jqGrid = jQuery.noConflict();

var InteractionObj = new Interaction();
Interaction.prototype.GeneralYes = "hdnGeneralYes";
Interaction.prototype.GeneralNo = "hdnGeneralNo";
Interaction.prototype.GeneralOk = "hdnGeneralOk";
Interaction.prototype.IsDisplayAsChecked = false;
Interaction.prototype.AssignedToUser = "";
Interaction.prototype.HdnInteractionPatientTable = "hdnInteractionPatientTable";
Interaction.prototype.HdnInteractionCustomerTable = "hdnInteractionCustomerTable";
Interaction.prototype.HdnInteractionCareHomeTable = "hdnInteractionCareHomeTable";
Interaction.prototype.txtCustomerSearch = "txtInteractionCustomerSearch";
Interaction.prototype.txtCustomerDisplay = "txtInteractionCustomerDisplay";
Interaction.prototype.txtPatientSearch = "txtInteractionPatientSearch";
Interaction.prototype.txtPatientDisplay = "txtInteractionPatientDisplay";
Interaction.prototype.txtcareHomeSearch = "txtInteractioncareHomeSearch";
Interaction.prototype.txtcareHomeDisplay = "txtInteractioncareHomeDisplay";
Interaction.prototype.BtnInteractionSummary = "btnInteractionSummary";
Interaction.prototype.BtnInteractionReset = "btnInteractionReset";
Interaction.prototype.HdnErrorMessageForCustomer = "hdnErrorMessageForCustomer";
Interaction.prototype.HdnInteractionPatientRecentOrderValue = "hdnInteractionPatientRecentOrderValue";
Interaction.prototype.HdnInteractionCareHomeRecentOrderValue = "hdnInteractionCareHomeRecentOrderValue";
Interaction.prototype.BtnCreateInteraction = "btnCreateInteraction";
Interaction.prototype.IsMenuInteraction = "isMenuInteraction";
Interaction.prototype.BtnInteractionUndo = "btnInteractionUndo";
Interaction.prototype.classErrorMessage = "validation_msg";
Interaction.prototype.classBtnDisable = "btn_disable";
Interaction.prototype.divSelectCustomer = "divInteractionSelectCustomer";
Interaction.prototype.JqInteractionSummary = "JqInteractionSummary";
Interaction.prototype.JqPatientInteractionRecentOrderInfoPopup = "jqPatientInteractionRecentOrderInfoPopup";
Interaction.prototype.InteractionParentFormName = "InteractionParentFormName";
Interaction.prototype.HdnIsCalledFromHomepage = "hdnIsCalledFromHomepage";
Interaction.prototype.HdnHomeInteractionId = "hdnHomeInteractionId";
Interaction.prototype.HdnInteractionId = "hdnInteractionId";
Interaction.prototype.SetInteractionSession = "SetInteractionSession";
Interaction.prototype.HdnIsCalledFromInteractionMenu = "hdnIsCalledFromInteractionMenu";
Interaction.prototype.HdnMenuPatientName = "hdnMenuPatientName";
Interaction.prototype.HdnMenuCarehomeName = "hdnMenuCarehomeName";
Interaction.prototype.HdnMenuCustomerName = "hdnMenuCustomerName";
Interaction.prototype.HdnMenuInteractionId = "hdnMenuInteractionId";
Interaction.prototype.HdnOpenCarehomeFlag = "hdnOpenCarehomeFlag";
Interaction.prototype.DivExitPrompt = "divInteractionError_lightbox";
Interaction.prototype.DdlInteractionType = "ddlInteractionType";
Interaction.prototype.DdlInteractionSubType = "ddlInteractionSubType";
Interaction.prototype.DdlInteractionAssignedTo = "ddlInteractionAssignedTo";
Interaction.prototype.InteractionDescription = "InteractionDescription";
Interaction.prototype.DescriptionInitialValue = "DescriptionInitialValue";
Interaction.prototype.HdnCallingForm = "hdnCallingForm";
Interaction.prototype.IsDirty = false;
Interaction.prototype.InteractionCustomerId = 0;
Interaction.prototype.InteractionPatinetId = 0;
Interaction.prototype.InteractionCareHomeId = 0;


var patientTable = $jqGrid('#' + InteractionObj.HdnInteractionPatientTable + '').val();
var customerTable = $jqGrid('#' + InteractionObj.HdnInteractionCustomerTable + '').val();
var careHomeTable = $jqGrid('#' + InteractionObj.HdnInteractionCareHomeTable + '').val();
var IsForceClose = false;

var usertype = "SCA";
Interaction.prototype.HomeAlertContactInfoAccess = "";

function Interaction() { }


Interaction.prototype.InteractionValidationShowHideError = function() {
    $jqGrid("#j_Interaction_lightbox_error").show();
    $jqGrid(".j_lightbox_error_close").click(function () {
        $jqGrid("#j_Interaction_lightbox_error").hide();
    });
    $jqGrid(".j_lightbox_error_ok").click(function () {
        $jqGrid("#j_Interaction_lightbox_error").hide();
    });
}

// Used to Fill the subtype menu based on the interaciton type id 
Interaction.prototype.FillInteractionSubType = function (interactionTypeId) {
    if (interactionTypeId == '' || interactionTypeId == undefined) {
        interactionTypeId = $jqGrid('#ddlInteractionType').val();
    }

    var ddlInteractionSubType = $jqGrid('#ddlInteractionSubType');
    if (interactionTypeId != '') {
        var url = $jqGrid('#InteractionSubTypeAction').val();
        $jqGrid.ajax({
            cache: false,
            type: "GET",
            url: url,
            data: { "interactionTypeId": interactionTypeId },
            async: false,
            cache: false,
            success: function (data) {
                ddlInteractionSubType.html('');
                ddlInteractionSubType.append($jqGrid('<option></option>').val('').html('Select'))
                $jqGrid.each(data, function (id, option) {
                    ddlInteractionSubType.append($jqGrid('<option></option>').val(option.Value).html(option.Text));
                });
                if ($jqGrid("#hdbIsDoubleClicked").val()) {
                    ddlInteractionSubType.val($jqGrid("#hdnInteractionSubTypeId").val());
                    //CommonScriptObj.DisableFormControls("frm_CreateInteraction");
                }
                else {
                    $jqGrid("#ddlInteractionSubType").val("");
                }
            },
        });
    }
    else {
        ddlInteractionSubType.html('');
        ddlInteractionSubType.append($jqGrid('<option></option>').val('').html('Select'));
        ddlInteractionSubType.val("");
    }
}

// Undo button logic
Interaction.prototype.Undo = function () {
    var resetMessage = $jqGrid('#UndoMessage').val();
    $jqGrid('#divIntercationUndoMessage').html(resetMessage);
    $jqGrid('#divIntercationUndoMessage').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + InteractionObj.GeneralYes).val().toString(),
            "id": InteractionObj.GeneralYes,
            click: function () {
                $jqGrid('#divIntercationUndoMessage').dialog('destroy');
                InteractionObj.SetDropdownValues();
                InteractionObj.StatusChangeEvent();
                $jqGrid('#InteractionDescription').val($jqGrid('#hdnInteractionDescription').val());
                $jqGrid('#chkResolved').prop("checked", $jqGrid('#hdnInteractionResolved').val().toLowerCase() == "true");
                return false;
            },
        }, {
            text: $jqGrid("#" + InteractionObj.GeneralNo).val().toString(),
            "id": InteractionObj.GeneralNo,
            click: function () {                
                $jqGrid('#divIntercationUndoMessage').dialog('destroy');
            },
        }]
    });
    $jqGrid('#divIntercationUndoMessage').dialog('open');
    return false;
}

// Used to validate the form fields
Interaction.prototype.ValidateInteraction = function () {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";
    errorMsg = errorMsgUl;

    //document.getElementById('ddlCustomer').selectedIndex < 1
    if (document.getElementById('ddlInteractionStatus').selectedIndex < 1) {
        errorMsg += errorMsgLi + $jqGrid("#ErrorMessageStatusId").val() + errorMsgLiclose;
    }

    if (document.getElementById('ddlInteractionType').selectedIndex < 1) {
        errorMsg += errorMsgLi + $jqGrid("#ErrorMessageInteractionType").val() + errorMsgLiclose;
    }

    if (document.getElementById('ddlInteractionSubType').selectedIndex < 1) {
        errorMsg += errorMsgLi + $jqGrid("#ErrorMessageInteractionSubType").val() + errorMsgLiclose;
    }

    if ($jqGrid("#InteractionDescription").val() == '') {
        errorMsg += errorMsgLi + $jqGrid("#ErrorMessageDescription").val() + errorMsgLiclose;
    }
    if ($jqGrid("#chkDisplayAsAlert").is(':checked')) {
        if (document.getElementById('ddlInteractionAssignedTo').selectedIndex < 1) {
            errorMsg += errorMsgLi + $jqGrid("#ErrorMessageAssignedTo").val() + errorMsgLiclose;
        }
    }

    var errmsg = errorMsg + errorMsgUlclose;
    $jqGrid("#divValidationInteractionErrorMessage").html(errmsg);
    if (errorMsg != "" && errorMsg != "<ul class='pop_error_ul'>") {
        InteractionObj.InteractionValidationShowHideError();
        return false;
    }
    else {
        return true;
    }
}

// Save Interaction 
Interaction.prototype.SaveInteractionDetails = function () {
    var isValid = InteractionObj.ValidateInteraction();
    if (isValid) {
        var url = '/Patient/SaveInteractionDetails';
        var interactionId = $jqGrid("#hdnInteractionId").val();
        var customerId = $jqGrid("#hdnCreateInteractionCustomerId").val();
        var patientId = $jqGrid("#hdnCreateInteractionPatientId").val();
        var interactionCategoryTypeId = $jqGrid("#ddlInteractionType :selected").val();
        var interactionSubTypeId = $jqGrid("#ddlInteractionSubType :selected").val();
        var description = $jqGrid("#InteractionDescription").val();
        var displayAsAlert = $jqGrid("#chkDisplayAsAlert").is(':checked');
        var assignedTo = $jqGrid("#ddlInteractionAssignedTo :selected").val();
        var careHomeId = $jqGrid("#hdnCreateInteractionCarehomeId").val();
        var statusId = $jqGrid("#ddlInteractionStatus :selected").val();
        var resolved = $jqGrid("#chkResolved").is(':checked');
        $jqGrid.ajax({
            url: url,
            type: 'POST',
            contentType: "application/json",
            dataType: 'json',
            async: false,
            cache: false,
            data: JSON.stringify({
                InteractionId: interactionId,
                CustomerId: customerId,
                PatientId: patientId,
                CarehomeId: careHomeId,
                InteractionTypeId: interactionCategoryTypeId,
                InteractionSubTypeId: interactionSubTypeId,
                Description: description,
                DisplayAsAlert: displayAsAlert,
                AssignedTo: assignedTo,
                StatusId: statusId,
                Resolved: resolved
            }),
            success: function (data) {
                if (data.interactionresponse.SavedStatus == 1) {
                    $jqGrid("#divInteractionSucess").text(data.interactionresponse.SavedStatusMessage);
                    $jqGrid('#divInteractionSucess').dialog({
                        autoOpen: false,
                        modal: true,
                        closeOnEscape: false,
                        width: 'auto',
                        resizable: false,
                        buttons: [{
                            text: $jqGrid("#" + InteractionObj.GeneralOk).val().toString(),
                            "id": InteractionObj.GeneralOk,
                            click: function () {
                                var callingForm = $jqGrid("#" + InteractionObj.HdnCallingForm).val();
                                IsForceClose = true;
                                InteractionObj.CloseCreateInteractionPopup(callingForm)
                                if ($jqGrid("#jqInteractionSummaryHome").length > 0) {
                                    HomeScriptObj.GetInteactionAlert();
                                    InteractionObj.RefreshHomeGrid();
                                }
                                return false;
                            },
                        }]
                    });
                    $jqGrid('#divInteractionSucess').dialog('open');
                    $jqGrid(".ui-dialog-titlebar-close").hide();

                    if (displayAsAlert && assignedTo != "" && assignedTo.toUpperCase() != $jqGrid("#UserId").val().toUpperCase())
                        SendInteractionNotificationToUser(assignedTo);
                } else {
                    CommonScriptObj.DisplayErrorMessage("divInteractionFail", data.interactionresponse.SavedStatusMessage, 'pop_error_ul');
                    return false;
                }
            }
        });
    }
}

// Contains the logic when the dialiog is loaded
$jqGrid(document).ready(function () {

    $jqGrid("#" + InteractionObj.InteractionDescription).focusout(function () {
        if (InteractionObj.DescriptionInitialValue != $jqGrid("#" + InteractionObj.InteractionDescription).val()) {
            InteractionObj.IsDirty = true;
        }
    })

    $jqGrid("#" + InteractionObj.DdlInteractionAssignedTo).bind("change", function () {
        InteractionObj.IsDirty = true;
    })

    $jqGrid("#" + InteractionObj.DdlInteractionSubType).bind("change", function () {
        InteractionObj.IsDirty = true;
    });

    $jqGrid("#" + InteractionObj.DdlInteractionType).bind("change", function () {
        InteractionObj.IsDirty = true;
    });

    $jqGrid("#jqgh_JqInteractionSummary_CreatedOn").bind("click", function () {
    });

    $jqGrid("#" + InteractionObj.BtnInteractionSummary).bind("click", function () {
        $jqGrid("#JqInteractionSummary").jqGrid('clearGridData');
        if ($jqGrid("#" + InteractionObj.txtCustomerSearch).val() != "" && ($jqGrid("#" + InteractionObj.txtPatientSearch).val() != "" || $jqGrid("#" + InteractionObj.txtcareHomeSearch).val() != "")) {
            var searchId = "";
            var ordertype = "";
            if ($jqGrid("#" + InteractionObj.txtcareHomeSearch).val() != "") {
                ordertype = $jqGrid("#" + InteractionObj.HdnInteractionCareHomeRecentOrderValue).val();
                searchId = CommonScriptObj.CareHomeId;
            }

            if ($jqGrid("#" + InteractionObj.txtPatientSearch).val() != "") {
                ordertype = $jqGrid("#" + InteractionObj.HdnInteractionPatientRecentOrderValue).val();
                searchId = CommonScriptObj.PatientId;
            }


            $jqGrid("#jqPatientInteractionRecentOrderInfoPopup").jqGrid('clearGridData');
            $jqGrid("#jqPatientInteractionRecentOrderInfoPopup").jqGrid('setGridParam', { postData: { searchId: searchId, recentOrderType: ordertype }, datatype: 'json', sortname: 'OrderDate', sortorder: 'desc' }).trigger('reloadGrid');

            // Used to Dislay the Ineraction Summary
            var parentFormName = "";
            $jqGrid.ajax({
                url: '/Patient/GetInteractionSummaryDetails',
                type: 'GET',
                cache: false,
                data: {
                    PatientId: CommonScriptObj.PatientId, CarehomeId: CommonScriptObj.CareHomeId, CustomerId: CommonScriptObj.CustomerId, ParentFormName: parentFormName
                },
                contentType: 'application/json',
                dataType: "json",
                async: false,
                success: function (responseData) {
                    $jqGrid("#InteractionName").text(responseData.LabelName);
                    $jqGrid("#JqInteractionSummary").jqGrid('clearGridData');
                    if (responseData.Interactions != null) {
                        $jqGrid("#JqInteractionSummary").jqGrid('setGridParam', { datatype: 'local', data: responseData.Interactions }).trigger('reloadGrid');
                    }
                    $jqGrid("#JqInteractionSummary").trigger("reloadGrid", [{ current: true }]);
                    return false;
                }
            });

            if (typeof (InteractionObj) != "undefined") {
                InteractionObj.RefreshInteractionSummaryGrid();
            }
        }
        else if ($jqGrid("#" + InteractionObj.txtCustomerSearch).val() == "") {
            CommonScriptObj.DisplayErrorMessage(InteractionObj.divSelectCustomer, $jqGrid("#hdnresErrorMessageCustomer").val(), 'pop_error_ul');
        }
        else if ($jqGrid("#" + InteractionObj.txtPatientSearch).val() == "" || $jqGrid("#" + InteractionObj.txtcareHomeSearch).val() == "") {
            CommonScriptObj.DisplayErrorMessage(InteractionObj.divSelectCustomer, $jqGrid("#hdnresRequiredCarehomeOrPatientId").val(), 'pop_error_ul');
        }
    });

    $jqGrid("#" + InteractionObj.BtnCreateInteraction).bind("click", function () {
        if ($jqGrid("#" + InteractionObj.txtCustomerSearch).val() != "" && ($jqGrid("#" + InteractionObj.txtPatientSearch).val() != "" || $jqGrid("#" + InteractionObj.txtcareHomeSearch).val() != "")) {
            InteractionObj.ShowCreateInteraction("", "", "", "interactionmenu");
        }
        else if ($jqGrid("#" + InteractionObj.txtCustomerSearch).val() == "") {
            CommonScriptObj.DisplayErrorMessage(InteractionObj.divSelectCustomer, $jqGrid("#hdnresErrorMessageCustomer").val(), 'pop_error_ul');
        }
        else if ($jqGrid("#" + InteractionObj.txtPatientSearch).val() == "" || $jqGrid("#" + InteractionObj.txtcareHomeSearch).val() == "") {
            CommonScriptObj.DisplayErrorMessage(InteractionObj.divSelectCustomer, $jqGrid("#hdnresRequiredCarehomeOrPatientId").val(), 'pop_error_ul');
        }

    });

    $jqGrid("#chkDisplayAsAlert").bind('change', function () {
        InteractionObj.ManditoryAssignedToField();
        InteractionObj.IsDirty = true;
    });

    $jqGrid("#chkResolved").bind('change', function () {
        InteractionObj.IsDirty = true;
    });

    $jqGrid("#ddlInteractionStatus").bind('change', function () {

        InteractionObj.StatusChangeEvent();
        InteractionObj.IsDirty = true;
    });

    $jqGrid("#" + InteractionObj.BtnInteractionUndo).click(function () {
        $jqGrid("#" + InteractionObj.txtCustomerDisplay).val('');
        $jqGrid("#" + InteractionObj.txtCustomerSearch).val('');

        $jqGrid("#" + InteractionObj.txtcareHomeSearch).val('');
        $jqGrid("#" + InteractionObj.txtcareHomeDisplay).val('');

        $jqGrid("#" + InteractionObj.txtPatientDisplay).val('');
        $jqGrid("#" + InteractionObj.txtPatientSearch).val('');

        $jqGrid("#" + InteractionObj.JqInteractionSummary).jqGrid('clearGridData');
        $jqGrid("#" + InteractionObj.JqPatientInteractionRecentOrderInfoPopup).jqGrid('clearGridData');
    });

    setTimeout(function () {
        var closebutton = $("#alertmod_jqInteractionSummaryHome").next().next().children().children().first().next().children().first();
        closebutton.click(function (e) {
            e.stopPropagation();
            InteractionObj.ExitCreateInteractionPopup();
        });

        var menuInteractionId = $jqGrid("#" + InteractionObj.HdnMenuInteractionId).val();
        var isCalledFromInteractionMenu = $jqGrid("#" + InteractionObj.HdnIsCalledFromInteractionMenu).val();
        if (typeof (isCalledFromInteractionMenu) != "undefined" && isCalledFromInteractionMenu.toLowerCase() == "true") {

            var patientname = $jqGrid("#" + InteractionObj.HdnMenuPatientName).val();
            var carehomeName = $jqGrid("#" + InteractionObj.HdnMenuCarehomeName).val();
            var customerName = $jqGrid("#" + InteractionObj.HdnMenuCustomerName).val();
            if (patientname != "" && typeof (patientname) != "undefined" && patientname != '|||') {
                var menuPatientFields = patientname.split('|');
                $jqGrid("#" + InteractionObj.txtPatientDisplay).val(menuPatientFields[0]);
                $jqGrid("#" + InteractionObj.txtPatientSearch).val(menuPatientFields[1]);
                CommonScriptObj.PatientId = menuPatientFields[2];
                CommonScriptObj.PatientName = menuPatientFields[0];
                CommonScriptObj.CareHomeName = "";
                CommonScriptObj.CareHomeId = "";
                CommonScriptObj.SAPCareHomeNumber = "";
                if (menuPatientFields[3] != "undefined") {
                    CommonScriptObj.SAPPatientNumber = menuPatientFields[3];
                    CommonScriptObj.SAPCareHomeNumber = "";
                }
            }

            if (carehomeName != "" && typeof (carehomeName) != "undefined" && carehomeName != '|||') {
                var menuCarehomeFields = carehomeName.split('|');
                $jqGrid("#" + InteractionObj.txtcareHomeDisplay).val(menuCarehomeFields[0]);
                $jqGrid("#" + InteractionObj.txtcareHomeSearch).val(menuCarehomeFields[1]);
                CommonScriptObj.CareHomeId = menuCarehomeFields[2];
                CommonScriptObj.CareHomeName = menuCarehomeFields[0];
                CommonScriptObj.PatientName = "";
                CommonScriptObj.SAPPatientNumber = "";
                CommonScriptObj.PatientId = "";
                if (menuCarehomeFields[3] != "undefined") {
                    CommonScriptObj.SAPCareHomeNumber = menuCarehomeFields[3];
                    CommonScriptObj.SAPPatientNumber = "";
                }
            }

            if (customerName != "" && typeof (customerName) != "undefined" && customerName != '|||') {
                var menuCustomerFields = customerName.split('|');
                $jqGrid("#" + InteractionObj.txtCustomerDisplay).val(menuCustomerFields[0]);
                $jqGrid("#" + InteractionObj.txtCustomerSearch).val(menuCustomerFields[1]);
                CommonScriptObj.CustomerId = menuCustomerFields[2];
                CommonScriptObj.CustomerName = menuCustomerFields[0];
                if (menuCustomerFields[3] != "undefined") {
                    CommonScriptObj.SAPCustomerNumber = menuCustomerFields[3];
                }
            }

            var openCarehomeFlag = $jqGrid("#" + InteractionObj.HdnOpenCarehomeFlag).val();
            if (typeof (openCarehomeFlag) != "undefined" && openCarehomeFlag.toLowerCase() == "false") {
                InteractionObj.ShowCreateInteraction(true, menuInteractionId, "", "interactionmenu");
                $jqGrid("#" + InteractionObj.HdnIsCalledFromInteractionMenu).val("false");
                $jqGrid("#" + InteractionObj.HdnMenuInteractionId).val("");
            }

            if (typeof (openCarehomeFlag) != "undefined" && openCarehomeFlag.toLowerCase() == "true") {
                InteractionObj.ShowCreateInteraction(true, menuInteractionId, "", "interactionmenu");
                $jqGrid("#" + InteractionObj.HdnIsCalledFromInteractionMenu).val("false");
                $jqGrid("#" + InteractionObj.HdnMenuInteractionId).val("");
            }

        }


        if ($jqGrid("#hdbIsDoubleClicked").val() == "True" || $jqGrid("#hdbIsDoubleClicked").val() == "true") {
            InteractionObj.SetDropdownValues();
        }

        if ($jqGrid('#hdnCreateInteractionPatientId').val() != "" && $jqGrid('#hdnCreateInteractionPatientId').val() != "0") {
            $jqGrid('#btnCarehomeDetails').hide();
        }
        else {
            $jqGrid('#btnPatientDetails').hide();
        }

        InteractionObj.IsDisplayAsChecked = $jqGrid("#chkDisplayAsAlert").is(':checked');
        InteractionObj.AssignedToUser = $jqGrid("#ddlInteractionAssignedTo").val();
        InteractionObj.StatusChangeEvent();
        InteractionObj.ManditoryAssignedToField();
        InteractionObj.IsDirty = false;
        InteractionObj.DescriptionInitialValue = $jqGrid("#" + InteractionObj.InteractionDescription).val();
        CommonScriptObj.DisableFormControls("frmOpenInteractions");
    }, 500);

    

});

Interaction.prototype.StatusChangeEvent = function () {
    if ($jqGrid("#ddlInteractionStatus").val() == "20100") {
        $jqGrid("#chkDisplayAsAlert").attr('disabled', 'disabled');
        $jqGrid("#chkDisplayAsAlert").prop("checked", false);
        InteractionObj.ManditoryAssignedToField();
        $jqGrid("#ddlInteractionAssignedTo").val("");
    }
    else {
        $jqGrid("#chkDisplayAsAlert").removeAttr('disabled');
        $jqGrid("#chkDisplayAsAlert").prop("checked", InteractionObj.IsDisplayAsChecked);
        InteractionObj.ManditoryAssignedToField();
        $jqGrid("#ddlInteractionAssignedTo").val(InteractionObj.AssignedToUser);
    }
}

// Used to exit the dialog
Interaction.prototype.ExitCreateInteractionPopup = function () {
    InteractionObj.InteractionShowHideError();            
}

// Used to set the interaction subtype dropdown
Interaction.prototype.SetDropdownValues = function () {    
    $jqGrid("#ddlInteractionType").val($jqGrid("#hdnInteractionTypeId").val());
    InteractionObj.FillInteractionSubType($jqGrid("#hdnInteractionTypeId").val());
    $jqGrid("#ddlInteractionStatus").val($jqGrid("#hdnStatuId").val());
    $jqGrid("#ddlInteractionAssignedTo").val($jqGrid("#hdnAssignedTo").val());
    $jqGrid("#ddlInteractionSubType").val($jqGrid("#hdnInteractionSubTypeId").val());
}

// Used to refresh the home page interaction grid
Interaction.prototype.RefreshHomeGrid = function () {
    if (typeof (HomeScriptObj) != "undefined") {
        HomeScriptObj.GetInteactionAlert();
    }
}

// Used refresh the interaction summary and recent order grid
Interaction.prototype.RefreshInteractionSummaryGrid = function () {
    $jqGrid("#jqgh_JqInteractionSummary_CreatedOn").click();
    $jqGrid("#jqgh_JqInteractionSummary_CreatedOn").click();
    //this is required to sort the grid by orderdate desc bydefault    
    $jqGrid("#qPatientInteractionRecentOrderInfoPopup").setGridParam({ sortname: 'OrderDate', sortorder: 'asc' }).trigger("reloadGrid");
    $jqGrid("#jqgh_qPatientInteractionRecentOrderInfoPopup_OrderDate").click();
    $jqGrid("#JqInteractionSummary").setGridParam({ sortname: 'CreatedOn', sortorder: 'desc' }).trigger("reloadGrid");
}

// Changes the text of assigned_to  label
Interaction.prototype.ManditoryAssignedToField = function () {
    var lbldisplay = $jqGrid("#lblAssignedTo").text();
    var isChecked = $jqGrid("#chkDisplayAsAlert").is(':checked');
    var indexOfAstrik = $jqGrid("#lblAssignedTo").text().indexOf('*');
    if (indexOfAstrik != -1) {
        lbldisplay = lbldisplay.substring(0, indexOfAstrik);
    }
    else {
        lbldisplay = lbldisplay;
    }
    if (isChecked) {
        $jqGrid("#lblAssignedTo").text(lbldisplay + " *");
        $jqGrid("#ddlInteractionAssignedTo").removeAttr('disabled');
        $jqGrid("#ddlInteractionAssignedTo").removeClass("btn_disable");
    }
    else {
        $jqGrid("#lblAssignedTo").text(lbldisplay);
        $jqGrid("#ddlInteractionAssignedTo").attr('disabled', 'disabled');
        $jqGrid("#ddlInteractionAssignedTo").val('');
        $jqGrid("#ddlInteractionAssignedTo").addClass("btn_disable");
    }
}

// Use to display the Recent order data
Interaction.prototype.CallRecentOrderPopup = function () {
    var data = $jqGrid("#hdnInteractionRecentOrderValue").val();
    showPopupRecentOrders(data);
}

// Use to navigate the Patient Details page
Interaction.prototype.ShowPatientDetails = function () {    
    if (InteractionObj.IsDirty == true) {
        $jqGrid("#InteractionExitDiv").show();
        $jqGrid("#divInteractionErrorMessage").text('');
        $jqGrid("#divInteractionErrorMessage").addClass("validation_msg").append($jqGrid("#hdnCancelprompt").val());
        $jqGrid(".j_YesClicked").click(function () {
            ShowPatientDetailsCopy();
        });
        $jqGrid(".j_NoClicked").click(function () {
            $jqGrid("#InteractionExitDiv").hide();
        });
    }
    else {
        ShowPatientDetailsCopy();
    }
}

function ShowPatientDetailsCopy() {
    var callingForm = $jqGrid("#" + InteractionObj.InteractionParentFormName).val();
    if (callingForm.toLowerCase() == "home" || callingForm.toLowerCase() == "interactionmenu") {
        var url = $jqGrid("#" + InteractionObj.SetInteractionSession).val();
        $jqGrid.ajax(
            {
                url: url,
                type: "POST",
                async: false,
                cache: false,
                data: {
                    HomePageAlert: callingForm.toLowerCase() == "home",
                    InteractionId: $jqGrid("#" + InteractionObj.HdnInteractionId).val(),
                    ParentFormName: callingForm.toLowerCase(),
                    PatientName: $jqGrid("#" + InteractionObj.txtPatientDisplay).val() + '|' + $jqGrid("#" + InteractionObj.txtPatientSearch).val() + '|' + CommonScriptObj.PatientId + '|' + CommonScriptObj.SAPPatientNumber,
                    CarehomeName: $jqGrid("#" + InteractionObj.txtcareHomeDisplay).val() + '|' + $jqGrid("#" + InteractionObj.txtcareHomeSearch).val() + '|' + CommonScriptObj.CareHomeId + '|' + CommonScriptObj.SAPCareHomeNumber,
                    CustomerName: $jqGrid("#" + InteractionObj.txtCustomerDisplay).val() + '|' + $jqGrid("#" + InteractionObj.txtCustomerSearch).val() + '|' + CommonScriptObj.CustomerId + '|' + +CommonScriptObj.SAPCustomerNumber,
                    OpenCarehomeFlag: false
                },
                success: function (data) {
                }
            }
            );
    }

    var patientId = $jqGrid('#hdnCreateInteractionPatientId').val();
    var customerId = $jqGrid('#hdnCreateInteractionCustomerId').val();
    var patientName = $jqGrid('#txtPatientName').val();
    var customerName = $jqGrid('#txtCustomerName').val();
    var url = $jqGrid('#RenderPatientDetailsAction').val();
    $jqGrid.ajax({
        url: url,
        type: 'POST',
        async: false,
        data: {
            PatientId: patientId, CustomerId: customerId, PatientName: patientName, CustomerName: customerName, RedirectFrom: 'CreateInteraction'
        },
        cache: false,
        success: function (Data) {
            var viewPatientElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnViewPatientMenuText").val() });
            // Sub menu available
            if (viewPatientElement.length == 0) {
                // SubMenu not available. Need to show error message.
                $(".disable_pop").show();
                $(".ui-dialog-titlebar-close").addClass('btn_disable');
                CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnViewPatientAllowMessage").val(), 'pop_error_ul');
                return false;
            }

            window.location.href = $jqGrid('#ShowPatientDetailsInteractionAction').val();

        }
    });
}

// Use to navigate the Care home maintainece page
Interaction.prototype.ShowCarehomeDetails = function () {
    if (InteractionObj.IsDirty == true) {
        $jqGrid("#InteractionExitDiv").show();
        $jqGrid("#divInteractionErrorMessage").text('');
        $jqGrid("#divInteractionErrorMessage").addClass("validation_msg").append($jqGrid("#hdnCancelprompt").val());
        $jqGrid(".j_YesClicked").click(function () {
            ShowCarehomeDetailsCopy();
        });
        $jqGrid(".j_NoClicked").click(function () {
            $jqGrid("#InteractionExitDiv").hide();
        });
    }
    else {
        ShowCarehomeDetailsCopy();
    }
}

function ShowCarehomeDetailsCopy() {
    var callingForm = $jqGrid("#" + InteractionObj.InteractionParentFormName).val();
    if (callingForm.toLowerCase() == "home" || callingForm.toLowerCase() == "interactionmenu") {
        var url = $jqGrid("#" + InteractionObj.SetInteractionSession).val();
        $jqGrid.ajax(
            {
                url: url,
                type: "POST",
                async: false,
                cache: false,
                data: {
                    HomePageAlert: callingForm.toLowerCase() == "home",
                    InteractionId: $jqGrid("#" + InteractionObj.HdnInteractionId).val(),
                    ParentFormName: callingForm.toLowerCase(),
                    PatientName: $jqGrid("#" + InteractionObj.txtPatientDisplay).val() + '|' + $jqGrid("#" + InteractionObj.txtPatientSearch).val() + '|' + CommonScriptObj.PatientId + '|' + CommonScriptObj.SAPPatientNumber,
                    CarehomeName: $jqGrid("#" + InteractionObj.txtcareHomeDisplay).val() + '|' + $jqGrid("#" + InteractionObj.txtcareHomeSearch).val() + '|' + CommonScriptObj.CareHomeId + '|' + CommonScriptObj.SAPCareHomeNumber,
                    CustomerName: $jqGrid("#" + InteractionObj.txtCustomerDisplay).val() + '|' + $jqGrid("#" + InteractionObj.txtCustomerSearch).val() + '|' + CommonScriptObj.CustomerId + '|' + +CommonScriptObj.SAPCustomerNumber,
                    OpenCarehomeFlag: true
                },
                success: function (data) {

                }
            }
            );
    }


    // Check for the Patient View Details ViewOnly or Full Rights
    var viewPatientElement = $("#nav").find(".subs a").filter(function (index) { return $(this).text() == $jqGrid("#hdnViewEditCareHomeDetails").val() });
    // Sub menu available
    if (viewPatientElement.length == 0) {
        // SubMenu not available. Need to show error message.
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage("divErrorToview", $jqGrid("#hdnresViewCarehomeNotAllowedMessage").val(), 'pop_error_ul');
        return false;
    }

    window.location.href = $jqGrid('#ShowCarehomeDetailsAction').val() + "/" + $jqGrid("#hdnCreateInteractionCarehomeId").val();
}

Interaction.prototype.ShowCreateInteraction = function (isDoubleClicked, interactionId, homePageAlert, parentFormName) {
     
    CommonScriptObj.InteractionId = interactionId;
    if ($jqGrid("#divInteractionSummaryPopup").length > 0) {
        document.getElementById("divInteractionSummaryPopup").style.zIndex = -1;
    }
    
    var url = $('#CreateInteractionAction').val();
    $("#divCreateInteraction").load(url, {
        InteractionId: interactionId,
        SAPCustomerId: CommonScriptObj.SAPCustomerNumber,
        SAPPatientId: CommonScriptObj.SAPPatientNumber,
        CustomerName: CommonScriptObj.CustomerName,
        CustomerId: CommonScriptObj.CustomerId,
        PatientId: CommonScriptObj.PatientId == "0" ? "" : CommonScriptObj.PatientId,
        PatientName: CommonScriptObj.PatientName,
        CarehomeId: CommonScriptObj.CareHomeId == "0" ? "" : CommonScriptObj.CareHomeId,
        CarehomeName: CommonScriptObj.CareHomeName,
        IsDoubleClicked: isDoubleClicked,
        SapCarehomeId: CommonScriptObj.SAPCareHomeNumber,
        HomePageAlert: homePageAlert,
        ParentFormName: parentFormName
    }, function () {
            if ($jqGrid('#hdnCreateInteractionPatientId').val() != "" && $jqGrid('#hdnCreateInteractionPatientId').val() != "0") {
                $jqGrid('#btnCarehomeDetails').hide();
            }
            else {
                $jqGrid('#btnPatientDetails').hide();
            }

            var isBtnEnabled = $jqGrid('#hdnIsBtnEnabled').val().toLowerCase();
            if (isBtnEnabled == "false") {
                $jqGrid('#btnCarehomeDetails').addClass("btn_disable");
                $jqGrid('#btnCarehomeDetails').attr("disabled", "disabled");

                $jqGrid('#btnPatientDetails').addClass("btn_disable");
                $jqGrid('#btnPatientDetails').attr("disabled", "disabled");
            }
        $jqGrid("#divCreateInteraction").dialog({
        autoOpen: false,
        modal: true,
        width: 930,
        closeOnEscape: false,
        resizable: true,
        zindex: 2001,
        height: 550,
        title: $jqGrid('#hdnresCreateInteractionTitle').val()
        }).dialog('open')
        IsForceClose = false;
    });

    // View Only implementation from Carehome Screen
    if (parseInt(CommonScriptObj.CareHomeId) > 0) {
        if (typeof (CareHomeObj) != "undefined") {
            if (CareHomeObj.InteractionAccess == "0") {

                // Set Interaction View Only
                setTimeout(function () {
                    //$jqGrid("#createInteractionMainbutton").addClass("read_only_div");
                    // Disable the Save, Carehome, Patient, undo button
                    $jqGrid("#btnSaveInteraction").addClass("btn_disable");
                    $jqGrid("#btnPatientDetails").addClass("btn_disable");
                    $jqGrid("#btnCarehomeDetails").addClass("btn_disable");
                    $jqGrid("#btnUndoInteraction").addClass("btn_disable");
                }, 200);
            }
        }
    }
    // View Only implementation from Home Screen Alert
    if (homePageAlert) {
        if (InteractionObj.HomeAlertContactInfoAccess == "0") {
            // Set Interaction View Only
            setTimeout(function () {
                //$jqGrid("#createInteractionMainbutton").addClass("read_only_div");
                // Disable the Save, Carehome, Patient, undo button
                $jqGrid("#btnSaveInteraction").addClass("btn_disable");
                $jqGrid("#btnPatientDetails").addClass("btn_disable");
                $jqGrid("#btnCarehomeDetails").addClass("btn_disable");
                $jqGrid("#btnUndoInteraction").addClass("btn_disable");
            }, 1000);
        }
    }
}

// Open Interaction Summary page 
Interaction.prototype.ShowPopupInteractionSummary = function (parentFormId) {
    var parentFormName = parentFormId;
    $jqGrid('#divInteractionSummaryPopup').dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        closeOnEscape: false,
        data: {
            ParentFormName: parentFormId
        },
        resizable: true,
        zindex: 1999,
        title: $jqGrid('#hdnresInteractionSummaryTitle').val(),
        open: function () {
            setTimeout(function () {
                $jqGrid(".recent_order_accord").removeAttr('style'); // this is for Internet Explorer
                $jqGrid(".interaction_summary").css({ "display": "block" }); // this is for Carehome
            }, 500);
        }
    });

    $jqGrid('#divInteractionSummaryPopup').dialog('open');
    CommonScriptObj.CallRecentOrderPopup($jqGrid("#hdnInteractionRecentOrderValue").val());

    $jqGrid.ajax({
        url: '/Patient/GetInteractionSummaryDetails',
        type: 'GET',
        cache: false,
        data: {
            PatientId: CommonScriptObj.PatientId, CarehomeId: CommonScriptObj.CareHomeId, CustomerId: CommonScriptObj.CustomerId, ParentFormName: parentFormName
        },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {
            $jqGrid("#InteractionName").text(responseData.LabelName);
            $jqGrid("#JqInteractionSummary").jqGrid('clearGridData');
            if (responseData.Interactions != null) {
                $jqGrid("#JqInteractionSummary").jqGrid('setGridParam', { datatype: 'local', data: responseData.Interactions }).trigger('reloadGrid');
            }
            $jqGrid("#JqInteractionSummary").trigger("reloadGrid", [{ current: true }]);
            return false;
        }
    });

    if (typeof (InteractionObj) != "undefined") {
        InteractionObj.RefreshInteractionSummaryGrid();
    }

    // View Only implementation from Patient Screen
    if (parseInt(CommonScriptObj.PatientId) > 0) {
        if (typeof (PatientInfoObj) != "undefined") {
            if (PatientInfoObj.InteractionAccess == "0") {

                // Set Interaction View Only
                //$jqGrid("#interactionMainButtons").addClass("read_only_div");
                $jqGrid("#btnCreateNewInteraction").addClass("btn_disable");
            }
        }
    }

    // View Only implementation from Carehome Screen
    if (parseInt(CommonScriptObj.CareHomeId) > 0) {
        if (typeof (CareHomeObj) != "undefined") {
            if (CareHomeObj.InteractionAccess == "0") {

                // Set Interaction View Only
                setTimeout(function () {
                    //$jqGrid("#interactionMainButtons").addClass("read_only_div");
                    $jqGrid("#btnCreateNewInteraction").addClass("btn_disable");
                }, 200);
            }
        }
    }
}

Interaction.prototype.CloseCreateInteractionPopup = function (callingForm) {

    var isMenuInteraction = $("#" + InteractionObj.IsMenuInteraction).val();
    if (!isMenuInteraction) {
        InteractionObj.IsInteractionRecentOrderDisplayed = false;
        InteractionObj.IsInteractionSummaryDisplayed = true;

        if (callingForm == 'InteractionSummary') {
            $(".ui-dialog-titlebar-close").trigger('click');
            return false;
        }
        $(".ui-dialog-titlebar-close").trigger('click');
        if (callingForm == "PatientInfo") {
            $(".ui-dialog-titlebar-close").trigger('click');
            InteractionObj.ShowPopupInteractionSummary($('#PatientContactInfo').val());
            InteractionObj.RefreshInteractionSummaryGrid();
            return false;
        }
        else {
            CommonScriptObj.PatientId = "";
            InteractionObj.ShowPopupInteractionSummary('CareHome');
            $jqGrid("#jqgh_JqInteractionSummary_CreatedOn").click();
            $jqGrid("#jqgh_JqInteractionSummary_CreatedOn").click();
            InteractionObj.RefreshInteractionSummaryGrid();
            InteractionObj.RefreshHomeGrid();
            return false;
        }
    }
    else {
        $(".ui-dialog-titlebar-close").trigger('click');
        $jqGrid("#" + InteractionObj.BtnInteractionSummary).click();
    }
}


$jqGrid('#' + InteractionObj.txtCustomerSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: customerTable, customerId: 0 }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    customerId: el.Id
                };
            }));
        });
    },
    select: function (event, ui) {
        CommonScriptObj.CustomerId = ui.item.customerId;
        var customerNameSAPNumber = ui.item.label;
        if (customerNameSAPNumber != '') {
            customerNameSAPNumber = customerNameSAPNumber.split('(');
        }
        CommonScriptObj.SAPCustomerNumber = customerNameSAPNumber[1].split(')')[0];
        CommonScriptObj.CustomerName = customerNameSAPNumber[0];
        $jqGrid('#' + InteractionObj.txtCustomerDisplay + '').val(ui.item.label.substring(0, ui.item.label.indexOf("(")));
        $jqGrid(".ui-autocomplete").hide();
    }
});


$jqGrid('#' + InteractionObj.txtPatientSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: patientTable, customerId: CommonScriptObj.CustomerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    patientId: el.Id
                };
            }));
        });
    },
    select: function (event, ui) {
        CommonScriptObj.PatientId = ui.item.patientId;
        CommonScriptObj.CareHomeId = 0;
        var patientName = ui.item.label;
        if (patientName != '') {
            patientName = patientName.split('(');
            CommonScriptObj.SAPPatientNumber = patientName[1].split(')')[0];
            CommonScriptObj.PatientName = patientName[0];
        }
        $jqGrid('#' + InteractionObj.txtPatientDisplay + '').val(ui.item.label.substring(0, ui.item.label.indexOf("(")));
        $jqGrid(".ui-autocomplete").hide();
    }
});

$jqGrid('#' + InteractionObj.txtcareHomeSearch + '').autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/AuditLog/GetSearchResults', { term: request.term, searchTable: careHomeTable, customerId: CommonScriptObj.CustomerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    careHomeId: el.Id
                };
            }));
        });
    },
    select: function (event, ui) {
        CommonScriptObj.CareHomeId = ui.item.careHomeId;
        CommonScriptObj.PatientId = 0;
        var careHometName = ui.item.label;
        if (careHometName != '') {
            careHometName = careHometName.split('(');
            CommonScriptObj.SAPCareHomeNumber = careHometName[1].split(')')[0];
            CommonScriptObj.CareHomeName = careHometName[0];
        }
        $jqGrid('#' + InteractionObj.txtcareHomeDisplay + '').val(ui.item.label.substring(0, ui.item.label.indexOf("(")));
        $jqGrid(".ui-autocomplete").hide();
    }
});

$jqGrid('#' + InteractionObj.txtPatientSearch + '').on('keyup', function (e) {
    $jqGrid('#' + InteractionObj.BtnInteractionReset + '').removeClass(InteractionObj.classBtnDisable);
    InteractionObj.ClearCareHome();    
    var charCode = (e.which) ? e.which : e.keyCode;    
    if (charCode == 8 || charCode == 46) {        
        InteractionObj.ClearPatientName();
    }
    if ($jqGrid('#' + InteractionObj.txtCustomerSearch + '').val() == '' || CommonScriptObj.CustomerId == 0) {
        var errorMessage = $jqGrid("#" + InteractionObj.HdnErrorMessageForCustomer).val();
        CommonScriptObj.DisplayErrorMessage(InteractionObj.divSelectCustomer, errorMessage, InteractionObj.classErrorMessage);
        return false;
    }

});

$jqGrid('#' + InteractionObj.txtcareHomeSearch + '').on('keyup', function (e) {
    $jqGrid('#' + InteractionObj.BtnInteractionReset + '').removeClass(InteractionObj.classBtnDisable);
    InteractionObj.ClearPatient();
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode == 8 || charCode == 46) {
        InteractionObj.ClearCareHomeName();
    }
    if ($jqGrid('#' + InteractionObj.txtCustomerSearch + '').val() == '' || CommonScriptObj.CustomerId == 0) {
        var errorMessage = $jqGrid("#" + InteractionObj.HdnErrorMessageForCustomer).val();
        CommonScriptObj.DisplayErrorMessage(InteractionObj.divSelectCustomer, errorMessage, InteractionObj.classErrorMessage);
        return false;
    }

});

$jqGrid('#' + InteractionObj.txtCustomerSearch + '').on('keyup', function (e) {
    $jqGrid('#' + InteractionObj.BtnInteractionReset + '').removeClass(InteractionObj.classBtnDisable);
    var charCode = (e.which) ? e.which : e.keyCode;        
    if (charCode == 8 || charCode == 46) {
        InteractionObj.ClearCustomerName();
    }
    InteractionObj.ClearCareHome();
    InteractionObj.ClearPatient();
});


Interaction.prototype.ClearPatient = function () {
    $jqGrid('#' + InteractionObj.txtPatientSearch + '').val('');
    $jqGrid('#' + InteractionObj.txtPatientDisplay + '').val('');
    InteractionObj.InteractionPatinetId = 0;
}

Interaction.prototype.ClearPatientName = function () {
    $jqGrid('#' + InteractionObj.txtPatientDisplay + '').val('');
    InteractionObj.InteractionPatinetId = 0;
}

Interaction.prototype.ClearCareHome = function () {
    $jqGrid('#' + InteractionObj.txtcareHomeSearch + '').val('');
    $jqGrid('#' + InteractionObj.txtcareHomeDisplay + '').val('');
    InteractionObj.InteractionCareHomeId = 0;
}

Interaction.prototype.ClearCareHomeName = function () {
    $jqGrid('#' + InteractionObj.txtcareHomeDisplay + '').val('');
    InteractionObj.InteractionCareHomeId = 0;
}

Interaction.prototype.ClearCustomer = function ()
{
    $jqGrid("#" + InteractionObj.txtCustomerSearch).val('');
    $jqGrid("#" + InteractionObj.txtCustomerDisplay).val('');
}
Interaction.prototype.ClearCustomerName = function ()
{   
    $jqGrid("#" + InteractionObj.txtCustomerDisplay).val('');
}

Interaction.prototype.GetInteractionAccessForHomeAlert = function () {
    var status = false;
    $jqGrid.ajax({
        url: '/Patient/GetInteractionAccess',
        type: 'GET',
        cache: false,
        async: false,
        success: function (responseData) {
            // Set the Contact Info, Attachment Edit access as per Role.
            if (responseData.InteractionAccess.length > 0) {
                InteractionObj.HomeAlertContactInfoAccess = responseData.InteractionAccess;
                status = true;
            }
        }
    });

    return status;
}

Interaction.prototype.InteractionShowHideError = function () {
    if (InteractionObj.IsDirty == true) {
        $jqGrid("#InteractionExitDiv").show();
        $jqGrid("#divInteractionErrorMessage").text('');
        $jqGrid("#divInteractionErrorMessage").addClass("validation_msg").append($jqGrid("#hdnCancelprompt").val());
        $jqGrid(".j_YesClicked").click(function () {
            var callingForm = $jqGrid("#" + InteractionObj.HdnCallingForm).val();
            ClearExisitingDataOnExist();
            IsForceClose = true;
            InteractionObj.CloseCreateInteractionPopup(callingForm);
            $jqGrid("#InteractionExitDiv").hide();
        });
        $jqGrid(".j_NoClicked").click(function () {
            $jqGrid("#InteractionExitDiv").hide();
        });
    }
    else {
        IsForceClose = true;
        var callingForm = $jqGrid("#" + InteractionObj.HdnCallingForm).val();
        InteractionObj.CloseCreateInteractionPopup(callingForm);
    }
}

function ClearExisitingDataOnExist() {
    $jqGrid("#txtCareHomeName").val('');
    $jqGrid("#txtPatientName").val('');
    $jqGrid("#txtCustomerName").val('');
    $jqGrid("#txtInteractionCreatedDate").val('');
    $jqGrid("#txtInteractionCreatedBy").val('');
    $jqGrid("#txtClosedDate").val('');
    $jqGrid("#txtClosedBy").val('');
    $jqGrid("#txtResolvedDate").val('');
    $jqGrid("#txtResolvedBy").val('');
    $jqGrid('#chkResolved').prop("checked", false);
    $jqGrid("#ddlInteractionType").val('');
    $jqGrid("#ddlInteractionStatus").val('');
    $jqGrid("#ddlInteractionSubType").val('');
    $jqGrid("#txtInteractionCreatedDate").val('');
    $jqGrid("#txtInteractionCreatedBy").val('');
}

$jqGrid("#divCreateInteraction").on('dialogbeforeclose', function (event) {
    $(this).siblings(".ui-dialog-titlebar").find("button").blur();
    if (!IsForceClose) {
        event.preventDefault();
        InteractionObj.ExitCreateInteractionPopup();
    }
})