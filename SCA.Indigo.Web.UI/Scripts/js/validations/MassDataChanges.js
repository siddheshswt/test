﻿$jqGrid = jQuery.noConflict();

//// Object declaration
var MassDataChangesObj = new MassDataChanges();

// controls declaration
MassDataChanges.prototype.DownloadTypeDropdown = "ddlDownloadType";
MassDataChanges.prototype.PatientTypeDropdown = "ddlPatientType";
MassDataChanges.prototype.PatientStatusDropdown = "ddlPatientStatus";
MassDataChanges.prototype.CarehomeOrderDropdown = "ddlOrderType";
MassDataChanges.prototype.PatientIds = "txtPatientId";
MassDataChanges.prototype.CarehomeIds = "txtCarehomeId";
MassDataChanges.prototype.ContactPersonType = "ddlContactPersonType";
MassDataChanges.prototype.CommunicationType = "ddlCommunicationType";
MassDataChanges.prototype.BtnMassChangesDownloadLog = "btnMassChangesDownloadLog";
MassDataChanges.prototype.CustomerDropdown = "ddlCustomerColumns";
MassDataChanges.prototype.CareHomeStatusDropdown = "ddlUpdateCareHomeStatus";

// Div declaration
MassDataChanges.prototype.CustomeSection = "divCustomerSection";
MassDataChanges.prototype.CarehomeSection = "divCarehomeSection";
MassDataChanges.prototype.PatientSection = "divPatientSection";
MassDataChanges.prototype.PrescriptionSection = "divPrescriptionSection";
MassDataChanges.prototype.ErrorMessage = "divMasssChangeErrorMessage";
MassDataChanges.prototype.DownloadData = "divDownloadData";
MassDataChanges.prototype.UploadFile = "divUploadFile";
MassDataChanges.prototype.divPrescUpdateValidation = "divPrescUpdateValidation";
MassDataChanges.prototype.PrescValidationMsgText = "divPrescValidationMsgText";
MassDataChanges.prototype.PatinetMedicalAnalysisSection = "divPatinetMedicalAnalysis";
MassDataChanges.prototype.ContactPersonSection = "divContactPersonSection";
MassDataChanges.prototype.CommunicationSection = "divCommunicationSection";
MassDataChanges.prototype.ContactPersonPatientCarehomeId = "divContactPatientCarehomeId";
MassDataChanges.prototype.CommunicationPatientCarehomeId = "divCommunicationPatientCarehomeId";
MassDataChanges.prototype.OrderSection = "divOrderSection";
MassDataChanges.prototype.MultipleCustomerSection = "divMultipleCustomerSection";


// Enum declaration
MassDataChanges.prototype.EnumPatient = "hdnEnumPatient";
MassDataChanges.prototype.EnumCarehome = "hdnEnumCarehome";
MassDataChanges.prototype.EnumPrescription = "hdnEnumPrescription";
MassDataChanges.prototype.EnumContactPerson = "hdnEnumContactPerson";
MassDataChanges.prototype.EnumCommunicationFormat = "hdnEnumCommunicationFormat";
MassDataChanges.prototype.EnumPostCodeMatrix = "hdnEnumPostCodeMatrix";
MassDataChanges.prototype.EnumCustomerClinicalContacts = "hdnEnumCustomerClinicalContacts";
MassDataChanges.prototype.EnumCustomerAnalysisInformation = "hdnEnumCustomerAnalysisInformation";
MassDataChanges.prototype.EnumPatientClinicalContacts = "hdnEnumPatientClinicalContacts";
MassDataChanges.prototype.EnumPatientAnalysisInformation = "hdnEnumPatientAnalysisInformation";
MassDataChanges.prototype.EnumOrder = "hdnEnumOrder";

// span declaration
MassDataChanges.prototype.SpanPatientCarehomeId = "spnPatientCarehomeId";
MassDataChanges.prototype.SpanCommunicationPatientCarehomeId = "spnCommPatientCarehomeId";

//hidden field validation
MassDataChanges.prototype.CustomerValidation = "hdnCustomerValidation";
MassDataChanges.prototype.PatientCarehomeValidation = "hdnPatientCarehomeValidation";
MassDataChanges.prototype.ErrorCannotExport = "hdnErrorCannotExport";
MassDataChanges.prototype.MassChangesController = "hdnMassChangesController";
MassDataChanges.prototype.DownloadActionMethod = "hdnDownloadActionMethod";
MassDataChanges.prototype.hdnErrorSelectCustomer = "hdnErrorSelectCustomer";
MassDataChanges.prototype.PatientValidation = "hdnPatientValidation";
MassDataChanges.prototype.ErrorContactPersonType = "hdnErrorContactPersonType";
MassDataChanges.prototype.ErrorCommunicationPersonType = "hdnErrorCommunicationPersonType";

// Buttons used
MassDataChanges.prototype.DownloadDataButton = "btnDownloadData";
MassDataChanges.prototype.btnPrescriptionDownload = "btnPrescriptionDownload";

//TextFields declaration
MassDataChanges.prototype.txtPrescUpdateCustomerSearch = "txtPrescUpdateCustomerSearch";
MassDataChanges.prototype.txtPrescUpdateCustomerDisplay = "txtPrescUpdateCustomerDisplay";
MassDataChanges.prototype.txtPrescUpdateProductSearch = "txtPrescUpdateProductSearch";
MassDataChanges.prototype.txtPrescUpdateProductDisplay = "txtPrescUpdateProductDisplay";
MassDataChanges.prototype.ContactPatientCarehomeId = "txtContactPatientCarehomeId";
MassDataChanges.prototype.CommunicationPatientCarehomeId = "txtCommPatientCarehomeId";
MassDataChanges.prototype.ClinicalPatientIds = "txtClinicalPatientId";
MassDataChanges.prototype.txtOrderUpdateProductSearch = "txtOrderUpdateProductSearch";
MassDataChanges.prototype.txtOrderUpdateProductDisplay = "txtOrderUpdateProductDisplay";

MassDataChanges.prototype.SearchCustomerTable = "hdnSearchCustomerTable";
MassDataChanges.prototype.SearchProductTable = "hdnSearchProductTable";
MassDataChanges.prototype.AppConfigMaxSixe = "hdnAppConfigMaxSixe";
MassDataChanges.prototype.UploadType = "ddlUploadType";
MassDataChanges.prototype.MassUpdateUploadExcel = "MassUpdateUploadExcel";
MassDataChanges.prototype.btnMassUpdateUpload = "btnMassUpdateUpload";

MassDataChanges.prototype.txtOrderUpdateOrderIds = "txtOrderUpdateOrderIds";
MassDataChanges.prototype.ddlOrderUpdateOrderStatus = "ddlOrderUpdateOrderStatus";
MassDataChanges.prototype.ddlOrderUpdateOrderType = "ddlOrderUpdateOrderType";

MassDataChanges.prototype.txtOrderUpdateCareHomeIds = "txtOrderUpdateCareHomeIds";
MassDataChanges.prototype.ddlOrderUpdateCareHomeStatus = "ddlOrderUpdateCareHomeStatus";
MassDataChanges.prototype.ddlOrderUpdateCareHomeOrderType = "ddlOrderUpdateCareHomeOrderType";

MassDataChanges.prototype.txtOrderUpdatePatientIds = "txtOrderUpdatePatientIds";
MassDataChanges.prototype.ddlOrderUpdatePatientStatus = "ddlOrderUpdatePatientStatus";
MassDataChanges.prototype.ddlOrderUpdatePatientType = "ddlOrderUpdatePatientType";
MassDataChanges.prototype.HdnDownloadLogFile = "hdnDownloadLogFile";
MassDataChanges.prototype.btnMassChangesUndo = "btnMassChangesUndo";
MassDataChanges.prototype.IsDirty = false;

MassDataChanges.prototype.hdnOrderStatusGenerated = "hdnOrderStatusGenerated";
MassDataChanges.prototype.hdnOrderStatusSentToSAP = "hdnOrderStatusSentToSAP";
MassDataChanges.prototype.hdnOrderStatusFailed = "hdnOrderStatusFailed";
MassDataChanges.prototype.LogfilePath = "";

MassDataChanges.prototype.ExcelFormat = "hdnExcelFormat";

MassDataChanges.prototype.PatientTypeDefaultText = "hdnDefaultTextPatientTypeDDL";
MassDataChanges.prototype.PatientStatusDefaultText = "hdnDefaultTextPatientStatusDDL";
MassDataChanges.prototype.CarehomeOrderTypeDefaultText = "hdnDefaultTextCarehomeOrderTypeDDL";
MassDataChanges.prototype.CareHomeStatusDefaultText = "hdnDefaultTextCareHomeStatusDDL";
MassDataChanges.prototype.OrderTypeDefaultText = "hdnDefaultTextOrderTypeDDL";
MassDataChanges.prototype.OrderStatusDefaultText = "hdnDefaultTextOrderStatusDDL";
MassDataChanges.prototype.FileNotSelected = "hdnFilNotSelected";

MassDataChangesObj.FileData = new FormData();

//// Form declaration
function MassDataChanges() { }

var customerId = 0;
var productId = 0;
var sapProductId = '';

$jqGrid(document).ready(function () {

    CommonScriptObj.EnableDisableFileUpload(MassDataChangesObj.UploadType, MassDataChangesObj.MassUpdateUploadExcel, MassDataChangesObj.btnMassUpdateUpload);

    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch + '').on('keyup', function (e) {
        MassDataChangesObj.IsDirty = true;
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8) {
            MassDataChangesObj.ClearCustomerName();
            MassDataChangesObj.ClearProduct();
        }
    });

    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductSearch + '').on('keyup', function (e) {
        if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
            MassDataChangesObj.ShowCustomerValidation();
            MassDataChangesObj.ClearProduct();
        }

        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8) {
            MassDataChangesObj.ClearProductName();
        }
    });

    $jqGrid('#' + MassDataChangesObj.txtOrderUpdatePatientIds + '').on('keyup', function (e) {
        if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
            MassDataChangesObj.ShowCustomerValidation();
            $jqGrid('#' + MassDataChangesObj.txtOrderUpdatePatientIds + '').val('');
            return false;
        }
        MassDataChangesObj.ClearOrderCareHome();
    });

    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateCareHomeIds + '').on('keyup', function (e) {
        if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
            MassDataChangesObj.ShowCustomerValidation();
            $jqGrid('#' + MassDataChangesObj.txtOrderUpdateCareHomeIds + '').val('');
            return false;
        }
        MassDataChangesObj.ClearOrderPatient();
    });

    $jqGrid("#" + MassDataChangesObj.BtnMassChangesDownloadLog).bind("click", function (e) {
        //localhost:D$\Uploads\MassChangesUploads\Patient_0d471f3a-c86f-4678-8a76-d4474245f3df.xls
        var data = $jqGrid("#" + MassDataChangesObj.HdnDownloadLogFile).val();
        window.open(data, "_blank");
    });

    $jqGrid("#" + MassDataChangesObj.btnMassChangesUndo).bind("click", function () {

        if (MassDataChangesObj.IsDirty) {
            $jqGrid('#divCancelPrompt').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralYes").val().toString(),
                    "id": "hdnGeneralYes",
                    click: function () {
                        window.location.reload();
                    },
                }, {
                    text: $jqGrid("#hdnGeneralNo").val().toString(),
                    "id": "hdnGeneralNo",
                    click: function () {
                        $jqGrid('#divCancelPrompt').dialog('close');
                    },
                }]
            });
            $jqGrid('#divCancelPrompt').dialog('open');
        }
    });

    $jqGrid("#" + MassDataChangesObj.UploadType).bind("change", function () {
        MassDataChangesObj.IsDirty = true;
        CommonScriptObj.EnableDisableFileUpload(MassDataChangesObj.UploadType, MassDataChangesObj.MassUpdateUploadExcel, MassDataChangesObj.btnMassUpdateUpload);
        if (typeof (MassDataChangesObj) != "undefined") {
            MassDataChangesObj.DiableDownloadButton();
        }
    });


    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateOrderIds + '').on('keyup', function (e) {
        if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
            MassDataChangesObj.ShowCustomerValidation();
            $jqGrid('#' + MassDataChangesObj.txtOrderUpdateOrderIds + '').val('');
            return false;
        }
        //MassDataChangesObj.ClearOrderPatient();
    });

    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateProductSearch + '').on('keyup', function (e) {
        if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
            MassDataChangesObj.ShowCustomerValidation();
            $jqGrid('#' + MassDataChangesObj.txtOrderUpdateProductSearch + '').val('');
            return false;
        }
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode == 8) {
            MassDataChangesObj.ClearOrderProductName();
        }
    });

    MassDataChangesObj.InitializeOrderUpdateDropdowns();

    setTimeout(function () {
        if (typeof (MassDataChangesObj) != "undefined")
        {
            MassDataChangesObj.DiableDownloadButton();
        }
    }, 500);

    CommonScriptObj.DisableFormControls("frm_MasterMassDataChange");
});

// Call this function if there is no filter criteria.
MassDataChanges.prototype.ShowDownloadButtonOnly = function () {
    $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
    $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
    $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
    $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
    $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
    $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
    $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
}

// Enable Disable section on Patient / Carehome dropdown
MassDataChanges.prototype.ToggelContactPersonSection = function (obj) {
    $jqGrid("#" + MassDataChangesObj.ContactPersonPatientCarehomeId).val("");
    $jqGrid("#txtContactPatientCarehomeId").val("");
    $jqGrid("#txtCommPatientCarehomeId").val("");

    if ($("#" + obj.id).val() == $("#" + MassDataChangesObj.ContactPersonType + " option").eq(1).val()) {
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "none";
        $("#divCommunicationPatientCarehomeId")[0].style.display = "none";
    }
    else if ($("#" + obj.id).val() == $("#" + MassDataChangesObj.ContactPersonType + " option").eq(2).val()) {
        $("#" + MassDataChangesObj.SpanPatientCarehomeId).text("Enter Patient Id's");
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "";
        $("#divCommunicationPatientCarehomeId")[0].style.display = "none";
    }
    else if ($("#" + obj.id).val() == $("#" + MassDataChangesObj.ContactPersonType + " option").eq(3).val()) {
        $("#" + MassDataChangesObj.SpanPatientCarehomeId).text("Enter Carehome Id's");
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "";
        $("#divCommunicationPatientCarehomeId")[0].style.display = "none";
    }
    else {
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "none";
        $("#divCommunicationPatientCarehomeId")[0].style.display = "none";
    }
}

// Enable Disable section on Patient / Carehome dropdown
MassDataChanges.prototype.ToggelCommunicationSection = function (obj) {
    $jqGrid("#" + MassDataChangesObj.CommunicationPatientCarehomeId).val("");
    $jqGrid("#txtContactPatientCarehomeId").val("");
    $jqGrid("#txtCommPatientCarehomeId").val("");

    if ($("#" + obj.id).val() == $("#" + MassDataChangesObj.CommunicationType + " option").eq(1).val()) {
        $("#" + MassDataChangesObj.SpanCommunicationPatientCarehomeId).text("Enter Patient Id's");
        //$("#" + MassDataChangesObj.CommunicationPatientCarehomeId)[0].style.display = "";
        $("#divCommunicationPatientCarehomeId")[0].style.display = ""
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "none";
    }
    else if ($("#" + obj.id).val() == $("#" + MassDataChangesObj.CommunicationType + " option").eq(2).val()) {
        $("#" + MassDataChangesObj.SpanCommunicationPatientCarehomeId).text("Enter Carehome Id's");
        //$("#" + MassDataChangesObj.CommunicationPatientCarehomeId)[0].style.display = "";
        $("#divCommunicationPatientCarehomeId")[0].style.display = ""
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "none";
    }
    else {
        //$("#" + MassDataChangesObj.CommunicationPatientCarehomeId)[0].style.display = "none";
        $("#divCommunicationPatientCarehomeId")[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonPatientCarehomeId)[0].style.display = "none";
    }
}

// Enable Disable section on Patient / Carehome dropdown
MassDataChanges.prototype.ToggelSection = function (obj) {    
    MassDataChangesObj.IsDirty = true;

    if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPatient).val()) {

        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
        $("#" + MassDataChangesObj.ContactPersonType + " option[value='1']").hide();
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumCarehome).val()) {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPrescription).val()) {
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "";
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPatientAnalysisInformation).val() || ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPatientClinicalContacts).val())) {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumContactPerson).val()) {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
        $jqGrid("#ddlContactPersonType").val($("#ddlCommunicationType option").eq(0).val());
        $jqGrid("#txtContactPatientCarehomeId").val("");
        $("#divContactPatientCarehomeId")[0].style.display = "none";
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumCommunicationFormat).val()) {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
        $jqGrid("#ddlCommunicationType").val($("#ddlCommunicationType option").eq(0).val());
        $jqGrid("#txtCommPatientCarehomeId").val("");
        $("#divCommunicationPatientCarehomeId")[0].style.display = "none";
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumOrder).val()) {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";

    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPostCodeMatrix).val()) {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
    }
    else if ($("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumCommunicationFormat).val()
        || $("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumCustomerClinicalContacts).val()
    || $("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumCustomerAnalysisInformation).val()
    || $("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPatientClinicalContacts).val()
    || $("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumPatientAnalysisInformation).val()
    || $("#" + obj.id).val() == $jqGrid('#' + MassDataChangesObj.EnumOrder).val()) {

        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        MassDataChangesObj.ShowDownloadButtonOnly();
    }
    else {
        $("#" + MassDataChangesObj.CustomeSection)[0].style.display = "";
        $("#" + MassDataChangesObj.PrescriptionSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CarehomeSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatientSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.DownloadData)[0].style.display = "none";
        $("#" + MassDataChangesObj.ContactPersonSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.CommunicationSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.PatinetMedicalAnalysisSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.OrderSection)[0].style.display = "none";
        $("#" + MassDataChangesObj.MultipleCustomerSection)[0].style.display = "none";
    }

    //Added this code to clear previous selections and values
    MassDataChangesObj.ClearAllElementValues();

    return false;
}

// Master Mass Data Change - Download functionality
MassDataChanges.prototype.MasterMassDataChangeDownloadData = function () {
    var multiselectCustomer = $jqGrid("#" + MassDataChangesObj.CustomerDropdown).multiselect('getChecked');    
    if (customerId == 0 && multiselectCustomer.length == 0) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.CustomerValidation).val(), 'pop_error_ul');
        return false;
    }

    if ($("#" + MassDataChangesObj.DownloadTypeDropdown + " option:selected").val() == 0) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.PatientCarehomeValidation).val(), 'pop_error_ul');
        return false;
    }

    var contactPersonId;
    var contactCommunicationType;

    var downloadType = $("#" + MassDataChangesObj.DownloadTypeDropdown + " option:selected").val();
    if (downloadType == $jqGrid('#' + MassDataChangesObj.EnumContactPerson).val()) {
        if ($jqGrid("#" + MassDataChangesObj.ContactPersonType + " option:selected").val() == 0) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.ErrorContactPersonType).val(), 'pop_error_ul');
            return false;
        }

        contactPersonId = $.trim($jqGrid("#" + MassDataChangesObj.ContactPatientCarehomeId).val()) == "" ? "" : $.trim($jqGrid("#" + MassDataChangesObj.ContactPatientCarehomeId).val());
        contactCommunicationType = $jqGrid("#" + MassDataChangesObj.ContactPersonType + " option:selected").val();
    }

    if (downloadType == $jqGrid('#' + MassDataChangesObj.EnumCommunicationFormat).val()) {
        if ($jqGrid("#" + MassDataChangesObj.CommunicationType + " option:selected").val() == 0) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.ErrorCommunicationPersonType).val(), 'pop_error_ul');
            return false;
        }
        contactPersonId = $.trim($jqGrid("#" + MassDataChangesObj.CommunicationPatientCarehomeId).val()) == "" ? "" : $.trim($jqGrid("#" + MassDataChangesObj.CommunicationPatientCarehomeId).val());
        contactCommunicationType = $jqGrid("#" + MassDataChangesObj.CommunicationType + " option:selected").val();
    }

    var patientId = $.trim($jqGrid("#" + MassDataChangesObj.PatientIds).val());

    if (downloadType == $jqGrid('#' + MassDataChangesObj.EnumPatientAnalysisInformation).val() || (downloadType == $jqGrid('#' + MassDataChangesObj.EnumPatientClinicalContacts).val())) {
        patientId = $.trim($jqGrid("#" + MassDataChangesObj.ClinicalPatientIds).val());
        //if (patientId == "") {
        //    $(".disable_pop").show();
        //    $(".ui-dialog-titlebar-close").addClass('btn_disable');
        //    CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.PatientValidation).val(), 'pop_error_ul');
        //    return false;
        //}
    }

    var CustomerForPostCodeMatrix = [];
    var patientsForOrderUpdate = "";
    var CareHomesForOrderUpdate = "";
    var OrderIdsForOrderUpdate = "";

    var patientTypesForOrderUpdate = [];
    var CareHomeOrderTypesForOrderUpdate = [];
    var OrderTypesForOrderUpdate = [];

    var PatientStatusForOrderUpdate = [];
    var CareHomeStatusForOrderUpdate = [];
    var OrderStatusForOrderUpdate = [];
    var CareHomeStatusForCarehome = [];
    var patientType = "";
    var patientStatus = "";

    $jqGrid("#" + MassDataChangesObj.CustomerDropdown).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
        function (e, item) {
            CustomerForPostCodeMatrix.push(item);
        });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientType).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
        function (e, item) {            
            patientTypesForOrderUpdate.push(item);
        });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeOrderType).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
        function (e, item) {
            CareHomeOrderTypesForOrderUpdate.push(item);
        });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderType).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
            function (e, item) {                
                OrderTypesForOrderUpdate.push(item);
            });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientStatus).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
        function (e, item) {            
            PatientStatusForOrderUpdate.push(item);
        });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeStatus).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
        function (e, item) {            
            CareHomeStatusForOrderUpdate.push(item);
        });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderStatus).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
    function (e, item) {        
        OrderStatusForOrderUpdate.push(item);
    });

    $jqGrid("#" + MassDataChangesObj.CareHomeStatusDropdown).multiselect('getChecked').map(function (index) {
        return $(this).attr('value');
    }).each(
    function (e, item) {
        CareHomeStatusForCarehome.push(item);
    });
    
    if ($jqGrid("#" + MassDataChangesObj.PatientTypeDropdown).val() != "" && $jqGrid("#" + MassDataChangesObj.PatientTypeDropdown).val() != null) {
        patientType = "," + $jqGrid("#" + MassDataChangesObj.PatientTypeDropdown).multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        patientType = patientType.substring(1, patientType.length)
    }

    if ($jqGrid("#" + MassDataChangesObj.PatientStatusDropdown).val() != "" && $jqGrid("#" + MassDataChangesObj.PatientStatusDropdown).val() != null) {
        patientStatus = "," + $jqGrid("#" + MassDataChangesObj.PatientStatusDropdown).multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        patientStatus = patientStatus.substring(1, patientStatus.length)
    }

    var OrderDataDownloadFor = MassDataChangesObj.OrderDownloadFor(patientTypesForOrderUpdate
                                                                , CareHomeOrderTypesForOrderUpdate
                                                                , PatientStatusForOrderUpdate
                                                                , CareHomeStatusForOrderUpdate
                                                                );
    
    
    // Download data
    var url = '/' + $("#" + MassDataChangesObj.MassChangesController).val() + '/' + $("#" + MassDataChangesObj.DownloadActionMethod).val();
    $jqGrid.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
            CustomerId: customerId,
            PatientTypeId: patientType,
            PatientStatusId: patientStatus,
            OrderTypeId: $jqGrid("#" + MassDataChangesObj.CarehomeOrderDropdown + " option:selected").val(),
            PatientId: patientId,
            CarehomeId: $.trim($jqGrid("#" + MassDataChangesObj.CarehomeIds).val()),
            DownloadTypeId: downloadType,
            ProductId: productId,
            ContactPatientCarehomeId: contactPersonId,
            ContactPersonType: contactCommunicationType,

            PatientTypesForOrderUpdate: patientTypesForOrderUpdate,
            CareHomeOrderTypesForOrderUpdate: CareHomeOrderTypesForOrderUpdate,
            OrderTypesForOrderUpdate: OrderTypesForOrderUpdate,
            CustomerIdForPostCodeMatrix: CustomerForPostCodeMatrix,

            PatientStatusForOrderUpdate: PatientStatusForOrderUpdate,
            CareHomeStatusForOrderUpdate: CareHomeStatusForOrderUpdate,
            OrderStatusForOrderUpdate: OrderStatusForOrderUpdate,

            PatientsForOrderUpdate: $jqGrid('#' + MassDataChangesObj.txtOrderUpdatePatientIds).val(),
            CareHomesForOrderUpdate: $jqGrid('#' + MassDataChangesObj.txtOrderUpdateCareHomeIds).val(),
            OrderIdsForOrderUpdate: $jqGrid('#' + MassDataChangesObj.txtOrderUpdateOrderIds).val(),
            OrderDataDownloadFor: OrderDataDownloadFor,
            SAPProductIdForOrderUpdate: sapProductId,
            CareHomeStatusForCareHome: CareHomeStatusForCarehome,
        },
        async: false,
        cache: false,
        success: function (retData) {
            if (retData.Count > 0) {
                // Download data for Master Mass Changes.                
                window.open(retData.Returnpath);
            }
            else {
                // Cannot export data due to no records found.
                $(".disable_pop").show();
                $(".ui-dialog-titlebar-close").addClass('btn_disable');
                CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.ErrorCannotExport).val(), 'pop_error_ul');
            }
        } // end succes function
    });

    return false;
}

$jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/MassDataChanges/GetSearchResults', {
            term: request.term, searchTable: $jqGrid('#' + MassDataChangesObj.SearchCustomerTable).val(), customerId: 0
        }, function (data) {
            $jqGrid.ajaxSetup({
                cache: false
            });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId === '' ? el.SearchAutoCompleteName.substring(0, el.SearchAutoCompleteName.indexOf("(")) : el.SAPId,
                    Id: el.Id
                };
            }));
        });
    },
    select: function (event, ui) {
        customerId = ui.item.Id;
        $('#' + MassDataChangesObj.txtPrescUpdateCustomerDisplay).val(ui.item.label.substring(0, ui.item.label.indexOf("(")));
    }
});

$jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductSearch).autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/MassDataChanges/GetSearchResults', { term: request.term, searchTable: $jqGrid('#' + MassDataChangesObj.SearchProductTable).val(), customerId: customerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId,
                    Id: el.Id,
                    SAPId: el.SAPId
                };
            }));
        });
    },
    select: function (event, ui) {
        productId = ui.item.Id;
        $('#' + MassDataChangesObj.txtPrescUpdateProductDisplay).val(ui.item.label);
    }
});



$jqGrid('#' + MassDataChangesObj.txtOrderUpdateProductSearch).autocomplete({
    minLength: 3,
    source: function (request, response) {
        $jqGrid.getJSON('/MassDataChanges/GetSearchResults', { term: request.term, searchTable: $jqGrid('#' + MassDataChangesObj.SearchProductTable).val(), customerId: customerId }, function (data) {
            $jqGrid.ajaxSetup({ cache: false });
            response($jqGrid.map(data, function (el) {
                return {
                    label: el.SearchAutoCompleteName,
                    value: el.SAPId,
                    Id: el.Id,
                    SAPId: el.SAPId
                };
            }));
        });
    },
    select: function (event, ui) {
        productId = ui.item.Id;
        sapProductId = ui.item.SAPId;
        $('#' + MassDataChangesObj.txtOrderUpdateProductDisplay).val(ui.item.label);
    }
});


MassDataChanges.prototype.ClearCustomer = function () {
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val('');
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerDisplay).val('');
    customerId = 0;

}

MassDataChanges.prototype.ClearCustomerName = function () {    
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerDisplay).val('');
    customerId = 0;

}

MassDataChanges.prototype.ClearProduct = function () {
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductSearch).val('');
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductDisplay).val('');
    productId = 0;
}

MassDataChanges.prototype.ClearProductName = function () {    
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductDisplay).val('');
    productId = 0;
}

MassDataChanges.prototype.ClearOrderProduct = function () {
    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateProductSearch).val('');
    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateProductDisplay).val('');
    sapProductId = '';
}

MassDataChanges.prototype.ClearOrderProductName = function () { 
    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateProductDisplay).val('');
    sapProductId = '';
}


MassDataChanges.prototype.FileUploadChange = function (e) {
    MassDataChangesObj.IsDirty = true;
    var files = e.files;
    if (files.length > 0) {
        $jqGrid("#btnMassUpdateUpload").removeClass("btn_disable");
        $jqGrid('#btnMassUpdateUpload').removeAttr("disabled");
        MassDataChangesObj.FileData.append("file", files[0]);
        MassDataChangesObj.FileData.append("Template", $jqGrid("#" + MassDataChangesObj.UploadType + " option:selected").val());
    }
    else {
        $jqGrid("#btnMassUpdateUpload").addClass("btn_disable");
        $jqGrid('#btnMassUpdateUpload').attr('disabled', 'disabled');
    }
}

MassDataChanges.prototype.ClearUploadField = function (e) {
        $jqGrid("#ddlUploadType").val("");
        CommonScriptObj.EnableDisableFileUpload(MassDataChangesObj.UploadType, MassDataChangesObj.MassUpdateUploadExcel, MassDataChangesObj.btnMassUpdateUpload);
    }

// Import data function call
MassDataChanges.prototype.ImportData = function () {
    var type = $jqGrid('#' + MassDataChangesObj.UploadType).val();    
    
    if ($("#MassUpdateUploadExcel").val() == "") {
        $jqGrid("#btnUpload").addClass("btn_disable");
        $jqGrid('#btnUpload').attr('disabled', 'disabled');
        CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", $("#" + MassDataChangesObj.FileNotSelected).val(), 'pop_error_ul');
        return false;
    }
    $jqGrid("#data_loader").show();

    if (MassDataChangesObj.ValidateFile()) {
        /// Check for file format
        /// file format can replaced with constant value. cusrrently facing some problem after taking from constants.
        if ((/\.(gif|pdf|jpg|txt|sql|exe|dll|jpeg|tiff|png)$/i).test($("#MassUpdateUploadExcel").val())) {
            CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", $("#" + MassDataChangesObj.ExcelFormat).val(), 'pop_error_ul');            
            $jqGrid("#data_loader").hide();
            return false;
        }

        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        if (type == "20152" || type == "20159") { // this is temporary, gets remove later..
            //For Type - Prescription - Mass Data Changes call..
            fd.append("uploadedFile", $jqGrid("#MassUpdateUploadExcel")[0].files[0]);
            fd.append("selectedTemplate", type);
            fd.append("isImport", false);
            xhr.open("POST", "/MassDataChanges/UploadMassUpdateData", true);
            xhr.responseType = 'json';
            xhr.send(fd, false);
            xhr.addEventListener("load", function (data) {

                $jqGrid("#data_loader").hide();                
                var parsedjsonData;                
                try {
                    parsedjsonData = jQuery.parseJSON(data.target.response);
                } catch (e) {
                    parsedjsonData = data.target.response;
                }

                if (parsedjsonData.LogFilePath != null && typeof (parsedjsonData.LogFilePath) != 'undefined') {
                    $jqGrid("#" + MassDataChangesObj.HdnDownloadLogFile).val(parsedjsonData.LogFilePath);
                }
                
                if (typeof (parsedjsonData.Status) != 'undefined' && parsedjsonData.Status != false) {
                    CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", parsedjsonData.Message, 'msgSuccess');
                    MassDataChangesObj.ClearUploadField();
                    MassDataChangesObj.EnableDownloadButton();
                    MassDataChangesObj.FileData = new FormData();
                }
                else {
                    ShowTemplateError(parsedjsonData);
                }
            }, false);
        }
        else {
            // For rest of the call - Mass Data Changes call..
            fd.append("uploadedFile", $jqGrid("#MassUpdateUploadExcel")[0].files[0]);
            fd.append("selectedTemplate", type);
            fd.append("isImport", false);
            xhr.open("POST", "/MassDataImport/ImportData", true);
            xhr.responseType = 'json';
            xhr.send(fd);
            xhr.addEventListener("load", function (data) {
                $jqGrid("#data_loader").hide();                
                var parsedjsonData;
                try {
                    parsedjsonData = jQuery.parseJSON(data.target.response);
                } catch (e) {
                    parsedjsonData = data.target.response;
                }

                if (parsedjsonData.LogFilePath != null && typeof (parsedjsonData.LogFilePath) != 'undefined') {
                    $jqGrid("#" + MassDataChangesObj.HdnDownloadLogFile).val(parsedjsonData.LogFilePath);
                }

                if (typeof (parsedjsonData.Status) != 'undefined' && parsedjsonData.Status != false) {
                    CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", parsedjsonData.Message, 'msgSuccess');
                    MassDataChangesObj.ClearUploadField();
                    MassDataChangesObj.EnableDownloadButton();
                    MassDataChangesObj.FileData = new FormData();
                }
                else {
                    ShowTemplateError(parsedjsonData);
                }
            }, false);
        }
    }
    else
    {
        $jqGrid("#data_loader").hide();
        return false;
    }
}

function ShowTemplateError(parsedjsonData)
{    
    $jqGrid("#jqInvalidTemplateColumn").jqGrid('clearGridData');
    MassDataChangesObj.DiableDownloadButton();
    $("#divMasssChangeErrorMessage").show();
    var length = parsedjsonData.validationRows.length;
    var Message = "";
    if (parsedjsonData.validationRows.length > 0) {
        Message = parsedjsonData.validationRows[0].message;
        $("#validationText").html(Message);
        if (Message == "" || Message == null) {
             $jqGrid("#validationErrorjqWrapper").show();
            $jqGrid("#jqInvalidTemplateColumn").jqGrid('setGridParam', { datatype: 'local', data: parsedjsonData.validationRows }).trigger('reloadGrid');
        }
        else {
            $jqGrid("#validationErrorjqWrapper").hide();
        }
    }
}

function closeValidationPopup() {
    $jqGrid("#divMasssChangeErrorMessage").hide()
}
MassDataChanges.prototype.ValidateFile = function () {
    var maxSizeAllowed = $jqGrid('#' + MassDataChangesObj.AppConfigMaxSixe).val() * 1024 * 1024;
    var fileSize = $("#MassUpdateUploadExcel")[0].files[0].size;
    if (fileSize > maxSizeAllowed) {
        var maxSizeMessage = $jqGrid("#hdnUpdateFilezSize").val().replace('{0}', $jqGrid('#' + MassDataChangesObj.AppConfigMaxSixe).val());
        CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, maxSizeMessage, 'pop_error_ul');
        return false;
    }
    return true;
}

MassDataChanges.prototype.InitializeOrderUpdateDropdowns = function () {

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientStatus).multiselect({
        noneSelectedText: $jqGrid("#" + MassDataChangesObj.PatientStatusDefaultText).val(),
        click: function (event, ui) {
        },
        beforeopen: function () {
            if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
                MassDataChangesObj.ShowCustomerValidation();
                return false;
            }
        },
        open: function () {
            MassDataChangesObj.ClearOrderCareHome();
        },
    });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientType).multiselect({
        noneSelectedText: $jqGrid("#" + MassDataChangesObj.PatientTypeDefaultText).val(),
        click: function (event, ui) {
        },
        beforeopen: function () {
            if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
                MassDataChangesObj.ShowCustomerValidation();
                return false;
            }
        },
        open: function () {
            MassDataChangesObj.ClearOrderCareHome();
        },
    });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeStatus).multiselect({
        noneSelectedText: $jqGrid("#" + MassDataChangesObj.CareHomeStatusDefaultText).val(),
        click: function (event, ui) {
        },
        beforeopen: function () {
            if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
                MassDataChangesObj.ShowCustomerValidation();
                return false;
            }
        },
        open: function () {
            MassDataChangesObj.ClearOrderPatient();
        },
    });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeOrderType).multiselect({
        noneSelectedText: $jqGrid("#" + MassDataChangesObj.CarehomeOrderTypeDefaultText).val(),
        click: function (event, ui) {
        },
        beforeopen: function () {
            if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
                MassDataChangesObj.ShowCustomerValidation();
                return false;
            }
        },
        open: function () {
            MassDataChangesObj.ClearOrderPatient();
        },
    });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderStatus).multiselect({
        noneSelectedText: $jqGrid("#" + MassDataChangesObj.OrderStatusDefaultText).val(),
        click: function (event, ui) {
        },
        beforeopen: function () {
            if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
                MassDataChangesObj.ShowCustomerValidation();
                return false;
            }
        },
        open: function () {
        },
    });

    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderType).multiselect({
        noneSelectedText: $jqGrid("#" + MassDataChangesObj.OrderTypeDefaultText).val(),
        click: function (event, ui) {
        },
        beforeopen: function () {
            if (customerId == 0 || $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val() == '') {
                MassDataChangesObj.ShowCustomerValidation();
                return false;
            }
        },
        open: function () {
        },
    });

}


MassDataChanges.prototype.ClearOrderPatient = function () {
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientStatus).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientType).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.txtOrderUpdatePatientIds).val('');
}

MassDataChanges.prototype.ClearOrderCareHome = function () {
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeStatus).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeOrderType).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.txtOrderUpdateCareHomeIds).val('');

}

MassDataChanges.prototype.ClearOrderDetails = function () {
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderStatus).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderType).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.txtOrderUpdateOrderIds).val('');
}

MassDataChanges.prototype.ShowCustomerValidation = function () {
    CommonScriptObj.DisplayErrorMessage(MassDataChangesObj.ErrorMessage, $jqGrid("#" + MassDataChangesObj.CustomerValidation).val(), 'pop_error_ul');
    return false;
}


MassDataChanges.prototype.OrderDownloadFor = function (patientTypesForOrderUpdate
                                                                    , CareHomeOrderTypesForOrderUpdate
                                                                    , PatientStatusForOrderUpdate
                                                                    , CareHomeStatusForOrderUpdate
                                                                    ) {

    var patientIds = $jqGrid('#' + MassDataChangesObj.txtOrderUpdatePatientIds).val();
    var careHomeIds = $jqGrid('#' + MassDataChangesObj.txtOrderUpdateCareHomeIds).val();

    var noPatientcriteriaSelected = (patientTypesForOrderUpdate == '' && PatientStatusForOrderUpdate == '' && patientIds == '') ? true : false;
    var noCareHomeCriteriaSelected = (CareHomeOrderTypesForOrderUpdate == '' && CareHomeStatusForOrderUpdate == '' && careHomeIds == '') ? true : false;

    if (noPatientcriteriaSelected && noCareHomeCriteriaSelected) {
        //Customer
        return 0;
    }

    else if (noCareHomeCriteriaSelected) {
        //Patient
        return 1;
    }
    else {
        //CareHome
        return 2;
    }

}




MassDataChanges.prototype.DiableDownloadButton = function ()
{
    $jqGrid("#" + MassDataChangesObj.BtnMassChangesDownloadLog).addClass("btn_disable");
    $jqGrid("#" + MassDataChangesObj.BtnMassChangesDownloadLog).attr('disabled', 'disabled');
}

MassDataChanges.prototype.EnableDownloadButton = function ()
{
    $jqGrid("#" + MassDataChangesObj.BtnMassChangesDownloadLog).removeClass("btn_disable");
    $jqGrid("#" + MassDataChangesObj.BtnMassChangesDownloadLog).removeAttr('disabled');
}

MassDataChanges.prototype.ClearAllElementValues = function () {
    //Common for all
    customerId = 0;
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerSearch).val('');
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateCustomerDisplay).val('');

    //Patient
    $jqGrid("#" + MassDataChangesObj.PatientTypeDropdown).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.PatientStatusDropdown).multiselect("uncheckAll");
    $jqGrid('#' + MassDataChangesObj.PatientIds).val('');

    //Carehome
    $jqGrid('#' + MassDataChangesObj.CarehomeOrderDropdown).get(0).selectedIndex = 0;
    $jqGrid("#" + MassDataChangesObj.CareHomeStatusDropdown).multiselect("uncheckAll");
    $jqGrid('#' + MassDataChangesObj.CarehomeIds).val('');

    //Prescription
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductSearch).val('');
    $jqGrid('#' + MassDataChangesObj.txtPrescUpdateProductDisplay).val('');

    //Contact Person Details
    $jqGrid('#' + MassDataChangesObj.ContactPersonType).get(0).selectedIndex = 0;
    $jqGrid('#' + MassDataChangesObj.ContactPatientCarehomeId).val('');

    //Communication Format
    $jqGrid('#' + MassDataChangesObj.CommunicationType).get(0).selectedIndex = 0;
    $jqGrid('#' + MassDataChangesObj.CommunicationPatientCarehomeId).val('');

    //Customer-Post Code Matrix
    $jqGrid("#" + MassDataChangesObj.CustomerDropdown).multiselect("uncheckAll");

    //Patient-Clinical Contacts
    $jqGrid('#' + MassDataChangesObj.ClinicalPatientIds).val('');

    //Order
    $jqGrid('#' + MassDataChangesObj.txtOrderUpdatePatientIds).val('');
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientStatus).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdatePatientType).multiselect("uncheckAll");

    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateCareHomeIds).val('');
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeStatus).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateCareHomeOrderType).multiselect("uncheckAll");

    $jqGrid('#' + MassDataChangesObj.txtOrderUpdateOrderIds).val('');
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderStatus).multiselect("uncheckAll");
    $jqGrid("#" + MassDataChangesObj.ddlOrderUpdateOrderType).multiselect("uncheckAll");
}