﻿$jqGrid = jQuery.noConflict();

// Object declaration
var PatientTypeMatrixObj = new PatientTypeMatrix();
PatientTypeMatrix.prototype.PatientTypeMatrixGrid = "jqPatientTypeMatrix";
PatientTypeMatrix.prototype.PatientTypeMatrixDialog = "divPatientTypeMatrix";
PatientTypeMatrix.prototype.CustomerController = "hdnCustomerController";
PatientTypeMatrix.prototype.PatientTypeMatrixActionMethod = "hdnActionMethodGetPatientTypeMatrix";
PatientTypeMatrix.prototype.PatientTypeDropDown = "ddlPatientType";
PatientTypeMatrix.prototype.ErrorMessage = "divPatientMatrixErrorMessage";
PatientTypeMatrix.prototype.ErrorNoPatientMatrisFound = "hdnerrorNoPatientMatrisFound";
PatientTypeMatrix.prototype.ErrorPatientTypeNotSelected = "hdnerrorInvalidPatientType";
PatientTypeMatrix.prototype.DefaultPatientType = "hdnerrorDefaultPatientType";
PatientTypeMatrix.prototype.PatientAlreadyExist = "hdnerrorRecordAlreadyAvailable";
PatientTypeMatrix.prototype.TrackDirty = "divTrackDirty";
PatientTypeMatrix.prototype.GeneralNoModificationMessage = "hdnerrorGeneralNoModification";
PatientTypeMatrix.prototype.SavePatientTypeMatrixUrl = "hdnActionSavePatientTypeMatrix";
PatientTypeMatrix.prototype.isTrackDirty = false;
PatientTypeMatrix.prototype.SaveSuccessMessage = "hdnerrorPatientTypeMatrixSaveMessage";
PatientTypeMatrix.prototype.SaveErrorMessage = "hdnerrorPatientTypeMatrixErrorMessage";
PatientTypeMatrix.prototype.RemoveErrorMessage = "hdnerrorPatientTypeMatrixRemove";
PatientTypeMatrix.prototype.NumericMessage = "hdnerrorNumericMessage";
PatientTypeMatrix.prototype.PatientTypeMatrixSubmitButton = "btnPatientTypeMatrixSubmit";
PatientTypeMatrix.prototype.ExportAction = "hdnActionMethodExportPatientTypeMatrix";

//Column names
PatientTypeMatrix.prototype.PatientType = "PatientType";
PatientTypeMatrix.prototype.PrescMinFrequency = "PrescMinFrequency";
PatientTypeMatrix.prototype.PrescMaxFrequency = "PrescMaxFrequency";
PatientTypeMatrix.prototype.IncrementFrequency = "IncrementFrequency";
PatientTypeMatrix.prototype.AdultBillTo = "AdultBillTo";
PatientTypeMatrix.prototype.PaedBillTo = "PaedBillTo";
PatientTypeMatrix.prototype.AdvanceOrderActivationDays = "AdvanceOrderActivationDays";
PatientTypeMatrix.prototype.ChangeOrderNddDays = "ChangeOrderNddDays";
PatientTypeMatrix.prototype.IsAddFromPrescriptionAllowed = "IsAddFromPrescriptionAllowed";
PatientTypeMatrix.prototype.IsAddFromProductAllowed = "IsAddFromProductAllowed";
PatientTypeMatrix.prototype.IsOneOffAddFromPrescriptionAllowed = "IsOneOffAddFromPrescriptionAllowed";
PatientTypeMatrix.prototype.IsOneOffAddFromProductAllowed = "IsOneOffAddFromProductAllowed";
PatientTypeMatrix.prototype.IsPrescriptionApprovalRequired = "IsPrescriptionApprovalRequired";
PatientTypeMatrix.prototype.Remove = "Remove";
PatientTypeMatrix.prototype.PatientTypeMatrixID = "PatientTypeMatrixID";
PatientTypeMatrix.prototype.PatientTypeId = "PatientTypeId";
PatientTypeMatrix.prototype.IncrementFrequencyValidation = "hdnPatientTypeMatrixIncFreqValidation";
PatientTypeMatrix.prototype.MinMaxValidation = "hdnMinMaxValidation";
PatientTypeMatrix.prototype.PatientTypeMatrixZeroError = "hdnPatientTypeMatrixZeroError";
PatientTypeMatrix.prototype.IsChecked = true;
PatientTypeMatrix.prototype.RemovePatientTypeMatrixError = "hdnRemovePatientTypeMatrixError";
PatientTypeMatrix.prototype.ExportToExcelButton = "btnPatientTypeExportToExcel";
PatientTypeMatrix.prototype.ExportName = "_PatientTypeMatrix";
PatientTypeMatrix.prototype.DefaultFrequency = "DefaultFrequency";
PatientTypeMatrix.prototype.DefaultFrequencyValidation = "hdnPatientTypeMatrixDefaultFreqValidation";
PatientTypeMatrix.prototype.DefaultMinValidation = "hdnDefaultMinValidation";

// Form declaration
function PatientTypeMatrix() { }

// Export to excel
$jqGrid("#" + PatientTypeMatrixObj.ExportToExcelButton).click(function () {    
    var url = '/' + $("#" +PatientTypeMatrixObj.CustomerController).val() + '/' + $("#" +PatientTypeMatrixObj.ExportAction ).val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();

    $jqGrid.ajax({
        url: url,
        type: 'GET',
        cache: false,
        async: false,
        contentType: 'application/json',
        dataType: "json",
        data: {
            customerId: customerId
        },        
        success: function (data) {            
            window.open(data, PatientTypeMatrixObj.ExportName);
        }
    });
    return false;

});

// this will track if any change.
PatientTypeMatrix.prototype.IsTextboxValueChanged = function (e, colName) {
    PatientTypeMatrixObj.isTrackDirty = true;

    var row = $jqGrid(e.target).closest('tr.jqgrow');
    var rowId = row.attr('id');

    PatientTypeMatrixObj.IsChecked = PatientTypeMatrixObj.CheckTextboxValidation(rowId, colName, false);

    return false;
}

// Check Textbox validation
PatientTypeMatrix.prototype.CheckTextboxValidation = function (rowId, colName, isSaveCalled) {    
    // Check for isNumeric
    var checkVal = parseInt($.trim($jqGrid("#" + rowId + "_" + colName).val()));
    if (isNaN(checkVal)) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.NumericMessage).val(), 'pop_error_ul');
        $jqGrid("#" + rowId + "_" + colName).val(PatientTypeMatrixObj.GetOriginalValue(colName));
        return false;
    }
    
    var incrementFreq = $jqGrid("#" + rowId + "_" + PatientTypeMatrixObj.IncrementFrequency).val();
    var maxFrequency = $jqGrid("#" + rowId + "_" + PatientTypeMatrixObj.PrescMaxFrequency).val();
    var minFrequency = $jqGrid("#" + rowId + "_" + PatientTypeMatrixObj.PrescMinFrequency).val();
    var defaultFreq = $jqGrid("#" + rowId + "_" + PatientTypeMatrixObj.DefaultFrequency).val();

    // Increment frequency cannot greater than Max frequency
    if (colName == PatientTypeMatrixObj.IncrementFrequency || colName == PatientTypeMatrixObj.PrescMaxFrequency) {
        if (parseInt($.trim(incrementFreq)) > parseInt($.trim(maxFrequency))) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.IncrementFrequencyValidation).val(), 'pop_error_ul');
            $jqGrid("#" + rowId + "_" + colName).val(PatientTypeMatrixObj.GetOriginalValue(colName));
            return false;
        }        
    }
    // Default frequency cannot greater than Max frequency
    if (colName == PatientTypeMatrixObj.DefaultFrequency) {
        if (parseInt($.trim(defaultFreq)) > parseInt($.trim(maxFrequency))) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.DefaultFrequencyValidation).val(), 'pop_error_ul');
            $jqGrid("#" + rowId + "_" + colName).val(PatientTypeMatrixObj.GetOriginalValue(colName));
            return false;
        }
    }

    // Min frequency cannot greater than Max frequency
    if (colName == PatientTypeMatrixObj.PrescMinFrequency || colName == PatientTypeMatrixObj.PrescMaxFrequency) {
        if (parseInt($.trim(minFrequency)) > parseInt($.trim(maxFrequency))) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.MinMaxValidation).val(), 'pop_error_ul');
            $jqGrid("#" + rowId + "_" + colName).val(PatientTypeMatrixObj.GetOriginalValue(colName));
            return false;
        }        
    }
    // Default frequency cannot less than Mix frequency
    if (colName == PatientTypeMatrixObj.DefaultFrequency) {
        if (parseInt($.trim(minFrequency)) > parseInt($.trim(defaultFreq))) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.DefaultMinValidation).val(), 'pop_error_ul');
            $jqGrid("#" + rowId + "_" + colName).val(PatientTypeMatrixObj.GetOriginalValue(colName));
            return false;
        }
    }
    // Value cannot be zero
    if (isSaveCalled) {
            var isValidPatientType = $jqGrid("#" +rowId + "_" + PatientTypeMatrixObj.PatientType).val();
            if (isValidPatientType == undefined) {
                isValidPatientType = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getCell', rowId, PatientTypeMatrixObj.PatientType);
                }

        if (($jqGrid("#" +rowId + "_" + colName).val() == 0 && colName != PatientTypeMatrixObj.AdvanceOrderActivationDays) ||
            ($jqGrid("#" +rowId + "_" + colName).val() == 0 && colName == PatientTypeMatrixObj.AdvanceOrderActivationDays
                        && isValidPatientType.toLowerCase().indexOf("selfcare") >= 0)) {

            var message = $jqGrid("#" +PatientTypeMatrixObj.PatientTypeMatrixZeroError).val().replace("{0}", isValidPatientType);
            var validationMsg = PatientTypeMatrixObj.GetColumnCaption(colName) + message;

            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, validationMsg, 'pop_error_ul');
            $jqGrid("#" +rowId + "_" + colName).val(PatientTypeMatrixObj.GetOriginalValue(colName));
            return false;
        }
    }

    return true;
}

// Return original value
PatientTypeMatrix.prototype.GetColumnCaption = function (colName) {
    var retVal;
    var columnNames = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getGridParam', 'colNames');

    if (colName == PatientTypeMatrixObj.PrescMinFrequency) {
        retVal = columnNames[5];
    }
    else if (colName == PatientTypeMatrixObj.PrescMaxFrequency) {
        retVal = columnNames[4];
    }
    else if (colName == PatientTypeMatrixObj.IncrementFrequency) {
        retVal = columnNames[6];
    }
    else if (colName == PatientTypeMatrixObj.DefaultFrequency) {
        retVal = columnNames[7];
    }
    else if (colName == PatientTypeMatrixObj.AdultBillTo) {
        retVal = columnNames[8];
    }
    else if (colName == PatientTypeMatrixObj.PaedBillTo) {
        retVal = columnNames[9];
    }
    else if (colName == PatientTypeMatrixObj.AdvanceOrderActivationDays) {
        retVal = columnNames[10];
    }
    else if (colName == PatientTypeMatrixObj.ChangeOrderNddDays) {
        retVal = columnNames[11];
    }
    return retVal;
}

// Return original value
PatientTypeMatrix.prototype.GetOriginalValue = function (colName) {    
    var retVal = 0;
    if (colName == PatientTypeMatrixObj.PrescMinFrequency) {
        retVal = $jqGrid("#hdnPrescMinFrequency").val();
    }
    else if (colName == PatientTypeMatrixObj.PrescMaxFrequency) {
        retVal = $jqGrid("#hdnPrescMaxFrequency").val();
    }
    else if (colName == PatientTypeMatrixObj.IncrementFrequency) {
        retVal = $jqGrid("#hdnIncrementFrequency").val();
    }
    else if (colName == PatientTypeMatrixObj.DefaultFrequency) {
        retVal = $jqGrid("#hdnDefaultFrequency").val();
    }
    else if (colName == PatientTypeMatrixObj.AdultBillTo) {
        retVal = $jqGrid("#hdnAdultBillTo").val();
    }
    else if (colName == PatientTypeMatrixObj.PaedBillTo) {
        retVal = $jqGrid("#hdnPaedBillTo").val();
    }
    else if (colName == PatientTypeMatrixObj.AdvanceOrderActivationDays) {
        retVal = $jqGrid("#hdnAdvanceOrderActivationDays").val();
    }
    else if (colName == PatientTypeMatrixObj.ChangeOrderNddDays) {
        retVal = $jqGrid("#hdnChangeOrderNddDays").val();
    }

    if (retVal == '' || retVal == undefined || retVal == 'undefined') {
        retVal = 0;
    }

    return retVal;
}

// Check common validation
PatientTypeMatrix.prototype.CommonValidation = function () {
    // Check if patient type is selected or not.
    if ($jqGrid("#" + PatientTypeMatrixObj.PatientTypeDropDown + " option:selected").text() == $jqGrid("#" + PatientTypeMatrixObj.DefaultPatientType).val()) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.ErrorPatientTypeNotSelected).val(), 'pop_error_ul');
        return true;
    }
    return false;
}

// Add Rows rows
PatientTypeMatrix.prototype.AddTypesInGrid = function (obj) {
    // Check common validation before procceding.
    var retVal = PatientTypeMatrixObj.CommonValidation();
    if (retVal) {
        return false;
    }

    var gridRows = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getRowData');
    var ids = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getDataIDs');

    for (var i = 0 ; i < gridRows.length; i++) {
        var rowId = ids[i];

        // get patient type
        var isValidPatientType = $jqGrid("#" + rowId + "_" + PatientTypeMatrixObj.PatientTypeId).val();
        if (isValidPatientType == undefined) {
            isValidPatientType = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getCell', rowId, PatientTypeMatrixObj.PatientTypeId);
        }

        if ($jqGrid("#" + PatientTypeMatrixObj.PatientTypeDropDown + " option:selected").val() == isValidPatientType) {
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.PatientAlreadyExist).val(), 'pop_error_ul');
            return false;
        }
    }

    var datarow = {
        PatientTypeId: $jqGrid("#" + PatientTypeMatrixObj.PatientTypeDropDown + " option:selected").val(),
        PatientType: $jqGrid("#" + PatientTypeMatrixObj.PatientTypeDropDown + " option:selected").text(),
        PrescMinFrequency: 0, PrescMaxFrequency: 0, IncrementFrequency: 0, DefaultFrequency: 0,
        AdultBillTo: 0, PaedBillTo: 0, AdvanceOrderActivationDays: 0, ChangeOrderNddDays: 0,
        IsAddFromPrescriptionAllowed: 'false', IsAddFromProductAllowed: 'false', IsOneOffAddFromPrescriptionAllowed: 'false',
        IsOneOffAddFromProductAllowed: 'false', IsPrescriptionApprovalRequired: 'false',
        RowStatus: "New", Status: "Active", IsRemoved: false, IsOverriddenFlag: false
    };
    PatientTypeMatrixObj.isTrackDirty = true;
    $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('addRowData', gridRows.length + 1, datarow, "last");
}

// Undo Patient Type Matrix data
PatientTypeMatrix.prototype.UndoPatientTypeMatrix = function () {
    $jqGrid("#" + PatientTypeMatrixObj.PatientTypeDropDown).val($("#" + PatientTypeMatrixObj.PatientTypeDropDown + " option").eq(0).val());
    PatientTypeMatrixObj.ShowPatientTypeMatrix();
    return false;
}

// Save Patient Type Matrix data
PatientTypeMatrix.prototype.SavePatientTypeMatrix = function () {
    $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).attr('disabled', 'disabled');
    setTimeout(PatientTypeMatrixObj.SubmitPatientTypeMatrix(), 5000);
}

// Submit Patient Type Matrix data
PatientTypeMatrix.prototype.SubmitPatientTypeMatrix = function () {

    // Check if validations are already checked or not.
    if (!PatientTypeMatrixObj.IsChecked) {
        $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
        return false;
    }

    // Get all values from the grid.
    var objMatrixList = [];
    var gridRows = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getRowData');
    var ids = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getDataIDs');
    
    

    // Check validations before saving
    for (var i = 0 ; i < gridRows.length; i++) {
        var rowId = ids[i];                
        var retMaxVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.PrescMaxFrequency, true);
        if (!retMaxVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retMinVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.PrescMinFrequency, true);
        if (!retMinVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retIncVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.IncrementFrequency, true);
        if (!retIncVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retIncVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.DefaultFrequency, true);
        if (!retIncVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retAdultVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.AdultBillTo, true);
        if (!retAdultVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retPaidVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.PaedBillTo, true);
        if (!retPaidVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retOrderVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.AdvanceOrderActivationDays, true);
        if (!retOrderVal) {
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }

        var retChangeVal = PatientTypeMatrixObj.CheckTextboxValidation(rowId, PatientTypeMatrixObj.ChangeOrderNddDays, true);
        if (!retChangeVal) {
            $("#" +PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            return false;
        }
    }

    //// Check any record gets updated or not.    
    if (!PatientTypeMatrixObj.isTrackDirty) {
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.GeneralNoModificationMessage).val(), 'pop_error_ul');
        $("#" +PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
        return false;
    }
    
    for (var i = 0 ; i < gridRows.length; i++) {
        var rowId = ids[i];

        // Get Patient Type
        var patientType = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.PatientType);

        // Get Patient Type Id
        var patientTypeId = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.PatientTypeId);

        // Get Min Freq
        var minFreq = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.PrescMinFrequency);

        // Get Max Frq
        var maxFreq = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.PrescMaxFrequency);

        // Get Increment Freq
        var incrementFreq = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.IncrementFrequency);

        // Get Default Freq
        var defaultFreq = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.DefaultFrequency);

        // Get Adult Bill To
        var adultBillTo = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.AdultBillTo);

        // Get Paid Bill To
        var paidBillTo = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.PaedBillTo);

        // Get Advance Order Activation Days
        var activationDay = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.AdvanceOrderActivationDays);

        // Get Change Order Ndd Days
        var changeNddDays = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.ChangeOrderNddDays);

        // Get IsAddFromPrescriptionAllowed
        var isPrescriptionAllowed = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.IsAddFromPrescriptionAllowed);

        // Get IsAddFromProductAllowed
        var isProductAllowed = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.IsAddFromProductAllowed);

        // Get IsOneOffAddFromPrescriptionAllowed
        var isOneOffPrescriptionAllowed = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.IsOneOffAddFromPrescriptionAllowed);

        // Get IsOneOffAddFromProductAllowed
        var isOneOffProductAllowed = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.IsOneOffAddFromProductAllowed);

        // Get IsPrescriptionApprovalRequired
        var isApprovalRequired = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.IsPrescriptionApprovalRequired);

        // Get IsRemove
        var remove = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.Remove);

        // Get Patient Type Matrix ID
        var patientTypeMatrixID = PatientTypeMatrixObj.GetValidGridValue(rowId, PatientTypeMatrixObj.PatientTypeMatrixID);

        // Build values in the List
        var objPatientType = {
            PatientTypeId: patientTypeId,
            PatientType: patientType,
            PrescMinFrequency: minFreq,
            PrescMaxFrequency: maxFreq,
            IncrementFrequency: incrementFreq,
            AdultBillTo: adultBillTo,
            PaedBillTo: paidBillTo,
            AdvanceOrderActivationDays: activationDay,
            ChangeOrderNddDays: changeNddDays,
            IsAddFromPrescriptionAllowed: isPrescriptionAllowed,
            IsAddFromProductAllowed: isProductAllowed,
            IsOneOffAddFromPrescriptionAllowed: isOneOffPrescriptionAllowed,
            IsOneOffAddFromProductAllowed: isOneOffProductAllowed,
            IsPrescriptionApprovalRequired: isApprovalRequired,
            IsRemove: remove,
            PatientTypeMatrixID: patientTypeMatrixID,
            DefaultFrequency: defaultFreq,
        }

        objMatrixList.push(objPatientType);
    }//for end  

    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();

    $.ajax({        
        url: '/' + $("#" + PatientTypeMatrixObj.CustomerController).val() + '/' + $("#" + PatientTypeMatrixObj.SavePatientTypeMatrixUrl).val(),
        type: 'POST',
        async: false,
        cache: false,
        dataType: 'json',
        data: {
            objPatientMatrixList: objMatrixList, objCustomerId: customerId
        },
        success: function (data) {
            $(".prepended").remove();
            $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
            $(".disable_pop").show();
            $(".ui-dialog-titlebar-close").addClass('btn_disable');
            if (data) {
                CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.SaveSuccessMessage).val(), 'msgSuccess');
                PatientTypeMatrixObj.isTrackDirty = false;
                PatientTypeMatrixObj.IsChecked = true;
                // this will retrive all updated records
                PatientTypeMatrixObj.UndoPatientTypeMatrix();
            }
            else {
                CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.SaveErrorMessage).val(), 'pop_error_ul');
            }
            return false;
        }
    });
     $("#" + PatientTypeMatrixObj.PatientTypeMatrixSubmitButton).removeAttr('disabled');
}// main if end

//get valid id from the grid
PatientTypeMatrix.prototype.GetValidGridValue = function (rowId, columnName) {
    var isValid = $jqGrid("#" + rowId + "_" + columnName).val();
    if (isValid == undefined) {
        isValid = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getCell', rowId, columnName);
    }
    return isValid;
}

// Show Patient Type Matrix
PatientTypeMatrix.prototype.ShowPatientTypeMatrix = function () {
    window.gridid = PatientTypeMatrixObj.PatientTypeMatrixGrid;
    PatientTypeMatrixObj.isTrackDirty = false;

    $jqGrid("#txtPatientTypeCustomerName").val($jqGrid("#txtCustomerName").val());
    $jqGrid("#txtPatientTypeSAPCustomerID").val($jqGrid("#txtSAPCustomerNumber").val());
    
    $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixDialog).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: '1165px',
        resizable: true,
        title: "Patient Type Matrix",
    });
    $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixDialog).dialog('open');
    $jqGrid(".disable_div").remove();
    $jqGrid.ajax({
        url: '/' + $("#" + PatientTypeMatrixObj.CustomerController).val() + '/' + $("#" + PatientTypeMatrixObj.PatientTypeMatrixActionMethod).val(),        
        type: 'GET',
        cache: false,
        data: {
            customerId: $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val()
        },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {
            $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('clearGridData');
            if (responseData.Count > 0) {
                $jqGrid("#" + PatientTypeMatrixObj.PatientTypeDropDown).val($("#" + PatientTypeMatrixObj.PatientTypeDropDown + " option").eq(0).val());
                $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('setGridParam', {
                    datatype: 'local', data: responseData.PatientTypeMatrixList
                }).trigger('reloadGrid');

                // Register click event for checkbox..
                $jqGrid(".isSelected").click(function (id) {
                    PatientTypeMatrixObj.isTrackDirty = true;
                });
            }
            else {
                $(".disable_pop").show();
                $(".ui-dialog-titlebar-close").addClass('btn_disable');
                CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.ErrorNoPatientMatrisFound).val(), 'pop_error_ul');
            }
        }
    });
    return false;
}

// Remove rows
PatientTypeMatrix.prototype.Removerows = function (cellvalue, options, rowobject) {
    return "<input type='checkbox' class='chkRemoveClass' id=" + options['rowId'] + "_" + options.colModel["name"] + " onchange=PatientTypeMatrixObj.RemoveRecordOnSelectAll(this)>";
}

// Select or deselect all checkbox
PatientTypeMatrix.prototype.RemoveRecordOnSelectAll = function (checkbox) {
    PatientTypeMatrixObj.isTrackDirty = true;
    var gridRows = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getRowData');
    var ids = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getDataIDs');
    var rowId = (checkbox.id).split("_")[0];
    
    var isValidPatientTypeId = $jqGrid("#" + rowId + "_PatientTypeId").val();
    if (isValidPatientTypeId == undefined) {
        isValidPatientTypeId = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getCell', rowId, "PatientTypeId");
    }

    var isValidPatientType = $jqGrid("#" + rowId + "_" + PatientTypeMatrixObj.PatientType).val();
    if (isValidPatientType == undefined) {
        isValidPatientType = $jqGrid("#" + PatientTypeMatrixObj.PatientTypeMatrixGrid).jqGrid('getCell', rowId, PatientTypeMatrixObj.PatientType);
    }

    var count = PatientTypeMatrixObj.CheckIfPatientTypeAssociatedWithPatient(isValidPatientTypeId);
    if (count > 0) {
        var messageToDisplay = $jqGrid("#" + PatientTypeMatrixObj.RemovePatientTypeMatrixError).val().replace('{0}', isValidPatientType);
        $jqGrid(checkbox).prop("checked", false);
        //$jqGrid("#" + checkbox.id).val('false');
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, messageToDisplay, 'pop_error_ul');
        return false;
    }

    if (checkbox.checked) {
        $jqGrid("#" + checkbox.id).val('true');
        
        $(".disable_pop").show();
        $(".ui-dialog-titlebar-close").addClass('btn_disable');
        CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.RemoveErrorMessage).val(), 'pop_error_ul');        
    }
    else {
        $jqGrid("#" + checkbox.id).val('false');
    }    
    return false;
}

//Check If Patient Type Associated With Patient
PatientTypeMatrix.prototype.CheckIfPatientTypeAssociatedWithPatient = function (patientType) {    
    var url = '/' + $("#" + ClinicalContactMappingObj.CustomerController).val() + '/GetPatientTypeAssociatedWithPatient';
    var customerId = $jqGrid("#" +CustomerInformationObj.CustomerCurrentId).val();
    var count = 0;
    $jqGrid.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {
            customerId: customerId, patientType: patientType
        },
        async: false,
        cache: false,
        success: function (retData) {           
            count = retData.Count;
            // end outer else condition
        } // end succes function
    });

    return count;
}
