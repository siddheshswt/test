﻿$jqGrid = jQuery.noConflict();

var OrderObj = new Order();
var userTypeforOrder = "SCA";

var lstReasonCode = "";
var jsfilename = "Order.js";
// Form declaration
function Order() { }

//Carehome Order
Order.prototype.ResidentialCancel = "btnCancelResidentialOrder";
Order.prototype.ResidentialGridSelectAll = "chkSelectAll";
Order.prototype.ResidentialGridSelectClass = "chkSelectClass";
Order.prototype.ResidentialMessage = "residentialMessage";
Order.prototype.ResidentialSelectOrderToCancel = "hdnResidentialSelectOrderToCancel";
Order.prototype.ResidentialConfirmCancel = "divResidentialConfirmCancel";

Order.prototype.GeneralYes = "Yes";
Order.prototype.GeneralNo = "No";

var isNDDManuallyChanged = false;

Order.prototype.CancelResidentialOrder = function (e) {
    if($jqGrid("#hdnResidentialNoteMandatory").val().toUpperCase() == userTypeforOrder.toUpperCase() && $jqGrid.trim($jqGrid("#txtResidentialNote").val()) == "") {
        $jqGrid("#residentialMessage").html($jqGrid('#hdnResidentialreserrormsNote').val());
        $jqGrid('#residentialMessage').show();
        return false;
    }
    var numberOfChecked = $jqGrid("." + OrderObj.ResidentialGridSelectClass + ":checked").length;
    if (numberOfChecked <= 0) {
        $jqGrid("#" + OrderObj.ResidentialMessage).html($jqGrid("#" + OrderObj.ResidentialSelectOrderToCancel).val());
        $jqGrid("#" + OrderObj.ResidentialMessage).show();
        return false;
    }

    $jqGrid("#" + OrderObj.ResidentialConfirmCancel).dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        resizable: false,
        closeOnEscape: false,
        buttons: [{
            text: OrderObj.GeneralYes,
            "id": OrderObj.GeneralYes,
            click: function () {                
                $jqGrid(this).dialog("close");
                saveResidentialData(false, true);
            },
        },
        {
            text: OrderObj.GeneralNo,
            "id": OrderObj.GeneralNo,
            click: function () {                
                $jqGrid(this).dialog("close");
            },
        }
        ]
    });
    $jqGrid("#" + OrderObj.ResidentialConfirmCancel).dialog('open');
    return false;        
}



var day1 = day2 = day3 = day4 = day5 = 0;
var actionName = "";
var holidayDaysList = [];
var ajaxFromDate;
var ajaxToDate;
var PrescriptionGrdTitle = $jqGrid('#hdnPrescriptionGrdTitle') != null ? $jqGrid('#hdnPrescriptionGrdTitle').val() : "";
var eventFromGridId = '';
var patientLevelNDD = '';
var changeOrderNddDays = 0;

function setNewNDD(gridId, ctrlId) {
    setDatePicker(gridId, ctrlId);
    if (eventFromGridId == "jqPatientPrescription" && patientLevelNDD == undefined) {
        patientLevelNDD = '';
        return false;
    }
    if (ctrlId == null)
        $jqGrid('.forChangeNDD').show().focus().hide();
}

function setDatePicker(gridId, ctrlId) {
    eventFromGridId = gridId;
    var rowCount = $jqGrid('#' + gridId).jqGrid('getGridParam', 'records');
    var nddDetails = null;
    if (rowCount < 1)
        return false;
    if (!$jqGrid('#rdoRushOrder').is(":checked")) {
        if (gridId != "jqPatientPrescription") {
            $jqGrid.ajax({
                url: "/Order/GetNDDRelatedDetails",
                type: 'GET',
                cache: false,
                dataType: "json",
                contentType: 'application/json',
                async: false,
                success: function (responseData) {
                    if (responseData != 0) {
                        changeOrderNddDays = responseData.ChangeOrderNddDays;
                        day1 = responseData.Day1;
                        day2 = responseData.Day2;
                        day3 = responseData.Day3;
                        day4 = responseData.Day4;
                        day5 = responseData.Day5;
                        roundID = responseData.RoundID;
                    }
                }
            });//ajax
        }
        else if (gridId == "jqPatientPrescription") {
            var postCodeMatrixData;
            if ($('#hdnPostcodeMatrix').val() != null || $('#hdnPostcodeMatrix').val() != undefined)
            {
                postCodeMatrixData = $.parseJSON(decodeURIComponent($('#hdnPostcodeMatrix').val()));
            }
            else {
                postCodeMatrixData = null;
            }

            if (postCodeMatrixData != null) {
                day1 = postCodeMatrixData.Day1;
                day2 = postCodeMatrixData.Day2;
                day3 = postCodeMatrixData.Day3;
                day4 = postCodeMatrixData.Day4;
                day5 = postCodeMatrixData.Day5;
                roundID = postCodeMatrixData.RoundID;
            }
        }

        if (nddDetails != null) {
            changeOrderNddDays = nddDetails.ChangeOrderNddDays;
        }
        if (changeOrderNddDays == 0) {
            changeOrderNddDays = 28; //to avoid any exceptions
        }
    }
    else {
        day1 = day2 = day3 = day4 = day5 = 0;
        roundID = "";
        changeOrderNddDays == 0
    }
    if (gridId == "jqCommunityOrders") {
        ajaxFromDate = $jqGrid('#hdnNDDStartDate').val();        
        ajaxToDate = $jqGrid('#hdnNDDEndDate').val();        
        actionName = $('#hdnActionName').val();
    }
    if (gridId == "jqOneOffOrders") {
        ajaxFromDate = $jqGrid('#hdnOneOffNDDStartDate').val();
        ajaxToDate = $jqGrid('#hdnOneOffNDDEndDate').val();
        actionName = $('#hdnOneOffActionName').val();
    }

    else if (gridId == "jqResidentialActivated" || gridId == "jqProductOrders") {
        ajaxFromDate = $jqGrid('#hdnResidentialNDDStartDate').val();
        ajaxToDate = $jqGrid('#hdnResidentialNDDEndDate').val();
        actionName = $('#hdnResidentialActionName').val();
    }

    else if (gridId == "jqNextDeliveryAmendment") {
        ajaxFromDate = $jqGrid('#hdnResidentialNDDStartDate').val();
        ajaxToDate = $jqGrid('#hdnResidentialNDDEndDate').val();
        actionName = $('#hdnResidentialActionName').val();
    }

    else if (gridId == "jqPatientPrescription") {     
        var endDate = $('#DeliveryDate').text();        
        if (endDate == "" || endDate == undefined) {
            endDate = $("#hdnNextDeliverydate").val();
            if (endDate == "" || endDate == undefined) {
                patientLevelNDD = undefined;
                return false;
            }
        }
        $jqGrid.ajax({
                url: "/Order/GetDatesForPrescription",
                type: 'GET',
                cache: false,
                data: { endDate: endDate },
                dataType: "json",
                contentType: 'application/json',
                async: false,
                success: function (responseData) {
                    ajaxFromDate = responseData.StartDate;
                    ajaxToDate = responseData.EndDate;
        }
        });//ajax

        ajaxFromDate = ConvertDateFormat(ajaxFromDate);
        ajaxToDate = ConvertDateFormat(ajaxToDate);

        actionName = "/Order/GetHolidayList";
    }
    $jqGrid.ajax({
        url: actionName,
        type: 'GET',
        cache: false,        
        data: { fromDate: ajaxFromDate, toDate: ajaxToDate },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {            
            holidayDaysList = [];
            if (responseData.length > 0)
                for (var i = 0; i < responseData.length; i++) {
                    holidayDaysList.push(ConvertDateToMMDDYYYY(new Date(responseData[i].HolidayDateStringFormat)));
                }
        }
    });//end ajax call
    ShowDatePicker(gridId, ctrlId);
}//end function

function addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

function ShowDatePicker(gridId, ctrlId) {
    if (ctrlId == null) {
        $jqGrid(".forChangeNDD").datepicker("destroy");
        $jqGrid('.forChangeNDD').datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            minDate: ajaxFromDate,
            maxDate: ajaxToDate,
            beforeShowDay: function (dt) {
                if (holidayDaysList.length > 0) {
                    var hindex = $jqGrid.inArray(ConvertDateToMMDDYYYY(dt), holidayDaysList);
                    if (hindex > -1)
                        return [false, '', ''];
                    else
                        return [dt.getDay() == 0 || dt.getDay() == 6 || dt.getDay() == day1 || dt.getDay() == day2 || dt.getDay() == day3 || dt.getDay() == day4 || dt.getDay() == day5 ? false : true];
                }
                else
                    return [dt.getDay() == 0 || dt.getDay() == 6 || dt.getDay() == day1 || dt.getDay() == day2 || dt.getDay() == day3 || dt.getDay() == day4 || dt.getDay() == day5 ? false : true];
            },
            onSelect: function (value, date) {
                window.NextDeliveryDate = value;                
                PatientNDD = CommonScriptObj.DateTextToMMDDYYYY(value);
                if (gridId == "jqNextDeliveryAmendment") {
                    $jqGrid('#divNextDeliveryAmendmentConfirm').dialog({
                        autoOpen: false,
                        modal: true,
                        width: 'auto',
                        resizable: false,
                        closeOnEscape: false,
                        buttons: [{
                            text: $jqGrid("#hdnGeneralYes").val().toString(),
                            "id": "hdnGeneralYes",
                            click: function () {
                                DateValueToGrid(gridId, value);
                                $jqGrid(this).dialog("close");
                            },
                        }, {
                            text: $jqGrid("#hdnGeneralNo").val().toString(),
                            "id": "hdnGeneralNo",
                            click: function () {
                                $jqGrid(this).dialog("close");
                            },
                        }]
                    });
                    $jqGrid('#divNextDeliveryAmendmentConfirm').dialog('open');
                    return false;
                }
                else {
                    DateValueToGrid(gridId, value);
                }
            },
        });
        $jqGrid(".forChangeNDD").datepicker("refresh");
    }
}

function DateValueToGrid(gridId, value) {    
    var allRowsInGrid = $jqGrid('#' + gridId).jqGrid('getGridParam', 'records');
    if (gridId == "jqNextDeliveryAmendment") {
        var selRowId = $jqGrid('#jqNextDeliveryAmendment').jqGrid('getGridParam', 'selrow');
        if (selRowId == "" || selRowId == null)
            return false;
        var SelOrderId = $jqGrid('#jqNextDeliveryAmendment').jqGrid('getCell', selRowId, 'OrderId');
        for (var i = 1; i <= allRowsInGrid; i++) {
            var rowOrderId = $jqGrid('#jqNextDeliveryAmendment' + ' #' + i + ' [aria-describedby=jqNextDeliveryAmendment_OrderId]').text();
            if (SelOrderId == rowOrderId) {
                $jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_NextDeliveryDate]').html(value);
                //$jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_ChangeNDD]').html(value);
                $jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_ChangeNDD]').val(value);                
                $jqGrid('#DeliveryDate').text(value);
                $jqGrid('#' + gridId).jqGrid('showCol', 'NextDeliveryDate');
            }
        }
    }
    else {
        try {
            if (gridId == 'jqPatientPrescription')
                IsForceDisplayNDD = false;
                PatientHasChanged();
        }
        catch (err) { }
        for (var i = 1; i <= allRowsInGrid; i++) {
            var isRemoved = $jqGrid("#" + i + "_IsRemoved").is(":checked");
            var isChecked = $jqGrid("#" + i + "_Select").is(":checked");
            if (!isRemoved) {
                if (gridId == 'jqOneOffOrders') {
                    $jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_DeliveryDate]').html(value);
                }
                $jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_NextDeliveryDate]').html(value);
                //$jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_ChangeNDD]').html(value);
                $jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_ChangeNDD]').val(value);
                $jqGrid('#' + gridId + ' #' + i + '_ChangeNDD').val('').val(value);                
                $jqGrid('#DeliveryDate').text(value);
                $jqGrid('#' + gridId).jqGrid('showCol', 'NextDeliveryDate');
                if (gridId == "jqProductOrders") {
                    $jqGrid('#' + gridId + ' #' + i + ' [aria-describedby=' + gridId + '_DeliveryDate]').html(value);
                }
            }
        }
    }

    if (window.PreDefinedNDD != value)
        isNDDManuallyChanged = true;
    else
        isNDDManuallyChanged = false;
}//function

function AddDaysToDate(dateText, days) {
    var calToDate = new Date();
    var ddmmyyyy = dateText.split("/");
    calToDate.setDate(ddmmyyyy[0]);
    calToDate.setMonth(ddmmyyyy[1] - 1);
    calToDate.setFullYear(ddmmyyyy[2]);
    calToDate.setDate(calToDate.getDate() + parseInt(days));
    var dd = ("0" + calToDate.getDate()).slice(-2);
    var mm = ("0" + (calToDate.getMonth() + 1)).slice(-2);
    var yyyy = calToDate.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
}

function showPopupPrescription(id) {
    if (id == 'btnAddCommunityOrderPrescription') {
        window.gridid = "jqCommunityOrders";
        HideNurseCommentsDiv();
    }
    if (id == 'btnAddOneOffOrderPrescription') {
        window.gridid = "jqOneOffOrders";
        HideNurseCommentsDiv();
    }
    $jqGrid('#divPrescriptionList').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: PrescriptionGrdTitle
    });
    $jqGrid('#divPrescriptionList').dialog('open');
    $jqGrid(".disable_div").remove();
    $jqGrid(".analysis_information_addinfo").hide();
    $jqGrid("#show_removed").hide();
    $('label[for="show_removed"]').hide();
    if (window.gridid == "jqCommunityOrders" || window.gridid == "jqOneOffOrders") {
        var grid = $jqGrid("#jqPatientPrescription"), ids = grid.jqGrid('getDataIDs'), i, l = ids.length;
        for (i = 0; i < l; i++) {
            $jqGrid('#' + ids[i] + '_IsOverriddenFlag').attr('disabled', 'disabled');
            setPrescriptionColumnsTrue();
            $jqGrid('#jqPatientPrescription').jqGrid('restoreRow', ids[i]);
            setPrescriptionColumnsFalse();
        }
        $jqGrid('#jqPatientPrescription').jqGrid('hideCol', "IsRemoved");
        $jqGrid('#jqPatientPrescription').jqGrid('showCol', "SelectPrescriptionProduct");
    }
    $('.chkSelectPrescriptionProduct').removeAttr('checked');
}

function setPrescriptionColumnsTrue() {

    $jqGrid('#jqPatientPrescription').setColProp('AssessedPadsPerDay', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('DQ1', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('DQ2', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('DQ3', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('DQ4', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('ActualPPD', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('Frequency', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('ValidFromDate', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('ValidToDate', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('DeliveryDate', { editable: true });
    $jqGrid('#jqPatientPrescription').setColProp('ChangeNDD', { editable: true });
}

function setPrescriptionColumnsFalse() {

    $jqGrid('#jqPatientPrescription').setColProp('AssessedPadsPerDay', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('DQ1', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('DQ2', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('DQ3', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('DQ4', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('ActualPPD', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('Frequency', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('ValidFromDate', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('ValidToDate', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('DeliveryDate', { editable: false });
    $jqGrid('#jqPatientPrescription').setColProp('ChangeNDD', { editable: false });
}

function removeFromOneOffOrder(clearAll, isFirstTimeLoad) {
    $jqGrid('#MsgSelectProductsToRemoveOneOff').hide();
    var isRemoved = '';
    var rowIds = [];
    if (clearAll == true) {
        $jqGrid(".chkRemoveClass").each(function (rowId) {
            var Trid = $(this).closest('tr').prop('id');
            rowIds.push(Trid);
        });
    }
    else {
        $jqGrid(".chkRemoveClass").each(function (rowId) {
            if ($(this).is(':checked')) {
                var Trid = $(this).closest('tr').prop('id');
                rowIds.push(Trid);
            }

        });
    }

    var reveseIds = (rowIds.reverse());
    if (reveseIds.length == 0 && (!isFirstTimeLoad)) {
        $jqGrid('#MsgSelectProductsToRemoveOneOff').show();

    }
    else {
        $jqGrid('#MsgSelectProductsToRemoveOneOff').hide();
        $.each(reveseIds, function (index) {
            var rowId = reveseIds[index];
            $jqGrid('#jqOneOffOrders').jqGrid('delRowData', rowId);
        })
    }
}

$('#btnAcceptDisclaimer').click(function () {
    var isError = 0;
    var disclaimerPersonName = jQuery.trim($jqGrid('#txtDisclaimerPersonName').val());
    var disclaimerPersonPosition = jQuery.trim($jqGrid('#txtDisclaimerPersonPosition').val());
    if (disclaimerPersonName == '') {
        $jqGrid('#errortxtDisclaimerPersonName').html("Please Enter Name");
        $jqGrid('#errortxtDisclaimerPersonName').show();
        isError = 1;
    }
    if (disclaimerPersonPosition == '') {
        $jqGrid('#errortxtDisclaimerPersonPosition').html("Please Enter Position");
        $jqGrid('#errortxtDisclaimerPersonPosition').show();
        isError = 1;
    }
    if (isError == 1) {
        isError = 0;
        return false;
    }
    else {
        saveResidentialData(true, false);
        $jqGrid("#j_lightbox").hide();
    }
});//end function

$('#btnSaveResidentialOrder').click(function () {    
    $jqGrid('#btnSaveResidentialOrder').addClass('btn_disable');    
    if (isResidentialDataValid()){
        if ($('#hdnLoggedInUserRoleName').val() == $('#hdnExtCareHomeRoleName').val()) {
            $jqGrid('#btnSaveResidentialOrder').removeClass('btn_disable');
            $jqGrid('#txtDisclaimerPersonName').val('');
            $jqGrid('#txtDisclaimerPersonPosition').val('');
            $jqGrid('#errortxtDisclaimerPersonName').hide();
            $jqGrid('#errortxtDisclaimerPersonPosition').hide();
            $jqGrid("#j_lightbox").show();
        }
        else {
            $jqGrid("#data_loader").show();            
            saveResidentialData(false, false);
        }
    }
    else {
        $jqGrid('#btnSaveResidentialOrder').removeClass('btn_disable');
    }
});

function isResidentialDataValid() {    
    var totalRecordCount = $jqGrid("#jqResidentialActivated").jqGrid('getGridParam', 'records');
    if (totalRecordCount < 1) {
        $jqGrid("#residentialMessage").html($jqGrid('#hdnResidentialAddProduct').val());
        $jqGrid('#residentialMessage').show();
        return false;
    }

    if ($("#hdnResidentialNoteMandatory").val().toUpperCase() == userTypeforOrder.toUpperCase() && $.trim($("#txtResidentialNote").val()) == "") {
        $jqGrid("#residentialMessage").html($jqGrid('#hdnResidentialreserrormsNote').val());
        $jqGrid('#residentialMessage').show();
        return false;
    }

    var gridRows = $jqGrid("#jqResidentialActivated").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqResidentialActivated").jqGrid('getDataIDs'); //siddhesh
    var numberOfChecked = $('.chkSelectClass:checked').length;    
    numberOfChecked += $('.chkPatientStatusClass:checked').length;
    var isNewOrder = $jqGrid('#spnOrderSelfcareTitle').text() == $jqGrid('#hdnSelfcareHeaderTextNew').val() ? true : false; //siddhesh
    
    if (numberOfChecked <= 0) {
        $jqGrid("#residentialMessage").html($jqGrid('#hdnSelfcareOrderEnterSelectProduct').val());
        $jqGrid('#residentialMessage').show();
        return false;
    }
    
    var gridRows = $jqGrid("#jqResidentialActivated").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqResidentialActivated").jqGrid('getDataIDs');
    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = gridIds[i];

        if ($jqGrid("#" + rowId + "_IsPatientRemoved").is(':checked') && $jqGrid("#" + rowId + "_ReasonCode").val() == 0) {
            $jqGrid("#residentialMessage").html($jqGrid('#hdnresSelfcareOrderRemovedReasonCode').val());
            $jqGrid('#residentialMessage').show();
            return false;
        }
    }
    
    var firstRow = gridRows[0];
    var deliveryDate = firstRow["NextDeliveryDate"];
    if (deliveryDate == "") {
        deliveryDate = firstRow["DeliveryDate"];
    }
    var isHoliday = IsHoliday(deliveryDate);
    if (isHoliday)
    { 
        ShowErrorMessage("divResidentialNDDHoliday");            
        return false;
    }
    return true;
}

function IsHoliday(date) {    
    var isHoliday = true;
    $jqGrid.ajax({
        url: '/Order/IsHoliday',
        type: 'GET',
        cache: false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: { date:date },
        success: function (data) {
            isHoliday = data;
        },
        error: function (xhr) {
        }
    });

    return isHoliday;
}

function saveResidentialData(isDisclaimerRequired, isCancel) {    
    var orderNote = jQuery.trim($jqGrid('#txtResidentialNote').val());
    if (orderNote == $jqGrid('#hdnBulkNotePlaceholder').val()) {
        orderNote = "";
    }
    if (isCancel) {
        $("#spnOrderSaved").hide();
        $("#spnOrderCancelled").show();
    }
    else {
        $("#spnOrderCancelled").hide();
        $("#spnOrderSaved").show();
    }
    $jqGrid('#residentialMessage').hide();
    var ordersList = [];
    var gridRows = $jqGrid("#jqResidentialActivated").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqResidentialActivated").jqGrid('getDataIDs'); //siddhesh
    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = gridIds[i]; //siddhesh        
        if ($jqGrid("#" + (rowId) + "_Select").is(':checked') || $jqGrid("#" + rowId + "_IsPatientRemoved").is(':checked')) {
            var deliveryDate = row["NextDeliveryDate"];
            if (deliveryDate == "") {
                deliveryDate = row["DeliveryDate"];
            }            
            var quantity = $jqGrid("#" + (rowId) + "_Quantity").val();
            if (isCancel)
            {
                quantity = 0;
            }
            else if (quantity == undefined) {
                quantity = row["Quantity"];
                if (quantity == undefined) {                    
                    quantity = $jqGrid('#jqResidentialActivated').jqGrid('getCell', rowId, 'Quantity');
                }
            }
            var orderOject = {
                PatientId: row["PatientID"],
                ProductID: row["ProductId"],
                CareHomeID: row["CareHomeID"],
                OrderId: row["OrderId"],
                Quantity: $jqGrid("#" + rowId + "_IsPatientRemoved").is(':checked') ? 0 : quantity,
                DeliveryDate: deliveryDate,
                OrderNote: orderNote,
                OrderType: window.orderTypeResidential,
                PersonName: isDisclaimerRequired == true ? jQuery.trim($jqGrid('#txtDisclaimerPersonName').val()) : '',
                PersonPosition: isDisclaimerRequired == true ? jQuery.trim($jqGrid('#txtDisclaimerPersonPosition').val()) : '',
                IsDislcaimerRequired: isDisclaimerRequired == true ? true : false,
                IsPatientRemoved: $jqGrid("#" + rowId + "_IsPatientRemoved").is(':checked'),
                ReasonCode: $jqGrid("#" + rowId + "_ReasonCode").val(),
                OrderCreationType: $('#hdnCarehomePatientOrder').val(),
                IsNDDManuallyChanged: isNDDManuallyChanged,
                IsOrderCancel: isCancel,
                IsProductOrderActivated: $jqGrid("#" + rowId + "_IsOrderActivated").is(':checked')
            }
            ordersList.push(orderOject);
        }
    }

    if (ordersList.length > 0) {        
        $jqGrid('#residentialMessage').hide();
        $.ajax({
            url: '/Order/SaveOrder',
            type: 'POST',
            contentType: "application/json",
            dataType: 'json',
            async: false,
            data: JSON.stringify(ordersList),
            success: function (data) {               
                $jqGrid('#txtResidentialNote').val('');
                $jqGrid('#divDataSavedOrders').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 'auto',
                    closeOnEscape: false,
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                        "id": "hdnGeneralOk",
                        click: function () {
                            $jqGrid('#btnSaveResidentialOrder').removeClass('btn_disable');
                            $jqGrid('#divDataSavedOrders').dialog('close');
                            $jqGrid('#jqResidentialActivated').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                            $jqGrid('#btnSaveResidentialOrder').val($jqGrid('#hdnbtnTextUpdate').val());
                            $jqGrid("#chkSelectAll").prop("checked", false);
                            $jqGrid("#chkSelectAll").parent(".ez-checkbox").removeClass("ez-checked");
                            window.location.reload(); //refresh the page after save
                        },
                    }]
                });               
                $jqGrid('#divDataSavedOrders').dialog('open');
                $jqGrid(".ui-dialog-titlebar-close").hide();
                return false;
            },
            error: function () {
                $jqGrid('#btnSaveResidentialOrder').removeClass('btn_disable');
            }
        });       
    }
}//function

function LoadCommunityOrderGrid() {    
    $jqGrid.ajax({
        url: '/Order/GetCommunityActivated',
        type: 'GET',
        cache: false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: { page: 1, rows: 50 },
        success: function (data) {
            if (data != null) {
                $jqGrid("#jqCommunityOrders").jqGrid('setGridParam', { datatype: 'local', data: data.rows }).trigger('reloadGrid');
                var isSentToSAP = data.isSentToSAP;
                enableDisableCommunityOrderPage(isSentToSAP);
                if(data.rows.length == 0)
                {
                    updateOrderDescriptioninScreen(true);
                }
            }
            else {
                updateOrderDescriptioninScreen(true);
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "LoadCommunityOrderGrid");
        }
    });
}//End of LoadCommunityOrderGrid

$('#btnSaveCommunityOrder').click(function () {
    
    $jqGrid('#btnSaveCommunityOrder').addClass('btn_disable');
    var totalRecordCount = $jqGrid("#jqCommunityOrders").jqGrid('getGridParam', 'records');
    if (totalRecordCount < 1) {
        $jqGrid('#btnSaveCommunityOrder').removeClass('btn_disable');
        $jqGrid("#communityMessage").html($jqGrid('#hdnOrderCommunityAddProduct').val());
        $jqGrid('#communityMessage').show();        
        return false;
    }

    if ($("#hdnADPMandetory").val() == "true" && $.trim($("#txtPatientADP").val()) == "") {
        $jqGrid('#btnSaveCommunityOrder').removeClass('btn_disable');

        CommonScriptObj.DisplayErrorMessage("divErrorMessage", $jqGrid('#hdnreserrormsADP').val(), 'pop_error_ul');
        return false;
    }

    if ($("#hdnNoteMandatory").val().toUpperCase() == userTypeforOrder.toUpperCase() && $.trim($("#txtCommunityNote").val()) == "") {
        $jqGrid('#btnSaveCommunityOrder').removeClass('btn_disable');

        $jqGrid("#divCommunityMessage").html($jqGrid('#hdnreserrormsNote').val());
        $jqGrid('#divCommunityMessage').show();
        return false;
    }

    var orderNote = $jqGrid.trim($jqGrid('#txtCommunityNote').val());
    if (orderNote == $jqGrid('#hdnNotePlaceholder').val()) {
        orderNote = "";
    }
    $jqGrid('#communityMessage').hide();

    var ordersList = [];
    var gridRows = $jqGrid("#jqCommunityOrders").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqCommunityOrders").jqGrid('getDataIDs');
    var zeroQuantityCount = 0;

    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = gridIds[i];
        var deliveryDate = row["NextDeliveryDate"];
        if (deliveryDate == "") {
            deliveryDate = row["DeliveryDate"];
        }
        var quantity = $jqGrid("#" + (rowId) + "_Quantity").val();
        if (quantity == undefined) {
            quantity = row["Quantity"];
            if (quantity == undefined) {
                quantity = $jqGrid('#jqCommunityOrders').jqGrid('getCell', rowId, 'Quantity');
            }
        }
        var description = $jqGrid("#" + (rowId) + "_ProductName").val();
        if (description == undefined) {
            description = row["ProductName"];
            if (description == undefined) {
                description = $jqGrid('#jqCommunityOrders').jqGrid('getCell', rowId, 'ProductName');
            }
        }

        var isNewOrder = $jqGrid('#spnOrderCommunityTitle').text() == $jqGrid('#hdnCommunityHeaderTextNew').val() ? true : false; //siddhesh

        if (quantity == 0 || quantity == "0") {
            zeroQuantityCount++;
        }                   

        var orderOject = {
            ProductID: row["ProductId"],
            Quantity: quantity,
            DeliveryDate: deliveryDate,
            OrderNote: $('#txtCommunityNote').val(),
            PatientADP: $('#txtPatientADP').val(),
            OrderType: window.orderTypeCommunity,
            OrderCreationType: $('#hdnCommunityOrderCrType').val(),
            Description: description,
            IsNDDManuallyChanged: isNDDManuallyChanged,
            IsCommunityOrder: true
        }
        ordersList.push(orderOject);
    }

    var isOrderCancelled = false;       
        
    if (zeroQuantityCount == gridRows.length) //siddhesh
    {
        isOrderCancelled = true;
    }

    ordersList.forEach(function (obj) {
        obj.IsOrderCancel = isOrderCancelled
    });

    if (!isOrderCancelled) {
        var firstRow = gridRows[0];
        var deliveryDate = firstRow["NextDeliveryDate"];
        if (deliveryDate == "") {
            deliveryDate = firstRow["DeliveryDate"];
        }

        if (IsHoliday(deliveryDate)) {
            ShowErrorMessage("divCommunityNDDHoliday");
            $jqGrid('#btnSaveCommunityOrder').removeClass('btn_disable');
            return false;
        }
    }
    $jqGrid("#data_loader").show();    
  
    $.ajax({
        url: '/Order/SaveOrder',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        data: JSON.stringify(ordersList),
        success: function (data) {            
            $jqGrid('#txtCommunityNote').val('');
            if (isOrderCancelled == true) {                
                $jqGrid("#divDataSavedCommunityOrders").text($jqGrid('#hdnOrderCancelled').val());
            }
            $jqGrid('#divDataSavedCommunityOrders').dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                resizable: false,
                closeOnEscape: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#btnSaveCommunityOrder').removeClass('btn_disable');
                        $jqGrid('#divDataSavedCommunityOrders').dialog('close');
                        $jqGrid('#jqCommunityOrders').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                        $jqGrid('#btnSaveCommunityOrder').val($jqGrid('#hdnbtnTextUpdate').val());
                        $jqGrid('#spnOrderCommunityTitle').text('');
                        $jqGrid('#spnOrderCommunityTitle').append('<span class="prependedCommunityOrderText">' + $jqGrid('#hdnCommunityHeaderTextExisting').val() + '</span>');
                        window.location.reload(); //refresh the page after save
                    },
                }]
            });
            $jqGrid('#divDataSavedCommunityOrders').dialog('open');
            $jqGrid(".ui-dialog-titlebar-close").hide();            
            return false;
        },
        error: function () {
            $jqGrid('#btnSaveCommunityOrder').removeClass('btn_disable');
        }
    });
    return false;
});

$('#btnSaveOneOffOrder').click(function () {
    $jqGrid('#MsgSelectProductsToRemoveOneOff').hide();
    $jqGrid('#btnSaveOneOffOrder').addClass('btn_disable');
    var totalRecordCount = $jqGrid("#jqOneOffOrders").jqGrid('getGridParam', 'records');
    var orderCreationType = '0';
    if ($("#hdnOneOffNoteMandatory").val().toUpperCase() == userTypeforOrder.toUpperCase() && $.trim($("#txtOneOffNote").val()) == "") {
        $jqGrid('#btnSaveOneOffOrder').removeClass('btn_disable');
        $jqGrid("#oneOffMessage").html($jqGrid('#hdnOneOffreserrormsNote').val());
        $jqGrid('#oneOffMessage').show();
        return false;
    }
    if (totalRecordCount < 1) {
        $jqGrid('#btnSaveOneOffOrder').removeClass('btn_disable');
        $jqGrid("#oneOffMessage").html($jqGrid('#hdnOrderOneOffAddProduct').val());
        $jqGrid('#oneOffMessage').show();
        return false;
    }

    var orderNote = jQuery.trim($jqGrid('#txtOneOffNote').val());
    if (orderNote == $jqGrid('#hdnOneOffPlaceholder').val()) {
        orderNote = "";
    }
    $jqGrid('#oneOffMessage').hide();
    var ordersList = [];
    var gridRows = $jqGrid("#jqOneOffOrders").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqOneOffOrders").jqGrid('getDataIDs');    
    var orderType = ($jqGrid("#hdnCarehomeId").val() > 0 && $jqGrid("#hdnPatientId").val() == "") ? $jqGrid("#hdnCarehomeProductOrderType").val() : window.orderTypeStandard;
    var excludeServiceCharge = $('#chkExcludeServiceCharge').is(':checked') ? true : false;

    if ($('#rdoRushOrder').is(':checked') || $('#rdoRushOrder').attr('checked') == true) { orderType = window.orderTypeRush; }    
    if (orderType != window.orderTypeRush) {
        var firstRow = gridRows[0];
        var deliveryDate = firstRow["DeliveryDate"];
    }

    if (orderType == window.orderTypeStandard)
    {
        if($jqGrid("#hdnPatientId").val() > 0 && ($jqGrid("#hdnCarehomeId").val() == 0 || $jqGrid("#hdnCarehomeId").val() == "")){
            orderCreationType = $jqGrid("#hdnCommunityOneOffStandard").val();
        }        
        else if ($jqGrid("#hdnPatientId").val() > 0 && $jqGrid("#hdnCarehomeId").val() > 0) {
            orderCreationType = $jqGrid("#hdnCarehomePatientOneOffStandard").val();
        }
    }
    else if (orderType == window.orderTypeRush)
    {
        if ($jqGrid("#hdnPatientId").val() > 0 && ($jqGrid("#hdnCarehomeId").val() == 0 || $jqGrid("#hdnCarehomeId").val() == "")) {
            orderCreationType = $jqGrid("#hdnCommunityOneOffRush").val();
        }
        else if (($jqGrid("#hdnPatientId").val() == "" || $jqGrid("#hdnPatientId").val() == 0) && $jqGrid("#hdnCarehomeId").val() > 0) {
            orderCreationType = $jqGrid("#hdnCarehomeOneOffRush").val();
        }
        else if ($jqGrid("#hdnPatientId").val() > 0 && $jqGrid("#hdnCarehomeId").val() > 0) {
            orderCreationType = $jqGrid("#hdnCarehomePatientOneOffRush").val();
        }       
    }
    else {
        orderCreationType = $jqGrid("#hdnCarehomeOneOffStandard").val();  //ZHDH order
    }

    var zeroQuantityCount = 0;
    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = gridIds[i];
        var isFoc = row["IsFOC"];
       
        if ($jqGrid("#" + (rowId) + "_IsFOC").val() != undefined) {
            isFoc = $jqGrid('#' + (rowId) + "_IsFOC").is(':checked');
        }
        
        var quantity = $jqGrid("#" + (rowId) + "_Quantity").val();
        if (quantity == undefined) {
            quantity = row["Quantity"];
            if (quantity == undefined) {                
                quantity = $jqGrid('#jqOneOffOrders').jqGrid('getCell', rowId, 'Quantity');
            }
        }

        if (quantity == 0) {
            zeroQuantityCount++;
        }
        var orderOject = {
            ProductID: row["ProductId"],
            Quantity: quantity,
            DeliveryDate: row["DeliveryDate"],
            OrderNote: orderNote,
            OrderType: orderType,
            IsFOC: isFoc,
            ExcludeServiceCharge: excludeServiceCharge,
            IsOneOffOrder: true,
            isProductLevel: ($jqGrid("#hdnCarehomeId").val() > 0 && $jqGrid("#hdnPatientId").val()=="") ? true : false,
            CareHomeID: $jqGrid("#hdnCarehomeId").val() > 0 ? $jqGrid("#hdnCarehomeId").val() : 0,
            OrderCreationType: orderCreationType,
            IsNDDManuallyChanged: isNDDManuallyChanged,
            OrderId: row["OrderId"],
        }
        ordersList.push(orderOject);
    }

    var isOrderCancelled = false;
    if (zeroQuantityCount == gridRows.length) {
        isOrderCancelled = true;
        $jqGrid("#divDataSavedOrders").text($jqGrid('#hdnOrderCancelled').val());
    } else {
        $jqGrid("#divDataSavedOrders").text($jqGrid('#hdnDivOrderSaved').val());
    }

    $jqGrid("#data_loader").show();    
    
    $.ajax({
        url: '/Order/SaveOrder',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        data: JSON.stringify(ordersList),
        success: function (data) {
            $jqGrid('#txtOneOffNote').val('');
            $jqGrid('#divDataSavedOrders').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#btnSaveOneOffOrder').removeClass('btn_disable');
                        $jqGrid('#divDataSavedOrders').dialog('close');
                        $jqGrid("#data_loader").hide();
                        if ($('#rdoStandardOrder').is(':checked')) {
                            LoadOneOffOrderDetails(false);
                        } else {
                            LoadOneOffOrderDetails(true);
                        }
                    },
                }]
            });
            $jqGrid('#divDataSavedOrders').dialog('open');
            $jqGrid(".ui-dialog-titlebar-close").hide();
            //OneOffGrid();
            return false;
        },
        error: function () {
            $jqGrid('#btnSaveOneOffOrder').removeClass('btn_disable');
        }
    });
    return false;
});

function LoadStandardNDD() {
    $.ajax({
        type: 'POST',
        url: '/Order/GetNDDForStandardOneOffOrders',
        async: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            window.NDD = data.NDD;
            $jqGrid('#hdnOneOffNDDStartDate').val(data.NDD);
            var allRowsInGrid = $jqGrid('#jqOneOffOrders').jqGrid('getGridParam', 'records');
            for (var i = 1; i <= allRowsInGrid; i++) {
                $jqGrid('#jqOneOffOrders' + ' #' + i + ' [aria-describedby=jqOneOffOrders' + '_DeliveryDate]').html(data.NDD);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function SelectStandardOrderType() {
    $jqGrid('#rdoStandardOrder').attr('checked', 'checked');
    $jqGrid('.rdoStandardOrder_class .ez-radio').addClass('ez-selected');
    $jqGrid('#rdoRushOrder').prop('checked', false);
    $jqGrid('.rdoRushOrder_class .ez-radio').removeClass('ez-selected');
}

function LoadRushOrderNDD() {
    $jqGrid('#rdoRushOrder').attr('checked', 'checked');
    $jqGrid('.rdoRushOrder_class .ez-radio').addClass('ez-selected');
    $jqGrid('#rdoStandardOrder').prop('checked', false);
    $jqGrid('.rdoStandardOrder_class .ez-radio').removeClass('ez-selected');
    $jqGrid('.DisablePages').removeClass('disable_div');
    $jqGrid('#j_lightbox_error').hide();
    $.ajax({
        type: 'POST',
        url: '/Order/GetNDDForRushOneOffOrders',
        async: true,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            window.NDD = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

$('#rdoStandardOrder').change(function () {
    if (!$('#rdoStandardOrder').is(':checked')) {
        return;
    }
    $jqGrid('#oneOffMessage').hide();
    $jqGrid('#txtOneOffNote').val('');
    LoadStandardNDD();
    LoadOneOffOrderDetails(false);
});

$('#rdoRushOrder').change(function () {
    if (!$('#rdoRushOrder').is(':checked')) {
        return;
    }
    $jqGrid('#oneOffMessage').hide();
    $jqGrid('#txtOneOffNote').val('');
    LoadRushNDD();
    LoadOneOffOrderDetails(true);
});

function LoadRushNDD() {
    $.ajax({
        type: 'POST',
        url: '/Order/GetNDDForRushOneOffOrders',
        async: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            window.NDD = data;
            $jqGrid('#hdnOneOffNDDStartDate').val(data);
            var allRowsInGrid = $jqGrid('#jqOneOffOrders').jqGrid('getGridParam', 'records');
            for (var i = 1; i <= allRowsInGrid; i++) {
                $jqGrid('#jqOneOffOrders' + ' #' + i + ' [aria-describedby=jqOneOffOrders' + '_DeliveryDate]').html(data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function removeRows(cellvalue, options, rowobject) {
    return "<input type='checkbox' class='chkRemoveClass' id=" + options['rowId'] + "_" + options.colModel["name"] + " onchange=countCheckedCheckboxOrder()>";
}

function countCheckedCheckboxOrder() {
    
    var numberOfChecked = $jqGrid('.chkRemoveClass:checked').length;
    var totalCheckboxes = $('.chkRemoveClass:checkbox').length;
    if (numberOfChecked == totalCheckboxes){
        $jqGrid("#chkRemove").prop("checked", true);
        $jqGrid("#chkRemove").parent(".ez-checkbox").addClass("ez-checked");
        
    }
    else{
        $jqGrid("#chkRemove").prop("checked", false);
        $jqGrid("#chkRemove").parent(".ez-checkbox").removeClass("ez-checked");
        
    }
}

//for Community orders 
function removeFromCommunityActivated() {
    if ($jqGrid('#btnSaveCommunityOrder').val() == "Update") {
        if ($jqGrid("#chkRemove").is(':checked')) {
            $jqGrid('#divRemoveAllActiveOrderValidation').dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                closeOnEscape: false,
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid(this).dialog("close");
                    },
                }]
            });
            $jqGrid('#divRemoveAllActiveOrderValidation').dialog('open');
            return false;
        }
    }

    $jqGrid('#MsgSelectProductsToRemove').hide();
    var isRemoved = '';
    var rowIds = [];
    $jqGrid(".chkRemoveClass").each(function (rowId) {
        if ($(this).is(':checked')) {
            var Trid = $(this).closest('tr').prop('id');
            rowIds.push(Trid);
        }
        $jqGrid("#chkRemove").prop("checked", false);
    });
    var reveseIds = (rowIds.reverse());

    if (reveseIds.length == 0) {
        $jqGrid('#MsgSelectProductsToRemove').show();
        return false;
    }
    else {
        $jqGrid('#MsgSelectProductsToRemove').hide();
        $jqGrid("#divRemoveCommunityProduct").dialog({
            resizable: false,
            height: 140,
            modal: true,
            buttons: [{
                text: $jqGrid("#hdnGeneralYes").val().toString(),
                "id": "hdnGeneralYes",
                click: function () {
                    var productIds = [];
                    $.each(reveseIds, function (index) {
                        var rowId = reveseIds[index];
                        var productId = $jqGrid("#" + rowId + "_ProductId").val();
                        var productisplayId = $jqGrid("#" + rowId + "ProductDisplayId").val();
                        if (productId == undefined) {
                            productId = $jqGrid('#jqCommunityOrders').jqGrid('getCell', rowId, 'ProductId');
                        }
                        productIds.push(productId);
                        $jqGrid('#jqCommunityOrders').jqGrid('delRowData', rowId);
                    })
                    $jqGrid("#divRemoveCommunityProduct").dialog("close");
                    $jqGrid.ajax({
                        url: '/Order/RemoveProduct',
                        data: {
                            productIdList: productIds
                        },
                        type: 'POST',
                        cache: false,
                        success: function (data) {
                            $jqGrid("#divRemoveCommunityProduct").dialog("close");
                            if (data == true) {
                                $jqGrid('#divRemoveCommunitySucess').dialog({
                                    autoOpen: false,
                                    modal: true,
                                    width: 'auto',
                                    closeOnEscape: false,
                                    resizable: false,
                                    buttons: [{
                                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                                        "id": "hdnGeneralOk",
                                        click: function () {
                                            $jqGrid(this).dialog("close");
                                        },
                                    }]
                                });
                                $jqGrid('#divRemoveCommunitySucess').dialog('open');
                                $jqGrid(".ui-dialog-titlebar-close").hide();
                            }

                        }
                    });
                },
            }, {
                text: $jqGrid("#hdnGeneralNo").val().toString(),
                "id": "hdnGeneralNo",
                click: function () {
                    $.each(reveseIds, function (index) {
                        var rowId = reveseIds[index];
                        $('#' + rowId + "_Remove").attr('checked', false);
                    })

                    $jqGrid("#divRemoveCommunityProduct").dialog("close");
                },
            }]
        });
    }
    $jqGrid('#divRemoveCommunityProduct').dialog('open');
    $jqGrid(".ui-dialog-titlebar-close").hide();
}

function addSelectedPrescriptionProduct(selectedProducts) {
    productsNotAdded = [];
    productsNotAddedUnique = [];
    productsNotAddedDisplayId = [];
    isReturn = 0;
    for (var i = 0; i < selectedProducts.length; i++) {
        var rowid = selectedProducts[i];
        {
            if (window.gridid == "jqCommunityOrders" || window.gridid == "jqOneOffOrders") {
                var rowData = $jqGrid("#jqPatientPrescription").jqGrid('getRowData', rowid);
                var gridid = window.gridid;
                var grid1 = $jqGrid("#" + gridid + ""), idsgrid = grid1.jqGrid('getDataIDs'), m, p = idsgrid.length;
                for (m = 0; m < p; m++) {
                    grid1.jqGrid('restoreRow', idsgrid[m]);
                }
                var deliveryDate = "";
                var NDD = "";

                var quantity = rowData['FlagDQ1'] == "true" ? rowData['DQ1'] : (rowData['FlagDQ2'] == "true" ? rowData['DQ2'] : (rowData['FlagDQ3'] == "true" ? rowData['DQ3'] : (rowData['FlagDQ4'] == "true" ? rowData['DQ4'] : 0)));
                if (gridid == 'jqCommunityOrders') {
                    var rowId = 0;
                    var ids = $jqGrid("#" + gridid + "").jqGrid('getDataIDs')

                    if (ids.length > 0) {
                        rowId = ids[0];
                    }
                    var parentRowData = $jqGrid("#" + gridid + "").getRowData(rowId);

                    deliveryDate = parentRowData['DeliveryDate'];
                    if (deliveryDate == undefined || deliveryDate == "") {                        
                        deliveryDate = $('#hdnSessionNextDeliveryDate').val();
                    }
                    NDD = parentRowData['NextDeliveryDate'];
                    if (NDD == undefined) {
                        if (window.NextDeliveryDate != undefined) {
                            NDD = window.NextDeliveryDate;
                        }
                        else {
                            NDD = deliveryDate;
                        }
                    }
                    else {
                        //If there is atleast one row in the grid, then else part will be executed
                        window.NextDeliveryDate = NDD;
                    }
                }
                else if (gridid == 'jqOneOffOrders') {
                    deliveryDate = window.NDD;
                    if (deliveryDate == undefined)
                        deliveryDate = '@Convert.ToString(Session["OneOffNDD"])';
                }

                var productId = rowData["ProductId"];
                var description = rowData["ProductName"];
                var productDisplayId = rowData["ProductDisplayId"];
                var salesUnit = rowData["SalesUnit"];
                var datarow = { ProductDisplayId: productDisplayId, ProductId: productId, ProductName: description, Unit: salesUnit, DeliveryDate: deliveryDate, Quantity: quantity, QuantityToCompare: quantity, NextDeliveryDate: NDD, RowStatus: "New" };
                var gridrowdata = $jqGrid("#" + gridid + "").jqGrid('getRowData');

                var gridValidation = $jqGrid("#" + gridid + ""),
                idsthis = gridValidation.jqGrid('getDataIDs'), p, m = idsthis.length;
                for (p = 1; p <= m; p++) {
                    var rowDataBackgrid = $jqGrid("#" + gridid + "").jqGrid('getRowData', p);
                    var productDisplayIdgrid = rowDataBackgrid["ProductId"];
                    if (productDisplayIdgrid == undefined) {
                        productDisplayIdgrid = $jqGrid("#" + gridid + "").jqGrid('getCell', p, 'ProductId');
                    }
                    if (productId == productDisplayIdgrid) {
                        ClosePopup('divPrescriptionList');
                        productsNotAdded.push(rowid);
                        isReturn = 1;
                    }
                }
                if (isReturn != 1) {
                    $jqGrid("#" + gridid + "").jqGrid('addRowData', gridrowdata.length + 1, datarow, "first");
                    $jqGrid("#" + gridid + "").trigger('reloadGrid');//added to resolved page 1 of 0
                }
                isReturn = 0;
            }
        }

    }//for loop
    if (productsNotAdded.length > 0) {
        productsNotAddedUnique = [];
        productsNotAddedDisplayId = [];
        $jqGrid.each(productsNotAdded, function (i, item) {
            if ($jqGrid.inArray(item, productsNotAddedUnique) === -1) {
                var rowData = $jqGrid('#jqPatientPrescription').jqGrid('getRowData', item);
                productsNotAddedUnique.push(item);
            }
        });
        $jqGrid.each(productsNotAddedUnique, function (i, item) {
            var rowData = $jqGrid('#jqPatientPrescription').jqGrid('getRowData', item);
            productsNotAddedDisplayId.push(rowData["ProductDisplayId"]);
        });
    }
    var grid = $jqGrid("#" + gridid + ""),
                              ids = grid.jqGrid('getDataIDs'), q, l = ids.length;
    for (q = 0; q < l; q++) {
        grid.jqGrid('editRow', ids[q], true);
    }
    ClosePopup('divPrescriptionList');
    if (productsNotAddedDisplayId.length > 0) {
        $(".prepended").remove();
        $('#lblProductNotAdded').append('<span class="prepended"> ' + productsNotAddedDisplayId.join(","));
        $jqGrid('#divErrorProductNotAdded').dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralOk").val().toString(),
                "id": "hdnGeneralOk",
                click: function () {
                    $jqGrid('#divErrorProductNotAdded').dialog('close');
                    var grid = $jqGrid("#jqPatientPrescription"), ids = grid.jqGrid('getDataIDs'), i, l = ids.length;
                    if ($("#show_removed").is(':checked')) {
                        for (var i = 0; i < l; i++) {
                            $jqGrid("#jqPatientPrescription #" + (ids[i])).show();
                        }
                    } else {
                        for (var i = 0; i < l; i++) {
                            if ($jqGrid("#jqPatientPrescription #" + ids[i] + "_IsRemoved").val() == 'notremoved') {
                                $jqGrid("#jqPatientPrescription #" + ids[i]).show();
                            }
                        }
                    }
                },
            }]
        });
        $jqGrid('#divErrorProductNotAdded').dialog('open');
    }
    $jqGrid("#chkRemove").prop("checked", false);
}//end of function

function HideNurseCommentsDiv() {
    $jqGrid('#divNurseDetails').hide();
}


function CloseOrderMsgPopup(PatientId, CustomerId, PatientName, CustomerName) {
    var referrer = document.referrer;
    var searchPage = "/Search/SearchData";
    var clickEvent = "";
    var redirectFrom = $jqGrid('#hdnOrderPage').val();

    $jqGrid.ajax({
        url: '/Patient/GetClickEventFromSession',
        async: false,
        cache: false,
        success: function (Data) {
            clickEvent = Data;
        }
    });

    if (clickEvent == "2")//double clicked on searchResult and came to order page
    {
        $jqGrid.ajax({
            url: '/Patient/Renderpatient',
            type: 'POST',
            async: false,
            data: { PatientId: PatientId, CustomerId: CustomerId, PatientName: PatientName, CustomerName: CustomerName, RedirectFrom: redirectFrom },
            cache: false,
            success: function (Data) {
                window.location.href = "/Patient/PatientDetails";
            }
        });
    }
    else {
        $jqGrid.ajax({
            url: '/Patient/SetRedirectPage',
            type: 'POST',
            async: false,
            data: { redirectFrom: redirectFrom },
            cache: false,
            success: function (Data) {
            }
        });
            
        if (window.location.href == referrer) {
            window.location.href = searchPage;
        }
        else {
            window.location.href = referrer;
        }
        
        $jqGrid("#dock li ul").animate({ left: "40px" }, 200);
        $jqGrid("#dock li ul").removeClass("free");
        $jqGrid("#dock li ul").addClass("dock");
        $jqGrid("#dock li ul").addClass("docked");
        $jqGrid("#search_dock_bg").css('display', 'block');
    }
}

function countCheckedResidential() {
    var numberOfChecked = $jqGrid('.chkSelectClass:checked').length;
    var totalCheckboxes = $('.chkSelectClass:checkbox').length;
    if (numberOfChecked == totalCheckboxes) {
        $jqGrid("#chkSelectAll").prop("checked", true);
        $jqGrid("#chkSelectAll").parent(".ez-checkbox").addClass("ez-checked");
    }
    else {
        $jqGrid("#chkSelectAll").prop("checked", false);
        $jqGrid("#chkSelectAll").parent(".ez-checkbox").removeClass("ez-checked");
    }
}

function ShowOrderErrorMessage(id, patientId, customerId, patientName, customerName) {
    $jqGrid('#' + id).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        closeOnEscape: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralOk").val().toString(),
            "id": "hdnGeneralOk",
            className: "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only",
            click: function () {
                $jqGrid('#' + id).dialog('close');
                CloseOrderMsgPopup(patientId, customerId, patientName, customerName);
            },
        }]
    });

    $jqGrid('#' + id).dialog('open');
}

function enableDisableOneOffOrderPage(isDisable) {
    if (isDisable) {
        $jqGrid('#gridOneOffOrders').addClass('read_only_div')
        $jqGrid("#OneOffOrderButtons").addClass('read_only_div');
        $jqGrid("#btnSaveOneOffOrder").addClass('btn_disable');
        $jqGrid("#spnOneOffOrderSentToSAP").show();
    }
    else {
        $jqGrid('#gridOneOffOrders').removeClass('read_only_div');
        $jqGrid("#OneOffOrderButtons").removeClass('read_only_div');
        $jqGrid("#btnSaveOneOffOrder").removeClass('btn_disable');
        $jqGrid("#spnOneOffOrderSentToSAP").hide();
    }
}

function enableDisableCommunityOrderPage(isDisable)
{
    if (isDisable) {
        $jqGrid('#gridCommunityOrders').addClass('read_only_div')
        $jqGrid("#CommunityOrderButtons").addClass('read_only_div');
        $jqGrid("#btnSaveCommunityOrder").addClass('btn_disable');
        $jqGrid("#spnOrderSentToSAP").show();
    }
    else {
        $jqGrid('#gridCommunityOrders').removeClass('read_only_div');
        $jqGrid("#CommunityOrderButtons").removeClass('read_only_div');
        $jqGrid("#btnSaveCommunityOrder").removeClass('btn_disable');
        $jqGrid("#spnOrderSentToSAP").hide();
    }
}

function enableDisableResidentialOrderPage(isDisable) {
    if (isDisable) {
        $jqGrid('#residentialOrderDisableGrid').addClass('disable_div')
        $jqGrid("#ResidentialOrderButtons").addClass('read_only_div');
        $jqGrid("#btnSaveResidentialOrder").addClass('btn_disable');
        $jqGrid("#btnCancelResidentialOrder").addClass('btn_disable');
        $jqGrid("#spnOrderSentToSAP").show();
    }
    else {
        $jqGrid('#residentialOrderDisableGrid').removeClass('disable_div');
        $jqGrid("#ResidentialOrderButtons").removeClass('read_only_div');
        $jqGrid("#btnSaveResidentialOrder").removeClass('btn_disable');
        $jqGrid("#btnCancelResidentialOrder").removeClass('btn_disable');
        $jqGrid("#spnOrderSentToSAP").hide();
    }
}

function getPatientReasonCodes(patientStatusCode, customerId) {

    $jqGrid.getJSON('/Patient/LoadReasonCode', {
        Patientstatus: patientStatusCode, customerId: customerId
    }, function (data) {
        lstReasonCode = data;
    })
    .fail(function(x, e) {
        HandleAjaxError(x, e, jsfilename, "getPatientReasonCodes");
    })
}

function chkPatientStatus(cellvalue, options, rowobject) {
    return "<input type='checkbox' class='chkPatientStatusClass chk_" + rowobject['PatientID'] + "'  id='" + options['rowId'] + "_IsPatientRemoved' onchange='PatientStatusChange(" + options['rowId'] + ")' />";
}

function getReasonCode(cellvalue, options, rowobject) {
    if (lstReasonCode.length == 0)
        return "";

    var objDropdown = "<select disabled='true' style='width:120px' class='ddl_" + rowobject['PatientID'] + "' id='" + options['rowId'] + "_ReasonCode' onchange='RemovedReasonCodeChange(" + options['rowId'] + ")'><option value='0'>" + $jqGrid("#hdnDefaultReasonCode").val() + "</option>";

    $.each(lstReasonCode, function (i) {
        objDropdown += "<option value='" + lstReasonCode[i].ListId + "'>" + lstReasonCode[i].DisplayText + "</option>";
    })

    objDropdown += "</select>";
    return objDropdown;
}

function PatientStatusChange(rowId) {
    var row = $jqGrid('#jqResidentialActivated').jqGrid('getRowData', rowId);
    var patientId = row.PatientID;
    var chkValue = $jqGrid("#" + rowId + "_IsPatientRemoved").is(':checked');

    $jqGrid("#jqResidentialActivated").find(".chk_" + patientId).prop('checked', chkValue);

    if (chkValue)
        $jqGrid("#jqResidentialActivated").find(".chk_" + patientId).parent(".ez-checkbox").addClass("ez-checked");
    else
        $jqGrid("#jqResidentialActivated").find(".chk_" + patientId).parent(".ez-checkbox").removeClass("ez-checked");

    $jqGrid("#jqResidentialActivated").find(".ddl_" + patientId).prop('disabled', !chkValue);
    $jqGrid("#jqResidentialActivated").find(".ddl_" + patientId).val(0);

    $jqGrid("#jqResidentialActivated").find(".chkSelect_" + patientId).prop('disabled', chkValue);
    $jqGrid("#jqResidentialActivated").find(".chkSelect_" + patientId).prop('checked', false);
    $jqGrid("#jqResidentialActivated").find(".chkSelect_" + patientId).parent(".ez-checkbox").removeClass("ez-checked");

    $jqGrid("#chkSelectAll").prop("checked", false);
    $jqGrid("#chkSelectAll").parent(".ez-checkbox").removeClass("ez-checked");
}


function RemovedReasonCodeChange(rowId) {
    var row = $jqGrid('#jqResidentialActivated').jqGrid('getRowData', rowId);
    var patientId = row.PatientID;
    var ddlValue = $jqGrid("#" + rowId + "_ReasonCode").val();

    $jqGrid("#jqResidentialActivated").find(".ddl_" + patientId).val(ddlValue);
}

//*******Start Function : GetOrderNote *********************************************
function GetOrderNote(hiddenfieldId, txtId) {
    $.ajax({
        url: '/Order/GetOrderActivationDetails',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        cache: false,
        async: false,
        success: function (responseData) {
            if (responseData != null) {
                $jqGrid('#' + txtId).val(responseData.NoteText);
                if (responseData.UserType.toUpperCase() == userTypeforOrder.toUpperCase()) {
                    var lblNote = '<strong> * </strong>';
                    $jqGrid("#lblNote").append(lblNote);
                    $jqGrid("#" + hiddenfieldId).val(responseData.UserType);
                }
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "GetOrderNote");
        }
    });
}
//*******End Function : GetOrderNote **********************************************

function IsFOC(cellvalue, options, rowobject) {
    var controlStr = "<input type='checkbox' id= '" + options['rowId'] + "_IsFOC' ";

    if (rowobject['IsFOC'] == true || rowobject['IsFOC'] == "1") {
        controlStr = controlStr + "checked = 'checked' ";
    }
    controlStr = controlStr + "/>";
    return controlStr;
}

function LoadOneOffOrderDetails(isRushOrder) {
    var isNew = true;
    var orderType = "New";
    var buttonText = "Save";
    $jqGrid('#MsgSelectProductsToRemoveOneOff').hide();
    $jqGrid("#jqOneOffOrders").jqGrid('clearGridData');

    $jqGrid.ajax({
        url: '/Order/GetOneOffOrderActivated',
        async: false,
        data: { isRushOrder: isRushOrder },
        cache: false,
        success: function (Data) {
            if (Data.rows.length > 0) {
                isNew = false;
                enableDisableOneOffOrderPage(Data.isSentToSAP);
                $jqGrid("#jqOneOffOrders").jqGrid('hideCol', ["ColumnName", "Remove"]);
                $jqGrid("#jqOneOffOrders").jqGrid('setGridParam', { datatype: 'local', data: Data.rows }).trigger('reloadGrid');
                $jqGrid("#removeoneoff").hide();
                if (Data.rows[0].ExcludeServiceCharge == true) {
                    $('#chkExcludeServiceCharge').attr('checked', 'checked')
                    $('#chkExcludeServiceCharge').closest(".ez-checkbox").addClass("ez-checked");
                }
                else {
                    $('#chkExcludeServiceCharge').removeAttr('checked')
                    $('#chkExcludeServiceCharge').closest(".ez-checkbox").removeClass("ez-checked");
                }
            }
            else {
                $jqGrid("#jqOneOffOrders").jqGrid('showCol', ["ColumnName", "Remove"]);
                $jqGrid("#removeoneoff").show();
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "LoadCustomerProductList");
        }
    });

    if (!isNew) {
        orderType = "Existing";
        buttonText = "Update";
    }
    $jqGrid('#spnOrderOneOffTitle').text('');
    $jqGrid('#spnOrderOneOffTitle').append('<span class="prependedCommunityOrderText">' + orderType + ' One Off Order</span>');
    $jqGrid('#btnSaveOneOffOrder').val(buttonText);

    return isNew;
}

//This is for Community Order
function updateOrderDescriptioninScreen(isNew) {
    if (isNew) {
        $jqGrid('#btnSaveCommunityOrder').val($jqGrid('#hdnbtnTextSaveCommunity').val());
        $jqGrid('#spnOrderCommunityTitle').text('');
        $jqGrid('#spnOrderCommunityTitle').append('<span class="prependedCommunityOrderText">' + $jqGrid('#hdnCommunityHeaderTextNew').val() + '</span>');

        $jqGrid('#btnRemoveFromCommunity').removeClass("btn_disable");
        $jqGrid('#btnRemoveFromCommunity').removeAttr("disabled");
    }
    else {
        $jqGrid('#btnSaveCommunityOrder').val($jqGrid('#hdnbtnTextUpdate').val());
        $jqGrid('#spnOrderCommunityTitle').text('');
        $jqGrid('#spnOrderCommunityTitle').append('<span class="prependedCommunityOrderText">' + $jqGrid('#hdnCommunityHeaderTextExisting').val() + '</span>');

        //Disable the remove button while updating the existing order
        $jqGrid('#btnRemoveFromCommunity').addClass("btn_disable");
        $jqGrid('#btnRemoveFromCommunity').attr("disabled");
    }
}

//This is for One Off Order
function updateOrderDescriptioninScreenOneOff(isNew) {
    if (isNew) {
        $jqGrid('#btnSaveCommunityOrder').val($jqGrid('#hdnbtnTextSaveOneOff').val());
        $jqGrid('#spnOrderCommunityTitle').text('');
        $jqGrid('#spnOrderCommunityTitle').append('<span class="prependedCommunityOrderText">' + $jqGrid('#hdnCommunityHeaderTextNew').val() + '</span>');

        $jqGrid('#btnRemoveFromCommunity').removeClass("btn_disable");
        $jqGrid('#btnRemoveFromCommunity').removeAttr("disabled");
    }
    else {
        $jqGrid('#btnSaveCommunityOrder').val($jqGrid('#hdnbtnTextUpdateOneOff').val());
        $jqGrid('#spnOrderCommunityTitle').text('');
        $jqGrid('#spnOrderCommunityTitle').append('<span class="prependedCommunityOrderText">' + $jqGrid('#hdnCommunityHeaderTextExisting').val() + '</span>');

        //Disable the remove button while updating the existing order
        $jqGrid('#btnRemoveFromCommunity').addClass("btn_disable");
        $jqGrid('#btnRemoveFromCommunity').attr("disabled");
    }
}