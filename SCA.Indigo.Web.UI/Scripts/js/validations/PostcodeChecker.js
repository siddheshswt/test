﻿$jqGrid = jQuery.noConflict();
var PopupParent = "";
function showPopupPostCode(parentpage, PostCode) {    
    $jqGrid("#jqPostCodeSearchResults").jqGrid('clearGridData');
    if (parentpage === "DeliveryAddress" || parentpage === "PatientAddress" || parentpage == "SampleOrder") {
        AddressListBegin($('#hdnPostCodeSearchKey').val(), PostCode);
    }
    else {
        AddressListBegin($('#hdnPostCodeSearchKey').val(), $('#PostCode').val());
    }
    PopupParent = parentpage
}
function AddressListBegin(Key, Postcode) {
    
    var scriptTag = document.getElementById("pcascript"),
         headTag = document.getElementsByTagName("head").item(0),
         strUrl = "";
    postCodeSearchUrl = $jqGrid('#hdnPostCodeSearchUrl').val();

    //Build the url
    strUrl = postCodeSearchUrl;
    strUrl += "&Key=" + escape(Key);
    strUrl += "&Postcode=" + escape(Postcode);
    strUrl += "&CallbackFunction=AddressListEnd";

    //Make the request
    if (scriptTag) try { headTag.removeChild(scriptTag) } catch (e) { }

    scriptTag = document.createElement("script");
    scriptTag.src = strUrl;
    scriptTag.type = "text/javascript";
    scriptTag.id = "pcascript";
    headTag.appendChild(scriptTag);
}

function AddressListEnd(response) {
  
    if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
        $jqGrid("#divPostCodeSearchPopup").hide();
        if (response[0].Error == 1001)
            $("#divErrorMessage").html($('#hdnPostCodeRequired').val());
        else if (response[0].Error == 1002)
            $("#divErrorMessage").html($('#hdnPostCodeInvalid').val());
        ShowHideError();
    }
    else {
        if (response.length == 0) {
            $jqGrid("#divPostCodeSearchPopup").hide();
            $("#divErrorMessage").html("Sorry, no matching items found");
            ShowHideError();
        }
        else {
            var postCodeDialog = $jqGrid("#divPostCodeSearchPopup").dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: true,
                title: $jqGrid('#hdnPostCodeTitle').val()
            });
            postCodeDialog.dialog('open');
            $jqGrid("#jqPostCodeSearchResults").jqGrid('clearGridData');
            $jqGrid("#jqPostCodeSearchResults").jqGrid('setGridParam', { datatype: 'local', data: response }).trigger('reloadGrid');
        }
    }
}

function SelectAddress(Key, Id) {
    if (!Id) return;
    var scriptTag = document.getElementById("pcascript"),
         headTag = document.getElementsByTagName("head").item(0),
         strUrl = "";
    postCodeSearchUrlRetrieve = $jqGrid('#hdnPostCodeSearchRetrieve').val();
    //Build the url
    strUrl = postCodeSearchUrlRetrieve;
    strUrl += "&Key=" + escape(Key);
    strUrl += "&Id=" + escape(Id);
    strUrl += "&CallbackFunction=SelectAddressEnd";

    //Make the request
    if (scriptTag) try { headTag.removeChild(scriptTag) } catch (e) { }

    scriptTag = document.createElement("script");
    scriptTag.src = strUrl;
    scriptTag.type = "text/javascript";
    scriptTag.id = "pcascript";
    headTag.appendChild(scriptTag);
}

function SelectAddressEnd(response) {
    if (response.length == 1 && typeof (response[0].Error) != 'undefined')
        $("#divPostCodeErrorMessage").html(response[0].Description);
    else {
        if (response.length == 0)
            $("#divPostCodeErrorMessage").html("Sorry, no matching items found");
        else {
            if (PopupParent == "carehome") {
              
                SetCareHomeAddress(response);
            }
            else if (PopupParent == "DeliveryAddress") {

                SetCorrespondenceAddress(response);
            }
            else if (PopupParent == "SampleOrder") {

                SetSampleOrderAddress(response);
            }
            else {
                SetPatientAddress(response);
            }
        }
    }
}

//*START SetSampleOrderAddress **********************************************************************************************************
function SetSampleOrderAddress(response)
{
    var houseName = "";
    if (response[0].BuildingName != undefined) {
        houseName = response[0].BuildingName.toString().toUpperCase();
    }
    if ($jqGrid.trim(houseName).length == 0 && response[0].Company != undefined) {
        houseName = response[0].Company.toString().toUpperCase();
    }
    if (document.getElementById('txtSampleHouseNumber') != null || document.getElementById('txtSampleHouseNumber') != undefined) {
        document.getElementById("txtSampleHouseNumber").value = response[0].BuildingNumber.toUpperCase();
    }
    if (document.getElementById('txtSampleHouseName') != null || document.getElementById('txtSampleHouseName') != undefined) {
        document.getElementById("txtSampleHouseName").value = houseName;
    }
    if (document.getElementById('txtAddress1') != null || document.getElementById('txtAddress1') != undefined) {
        document.getElementById("txtAddress1").value = response[0].Line1.toUpperCase();
    }
    if (document.getElementById('txtAddress2') != null || document.getElementById('txtAddress2') != undefined) {
        document.getElementById("txtAddress2").value = response[0].Line2.toUpperCase();
    }
    if (document.getElementById('txtTownCity') != null || document.getElementById('txtTownCity') != undefined) {
        document.getElementById("txtTownCity").value = response[0].PostTown.toUpperCase();
    }
    if (document.getElementById('ddlSampleCounty') != null || document.getElementById('ddlSampleCounty') != undefined) {
        SetDropdownValue('ddlSampleCounty', response[0].County.toUpperCase());
    }
    $jqGrid('#divPostCodeSearchPopup').dialog('destroy');
}
//**END****************************************************************************************************************************************

function SetCareHomeAddress(response)
{        
    $("#txtHouseNumber").val('');
    $("#txtHouseName").val('');
    $("#txtAddress1").val('');
    $("#txtAddress2").val('');
    $("#txtTownOrCity").val('');
    $('#ddlCounty option:eq(0)').attr('selected', 'selected');

    
    if (response[0].BuildingName != undefined) {
        HouseName = response[0].BuildingName.toString().toUpperCase();
    }
       
    if ($jqGrid.trim(HouseName).length == 0 && response[0].Company != undefined) {
        HouseName = response[0].Company.toString().toUpperCase();
    }

    $jqGrid('#CareHomeName').val(HouseName);    

    // FOR CAREHOME PAGE, TO COPY HOUSENAME IN CAREHOME NAME BOX
    if (document.getElementById('txtCareHomeName') != null || document.getElementById('txtCareHomeName') != undefined) {
        document.getElementById("txtCareHomeName").value = HouseName;
    }
    //END

    if (document.getElementById('txtHouseNumber') != null || document.getElementById('txtHouseNumber') != undefined) {
        document.getElementById("txtHouseNumber").value = response[0].BuildingNumber.toUpperCase();
    }
    if (document.getElementById('txtHouseName') != null || document.getElementById('txtHouseName') != undefined) {
        document.getElementById("txtHouseName").value = HouseName;
    }
    if (document.getElementById('txtAddress1') != null || document.getElementById('txtAddress1') != undefined) {
        document.getElementById("txtAddress1").value = response[0].Line1.toUpperCase();
    }
    if (document.getElementById('txtAddress2') != null || document.getElementById('txtAddress2') != undefined) {
        document.getElementById("txtAddress2").value = response[0].Line2.toUpperCase();
    }
    if (document.getElementById('txtTownOrCity') != null || document.getElementById('txtTownOrCity') != undefined) {
        document.getElementById("txtTownOrCity").value = response[0].PostTown.toUpperCase();
    }

    if (document.getElementById('ddlCounty') != null || document.getElementById('ddlCounty') != undefined) {
        SetMatchedValueAsSelected('ddlCounty', response[0].County.toUpperCase());
    }

    $jqGrid('#divPostCodeSearchPopup').dialog('destroy');
}

function SetCorrespondenceAddress(response) {    
    ClearCorrespondenceAddress(); 
    if (response[0].BuildingName != undefined) {
        HouseName = response[0].BuildingName.toString().toUpperCase();
    }
    $jqGrid('#CorrespondenceCareHomeName').val(HouseName);      
    document.getElementById("CorrespondenceAddress1").value = response[0].Line1;
    document.getElementById("CorrespondenceAddress2").value = response[0].Line2;
    document.getElementById("CorrespondenceTownOrCity").value = response[0].PostTown;
    SetMatchedValueAsSelected('ddlCorrespondenceCounty', response[0].County);
    document.getElementById("CorrespondencePostCode").value = response[0].Postcode;
    $jqGrid('#divPostCodeSearchPopup').dialog('destroy');
}

function SetPatientAddress(response)
{    
    $("#HouseNumber").val('');
    $("#HouseName").val('');
    $("#Address1").val('');
    $("#Address2").val('');
    $("#TownOrCity").val('');
    $('#ddlCounty option:eq(0)').attr('selected', 'selected');

   
    if (response[0].BuildingName != undefined) {
        HouseName = response[0].BuildingName.toString().toUpperCase();
    }

    if ($jqGrid.trim(HouseName).length == 0 && response[0].Company != undefined) {
        HouseName = response[0].Company.toString().toUpperCase();
    }


    $jqGrid('#CareHomeName').val(HouseName);

    if (document.getElementById('HouseNumber') != null || document.getElementById('HouseNumber') != undefined) {
        document.getElementById("HouseNumber").value = response[0].BuildingNumber.toUpperCase();
    }
    if (document.getElementById('HouseName') != null || document.getElementById('HouseName') != undefined) {
        document.getElementById("HouseName").value = HouseName;
    }
    if (document.getElementById('Address1') != null || document.getElementById('Address1') != undefined) {
        document.getElementById("Address1").value = response[0].Line1.toUpperCase();
    }
    if (document.getElementById('Address2') != null || document.getElementById('Address2') != undefined) {
        document.getElementById("Address2").value = response[0].Line2.toUpperCase();
    }
    if (document.getElementById('TownOrCity') != null || document.getElementById('TownOrCity') != undefined) {
        document.getElementById("TownOrCity").value = response[0].PostTown.toUpperCase();
    }

    if (document.getElementById('ddlCounty') != null || document.getElementById('ddlCounty') != undefined) {
        SetMatchedValueAsSelected('ddlCounty', response[0].County.toUpperCase());
    }
    CopyDeliveryAddress();
    $jqGrid('#divPostCodeSearchPopup').dialog('destroy');
}