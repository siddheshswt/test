﻿$jqGrid = jQuery.noConflict();
var jsfilename = "ProductList.js";
$jqGrid(document).ready(function () {

    $jqGrid("#btnSearch").click(function () {
        loadProductdetails();
    });

    $jqGrid("#txtsearchproduct").keypress(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13) {
            loadProductdetails();
            return false;
        }
    });

    function loadProductdetails() {
        var searchText = $jqGrid('#txtsearchproduct').val();
        if (searchText == "")
            searchText = "blank";
        $jqGrid.ajax({
            url: '/Search/DisplayProductDetails',
            type: 'POST',
            cache: false,
            data: {searchText: searchText},
            async: false,
            success: function (data) {                
                $jqGrid("#jqProductList").jqGrid('clearGridData');
                $jqGrid("#jqProductList").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
            },
            error: function (x, e) { //checking if response is null while searching text for product
                if (x.statusText == "OK") {
                    $jqGrid("#jqProductList").jqGrid('clearGridData');
                    $jqGrid("#jqProductList").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
                }
                else {
                    $jqGrid("#jqProductList").jqGrid('clearGridData');
                    $jqGrid("#jqProductList").jqGrid('setGridParam', { datatype: 'local', data: data }).trigger('reloadGrid');
                    HandleAjaxError(x, e, jsfilename, "btnSearch");
                }
            }

        });//end ajax call
    }

});

function showPopupProduct(id, selectedCustomer) {    
    window.gridid = "";
    if (id == 'btnAddProduct') {        
        selectedCustomer = $jqGrid('#ddlCustomer option:selected').val();
        if ($jqGrid('#ddlPatientType option:selected').index() == 0) {
            $jqGrid('#divPatientTypeNotSelected').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralOk").val().toString(),
                    "id": "hdnGeneralOk",
                    click: function () {
                        $jqGrid('#divPatientTypeNotSelected').dialog('close');
                        return false;
                    },
                }]               
            });
            $jqGrid('#divPatientTypeNotSelected').dialog('open');
            return false;
        }
        window.gridid = "jqPatientPrescription";
        $jqGrid("#hdnFromAddProduct").val("1");
    }
    else if (id == 'btnAddCommunityOrderProdcuts') {
        window.gridid = "jqCommunityOrders";
    }
    else if (id == 'btnAddOneOffProducts') {
        window.gridid = "jqOneOffOrders";
    }
    if ($jqGrid('#txtsearchproduct').val() != "" && $jqGrid('#txtsearchproduct').val() != undefined) {
        $jqGrid('#txtsearchproduct').val('');
    }
    $jqGrid('#divProductList').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: "Product List",
        close: function (event, ui) { ClosePopup('divProductList'); }
    });
    $jqGrid('#divProductList').dialog('open');
    $jqGrid.ajax({
        url: '/Patient/ProductDetails',
        type: 'GET',
        data: { selectedCustomer: selectedCustomer },
        cache: false,
        async: false,
        success: function (responseData) {
            $jqGrid("#jqProductList").jqGrid('clearGridData');
            $jqGrid("#jqProductList").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
        }
    });
}//end function



