﻿$jqGrid = jQuery.noConflict();
$jqGrid(document).ready(function () {    
    EnablePatientInfoPage();    
    if (window.customerName = "" || window.customerName == undefined) {
        window.patientId = 0;
        window.customerId = 0;
    }
});


function BackToHomepage() {
    $jqGrid.ajax({
        url: '/Patient/GetHomePageFlag',
        type: 'POST',
        dataType: 'Json',
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            window.location.href = data;
        }
    });
}

//This is from designer. Please do not change this.
$jqComm(document).ready(function () {    
    $jqComm(".span_expnd").removeClass("heading-1");
    $jqComm(".span_expnd").addClass("heading-01");    
    $jqComm("#inner-page").expandAll({ trigger: ".heading-01", ref: ".heading-01" });
});


