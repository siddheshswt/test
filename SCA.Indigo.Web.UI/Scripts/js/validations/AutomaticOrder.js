﻿$jqGrid = jQuery.noConflict();

var AutomaticCriteriaObj = new AutomaticOrder();
AutomaticOrder.prototype.Customer = "ddlCustomer";
AutomaticOrder.prototype.RunDate = "txtRunDate";
AutomaticOrder.prototype.CareHomeIds = "txtACCarehome";
AutomaticOrder.prototype.PatientIds = "txtACPatient";
AutomaticOrder.prototype.btnExecute = "btnExecuteAutomaticOrder";
AutomaticOrder.prototype.btnExportToExcel = "btnExportToExcel";
AutomaticOrder.prototype.TestRun = "chkTestRun";
AutomaticOrder.prototype.jqAutomaticOrder = "jqAutomaticOrder";
AutomaticOrder.prototype.IdSeperator = ";";
AutomaticOrder.prototype.GeneralYes = "Yes";
AutomaticOrder.prototype.GeneralNo = "No";
AutomaticOrder.prototype.classBtnDisable = "btn_disable";

//hidden variables
AutomaticOrder.prototype.hdnObjectTypeCareHome = "hdnObjectTypeCareHome";
AutomaticOrder.prototype.hdnObjectTypePatient = "hdnObjectTypePatient";
AutomaticOrder.prototype.hdnObjectTypeCustomer = "hdnObjectTypeCustomer";
AutomaticOrder.prototype.hdnGeneralOk = "hdnGeneralOk";
AutomaticOrder.prototype.OrderController = "hdnOrderController";
AutomaticOrder.prototype.ActionGenerateAutomaticCriteria = "hdnActionGenerateAutomaticCriteria";
AutomaticOrder.prototype.ActionAutomaticOrderCriteriaExportToExcel = "hdnActionAutomaticCriteriaExportToExcel";
AutomaticOrder.prototype.ResultAutomaticOrderData = [];


//Div's
AutomaticOrder.prototype.divValidationSelectCustomer = "divValidationSelectCustomer";
AutomaticOrder.prototype.divValidationSelectRunDate = "divValidationSelectRunDate";

AutomaticOrder.prototype.divValidationConfirmOrderCreation = "divValidationConfirmOrderCreation";

//Variable's
var AutomaticOrderGridData = [];

// Form declaration
function AutomaticOrder() { }

AutomaticOrder.prototype.GetAutomaticOrderData = function (viewModel) {
    $.ajax({
        url: '/' + $jqGrid("#" + AutomaticCriteriaObj.ActionGenerateAutomaticCriteria).val(),
        type: 'POST',
        async: false,
        contentType: "application/json",
        dataType: 'json',
        cache: false,
        data: JSON.stringify({
            model: viewModel
        }),
        success: function (results) {
            AutomaticCriteriaObj.ResultAutomaticOrderData = results;
        },
        error: function () {
        }
    });//ajax
}

AutomaticOrder.prototype.ReturnAutomaticOrderData = function () {
    var patientId = [];
    var carehomeId = [];
    var selectedData = [];

    //selection criteria - customer/carehome/patient
    //TODO : to be removed
    var carehomeType = $("#" + AutomaticCriteriaObj.hdnObjectTypeCareHome).val();
    var patientType = $("#" + AutomaticCriteriaObj.hdnObjectTypePatient).val();
    var customerType = $("#" + AutomaticCriteriaObj.hdnObjectTypeCustomer).val();
    var isOnlyCustomerChecked = false;

    //selected customer
    var customerId = $jqGrid("#" + AutomaticCriteriaObj.Customer).multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    //customer selection validation
    if (customerId.length < 1) {
        $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectCustomer).dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            resizable: false,
            cache: false,
            closeOnEscape: false,
            buttons: [{
                text: $jqGrid("#" + AutomaticCriteriaObj.hdnGeneralOk).val().toString(),
                "id": AutomaticCriteriaObj.hdnGeneralOk,
                click: function () {
                    $jqGrid(this).dialog("close");
                },
            }]
        });
        $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectCustomer).dialog('open');
        return false;
    }

        //run date validation

    else if ($jqGrid("#" + AutomaticCriteriaObj.RunDate).val() == "") {
        $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectRunDate).dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            resizable: false,
            cache: false,
            closeOnEscape: false,
            buttons: [{
                text: $jqGrid("#" + AutomaticCriteriaObj.hdnGeneralOk).val().toString(),
                "id": AutomaticCriteriaObj.hdnGeneralOk,
                click: function () {
                    $jqGrid(this).dialog("close");
                },
            }]
        });
        $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectRunDate).dialog('open');
        return false;
    }
        //carehome and patient validation
    else if ($jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).val() == "" && $jqGrid("#" + AutomaticCriteriaObj.PatientIds).val() == "") {
        isOnlyCustomerChecked = true;
    }



    var customers = $jqGrid("#" + AutomaticCriteriaObj.Customer).multiselect("getChecked").map(function () {
        return parseFloat(this.value);
    }).get();

    var careHomeIds = $jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).val();
    var patientIds = $jqGrid("#" + AutomaticCriteriaObj.PatientIds).val();
    var runDate = $jqGrid("#" + AutomaticCriteriaObj.RunDate).val();
    var isTestRun = $jqGrid("#" + AutomaticCriteriaObj.TestRun).is(":checked");

    var automaticOrderCriteriaObj = {
        "Customers": customers,
        "CareHomeIds": careHomeIds,
        "PatientIds": patientIds,
        "IsTestRun": isTestRun,
        "Rundate": runDate,
        "IsOnlyCustomerChecked": isOnlyCustomerChecked
    };


    //confirm for creating the order        
    if (!isTestRun) {
        $jqGrid("#" + AutomaticCriteriaObj.divValidationConfirmOrderCreation).dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            resizable: false,
            closeOnEscape: false,
            buttons: [{
                text: AutomaticCriteriaObj.GeneralYes,
                "id": AutomaticCriteriaObj.GeneralYes,
                click: function () {
                    $jqGrid(this).dialog("close");
                    AutomaticCriteriaObj.GetAutomaticOrderData(automaticOrderCriteriaObj);
                },
            },
            {
                text: AutomaticCriteriaObj.GeneralNo,
                "id": AutomaticCriteriaObj.GeneralNo,
                click: function () {
                    $jqGrid(this).dialog("close");
                },
            }
            ]
        });
        $jqGrid("#" + AutomaticCriteriaObj.divValidationConfirmOrderCreation).dialog('open');
        return false;
    }
    else {
        AutomaticCriteriaObj.GetAutomaticOrderData(automaticOrderCriteriaObj);
    }
}

$jqGrid(document).ready(function () {
    var holidayDaysList = [];
    $jqComm("#" + AutomaticCriteriaObj.Customer).multiselect({ noneSelectedText: 'Customer' });

    var holidayDaysList = GetHolidayList();

    //Run Date validation
    $jqGrid("#" + AutomaticCriteriaObj.RunDate).datepicker({
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        minDate: 0,
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        showOn: 'both',
        beforeShowDay: function (dt) {
            if (holidayDaysList.length > 0) {
                var hindex = $jqGrid.inArray(ConvertDateToMMDDYYYY(dt), holidayDaysList);
            }
            if (hindex > -1)
                return [false, '', ''];
            else
                return [dt.getDay() == 0 || dt.getDay() == 6 ? false : true];
        }
    });

    $jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).on('keyup', function () {
        if ($jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).val() != '') {
            $jqGrid("#" + AutomaticCriteriaObj.PatientIds).addClass(AutomaticCriteriaObj.classBtnDisable);
            $jqGrid("#" + AutomaticCriteriaObj.PatientIds).attr("disabled", "disabled");
        }
        else {
            $jqGrid("#" + AutomaticCriteriaObj.PatientIds).removeAttr("disabled");
            $jqGrid('#' + AutomaticCriteriaObj.PatientIds + '').removeClass(AutomaticCriteriaObj.classBtnDisable);
        }
    });

    $jqGrid("#" + AutomaticCriteriaObj.PatientIds).on('keyup', function () {
        if ($jqGrid("#" + AutomaticCriteriaObj.PatientIds).val() != '') {
            $jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).addClass(AutomaticCriteriaObj.classBtnDisable);
            $jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).attr("disabled", "disabled");
        }
        else {
            $jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).removeAttr("disabled");
            $jqGrid('#' + AutomaticCriteriaObj.CareHomeIds).removeClass(AutomaticCriteriaObj.classBtnDisable);
        }
    });

    $jqGrid("#" + AutomaticCriteriaObj.btnExecute).on('click', function () {        
        $jqGrid("#" + AutomaticCriteriaObj.btnExecute).addClass('btn_disable');
        $jqGrid("#data_loader").show();              
        var patientId = [];
        var carehomeId = [];
        var selectedData = [];

        //selection criteria - customer/carehome/patient
        //TODO : to be removed
        var carehomeType = $("#" + AutomaticCriteriaObj.hdnObjectTypeCareHome).val();
        var patientType = $("#" + AutomaticCriteriaObj.hdnObjectTypePatient).val();
        var customerType = $("#" + AutomaticCriteriaObj.hdnObjectTypeCustomer).val();
        var isOnlyCustomerChecked = false;

        //selected customer
        var customerId = $jqGrid("#" + AutomaticCriteriaObj.Customer).multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        //customer selection validation
        if (customerId.length < 1) {
            $jqGrid("#" + AutomaticCriteriaObj.btnExecute).removeClass('btn_disable');
            $jqGrid("#data_loader").hide();            
            $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectCustomer).dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                resizable: false,
                closeOnEscape: false,
                buttons: [{
                    text: $jqGrid("#" + AutomaticCriteriaObj.hdnGeneralOk).val().toString(),
                    "id": AutomaticCriteriaObj.hdnGeneralOk,
                    click: function () {
                        $jqGrid(this).dialog("close");
                    },
                }]
            });
            $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectCustomer).dialog('open');
            return false;
        }

            //run date validation

        else if ($jqGrid("#" + AutomaticCriteriaObj.RunDate).val() == "") {
            $jqGrid("#" + AutomaticCriteriaObj.btnExecute).removeClass('btn_disable');
            $jqGrid("#data_loader").hide();            
            $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectRunDate).dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                resizable: false,
                closeOnEscape: false,
                buttons: [{
                    text: $jqGrid("#" + AutomaticCriteriaObj.hdnGeneralOk).val().toString(),
                    "id": AutomaticCriteriaObj.hdnGeneralOk,
                    click: function () {
                        $jqGrid(this).dialog("close");
                    },
                }]
            });
            $jqGrid("#" + AutomaticCriteriaObj.divValidationSelectRunDate).dialog('open');
            return false;
        }
            //carehome and patient validation
        else if ($jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).val() == "" && $jqGrid("#" + AutomaticCriteriaObj.PatientIds).val() == "") {
            isOnlyCustomerChecked = true;
        }



        var customers = $jqGrid("#" + AutomaticCriteriaObj.Customer).multiselect("getChecked").map(function () {
            return parseFloat(this.value);
        }).get();

        var careHomeIds = $jqGrid("#" + AutomaticCriteriaObj.CareHomeIds).val();
        var patientIds = $jqGrid("#" + AutomaticCriteriaObj.PatientIds).val();
        var runDate = $jqGrid("#" + AutomaticCriteriaObj.RunDate).val();
        var isTestRun = $jqGrid("#" + AutomaticCriteriaObj.TestRun).is(":checked");

        var automaticOrderCriteriaObj = {
            "Customers": customers,
            "CareHomeIds": careHomeIds,
            "PatientIds": patientIds,
            "IsTestRun": isTestRun,
            "Rundate": runDate,
            "IsOnlyCustomerChecked": isOnlyCustomerChecked
        };
        
        //confirm for creating the order        
        if (!isTestRun) {
            $jqGrid("#" + AutomaticCriteriaObj.divValidationConfirmOrderCreation).dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                resizable: false,
                closeOnEscape: false,
                buttons: [{
                    text: AutomaticCriteriaObj.GeneralYes,
                    "id": AutomaticCriteriaObj.GeneralYes,
                    click: function () {
                        $jqGrid(this).dialog("close");                       
                        GenerateAutomaticOrderCriteria(automaticOrderCriteriaObj);
                    },
                },
                {
                    text: AutomaticCriteriaObj.GeneralNo,
                    "id": AutomaticCriteriaObj.GeneralNo,
                    click: function () {
                        $jqGrid(this).dialog("close");
                    },
                }
                ]
            });
            $jqGrid("#" + AutomaticCriteriaObj.divValidationConfirmOrderCreation).dialog('open');
            return false;
        }
        else {
            GenerateAutomaticOrderCriteria(automaticOrderCriteriaObj);
        }        
    });

    function GenerateAutomaticOrderCriteria(viewModel) {        
        $.ajax({
            url: '/' + $jqGrid("#" + AutomaticCriteriaObj.ActionGenerateAutomaticCriteria).val(),
            type: 'POST',
            async: true,
            contentType: "application/json",
            dataType: 'json',
            cache: false,
            data: JSON.stringify({
                model: viewModel
            }),
            success: function (results) {
                $jqGrid("#data_loader").hide();                
                $jqGrid("#" + AutomaticCriteriaObj.btnExecute).removeClass('btn_disable');
                $jqGrid("#" + AutomaticCriteriaObj.jqAutomaticOrder).jqGrid('clearGridData');
                $jqGrid("#" + AutomaticCriteriaObj.jqAutomaticOrder).jqGrid('setGridParam', { datatype: 'local', data: results }).trigger('reloadGrid');                
            },
            error: function () {
                $jqGrid("#data_loader").hide();                
                $jqGrid("#" + AutomaticCriteriaObj.btnExecute).removeClass('btn_disable');
            }
        });//ajax
    }

    $jqGrid("#" + AutomaticCriteriaObj.btnExportToExcel).on('click', function () {
        $jqGrid("#data_loader").show();
        AutomaticOrderGridData = [];
        AutomaticCriteriaObj.ResultAutomaticOrderData = [];
        AutomaticCriteriaObj.ReturnAutomaticOrderData();
        automaticOrderObj = [];
        for (var i = 0; i < AutomaticCriteriaObj.ResultAutomaticOrderData.length; i++) {
            var row = AutomaticCriteriaObj.ResultAutomaticOrderData[i];
            var automaticOrderObj = {
                SAPCustomerId: row["SAPCustomerId"],
                SAPPatientId: row["SAPPatientId"],
                PatientType: row["PatientType"],
                SAPCareHomeId: row["SAPCareHomeId"],
                OrderId: row["OrderId"],
                DeliveryDate: row["DeliveryDate"],
                Status: row["Status"],
                Message: row["Message"]
            }
            AutomaticOrderGridData.push(automaticOrderObj);
        }        
        if (AutomaticCriteriaObj.ResultAutomaticOrderData.length < 1) {            
            $jqGrid("#data_loader").hide();            
            return false;
        }
        $.ajax({
            url: $jqGrid("#" + AutomaticCriteriaObj.ActionAutomaticOrderCriteriaExportToExcel).val(),
            type: 'POST',
            async: false,
            contentType: "application/json",
            dataType: 'json',
            cache: false,
            data: JSON.stringify({ gridData: AutomaticOrderGridData }),
            success: function (results) {                
                $jqGrid("#data_loader").hide();                
                window.open(results);
            },
            error: function () {                
                $jqGrid("#data_loader").hide();                
            }
        });//ajax
    });

    // Check for ViewOnly 
    CommonScriptObj.DisableFormControls('frm_AutomaticOrder');


});//ready
