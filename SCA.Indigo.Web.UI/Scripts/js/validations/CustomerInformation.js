﻿$jqGrid = jQuery.noConflict();
var CustomerInformationObj = new CustomerInformation();

CustomerInformation.prototype.MessageDiv = "divCustomerInformationMessage";
CustomerInformation.prototype.CustomerUniqueIdRequiredMessage = "hdnCustomerUniqueIdRequire";
CustomerInformation.prototype.CustomerUpdatedSuccessMessage = "hdnCustomerUpdatedSuccess";
CustomerInformation.prototype.CustomerInvalidEmailAddressMessage = "hdnreserrormsgEmailAddress";
CustomerInformation.prototype.CustomerEmailAddress = "txtEmailAddress";
CustomerInformation.prototype.CustomerButtonSave = "btnSaveCustomerInformation";
CustomerInformation.prototype.CustomerButtonReset = "btnResetCustomerInformation";
CustomerInformation.prototype.CustomerNameDropDown = "ddlCustomerInfomation";
CustomerInformation.prototype.CustomerButtonContactInfo = "btnContactInfo";
CustomerInformation.prototype.CustomerContactType = "hdnCustomerContactType";
CustomerInformation.prototype.CustomerUserRequireToRemove = "hdnUserRequireToRemove";
CustomerInformation.prototype.CustomerInvalidStartDateMessage = "hdnInvalidStartDate";
CustomerInformation.prototype.CustomerUserExits = "hdnMessageExistUser";
CustomerInformation.prototype.CustomerCurrentId = "hdnCurrentCustomerId";
CustomerInformation.prototype.CustomerInvalidStatusMessage = "hdnInvalidStatusMessage";
CustomerInformation.prototype.CustomerActivePatientsMessage = "hdnActivePatientsMessage";
CustomerInformation.prototype.CustomerBlockedStatus = "20123";
CustomerInformation.prototype.GeneralYes = "hdnGeneralYes";
CustomerInformation.prototype.GeneralNo = "hdnGeneralNo";
CustomerInformation.prototype.BtnCustomerBack = "btnCustomerBack";
CustomerInformation.prototype.ShowCustomerViewAlert = "hdnShowCustomerViewAlert";



CustomerInformation.prototype.ContactInfoAccess = "";
CustomerInformation.prototype.AttachemntAccess = "";
CustomerInformation.prototype.NoAccessMessage = "No Access";

function CustomerInformation() { }

$jqGrid(document).ready(function () {
    // Load the Date Picker
    $jqGrid('.CalenderImg').datepicker({
        yearRange: "-100:+100",
        dateFormat: 'dd/mm/yy',
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'both',
        firstDay: 1
    })
       .on('change', function (ev) {
           val = $(this).val();
           try {
               if ($jqGrid.datepicker.parseDate('dd/mm/yy', val)) {
                   $jqGrid("#lblDateParse").text('');
               }
           }
           catch (err) {
               $jqGrid("#lblDateParse").text($jqGrid("#" + CustomerInformationObj.CustomerInvalidStartDateMessage).val());
           }
       });

    if ($("#" + CustomerInformationObj.ShowCustomerViewAlert).val() == "True") {
        $jqGrid('#divViewCustomerInformationError').show();
    }

    $jqGrid("#" + CustomerInformationObj.CustomerButtonSave).click(function () {

        CustomerInformationObj.SaveCustomerInformation();
        return false;
    });

    $jqGrid("#" + CustomerInformationObj.CustomerButtonReset).click(function () {

        CustomerInformationObj.ResetCustomerInformation();
        return false;
    });

    $jqGrid("#" + CustomerInformationObj.CustomerNameDropDown).change(function () {
        CustomerInformationObj.GetCustomerInformationById();
    });

    $jqGrid("#" + CustomerInformationObj.CustomerButtonContactInfo).click(function () {
        CustomerInformationObj.OpenContactInfoPopUp();
        return false;
    });

    $jqGrid("#" + CustomerInformationObj.BtnCustomerBack).click(function () {
        CustomerInformationObj.BackButtonClicked();
    });

    $jqGrid("#btnRemovePrescriptionUsers").click(function () {
        CustomerInformationObj.RemovePrescription();
        return false;
    });

    // Status On Change List
    $jqGrid("#ddlCustomerStatus").change(function () {
        CustomerInformationObj.CheckActivePatientsByCustomer();
        return false;
    });


    // Set Default Customer Id
    // $jqGrid('#ddlCustomerInfomation').val($jqGrid('#ddlCustomerInfomation option').eq(1).val());
    CustomerInformationObj.GetCustomerInformationById();

    // Clear Patient Sesion
    //CommonScriptObj.ClearPatientSession();

    // Get Functional module View Only and Full Rights for Customer Sub Module
    CustomerInformationObj.GetCustomerSubModuleAccess();

    CommonScriptObj.DisableFormControls("frm_CustomerInfo");
});

// Get Customer Sub module Access :Contact Info, Attachment
CustomerInformation.prototype.GetCustomerSubModuleAccess = function () {

    if ($("#" + CustomerInformationObj.ShowCustomerViewAlert).val() == "True") {
        return false;
    }

    $jqGrid.ajax({
        url: '/Customer/GetCustomerSubModuleAccess',
        type: 'GET',
        cache: false,
        async: false,
        success: function (responseData) {
            // Set the Contact Info, Attachment Edit access as per Role.
            if (responseData.ContactInfoAccess.length > 0) {
                CustomerInformationObj.ContactInfoAccess = responseData.ContactInfoAccess;
            }
            else {
                // Disable the Contact Info Button
                $jqGrid("#btnContactInfo").addClass("btn_disable").attr("title", CustomerInformationObj.NoAccessMessage);
            }

            // Chcek for Attachment
            if (responseData.AttachmentAccess.length > 0) {
                CustomerInformationObj.AttachmentAccess = responseData.AttachmentAccess;
            }
            else {
                // disable the Attachment Button
                $jqGrid("#btnAttachment").addClass("btn_disable").attr("title", CustomerInformationObj.NoAccessMessage);
            }
        }
    });
}

CustomerInformation.prototype.CheckActivePatientsByCustomer = function () {
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var statusId = $("#ddlCustomerStatus :selected").val();
    if (statusId == CustomerInformationObj.CustomerBlockedStatus) {
        // Validate the Active Patient By Customer
        $jqGrid.ajax({
            url: '/Customer/CheckActivePatientsByCustomer',
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            data: {
                CustomerId: customerId
            },
            cache: false,
            async: false,
            success: function (responseData) {
                if (responseData) {
                    // Display the Error Message
                    CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerActivePatientsMessage).val(), 'pop_error_ul');

                    // Set the Previous State of the Status Drop Down
                    $jqGrid('#ddlCustomerStatus').val($jqGrid('#hdnCurrentCustomerStatus').val());
                    return false;
                }
            }
        });
        return false;
    }
}

CustomerInformation.prototype.ValidateDateTime = function (dateTimeToValidate) {
    if ($.trim(dateTimeToValidate) == "") {
        return false;
    }
    try {
        if ($jqGrid.datepicker.parseDate('dd/mm/yy', dateTimeToValidate)) {
            $jqGrid("#lblDateParse").text('');
        }
    }
    catch (err) {
    }

    return false;
}

CustomerInformation.prototype.OpenContactInfoPopUp = function () {
    // var customerId = $("#ddlCustomerInfomation :selected").val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    if (customerId == "") {
        //  Display the PopUp
        CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerUniqueIdRequiredMessage).val(), 'pop_error_ul');
        return false;
    }
    else {
        var customerContactType = $jqGrid("#" + CustomerInformationObj.CustomerContactType).val();
        ShowPopupContactInfo(customerContactType);
        return false;
    }
}

// This method only call on the Prescription Grid modification (Add/Update/Remove)
CustomerInformation.prototype.GetPrescriptionApproval = function () {
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    if (customerId == "") {
        customerId = 0;
    }
    $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('clearGridData');
    $jqGrid.ajax({
        url: '/Customer/GetPrescriptionApproval',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        data: {
            CustomerId: customerId
        },
        cache: false,
        async: false,
        success: function (responseData) {
            var prescriotionData = responseData.CustomerBusinessModel.PrescriptionApprovals;
            $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('clearGridData');
            $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('setGridParam', { datatype: 'local', data: prescriotionData }).trigger('reloadGrid');
        }
    });
}

CustomerInformation.prototype.SaveCustomerInformation = function () {
    $jqGrid("#data_loader").show();
    $jqGrid("body").css('overflow', 'hidden');

    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var customerName = $("#txtCustomerName").val();

    if (customerId == "") {
        CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerUniqueIdRequiredMessage).val(), 'pop_error_ul');
        $jqComm("#data_loader").hide();
        $jqComm("body").css('overflow', 'auto');
        return false;
    }

    // Validate the Start Date
    var startDate = $jqGrid.trim($jqGrid("#txtStartDate").val());
    if (startDate != "") {
        var data = $jqGrid("#txtStartDate").val().split("/");
        var invalidDate = "Invalid Date";
        var fromYear = 1900;
        if (data.length == 3) {
            var day = data[0];
            var month = data[1];
            var year = data[2];
            if (year.length != 4) {
                CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerInvalidStartDateMessage).val(), 'pop_error_ul');
                $jqComm("#data_loader").hide();
                $jqComm("body").css('overflow', 'auto');
                return false;
            }
            else if (parseInt(year) < parseInt(fromYear)) {
                CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerInvalidStartDateMessage).val(), 'pop_error_ul');
                $jqComm("#data_loader").hide();
                $jqComm("body").css('overflow', 'auto');
                return false;
            }
            else
            {
                var newDate = month + "/" + day + "/" + year;
                var parsedDate = new Date(newDate);
                var newMonth = parsedDate.getMonth() + 1;

                if (newMonth != month) {
                    CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerInvalidStartDateMessage).val(), 'pop_error_ul');
                    $jqComm("#data_loader").hide();
                    $jqComm("body").css('overflow', 'auto');
                    return false;
                }
                else if (invalidDate == new Date(newDate)) {
                    CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerInvalidStartDateMessage).val(), 'pop_error_ul');
                    $jqComm("#data_loader").hide();
                    $jqComm("body").css('overflow', 'auto');
                    return false;
                }
                else {
                    if (month.length == 1) {
                        appendZeroToMonth = '0' + month.toString();
                    }
                    else {
                        appendZeroToMonth = month;
                    }
                    if (day.length == 1) {
                        appendZeroToDay = '0' + day.toString();
                    }
                    else {
                        appendZeroToDay = day;
                    }

                    startDate = appendZeroToDay + "/" + appendZeroToMonth + "/" + year;
                }
            }
            
        }
    }
    else {
        CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerInvalidStartDateMessage).val(), 'pop_error_ul');
        $jqComm("#data_loader").hide();
        $jqComm("body").css('overflow', 'auto');
        return false;
    }
    
    var status = $("#ddlCustomerStatus :selected").val();
    if (status == "") {
        CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerInvalidStatusMessage).val(), 'pop_error_ul');
        $jqComm("#data_loader").hide();
        $jqComm("body").css('overflow', 'auto');
        return false;
    }

    $.ajax({
        url: '/Customer/SaveCustomerDetails',
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        cache: false,
        data: JSON.stringify({ CustomerId: customerId, Status: status, StartDate: startDate }),
        success: function (data) {
            CustomerInformationObj.SaveUsersToCustomer();
            return false;
        }
    });

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
    return false;
}

CustomerInformation.prototype.ResetCustomerInformation = function () {

    // Reset the Prescription Grid Data
    var resetMessage = $jqGrid("#hdnCustomerInformationResetMessage").val();
    $jqGrid('#divCustomerInformationMessage').html(resetMessage);

    $jqGrid('#divCustomerInformationMessage').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + CustomerInformationObj.GeneralYes).val().toString(),
            "id": CustomerInformationObj.GeneralYes,
            click: function () {
                // Clear the Form Data
                CustomerInformationObj.CleanForm();
                $jqGrid('#divCustomerInformationMessage').dialog('close');
                return false;
            },
        }, {
            text: $jqGrid("#" + CustomerInformationObj.GeneralNo).val().toString(),
            "id": CustomerInformationObj.GeneralNo,
            click: function () {
                $jqGrid('#divCustomerInformationMessage').dialog('close');
                return false;
            },
        }]
    });
    $jqGrid('#divCustomerInformationMessage').dialog('open');

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
    return false;
}

CustomerInformation.prototype.CleanForm = function () {
    $jqGrid("#cboxPrescription").prop("checked", false);
    CustomerInformationObj.GetCustomerInformationById();

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
    return false;
}

CustomerInformation.prototype.GetCustomerInformationById = function () {
    if ($("#" + CustomerInformationObj.ShowCustomerViewAlert).val() == "True") {
        return false;
    }
    $jqGrid("#data_loader").show();
    $jqGrid("body").css('overflow', 'hidden');

    // Get Customer Id
    //var customerId = $("#ddlCustomerInfomation :selected").val();
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    if (customerId == "") {
        customerId = 0;
        return false;
    }

    $jqGrid.ajax({
        url: '/Customer/GetCustomerDetails',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        data: {
            customerId: customerId
        },
        cache: false,
        async: false,
        success: function (responseData) {
            if (responseData.CustomerBusinessModel != null) {
                var cusotmerDetails = responseData.CustomerBusinessModel;
                //Added this to sho customer name / id in patient type matrix
                $jqGrid("#hdnCustomerName").val(cusotmerDetails.CustomerName);
                $jqGrid("#hdnCustomerId").val(cusotmerDetails.SAPCustomerNumber);

                // Update the Parameters
                $jqGrid("#txtSAPCustomerNumber").val(cusotmerDetails.SAPCustomerNumber);
                $jqGrid("#txtHouseNumber").val(cusotmerDetails.HouseNumber);
                $jqGrid("#txtAddressLine1").val(cusotmerDetails.AddressLine1);
                $jqGrid("#txtAddressLine2").val(cusotmerDetails.AddressLine2);
                $jqGrid("#txtTownCity").val(cusotmerDetails.TownCity);
                $jqGrid("#txtCounty").val(cusotmerDetails.County);
                $jqGrid("#txtCountry").val(cusotmerDetails.Country);
                $jqGrid("#txtTransportCompany").val(cusotmerDetails.TransportCompany);
                //$jqGrid("#txtPhone").val(cusotmerDetails.Phone);
                //$jqGrid("#txtEmailAddress").val(cusotmerDetails.EmailAddress);
                $jqGrid("#ddlCustomerStatus").val(cusotmerDetails.Status);
                $jqGrid("#txtStartDate").val(cusotmerDetails.StartDate);
                $jqGrid("#txtCustomerName").val(cusotmerDetails.CustomerName);
                $jqGrid("#txtCreatedBy").val(cusotmerDetails.CreatedBy);
                $jqGrid("#txtCreatedDate").val(cusotmerDetails.CreatedDate);
                $jqGrid("#txtPostCode").val(cusotmerDetails.PostCode);
                $jqGrid("#hdnCurrentCustomerStatus").val(cusotmerDetails.Status);

                // Validate the Start Date
                CustomerInformationObj.ValidateDateTime(cusotmerDetails.StartDate);

                // Bind the Prescription Approval details
                var prescriotionData = responseData.CustomerBusinessModel.PrescriptionApprovals;
                if (prescriotionData != null) {
                    $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('clearGridData');
                    $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('setGridParam', { datatype: 'local', data: prescriotionData }).trigger('reloadGrid');

                    //Register the Click event for all the Checkbox inside the Prescription Grid
                    CustomerInformationObj.BindPrescriptionGridCheckBox();
                }

            }
        }
    });

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
    return false;
}

CustomerInformation.prototype.SelectAllPrescription = function (obj) {
    $jqGrid('.check').children().prop('checked', obj.checked);
    return false;
}

CustomerInformation.prototype.BindPrescriptionGridCheckBox = function () {
    $jqGrid(".check").click(function () {
        if ($jqGrid(".check :checked").length > 0) {
            var numberOfChecked = $jqGrid(".check :checked").length;
            var totalCheckboxes = $jqGrid(".check :checkbox").length;
            if (numberOfChecked == totalCheckboxes)
                $jqGrid("#cboxPrescription").prop("checked", true);
            else
                $jqGrid("#cboxPrescription").prop("checked", false);
        }
    });
}

function ShowCustomerUserPopup() {
    var customerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    if (customerId == "") {
        CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerUniqueIdRequiredMessage).val(), 'pop_error_ul');
        return false;
    }

    var customerUserDialog = $jqGrid("#divCustomerUserPopup").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: 'Add User'
    });
    customerUserDialog.dialog('open');

    $jqGrid("#jqCustomerUsersResults").jqGrid('clearGridData');
    $jqGrid.ajax({
        url: '/Customer/LoadUserCustomer',
        type: 'GET',
        cache: false,
        contentType: 'application/json',
        dataType: "json",
        data: {
            customerId: customerId
        },
        async: false,
        success: function (responseData) {
            $jqGrid("#jqCustomerUsersResults").jqGrid('clearGridData');
            $jqGrid("#jqCustomerUsersResults").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
            return false;
        }
    });
    return false;
}

CustomerInformation.prototype.RemovePrescription = function () {
    var removeIds = "";
    removeIds = ($(".check :checked").map(function () { return $(this).parents('tr').find(".idxCustomerId").text(); }).get().join(', '));
    if (removeIds == "") {
        CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", $jqGrid("#" + CustomerInformationObj.CustomerUserRequireToRemove).val(), 'pop_error_ul');
        return false;
    }
    var removeMessage = $jqGrid("#hdnMessageRemoveUserCustomer").val();
    $jqGrid('#divCustomerInformationRemoveUser').html(removeMessage);

    $jqGrid('#divCustomerInformationRemoveUser').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + CustomerInformationObj.GeneralYes).val().toString(),
            "id": CustomerInformationObj.GeneralYes,
            click: function () {
                // Temporary Remove from the Grid
                var idsToDelete = ($(".check :checked").closest('tr').map(function () { return $(this).attr("id"); }).get().join(','));
                var ids = idsToDelete.split(",");
                for (var i = 0; i < ids.length; i++) {
                    $jqGrid('#jqCustomerAuthorizedPersonPrescriptionApproval').jqGrid('delRowData', ids[i]);
                }

                $jqGrid('#divCustomerInformationRemoveUser').dialog('close');
                return false;
            },
        }, {
            text: $jqGrid("#" + CustomerInformationObj.GeneralNo).val().toString(),
            "id": CustomerInformationObj.GeneralNo,
            click: function () {
                $jqGrid('#divCustomerInformationRemoveUser').dialog('close');
                return false;
            },
        }]
    });
    $jqGrid('#divCustomerInformationRemoveUser').dialog('open');

    $jqComm("#data_loader").hide();
    $jqComm("body").css('overflow', 'auto');
    return false;
}

CustomerInformation.prototype.AddPrescriptionApprovalGridData = function (userId) {
    // Add new Row into the Grid.
    // Get User Details from User
    $jqGrid.ajax({
        url: '/Customer/GetUserDetailsByUserId',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        data: {
            userToAuthorize: userId
        },
        cache: false,
        async: false,
        success: function (responseData) {
            var userData = responseData.CustomerBusinessModel.PrescriptionApprovals[0];
            if (userData.UserName.length > 0) {

                var datarow = {
                    UserName: userData.UserName, UserId: userData.UserId,
                    AuthorizationPin: userData.AuthorizationPin, FirstName: userData.FirstName,
                    SurName: userData.SurName, JobTitle: userData.JobTitle, PhoneNumber: userData.PhoneNumber,
                    MobileNumber: userData.MobileNumber, EmailAddress: userData.EmailAddress, UserId: userData.UserId, IDXUserCustomerId: userData.IDXUserCustomerId
                };
                var gridrowdata = $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('getRowData');
                $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").jqGrid('addRowData', gridrowdata.length + 1, datarow, "first");
                $jqGrid("#jqCustomerAuthorizedPersonPrescriptionApproval").trigger('reloadGrid');//added to resolved page 1 of 0
            }
        }
    });

    return false;
}

CustomerInformation.prototype.SaveUsersToCustomer = function () {
    // Get all Users from the Grid and Push into the Array.
    var objcustomerId = $jqGrid("#" + CustomerInformationObj.CustomerCurrentId).val();
    var usersToMap = ($(".check").parent().map(function () { return $(this).find(".userIdToMap").text(); }).get().join(', '));
    var customerName = $("#txtCustomerName").val();

    $.ajax({
        url: '/Customer/SavePrescriptionApproval',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        cache: false,
        data: JSON.stringify({ objUserList: usersToMap, customerId: objcustomerId }),
        success: function (data) {
            // Reload Page
            // Display Success Message;
            var displayMessage = $jqGrid("#" + CustomerInformationObj.CustomerUpdatedSuccessMessage).val();
            CommonScriptObj.DisplayErrorMessage("divCustomerInformationMessage", displayMessage.replace("{0}", customerName));
            //$jqGrid('#divCustomerUserPopup').dialog('close');
            //ClosePopup('divCustomerUsers');
        }
    });
}

CustomerInformation.prototype.BackButtonClicked = function () {
    $jqGrid.ajax({
        url: '/Patient/GetHomePageFlag',
        type: 'POST',
        dataType: 'Json',
        async: false,
        cache: false,
        data: {},
        success: function (data) {
            window.location.href = data;
        }
    });
}

CustomerInformation.prototype.IsDate = function (currVal) {
    if (currVal == '')
        return false;

    //Declare Regex 
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for dd/mm/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12) {
        return false;
    }
    else if (dtDay < 1 || dtDay > 31) {
        return false;
    }
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
        return false;
    }

    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    else if (dtYear < 1000)
    {
        return false;
    }

    return true;
}