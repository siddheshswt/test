﻿$jqGrid = jQuery.noConflict();

var jsfilename = "carehome.js";
var CareHomeObj = new CareHomeDetails();
function CareHomeDetails() { };
CareHomeDetails.prototype.ContactInfoAccess = "";
CareHomeDetails.prototype.InteractionAccess = "";
CareHomeDetails.prototype.AttachemntAccess = "";
CareHomeDetails.prototype.NotesAccess = "";
CareHomeDetails.prototype.RecentOrdersAccess = "";
CareHomeDetails.prototype.NoAccessMessage = "No Access";
CareHomeDetails.prototype.AuditLogAccess = "";
CareHomeDetails.prototype.HdnSetCarehomeSession = "hdnGoBackUrl";
CareHomeDetails.prototype.HdnIsCalledFromInteractionMenu = "hdnIsCalledFromInteractionMenu";
var NoCommunicationChecked = false;
var day1 = day2 = day3 = day4 = day5 = 0;
var carehomeOrderType = "";
var careHomeStatus = "";
var isExport = false;

var holidayDaysList = [];
$("#txtPhoneNo").keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isNumeric(charCode))
        return true;
    else
        return false;
});

$("#txtMobileNo").keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isNumeric(charCode))
        return true;
    else
        return false;
});

$("#txtDeliveryFrequency").keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isNumeric(charCode))
        return true;
    else
        return false;
});

//$("#txtRoundId").keypress(function (e) {
//    var charCode = (e.which) ? e.which : e.keyCode;
//    if (isAlphaKey(charCode))
//        return true;
//    else
//        return false;
//});

$jqGrid(document).ready(function () {
    $jqGrid.ajaxSetup({ cache: false }); // This is required for Report Pop up page
    var CareHomeId = $("#hdnCareHomeId").val();
    // copy housne name in carehome name box
    $("#txtHouseName").blur(function (e) {
        $('#txtCareHomeName').val($.trim($("#txtHouseName").val()));
    });
    //
    //Show aleart message for carehome when carehome not selected
    if ($("#hdnShowViewAlert").val() === "False") {
        var CareHomeId = $("#hdnCareHomeId").val();
        if ($jqGrid("#ddlCustomer option:selected").text() != '') {
            var carehomeNameSAPNumber = $jqGrid("#ddlCustomer option:selected").text().split('(');
        }
        SetNddForNewCarehome(null);
        if (CareHomeId > 0) {
            CommonScriptObj.CareHomeId = CareHomeId;
            CommonScriptObj.CareHomeName = $('#txtCareHomeName').val();
            CommonScriptObj.CustomerId = $("#ddlCustomer").val();
            CommonScriptObj.CustomerName = carehomeNameSAPNumber[0];
            CommonScriptObj.SAPCustomerNumber = carehomeNameSAPNumber[1].split(')')[0];
            CommonScriptObj.SAPCareHomeNumber = $("#lblCareHomeId").text();
            FillPatientDetails(CareHomeId);
            DisableControls();
            EnablePopupButtons();

        }
        else {
            DisablePopupButtons();
        }
        //Call the common Set NDD method implemented on Order.js  

        //Under discussion
        SetSCAUserTypeValidation()

        // Clear Patient Sesion
        // CommonScriptObj.ClearPatientSession();
    }
    else {
        $jqGrid('#divViewCarehomeError').show();
    }

    $jqGrid('#chkShowRemoved').click(function () {
        ShowHideRemovedStatusPatients();
    });

    $jqGrid("#ddlCommunicationFormat").multiselect({
        noneSelectedText: $("#hdnresddlCommunicationFormatDefaultValue").val(),
        click: function (event, ui) {
            SetNoCommunicationValidation(ui, "ddlCommunicationFormat");
        }
    });

    // Check for ViewOnly 
    //Element: - Main Div of the Form
    CommonScriptObj.DisableFormControls("div_Carehome");

    // Get Functional module View Only and Full Rights for Carehome Sub Module
    GetCareHomeSubModuleAccess();

    setTimeout(function () {
        if ($jqGrid("#hdnIsCalledFromHomePage").val().toLowerCase() == "true" || $jqGrid("#hdnIsCalledFromSearchPage").val().toLowerCase() == "true" || $jqGrid("#" + CareHomeObj.HdnIsCalledFromInteractionMenu).val().toLowerCase() == "true") {
            $jqGrid('#btnCareHomeCancel').removeClass("btn_disable");
            $jqGrid('#btnCareHomeCancel').removeAttr("disabled");
        }
        else {
            $jqGrid('#btnCareHomeCancel').addClass("btn_disable");
            $jqGrid('#btnCareHomeCancel').attr('disabled', 'disabled');
        }

    }, 500);

    var SelectedArray = [];
    var strCommPreference = $jqGrid("#CarehomeCommunicationPreferences").val();
    strCommPreference = strCommPreference.split(",");
    $jqGrid("#ddlCommunicationFormat").val(strCommPreference);
    $jqGrid("#ddlCommunicationFormat").multiselect('refresh');
    $jqGrid('input[type="radio"], .ui-corner-all input[type="checkbox"]').ezMark();
    carehomeOrderType = $("#ddlOrderType").val();
    careHomeStatus = $("#ddlStatus").val();

    LoadJQGridAlertNotes();//Load JQGrid for jqAlertNotes table
    
    if ($("#hdnSessionDoNotShow").val() == "false") {
        var noteType = $("#hdnNoteType").val();
        var noteTypeId = $("#hdnCareHomeId").val();
        LoadAlertNotes(noteType, noteTypeId);
    }
});

function ShowHideRemovedStatusPatients() {
    var grid = $jqGrid("#jqCareHomePatientDetails"), ids = grid.jqGrid('getDataIDs'), i, l = ids.length;
    if ($jqGrid("#chkShowRemoved").is(':checked')) {
        for (var i = 0; i < l; i++) {
            $jqGrid("#jqCareHomePatientDetails #" + (ids[i])).show();
        }
    }
    else {
        for (var i = 0; i < l; i++) {
            var isPatientStatusRemoved = $jqGrid('#jqCareHomePatientDetails').jqGrid('getCell', ids[i], 'PatientStatus');
            if (isPatientStatusRemoved == $jqGrid('#hdnPatientStatusRemoved').val()) {
                $jqGrid("#jqCareHomePatientDetails #" + ids[i]).hide();
            }
        }
    }
}

// Get Carehome Sub module Access :Contact Info, Recent Orders, Notes, Attachment & Interaction
function GetCareHomeSubModuleAccess() {
    // No Need to check for Add New Carehome 
    $jqGrid.ajax({
        url: '/Carehome/GetCareHomeSubModuleAccess',
        type: 'GET',
        cache: false,
        async: false,
        success: function (responseData) {
            // Set the Interaction, Contact Info, Attachment, Notes and Recent Orders Edit access as per Role.
            if (responseData.ContactInfoAccess.length > 0) {
                CareHomeObj.ContactInfoAccess = responseData.ContactInfoAccess;
            }
            else {
                // Disable the Contact Info Button
                $jqGrid("#btnContactInfo").addClass("btn_disable").attr("title", CareHomeObj.NoAccessMessage);
            }

            //Check for Interaction
            if (responseData.InteractionAccess.length > 0) {
                CareHomeObj.InteractionAccess = responseData.InteractionAccess;
                $jqGrid("#hdnuserType").val(responseData.UserType);
            }
            else {
                // disable the Interaction Button
                $jqGrid("#btnInteractionsummary").addClass("btn_disable").attr("title", CareHomeObj.NoAccessMessage);
            }

            // Chcek for Attachment
            if (responseData.AttachmentAccess.length > 0) {
                CareHomeObj.AttachmentAccess = responseData.AttachmentAccess;
            }
            else {
                // disable the Attachment Button
                $jqGrid("#btnAttachment").addClass("btn_disable").attr("title", CareHomeObj.NoAccessMessage);
            }

            // Chcek for Notes
            if (responseData.NotesAccess.length > 0) {
                CareHomeObj.NotesAccess = responseData.NotesAccess;
            }
            else {
                // disable the Notes Button
                $jqGrid("#btnNote").addClass("btn_disable").attr("title", CareHomeObj.NoAccessMessage);
            }

            // Chcek for Recent Orders 
            if (responseData.RecentOrdersAccess.length > 0) {
                CareHomeObj.RecentOrdersAccess = responseData.RecentOrdersAccess;
            }
            else {
                // disable the Recent Orders Button
                $jqGrid("#btnRecentOrder").addClass("btn_disable").attr("title", CareHomeObj.NoAccessMessage);
            }

            // Chcek for AuditLog 
            if (responseData.AuditLogAccess.length > 0) {
                CareHomeObj.AuditLogAccess = responseData.AuditLogAccess;
            }
            else {
                // disable the Recent Orders Button
                $jqGrid("#btnCareHomeAuditLog").addClass("btn_disable").attr("title", CareHomeObj.NoAccessMessage);
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "GetCareHomeSubModuleAccess");
        }
    });
}

function SetSCAUserTypeValidation() {

    if ($("#hdnUserType").val() != "SCA") {
        $('#txtRoundId').attr('disabled', 'disabled');
        $('#txtRoundId').addClass('bg_ddd');
    }
}

$jqGrid("#ddlCustomer").on('change', function () {
    var CustomerId = $(this).val();
    var CareHomeId = $("#hdnCareHomeId").val();
    if (CustomerId > 0) {
        $("#lblCustomerId").text(CustomerId);
        $("#hdnCustomerId").val(CustomerId);

        if (CareHomeId < 1) {
            setCustomerParameter(CustomerId);
        }
        $('#txtNDD').removeClass("bg_ddd");
        $("#txtNDD").removeAttr('readonly');
        $('#txtNDD').removeAttr('disabled');
        $(".ui - datepicker - trigger").removeClass('btn_disable');
    }
    else {

        $("#lblCustomerId").text("");
        $("#hdnCustomerId").val("");
        $(".ui-datepicker-trigger").addClass('btn_disable');
        $('#txtNDD').addClass("bg_ddd");
        $("#txtNDD").attr('readonly', 'true');
        $('#txtNDD').attr('disabled', 'disabled');
    }
});

$jqGrid("#ddlOrderType").on('change', function () {
    var hasAnyActiveOrderForOrderType = $("#hdnHasAnyActiveOrderForOrderType").val();
    if (carehomeOrderType == $("#hdnCarehomeOrderTypeAutomatic").val() && $jqGrid("#ddlOrderType option:selected").index() != 0 && $jqGrid("#ddlOrderType").val() != carehomeOrderType) {
        $jqGrid("#divCarehomeOrderTypeAutomatic").dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralYes").val().toString(),
                "id": "hdnGeneralYes",
                click: function () {
                    $jqGrid('#divCarehomeOrderTypeAutomatic').dialog('close');
                },
            }, {
                text: $jqGrid("#hdnGeneralNo").val().toString(),
                "id": "hdnGeneralNo",
                click: function () {
                    $jqGrid('#divCarehomeOrderTypeAutomatic').dialog('close');
                    $jqGrid("#ddlOrderType").val(carehomeOrderType);
                },
            }]
        });
        $jqGrid('#divCarehomeOrderTypeAutomatic').dialog('open');
    }
    else if (hasAnyActiveOrderForOrderType.toUpperCase() == "TRUE") {
        $jqGrid("#divHasActiveOrderCarehomeOrderType").dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralOk").val().toString(),
                "id": "hdnGeneralOk",
                click: function () {
                    $jqGrid('#divHasActiveOrderCarehomeOrderType').dialog('close');
                    $jqGrid("#ddlOrderType").val(carehomeOrderType);
                },
            }]
        });
        $jqGrid('#divHasActiveOrderCarehomeOrderType').dialog('open');
    }//if
});

$jqGrid("#ddlStatus").on('change', function () {
    var hasAnyActiveOrderForOrderType = $("#hdnHasAnyActiveOrderForOrderType").val();
    if (hasAnyActiveOrderForOrderType.toUpperCase() == "TRUE") {
        $jqGrid("#divHasActiveOrderForStatusChange").dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralOk").val().toString(),
                "id": "hdnGeneralOk",
                click: function () {
                    $jqGrid('#divHasActiveOrderForStatusChange').dialog('close');
                    $jqGrid("#ddlStatus").val(careHomeStatus);
                },
            }]
        });
        $jqGrid('#divHasActiveOrderForStatusChange').dialog('open');
    }
});

function setCustomerParameter(CustomerId) {
    $jqGrid.ajax({
        url: '/CareHome/SetCustomerParameter',
        type: 'GET',
        cache: false,
        data: { customerid: CustomerId },
        async: false,
        success: function (responseData) {
            //Set The Startdate and End Date for Carehome NDD           
            SetNddForNewCarehome(responseData);
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "setCustomerParameter");
        }
    });
}

function SetNddForNewCarehome(responseData) {
    var NddStartDate = new Date();
    var NddEndDate = new Date();
    if (responseData != undefined || responseData != null) {
        NddStartDate = ConvertDateFormat(responseData.NddStartDate);
        NddEndDate = ConvertDateFormat(responseData.NddEndDate);
        GetCarehomeNDDRelatedDetails($("#ddlCustomer").val(), 0);
        holidayDaysList = GetHolidayListForCarehome();
    }
    else if ($("#hdnNddStartDate").val() != null || $("#hdnNddStartDate").val() == "") {
        NddStartDate = ConvertDateFormat($("#hdnNddStartDate").val());
        NddEndDate = ConvertDateFormat($("#hdnNddEndDate").val());
        GetCarehomeNDDRelatedDetails($("#ddlCustomer").val(), $("#hdnCareHomeId").val());
        holidayDaysList = GetHolidayListForCarehome();
    }
    $jqGrid("#txtNDD").datepicker("destroy");
    $jqGrid('#txtNDD').datepicker({
        yearRange: "-115:+10",
        dateFormat: 'dd/mm/yy',
        buttonImage: '../../Images/calander_icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'button',
        firstDay: 1,
        minDate: NddStartDate,
        maxDate: NddEndDate,
        //beforeShowDay: $jqGrid.datepicker.noWeekends
        beforeShowDay: function (dt) {
            if (holidayDaysList.length > 0) {
                var hindex = $jqGrid.inArray(ConvertDateToMMDDYYYY(dt), holidayDaysList);
                if (hindex > -1)
                    return [false, '', ''];
                else
                    return [dt.getDay() == 0 || dt.getDay() == 6 || dt.getDay() == day1 || dt.getDay() == day2 || dt.getDay() == day3 || dt.getDay() == day4 || dt.getDay() == day5 ? false : true];
            }
            else {
                return [dt.getDay() == 0 || dt.getDay() == 6 || dt.getDay() == day1 || dt.getDay() == day2 || dt.getDay() == day3 || dt.getDay() == day4 || dt.getDay() == day5 ? false : true];
            }
        },
    })
    .on('change', function (ev) {
        val = $(this).val();
        var dateParts = val.split("/");
        var year = dateParts[2];
        var minYear = 1900;
        try {
            if ($jqGrid.datepicker.parseDate('dd/mm/yy', val) && !(year < minYear)) {
                $jqGrid("#lblDateParse").text('');
            }
            else {
                $jqGrid("#lblDateParse").text($jqGrid("#hdnreserrormsgValidNDD").val());
                return false;
            }
        }
        catch (err) {
            $jqGrid("#lblDateParse").text($jqGrid("#hdnreserrormsgValidNDD").val());
        }
    });

}

function FillPatientDetails(CareHomeId) {
    $jqGrid.ajax({
        url: '/CareHome/GetCareHomePatientList',
        type: 'GET',
        cache: false,
        data: { careHomeID: CareHomeId },
        async: false,
        success: function (responseData) {
            $jqGrid("#jqCareHomePatientDetails").jqGrid('clearGridData');
            $jqGrid("#jqCareHomePatientDetails").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "FillPatientDetails");
        }
    });
}

function UndoCareHome() {
    var resetMessage = $jqGrid("#hdnresGeneralUndoMessage").val();
    $jqGrid('#divErrorMessage').html(resetMessage);

    $jqGrid('#divErrorMessage').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#hdnGeneralYes").val().toString(),
            "id": "hdnGeneralYes",
            click: function () {
                // Clear the Form Data
                var carehomeId = $("#hdnCareHomeId").val();
                if (carehomeId > 0) {
                    window.location.href = "/CareHome/GetCareHome/" + carehomeId;
                }
                else {
                    ClearData();
                    $jqGrid('#divErrorMessage').dialog('destroy');
                    return false;
                }
            },
        }, {
            text: $jqGrid("#hdnGeneralNo").val().toString(),
            "id": "hdnGeneralNo",
            click: function () {
                $jqGrid('#divErrorMessage').dialog('destroy');
                return false;
            },
        }]
    });
    $jqGrid('#divErrorMessage').dialog('open');
}

function ClearData() {
    $("#ddlCustomer").val("");
    $("#lblCareHomeId").text("");
    $("#hdnCareHomeId").val();
    $("#lblCustomerId").text("");
    $("#hdnCustomerId").val("");
    $("#txtCareHomeName").val("");
    $("#txtNDD").val("");
    $("#PostCode").val("");
    $("#txtHouseNumber").val("");
    $("#txtHouseName").val("");
    $("#txtAddress1").val("");
    $("#txtAddress2").val("");
    $("#txtTownOrCity").val("");
    $("#ddlCounty").val("");
    $("#txtPhoneNo").val("");
    $("#txtEmailAddress").val("");
    $("#txtRoundId").val("");
    $("#txtDeliveryFrequency").val("");
    $("#ddlOrderType").val("");
    $("#txtPurchaseOrderNo").val("");
    $("#ddlStatus").val("");
    //$("#ddlCarehomeType").val($("#hdnCareHomeType").val());
    $("#ddlCommunicationFormat").val("");
}
function ValidateCareHomeData() {

    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";
    var isErrorOccured = false;
    errorMsg = errorMsgUl;

    if (document.getElementById('ddlCustomer').selectedIndex < 1) {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCustomer").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    if ($.trim($("#txtCareHomeName").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCarehome").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    if ($.trim($("#txtNDD").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgNDD").val() + errorMsgLiclose;
        isErrorOccured = true;


    }
    else {
        var NDD = $.trim($("#txtNDD").val());
        var dateParts = NDD.split("/");
        var year = dateParts[2];
        var minYear = 1900;
        try {
            if ($jqGrid.datepicker.parseDate('dd/mm/yy', NDD) && !(year < minYear)) {
                $jqGrid("#lblDateParse").text('');
            }
            else {
                errorMsg += errorMsgLi + $("#hdnreserrormsgValidNDD").val() + errorMsgLiclose;
                isErrorOccured = true;
            }
        }
        catch (err) {
            errorMsg += errorMsgLi + $("#hdnreserrormsgValidNDD").val() + errorMsgLiclose;
            isErrorOccured = true;
        }
    }

    if ($.trim($("#PostCode").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgPostCode").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    if ($.trim($("#txtAddress1").val()) == "") {

        errorMsg += errorMsgLi + $("#hdnresErrorMessageAddress1").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    if ($.trim($("#txtTownOrCity").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgCity").val() + errorMsgLiclose;
        isErrorOccured = true;
    }
    if ($.trim($("#txtPhoneNo").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsTelephone").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    if ($.trim($("#txtEmailAddress").val()) != "") {
        if (!ValidateEmail($("#txtEmailAddress").val())) {
            errorMsg += errorMsgLi + $("#hdnreserrormsgEmailAddress").val() + errorMsgLiclose;
            isErrorOccured = true;
        }
    }
    if ($.trim($("#txtDeliveryFrequency").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgDeliveryFrequency").val() + errorMsgLiclose;
        isErrorOccured = true;
    }
    else if ($.trim($("#txtDeliveryFrequency").val()) === "0") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgDeliveryFrequencyZero").val() + errorMsgLiclose;
        isErrorOccured = true;
    }
    if (document.getElementById('ddlOrderType').selectedIndex < 1) {

        errorMsg += errorMsgLi + $("#hdnreserrormsgOrderType").val() + errorMsgLiclose;
        isErrorOccured = true;
    }
    if (document.getElementById('ddlStatus').selectedIndex < 1) {

        errorMsg += errorMsgLi + $("#hdnreserrormsgCareHomeStatus").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    if ($.trim($("#txtRound").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgRound").val() + errorMsgLiclose;
        isErrorOccured = true;
    }
    else {
        if ($.trim($("#txtRound").val()).length == 10) {
            var round = $.trim($("#txtRound").val());
            var roundChar = round.slice(0, -2);
            if (roundChar.toUpperCase() != $("#hdnDefaultRoundChar").val()) {
                errorMsg += errorMsgLi + $("#hdnreserrormsgValidRound").val() + errorMsgLiclose;
                isErrorOccured = true;
            }
            else {
                var lastTwo = round.slice(-2);
                if (!$.isNumeric(lastTwo)) {
                    errorMsg += errorMsgLi + $("#hdnreserrormsgValidRound").val() + errorMsgLiclose;
                    isErrorOccured = true;
                }
            }
        }
        else {
            errorMsg += errorMsgLi + $("#hdnreserrormsgValidRound").val() + errorMsgLiclose;
            isErrorOccured = true;
        }
    }
    if ($.trim($("#txtRoundId").val()) == "") {
        errorMsg += errorMsgLi + $("#hdnreserrormsgRoundId").val() + errorMsgLiclose;
        isErrorOccured = true;
    }

    $("#divErrorMessage").html(errorMsg);
    if (isErrorOccured) {
        CommonScriptObj.DisplayErrorMessage("divErrorMessage", errorMsg, 'pop_error_ul');
        //ShowHideError();
        return false;
    }
    else {
        return true;
    }
}

function SaveCareHomeDetails() {
    //Validate Form before submit
    if (ValidateCareHomeData())
        //Create Post Data
    {
        var arrCommunicationFormat = [];
        $($jqGrid("#ddlCommunicationFormat").multiselect('getChecked').map(function (index) { return $(this).attr('value'); })).each(
            function (e, item) {
                arrCommunicationFormat.push(item);
            });
        var county = null;
        if ($('#ddlCounty').val() != $('#hdnresddlCountyDefaultValueForCarehome').val() && document.getElementById('ddlCounty').selectedIndex >= 1) {
            county = $('#ddlCounty').val();
        }
        var objCareHome = {
            "CustomerId": $("#ddlCustomer").val(),
            "CustomerName": $("#ddlCustomer :selected").text(),
            "CareHomeId": $("#hdnCareHomeId").val(),
            "CareHomeName": $("#txtCareHomeName").val(),
            "NextDeliveryDate": $("#txtNDD").val(),
            "PostCode": $("#PostCode").val(),
            "HouseNumber": $("#txtHouseNumber").val(),
            "HouseName": $("#txtHouseName").val(),
            "Address1": $("#txtAddress1").val(),
            "Address2": $("#txtAddress2").val(),
            "TownOrCity": $("#txtTownOrCity").val(),
            "County": county,
            "PhoneNo": $("#txtPhoneNo").val(),
            "EmailAddress": $("#txtEmailAddress").val(),
            "RoundId": $("#txtRoundId").val(),
            "Round": $("#txtRound").val(),
            "DeliveryFrequency": $("#txtDeliveryFrequency").val(),
            "OrderType": $("#ddlOrderType").val(),
            "PurchaseOrderNo": $("#txtPurchaseOrderNo").val(),
            "CareHomeStatus": $("#ddlStatus").val(),
            "SapCareHomeNumber": $("#lblCareHomeId").text(),
            //"CareHomeType": $("#ddlCarehomeType").val()
            "CommunicationPreferences": arrCommunicationFormat,
            "HasAnyActiveOrderForOrderType": $("#hdnHasAnyActiveOrderForOrderType").val(),
            "OldCarehomeStatus": $("#hdnCarehomeStatus").val()
        };
        $.ajax({
            url: '/CareHome/SaveCarehome',
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            data: JSON.stringify(objCareHome),
            success: function (results) {
                if (results > 0) {
                    $("#hdnCareHomeId").val(results);
                    CommonScriptObj.DisplayErrorMessage("divErrorMessage", $("#hdnSuccessMessage").val(), 'msgSuccess');
                    DisableControls();
                    EnablePopupButtons();
                    SetCommomObjects();
                    $("#hdnIsReloadCarehome").val(true);
                }
                else {
                    CommonScriptObj.DisplayErrorMessage("divErrorMessage", $("#hdnFailedMessage").val(), 'pop_error_ul');
                }
            },
            error: function (x, e) {
                HandleAjaxError(x, e, jsfilename, "SaveCareHomeDetails");
            }
        });
    }
}

function DisableControls() {
    $('#ddlCustomer').attr('disabled', 'disabled');

    if ($("#ddlStatus").val() == $("#hdnCareHomeRemovedStatus").val()) {
        $('#btnAddPatient').attr('disabled', 'disabled');
        $jqGrid("#btnAddPatient").addClass("btn_disable");
    }
    else {
        $('#btnAddPatient').removeAttr('disabled');
        $jqGrid("#btnAddPatient").removeClass("btn_disable");
    }
}

function DisablePopupButtons() {
    $jqGrid('#btnContactInfo').addClass("btn_disable");
    $jqGrid('#btnContactInfo').attr('disabled', 'disabled');
    $jqGrid("#btnRecentOrder").addClass("btn_disable");
    $jqGrid('#btnRecentOrder').attr('disabled', 'disabled');
    $jqGrid("#btnInteractionsummary").addClass("btn_disable");
    $jqGrid('#btnInteractionsummary').attr('disabled', 'disabled');
    $jqGrid("#btnAddPatient").addClass("btn_disable");
    $jqGrid('#btnAddPatient').attr('disabled', 'disabled');
    $jqGrid("#btnNote").addClass("btn_disable");
    $jqGrid('#btnNote').attr('disabled', 'disabled');
    $jqGrid("#btnAttachment").addClass("btn_disable");
    $jqGrid('#btnAttachment').attr('disabled', 'disabled');
    $jqGrid('#btnCareHomeAuditLog').addClass("btn_disable");
    $jqGrid('#btnCareHomeAuditLog').attr('disabled', 'disabled');

    //Disable Carehome status while creating new Carehome
    $('#ddlStatus').attr('disabled', 'disabled');
    $('#ddlStatus').addClass('bg_ddd');
    $(".ui-datepicker-trigger").addClass('btn_disable');
    $('#txtNDD').addClass("bg_ddd");
    $("#txtNDD").attr('readonly', 'true');
    $('#txtNDD').attr('disabled', 'disabled');

    //Disable Carehome report button while adding carehome
    $jqGrid('#btnCarehomeReport').addClass("btn_disable");
    $('#btnCarehomeReport').attr('disabled', 'disabled');

}

function ShowCarehomeNotePopup() {
    var NoteTypeId = $("#hdnCareHomeId").val();
    showPopupNotes(NoteTypeId);
}

function ShowPopupCareHomeReport() {
    isExport = false;
    var carehomeId = $("#hdnCareHomeId").val();
    var carehometitle = $("#hdnCarehomeReport").val();
    var carehomeReport = $jqGrid("#divCareHomeReportPopup").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 1200,
        height: 550,
        resizable: true,
        title: carehometitle,
        open: function () {
            $jqGrid.getJSON("/Carehome/GetCareHomeReportHeaderData", function (data) {
                if (data != null) {
                    $jqGrid("#lblCustomerNameId").text(data.CustomerNameId);
                    $jqGrid("#lblCarehomeNDD").text(data.CarehomeNDD);
                    $jqGrid("#lblCareHomeNameId").text(data.CareHomeNameId);
                    $jqGrid("#lblCarehomeFrequency").text(data.CarehomeFrequency);
                    $jqGrid("#lblCarehomeAddress").text(data.CarehomeAddress);
                    $jqGrid("#lblCarehomePhoneNo").text(data.CarehomePhoneNo);
                }
            });
            ShowCarehomeMaintenceReport(carehomeId);
        },
    });
    carehomeReport.dialog('open');
}

function ExportToExcel() {
    var carehomeId = $("#hdnCareHomeId").val();
    isExport = true;
    ShowCarehomeMaintenceReport(carehomeId);
}

function ExportToPDF() {
    $jqGrid("#data_loader").show();
    $.ajax({
        url: '/Carehome/GetCarhomeDetailsinPDF',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        cache: false,
        async: false,
        data: {},
        success: function (data) {
            $jqGrid("#data_loader").hide();
            if (data == "-1") {
                $jqGrid('#' + CareHomeObj.divValidationMsgText).html($jqGrid('#' + CareHomeObj.hdnErrorCannotExport + '').val());
                $jqGrid('#' + CareHomeObj.divAuditLogValidation).show();
            }
            else {
                window.open(data);
            }
        },
        error: function (x, e) {
            $jqGrid("#data_loader").hide();
            //alert('Ajax status code : ' + xhr.Status + ' and ' + xhr.statusText);
            HandleAjaxError(x, e, jsfilename, "ExportToPDF");
        }
    });
}

function ShowCarehomeMaintenceReport(careHomeId) {
    if (!isExport) {
        $jqGrid("#jqCarehomeReport").jqGrid('setGridParam', { postData: { isExport: isExport, careHomeId: careHomeId, patientId: patientId, auditSearchUserId: auditSearchUserId }, datatype: 'json' }).trigger('reloadGrid');
    }
    else {
        $jqGrid("#data_loader").show();
        $.ajax({
            url: '/Carehome/GetCareHomeReportDetailsData',
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            cache: false,
            async: false,
            data: { page: 1, rows: 100, isExport: isExport, careHomeId: careHomeId, patientId: patientId, auditSearchUserId: auditSearchUserId },
            success: function (data) {
                $jqGrid("#data_loader").hide();
                if (data == "-1") {
                    $jqGrid('#' + CareHomeObj.divValidationMsgText).html($jqGrid('#' + CareHomeObj.hdnErrorCannotExport + '').val());
                    $jqGrid('#' + CareHomeObj.divAuditLogValidation).show();
                }
                else {
                    window.open(data);
                }
            },
            error: function (x, e) {
                $jqGrid("#data_loader").hide();
                //alert('Ajax status code : ' + xhr.Status + ' and ' + xhr.statusText);
                HandleAjaxError(x, e, jsfilename, "ShowCarehomeMaintenceReport");
            }
        });
    }
}

function EnablePopupButtons() {
    $jqGrid('#btnContactInfo').removeClass("btn_disable");
    $jqGrid('#btnContactInfo').removeAttr("disabled");
    $jqGrid("#btnRecentOrder").removeClass("btn_disable");
    $jqGrid('#btnRecentOrder').removeAttr("disabled");
    $jqGrid('#btnInteractionsummary').removeClass("btn_disable");
    $jqGrid('#btnInteractionsummary').removeAttr("disabled");
    $jqGrid("#btnAddPatient").removeClass("btn_disable");
    $jqGrid('#btnAddPatient').removeAttr('disabled');
    $jqGrid("#btnNote").removeClass("btn_disable");
    $jqGrid('#btnNote').removeAttr('disabled');
    $jqGrid("#btnAttachment").removeClass("btn_disable");
    $jqGrid('#btnAttachment').removeAttr('disabled');
    $jqGrid("#btnCareHomeAuditLog").removeClass("btn_disable");
    $jqGrid('#btnCareHomeAuditLog').removeAttr('disabled');
    //Enable Carehome status while creating new Carehome
    $('#ddlStatus').removeAttr('disabled');
    $('#ddlStatus').removeClass('bg_ddd');

    $('#txtNDD').removeClass("bg_ddd");
    $("#txtNDD").removeAttr('readonly');
    $('#txtNDD').removeAttr('disabled');
    $(".ui - datepicker - trigger").removeClass('btn_disable');
    //Enable carehome Report button while Adding Carehome
    $('#btnCarehomeReport').removeAttr("disabled");
}
function SetPatientLoadParameter(rowData, SetSessionData) {

    patientId = rowData['PatientId'];
    customerId = $("#ddlCustomer").val();
    PatientName = rowData['FirstName'] + " " + rowData['LastName'];
    CustomerName = $("#ddlCustomer option:selected").text();
    SAPCustomerNumber = rowData['SAPCustomerNumber'];
    SAPPatientNumber = rowData['SAPPatientNumber'];
    //IsOrderActived = rowData['OrderActivated'];
    IsOrderActived = true;
    PatientStatus = rowData['PatientStatus'];
    carehomeId = $("#hdnCareHomeId").val();
    carehomeName = $("#txtCareHomeName").val();
    postCode = rowData['Postcode'];
    if (SetSessionData) {
        if (patientId != 0 && customerId != "" && PatientName != "" && CustomerName != "" && SAPCustomerNumber != "") {
            $jqGrid.ajax({
                url: '/Patient/SetPatientSession',
                type: 'POST',
                cache: false,
                async: false,
                data: { patientId: patientId, customerId: customerId, PatientName: PatientName, CustomerName: CustomerName, SAPCustomerNumber: SAPCustomerNumber, SAPPatientNumber: SAPPatientNumber, IsOrderActived: IsOrderActived, PatientStatus: PatientStatus, CareHomeID: carehomeId, CareHomeName: carehomeName, PostCode: postCode },
                success: function (data) {

                },
                error: function (x, e) {
                    HandleAjaxError(x, e, jsfilename, "SetPatientLoadParameter");
                }
            });
        }
    }
}

function AddPatient() {

    var carehomeStatus = $("#ddlStatus").val();
    if ($jqGrid("#hdnCareHomeActiveStatus").val() != carehomeStatus) {
        CommonScriptObj.DisplayErrorMessage("divErrorMessage", $("#hdnCareHomeStoppedActiveForAddPatient").val(), 'pop_error_ul');
        return false;
    }

    SetCareHomeSession();

    if ($("#hdnCareHomeId").val() != "") {
        window.location.href = "../../Patient/Details/" + $("#hdnCareHomeId").val();
    }
    else {
        window.location.href = "../../Patient/Details";
    }
}

    function SetCommomObjects() {
        var customerNameSAPNumber = "";
        if ($jqGrid("#ddlCustomer option:selected").text() != '') {
            var customerNameSAPNumber = $jqGrid("#ddlCustomer option:selected").text().split('(');
            }
        CommonScriptObj.SAPCustomerNumber = customerNameSAPNumber[1].split(')')[0];
    CommonScriptObj.CustomerName = customerNameSAPNumber[0];
    CommonScriptObj.CustomerId = $("#ddlCustomer").val();
    CommonScriptObj.SAPCareHomeNumber = $("#lblCareHomeId").text();
    CommonScriptObj.CareHomeName = $('#txtCareHomeName').val();
    CommonScriptObj.CareHomeId = $("#hdnCareHomeId").val();
    }

function CancelClick() {
    $jqGrid.ajax({
        url: '/Carehome/GetHomePageFlag',
        type: 'POST',
        async: false,
        cache: false,
            dataType: 'Json',
                data: { },
            success: function (data) {
        window.location.href = data;
        },
            error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "CancelClick");
        }
    });
}

function SetCareHomeSession() {
    var url = $jqGrid("#" + CareHomeObj.HdnSetCarehomeSession).val();
    $jqGrid.ajax({
        url: url,
        type: 'POST',
        cache: false
    })
}

function SetNoCommunicationValidation(ui, dropdown) {
    var NoCommunicationId = $("#hdnCommFormateNoCommunication").val();
    var ul = $jqGrid(".ui-multiselect-checkboxes input").filter(function (index) { return $(this).attr("name") == 'multiselect_' + dropdown; }).closest(".ui-multiselect-checkboxes");
    if (NoCommunicationChecked == false && ui.value == NoCommunicationId) {
        // Get the Communication Format Dropdown       

        $jqGrid(".ui-multiselect-checkboxes input").filter(function (index) { return $(this).attr("name") == 'multiselect_' + dropdown; }).filter('input').closest("div").removeClass("ez-checked");
        //$jqGrid(ul).filter('input').prop('checked', false);

        $jqGrid(".ui-multiselect-checkboxes input").filter(function (index) { return $(this).attr("name") == 'multiselect_' + dropdown; }).filter('input').prop('checked', false);

        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").addClass("ez-checked");
        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', true);
        NoCommunicationChecked = true;
    }

    else if (NoCommunicationChecked && ui.value == NoCommunicationId) {

        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', false);
        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").removeClass("ez-checked");
        NoCommunicationChecked = false;
    }
    else if (ui.checked && ui.value != NoCommunicationId) {
        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").removeClass("ez-checked");
        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', false);

        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == ui.value }).parent(".ez-checkbox").addClass("ez-checked");
        $jqGrid(ul).find("input").filter(function (index) { return $(this).attr('value') == ui.value }).prop('checked', true);

        NoCommunicationChecked = false;
    }
}

function SetNoCommunicationOnCheckAll(flag, dropdwon) {
    var NoCommunicationId = $("#hdnCommFormateNoCommunication").val();
    if (flag) {
        dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").removeClass("ez-checked");
        dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', false);
        NoCommunicationChecked = false;
    }
    else {
        dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).parent(".ez-checkbox").addClass("ez-checked");
        dropdwon.filter(function (index) { return $(this).attr('value') == NoCommunicationId }).prop('checked', true);
        NoCommunicationChecked = true;
    }
}

function GetCarehomeNDDRelatedDetails(customerId, carehomeId) {
    $jqGrid.ajax({
        url: '/Carehome/GetCarehomeNDDRelatedDetails',
        type: 'GET',
        contentType: "application/json",
        dataType: 'json',
        data: { customerId: customerId, carehomeId: carehomeId },
        cache: false,
        async: false,
        success: function (responseData) {
            if (responseData != 0) {
                changeOrderNddDays = responseData.ChangeOrderNddDays;
                day1 = responseData.Day1;
                day2 = responseData.Day2;
                day3 = responseData.Day3;
                day4 = responseData.Day4;
                day5 = responseData.Day5;
                roundID = responseData.RoundID;
            }
        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "GetCarehomeNDDRelatedDetails");
        }
    });
}

function GetHolidayListForCarehome() {
    holidayDaysList = [];
    $jqGrid.ajax({
        url: '/Carehome/GetHolidayListForCarehome',
        type: 'GET',
        cache: false,
        async: false,
        data: { customerId: $("#ddlCustomer").val() },
        success: function (responseData) {
            if (responseData.length > 0)
                for (var i = 0; i < responseData.length; i++) {
                    holidayDaysList.push(ConvertDateToMMDDYYYY(new Date(responseData[i].HolidayDateStringFormat)));
                }

        },
        error: function (x, e) {
            HandleAjaxError(x, e, jsfilename, "GetCarehomeNDDRelatedDetails");
        }
    });
    return holidayDaysList;
}