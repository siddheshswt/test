﻿$jqGrid = jQuery.noConflict();

//// Form declaration
function MassDataImport() { }

//// Object declaration
var MassDataImportObj = new MassDataImport();
MassDataImport.prototype.FileData = new FormData();

////Control Declaration
MassDataImport.prototype.CustomerDropdown = "ddlCustomer";
MassDataImport.prototype.UploadType = "ddlUploadDropdown";
MassDataImport.prototype.DownloadButton = "DownloadButton";
MassDataImport.prototype.HdnDownLoadTemplateUrl = "hdnDownLoadTemplateUrl";
MassDataImport.prototype.DdlImportDropdown = "ddlImportDropdown";
MassDataImport.prototype.EnumOrder = "hdnEnumOrder";
MassDataImport.prototype.btnMassChangesUndo = "btnMassChangesUndo";
MassDataImport.prototype.IsDirty = false;
MassDataImport.prototype.UploadExcelFileControl = "UploadExcelFileControl";
MassDataImport.prototype.btnUpload = "btnUpload";
MassDataImport.prototype.ExcelFormat = "hdnExcelFormat";
MassDataImport.prototype.FileNotSelected = "hdnFilNotSelected";

$jqGrid(document).ready(function () {
    MassDataImportObj.EnableDisableDownloadButton();
    //MassDataImportObj.EnableDisableFileUpload();
    CommonScriptObj.EnableDisableFileUpload(MassDataImportObj.UploadType, MassDataImportObj.UploadExcelFileControl, MassDataImportObj.btnUpload);

    $jqGrid("#ddlImportDropdown").change(function () {
        MassDataImportObj.IsDirty = true;
        MassDataImportObj.EnableDisableDownloadButton();
        return false;
    });

    $jqGrid("#ddlUploadDropdown").change(function () {
        MassDataImportObj.IsDirty = true;
        //MassDataImportObj.EnableDisableFileUpload();
        CommonScriptObj.EnableDisableFileUpload(MassDataImportObj.UploadType, MassDataImportObj.UploadExcelFileControl, MassDataImportObj.btnUpload);
        return false;
    });
        
    /// Disable Order option for Mass Import.
    //$("#" + MassDataImportObj.DdlImportDropdown + " option[value='" + MassDataImportObj.EnumOrder).val() + "']").css({ display: 'none' });
    //$("#" + MassDataImportObj.UploadType + " option[value=" + MassDataImportObj.EnumOrder).val() + "]").css({ display: 'none' });

    $("#" + MassDataImportObj.DdlImportDropdown + " option[value='" + $("#" + MassDataImportObj.EnumOrder).val() + "']").attr('disabled', 'disabled');
    $("#" + MassDataImportObj.UploadType + " option[value='" + $("#" + MassDataImportObj.EnumOrder).val() + "']").attr('disabled', 'disabled');

    $jqGrid("#" + MassDataImportObj.DownloadButton).bind("click", function () {      
      var url = $jqGrid("#" + MassDataImportObj.HdnDownLoadTemplateUrl).val();
      var selectedTemplateValue = $jqGrid("#" + MassDataImportObj.DdlImportDropdown).val();   

        $jqGrid.ajax({
            url: url,
            type: 'POST',
            dataType: 'Json',
            async: false,
            cache: false,
            data: { selectedTemplateValue: selectedTemplateValue },
            success: function (data) {                
                if (data.length > 0)
                {
                    window.open(data, '_blank');
                }
            }
        });
    });

    $jqGrid("#" + MassDataImportObj.btnMassChangesUndo).bind("click", function () {
        
        if (MassDataImportObj.IsDirty) {
            $jqGrid('#divCancelPrompt').dialog({
                autoOpen: false,
                modal: true,
                closeOnEscape: false,
                width: 'auto',
                resizable: false,
                buttons: [{
                    text: $jqGrid("#hdnGeneralYes").val().toString(),
                    "id": "hdnGeneralYes",
                    click: function () {
                        window.location.reload();
                    },
                }, {
                    text: $jqGrid("#hdnGeneralNo").val().toString(),
                    "id": "hdnGeneralNo",
                    click: function () {
                        $jqGrid('#divCancelPrompt').dialog('close');
                    },
                }]
            });
            $jqGrid('#divCancelPrompt').dialog('open');
        }
        });
    
    CommonScriptObj.DisableFormControls("frm_MasterMassDataImport");
});

MassDataImport.prototype.EnableDisableDownloadButton = function () {
    var ddlImport = $jqGrid("#ddlImportDropdown :selected").val();
    $jqGrid("#DownloadButton").removeClass("btn_disable");
    if (ddlImport == "") {
        $jqGrid("#DownloadButton").addClass("btn_disable");
    }
}

MassDataImport.prototype.EnableDisableFileUpload = function () {
    var ddlUpload = $jqGrid("#ddlUploadDropdown").val();
    
    clear_upload("UploadExcelFileControl");
    $jqGrid('#UploadExcelFileControl').val('');
    MassDataImport.prototype.FileData = new FormData();

    $jqGrid("#btnUpload").addClass("btn_disable");
    $jqGrid('#btnUpload').attr('disabled', 'disabled');

    if (ddlUpload > 0) {
       
        $jqGrid("#UploadExcelFileControl").removeClass("btn_disable");
        $jqGrid('#UploadExcelFileControl').removeAttr("disabled");
    }
    else {        
        
        $jqGrid("#UploadExcelFileControl").addClass("btn_disable");
        $jqGrid('#UploadExcelFileControl').attr('disabled', 'disabled');
    }    
}

// click event of upload button
MassDataImport.prototype.ImportData = function () {    
    if ($("#UploadExcelFileControl").val() == "") {
        $jqGrid("#btnUpload").addClass("btn_disable");
        $jqGrid('#btnUpload').attr('disabled', 'disabled');
        CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", $("#" + MassDataImportObj.FileNotSelected).val(), 'pop_error_ul');
        $jqGrid("#data_loader").hide();
        return false;
    }

    $jqGrid("#data_loader").show();    
    var type = $jqGrid('#' + MassDataImportObj.UploadType).val();
    if (MassDataImportObj.ValidateFile()) {        
        /// Check for file format
        /// file format can replaced with constant value. cusrrently facing some problem after taking from constants.
        if ((/\.(gif|pdf|jpg|txt|sql|exe|dll|jpeg|tiff|png)$/i).test($("#UploadExcelFileControl").val())) {
            CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", $("#" + MassDataImportObj.ExcelFormat).val(), 'pop_error_ul');
            $jqGrid("#data_loader").hide();
            return false;
        }

        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        if (type == "20152") { // this is temporary, gets remove later..
            //For Type - Prescription - Mass Data Changes call..
            fd.append("uploadedFile", $jqGrid("#UploadExcelFileControl")[0].files[0]);
            fd.append("selectedTemplate", type);
            fd.append("isImport", true);
            xhr.open("POST", "/MassDataChanges/UploadMassUpdateData", true);
            xhr.responseType = 'json';
            xhr.send(fd, true);
            xhr.addEventListener("load", function (data) {
                $jqGrid("#data_loader").hide();
                var parsedjsonData;
                try {
                    parsedjsonData = jQuery.parseJSON(data.target.response);                   
                } catch (e) {
                    parsedjsonData = data.target.response;
                }

                if (typeof (parsedjsonData) != 'undefined' && parsedjsonData.Status != false) {
                    CommonScriptObj.DisplayErrorMessage("divMasssChangeErrorMessage", parsedjsonData.Message, 'msgSuccess');
                    MassDataImportObj.ClearUploadField();
                    MassDataImportObj.FileData = new FormData();
                }
                else {
                    ShowTemplateError(parsedjsonData);


                }
            }, false);
        }
        else {
            fd.append("uploadedFile", $jqGrid("#UploadExcelFileControl")[0].files[0]);
            fd.append("selectedTemplate", $jqGrid('#' + MassDataImportObj.UploadType).val());
            fd.append("isImport", true);
            xhr.open("POST", "/MassDataImport/ImportData", true);
            xhr.responseType = 'json';
            xhr.send(fd);
            xhr.addEventListener("load", function (data) {
                $jqGrid("#data_loader").hide();
                var parsedjsonData;
                try {
                    parsedjsonData = jQuery.parseJSON(data.target.response);                    
                } catch (e) {
                    parsedjsonData = data.target.response;
                }

                if (typeof (parsedjsonData) != 'undefined' && parsedjsonData.Status != false) {                    
                    CommonScriptObj.DisplayErrorMessage("divUploadExcelErrorMessage", parsedjsonData.Message, 'msgSuccess');
                    MassDataImportObj.ClearUploadField();
                    MassDataImportObj.FileData = new FormData();
                }
                else {                    
                    ShowTemplateError(parsedjsonData);
                }
            }, false);
        }
    }
    else
    {
        $jqGrid("#data_loader").hide();
        return false;
    }
}


function ShowTemplateError(parsedjsonData)
{
    $("#divAlertErrorToview").show();
    var length = parsedjsonData.validationRows.length;
    var Message = "";
    if (parsedjsonData.validationRows.length > 0) {
        Message = parsedjsonData.validationRows[0].message;
        $("#validationText").html(Message);
        if (Message == "" || Message == null) {
            $jqGrid("#validationErrorjqWrapper").show();
            $jqGrid("#jqInvalidTemplateColumn").jqGrid('setGridParam', { datatype: 'local', data: parsedjsonData.validationRows }).trigger('reloadGrid');
        }
        else {
            $jqGrid("#validationErrorjqWrapper").hide();
        }
    }
}


function closeValidationPopup() {
    $jqGrid("#divAlertErrorToview").hide()
}

MassDataImport.prototype.ClearUploadField = function (e) {
    {
        $jqGrid("#ddlUploadDropdown").val("");
        //MassDataImportObj.EnableDisableFileUpload();
        CommonScriptObj.EnableDisableFileUpload(MassDataImportObj.UploadType, MassDataImportObj.UploadExcelFileControl, MassDataImportObj.btnUpload);
    }
}

    // change event of file upload control
    MassDataImport.prototype.FileUploadChange = function (e) {
        var files = e.files;
        if (files.length > 0) {
        $jqGrid("#btnUpload").removeClass("btn_disable");
        $jqGrid('#btnUpload').removeAttr("disabled");
        var data= new FormData();
        MassDataImportObj.FileData.append("file", files[0]);
        MassDataImportObj.FileData.append("file", $jqGrid("#ddlUploadDropdown").val());
        }
        else {
        $jqGrid("#btnUpload").addClass("btn_disable");
        $jqGrid('#btnUpload').attr('disabled', 'disabled');
}
}


    // Validate attachment like format and size.
    MassDataImport.prototype.ValidateFile = function () {
        var maxSizeAllowed = $jqGrid("#hdnAppConfigMaxSixe").val() * 1024 * 1024;
        var fileSize = $("#UploadExcelFileControl")[0].files[0].size;      
        if (fileSize > maxSizeAllowed) {
            var maxSizeMessage = $jqGrid("#hdnFilzSize").val().replace('{0}', $jqGrid("#hdnAppConfigMaxSixe").val());
            CommonScriptObj.DisplayErrorMessage("divUploadExcelErrorMessage", maxSizeMessage, 'pop_error_ul');
            return false;
        }
        return true;
    }

    MassDataImport.prototype.ReadExcelData = function () {
        $jqGrid.ajax({
                url: '/MassDataImport/ReadExcelData',
                type: 'GET',
                dataType: 'Json',                
                cache: false,
                data: {
        },
                success: function (data) {
            if (data.ErrorMessage.length > 0) {
                CommonScriptObj.DisplayErrorMessage("divAlertErrorToview", data.Message, 'pop_error_ul');
            }
            else {
                //TODO: Import successful.. Need to log details into the Data file
                }
        }
        });
    }
