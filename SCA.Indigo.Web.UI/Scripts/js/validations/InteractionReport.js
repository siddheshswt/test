﻿$jqGrid = jQuery.noConflict();

var jsfilename = "InteractionReport.js";
//All the values Selected or Entered by users are stored in below variables.
//Call "SetReportData" function to set values to below variables.
var CustomerIds = "";
var CareHomeIds = "";
var PatientIds = "";
var InteractionTypeIds = "";
var InteractionSubTypeIds = "";
var StatusId = "";
var CreatedBy = "";
var CreatedDtFrom = "";
var CreatedDtTo = "";
var ModifiedDtFrom = "";
var ModifiedDtTo = "";

var isExport = false;

$jqGrid(document).ready(function () {
    $jqComm("#ddlCustomer").multiselect({ noneSelectedText: 'Select Customer' });

    var customerId = $jqGrid("#ddlCustomer").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqComm("#ddlInteractionType").multiselect({ noneSelectedText: 'Select Interaction Type' });

    var InteractionType = $jqGrid("#ddlInteractionType").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqComm("#ddlInteractionSubType").multiselect({ noneSelectedText: 'Select Interaction Sub Type' });

    var InteractionSubType = $jqGrid("#ddlInteractionSubType").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqGrid("#ddlInteractionSubType").multiselect("disable");

    $jqComm("#ddlInteractionStatus").multiselect({ noneSelectedText: 'Select Status' });

    var InteractionStatus = $jqGrid("#ddlInteractionStatus").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqComm("#ddlCreatedBy").multiselect({ noneSelectedText: 'Select Created By' });

    var CreatedBy = $jqGrid("#ddlCreatedBy").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    $jqGrid("#ddlCreatedBy").multiselect("disable");

    $jqComm(function () {
        $jqGrid("#dtIRCreatedDateFrom").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            buttonImage: '../../Images/calander_icon.png',
            buttonImageOnly: true,
            showOn: 'both',
        });

        $jqGrid("#dtIRCreatedDateTo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            buttonImage: '../../Images/calander_icon.png',
            buttonImageOnly: true,
            showOn: 'both',
        });

        $jqGrid("#dtIRModifiedDateFrom").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            buttonImage: '../../Images/calander_icon.png',
            buttonImageOnly: true,
            showOn: 'both',
        });

        $jqGrid("#dtIRModifiedDateTo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            buttonImage: '../../Images/calander_icon.png',
            buttonImageOnly: true,
            showOn: 'both',
        });
    });

    $jqGrid("#ddlInteractionType").on('change', function (e) {
        var interactionTypeId = $jqComm("#ddlInteractionType").multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        if (interactionTypeId.length == 0)
            $jqGrid("#ddlInteractionSubType").multiselect("disable");
        else
            $jqGrid("#ddlInteractionSubType").multiselect("enable");

        $("#ddlInteractionSubType").empty();
        var InteractionName = $("#ddlInteractionType option:selected").text();
        if (InteractionName != "" && InteractionName != undefined) {
            LoadInteractionSubTypeDDL(interactionTypeId);
        }
        $jqComm("#ddlInteractionSubType").multiselect('refresh');
        $jqComm('.ui-corner-all input[type="checkbox"]').ezMark();
    });

    $jqGrid("#btnInterReportUndo").click(function () {
        window.location.reload(true);
    });

    $jqGrid("#btnInterReportExpToExl").click(function () {
        isExport = true;
        getInteractionReport();
    });

    ///Check for ViewOnly 
    ///Element: Main div of the form       
    CommonScriptObj.DisableFormControls("dvInteractionReport");
    var PageName = $jqGrid("#hdnInteractionReport").val();
    var IsViewOnly = $jqGrid("#hdnIsViewOnly").val();
    var IsFullRights = $jqGrid("#hdnIsFullRights").val();
    if (PageName == "InteractionReport" &&
                               (IsViewOnly == "True" || IsViewOnly == true) &&
                                   (IsFullRights == "False" || IsFullRights == false)) {
        setTimeout(function () {
            //dropdowns        
            $jqGrid("#ddlCustomer").multiselect("uncheckAll");
            $jqGrid("#ddlInteractionType").multiselect("uncheckAll");
            $jqGrid("#ddlInteractionSubType").multiselect("uncheckAll");
            $jqGrid("#ddlInteractionStatus").multiselect("uncheckAll");
            $jqGrid("#ddlCreatedBy").multiselect("uncheckAll");
            $jqGrid("#ddlCustomer").multiselect("disable");
            $jqGrid("#ddlInteractionType").multiselect("disable");
            $jqGrid("#ddlInteractionSubType").multiselect("disable");
            $jqGrid("#ddlInteractionStatus").multiselect("disable");
            $jqGrid("#ddlCreatedBy").multiselect("disable");
            //text area
            $jqGrid("#txtACCarehome").addClass("btn_disable");
            $jqGrid("#txtACCarehome").attr('disabled', 'disabled');
            $jqGrid("#txtACPatient").addClass("btn_disable");
            $jqGrid("#txtACPatient").attr('disabled', 'disabled');
            //calander  
            $jqGrid(".ui-datepicker-trigger").unbind('click');
            $jqGrid("#dtIRCreatedDateFrom").addClass("btn_disable");
            $jqGrid("#dtIRCreatedDateFrom").attr('disabled', 'disabled');
            $jqGrid("#dtIRCreatedDateTo").addClass("btn_disable");
            $jqGrid("#dtIRCreatedDateTo").attr('disabled', 'disabled');
            $jqGrid("#dtIRModifiedDateFrom").addClass("btn_disable");
            $jqGrid("#dtIRModifiedDateFrom").attr('disabled', 'disabled');
            $jqGrid("#dtIRModifiedDateTo").addClass("btn_disable");
            $jqGrid("#dtIRModifiedDateTo").attr('disabled', 'disabled');
            //buttons
            $jqGrid("#btnInterReportViewReport").addClass("btn_disable");
            $jqGrid("#btnInterReportViewReport").attr('disabled', 'disabled');
            $jqGrid("#btnInterReportExpToExl").addClass("btn_disable");
            $jqGrid("#btnInterReportExpToExl").attr('disabled', 'disabled');
            $jqGrid("#btnInterReportExpToPdf").addClass("btn_disable");
            $jqGrid("#btnInterReportExpToPdf").attr('disabled', 'disabled');
            $jqGrid("#btnInterReportUndo").addClass("btn_disable");
            $jqGrid("#btnInterReportViewReport").attr('disabled', 'disabled');
            //grid
            $jqGrid(".interaction_summary").hide();
        }, 500);
    }

    //**************Start Function : For Created by ddl after selecting customer id**********************************//
    $jqGrid("#ddlCustomer").on('change', function (e) {
        var customerId = $jqComm("#ddlCustomer").multiselect("getChecked").map(function () {
            return this.value;
        }).get();

        if (customerId.length == 0)
            $jqGrid("#ddlCreatedBy").multiselect("disable");
        else
            $jqGrid("#ddlCreatedBy").multiselect("enable");

        $("#ddlCreatedBy").empty();
        var customerName = $("#ddlCustomer option:selected").text();
        if (customerName != "" && customerName != undefined) {
            LoadCreatedByDDL(customerId);
        }
        $jqComm("#ddlCreatedBy").multiselect('refresh');
        $jqComm('.ui-corner-all input[type="checkbox"]').ezMark();
    });

    function LoadCreatedByDDL(customerId) {
        if (customerId == '' || customerId == undefined) {
            customerId = $jqGrid('#ddlCustomer').val();
        }

        if (customerId != '') {
            var url = $jqGrid('#CreatedByAction').val();
            $jqGrid.ajax({
                cache: false,
                type: "GET",
                url: url,
                data: { "customerIds": customerId.toString() },
                async: false,
                cache: false,
                success: function (data) {
                    $.each(data, function (key, option) {
                        $("#ddlCreatedBy").append($jqGrid('<option></option>').val(option.Value).html(option.Text));
                    });
                },
            });
        }
    }
    //**************End Function : For Created by ddl after selecting customer id************************************//
});

function LoadInteractionSubTypeDDL(interactionTypeId) {

    if (interactionTypeId == '' || interactionTypeId == undefined) {
        interactionTypeId = $jqGrid('#ddlInteractionType').val();
    }

    var ddlInteractionSubType = $jqGrid('#ddlInteractionSubType');
    if (interactionTypeId != '') {
        var url = $jqGrid('#InteractionSubTypeAction').val();
        $jqGrid.ajax({
            cache: false,
            type: "GET",
            url: url,
            data: { "interactionTypeId": interactionTypeId.toString() },
            async: false,
            cache: false,
            success: function (data) {
                $.each(data, function (key, option) {
                    $("#ddlInteractionSubType").append($jqGrid('<option></option>').val(option.Value).html(option.Text));
                });
            },
        });
    }
}

function SetReportData() {
    var customerId = $jqComm("#ddlCustomer").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    CustomerIds = customerId.toString();

    CareHomeIds = $jqComm("#txtACCarehome").val();
    PatientIds = $jqComm("#txtACPatient").val();

    var interactionTypeId = $jqComm("#ddlInteractionType").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    InteractionTypeIds = interactionTypeId.toString();

    var interactionSubTypeId = $jqComm("#ddlInteractionSubType").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    InteractionSubTypeIds = interactionSubTypeId.toString();

    var statusIds = $jqComm("#ddlInteractionStatus").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    StatusId = statusIds.toString();

    var createdByIds = $jqComm("#ddlCreatedBy").multiselect("getChecked").map(function () {
        return this.value;
    }).get();

    CreatedBy = createdByIds.toString();

    CreatedDtFrom = $jqComm("#dtIRCreatedDateFrom").val();
    CreatedDtTo = $jqComm("#dtIRCreatedDateTo").val();
    ModifiedDtFrom = $jqComm("#dtIRModifiedDateFrom").val();
    ModifiedDtTo = $jqComm("#dtIRModifiedDateTo").val();
}

function ReportValidation() {
    SetReportData();

    if (CustomerIds.length == 0) {
        CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportCustomerErrMsg").val(), 'pop_error_ul');
        return false;
    }

    if (InteractionTypeIds.length == 0) {
        CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportInteractionIDErrMsg").val(), 'pop_error_ul');
        return false;
    }

    var createdDtFrom;
    var createdDtTo;
    if (CreatedDtFrom != '') {
        createdDtFrom = CommonScriptObj.isValidDate(CreatedDtFrom);
        if (createdDtFrom != false) {
            createdDtFrom = Date.parse(createdDtFrom)
        }
        else {
            CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportCreInvalidFrmDtErrMsg").val(), 'pop_error_ul');
            return false;
        }
    }

    if (CreatedDtTo != '') {
        createdDtTo = CommonScriptObj.isValidDate(CreatedDtTo);
        if (createdDtTo != false) {
            createdDtTo = Date.parse(createdDtTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportCreInvalidToDtErrMsg").val(), 'pop_error_ul');
            return false;
        }
    }

    if (CreatedDtFrom != '' && CreatedDtTo != '') {
        if (createdDtFrom > createdDtTo) {
            CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportFrmToCErrMsg").val(), 'pop_error_ul');
            return false;
        }
    }

    var modifiedDtFrom;
    var modifiedDtTo;
    if (ModifiedDtFrom != '') {
        modifiedDtFrom = CommonScriptObj.isValidDate(ModifiedDtFrom);
        if (modifiedDtFrom != false) {
            modifiedDtFrom = Date.parse(modifiedDtFrom)
        }
        else {
            CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportModInvalidFrmDtErrMsg").val(), 'pop_error_ul');
            return false;
        }
    }

    if (ModifiedDtTo != '') {
        modifiedDtTo = CommonScriptObj.isValidDate(ModifiedDtTo);
        if (modifiedDtTo != false) {
            modifiedDtTo = Date.parse(modifiedDtTo)
        }
        else {
            CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportModInvalidToDtErrMsg").val(), 'pop_error_ul');
            return false;
        }
    }

    if (ModifiedDtFrom != '' && ModifiedDtTo != '') {
        if (modifiedDtFrom > modifiedDtTo) {
            CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqComm("#hdnresInterReportFrmToMErrMsg").val(), 'pop_error_ul');
            return false;
        }
    }
    return true;
}

$jqGrid("#btnInterReportExpToPdf").click(function () {
    isExport = true;
    if (ReportValidation()) {
        $jqGrid("#data_loader").show();
        $.ajax({
            url: '/Interaction/GenerateInteractionReportPdf',
            type: 'POST',
            //contentType: "application/json",
            dataType: 'json',
            cache: false,
            async: false,
            data: {
                page: 1, rows: 100, isExport: isExport, CustomersIds: CustomerIds, PatientIds: PatientIds, CareHomeIds: CareHomeIds,
                InteractionTypeId: InteractionTypeIds, InteractionSubTypeId: InteractionSubTypeIds, StatusId: StatusId,
                CreatedBy: CreatedBy, CreatedDateFrom: CreatedDtFrom, CreatedDateTo: CreatedDtTo, ModifiedDateFrom: ModifiedDtFrom, ModifiedDateTo: ModifiedDtTo
            },
            success: function (data) {
                $jqGrid("#data_loader").hide();
                if (data == "-1") {
                    CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqGrid("#hdnErrorCannotExportToPdf").val(), "pop_error_ul");
                }
                else {
                    window.open(data);
                }
            },
            error: function (x, e) {
                $jqGrid("#data_loader").hide();
                //alert('Ajax status code : ' + xhr.Status + ' and ' + xhr.statusText);
                HandleAjaxError(x, e, jsfilename, "btnInterReportExpToPdf");
            }
        });
    }
});

//**************Start Function: JqGrid for interaction report summary section******************//

$jqGrid('#btnInterReportViewReport').click(function () {
    isExport = false;
    getInteractionReport();
});

function getInteractionReport() {
    if (ReportValidation()) {
        if (!isExport) {
            $jqGrid("#jqInterReportSummary").jqGrid('setGridParam', {
                postData: {
                    isExport: isExport, CustomersIds: CustomerIds, PatientIds: PatientIds, CareHomeIds: CareHomeIds,
                    InteractionTypeId: InteractionTypeIds, InteractionSubTypeId: InteractionSubTypeIds, StatusId: StatusId,
                    CreatedBy: CreatedBy, CreatedDateFrom: CreatedDtFrom, CreatedDateTo: CreatedDtTo, ModifiedDateFrom: ModifiedDtFrom, ModifiedDateTo: ModifiedDtTo
                }, datatype: 'json'
            }).trigger('reloadGrid');
        }
        else {
            $jqGrid("#data_loader").show();
            $.ajax({
                url: '/Interaction/GetInteractionReport',
                type: 'POST',
                //contentType: "application/json",
                dataType: 'json',
                cache: false,
                async: false,
                data: {
                    page: 1, rows: 100, isExport: isExport, CustomersIds: CustomerIds, PatientIds: PatientIds, CareHomeIds: CareHomeIds,
                    InteractionTypeId: InteractionTypeIds, InteractionSubTypeId: InteractionSubTypeIds, StatusId: StatusId,
                    CreatedBy: CreatedBy, CreatedDateFrom: CreatedDtFrom, CreatedDateTo: CreatedDtTo, ModifiedDateFrom: ModifiedDtFrom, ModifiedDateTo: ModifiedDtTo
                },
                success: function (data) {
                    $jqGrid("#data_loader").hide();
                    if (data == "-1") {
                        CommonScriptObj.DisplayErrorMessage("divInteractionReportErrorMessage", $jqGrid("#hdnErrorCannotExport").val(), "pop_error_ul");
                    }
                    else {
                        window.open(data);
                    }
                },
                error: function (x, e) {
                    $jqGrid("#data_loader").hide();
                    alert("gor the error");
                    //alert('Ajax status code : ' + xhr.Status + ' and ' + xhr.statusText);
                    HandleAjaxError(x, e, jsfilename, "getInteractionReport");
                }
            });
        }
    }
    else {
    }
}


//**************End Function******************************************************************//

