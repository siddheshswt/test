﻿$jqGrid = jQuery.noConflict();
var userTypeforOrder = "SCA";
//END: 1.13.1 COMMUNITY/ONE-OFF/CAREHOME ORDER ACTIVATION-NOTES(NEED TO REMOVE FOR SPRINT 2)

var ProductOrderObj = new ProductOrder();
function ProductOrder() { }

$jqGrid(document).ready(function () {
    var careHomeId = $jqGrid('#hdnCareHomeId').val();

    if (careHomeId > 0) {
        FillPatientDetails(careHomeId);
    }    

    CommonScriptObj.DisableFormControls("frm_ProductOrders");
});//ready


function FillPatientDetails(CareHomeId) {
    $jqGrid.ajax({
        url: '/CareHome/GetCareHomePatientList',
        type: 'GET',
        cache: false,
        data: { careHomeID: CareHomeId },
        async: false,
        success: function (responseData) {
            $jqGrid("#jqProductOrderPatientDetails").jqGrid('clearGridData');
            $jqGrid("#jqProductOrderPatientDetails").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
        }
    });
}

function SetPatientLoadParameter(rowData, SetSessionData) {    
    patientId = rowData['PatientId'];
    customerId = $("#hdnCustomerId").val();
    PatientName = rowData['FirstName'] + " " + rowData['LastName'];
    CustomerName = rowData['CustomerName'];
    SAPCustomerNumber = rowData['SAPCustomerNumber'];
    SAPPatientNumber = rowData['SAPPatientNumber'];
    IsOrderActived = true;
    PatientStatus = rowData['PatientStatus'];
    carehomeId = $("#hdnCareHomeId").val();
    carehomeName = $("#txtCareHomeName").val();
    postCode = rowData['Postcode'];
    if (SetSessionData) {
        if (patientId != 0 && customerId != "" && PatientName != "" && CustomerName != "" && SAPCustomerNumber != "") {
            $jqGrid.ajax({
                url: '/Patient/SetPatientSession',
                type: 'POST',
                async: false,
                cache: false,
                data: { patientId: patientId, customerId: customerId, PatientName: PatientName, CustomerName: CustomerName, SAPCustomerNumber: SAPCustomerNumber, SAPPatientNumber: SAPPatientNumber, IsOrderActived: IsOrderActived, PatientStatus: PatientStatus, CareHomeID: carehomeId, CareHomeName: carehomeName, PostCode: postCode, isRedirectFromProductOrder: "1" },
                success: function (data) {
                }
            });
        }
    }
}

$jqGrid("#btnSaveProductOrder").click(function () {
    if (isProductOrderDataValid()) {
        SaveProductOrderData(false);
    }
});

$jqGrid("#btnCancelProductOrder").click(function () {
    if (isProductOrderDataValid()) {
        $jqGrid("#divProductOrdersMsgCancelConfirm").dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            resizable: false,
            closeOnEscape: false,
            buttons: [{
                text: $jqGrid("#hdnGeneralYes").val().toString(),
                "id": "hdnGeneralYes",
                click: function () {
                    $jqGrid(this).dialog("close");
                    SaveProductOrderData(true);
                },
            },
            {
                text: $jqGrid("#hdnGeneralNo").val().toString(),
                "id": "hdnGeneralNo",
                click: function () {
                    $jqGrid(this).dialog("close");
                },
            }
            ]
        });
        $jqGrid("#divProductOrdersMsgCancelConfirm").dialog('open');
        return false;
    }
});

function SaveProductOrderData(isCancel) {
    var orderNote = jQuery.trim($jqGrid('#txtProductOrderNote').val());
    if (orderNote == $jqGrid('#hdnBulkNotePlaceholder').val()) {
        orderNote = "";
    }
    if (isCancel) {
        $("#spnOrderSaved").hide();
        $("#spnOrderCancelled").show();
    }
    else {
        $("#spnOrderCancelled").hide();
        $("#spnOrderSaved").show();
    }
    var ordersList = [];
    var gridRows = $jqGrid("#jqProductOrders").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqProductOrders").jqGrid('getDataIDs');
    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = gridIds[i];
        if ($jqGrid("#" + (rowId) + "_Select").is(':checked')) {
            var carehomeId = $jqGrid("#hdnCareHomeId").val();
            var productId = row["ProductId"];
            var quantity = $jqGrid("#" + (rowId) + "_Quantity").val();
            if (quantity == undefined) {
                quantity = row["Quantity"];
                if (quantity == undefined) {
                    quantity = $jqGrid('#jqProductOrders').jqGrid('getCell', rowId, 'Quantity');
                }
            }
            if (isCancel) {
                quantity = 0;
            }
            var deliveryDate = row["DeliveryDate"];

            var orderOject = {
                CareHomeID: $jqGrid("#hdnCareHomeId").val(),
                ProductId: productId,
                Quantity: quantity,
                DeliveryDate: deliveryDate,
                OrderType: $jqGrid("#hdnProductOrderType").val(),
                isProductLevel: true,
                OrderId: row["OrderId"],
                OrderNote: orderNote,
                PurchaseOrderNum: $jqGrid("#txtPurchaseOrderNo").val(),
                OrderCreationType: $jqGrid('#hdnCarehomeProductOrder').val(),
                IsNDDManuallyChanged: isNDDManuallyChanged,
                IsOrderCancel: isCancel
            }
            ordersList.push(orderOject);
        }
    }
    if (ordersList.length > 0) {
        $.ajax({
            url: '/Order/SaveOrder',
            type: 'POST',
            contentType: "application/json",
            dataType: 'json',
            async: false,
            data: JSON.stringify(ordersList),
            success: function (data) {
                $jqGrid('#divDataSavedOrders').dialog({
                    autoOpen: false,
                    modal: true,
                    width: 'auto',
                    closeOnEscape: false,
                    resizable: false,
                    buttons: [{
                        text: $jqGrid("#hdnGeneralOk").val().toString(),
                        "id": "hdnGeneralOk",
                        click: function () {
                            $jqGrid('#divDataSavedOrders').dialog('close');
                            $jqGrid('#jqProductOrders').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                            $jqGrid('#btnSaveProductOrder').val($jqGrid('#hdnbtnTextUpdate').val());
                            $jqGrid("#chkSelectAll").prop("checked", false);
                            $jqGrid("#chkSelectAll").parent(".ez-checkbox").removeClass("ez-checked");
                            window.location.reload(); //refresh the page after save
                        },
                    }]
                });
                $jqGrid('#divDataSavedOrders').dialog('open');
                $jqGrid(".ui-dialog-titlebar-close").hide();
                return false;
            },
            error: function () {
            }
        });//ajax
    }//if
}

function isProductOrderDataValid() {
    var gridRows = $jqGrid("#jqProductOrders").jqGrid('getRowData');
    var gridIds = $jqGrid("#jqProductOrders").jqGrid('getDataIDs');
    var numberOfChecked = $('.chkSelectClass:checked').length;
    var isNewOrder = $jqGrid('#spnOrderSelfcareTitle').text() == $jqGrid('#hdnProductOrderHeaderTextNew').val() ? true : false;

    if ($("#hdnProductOrderNoteMandatory").val().toUpperCase() == userTypeforOrder.toUpperCase() && $.trim($("#txtProductOrderNote").val()) == "") {

        $jqGrid("#divProductDetailsMessage").html($jqGrid('#hdnreserrormsNote').val());
        $jqGrid('#divProductDetailsMessage').show();
        return false;
    }

    if (numberOfChecked < 1) {
        $jqGrid("#divProductDetailsMessage").html($jqGrid('#hdnreserrormsNoProductSelected').val());
        $jqGrid('#divProductDetailsMessage').show();
        return false;
    }

    for (var i = 0; i < gridRows.length; i++) {
        var row = gridRows[i];
        var rowId = gridIds[i];
        if ($jqGrid("#" + (rowId) + "_Select").is(':checked')) {
            var deliveryDate = row["DeliveryDate"];
            var isHoliday = IsHoliday(deliveryDate);
            if (isHoliday) {
                ShowErrorMessage("divProductOrdersNDDHoliday");
                return false;
            }
        }
    }

    return true;
}

function IsHoliday(date) {
    var isHoliday = true;
    $jqGrid.ajax({
        url: '/Order/IsHoliday',
        type: 'GET',
        cache: false,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: { date: date },
        success: function (data) {
            isHoliday = data;
        },
        error: function (xhr) {
        }
    });

    return isHoliday;
}

function enableDisableProductOrderPage(isDisable) {
    if (isDisable) {
        $jqGrid('#productOrderDisable').addClass('disable_div')
        $jqGrid("#ProductOrderButtons").addClass('read_only_div');
        $jqGrid("#btnSaveProductOrder").addClass('btn_disable');
        $jqGrid("#btnCancelProductOrder").addClass('btn_disable');
        $jqGrid('#txtPurchaseOrderNo').addClass('btn_disable');
        $jqGrid("#spnOrderSentToSAP").show();
    }
    else {
        $jqGrid('#productOrderDisable').removeClass('disable_div');
        $jqGrid("#ProductOrderButtons").removeClass('read_only_div');
        $jqGrid("#btnSaveProductOrder").removeClass('btn_disable');
        $jqGrid("#btnCancelProductOrder").removeClass('btn_disable');
        $jqGrid("#txtPurchaseOrderNo").removeClass('btn_disable');
        $jqGrid("#spnOrderSentToSAP").hide();
    }
}