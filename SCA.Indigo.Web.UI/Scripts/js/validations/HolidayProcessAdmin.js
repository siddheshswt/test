﻿$jqGrid = jQuery.noConflict();

// Global Variables
var HolidayProcessAdminObj = new HolidayProcessAdmin();
HolidayProcessAdmin.prototype.CustomerDropdown = "ddlCustomer";
HolidayProcessAdmin.prototype.GetCustomerDetails = "/HolidayProcess/GetCustomerDetails";
HolidayProcessAdmin.prototype.HolidayProcessAdminGrid = "jqHolidayProcessAdmin";
HolidayProcessAdmin.prototype.SaveUrl = "/HolidayProcess/SaveHolidayProcess";
HolidayProcessAdmin.prototype.RemoveClass = "chkRemoveClass";
HolidayProcessAdmin.prototype.StartHour = "ddlStartHour";
HolidayProcessAdmin.prototype.StartMinute = "ddlStartMinute";
HolidayProcessAdmin.prototype.EndHour = "ddlEndHour";
HolidayProcessAdmin.prototype.EndMinute = "ddlEndMinute";
HolidayProcessAdmin.prototype.StartFormat = "ddlStartFormat";
HolidayProcessAdmin.prototype.EndFormat = "ddlEndFormat";
HolidayProcessAdmin.prototype.NDD = "txtNDD";
HolidayProcessAdmin.prototype.ArrangedNDD = "txtArrangedNDD";
HolidayProcessAdmin.prototype.RemoveNDDMessage = "msgRemoveNDD";
HolidayProcessAdmin.prototype.SaveMessage = "divSaveHolidayProcess";
HolidayProcessAdmin.prototype.ErrorMessage = "divErrorHolidayProcess";
HolidayProcessAdmin.prototype.InvalidCustomer = "hdnerrorInvalidCustomer";
HolidayProcessAdmin.prototype.ErrorMessage = "divErrorMessage";
HolidayProcessAdmin.prototype.ErrorStartTimeMessage = "hdnerrorStartTime";
HolidayProcessAdmin.prototype.ErrorEndTimeMessage = "hdnerrorEndTime";
HolidayProcessAdmin.prototype.ErrorNoGridRowsMessage = "hdnerrorNDDRemove";
HolidayProcessAdmin.prototype.btnAddHolidayProcess = "btnAddHolidayProcess";
HolidayProcessAdmin.prototype.RemoveHolidayAll = "chkRemoveHolidayAll";
HolidayProcessAdmin.prototype.RemoveHolidayProcessRecord = "divHolidayProcess";
HolidayProcessAdmin.prototype.GeneralInvalidDateMessage = "hdnerrorGeneralInvalidDate";
HolidayProcessAdmin.prototype.InvalidNDDDateMessage = "hdnerrorInvalidNDDDate";
HolidayProcessAdmin.prototype.InvalidArrangedNDDDateMessage = "hdnerrorInvalidArrangedNDDDate";
HolidayProcessAdmin.prototype.InvalidDatesMessage = "hdnerrorInvalidDates";
HolidayProcessAdmin.prototype.HolidayOverlappingMessage = "hdnerrorHolidayOverlapping";
HolidayProcessAdmin.prototype.GeneralNoModificationMessage = "hdnerrorGeneralNoModification";
HolidayProcessAdmin.prototype.TrackDirty = "divTrackDirty";
HolidayProcessAdmin.prototype.isTrackDirty = false;
HolidayProcessAdmin.prototype.GeneralOk = "hdnGeneralOk";
HolidayProcessAdmin.prototype.GeneralCancel = "hdnGeneralCancel";

// Form declaration
function HolidayProcessAdmin() { }

// Display error message
HolidayProcessAdmin.prototype.DisplayErrorMessage = function (messageToDisplay) {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";
    errorMsg += errorMsgUl + errorMsgLi + messageToDisplay + errorMsgLiclose + errorMsgUlclose;
    $jqGrid("#" + HolidayProcessAdminObj.ErrorMessage).html(errorMsg);
    ShowErrorMessage(HolidayProcessAdminObj.ErrorMessage);
    return false;
}

// Show track dirty message
HolidayProcessAdmin.prototype.ShowTrackDirtyMessage = function () {
    if (HolidayProcessAdminObj.isTrackDirty) {
        $jqGrid('#' + HolidayProcessAdminObj.TrackDirty).dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',
            resizable: false,
            buttons: [{
                text: $jqGrid("#" + HolidayProcessAdminObj.GeneralOk).val().toString(),
                "id": HolidayProcessAdminObj.GeneralOk,
                click: function () {
                    $jqGrid('#' + HolidayProcessAdminObj.TrackDirty).dialog('close');
                    HolidayProcessAdminObj.ResetAll(true);
                },
            }, {
                text: $jqGrid("#" + HolidayProcessAdminObj.GeneralCancel).val().toString(),
                "id": HolidayProcessAdminObj.GeneralCancel,
                click: function () {
                    $jqGrid('#' + HolidayProcessAdminObj.TrackDirty).dialog('close');
                },
            }]
        });//dailog

        $jqGrid('#' + HolidayProcessAdminObj.TrackDirty).dialog('open');
    }
    else {
        HolidayProcessAdminObj.ResetAll(true);
    }
}

// Reset all controls from the page
HolidayProcessAdmin.prototype.ResetAll = function (flag) {

    if (flag) {
        $jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown).val($("#" + HolidayProcessAdminObj.CustomerDropdown + " option").eq(0).val());
    }

    $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('clearGridData');
    $jqGrid("#" + HolidayProcessAdminObj.StartHour).val($("#" + HolidayProcessAdminObj.StartHour + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.StartMinute).val($("#" + HolidayProcessAdminObj.StartMinute + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.EndHour).val($("#" + HolidayProcessAdminObj.EndHour + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.EndMinute).val($("#" + HolidayProcessAdminObj.EndMinute + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.StartFormat).val($("#" + HolidayProcessAdminObj.StartFormat + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.EndFormat).val($("#" + HolidayProcessAdminObj.EndFormat + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.RemoveNDDMessage).hide();
    var currentDate = HolidayProcessAdminObj.SetCurrentDate();
    $jqGrid("#" + HolidayProcessAdminObj.NDD).val(currentDate);
    $jqGrid("#" + HolidayProcessAdminObj.ArrangedNDD).val(currentDate);
    HolidayProcessAdminObj.isTrackDirty = false;
    return false;
}

// Set current date
HolidayProcessAdmin.prototype.SetCurrentDate = function () {
    var dateText = new Date();
    var dd = ("0" + dateText.getDate()).slice(-2);
    var mm = ("0" + (dateText.getMonth() + 1)).slice(-2);
    var yyyy = dateText.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
}

// Format NDD rows
HolidayProcessAdmin.prototype.RemoveNDDRows = function (cellvalue, options, rowobject) {
    return "<input type='checkbox' class='chkRemoveClass' id=" + options['rowId'] + " onchange=HolidayProcessAdminObj.CountChecked(this)>";
}

// Select or deselect all checkbox
HolidayProcessAdmin.prototype.CountChecked = function (checkbox) {
    var numberOfChecked = $jqGrid('.chkRemoveClass:checked').length;
    var totalCheckboxes = $('.chkRemoveClass:checkbox').length;
    if (numberOfChecked == totalCheckboxes) {
        $jqGrid("#" + HolidayProcessAdminObj.RemoveHolidayAll).prop("checked", true);
    }
    else {
        $jqGrid("#" + HolidayProcessAdminObj.RemoveHolidayAll).prop("checked", false);
    }

    if (checkbox.checked) {
        HolidayProcessAdminObj.ShowRemoveWarningMessage();
    }
    return false;
}

// Convert string to Date format 
HolidayProcessAdmin.prototype.ConvertDateFormat = function (dateText) {
    if (dateText instanceof Date && !isNaN(dateText.valueOf())) {
        dateText = new Date(dateText);
        var dd = ("0" + dateText.getDate()).slice(-2);
        var mm = ("0" + (dateText.getMonth() + 1)).slice(-2);
        var yyyy = dateText.getFullYear();
        return yyyy + '-' + mm + '-' + dd;
    }
    else {
        var calDate = new Date();
        var dateText = dateText.split("/");
        calDate.setDate(dateText[0]);
        calDate.setMonth(dateText[1] - 1);
        calDate.setFullYear(dateText[2]);
        calDate.setDate(calDate.getDate());
        var dd = ("0" + calDate.getDate()).slice(-2);
        var mm = ("0" + (calDate.getMonth() + 1)).slice(-2);
        var yyyy = calDate.getFullYear();
        return yyyy + '-' + mm + '-' + dd;
    }
}//end function

// Get Time slot to displat in the grid
HolidayProcessAdmin.prototype.GetTime = function (hrId, minId, format, type) {
    var startTime;
    if ($jqGrid("#" + hrId + " option:selected").val() == "" && $jqGrid("#" + minId + " option:selected").val() == "") {
        if (type) {
            HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.ErrorStartTimeMessage).val());
        }
        else {
            HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.ErrorEndTimeMessage).val());
        }
        return false;
    }
    else {

        var defaultHour = "00";
        var defaultMIn = "00";
        if ($jqGrid("#" + hrId + " option:selected").val() != "") {
            defaultHour = $jqGrid("#" + hrId + " option:selected").val();
        }
        if ($jqGrid("#" + minId + " option:selected").val() != "") {
            defaultMIn = $jqGrid("#" + minId + " option:selected").val();
        }
        startTime = defaultHour + ":" + defaultMIn + " " + $jqGrid("#" + format + " option:selected").val();
        return startTime;
    }
}

// Get Date into 24 hours format
HolidayProcessAdmin.prototype.ConvertInto24HourFormat = function (time) {
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return sHours + ":" + sMinutes + " " + AMPM;
}

// Format NDD rows
HolidayProcessAdmin.prototype.CheckSameTimeSlot = function (date, startTime, endTime) {
    var gridRows = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getRowData');
    var ids = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getDataIDs');

    for (var i = 0 ; i < gridRows.length; i++) {
        var rowId = ids[i];

        // get NDD
        var isValidNDD = $jqGrid("#" + rowId + "_NDD").val();
        if (isValidNDD == undefined) {
            isValidNDD = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "NDD");
        }

        // get Start Time
        var isValidStartTime = $jqGrid("#" + rowId + "_StartTime").val();
        if (isValidStartTime == undefined) {
            isValidStartTime = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "StartTime");
        }

        // get End Time
        var isValidEndTime = $jqGrid("#" + rowId + "_EndTime").val();
        if (isValidEndTime == undefined) {
            isValidEndTime = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "EndTime");
        }

        var dtNDD = HolidayProcessAdminObj.ConvertDateFormat(isValidNDD);
        var dtDate = HolidayProcessAdminObj.ConvertDateFormat(date);

        if ((dtDate == dtNDD) && (startTime == isValidStartTime) && (endTime == isValidEndTime)) {
            return true;

        }

        if ((dtDate == dtNDD) && (startTime < isValidEndTime) && (startTime > isValidStartTime)) {
            return true;
        }

        if ((dtDate == dtNDD) && (endTime < isValidEndTime) && (endTime > isValidStartTime)) {
            return true;
        }

        if ((dtDate == dtNDD) && (startTime <= isValidStartTime && endTime >= isValidEndTime)) {
            return true;
        }

    }
    return false;
}

HolidayProcessAdmin.prototype.ShowRemoveWarningMessage = function () {    
    $jqGrid('#' + HolidayProcessAdminObj.RemoveHolidayProcessRecord).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + HolidayProcessAdminObj.GeneralOk).val().toString(),
            "id": HolidayProcessAdminObj.GeneralOk,
            click: function () {
                $jqGrid('#' + HolidayProcessAdminObj.RemoveHolidayProcessRecord).dialog('close');
                $jqGrid('.chkRemoveClass').each(function () {
                    if ($jqGrid(this).is(':checked')) {
                        var rowID = $(this).closest("tr").attr('id');
                        $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('delRowData', rowID);
                        HolidayProcessAdminObj.isTrackDirty = true;
                    }
                });

                var numberOfChecked = $jqGrid('.chkRemoveClass:checked').length;
                if (numberOfChecked == 0) {
                    $jqGrid("#" + HolidayProcessAdminObj.RemoveHolidayAll).prop("checked", false);
                }
            },
        }, {
            text: $jqGrid("#" + HolidayProcessAdminObj.GeneralCancel).val().toString(),
            "id": HolidayProcessAdminObj.GeneralCancel,
            click: function () {
                $jqGrid('#' + HolidayProcessAdminObj.RemoveHolidayProcessRecord).dialog('close');
                $jqGrid(".chkRemoveClass").prop("checked", false);
                $jqGrid("#" + HolidayProcessAdminObj.RemoveHolidayAll).prop("checked", false);

            },
        }]
    });//dailog

    $jqGrid('#' + HolidayProcessAdminObj.RemoveHolidayProcessRecord).dialog('open');
}


// Add customer details in the grid
HolidayProcessAdmin.prototype.AddCustomerDetails = function () {
    $jqGrid("#" + HolidayProcessAdminObj.RemoveNDDMessage).hide();
    var nddDate = $("#" + HolidayProcessAdminObj.NDD).val();
    var arrangedDate = $("#" + HolidayProcessAdminObj.ArrangedNDD).val();
    var currentDate = HolidayProcessAdminObj.SetCurrentDate();
    var selectedVal = $jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown + " option:selected").val();

    // Check if customer is selected or not.
    if ($jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown + " option:selected").text() == 'Select Customer') {
        HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.InvalidCustomer).val());
        return false;
    }

    var checkVal;
    checkVal = HolidayProcessAdminObj.IsDate(nddDate);
    if (!checkVal) {
        HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.GeneralInvalidDateMessage).val());
        return false;
    }

    checkVal = HolidayProcessAdminObj.IsDate(arrangedDate);
    if (!checkVal) {
        HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.GeneralInvalidDateMessage).val());
        return false;
    }

    // NDD date should not be in the past.
    var dtNdd = HolidayProcessAdminObj.ConvertDateFormat(nddDate);
    var dtArrangedNdd = HolidayProcessAdminObj.ConvertDateFormat(arrangedDate);
    var dtCurrent = HolidayProcessAdminObj.ConvertDateFormat(currentDate);

    if (dtNdd < dtCurrent) {
        HolidayProcessAdminObj.DisplayErrorMessage("NDD " + $jqGrid("#" + HolidayProcessAdminObj.InvalidNDDDateMessage).val());
        return false;
    }

    // Arranged NDD date should not be in the past.
    if (dtArrangedNdd < dtCurrent) {
        HolidayProcessAdminObj.DisplayErrorMessage("Arranged NDD " + $jqGrid("#" + HolidayProcessAdminObj.InvalidArrangedNDDDateMessage).val());
        return false;
    }


    var startTime = HolidayProcessAdminObj.GetTime(HolidayProcessAdminObj.StartHour, HolidayProcessAdminObj.StartMinute, HolidayProcessAdminObj.StartFormat, true);
    if (startTime == false) {
        return false;
    }

    var endTime = HolidayProcessAdminObj.GetTime(HolidayProcessAdminObj.EndHour, HolidayProcessAdminObj.EndMinute, HolidayProcessAdminObj.EndFormat, false);
    if (endTime == false) {
        return false;
    }

    // This will convert time into 24 hours format
    startTime = HolidayProcessAdminObj.ConvertInto24HourFormat(startTime);
    endTime = HolidayProcessAdminObj.ConvertInto24HourFormat(endTime);

    // Start Time Should not be greater than End Time.
    if (startTime >= endTime) {
        HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.InvalidDatesMessage).val());
        return false;
    }

    // Check Overlapping timeslot
    var isOverlapTimeSlot = HolidayProcessAdminObj.CheckSameTimeSlot(nddDate, startTime, endTime);
    if (isOverlapTimeSlot == true) {
        HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.HolidayOverlappingMessage).val() + "for date:  " + nddDate);
        return false;
    }

    var datarow = {
        NDD: nddDate, StartTime: startTime.toString().substring(0, 5), EndTime: endTime.toString().substring(0, 5), ArrangedNDD: arrangedDate, RowStatus: "New", Status: "Active", IsRemoved: false, IsOverriddenFlag: false
    };
    var gridrowdata = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getRowData');
    $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('addRowData', gridrowdata.length + 1, datarow, "first");
    $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).trigger('reloadGrid');//added to resolved page 1 of 0
    HolidayProcessAdminObj.isTrackDirty = true;
    return false;
}

// This will load when dropdown gets selected.
HolidayProcessAdmin.prototype.LoadCustomerExistingDetails = function (id) {
    $jqGrid("#" + HolidayProcessAdminObj.RemoveNDDMessage).hide();
    var selectedVal = $jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown + " option:selected").val();

    if ($jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown + " option:selected").text() == 'Select Customer') {
        HolidayProcessAdminObj.ResetAll(false);
        return false;
    }

    $jqGrid("#" + HolidayProcessAdminObj.StartHour).val($("#" + HolidayProcessAdminObj.StartHour + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.StartMinute).val($("#" + HolidayProcessAdminObj.StartMinute + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.EndHour).val($("#" + HolidayProcessAdminObj.EndHour + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.EndMinute).val($("#" + HolidayProcessAdminObj.EndMinute + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.StartFormat).val($("#" + HolidayProcessAdminObj.StartFormat + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.EndFormat).val($("#" + HolidayProcessAdminObj.EndFormat + " option").eq(0).val());
    $jqGrid("#" + HolidayProcessAdminObj.RemoveNDDMessage).hide();
    var currentDate = HolidayProcessAdminObj.SetCurrentDate();

    $jqGrid("#" + HolidayProcessAdminObj.NDD).val(currentDate);
    $jqGrid("#" + HolidayProcessAdminObj.ArrangedNDD).val(currentDate);
    HolidayProcessAdminObj.isTrackDirty = false;

    $jqGrid.ajax({
        url: HolidayProcessAdminObj.GetCustomerDetails,
        type: 'GET',
        dataType: 'json',
        async: false,        
        data: {
            customerId: selectedVal
        },
        cache: false,
        success: function (retData) {
            $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('clearGridData');
            if (retData.CustomerCount > 0) {
                $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('setGridParam', { datatype: 'local', data: retData.HolidayProcessAdminDetails }).trigger('reloadGrid');
            }

        } // end succes function
    });

    return false;
}

// This will validate before saving.
HolidayProcessAdmin.prototype.ValidateHolidayProcessDetails = function () {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";

    errorMsg = errorMsgUl;

    if ($jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown + " option:selected").text() == 'Select Customer') {
        errorMsg += errorMsgLi + $jqGrid("#" + HolidayProcessAdminObj.InvalidCustomer).val() + errorMsgLiclose;
    }

    return errorMsg + errorMsgUlclose;
}

// Save Holiday Process data
HolidayProcessAdmin.prototype.SaveHolidayProcessDetails = function () {
    var errmsg = HolidayProcessAdminObj.ValidateHolidayProcessDetails();
    $jqGrid("#" + HolidayProcessAdminObj.ErrorMessage).html(errmsg);

    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        ShowErrorMessage(HolidayProcessAdminObj.ErrorMessage);
        return false;
    }
    else {
        $jqGrid("#" + HolidayProcessAdminObj.RemoveNDDMessage).hide();

        if (!HolidayProcessAdminObj.isTrackDirty) {
            HolidayProcessAdminObj.DisplayErrorMessage($jqGrid("#" + HolidayProcessAdminObj.GeneralNoModificationMessage).val());
            return false;
        }

        var objNDDList = [];
        var gridRows = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getRowData');
        var ids = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getDataIDs');

        for (var i = 0 ; i < gridRows.length; i++) {
            var rowId = ids[i];

            // get NDD
            var isValidNDD = $jqGrid("#" + rowId + "_NDD").val();
            if (isValidNDD == undefined) {
                isValidNDD = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "NDD");
            }

            // get Start Time
            var isValidStartTime = $jqGrid("#" + rowId + "_StartTime").val();
            if (isValidStartTime == undefined) {
                isValidStartTime = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "StartTime");
            }

            // get End Time
            var isValidEndTime = $jqGrid("#" + rowId + "_EndTime").val();
            if (isValidEndTime == undefined) {
                isValidEndTime = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "EndTime");
            }

            // get Arranged NDD
            var isValidArrangedNDD = $jqGrid("#" + rowId + "_ArrangedNDD").val();
            if (isValidArrangedNDD == undefined) {
                isValidArrangedNDD = $jqGrid("#" + HolidayProcessAdminObj.HolidayProcessAdminGrid).jqGrid('getCell', rowId, "ArrangedNDD");
            }

            // convert to date format before saving.
            isValidNDD = HolidayProcessAdminObj.ConvertDateFormat(isValidNDD);
            isValidArrangedNDD = HolidayProcessAdminObj.ConvertDateFormat(isValidArrangedNDD);

            var objCustomerNDD = {
                NDD: isValidNDD,
                StartTime: isValidNDD + " " + isValidStartTime,
                EndTime: isValidNDD + " " + isValidEndTime,
                ArrangedNDD: isValidArrangedNDD
            }
            objNDDList.push(objCustomerNDD);
        }

        var customerId = $jqGrid("#" + HolidayProcessAdminObj.CustomerDropdown + " option:selected").val();

        $.ajax({
            url: HolidayProcessAdminObj.SaveUrl,
            type: 'POST',
            dataType: 'json',
            async: false,
            cache: false,
            data: {
                objCustomerList: objNDDList, objCustomerId: customerId
            },
            success: function (data) {
                $(".prepended").remove();
                if (data) {
                    ShowErrorMessage(HolidayProcessAdminObj.SaveMessage);
                    HolidayProcessAdminObj.isTrackDirty = false;
                }
                else {
                    ShowErrorMessage(HolidayProcessAdminObj.ErrorMessage);
                }
                return false;
            }
        });
    }
}

HolidayProcessAdmin.prototype.IsDate = function (currVal) {
    if (currVal == '')
        return false;

    //Declare Regex 
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for dd/mm/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12) {
        return false;
    }
    else if (dtDay < 1 || dtDay > 31) {
        return false;
    }
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
        return false;
    }

    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }

    return true;
}

