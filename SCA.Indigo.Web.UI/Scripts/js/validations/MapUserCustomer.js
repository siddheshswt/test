﻿$jqGrid = jQuery.noConflict();

var MapUserCustomerObj = new MapUserCustomer();
MapUserCustomer.prototype.objSelectedCustomerList = [];
MapUserCustomer.prototype.UserToCustomerDropdown = "ddlUserToCustomer";
MapUserCustomer.prototype.SaveUrl = "/UserAdmin/SaveUserToCustomers";
MapUserCustomer.prototype.GetAllUrl = "/UserAdmin/GetUserToAllCustomer";
MapUserCustomer.prototype.CheckMapCustomerClass = "chkMapCustomerClass";
MapUserCustomer.prototype.MapUserCustomerGrid = "jqMapUserCustomer";
MapUserCustomer.prototype.Loader = "data_loader";
MapUserCustomer.prototype.ErrorMessage = "divErrorMessage";
MapUserCustomer.prototype.NoDatabaseChange = "divNoDatabaseChange";
MapUserCustomer.prototype.OutPutMessage = "divOutPutMessage";
MapUserCustomer.prototype.NotAlreadyMapped = "divNotAlreadyMapped";
MapUserCustomer.prototype.ErrorUserToCustomer = "hdnerrorUserToCustomer";
MapUserCustomer.prototype.ErrorInvaliSelection = "hdnErrorInvaliSelection";
MapUserCustomer.prototype.CustomerId = "CustomerId";
MapUserCustomer.prototype.MapCustomer = "MapCustomer";
MapUserCustomer.prototype.ContentType = "application/json";
MapUserCustomer.prototype.Body = "body";
MapUserCustomer.prototype.GeneralYes = "hdnGeneralYes";
MapUserCustomer.prototype.GeneralNo = "hdnGeneralNo";

function MapUserCustomer() { }

// Reset all controls from the page
function ResetAll() {
    var resetMessage = $jqGrid("#hdnUserAdminResetMessage").val();
    $jqGrid('#divOutPutMessage').html(resetMessage);

    $jqGrid('#divOutPutMessage').dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: false,
        buttons: [{
            text: $jqGrid("#" + MapUserCustomerObj.GeneralYes).val().toString(),
            "id": MapUserCustomerObj.GeneralYes,
            click: function () {
                $jqGrid('#divOutPutMessage').dialog('close');

                ClearGridData(false);
                $("#" + MapUserCustomerObj.UserToCustomerDropdown).val($("#" + MapUserCustomerObj.UserToCustomerDropdown + " option").eq(0).val());
                $jqGrid("#jqMapUserCustomer").addClass("btn_disable");
                $jqGrid("#jqMapUserCustomer").find("[type='checkbox']").attr("disabled", "disabled");
                $jqGrid("#jqMapUserCustomer").find("[type='checkbox']").attr('readonly', 'true');
                return false;
            },
        }, {
            text: $jqGrid("#" + MapUserCustomerObj.GeneralNo).val().toString(),
            "id": MapUserCustomerObj.GeneralNo,
            click: function () {
                $jqGrid('#divOutPutMessage').dialog('close');
            },
        }]
    });
    $jqGrid('#divOutPutMessage').dialog('open');
    return false;
}

// This will check original value with modified value if both are same then this will return instead of database
function CheckArraysAreIdentical(arr1, arr2) {
    if (arr1.length !== arr2.length) return false;
    for (var i = 0, len = arr1.length; i < len; i++) {
        if (arr1[i].CustomerID != arr2[i].CustomerID) {
            return false;
        }
    }
    return true;
}

// Save User Customer mapping
function SaveMapUserToCustomer() {
    var selectedUserText = $("#ddlUserToCustomer :selected").text();
    var errmsg = ValidateUserToCustomerInfo();
    $jqGrid("#" + MapUserCustomerObj.ErrorMessage).html(errmsg);

    if (errmsg != "" && errmsg != "<ul class='pop_error_ul'></ul>") {
        ShowErrorMessage(MapUserCustomerObj.ErrorMessage);
        return false;
    }
    else {
        var objCustomerList = [];
        var iRow = 0;
        $jqGrid("." + MapUserCustomerObj.CheckMapCustomerClass).each(function () {
            var gridRows = $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('getRowData');
            var ids = $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('getDataIDs');

            if ($(this).is(":checked")) {
                var rowId = ids[iRow];

                // get customer id
                var isValidCustomerId = $jqGrid("#" + rowId + "_" + MapUserCustomerObj.CustomerId).val();
                if (isValidCustomerId == undefined) {
                    isValidCustomerId = $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('getCell', rowId, MapUserCustomerObj.CustomerId);
                }

                var objCustomer = {
                    CustomerID: isValidCustomerId
                }
                objCustomerList.push(objCustomer);
            }
            iRow = iRow + 1;
        });

        //// Check dirty
        //var isDirty = CheckArraysAreIdentical(MapUserCustomerObj.objSelectedCustomerList, objCustomerList);
        //if (isDirty) {
        //    ShowErrorMessage(MapUserCustomerObj.NoDatabaseChange);
        //    return false;
        //}

        $jqGrid("#" + MapUserCustomerObj.ErrorMessage).hide();
        $.ajax({
            url: MapUserCustomerObj.SaveUrl,
            type: 'POST',
            contentType: MapUserCustomerObj.ContentType,
            dataType: 'json',
            data: JSON.stringify({ objCustomerList: objCustomerList, objUserId: $jqGrid("#" + MapUserCustomerObj.UserToCustomerDropdown + " option:selected").val() }),
            async: false,
            cache: false,
            success: function (data) {
                $("#divOutPutMessage").text($("#hdnresOutPutMessage").val().replace("{0}", selectedUserText));
                $(".prepended").remove();
                if (data) {
                    ShowErrorMessage(MapUserCustomerObj.OutPutMessage);
                    //ResetAll();
                }
                else {
                    ShowErrorMessage(MapUserCustomerObj.ErrorMessage);
                }
                return false;
            }
        });
    }
}

// This will validate before saving.
function ValidateUserToCustomerInfo() {
    var errorMsg = "";
    var newline = "</br>";
    var errorMsgUl = "<ul class='pop_error_ul'>";
    var errorMsgUlclose = "</ul>";
    var errorMsgLi = "<li>";
    var errorMsgLiclose = "</li>";

    errorMsg = errorMsgUl;

    if ($jqGrid("#" + MapUserCustomerObj.UserToCustomerDropdown + " option:selected").val() == "") {
        errorMsg += errorMsgLi + $jqGrid("#" + MapUserCustomerObj.ErrorUserToCustomer).val() + errorMsgLiclose;
    }

    //var anyBoxesChecked = false;
    //$jqGrid("." + MapUserCustomerObj.CheckMapCustomerClass).each(function () {
    //    if ($(this).is(":checked")) {
    //        anyBoxesChecked = true;
    //    }
    //});

    //if (anyBoxesChecked == false) {
    //    errorMsg += errorMsgLi + $jqGrid("#" + MapUserCustomerObj.ErrorInvaliSelection).val() + errorMsgLiclose;
    //}

    return errorMsg + errorMsgUlclose;
}

// Build checkbox along with id
function FormatMapCustomer(cellvalue, options, rowobject) {
    return "<input type='checkbox' class='" + MapUserCustomerObj.CheckMapCustomerClass + "' id=" + options['rowId'] + "_" + options.colModel["name"] + ">";
}

// This will load all values initially else also gets called when dropdown gets selected.
function LoadUserToCustomer(flag) {
    ClearGridData(false);
    var selectedUser = $("#ddlUserToCustomer :selected").val();
    var selectedVal = $jqGrid("#" + MapUserCustomerObj.UserToCustomerDropdown + " option:selected").val();

    if (flag == false && $jqGrid("#" + MapUserCustomerObj.UserToCustomerDropdown + " option:selected").val() == '') {
        return false;
    }

    $jqGrid("#jqMapUserCustomer").removeClass("btn_disable");
    $jqGrid("#jqMapUserCustomer").find("[type='checkbox']").removeAttr("disabled");
    $jqGrid("#jqMapUserCustomer").find("[type='checkbox']").removeAttr('readonly');

    $jqGrid("#" + MapUserCustomerObj.Loader).show();
    $jqGrid("#" + MapUserCustomerObj.Body).css('overflow', 'hidden');

    $jqGrid.ajax({
        url: MapUserCustomerObj.GetAllUrl,
        type: 'GET',
        contentType: MapUserCustomerObj.ContentType,
        dataType: 'json',
        data: { userID: selectedVal, isFirstTime: flag },
        async: false,
        cache: false,
        success: function (retData) {
            if (retData.CustomerCount == 0) {
                ClearGridData(false);
            }
            else {
                var userAdminList;
                if (retData.IsFirstTime) {
                    $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('clearGridData');
                    userAdminList = retData.UsersBusinessModel;
                    for (a = 0; a < userAdminList.length; a++) {
                        var row = userAdminList[a];
                        var rowIndex = a + 1;

                        var datarow = { SrNo: rowIndex, SAPCustomerNumber: row.SAPCustomerNumber, CustomerId: row.CustomerId, CustomerName: row.CustomerName };
                        $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid("addRowData", rowIndex, datarow, "last");
                    }
                    $jqGrid("#jqMapUserCustomer").jqGrid('setGridParam', { datatype: 'local' }).trigger('reloadGrid');
                }
                else {
                    var gridRows = $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('getRowData');
                    var ids = $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('getDataIDs');
                    $jqGrid("#jqMapUserCustomer").jqGrid('setGridParam', {
                        datatype: 'local'
                    }).trigger('reloadGrid');

                    userAdminList = retData.UsersBusinessModel;
                    ClearGridData(false);

                    MapUserCustomerObj.objSelectedCustomerList = [];
                    for (var i = 0; i < gridRows.length; i++) {
                        var rowId = ids[i];
                        var row = gridRows[i];

                        // get customer id
                        var isValidCustomerId = $jqGrid("#" + rowId + "_" + MapUserCustomerObj.CustomerId).val();
                        if (isValidCustomerId == undefined) {
                            isValidCustomerId = $jqGrid("#" + MapUserCustomerObj.MapUserCustomerGrid).jqGrid('getCell', rowId, MapUserCustomerObj.CustomerId);
                        }

                        // Loop through all customers return for selected users
                        for (var k = 0; k < userAdminList.length; k++) {
                            if (isValidCustomerId == userAdminList[k].CustomerId) {
                                $jqGrid("#" + rowId + "_" + MapUserCustomerObj.MapCustomer).prop('checked', true)
                                $jqGrid("#" + rowId + "_" + MapUserCustomerObj.MapCustomer).parent("div").addClass("ez-checked");
                                // Keep original values in separate array.
                                var objCustomer = {
                                    CustomerID: isValidCustomerId
                                }
                                MapUserCustomerObj.objSelectedCustomerList.push(objCustomer);
                            }
                        }// end inner for

                    } // end outer for

                } // end inner else condition

                if (selectedUser == "" || $("#ddlUserToCustomer :selected").text() == $("#hdnAdminUserName").val()) {
                    $jqGrid("#jqMapUserCustomer").addClass("btn_disable");
                    $jqGrid("#jqMapUserCustomer").find("[type='checkbox']").attr("disabled", "disabled");
                    $jqGrid("#jqMapUserCustomer").find("[type='checkbox']").attr('readonly', 'true');
                }

            }// end outer else condition

        } // end succes function
    });

    $jqComm("#" + MapUserCustomerObj.Loader).hide();
    $jqComm("#" + MapUserCustomerObj.Body).css('overflow', 'auto');

    return false;
}

// This will check / uncheck checkbox values.
function ClearGridData(flag) {
    if (flag) {
        $jqGrid("." + MapUserCustomerObj.CheckMapCustomerClass).prop("checked", true);
    } else {
        $jqGrid("." + MapUserCustomerObj.CheckMapCustomerClass).prop("checked", false);
        $jqGrid("." + MapUserCustomerObj.CheckMapCustomerClass).parent("div").removeClass("ez-checked");
    }
}

