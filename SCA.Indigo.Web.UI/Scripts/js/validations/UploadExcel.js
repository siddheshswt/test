﻿$jq = jQuery.noConflict();

var UploadExcelObj = new UploadExcel();
UploadExcel.prototype.FileData = new FormData();

function UploadExcel() { }

$jqGrid(document).ready(function () {
    $jqGrid("#btnUpload").addClass("btn_disable");
    $jqGrid('#btnUpload').attr('disabled', 'disabled');
});

// Validate attachment like format and size.
UploadExcel.prototype.ValidateFile = function () {    
    var maxSizeAllowed = $jqGrid("#hdnAppConfigMaxSixe").val() * 1024 * 1024;
    var fileSize = $("#UploadExcelFileControl")[0].files[0].size;
    if (fileSize > maxSizeAllowed) {
        var maxSizeMessage = $jqGrid("#hdnFilzSize").val().replace('{0}', $jqGrid("#hdnAppConfigMaxSixe").val());
        CommonScriptObj.DisplayErrorMessage("divUploadExcelErrorMessage", maxSizeMessage, 'pop_error_ul');
        return false;
    }
    return true;
}

// click event of upload button
UploadExcel.prototype.UploadFile = function (type) {    
    $jqGrid("#data_loader").show();    
    var retVal = UploadExcelObj.ValidateFile();
    if (!retVal)
    {
        return false;
    }
    
    // Call respective upload function depending on the type.    
    MassDataImportObj.ImportData();
}

// change event of file upload control
UploadExcel.prototype.FileUploadChange = function (e) {    
    if (typeof (MassDataChangesObj) != "undefined")
    {
        MassDataChangesObj.isDirty = true;       
        MassDataChangesObj.DiableDownloadButton();
       
    }

    if (typeof (MassDataImportObj) != "undefined")
    {
        MassDataImportObj.IsDirty = true;
    }

    var files = e.files;
    if (files.length > 0) {
        $jqGrid("#btnUpload").removeClass("btn_disable");
        $jqGrid('#btnUpload').removeAttr("disabled");
        MassDataImportObj.FileData.append("file", files[0]);
        MassDataImportObj.FileData.append("Template", $jqGrid("#" + MassDataImportObj.UploadType + " option:selected").val());
    }
    else {
        $jqGrid("#btnUpload").addClass("btn_disable");
        $jqGrid('#btnUpload').attr('disabled', 'disabled');
    }
}
