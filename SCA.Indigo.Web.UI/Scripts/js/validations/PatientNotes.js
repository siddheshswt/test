﻿$jqGrid = jQuery.noConflict();

function showPopupPatientNotes() {
    var dd = $jqGrid("#divPatientNotesPopup").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        title: $jqGrid('#hdnresPatientNoteTitle').val()
    });
    dd.dialog('open');
    $jqGrid("#jqPatientNotes").jqGrid('clearGridData');
    LoadPatientNotes();
    $jqGrid('#divErrorNote').hide();
}



$jqGrid("#btnAddPatientNote").click(function () {
    var txtNote = $jqGrid("#txtNote").val();
    if (txtNote.trim() == "") {
        $jqGrid('#divErrorNote').show();
        return false;
    } else {
        $jqGrid('#divErrorNote').hide();
        $jqGrid.ajax({
            url: '/Patient/AddPatientNote',
            type: 'POST',
            async: false,
            cache: false,
            data: { noteText: txtNote },
            cache: false,
            success: function (data) {
                $jqGrid("#txtNote").val('');
                $jqGrid('#divErrorNote').hide();
                LoadPatientNotes();
            }
        });
    }
    return true;
});


function LoadPatientNotes() {
    $jqGrid.ajax({
            url: '/' + $jqGrid("#hdnPatientController").val() + '/' + $jqGrid("#hdnLoadPatientNotes").val(),
            type:'GET',
            cache: false,
            data: { page: 1, rows : 50 },
            contentType: 'application/json',
            dataType: "json",
            async:false,
            success: function (responseData) {
            $jqGrid("#jqPatientNotes").jqGrid('clearGridData');
            $jqGrid("#jqPatientNotes").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid'); 
        }
        });
}