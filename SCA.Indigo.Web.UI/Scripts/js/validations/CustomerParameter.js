﻿$jqGrid = jQuery.noConflict();

var CustomerParameterObj = new CustomerParameter();
CustomerParameter.prototype.CustomerName = "txtCPCustomerName";
CustomerParameter.prototype.SAPCustomerId = "txtCPCustomerId";
CustomerParameter.prototype.OrderLeadTime = "txtCPOrderLeadTime";
CustomerParameter.prototype.PaedAgeLimit = "txtCPPaedAgeLimit";
CustomerParameter.prototype.MaxPPD = "txtCPMaxPPD";
CustomerParameter.prototype.DefaultNextAssessmentDate = "txtCPDefaultNextAssessmentDt";
CustomerParameter.prototype.AgeBillTo = "chkCPAgeBillTo";
CustomerParameter.prototype.IsOrderCreationAllowed = "chkCPAllowOrderCreation";
CustomerParameter.prototype.IsNHSIDMandatory = "chkCPNHSID";
CustomerParameter.prototype.IsADPMandatory = "chkCPADP";
CustomerParameter.prototype.IsTelephoneMandatory = "chkCPTelephoneNumber";
CustomerParameter.prototype.IsNextAssessmentDateMandatory = "chkCPNextAssessmentDate";
CustomerParameter.prototype.ddlCPCutOffHour = "ddlCPCutOffHour";
CustomerParameter.prototype.ddlCPCutOffMinute = "ddlCPCutOffMinute";
CustomerParameter.prototype.ddlCPCutOffSecond = "ddlCPCutOffSecond";
CustomerParameter.prototype.jqCPAnalysisInfo = "jqCPAnalysisInfo";
CustomerParameter.prototype.jqCPMedicalStaff = "jqCPMedicalStaff";
CustomerParameter.prototype.AllowQtyIncrease = "chkCPAllowQtyIncrease";
CustomerParameter.prototype.IsNddAllow = "chkCPIsNddAllow";
CustomerParameter.prototype.IsCollectionYes = "CPColl_Yes";
CustomerParameter.prototype.IsCollectionNo = "CPColl_No";
CustomerParameter.prototype.jqCPReasonCode = "jqCPReasonCode";
CustomerParameter.prototype.jqCPStoppedReasonCode = "jqCPStoppedReasonCode";
CustomerParameter.prototype.ButtonPrint = "btnPrint";
CustomerParameter.prototype.AddNewCustomerPostcodeButton = "AddNewCustomerPostcode";
CustomerParameter.prototype.SavePostcodeMatrixButton = "btnSavePostcodeMatrix";
CustomerParameter.prototype.SaveClinicalContactButton = "btnSaveClinicalContact";
CustomerParameter.prototype.SaveAnalysisInfoButton = "btnSaveAnalysisInfo";

CustomerParameter.prototype.divEnterAgeLimit = "divEnterAgeLimit";
CustomerParameter.prototype.divConfirmUndo = "divConfirmUndo";
CustomerParameter.prototype.DivCustomerParameter = "divCustomerParameter";
CustomerParameter.prototype.divCPSaveSuccess = "divCPSaveSuccess";
CustomerParameter.prototype.divCPSaveError = "divCPSaveError";
CustomerParameter.prototype.divCPReasonCodeError = "divCPReasonCodeError";

CustomerParameter.prototype.PostcodeMatrixGrid = "jqPostcodeMatrix";
CustomerParameter.prototype.DivPostcodeMatrixPopup = "divPostcodeMatrixPopup";
CustomerParameter.prototype.CustomerDropDown = "hdnCurrentCustomerId";
CustomerParameter.prototype.CustomerParameterTitle = "hdnCustomerParameterTitle";
CustomerParameter.prototype.CustomerController = "hdnCustomerController";
CustomerParameter.prototype.MessageSelectCustomer = "msgSelectCustomer";
CustomerParameter.prototype.ActionLoadCustomerPostcode = "hdnActionLoadCustomerPostcode";
CustomerParameter.prototype.PostocdePopupTitle = "hdnPostocdePopupTitle";
CustomerParameter.prototype.CustomerSelectMsg = "hdnMsgSelectCustomer";
CustomerParameter.prototype.PostcodeErrorMsg = "PostcodeErrorMsgSpan";
CustomerParameter.prototype.msgPostcodeNotSaved = "msgPostcodeNotSaved";
CustomerParameter.prototype.txtCustomerName = "txtCustomerName";

CustomerParameter.prototype.ActionLoadCustomerParameter = "hdnActionLoadCustomerParameter";
CustomerParameter.prototype.ActionSaveCustomerParameter = "hdnActionSaveCustomerParameter";
CustomerParameter.prototype.ActionSaveCustomerPostcode = "hdnActionSaveCustomerPostcode";
CustomerParameter.prototype.ActionRemoveClinicalContactValue = "hdnActionRemoveClinicalContactValue";
CustomerParameter.prototype.ActionPrintCustomerParameter = "hdnActionPrintCustomerParameter";

CustomerParameter.prototype.PostcodeMonday = "_Monday";
CustomerParameter.prototype.PostcodeTuesday = "_Tuesday";
CustomerParameter.prototype.PostcodeWednesday = "_Wednesday";
CustomerParameter.prototype.PostcodeThursday = "_Thursday";
CustomerParameter.prototype.PostcodeFriday = "_Friday";
CustomerParameter.prototype.PostcodeRemove = "_Remove";
CustomerParameter.prototype.PostcodeRound = "_Round";
CustomerParameter.prototype.PostcodeRoundId = "_RoundID";
CustomerParameter.prototype.PostcodePostcode = "_Postcode";

CustomerParameter.prototype.PostocdePopupTitle = "hdnPostocdePopupTitle";

CustomerParameter.prototype.RoundError = "hdnRoundError";
CustomerParameter.prototype.RoundIdError = "hdnRoundIdError";
CustomerParameter.prototype.PostcodeError = "hdnPostcodeError";
CustomerParameter.prototype.ForRowNumber = "hdnForRowNumber";
CustomerParameter.prototype.DeliveryDayError = "hdnDeliveryDayError";

CustomerParameter.prototype.ClinicalContactListType = "hdnClinicalContactListType";
CustomerParameter.prototype.AnalysisInfoListType = "hdnAnalysisInfoListType";
CustomerParameter.prototype.ClinicalContactRequire = "hdnClinicalContactRequire";
CustomerParameter.prototype.ClinicalContactAlreadyExist = "hdnClinicalContactAlreadyExist";
CustomerParameter.prototype.AnalysisInfoRequire = "hdnAnalysisInfoRequire";
CustomerParameter.prototype.AnalysisInfoAlreadyExist = "hdnAnalysisInfoAlreadyExist";

CustomerParameter.prototype.NewAnalysisInfoTextBox = "newAnalysisInfoTextBox";
CustomerParameter.prototype.NewClinicalContactTextBox = "newClinicalContactTextBox";
CustomerParameter.prototype.ClinicalContactRequireSpan = "spanClinicalContactRequire";
CustomerParameter.prototype.AnalysisInfoRequireSpan = "spanAnalysisRequire";
CustomerParameter.prototype.ClinicalContactLinkage = "clinicalContactLinkage";
CustomerParameter.prototype.ClinicalContactWithoutLinkage = "clinicalContactWithoutLinkage";
CustomerParameter.prototype.ClinicalContactLinkageChecked = "";
CustomerParameter.prototype.ClinicalContactLinkageButton = "btnOpenLinkage";
CustomerParameter.prototype.RemoveClinicalContactAnalysisError = "hdnRemoveClinicalContactAnalysisError";

CustomerParameter.prototype.ClinicalContactType = "ClinicalContact";
CustomerParameter.prototype.AnalysisInformationType = "AnalysisInformation";
CustomerParameter.prototype.ExportCustomerParameterAction = "ExportCustomerParameterAction";
CustomerParameter.prototype.divPrintCustomerparameterPopup = "divPrintCustomerparameterPopup";
CustomerParameter.prototype.PrintCustomerParameterTitle = "hdnPrintCustomerParameterTitle";

// Form declaration
function CustomerParameter() { }

CustomerParameter.prototype.ShowPopupCustomerParameter = function (e) {
    var title = $jqGrid("#" + CustomerParameterObj.CustomerParameterTitle) != null ? $jqGrid("#" + CustomerParameterObj.CustomerParameterTitle).val() : "";
    if ($jqGrid("#" + PatientTypeMatrixObj.ParentCustomerValue + " option:selected").val() == "") {
        CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, $jqGrid("#" + PatientTypeMatrixObj.ParentCustomerMessage).val(), 'pop_error_ul');
        return false;
    }

    $jqGrid("#" + CustomerParameterObj.DivCustomerParameter).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: '960px',
        resizable: true,
        title: title
    });
    $jqGrid("#" + CustomerParameterObj.DivCustomerParameter).dialog('open');
    $jqGrid(".disable_div").remove();
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
    CustomerParameterObj.LoadCustomerParameter(customerId);
}

$jqGrid("#" + CustomerParameterObj.AgeBillTo).change(function () {
    CustomerParameterObj.CheckAgeParameter();
});

CustomerParameter.prototype.CheckAgeParameter = function () {
    if ($jqGrid("#" + CustomerParameterObj.AgeBillTo).is(":checked")) {
        $jqGrid("#" + CustomerParameterObj.PaedAgeLimit).removeClass('btn_disable');
    }
    else {
        $jqGrid("#" + CustomerParameterObj.PaedAgeLimit).val('');
        $jqGrid("#" + CustomerParameterObj.PaedAgeLimit).addClass('btn_disable');
    }

}

// Undo Customer Parameter
CustomerParameter.prototype.ReloadCustomerParameter = function () {
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
    CustomerParameterObj.LoadCustomerParameter(customerId);
}

CustomerParameter.prototype.LoadCustomerParameter = function (customerId) {
    $jqGrid.ajax({
        url: '/' + $jqGrid("#" + CustomerParameterObj.CustomerController).val() + '/' + $jqGrid("#" + CustomerParameterObj.ActionLoadCustomerParameter).val(),
        type: 'GET',
        cache: false,
        data: {
            customerId: customerId
        },
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (responseData) {
            $jqGrid("#" + CustomerParameterObj.CustomerName).val(responseData.CustomerName);
            $jqGrid("#" + CustomerParameterObj.SAPCustomerId).val(responseData.SAPCustomerID);
            $jqGrid("#" + CustomerParameterObj.OrderLeadTime).val(responseData.OrderLeadTime);
            $jqGrid("#" + CustomerParameterObj.PaedAgeLimit).val(responseData.PaedAgeLimit);
            $jqGrid("#" + CustomerParameterObj.MaxPPD).val(responseData.MaxPPD);
            $jqGrid("#" + CustomerParameterObj.DefaultNextAssessmentDate).val(responseData.DefaultNextAssessmentDate);
            $jqGrid("#" + CustomerParameterObj.ddlCPCutOffHour).val(responseData.CutOffHour);
            $jqGrid("#" + CustomerParameterObj.ddlCPCutOffMinute).val(responseData.CutOffMinute);
            $jqGrid("#" + CustomerParameterObj.ddlCPCutOffSecond).val(responseData.CutOffSecond);

            $("#" + CustomerParameterObj.AgeBillTo).prop('checked', responseData.AgeBillTo);
            $jqGrid("#" + CustomerParameterObj.AgeBillTo).closest("div").toggleClass("ez-checked", responseData.AgeBillTo);
            CustomerParameterObj.CheckAgeParameter();

            $("#" + CustomerParameterObj.AllowQtyIncrease).prop('checked', responseData.AllowIncreaseOrderQuantity);
            $jqGrid("#" + CustomerParameterObj.AllowQtyIncrease).closest("div").toggleClass("ez-checked", responseData.AllowIncreaseOrderQuantity);

            $("#" + CustomerParameterObj.IsNddAllow).prop('checked', responseData.IsNDDAllow);
            $jqGrid("#" + CustomerParameterObj.IsNddAllow).closest("div").toggleClass("ez-checked", responseData.IsNDDAllow);

            $("#" + CustomerParameterObj.IsNextAssessmentDateMandatory).prop('checked', responseData.IsNextAssessmentDateMandatory);
            $jqGrid("#" + CustomerParameterObj.IsNextAssessmentDateMandatory).closest("div").toggleClass("ez-checked", responseData.IsNextAssessmentDateMandatory);

            $("#" + CustomerParameterObj.IsOrderCreationAllowed).prop('checked', responseData.IsOrderCreationAllowed);
            $jqGrid("#" + CustomerParameterObj.IsOrderCreationAllowed).closest("div").toggleClass("ez-checked", responseData.IsOrderCreationAllowed);

            $("#" + CustomerParameterObj.IsNHSIDMandatory).prop('checked', responseData.IsNHSIDMandatory);
            $jqGrid("#" + CustomerParameterObj.IsNHSIDMandatory).closest("div").toggleClass("ez-checked", responseData.IsNHSIDMandatory);

            $("#" + CustomerParameterObj.IsADPMandatory).prop('checked', responseData.IsADPMandatory);
            $jqGrid("#" + CustomerParameterObj.IsADPMandatory).closest("div").toggleClass("ez-checked", responseData.IsADPMandatory);

            $("#" + CustomerParameterObj.IsADPMandatory).prop('checked', responseData.IsADPMandatory);
            $jqGrid("#" + CustomerParameterObj.IsADPMandatory).closest("div").toggleClass("ez-checked", responseData.IsADPMandatory);

            if (responseData.IsCollectionApplicable) {
                $("#" + CustomerParameterObj.IsCollectionYes).prop("checked", true);
                $jqGrid("#" + CustomerParameterObj.IsCollectionYes).closest("div").toggleClass("ez-selected", true);

                $("#" + CustomerParameterObj.IsCollectionNo).prop("checked", false);
                $jqGrid("#" + CustomerParameterObj.IsCollectionNo).closest("div").toggleClass("ez-selected", false);
            }
            else {
                $("#" + CustomerParameterObj.IsCollectionYes).prop("checked", false);
                $jqGrid("#" + CustomerParameterObj.IsCollectionYes).closest("div").toggleClass("ez-selected", false);

                $("#" + CustomerParameterObj.IsCollectionNo).prop("checked", true);
                $jqGrid("#" + CustomerParameterObj.IsCollectionNo).closest("div").toggleClass("ez-selected", true);
            }

            $("#" + CustomerParameterObj.IsTelephoneMandatory).prop('checked', responseData.IsTelephoneMandatory);
            $jqGrid("#" + CustomerParameterObj.IsTelephoneMandatory).closest("div").toggleClass("ez-checked", responseData.IsTelephoneMandatory);

            $("#" + CustomerParameterObj.IsNextAssessmentDateMandatory).prop('checked', responseData.IsNextAssessmentDateMandatory);
            $jqGrid("#" + CustomerParameterObj.IsNextAssessmentDateMandatory).closest("div").toggleClass("ez-checked", responseData.IsNextAssessmentDateMandatory);

            $jqGrid("#" + CustomerParameterObj.jqCPMedicalStaff).jqGrid('clearGridData');
            $jqGrid("#" + CustomerParameterObj.jqCPAnalysisInfo).jqGrid('clearGridData');

            $jqGrid("#" + CustomerParameterObj.jqCPMedicalStaff).jqGrid('setGridParam', { datatype: 'local', data: responseData.MedicalStaff }).trigger('reloadGrid');
            $jqGrid("#" + CustomerParameterObj.jqCPAnalysisInfo).jqGrid('setGridParam', { datatype: 'local', data: responseData.AnalysisInformation }).trigger('reloadGrid');
            $jqGrid("#" + CustomerParameterObj.jqCPReasonCode).jqGrid('setGridParam', { datatype: 'local', data: responseData.RemovePatientReasonCodes }).trigger('reloadGrid');
            $jqGrid("#" + CustomerParameterObj.jqCPStoppedReasonCode).jqGrid('setGridParam', { datatype: 'local', data: responseData.StopPatientReasonCodes }).trigger('reloadGrid');

            // Reset the Analysis Info Text Box
            $jqGrid("#" + CustomerParameterObj.NewAnalysisInfoTextBox).val('');
            $jqGrid("#" + CustomerParameterObj.NewClinicalContactTextBox).val('');
            $jqGrid("#" + CustomerParameterObj.ClinicalContactRequireSpan).text('');
            $jqGrid("#" + CustomerParameterObj.AnalysisInfoRequireSpan).text('');

            //// Bind the Clinical Linkage as default blank
            CustomerParameterObj.ShowHideClinicalContactLinkage(responseData.IsClinicalContactWithLinkage);

            // Binding the value for Sample Order product Type
            if (responseData.SampleOrderProductType == "TENA") {               

                $("#CP_TENAList").prop("checked", true);
                $jqGrid("#CP_TENAList").closest("div").toggleClass("ez-selected", true);

                $("#CP_CustProdList").prop("checked", false);
                $jqGrid("#CP_CustProdList").closest("div").toggleClass("ez-selected", false);
            }
            else if (responseData.SampleOrderProductType == "Customer") {              

                $("#CP_TENAList").prop("checked", false);
                $jqGrid("#CP_TENAList").closest("div").toggleClass("ez-selected", false);

                $("#CP_CustProdList").prop("checked", true);
                $jqGrid("#CP_CustProdList").closest("div").toggleClass("ez-selected", true);
            }
            else {
                $("#CP_TENAList").prop("checked", true);
                $jqGrid("#CP_TENAList").closest("div").toggleClass("ez-selected", true);

                $("#CP_CustProdList").prop("checked", false);
                $jqGrid("#CP_CustProdList").closest("div").toggleClass("ez-selected", false);
            }
        }
    });
}

CustomerParameter.prototype.SaveCustomerParameter = function () {
    if ($jqGrid("#" + CustomerParameterObj.AgeBillTo).is(":checked")) {
        var ageLimit = $jqGrid("#" + CustomerParameterObj.PaedAgeLimit).val();
        if (ageLimit == undefined || ageLimit == '') {
            $jqGrid("#" + CustomerParameterObj.divEnterAgeLimit).show();
            return false;
        }
    }
    CustomerParameterObj.SaveCustomerParameterInformation();
}

$jqGrid("#" + CustomerParameterObj.OrderLeadTime).keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isNumeric(charCode))
        return true;
    else
        return false;
});

$jqGrid("#" + CustomerParameterObj.PaedAgeLimit).keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isNumeric(charCode))
        return true;
    else
        return false;
});

$jqGrid("#" + CustomerParameterObj.MaxPPD).keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isDecimal(charCode))
        return true;
    else
        return false;
});

$jqGrid("#" + CustomerParameterObj.DefaultNextAssessmentDate).keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (isNumeric(charCode))
        return true;
    else
        return false;
});

CustomerParameter.prototype.SaveCustomerParameterInformation = function () {
    var isCustomerApprovalReq = false; //Need discussion   
    var orderApproval = false; //need to discussion
    var sampleOrderProductType="";
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
    var checkAge = $jqGrid("#" + CustomerParameterObj.AgeBillTo).is(":checked");
    var orderCreationAllowed = $jqGrid("#" + CustomerParameterObj.IsOrderCreationAllowed).is(":checked");
    var isADPMandatory = $jqGrid("#" + CustomerParameterObj.IsADPMandatory).is(":checked");
    var isNextAssessmentDateMandatory = $jqGrid("#" + CustomerParameterObj.IsNextAssessmentDateMandatory).is(":checked");
    var isNHSIDMandatory = $jqGrid("#" + CustomerParameterObj.IsNHSIDMandatory).is(":checked");
    var isTelephoneMandatory = $jqGrid("#" + CustomerParameterObj.IsTelephoneMandatory).is(":checked");
    var isNDDAllow = $jqGrid("#" + CustomerParameterObj.IsNddAllow).is(":checked");
    var allowIncreaseOrderQty = $jqGrid("#" + CustomerParameterObj.AllowQtyIncrease).is(":checked");
    var collectionApplicable = $jqGrid("#" + CustomerParameterObj.IsCollectionYes).is(":checked");
    // Checking for product type for sample order
    if ($("#CP_CustProdList").prop("checked")) {
        sampleOrderProductType = $("#CP_CustProdList").val();
    }
    else {
        sampleOrderProductType = $("#CP_TENAList").val();
    }

    var age = checkAge ? $jqGrid("#" + CustomerParameterObj.PaedAgeLimit).val() : null;
    var maxPadPerDay = $jqGrid("#" + CustomerParameterObj.MaxPPD).val();
    var nextAssessmentDate = $jqGrid("#" + CustomerParameterObj.DefaultNextAssessmentDate).val();

    var hour = $jqGrid("#" + CustomerParameterObj.ddlCPCutOffHour).val();
    if (hour == undefined || hour == "") {
        hour = "00";
    }
    var minute = $jqGrid("#" + CustomerParameterObj.ddlCPCutOffMinute).val();
    if (minute == undefined || minute == "") {
        minute = "00";
    }
    var seconds = $jqGrid("#" + CustomerParameterObj.ddlCPCutOffSecond).val();
    if (seconds == undefined || seconds == "") {
        seconds = "00";
    }
    var orderCutOffTime = hour + ":" + minute + ":" + seconds;
    var orderLeadTime = $jqGrid("#" + CustomerParameterObj.OrderLeadTime).val();

    var isClinicalContactWithLinkage = CustomerParameterObj.ClinicalContactLinkageChecked;

    //loop through medicalAnanlysisInfoes
    var medicalAnalysisList = [];

    var medicalGridRows = $jqGrid("#" + CustomerParameterObj.jqCPMedicalStaff).jqGrid('getRowData');
    for (var i = 0; i < medicalGridRows.length; i++) {
        var row = medicalGridRows[i];
        var medicalAnalysisOject = {
            ListId: row["ListId"],
            ListTypeId: row.ListTypeId,
            DisplayText: row.DisplayText,
            IsActive: row.IsActive,
            IsMandatory: row.IsMandatory,
        }
        medicalAnalysisList.push(medicalAnalysisOject);
    }

    var analysisGridRows = $jqGrid("#" + CustomerParameterObj.jqCPAnalysisInfo).jqGrid('getRowData');
    for (var i = 0; i < analysisGridRows.length; i++) {
        var row = analysisGridRows[i];
        var medicalAnalysisOject = {
            ListId: row["ListId"],
            ListTypeId: row.ListTypeId,
            DisplayText: row.DisplayText,
            IsActive: row.IsActive,
            IsMandatory: row.IsMandatory,
        }
        medicalAnalysisList.push(medicalAnalysisOject);
    }

    var reasonCodes = [];
    var reasonCodeGridRows = $jqGrid("#" + CustomerParameterObj.jqCPReasonCode).jqGrid('getRowData');
    for (var i = 0; i < reasonCodeGridRows.length; i++) {
        var row = reasonCodeGridRows[i];
        var reasonCodeOject = {
            ListId: row["ListId"],
            IsActive: $jqGrid("#" + CustomerParameterObj.jqCPReasonCode + "_" + (i + 1) + "_" + row["ListId"]).is(":checked")
        }
        reasonCodes.push(reasonCodeOject);
    }

    var stoppedReasonCodes = [];
    var stoppedReasonCodeGridRows = $jqGrid("#" + CustomerParameterObj.jqCPStoppedReasonCode).jqGrid('getRowData');
    for (var i = 0; i < stoppedReasonCodeGridRows.length; i++) {
        var row = stoppedReasonCodeGridRows[i];
        var stoppedReasonCodeOject = {
            ListId: row["ListId"],
            IsActive: $jqGrid("#" + CustomerParameterObj.jqCPStoppedReasonCode + "_" + (i + 1) + "_" + row["ListId"]).is(":checked")
        }
        stoppedReasonCodes.push(stoppedReasonCodeOject);
    }

    var customerParameter = {
        CustomerID: customerId,
        AllowIncreaseOrderQty: allowIncreaseOrderQty,
        CheckAge: checkAge,
        Age: age,
        IsADPMandatory: isADPMandatory,
        IsCustomerApprovalReq: isCustomerApprovalReq,
        IsNDDAllow: isNDDAllow,
        IsNextAssessmentDateMandatory: isNextAssessmentDateMandatory,
        IsNHSIDMandatory: isNHSIDMandatory,
        IsOrderAllowed: orderCreationAllowed,
        IsTelephoneMandatory: isTelephoneMandatory,
        MaxPadPerDay: maxPadPerDay,
        NextAssessmentDate: nextAssessmentDate,
        OrderApproval: orderApproval,
        OrderCreationAllowed: orderCreationAllowed,
        OrderCutOffTime: orderCutOffTime,
        OrderLeadTime: orderLeadTime,
        IsCollectionApplicable: collectionApplicable,
        MedicalAnalysisInformation: medicalAnalysisList,
        RemovePatientReasonCodes: reasonCodes,
        StopPatientReasonCodes: stoppedReasonCodes,
        IsClinicalContactWithLinkage: isClinicalContactWithLinkage,
        SampleOrderProductType: sampleOrderProductType
    }

    $jqGrid.ajax({
        url: '/' + $jqGrid("#" + CustomerParameterObj.CustomerController).val() + '/' + $jqGrid("#" + CustomerParameterObj.ActionSaveCustomerParameter).val(),
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        async: false,
        data: JSON.stringify(customerParameter),
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {
            if (responseData == true) {
                $jqGrid("#" + CustomerParameterObj.divCPSaveSuccess).show();
                return false;
            }
            else {
                $jqGrid("#" + CustomerParameterObj.divCPSaveError).show();
                return false;
            }
        },
    });
}

CustomerParameter.prototype.OpenPostcodePopup = function () {
    $jqGrid('#' + CustomerParameterObj.PostcodeErrorMsg + '').hide();
    var title = $jqGrid("#" + CustomerParameterObj.PostocdePopupTitle).val();
    var postCodePopup = $jqGrid("#" + CustomerParameterObj.DivPostcodeMatrixPopup).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        draggable: false,
        title: title
    });
    postCodePopup.dialog('open');
    CustomerParameterObj.LoadCustomerPostcodes();
}

CustomerParameter.prototype.LoadCustomerPostcodes = function () {
    if ($jqGrid("#" + CustomerParameterObj.CustomerDropDown).val() == "") {
        $jqGrid("#" + CustomerParameterObj.PostcodeMatrixGrid).jqGrid('clearGridData');
        CommonScriptObj.DisplayErrorMessage(CustomerParameterObj.MessageSelectCustomer, $jqGrid('#' + CustomerParameterObj.CustomerSelectMsg).val(), 'pop_error_ul');
        $jqGrid("body").css('overflow', 'auto');
    }//Select Customer
    else {
        $jqGrid("body").css('overflow', 'hidden');
        var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
        $jqGrid.ajax({
            url: '/' + $jqGrid("#" + CustomerParameterObj.CustomerController).val() + '/' + $jqGrid("#" + CustomerParameterObj.ActionLoadCustomerPostcode).val(),
            type: 'GET',
            cache: false,
            data: {
                page: 1, rows: 50, customerId: customerId
            },
            contentType: 'application/json',
            dataType: "json",
            async: false,
            success: function (responseData) {
                $jqGrid("#" + CustomerParameterObj.PostcodeMatrixGrid).jqGrid('clearGridData');
                $jqGrid("#" + CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setGridParam', {
                    datatype: 'local', data: responseData
                }).trigger('reloadGrid');
            }
        });
    }
}

CustomerParameter.prototype.SelectMedicalStaff = function (cellvalue, options, rowobject) {
    if (cellvalue == true) {
        return "<input type='checkbox' checked = 'checked' class='chkMedicalStaff'  id=" + CustomerParameterObj.jqCPMedicalStaff + '_' + options['rowId'] + "_" + rowobject["ListId"] + ">";
    }
    else {
        return "<input type='checkbox' class='chkMedicalStaff' id=" + CustomerParameterObj.jqCPMedicalStaff + '_' + options['rowId'] + "_" + rowobject["ListId"] + ">";
    }
}

CustomerParameter.prototype.SelectAnalysisInfo = function (cellvalue, options, rowobject) {
    if (cellvalue == true) {
        return "<input type='checkbox' checked = 'checked' class='chkAnalysisInfo' id=" + CustomerParameterObj.jqCPAnalysisInfo + '_' + options['rowId'] + "_" + rowobject["ListId"] + ">";
    }
    else {
        return "<input type='checkbox' class='chkAnalysisInfo' id=" + CustomerParameterObj.jqCPAnalysisInfo + '_' + options['rowId'] + "_" + rowobject["ListId"] + ">";
    }
}

CustomerParameter.prototype.SelectReasonCode = function (cellvalue, options, rowobject) {
    if (cellvalue == true) {
        return "<input type='checkbox' checked = 'checked' onchange='ReasonCodeChange(this," +rowobject["ListId"] + ");' class='chkReasonCodes' id=" +CustomerParameterObj.jqCPReasonCode + '_' +options['rowId']+ "_" + rowobject["ListId"]+ ">";
        }
        else {
        return "<input type='checkbox' onchange='ReasonCodeChange(this," + rowobject["ListId"]+ ");'  class='chkAnalysisInfo' id=" + CustomerParameterObj.jqCPReasonCode + '_' +options['rowId']+ "_" +rowobject["ListId"]+ ">";
        }
        }

CustomerParameter.prototype.SelectStoppedReasonCode = function (cellvalue, options, rowobject) {

    if(cellvalue == true) {
        return "<input type='checkbox' checked = 'checked' onchange='ReasonCodeChange(this," +rowobject["ListId"]+ ");' class='chkStoppedReasonCodes' id=" + CustomerParameterObj.jqCPStoppedReasonCode + '_' +options['rowId']+ "_" +rowobject["ListId"]+ ">";
        }
        else {
        return "<input type='checkbox' class='chkStoppedReasonCodes'  onchange='ReasonCodeChange(this," +rowobject["ListId"]+ ");' id=" +CustomerParameterObj.jqCPStoppedReasonCode + '_' +options['rowId']+ "_" + rowobject["ListId"]+ ">";
        }
        }

function ReasonCodeChange(e, reasonCodeId)
        {
    if (!e.checked) {
        var customerId = $jqGrid("#" +CustomerParameterObj.CustomerDropDown).val();
            //checked for patients with the reasonCodeId attached for the selected customer
            $jqGrid.ajax({
                    url: '/' +$jqGrid("#" +CustomerParameterObj.CustomerController).val() + '/' + 'RemoveReasonCodeMapping',
                type: 'GET',
                    cache: false,
                    data: {
                    customerId : customerId, reasonCodeId: reasonCodeId
            },
                contentType: 'application/json',
                dataType: "json",
                async: false,
                success: function (responseData) {
                        if (!responseData)
            {
                            e.checked = true;
                            $jqGrid("#" +CustomerParameterObj.divCPReasonCodeError).show();
                }
                            }
                            });
                            }
                            }

                $jqGrid('#' +CustomerParameterObj.AddNewCustomerPostcodeButton).click(function () {
            var customerId = $jqGrid('#' +CustomerParameterObj.CustomerDropDown).val();
            var gridrowdata = $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getRowData');
            var datarow = {
                CustomerID: customerId, Round : "", RoundID : "", isNewRow: "new"
                };
            $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid + "").jqGrid('addRowData', gridrowdata.length +1, datarow, "first");
            });

        CustomerParameter.prototype.SetEditableRows = function () {
            var rowIds = $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getDataIDs');
            for (var i = 0; i <= rowIds.length; i++) {
                var rowData = $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).getRowData(i);
                isNewRow = rowData['isNewRow'];
                    if (isNewRow != "new") {
                        $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setCell', i, 'Round', '', 'not-editable-cell');
                        $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setCell', i, 'RoundID', '', 'not-editable-cell');
                        $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setCell', i, 'Postcode', '', 'not-editable-cell');
                        }
        else {
            $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setCell', i, 'Round', '', 'editable-cell');
                        $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setCell', i, 'RoundID', '', 'editable-cell');
                        $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setCell', i, 'Postcode', '', 'editable-cell');
                        }
                        }
                        }

                            $jqGrid('#' + CustomerParameterObj.SavePostcodeMatrixButton).click(function () {
                                var customerId = $jqGrid('#' +CustomerParameterObj.CustomerDropDown).val();
                                var isValid = CustomerParameterObj.ValidateCustomerPostcodeMatrix();
                                var postCodeMatrixgridData = $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getRowData');
                                var rowCount = $jqGrid("#" + CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getGridParam', 'records');
                                var postCodeMatrixList =[];
                                var postCode;
                                if (isValid) {
                                    for (var i = 0; i < postCodeMatrixgridData.length; i++) {
                                        postCode = CustomerParameterObj.GetValidGridValue((i +1), 'Postcode');
                                        var postCodeObject = {
                                                customerId: customerId,
                                                Round: CustomerParameterObj.GetValidGridValue((i+1), 'Round'),
                    RoundId: CustomerParameterObj.GetValidGridValue((i +1), 'RoundID'),
                        Postcode: postCode != "" ? postCode: "-1",
                        PostcodeMatrixID: $jqGrid('#' +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getCell', i + 1, 'PostcodeMatrixID'),

                        Monday: $jqGrid("#" +(i + 1) +CustomerParameterObj.PostcodeMonday).is(":checked") ? true: false,
                    Tuesday: $jqGrid("#" +(i +1) +CustomerParameterObj.PostcodeTuesday).is(":checked") ? true: false,
                    Wednesday: $jqGrid("#" +(i +1) + CustomerParameterObj.PostcodeWednesday).is(":checked") ? true: false,
                    Thursday: $jqGrid("#" +(i +1) + CustomerParameterObj.PostcodeThursday).is(":checked") ? true: false,
                    Friday: $jqGrid("#" +(i +1) +CustomerParameterObj.PostcodeFriday).is(":checked") ? true: false,
                    IsActive: $jqGrid("#" +(i +1) +CustomerParameterObj.PostcodeRemove).is(":checked") ? false: true,

                }
                                            postCodeMatrixList.push(postCodeObject);
                                            }

                                        $jqGrid.ajax({
                                                url: '/' +$jqGrid("#" +CustomerParameterObj.CustomerController).val() + '/' +$jqGrid("#" +CustomerParameterObj.ActionSaveCustomerPostcode).val(),
                                                        type: 'POST',
                                                        contentType: "application/json",
                                                        dataType: 'json',
                                                        async: false,
                                                    data: JSON.stringify(postCodeMatrixList),
                                                    success: function (data) {
                                                    if (data == true) {
                                                        $jqGrid("#" +CustomerParameterObj.MessageSelectCustomer).show();
                                                        var customerId = $jqGrid("#" +CustomerParameterObj.CustomerDropDown).val();
                                                        $jqGrid.ajax({
                                                                url : '/' + $jqGrid("#" + CustomerParameterObj.CustomerController).val() + '/' +$jqGrid("#" +CustomerParameterObj.ActionLoadCustomerPostcode).val(),
                                                                        type: 'GET',
                                                                        cache : false,
                                                                        data: {
                                                                    page: 1, rows: 50, customerId: customerId
                        },
                                                                    contentType: 'application/json',
                                                                        dataType: "json",
                                                                        async: false,
                                                                        success: function (responseData) {
                                                                    $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('clearGridData');
                                                                    $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('setGridParam', {
                                                                        datatype: 'local', data: responseData
                                                                        }).trigger('reloadGrid');
                        }
                    });
                }
                        else {
                                                            $jqGrid("#" +CustomerParameterObj.msgPostcodeNotSaved).show();
                                                        }
                                                        }

                                                        });
                                                        }
                                                        });

                                    CustomerParameter.prototype.GetValidGridValue = function (rowId, columnName) {
                                        var isValid = $jqGrid("#" +rowId + "_" +columnName).val();
                                        if (isValid == undefined) {
                                            isValid = $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getCell', rowId, columnName);
                                            }
                                        return isValid;
                                            }

                                            CustomerParameter.prototype.ValidateCustomerPostcodeMatrix = function() {
                                                $jqGrid('#' +CustomerParameterObj.PostcodeErrorMsg + '').hide();
                                                var postCodeMatrixgridData = $jqGrid("#" +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getRowData');
                                                var rowIds = $jqGrid("#" + CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getDataIDs');

                                                var errorMsg = "<ul class='pop_error_ul'>";
                                                var newline = "</br>";
                                                var errorMsgUlclose = "</ul>";
                                                var errorMsgLi = "<li>";
                                                var errorMsgLiclose = "</li>";
                                                var inValidCount = 0;

                                                var roundValidationMsg = $jqGrid("#" +CustomerParameterObj.RoundError).val();
                                                var roundIdValidationMsg = $jqGrid("#" +CustomerParameterObj.RoundIdError).val();
                                                var postcodeValidationMsg = $jqGrid("#" +CustomerParameterObj.PostcodeError).val();
                                                var forRowNumber = $jqGrid("#" +CustomerParameterObj.ForRowNumber).val();
                                                var DeliveryDayValidationMsg = $jqGrid("#" +CustomerParameterObj.DeliveryDayError).val();

                                                var isRoundValid = true;
                                                var isRoundIdValid = true;
                                                var isPostCodeValid = true;
                                                var isDeliveryDayValid = true;

                                                var invalidRoundRows = "";
    var invalidRoundIdRows = "";
    var invalidPostcodeRows = "";
    var invalidDayRows = "";

                                                for (var i = 1; i <= rowIds.length; i++) {
                                                    var rowId = rowIds[i - 1];
                                                    var row = postCodeMatrixgridData[rowId -1];
        isRemove = $jqGrid("#" +(rowId) +CustomerParameterObj.PostcodeRemove).is(":checked") ? true : false;
        if (!isRemove) {
            if ($jqGrid('#' +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getCell', rowId, 'Round') == "")
            {
                inValidCount++;
                invalidRoundRows += " " +(i);
                isRoundValid = false;
                }
            if ($jqGrid('#' +CustomerParameterObj.PostcodeMatrixGrid).jqGrid('getCell', rowId, 'RoundID') == "") {
                inValidCount++;
                invalidRoundIdRows += " " +(i);
                isRoundIdValid = false;
                }
            Monday = $jqGrid("#" +(rowId) +CustomerParameterObj.PostcodeMonday).is(":checked") ? true : false;
            Tuesday = $jqGrid("#" +(rowId) +CustomerParameterObj.PostcodeTuesday).is(":checked") ? true: false;
            Wednesday = $jqGrid("#" + (rowId) +CustomerParameterObj.PostcodeWednesday).is(":checked") ? true: false;
            Thursday = $jqGrid("#" + (rowId) +CustomerParameterObj.PostcodeThursday).is(":checked") ? true: false;
            Friday = $jqGrid("#" +(rowId) +CustomerParameterObj.PostcodeFriday).is(":checked") ? true: false;


        if (Monday == false && Tuesday == false && Wednesday == false && Thursday == false && Friday == false) {
            inValidCount++;
            invalidDayRows += " " +(i);
            isDeliveryDayValid = false;
            }
            }
            }

    if (inValidCount > 0) {
        if (!isRoundValid) {
            errorMsg += errorMsgLi +roundValidationMsg + " " +forRowNumber + " " +invalidRoundRows +errorMsgLiclose;
            }
        if (!isRoundIdValid) {
            errorMsg += errorMsgLi + roundIdValidationMsg + " " + forRowNumber + " " +invalidRoundIdRows +errorMsgLiclose;

            }
        if (!isDeliveryDayValid) {
            errorMsg += errorMsgLi + DeliveryDayValidationMsg + " " +forRowNumber + " " +invalidDayRows +errorMsgLiclose;
            }
        $jqGrid('#' + CustomerParameterObj.PostcodeErrorMsg).html(errorMsg);
        $jqGrid('#' + CustomerParameterObj.PostcodeErrorMsg).show();
        }
    return isValid = inValidCount > 0 ? false: true;
            }

                // Save button Event for Clinical Contact & Analysi Information
$jqGrid('#' +CustomerParameterObj.SaveClinicalContactButton).click(function () {
    $jqGrid("#" + CustomerParameterObj.ClinicalContactRequireSpan).text('');
    var contactName = $jqGrid("#" +CustomerParameterObj.NewClinicalContactTextBox).val();
    if (contactName.trim().length > 0) {

        // Get the Selected Row
        var rowId =  $jqGrid("#" +CustomerParameterObj.jqCPMedicalStaff).jqGrid('getGridParam', 'selrow');

        if (rowId != null && rowId > 0) {

            var selectedRowId = $jqGrid("#" +CustomerParameterObj.jqCPMedicalStaff).jqGrid('getGridParam', 'selrow');
            $jqGrid("#" +CustomerParameterObj.jqCPMedicalStaff).jqGrid('setCell', selectedRowId, 'DisplayText', contactName);
        }
        else {

            // Verify for Duplicate Entry
            var existLength = $(".clinicalDisplayText").filter(function (index) { return $(this).text().toUpperCase() == contactName.trim().toUpperCase(); }).length;
            if (existLength > 0) {

               $jqGrid("#" +CustomerParameterObj.ClinicalContactRequireSpan).text($jqGrid("#" +CustomerParameterObj.ClinicalContactAlreadyExist).val());
                return false;
            }

            // Add New Clinical Contact item into Grid
            var datarow = {
                ListId: 0,
                ListTypeId: $jqGrid("#" + CustomerParameterObj.ClinicalContactListType).val(),
                DisplayText: contactName,
                IsActive: false,
                IsMandatory: false,
            };
            var gridrowdata =  $jqGrid("#" +CustomerParameterObj.jqCPMedicalStaff).jqGrid('getRowData');
            $jqGrid("#" + CustomerParameterObj.jqCPMedicalStaff).jqGrid('addRowData', gridrowdata.length + 1, datarow, "first");
            
        }
       $jqGrid("#" +CustomerParameterObj.jqCPMedicalStaff).jqGrid('resetSelection');
       $jqGrid("#" +CustomerParameterObj.NewClinicalContactTextBox).val('');
    }
    else
    {
       $jqGrid("#" +CustomerParameterObj.ClinicalContactRequireSpan).text($jqGrid("#" + CustomerParameterObj.ClinicalContactRequire).val());
    }
    return false;
});

// Save Analysis Info Event
$jqGrid('#' + CustomerParameterObj.SaveAnalysisInfoButton).click(function () {
    $jqGrid("#" + CustomerParameterObj.AnalysisInfoRequireSpan).text('');
    var contactName = $jqGrid("#" + CustomerParameterObj.NewAnalysisInfoTextBox).val();
    if (contactName.trim().length > 0) {

        // Get the Selected Row
        var rowId = $jqGrid("#" + CustomerParameterObj.jqCPAnalysisInfo).jqGrid('getGridParam', 'selrow');

        if (rowId != null && rowId > 0) {

        var selectedRowId = $jqGrid("#" +CustomerParameterObj.jqCPAnalysisInfo).jqGrid('getGridParam', 'selrow');
        $jqGrid("#" + CustomerParameterObj.jqCPAnalysisInfo).jqGrid('setCell', selectedRowId, 'DisplayText', contactName);

        }
        else {

            // Verify for Duplicate Entry
            var existLength = $(".analysisInfoDisplayText").filter(function (index) { return $(this).text().toUpperCase() == contactName.trim().toUpperCase(); }).length;
            if (existLength > 0) {

                $jqGrid("#" + CustomerParameterObj.AnalysisInfoRequireSpan).text($jqGrid("#" + CustomerParameterObj.AnalysisInfoAlreadyExist).val());
                return false;
            }

            // Add New Analysis Info item into Grid
            var datarow = {
                ListId: 0,
                ListTypeId: $jqGrid("#" + CustomerParameterObj.AnalysisInfoListType).val(),
                DisplayText: $jqGrid("#" + CustomerParameterObj.NewAnalysisInfoTextBox).val(),
                IsActive: false,
                IsMandatory: false,
            };
            var gridrowdata = $jqGrid("#" +CustomerParameterObj.jqCPAnalysisInfo).jqGrid('getRowData');
           $jqGrid("#" +CustomerParameterObj.jqCPAnalysisInfo).jqGrid('addRowData', gridrowdata.length + 1, datarow, "first");
        }
        $jqGrid("#" + CustomerParameterObj.jqCPAnalysisInfo).jqGrid('resetSelection');
        $jqGrid("#" + CustomerParameterObj.NewAnalysisInfoTextBox).val('');
    }
    else {
       $jqGrid("#" + CustomerParameterObj.AnalysisInfoRequireSpan).text($jqGrid("#" + CustomerParameterObj.AnalysisInfoRequire).val());
    }
    return false;
});

// Open the Clinical Contact Heirachy Page
$jqGrid("#" + CustomerParameterObj.ClinicalContactLinkage).click(function ()
{
    CustomerParameterObj.ShowHideClinicalContactLinkage(true);
});

$jqGrid("#" + CustomerParameterObj.ClinicalContactLinkageButton).click(function () {
    ClinicalContactMappingObj.ShowClinicalContactMapping();
    return false;
})

$jqGrid("#" +CustomerParameterObj.ClinicalContactWithoutLinkage).click(function () {    
    CustomerParameterObj.ShowHideClinicalContactLinkage(false);
});

CustomerParameter.prototype.ShowHideClinicalContactLinkage = function (isClinicalLinkage) {
    if (isClinicalLinkage) {
        // Set the Clinical contact linkage radio button checked
        $("#" + CustomerParameterObj.ClinicalContactLinkage).prop("checked", true);
        $jqGrid("#" + CustomerParameterObj.ClinicalContactLinkage).closest("div").toggleClass("ez-selected", true);

        $("#" + CustomerParameterObj.ClinicalContactWithoutLinkage).prop("checked", false);
        $jqGrid("#" + CustomerParameterObj.ClinicalContactWithoutLinkage).closest("div").toggleClass("ez-selected", false);

        $jqGrid(".cust_param_medical_staff").hide();
        $jqGrid(".cust_param_anlysis_info").addClass("float_left_0");
        $jqGrid(".open_linkage").show();
    }
    else {
        // Set the Clinical contact linkage radio button checked
        $("#" + CustomerParameterObj.ClinicalContactLinkage).prop("checked", false);
        $jqGrid("#" + CustomerParameterObj.ClinicalContactLinkage).closest("div").toggleClass("ez-selected", false);

        $("#" + CustomerParameterObj.ClinicalContactWithoutLinkage).prop("checked", true);
        $jqGrid("#" + CustomerParameterObj.ClinicalContactWithoutLinkage).closest("div").toggleClass("ez-selected", true);

        $jqGrid(".cust_param_medical_staff").show();
        $jqGrid(".cust_param_anlysis_info").removeClass("float_left_0");
        $jqGrid(".open_linkage").hide();
    }
    
    CustomerParameterObj.ClinicalContactLinkageChecked = isClinicalLinkage;
}

CustomerParameter.prototype.ClearGridSelection = function (rowid, typeId) {

    var gridNameToUpdate = "";

    if (typeId == 1) {
        gridNameToUpdate = CustomerParameterObj.jqCPMedicalStaff;
        $jqGrid("#" + CustomerParameterObj.NewClinicalContactTextBox).val('');
        $jqGrid("#" + CustomerParameterObj.ClinicalContactRequireSpan).text('');
    }
    else {
        gridNameToUpdate = CustomerParameterObj.jqCPAnalysisInfo;
        $jqGrid("#" + CustomerParameterObj.AnalysisInfoRequireSpan).text('');
        $jqGrid("#" + CustomerParameterObj.NewAnalysisInfoTextBox).val('');
    }

    $jqGrid("#" + gridNameToUpdate).jqGrid('resetSelection');

    var CurrentSelectIndex = $jqGrid("#" + gridNameToUpdate).jqGrid('getInd', rowid);
    var InitialSelectIndex = $jqGrid("#" + gridNameToUpdate).jqGrid('getInd', initialRowSelect);
    var startID = "";
    var endID = "";
    if (CurrentSelectIndex > InitialSelectIndex) {
        startID = initialRowSelect;
        endID = rowid;
    }
    else {
        startID = rowid;
        endID = initialRowSelect;
    }

    var shouldSelectRow = false;
    $.each($jqGrid("#" + gridNameToUpdate).getDataIDs(), function (_, id) {
        if ((shouldSelectRow = id == startID || shouldSelectRow)) {
            $jqGrid("#" + gridNameToUpdate).jqGrid('setSelection', id, false);
        }
        return id != endID;
    });
}

CustomerParameter.prototype.RemoveClinicalContactAnalysis = function(id, selectedListId, listTypeId)
{
    var contactName = "";
    if (listTypeId ==1 ) {
        contactName = CustomerParameterObj.ClinicalContactType;
    }
    else
    {
        contactName = CustomerParameterObj.AnalysisInformationType;
    }

    if (! $(id).children().is(":checked")) {

        var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
        // Check the Clinical Contact mapping with the Patient
        $jqGrid.ajax({
            url: '/' + $jqGrid("#" + CustomerParameterObj.CustomerController).val() + '/' + $jqGrid("#" + CustomerParameterObj.ActionRemoveClinicalContactValue).val(),
            type: 'GET',
            cache: false,
            data: {
                CustomerId: customerId, CustomerMedicalStaffAnalysisInfoId: selectedListId
            },
            contentType: 'application/json',
            dataType: "json",
            async: false,
            success: function (responseData) {
                if (responseData) {
                    
                    // Undo the Check checked and Display the Message
                    var messageToDisplay = $jqGrid("#" + CustomerParameterObj.RemoveClinicalContactAnalysisError).val().replace('{0}', contactName);
                    $(id).children().prop("checked", true);
                    $(".disable_pop").show();
                    $(".ui-dialog-titlebar-close").addClass('btn_disable');
                    CommonScriptObj.DisplayErrorMessage(PatientTypeMatrixObj.ErrorMessage, messageToDisplay, 'pop_error_ul');
                }
            }
        });
    }
}

CustomerParameter.prototype.OpenPrintCustomerparameterPopup = function () {
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
    if (customerId != '' && customerId != '0' && customerId != null) {
    var printPopup = $jqGrid("#" + CustomerParameterObj.divPrintCustomerparameterPopup).dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        width: 'auto',
        resizable: true,
        draggable: false,
            title: $jqGrid("#" + CustomerParameterObj.PrintCustomerParameterTitle).val()
    });
    printPopup.dialog('open');
    CustomerParameterObj.LoadPrintCustomerparameter();
}
}

CustomerParameter.prototype.LoadPrintCustomerparameter = function () {
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
    $jqGrid.ajax({
        url: $jqGrid("#" + CustomerParameterObj.ExportCustomerParameterAction).val(),
        type: 'GET',
        cache: false,
        data: {
            customerId: customerId,
            isExportToExcel: false
        },
        contentType: 'application/json',
        dataType: "json",
        async: false,
        success: function (responseData) {
            $jqGrid("#jqPrintGrid").jqGrid('clearGridData');
            $jqGrid("#jqPrintGrid").jqGrid('setGridParam', { datatype: 'local', data: responseData }).trigger('reloadGrid');
        }
    });
}

CustomerParameter.prototype.PrintCustomerParameter = function () {
    $jqGrid('#jqPrintGrid').print({
    });
}

CustomerParameter.prototype.ExportCustomerparameter = function () {
    var customerId = $jqGrid("#" + CustomerParameterObj.CustomerDropDown).val();
    if (customerId != '' && customerId != '0' && customerId != null) {
    $jqGrid.ajax({
        url: $jqGrid("#" + CustomerParameterObj.ExportCustomerParameterAction).val(),
        type: 'GET',
        async: false,
        cache: false,
        contentType: 'application/json',
        data: {
            customerId: customerId,
            isExportToExcel: true
        },
        success: function (responseData) {
            window.open(responseData);
        }
    });
}
}