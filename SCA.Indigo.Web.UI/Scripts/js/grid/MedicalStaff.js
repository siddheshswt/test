﻿$jqGrid = jQuery.noConflict();
$jqGrid(document).ready(function () {
    $jqGrid("#jqMedicalStaff").jqGrid({
        // Ajax related configurations
        url: "",
        datatype: "",
        mtype: "GET",
        // Specify the column names
        colNames: ['ID', 'Name'],
        colModel: [
            { name: 'ID', index: 'ID', editable: false, hidden: false, },
            { name: 'Name', index: 'Name', editable: true }            

        ],
        // Grid total width and height
        width: 700,
        // Paging
        toppager: false,
        jsonReader: { repeatitems: false },
        pager: $jqGrid("#jqMedicalStaffTablePager"),
        rowNum: 20,
        rowList: [2, 5, 10, 20, 50],
        viewrecords: true, // Specify if "total number of records" is displayed
        height: 'auto',
        autowidth: false,
        // Default sorting
        sortname: "",
        sortorder: "asc",
        editurl: "/Default/EditUpdatePatientDetails",
        // Grid caption
        //caption: "Customer Address",
        gridComplete: function () {
           
            //            $jqGrid('table').addClass("search_pannel_result_table");


        },
        onCellSelect: function (rowid, iCol) {
        },

        ondblClickRow: function (rowid) {

            //alert(rowid)

        },

        loadComplete: function (data) {
           
            var pagesize = jQuery("#jqMedicalStaff").jqGrid('getGridParam', 'rowNum');
            if (data.rows.length < pagesize) {
                for (i = 0; i < pagesize - data.rows.length; i++) {
                    $jqGrid("#jqMedicalStaff").addRowData(i + 1, {});
                }
            }
        },

    }).navGrid("#jqMedicalStaffTablePager",
                   { search: false, refresh: false, add: false, edit: false, del: false },
                   {}, // settings for edit
                   {},
                   {}
               );

    $jqGrid("#jqMedicalStaff").jqGrid('inlineNav', "#jqMedicalStaffTablePager",
    {
        edit: false,
        editicon: "ui-icon-pencil",
        add: false,
        addicon: "ui-icon-plus",
        save: true,
        saveicon: "ui-icon-disk",
        cancle: true,
        cancelicon: "ui-icon-cancel",
        addParams: {
            //                position: "last",
            addRowParams: {
                // the parameters of editRow used to edit new row
                keys: true,
                oneditfunc: function (rowid) {

                }
            }
        },
        editParams: {
            keys: true,
            oneditfunc: function (data, value) {

            },
            sucessfunc: function (data) {
            },
            url: null,
            extraparam: {

            },
            aftersavefunc: function () {
                $jqGrid("#InvoiceAddressSucess").dialog({
                    resizable: false,
                    autoOpen: true,
                    height: 140,
                    width: 300,
                    modal: true,
                    closeOnEscape: false,
                    dialogClass: "noclose",
                    buttons: {
                        Ok: function () {
                            $jqGrid(this).dialog("close");
                            jQuery("#jqAddInvoiceAddressTable").trigger("reloadGrid");
                        }
                    }
                });

            },
            errorfunc: null,
            afterrestorefunc: null,
            restoreAfterError: true,
            mtype: "POST"
        }
    }
    );

        $jqGrid('#lnkContinenceAdvisor').click(function() {

            $jqGrid('#divMedicalStaffPopup').dialog({
                autoOpen: false,
                modal: true,
                width: 'auto',
                closeOnEscape: false,
                // position: ['center', 'top'],            
                //position: 'top',
                //position: { my: "center", at: "top+30%", of: window },
                position: [300, 200],
                resizable: true,
                title: "Medical Staff"
            });
            $jqGrid('#divMedicalStaffPopup').dialog('open');
            $jqGrid("#jqMedicalStaff").trigger("reloadGrid");
        });

    }
);