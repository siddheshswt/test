﻿$jqGrid = jQuery.noConflict();
$jqGrid(document).ready(function () {

    if ($jqGrid('#txtsearchproduct').val() != "" && $jqGrid('#txtsearchproduct').val() != undefined) {
        $jqGrid('#txtsearchproduct').val('');
    }
    $jqGrid('#btnAddProduct').click(function () {
        $jqGrid('#divProductList').dialog({
            autoOpen: false,
            modal: true,
            closeOnEscape: false,
            width: 'auto',                       
            resizable: true,
            title: "Product List",
           
        });
        $jqGrid('#divProductList').dialog('open');
    })

}
);