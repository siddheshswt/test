﻿// ***********************************************************************
// <copyright file="ClinicalContactMappingViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the ClinicalContactMappingViewModel file.</summary>
// ***********************************************************************

/// <summary>
/// The ViewModel namespace.
/// </summary>
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Class ClinicalContactMappingViewModel.
    /// </summary>
    public class ClinicalContactMappingViewModel
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the medical staff master list.
        /// </summary>
        /// <value>
        /// The medical staff master list.
        /// </value>
        public SelectList MedicalStaffMasterList { get; set; }

        /// <summary>
        /// Gets or sets the hierarchy master list.
        /// </summary>
        /// <value>
        /// The hierarchy master list.
        /// </value>
        public SelectList HierarchyMasterList { get; set; }

        /// <summary>
        /// Gets or sets the clinical child list.
        /// </summary>
        /// <value>
        /// The clinical child list.
        /// </value>
        public SelectList ClinicalChildList { get; set; }

        /// <summary>
        /// Gets or sets the clinical parent list.
        /// </summary>
        /// <value>
        /// The clinical parent list.
        /// </value>
        public SelectList ClinicalParentList { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact value mapping list.
        /// </summary>
        /// <value>
        /// The clinical contact value mapping list.
        /// </value>
        public List<ClinicalContactValueMappingViewModel> ClinicalContactValueMappingList { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact role mapping list.
        /// </summary>
        /// <value>
        /// The clinical contact role mapping list.
        /// </value>
        public List<ClinicalContactRoleMappingViewModel> ClinicalContactRoleMappingList { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the role mapping.
        /// </summary>
        /// <value>
        /// The role mapping.
        /// </value>
        public string RoleMapping { get; set; }

        /// <summary>
        /// Gets or sets the parent link items.
        /// </summary>
        /// <value>
        /// The parent link items.
        /// </value>
        public SelectList ParentLinkItems { get; set; }

        /// <summary>
        /// Gets or sets the hierarchy list identifier.
        /// </summary>
        /// <value>
        /// The hierarchy list identifier.
        /// </value>
        public long? HierarchyListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the hierarchy.
        /// </summary>
        /// <value>
        /// The name of the hierarchy.
        /// </value>
        public string HierarchyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parent list identifier.
        /// </summary>
        /// <value>
        /// The parent list identifier.
        /// </value>
        public long? ParentListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the parent list.
        /// </summary>
        /// <value>
        /// The name of the parent list.
        /// </value>
        public string ParentListName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the clinical contact role mapping identifier.
        /// </summary>
        /// <value>
        /// The clinical contact role mapping identifier.
        /// </value>
        public long? ClinicalContactRoleMappingId
        {
            get;
            set;
        }
    }
}