﻿using System;
using System.Collections.Generic;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// SignalR User Model
    /// </summary>
    public class SignalRUserModel
    {
        /// <summary>
        /// Constructor for initilization
        /// </summary>
        /// <param name="userId"></param>
        public SignalRUserModel(string userId)
        {
            LstRequestId = new List<string>();
            LastNotificationTime = DateTime.Now;
            InteractionCount = 0;
            UserId = userId;
        }

        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Private list for request Id
        /// </summary>
        private List<string> LstRequestId;

        /// <summary>
        /// List of request Id
        /// </summary>
        public List<string> RequestId { get { return this.LstRequestId; }}

        /// <summary>
        /// Last notification time
        /// </summary>
        public DateTime LastNotificationTime { get; set; }

        /// <summary>
        /// Interaction count
        /// </summary>
        public Int64 InteractionCount { get; set; }
    }
}