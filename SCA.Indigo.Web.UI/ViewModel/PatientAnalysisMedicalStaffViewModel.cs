﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="PatientAnalysisMedicalStaffViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System.Collections.Generic;

    /// <summary>
    /// Patient Analysis Medical Staff View Model
    /// </summary>
    public class PatientAnalysisMedicalStaffViewModel
    {
        /// <summary>
        /// Gets or sets the index patient medical staff identifier.
        /// </summary>
        /// <value>
        /// The index patient medical staff identifier.
        /// </value>
        public long IdxPatientMedicalStaffId { get; set; }

        /// <summary>
        /// Gets or sets the customer medical staff identifier.
        /// </summary>
        /// <value>
        /// The customer medical staff identifier.
        /// </value>
        public long? CustomerMedicalStaffId { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public long? PatientId { get; set; }

        /// <summary>
        /// Gets or sets the customer medical staff analysis identifier.
        /// </summary>
        /// <value>
        /// The customer medical staff analysis identifier.
        /// </value>
        public long CustomerMedicalStaffAnalysisId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the list identifier.
        /// </summary>
        /// <value>
        /// The list identifier.
        /// </value>
        public long? ListId { get; set; }

        /// <summary>
        /// Gets or sets the list type identifier.
        /// </summary>
        /// <value>
        /// The list type identifier.
        /// </value>
        public long? ListTypeId { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public System.Guid ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public System.DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
    }
}