﻿namespace SCA.Indigo.Web.UI.ViewModel.UserAdmin
{
    using System.Web.Mvc;

    /// <summary>
    /// Carehome Users View Model
    /// </summary>
    public class CareHomeUserViewModel
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the Customers.
        /// </summary>
        /// <value>
        /// The Customers.
        /// </value>
        public SelectList Customers { get; set; }

    }
}