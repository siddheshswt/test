﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// Home View Model
    /// </summary>
    public class HomeViewModel
    {
        /// <summary>
        /// Gets or sets the is called from home page.
        /// </summary>
        /// <value>
        /// The is called from home page.
        /// </value>
        public string IsCalledFromHomePage { get; set; }

        /// <summary>
        /// Gets or sets the interaction identifier.
        /// </summary>
        /// <value>
        /// The interaction identifier.
        /// </value>
        public string InteractionId { get; set; }

		/// <summary>
		/// Gets or sets the IsInternalUser
		/// </summary>
		/// <value>
		/// IsInternalUser
		/// </value>
		public string IsInternalUser { get; set; }
    }
}