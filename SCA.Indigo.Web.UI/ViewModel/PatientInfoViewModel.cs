﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sagar Yerva
// Created          : 12-02-2014
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="PatientInfoViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;

    /// <summary>
    /// Patient Info View Model
    /// </summary>
    [Serializable]
    public class PatientInfoViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public long PatientId { get; set; }

        /// <summary>
        /// Gets or sets the sap patient number.
        /// </summary>
        /// <value>
        /// The sap patient number.
        /// </value>
        public string SAPPatientNumber { get; set; }

        /// <summary>
        /// Gets or sets the person identifier.
        /// </summary>
        /// <value>
        /// The person identifier.
        /// </value>
        public long? PersonId { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgDateofBirth")]
        [DataType(DataType.Date)]
        public string DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the type of the patient.
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgPatientType")]
        public long? PatientType { get; set; }

        /// <summary>
        /// Gets or sets the last modified by.
        /// </summary>
        /// <value>
        /// The last modified by.
        /// </value>
        public long? LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the contact person identifier.
        /// </summary>
        /// <value>
        /// The contact person identifier.
        /// </value>
        public long? ContactPersonId { get; set; }

        /// <summary>
        /// Gets or sets the name of the contact person.
        /// </summary>
        /// <value>
        /// The name of the contact person.
        /// </value>
        public string ContactPersonName { get; set; }

        /// <summary>
        /// Gets or sets the delivery instruction text.
        /// </summary>
        /// <value>
        /// The delivery instruction text.
        /// </value>
        public string DeliveryInstructionText { get; set; }

        /// <summary>
        /// Gets or sets the prescription identifier.
        /// </summary>
        /// <value>
        /// The prescription identifier.
        /// </value>
        public long PrescriptionId { get; set; }

        /// <summary>
        /// Gets or sets the delivery frequency.
        /// </summary>
        /// <value>
        /// The delivery frequency.
        /// </value>
        public string DeliveryFrequency { get; set; }

        /// <summary>
        /// Gets or sets the round identifier.
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        public string RoundId { get; set; }

        /// <summary>
        /// Gets or sets the round.
        /// </summary>
        /// <value>
        /// The round.
        /// </value>
        public string Round { get; set; }

        /// <summary>
        /// Gets or sets the alternate delivery point.
        /// </summary>
        /// <value>
        /// The alternate delivery point.
        /// </value>
        public string AlternateDeliveryPoint { get; set; }

        /// <summary>
        /// Gets or sets the NHS identifier.
        /// </summary>
        /// <value>
        /// The NHS identifier.
        /// </value>
        public string NHSId { get; set; }

        /// <summary>
        /// Gets or sets the local identifier.
        /// </summary>
        /// <value>
        /// The local identifier.
        /// </value>
        public string LocalId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the carehome assignment date.
        /// </summary>
        /// <value>
        /// The carehome assignment date.
        /// </value>
        public string CarehomeAssignmentDate { get; set; }

        /// <summary>
        /// Gets or sets the assessment date.
        /// </summary>
        /// <value>
        /// The assessment date.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgAssessmentDate")]
        [DataType(DataType.Date)]
        public string AssessmentDate { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataType(DataType.Date)]
        public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the next assessment date.
        /// </summary>
        /// <value>
        /// The next assessment date.
        /// </value>
        public string NextAssessmentDate { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        public string DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the next delivery date.
        /// </summary>
        /// <value>
        /// The next delivery date.
        /// </value>
        public string NextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the communication format.
        /// </summary>
        /// <value>
        /// The communication format.
        /// </value>
        public long? CommunicationFormat { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        public long? CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the name of the correspondence care home.
        /// </summary>
        /// <value>
        /// The name of the correspondence care home.
        /// </value>
        public string CorrespondenceCareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the type of the DDL patient.
        /// </summary>
        /// <value>
        /// The type of the DDL patient.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgPatientType")]
        public SelectList ddlPatientType { get; set; }

        /// <summary>
        /// Gets or sets the patient status.
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        public string PatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the DDL patient status.
        /// </summary>
        /// <value>
        /// The DDL patient status.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgPatientStatus")]
        public SelectList ddlPatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the DDL reason code.
        /// </summary>
        /// <value>
        /// The DDL reason code.
        /// </value>
        public SelectList ddlReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the DDL title.
        /// </summary>
        /// <value>
        /// The DDL title.
        /// </value>
        public SelectList ddlTitle { get; set; }

        /// <summary>
        /// Gets or sets the DDL communication format.
        /// </summary>
        /// <value>
        /// The DDL communication format.
        /// </value>
        public SelectList ddlCommunicationFormat { get; set; }

        /// <summary>
        /// Gets or sets the DDL gender.
        /// </summary>
        /// <value>
        /// The DDL gender.
        /// </value>
        public SelectList ddlGender { get; set; }

        /// <summary>
        /// Gets or sets the DDL customer.
        /// </summary>
        /// <value>
        /// The DDL customer.
        /// </value>
        public SelectList ddlCustomer { get; set; }

        /// <summary>
        /// Gets or sets the house number.
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        public string HouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the house.
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        public string HouseName { get; set; }

        // Correspondence appended to address fields to represent Correspondence Address 

        /// <summary>
        /// Gets or sets a value indicating whether [correspondence same as delivery].
        /// </summary>
        /// <value>
        /// <c>true</c> if [correspondence same as delivery]; otherwise, <c>false</c>.
        /// </value>
        public bool CorrespondenceSameAsDelivery { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the correspondence address1.
        /// </summary>
        /// <value>
        /// The correspondence address1.
        /// </value>
        public string CorrespondenceAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the correspondence address2.
        /// </summary>
        /// <value>
        /// The correspondence address2.
        /// </value>
        public string CorrespondenceAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the town or city.
        /// </summary>
        /// <value>
        /// The town or city.
        /// </value>
        public string TownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the correspondence town or city.
        /// </summary>
        /// <value>
        /// The correspondence town or city.
        /// </value>
        public string CorrespondenceTownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets the correspondence post code.
        /// </summary>
        /// <value>
        /// The correspondence post code.
        /// </value>
        public string CorrespondencePostCode { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the correspondence country.
        /// </summary>
        /// <value>
        /// The correspondence country.
        /// </value>
        public string CorrespondenceCountry { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the correspondence county.
        /// </summary>
        /// <value>
        /// The correspondence county.
        /// </value>
        public string CorrespondenceCounty { get; set; }

        /// <summary>
        /// Gets or sets the ad p1.
        /// </summary>
        /// <value>
        /// The ad p1.
        /// </value>
        public string ADP1 { get; set; }

        /// <summary>
        /// Gets or sets the ad p2.
        /// </summary>
        /// <value>
        /// The ad p2.
        /// </value>
        public string ADP2 { get; set; }

        /// <summary>
        /// Gets or sets the drop down selectedvalue.
        /// </summary>
        /// <value>
        /// The drop down selectedvalue.
        /// </value>
        public SelectedValue DropDownSelectedvalue { get; set; }

        /// <summary>
        /// Gets or sets the VBM patient.
        /// </summary>
        /// <value>
        /// The VBM patient.
        /// </value>
        public ViewModelBase VbmPatient { get; set; }

        /// <summary>
        /// Gets or sets the patient status identifier.
        /// </summary>
        /// <value>
        /// The patient status identifier.
        /// </value>
        public long? PatientStatusId { get; set; }

        /// <summary>
        /// Gets or sets the patient type identifier.
        /// </summary>
        /// <value>
        /// The patient type identifier.
        /// </value>
        public long? PatientTypeId { get; set; }

        /// <summary>
        /// Gets or sets the reason code identifier.
        /// </summary>
        /// <value>
        /// The reason code identifier.
        /// </value>
        public long? ReasonCodeId { get; set; }             

        /// <summary>
        /// Gets or sets the title identifier.
        /// </summary>
        /// <value>
        /// The title identifier.
        /// </value>
        public long TitleId { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public long Gender { get; set; }

        /// <summary>
        /// Gets or sets the address identifier.
        /// </summary>
        /// <value>
        /// The address identifier.
        /// </value>
        public long AddressId { get; set; }

        /// <summary>
        /// Gets or sets the telephone number.
        /// </summary>
        /// <value>
        /// The telephone number.
        /// </value>
        public string TelephoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the mobile number.
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the old patient status.
        /// </summary>
        /// <value>
        /// The old patient status.
        /// </value>
        public string OldPatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the old type of the patient.
        /// </summary>
        /// <value>
        /// The old type of the patient.
        /// </value>
        public string OldPatientType { get; set; }

        /// <summary>
        /// Gets or sets the new patient status.
        /// </summary>
        /// <value>
        /// The new patient status.
        /// </value>
        public string newPatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the new type of the patient.
        /// </summary>
        /// <value>
        /// The new type of the patient.
        /// </value>
        public string newPatientType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is redirect from approval.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is redirect from approval; otherwise, <c>false</c>.
        /// </value>
        public bool isRedirectFromApproval { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the assigned customer.
        /// </summary>
        /// <value>
        /// The assigned customer.
        /// </value>
        public string AssignedCustomer { get; set; }

        /// <summary>
        /// Gets or sets the assigned care home.
        /// </summary>
        /// <value>
        /// The assigned care home.
        /// </value>
        public long? AssignedCareHome { get; set; }

        /// <summary>
        /// Gets or sets the bill to.
        /// </summary>
        /// <value>
        /// The bill to.
        /// </value>
        public int? BillTo { get; set; }

        /// <summary>
        /// Gets or sets the customer parameters.
        /// </summary>
        /// <value>
        /// The customer parameters.
        /// </value>
        public List<CustomerParameterBusinessModel> CustomerParameters { get; set; }

        /// <summary>
        /// Gets or sets the DDL county.
        /// </summary>
        /// <value>
        /// The DDL county.
        /// </value>
        public SelectList ddlCounty { get; set; }

        /// <summary>
        /// Gets or sets the sap customer number.
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        public string SAPCustomerNumber { get; set; }

        /// <summary>
        /// Gets or sets the care home next delivery date.
        /// </summary>
        /// <value>
        /// The care home next delivery date.
        /// </value>
        public string CareHomeNextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is clinical linkage.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is clinical linkage; otherwise, <c>false</c>.
        /// </value>
        public bool IsClinicalLinkage { get; set; }

        /// <summary>
        /// Gets or sets the care home sap identifier.
        /// </summary>
        /// <value>
        /// The care home sap identifier.
        /// </value>
        public string CareHomeSAPId { get; set; }

        /// <summary>
        /// Gets the type of the non care home patient.
        /// </summary>
        /// <value>
        /// The type of the non care home patient.
        /// </value>
        public string NonCareHomePatientType
        {
            get
            {
                return Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.CommunitySelfcareNursing, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.CommunitySelfcareResidential, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.PaediatricProcare, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.PaediatricSelfcare, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.Procare, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.SelfCare, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.Bulk, CultureInfo.InvariantCulture) + "," +
                        Convert.ToString((long)SCA.Indigo.Common.Enums.SCAEnums.PatientType.ChildSelfCare, CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Gets or sets the interaction view model.
        /// </summary>
        /// <value>
        /// The interaction view model.
        /// </value>
        public InteractionViewModel InteractionViewModel { get; set; }

        /// <summary>
        /// Gets or sets the viewmodel audit log.
        /// </summary>
        /// <value>
        /// The viewmodel audit log.
        /// </value>
        public AuditLogViewModel ViewmodelAuditLog { get; set; }

        /// <summary>
        /// Gets or sets the type of the parent form.
        /// </summary>
        /// <value>
        /// The type of the parent form.
        /// </value>
        public string ParentFormType { get; set; }

        /// <summary>
        /// Gets or sets the name of the interaction lable.
        /// </summary>
        /// <value>
        /// The name of the interaction lable.
        /// </value>
        public string InteractionLableName { get; set; }

        /// <summary>
        /// Gets or sets the care home status.
        /// </summary>
        /// <value>
        /// The care home status.
        /// </value>
        public long? CareHomeStatus { get; set; }

        /// <summary>
        /// Gets or sets the file count.
        /// </summary>
        /// <value>
        /// The file count.
        /// </value>
        public string FileCount { get; set; }

        /// <summary>
        /// Gets or sets the file count error message.
        /// </summary>
        /// <value>
        /// The file count error message.
        /// </value>
        public string FileCountErrorMessage { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is data protected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is data protected; otherwise, <c>false</c>.
        /// </value>
        public bool IsDataProtected { get; set; }

        /// <summary>
        /// Gets or sets the communication preferences.
        /// </summary>
        /// <value>
        /// The communication preferences.
        /// </value>
        public string[] CommunicationPreferences { get; set; }

        /// <summary>
        /// Gets or sets the marketing preferences.
        /// </summary>
        /// <value>
        /// The marketing preferences.
        /// </value>
        public string[] MarketingPreferences { get; set; }

        /// <summary>
        /// Gets or sets the is called from home page.
        /// </summary>
        /// <value>
        /// The is called from home page.
        /// </value>
        public string IsCalledFromHomePage {get;set;}

        /// <summary>
        /// Gets or sets the patient interaction identifier.
        /// </summary>
        /// <value>
        /// The patient interaction identifier.
        /// </value>
        public string PatientInteractionId {get;set;}

        /// <summary>
        /// Gets or sets the is called from search page.
        /// </summary>
        /// <value>
        /// The is called from search page.
        /// </value>
        public string IsCalledFromSearchPage { get; set; }

        /// <summary>
        /// Gets or sets the search customer.
        /// </summary>
        /// <value>
        /// The search customer.
        /// </value>
        public string SearchCustomer { get; set; }

        /// <summary>
        /// Gets or sets the search patient status.
        /// </summary>
        /// <value>
        /// The search patient status.
        /// </value>
        public string SearchPatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the type of the search patient.
        /// </summary>
        /// <value>
        /// The type of the search patient.
        /// </value>
        public string SearchPatientType { get; set; }

        /// <summary>
        /// Gets or sets the search order number.
        /// </summary>
        /// <value>
        /// The search order number.
        /// </value>
        public string SearchOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the search identifier.
        /// </summary>
        /// <value>
        /// The search identifier.
        /// </value>
        public string SearchId { get; set; }

        /// <summary>
        /// Gets or sets the type of the search.
        /// </summary>
        /// <value>
        /// The type of the search.
        /// </value>
        public string SearchType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is clinical contact changed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is clinical contact changed; otherwise, <c>false</c>.
        /// </value>
        public bool IsClinicalContactChanged { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is analysis information changed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is analysis information changed; otherwise, <c>false</c>.
        /// </value>
        public bool IsAnalysisInfoChanged { get; set; }

        /// <summary>
        /// Gets or sets the RemovedStoppedDateTime.
        /// </summary>
        /// <value>
        /// The Removed Stopped Date Time.
        /// </value>
        public string RemovedStoppedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is non carehome patient or carehome patient.
        /// </summary>
        /// <value>
        /// True/False
        /// </value>
        public bool IsCarehomePatient { get; set; }

        /// <summary>
        /// Gets or Sets Patient NDD of DB
        /// </summary>
        public string ExistingPatientNDD { get; set; }

        /// <summary>
        /// Gets or Sets RecalcuateNDDForProCare
        /// </summary>
        public bool RecalcuateNDDForProCare { get; set; }
    }

    /// <summary>
    /// Selected Value
    /// </summary>
    public partial class SelectedValue
    {
        /// <summary>
        /// Gets or sets the patient type selected value.
        /// </summary>
        /// <value>
        /// The patient type selected value.
        /// </value>
        public object PatientTypeSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the patient status selected value.
        /// </summary>
        /// <value>
        /// The patient status selected value.
        /// </value>
        public object PatientStatusSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the gender selected value.
        /// </summary>
        /// <value>
        /// The gender selected value.
        /// </value>
        public object GenderSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the title selected value.
        /// </summary>
        /// <value>
        /// The title selected value.
        /// </value>
        public object TitleSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the communication format selected value.
        /// </summary>
        /// <value>
        /// The communication format selected value.
        /// </value>
        public object CommunicationFormatSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the reason code selected value.
        /// </summary>
        /// <value>
        /// The reason code selected value.
        /// </value>
        public object ReasonCodeSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the customer selected value.
        /// </summary>
        /// <value>
        /// The customer selected value.
        /// </value>
        public object CustomerSelectedValue { get; set; }

        /// <summary>
        /// Gets or sets the county selected value.
        /// </summary>
        /// <value>
        /// The county selected value.
        /// </value>
        public object CountySelectedValue { get; set; }        
    }
}