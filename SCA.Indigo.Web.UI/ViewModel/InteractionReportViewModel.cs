﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Saurabh
// Created          : 21-12-2015
//
// Last Modified By : Saurabh
// Last Modified On : 23-12-2015
// ***********************************************************************
// <copyright file="InteractionReportViewModel.cs" company="Capgemini">
//     Copyright © Capgemini 2014
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class InteractionReportViewModel
    {
        /// <summary>
        /// Gets or sets the customers ids.
        /// </summary>
        /// <value>
        /// The customers list.
        /// </value>
        public string CustomersIds { get; set; }

        /// <summary>
        /// Gets or sets the customers list.
        /// </summary>
        /// <value>
        /// The customers list.
        /// </value>
        public SelectList CustomersList { get; set; }

        /// <summary>
        /// Gets or sets the Patient Ids.
        /// </summary>
        /// <value>
        /// The Patient list.
        /// </value>
        public string PatientIds { get; set; }

        /// <summary>
        /// Gets or sets the Carehome Ids.
        /// </summary>
        /// <value>
        /// The Care Home List.
        /// </value>
        public string CareHomeIds { get; set; }

        /// <summary>
        /// Gets or sets the interaction type identifier.
        /// </summary>
        /// <value>
        /// The interaction type identifier.
        /// </value>
        public string InteractionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the interaction type list.
        /// </summary>
        /// <value>
        /// The interaction type list.
        /// </value>
        public SelectList InteractionTypeList { get; set; }

        /// <summary>
        /// Gets or sets the interaction sub type identifier.
        /// </summary>
        /// <value>
        /// The interaction sub type identifier.
        /// </value>
        public string InteractionSubTypeId { get; set; }

        /// <summary>
        /// Gets or sets the status identifier.
        /// </summary>
        /// <value>
        /// The status identifier.
        /// </value>
        public string StatusId { get; set; }

        /// <summary>
        /// Gets or sets the status list.
        /// </summary>
        /// <value>
        /// The status Id.
        /// </value>
        public SelectList StatusList { get; set; }

        /// <summary>
        /// Gets or sets Created By.
        /// </summary>
        /// <value>
        /// The status list
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets Created By List.
        /// </summary>
        /// <value>
        /// Created By List.
        /// </value>
        public SelectList CreatedByList { get; set; }

        /// <summary>
        /// Gets or sets the Created date from.
        /// </summary>
        /// <value>
        /// Created By List.
        /// </value>
        public string CreatedDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Created date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        public string CreatedDateTo { get; set; }

        /// <summary>
        /// Gets or sets the Modified date from.
        /// </summary>
        /// <value>
        /// The order date from.
        /// </value>
        public string ModifiedDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Modified date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        public string ModifiedDateTo { get; set; }

        /// <summary>
        /// Gets or sets the userid.
        /// </summary>
        /// <value>
        /// The logged in user id.
        /// </value>
        public string UserId { get; set; }

    }
}