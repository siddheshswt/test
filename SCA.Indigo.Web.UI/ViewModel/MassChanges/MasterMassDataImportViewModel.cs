﻿

// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Avinash
// Created          : 09-Jun-2015
//
// ***********************************************************************
// <copyright file="MasterMassDataImportViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel.MassChanges
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class MasterMassDataImportViewModel
    {
        /// <summary>
        /// Gets or Sets ImportTemplate
        /// </summary>
        public SelectList ImportTemplate { get; set; }

        /// <summary>
        /// Gets or Sets SelectedTemplate
        /// </summary>
        public int SelectedTemplate { get; set; }

        /// <summary>
        /// Gets or Sets UploadedFile
        /// </summary>
        public HttpPostedFileBase UploadedFile { get; set; }

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or Sets Status
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Gets or Sets is import flag
        /// </summary>
        public bool IsImport { get; set; }

        /// <summary>
        /// Gets or sets the log file path.
        /// </summary>
        /// <value>
        /// The log file path.
        /// </value>
        public string LogFilePath { get; set; }

        /// <summary>
        /// Gets Or sets the Template Validation Mesage Rows
        /// </summary>
        public List<TemplateValidationViewModel> TemplateValidationRows { get; set; }
    }
}