﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sachin
// Created          : 09-Jun-2015
//
// ***********************************************************************
// <copyright file="MasterMassDataChangeViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel.MassChanges
{
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Master Mass Data Change View Model Class
    /// </summary>
    public class MasterMassDataChangeViewModel
    {
        /// <summary>
        /// Gets or Sets Customer
        /// </summary>
        /// <value>
        /// The customer.
        /// </value>
        public SelectList Customer { get; set; }

        /// <summary>
        /// Gets or Sets Patient Status
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        public SelectList PatientStatus { get; set; }

        /// <summary>
        /// Gets or Sets Patient Type
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        public SelectList PatientType { get; set; }

        /// <summary>
        /// Gets or Sets Order Type
        /// </summary>
        /// <value>
        /// The type of the order.
        /// </value>
        public SelectList OrderType { get; set; }

        /// <summary>
        /// Gets or Sets Download Type
        /// </summary>
        /// <value>
        /// The type of the download.
        /// </value>
        public SelectList DownloadType { get; set; }

        /// <summary>
        /// Gets or Sets CareHome Statuses
        /// </summary>
        /// <value>
        /// The care home status.
        /// </value>
        public SelectList CareHomeStatus { get; set; }

        /// <summary>
        /// Gets or Sets CareHome Order Types
        /// </summary>
        /// <value>
        /// The type of the care home order.
        /// </value>
        public SelectList CareHomeOrderType { get; set; }

        /// <summary>
        /// Gets or Sets Order Statuses
        /// </summary>
        /// <value>
        /// The order status.
        /// </value>
        public SelectList OrderStatus { get; set; }

        /// <summary>
        /// Gets or Sets Carehome id
        /// </summary>
        /// <value>
        /// The carehome identifier.
        /// </value>
        public string CarehomeId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the order type identifier.
        /// </summary>
        /// <value>
        /// The order type identifier.
        /// </value>
        public string OrderTypeId { get; set; }

        /// <summary>
        /// Gets or sets the returnpath.
        /// </summary>
        /// <value>
        /// The returnpath.
        /// </value>
        public string Returnpath { get; set; }

        /// <summary>
        /// Gets or Sets patient id
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public string PatientId { get; set; }

        /// <summary>
        /// Gets or sets the patient type identifier.
        /// </summary>
        /// <value>
        /// The patient type identifier.
        /// </value>
        public string PatientTypeId { get; set; }

        /// <summary>
        /// Gets or sets the patient status identifier.
        /// </summary>
        /// <value>
        /// The patient status identifier.
        /// </value>
        public string PatientStatusId { get; set; }

        /// <summary>
        /// Gets or sets the download type identifier.
        /// </summary>
        /// <value>
        /// The download type identifier.
        /// </value>
        public long DownloadTypeId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public long ProductId { get; set; }

        /// <summary>
        /// Gets or sets the contact patient carehome identifier.
        /// </summary>
        /// <value>
        /// The contact patient carehome identifier.
        /// </value>
        public string ContactPatientCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the contact person.
        /// </summary>
        /// <value>
        /// The type of the contact person.
        /// </value>
        public long ContactPersonType { get; set; }

        /// <summary>
        /// Gets or sets the log file path.
        /// </summary>
        /// <value>
        /// The log file path.
        /// </value>
        public string LogFilePath { get; set; }

        /// <summary>
        /// Gets or sets the Patient Details List.
        /// </summary>
        /// <value>
        /// The Patient Details list.
        /// </value>
        public List<ClinicalContactValueMappingViewModel> PatientDetailsList { get; set; }

        /// <summary>
        /// Gets or Sets Patient Type
        /// </summary>
        /// <value>
        /// The template list.
        /// </value>
        public TemplateList TemplateList { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the Clinical Contact Type Id.
        /// </summary>
        /// <value>
        /// The Type Id.
        /// </value>
        public int ClinicalContactTypeId { get; set; }

        /// <summary>
        /// Gets or Sets UploadedFile
        /// </summary>
        /// <value>
        /// The uploaded file.
        /// </value>
        public HttpPostedFileBase UploadedFile { get; set; }

        /// <summary>
        /// Gets or Sets SelectedTemplate
        /// </summary>
        /// <value>
        /// The selected template.
        /// </value>
        public int SelectedTemplate { get; set; }

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or Sets Status
        /// </summary>
        /// <value>
        ///   <c>true</c> if status; otherwise, <c>false</c>.
        /// </value>
        public bool Status { get; set; }

        #region OrderUpdate
        /// <summary>
        /// Gets or sets Patients For OrderUpdate
        /// </summary>
        public string PatientsForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets CareHomes For OrderUpdate
        /// </summary>
        public string CareHomesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets OrderIds For OrderUpdate
        /// </summary>
        public string OrderIdsForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets Patient Types For Order Update
        /// </summary>
        public string[] PatientTypesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets CareHome Types For OrderUpdate
        /// </summary>
        public string[] CareHomeOrderTypesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets Order Types For OrderUpdate
        /// </summary>
        public string[] OrderTypesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets Patient Status For OrderUpdate
        /// </summary>
        public string[] PatientStatusForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string[] CareHomeStatusForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets Order Status For OrderUpdate
        /// </summary>
        public string[] OrderStatusForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier for post code matrix.
        /// </summary>
        /// <value>
        /// The customer identifier for post code matrix.
        /// </value>
        public string[] CustomerIdForPostCodeMatrix { get; set; }

        /// <summary>
        /// Gets or sets SAP ProductId For OrderUpdate
        /// </summary>
        public string SAPProductIdForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets Order Data DownloadFor [i.e. for Carehome/Patient]
        /// </summary>
        public int OrderDataDownloadFor { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string[] CareHomeStatusForCareHome { get; set; }

        #endregion

        public List<TemplateValidationViewModel> TemplateValidationRows { get; set; }
    }

    /// <summary>
    /// Gets or Sets Template Name
    /// </summary>
    public class TemplateList
    {
        /// <summary>
        /// Gets or sets the name of the template.
        /// </summary>
        /// <value>
        /// The name of the template.
        /// </value>
        public string TemplateName { get; set; }
    }
}