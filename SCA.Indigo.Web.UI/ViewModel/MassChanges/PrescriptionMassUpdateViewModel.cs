﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SCA.Indigo.Web.UI.ViewModel.MassChanges
{
    /// <summary>
    /// PrescriptionMassUpdateViewModel
    /// </summary>
    public class PrescriptionMassUpdateViewModel
    {
        /// <summary>
        /// Customer
        /// </summary>
        public SelectList Customer { get; set; }
    }
}