﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCA.Indigo.Web.UI.ViewModel.MassChanges
{
    /// <summary>
    /// TemplateValidationViewModel
    /// </summary>
    public class TemplateValidationViewModel
    {
        /// <summary>
        /// TemplateColumnName
        /// </summary>
        public string TemplateColumnName { get; set; }

        /// <summary>
        /// UploadColumnName
        /// </summary>
        public string UploadColumnName { get; set; }

        /// <summary>
        /// message
        /// </summary>
        public string message { get; set; }
    }
}