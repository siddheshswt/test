﻿// ***********************************************************************
// <copyright file="RoleViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the RoleViewModel file.</summary>
// ***********************************************************************

/// <summary>
/// The ViewModel namespace.
/// </summary>
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Class RoleViewModel.
    /// </summary>

    public class RoleViewModel
    {
        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        public long? RoleId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the RoleName identifier.
        /// </summary>
        /// <value>
        /// The RoleName.
        /// </value>
        public string RoleName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the UserType identifier.
        /// </summary>
        /// <value>
        /// The UserType.
        /// </value>
        public string UserType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets PatientInfoEditable
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is patient information editable; otherwise, <c>false</c>.
        /// </value>
        public bool IsPatientInfoEditable { get; set; }

        /// <summary>
        /// Gets or Sets MedicalInfoEditable
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is medical information editable; otherwise, <c>false</c>.
        /// </value>
        public bool IsMedicalInfoEditable { get; set; }

        /// <summary>
        /// Gets or Sets PrescriptionEditable
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is prescription editable; otherwise, <c>false</c>.
        /// </value>
        public bool IsPrescriptionEditable { get; set; }
    }
}