﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// ExceptionViewModel
    /// </summary>
    public class ExceptionViewModel
    {
        /// <summary>
        /// ErrorStatus
        /// </summary>
        public string ErrorStatus { get; set; }

        /// <summary>
        /// ErrorMessage
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// JsMethodName
        /// </summary>
        public string JsMethodName { get; set; }

        /// <summary>
        /// JsFileName
        /// </summary>
        public string JsFileName { get; set; }

    }
}