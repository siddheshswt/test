﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind Nishad
// Created          : 03-04-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="CareHomeViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the CareHomeViewModel file.</summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using SCA.Indigo.Business.BusinessModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// Carehome View Model
    /// </summary>
    public class CareHomeViewModel
    {
        /// <summary>
        /// Gets or sets the delivery frequency.
        /// </summary>
        /// <value>
        /// The delivery frequency.
        /// </value>
        public string DeliveryFrequency { get; set; }

        /// <summary>
        /// Gets or sets the round identifier.
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        public string RoundId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        public string DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the next delivery date.
        /// </summary>
        /// <value>
        /// The next delivery date.
        /// </value>
        public string NextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        public long CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the customers list.
        /// </summary>
        /// <value>
        /// The customers list.
        /// </value>
        public SelectList CustomersList { get; set; }

        /// <summary>
        /// Gets or sets the county list.
        /// </summary>
        /// <value>
        /// The county list.
        /// </value>
        public SelectList CountyList { get; set; }

        /// <summary>
        /// Gets or sets the carehome type list.
        /// </summary>
        /// <value>
        /// The carehome type list.
        /// </value>
        public SelectList CarehomeTypeList { get; set; }

        /// <summary>
        /// Gets or sets the carehome status list.
        /// </summary>
        /// <value>
        /// The carehome status list.
        /// </value>
        public SelectList CarehomeStatusList { get; set; }

        /// <summary>
        /// Gets or sets the carehome order type list.
        /// </summary>
        /// <value>
        /// The carehome order type list.
        /// </value>
        public SelectList CarehomeOrderTypeList { get; set; }

        /// <summary>
        /// Gets or sets the communication format list.
        /// </summary>
        /// <value>
        /// The communication format list.
        /// </value>
        public SelectList CommunicationFormatList { get; set; }

        /// <summary>
        /// Gets or sets the house number.
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        public string HouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the house.
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        public string HouseName { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the town or city.
        /// </summary>
        /// <value>
        /// The town or city.
        /// </value>
        public string TownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the address identifier.
        /// </summary>
        /// <value>
        /// The address identifier.
        /// </value>
        public long AddressId { get; set; }

        /// <summary>
        /// Gets or sets the personal information identifier.
        /// </summary>
        /// <value>
        /// The personal information identifier.
        /// </value>
        public long PersonalInformationId { get; set; }

        /// <summary>
        /// Gets or sets the phone no.
        /// </summary>
        /// <value>
        /// The phone no.
        /// </value>
        public string PhoneNo{ get; set; }

        /// <summary>
        /// Gets or sets the mobile no.
        /// </summary>
        /// <value>
        /// The mobile no.
        /// </value>
        public string MobileNo { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the sap customer number.
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        public string SAPCustomerNumber { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the round.
        /// </summary>
        /// <value>
        /// The round.
        /// </value>
        public string Round { get; set; }

        /// <summary>
        /// Gets or sets the bill to.
        /// </summary>
        /// <value>
        /// The bill to.
        /// </value>
        public long BillTo { get; set; }

        /// <summary>
        /// Gets or sets CareHomeType
        /// </summary>
        /// <value>
        /// The type of the care home.
        /// </value>
        
        public long? CareHomeType { get; set; }
        /// <summary>
        /// Gets or sets OrderType
        /// </summary>
        /// <value>
        /// The type of the order.
        /// </value>
        
        public long? OrderType { get; set; }

        /// <summary>
        /// Gets or sets PurchaseOrderNo
        /// </summary>
        /// <value>
        /// The purchase order no.
        /// </value>        
        public string PurchaseOrderNo { get; set; }
        
        /// <summary>
        /// Gets or sets CareHomeStatus
        /// </summary>
        /// <value>
        /// The care home status.
        /// </value>        
        public long? CareHomeStatus { get; set; }

        /// <summary>
        /// Gets or sets the fax number.
        /// </summary>
        /// <value>
        /// The fax number.
        /// </value>
        public string FaxNumber { get; set; }

        /// <summary>
        /// Gets or sets the patient information view model.
        /// </summary>
        /// <value>
        /// The patient information view model.
        /// </value>
        public PatientInfoViewModel PatientInfoViewModel { get; set; }

        /// <summary>
        /// Gets or sets the carehome report view model.
        /// </summary>
        ///  /// <value>
        /// The CareHome Report View Model.
        /// </value>
        public CareHomeReportViewModel HeaderData { get; set; }

        /// <summary>
        /// Gets or sets the sap care home number.
        /// </summary>
        /// <value>
        /// The sap care home number.
        /// </value>
        public string SapCareHomeNumber { get; set; }

        /// <summary>
        /// Gets or sets the communication format.
        /// </summary>
        /// <value>
        /// The communication format.
        /// </value>
        public long? CommunicationFormat { get; set; }

        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets the holiday list.
        /// </summary>
        /// <value>
        /// The holiday list.
        /// </value>
        public List<HolidayBusinessModel> HolidayList { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show view alert].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show view alert]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowViewAlert { get; set; }

        /// <summary>
        /// Gets or sets the file count error message.
        /// </summary>
        /// <value>
        /// The file count error message.
        /// </value>
        public string FileCountErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the is called from home page.
        /// </summary>
        /// <value>
        /// The is called from home page.
        /// </value>
        public string IsCalledFromHomePage { get; set; }

        /// <summary>
        /// Gets or sets the care home interaction identifier.
        /// </summary>
        /// <value>
        /// The care home interaction identifier.
        /// </value>
        public string CareHomeInteractionId { get; set; }

        /// <summary>
        /// Gets or sets the is called from search page.
        /// </summary>
        /// <value>
        /// The is called from search page.
        /// </value>
        public string IsCalledFromSearchPage { get; set; }

        /// <summary>
        /// Gets or sets Ndd Star tDate
        /// </summary>        
        public string NddStartDate { get; set; }

        /// <summary>
        /// Gets or sets Ndd End tDate
        /// </summary>        
        public string NddEndDate { get; set; }

        /// <summary>
        /// Gets or sets the is called from interaction menu.
        /// </summary>
        /// <value>
        /// The is called from interaction menu.
        /// </value>
        public string IsCalledFromInteractionMenu { get; set; }

        /// <summary>
        /// Gets or sets the communication preferences.
        /// </summary>
        /// <value>
        /// The communication preferences.
        /// </value>
        public string[] CommunicationPreferences { get; set; }

        /// <summary>
        /// Gets or sets the communication preferences.
        /// </summary>
        /// <value>
        /// The communication preferences.
        /// </value>
        public string CarehomeCommunicationPreferences { get; set; }

        /// <summary>
        /// Gets or sets the HasAnyActiveOrderForOrderType.
        /// </summary>
        public bool HasAnyActiveOrderForOrderType { get; set; }
        /// <summary>
        /// Gets or sets the Carehome Status.
        /// </summary>
        /// <value>
        /// The Carehome Status.
        /// </value>
        public long? OldCarehomeStatus { get; set; }
    }
}