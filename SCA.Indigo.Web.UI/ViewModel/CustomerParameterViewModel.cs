﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Mamatha Shetty
// Created          : 03-04-2015
//
// ***********************************************************************
// <copyright file="CustomerParameterViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// Customer Parameter View Model
    /// </summary>
    public class CustomerParameterViewModel
    {
        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long CustomerID { get; set; }

        /// <summary>
        /// Gets or sets SAP Customer Id
        /// </summary>
        /// <value>
        /// The sap customer identifier.
        /// </value>
        public string SAPCustomerID { get; set; }

        /// <summary>
        /// Gets or sets order creation
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is order creation allowed; otherwise, <c>false</c>.
        /// </value>
        public bool IsOrderCreationAllowed { get; set; }

        /// <summary>
        /// Gets or sets Order lead time
        /// </summary>
        /// <value>
        /// The order lead time.
        /// </value>
        public long OrderLeadTime { get; set; }

        /// <summary>
        /// Gets or sets allow increased quantity
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow increase order quantity]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowIncreaseOrderQuantity { get; set; }
        
        /// <summary>
        /// Gets or sets Cut Off Hours List
        /// </summary>
        /// <value>
        /// The cut off hours list.
        /// </value>
        public SelectList CutOffHoursList { get; set; }

        /// <summary>
        /// Gets or sets Cut Off Minutes List
        /// </summary>
        /// <value>
        /// The cut off minutes list.
        /// </value>
        public SelectList CutOffMinutesList { get; set; }

        /// <summary>
        /// Gets or sets Cut Off Seconds List
        /// </summary>
        /// <value>
        /// The cut off seconds list.
        /// </value>
        public SelectList CutOffSecondsList { get; set; }

        /// <summary>
        /// Gets or sets Cut Off Hour
        /// </summary>
        /// <value>
        /// The cut off hour.
        /// </value>
        public string CutOffHour { get; set; }

        /// <summary>
        /// Gets or sets Cut Off Minute
        /// </summary>
        /// <value>
        /// The cut off minute.
        /// </value>
        public string CutOffMinute { get; set; }

        /// <summary>
        /// Gets or sets Cut Off Second
        /// </summary>
        /// <value>
        /// The cut off second.
        /// </value>
        public string CutOffSecond { get; set; }

        /// <summary>
        /// Gets or sets Age Bill To
        /// </summary>
        /// <value>
        ///   <c>true</c> if [age bill to]; otherwise, <c>false</c>.
        /// </value>
        public bool AgeBillTo { get; set; }

        /// <summary>
        /// Gets or sets Allow flag
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is NDD allow; otherwise, <c>false</c>.
        /// </value>
        public bool IsNDDAllow { get; set; }

        /// <summary>
        /// Gets or sets Collection Applicable
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is collection applicable; otherwise, <c>false</c>.
        /// </value>
        public bool IsCollectionApplicable { get; set; }

        /// <summary>
        /// Gets or sets Paid Age Limit
        /// </summary>
        /// <value>
        /// The paed age limit.
        /// </value>
        public int? PaedAgeLimit { get; set; }

        /// <summary>
        /// Gets or sets Mandatory flag
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is nhsid mandatory; otherwise, <c>false</c>.
        /// </value>
        public bool IsNHSIDMandatory { get; set; }

        /// <summary>
        /// Gets or sets Mandatory flag
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is adp mandatory; otherwise, <c>false</c>.
        /// </value>
        public bool IsADPMandatory { get; set; }

        /// <summary>
        /// Gets or sets Telephone Mandatory
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is telephone mandatory; otherwise, <c>false</c>.
        /// </value>
        public bool IsTelephoneMandatory { get; set; }

        /// <summary>
        /// Gets or sets Next Assessment Date Mandatory
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is next assessment date mandatory; otherwise, <c>false</c>.
        /// </value>
        public bool IsNextAssessmentDateMandatory { get; set; }

        /// <summary>
        /// Gets or sets Maximum pad
        /// </summary>
        /// <value>
        /// The maximum PPD.
        /// </value>
        public decimal MaxPPD { get; set; }

        /// <summary>
        /// Gets or sets Default Next Assessment Date
        /// </summary>
        /// <value>
        /// The default next assessment date.
        /// </value>
        public int? DefaultNextAssessmentDate { get; set; }

        /// <summary>
        /// Gets or sets Medical Staff
        /// </summary>
        /// <value>
        /// The medical staff.
        /// </value>
        public List<ListTypeAdvancedBusinessModel> MedicalStaff { get; set; }

        /// <summary>
        /// Gets or sets Analysis Information
        /// </summary>
        /// <value>
        /// The analysis information.
        /// </value>
        public List<ListTypeAdvancedBusinessModel> AnalysisInformation { get; set; }

        /// <summary>
        /// Gets or sets Remove Patient Reason Code
        /// </summary>
        /// <value>
        /// The remove patient reason codes.
        /// </value>
        public List<ListTypeAdvancedBusinessModel> RemovePatientReasonCodes { get; set; }

        /// <summary>
        /// Gets or sets Stop Patient Reason Code
        /// </summary>
        /// <value>
        /// The stop patient reason codes.
        /// </value>
        public List<ListTypeAdvancedBusinessModel> StopPatientReasonCodes { get; set; }

        /// <summary>
        /// Gets or sets Clinical Contact With Linkage
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is clinical contact with linkage; otherwise, <c>false</c>.
        /// </value>
        public bool IsClinicalContactWithLinkage { get; set; }

        /// <summary>
        /// Gets or sets Sample Order Product Type
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is collection applicable; otherwise, <c>false</c>.
        /// </value>
        public string SampleOrderProductType { get; set; }
    }
}