﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="PatientViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// Patient View Model
    /// </summary>
    public class PatientViewModel
    {
        // public PatientInfoViewModel PatientInfoViewModel { get; set; }
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public IEnumerable<SelectListItem> CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the view model patient analysis medical staff.
        /// </summary>
        /// <value>
        /// The view model patient analysis medical staff.
        /// </value>
        public PatientAnalysisMedicalStaffViewModel ViewModelPatientAnalysisMedicalStaff { get; set; }

        /// <summary>
        /// Gets or sets the view model patient prescription.
        /// </summary>
        /// <value>
        /// The view model patient prescription.
        /// </value>
        public PatientPrescriptionViewModel ViewModelPatientPrescription { get; set; }

        /// <summary>
        /// Gets or sets the view model patient information.
        /// </summary>
        /// <value>
        /// The view model patient information.
        /// </value>
        public PatientInfoViewModel ViewModelPatientInfo { get; set; }

        /// <summary>
        /// Gets or sets the session is called from care home.
        /// </summary>
        /// <value>
        /// The session is called from care home.
        /// </value>
        public string SessionIsCalledFromCareHome { get; set; }
    }
}