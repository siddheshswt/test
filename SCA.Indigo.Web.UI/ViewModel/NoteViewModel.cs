﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind Nishad
// Created          : 30-04-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="CareHomeViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the NoteViewModel file.</summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using SCA.Indigo.Business.BusinessModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// Note View Model
    /// </summary>
    public class NoteViewModel
    {
        /// <summary>
        /// Gets or sets the note identifier.
        /// </summary>
        /// <value>
        /// The note identifier.
        /// </value>
        public long NoteId { get; set; }

        /// <summary>
        /// Gets or sets the type of the note.
        /// </summary>
        /// <value>
        /// The type of the note.
        /// </value>
        public long NoteType { get; set; }

        /// <summary>
        /// Gets or sets the note type identifier.
        /// </summary>
        /// <value>
        /// The note type identifier.
        /// </value>
        public long NoteTypeId { get; set; }

        /// <summary>
        /// Gets or sets the note text.
        /// </summary>
        /// <value>
        /// The note text.
        /// </value>
        public string NoteText { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets Alert as true/false
        /// </summary>
        public bool IsDisplayAsAlert { get; set; }
    }
}