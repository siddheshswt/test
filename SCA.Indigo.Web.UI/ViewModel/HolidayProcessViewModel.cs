﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind Nishad
// Created          : 23-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="HolidayProcessViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the HolidayProcessViewModel file.</summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// Holiday process view model
    /// </summary>
    public class HolidayProcessViewModel
    {
        /// <summary>
        /// Gets or Sets Customers
        /// </summary>
        /// <value>
        /// The customers.
        /// </value>
        public SelectList Customers { get; set; }

        /// <summary>
        /// Gets or Sets Next Delievery Date
        /// </summary>
        /// <value>
        /// The NDD.
        /// </value>
        [DataType(DataType.Date)]
        public string NDD { get; set; }

        /// <summary>
        /// Gets or sets the arranged NDD.
        /// </summary>
        /// <value>
        /// The arranged NDD.
        /// </value>
        /// Gets or Sets Arranged Next Delievery Date
        [DataType(DataType.Date)]
        public string ArrangedNDD { get; set; }

        /// <summary>
        /// Gets or Sets Hours
        /// </summary>
        /// <value>
        /// The hours.
        /// </value>
        public SelectList Hours { get; set; }

        /// <summary>
        /// Gets or Sets Minutes
        /// </summary>
        /// <value>
        /// The minutes.
        /// </value>
        public SelectList Minutes { get; set; }

        /// <summary>
        /// Gets or Sets Holiday Process Admin Details List
        /// </summary>
        /// <value>
        /// The holiday process admin details.
        /// </value>
        public List<HolidayProcessAdminDetails> HolidayProcessAdminDetails { get; set; }

        /// <summary>
        /// Gets or Sets Customer Count
        /// </summary>
        /// <value>
        /// The customer count.
        /// </value>
        public int CustomerCount { get; set; }
    }
}