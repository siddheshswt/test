﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SCA.Indigo.Business.BusinessModels;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// Audit Log View model
    /// </summary>
    [Serializable]
    public class AuditLogViewModel
    {

        /// <summary>
        /// Gets the AuditLogs for selected parameter
        /// </summary>
        /// <value>
        /// The audit logs.
        /// </value>
        public List<AuditLogReportBusinessModel> AuditLogs { get; set; }

        #region Frontend Id's
        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public long PatientId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        public long? CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public Guid UserId { get; set; }
        #endregion

        #region Names
        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }
        #endregion

        #region SAP Id's
        /// <summary>
        /// Gets or sets the patient sap identifier.
        /// </summary>
        /// <value>
        /// The patient sap identifier.
        /// </value>
        public string PatientSAPId { get; set; }

        /// <summary>
        /// Gets or sets the customer sap identifier.
        /// </summary>
        /// <value>
        /// The customer sap identifier.
        /// </value>
        public string CustomerSAPId { get; set; }

        /// <summary>
        /// Gets or sets the care home sap identifier.
        /// </summary>
        /// <value>
        /// The care home sap identifier.
        /// </value>
        public string CareHomeSAPId { get; set; }
        #endregion

        /// <summary>
        /// Sets TotalRecords
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        public long TotalRecords { get; set; }
    }
}