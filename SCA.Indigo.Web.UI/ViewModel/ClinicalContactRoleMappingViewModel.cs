﻿// ***********************************************************************
// <copyright file="ClinicalContactValueMappingViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the ClinicalContactValueMappingViewModel file.</summary>
// ***********************************************************************

/// <summary>
/// The ViewModel namespace.
/// </summary>
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Class ClinicalContactRoleMappingViewModel.
    /// </summary>
    public class ClinicalContactRoleMappingViewModel
    {
        /// <summary>
        /// Gets or sets the clinical contact role mapping identifier.
        /// </summary>
        /// <value>
        /// The clinical contact role mapping identifier.
        /// </value>
        public long? ClinicalContactRoleMappingId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the hierarchy list identifier.
        /// </summary>
        /// <value>
        /// The hierarchy list identifier.
        /// </value>
        public long? HierarchyListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the hierarchy.
        /// </summary>
        /// <value>
        /// The hierarchy.
        /// </value>
        public string Hierarchy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Parent.
        /// </summary>
        /// <value>
        /// The Parent.
        /// </value>
        public string Parent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Child.
        /// </summary>
        /// <value>
        /// The Child.
        /// </value>
        public string Child
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the child list identifier.
        /// </summary>
        /// <value>
        /// The child list identifier.
        /// </value>
        public long? ChildListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parent list identifier.
        /// </summary>
        /// <value>
        /// The parent list identifier.
        /// </value>
        public long? ParentListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// CustomerId.
        /// </value>
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets parentExistFlag
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is parent exist; otherwise, <c>false</c>.
        /// </value>
        public bool IsParentExist { get; set; }
    }
}