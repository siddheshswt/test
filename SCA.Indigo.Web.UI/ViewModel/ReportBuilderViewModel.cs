﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Jagdish
// Created          : 4 May 2015
// ***********************************************************************
// <copyright file="ReportBuilderViewModel.cs" company="Capgemini">
//     Copyright © Capgemini 2014
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using SCA.Indigo.Business.BusinessModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Report Builder View Model
    /// </summary>
    public class ReportBuilderViewModel
    {
        /// <summary>
        /// Gets or sets the report builder identifier.
        /// </summary>
        /// <value>
        /// The report builder identifier.
        /// </value>
        public string ReportBuilderId { get; set; }

        /// <summary>
        /// Gets or sets the name of the report format.
        /// </summary>
        /// <value>
        /// The name of the report format.
        /// </value>
        public string ReportFormatName { get; set; }

        /// <summary>
        /// Gets or sets the column names.
        /// </summary>
        /// <value>
        /// The column names.
        /// </value>
        public string ColumnNames { get; set; }

        /// <summary>
        /// Gets or sets the column headers.
        /// </summary>
        /// <value>
        /// The column headers.
        /// </value>
        public string ColumnHeaders { get; set; }

        /// <summary>
        /// Gets or sets the patient name identifier.
        /// </summary>
        /// <value>
        /// The patient name identifier.
        /// </value>
        public string PatientNameId { get; set; }

        /// <summary>
        /// Gets or sets the customer ids.
        /// </summary>
        /// <value>
        /// The customer ids.
        /// </value>
        public string CustomerIds { get; set; }

        /// <summary>
        /// Gets or sets the care home ids.
        /// </summary>
        /// <value>
        /// The care home ids.
        /// </value>
        public string CareHomeIds { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery date from.
        /// </summary>
        /// <value>
        /// The patient delivery date from.
        /// </value>
        public string PatientDeliveryDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery date to.
        /// </summary>
        /// <value>
        /// The patient delivery date to.
        /// </value>
        public string PatientDeliveryDateTo { get; set; }

        /// <summary>
        /// Gets or sets the order date from.
        /// </summary>
        /// <value>
        /// The order date from.
        /// </value>
        public string OrderDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the order date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        public string OrderDateTo { get; set; }

        /// <summary>
        /// Gets or sets the invoice date from.
        /// </summary>
        /// <value>
        /// The invoice date from.
        /// </value>
        public string InvoiceDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the invoice date to.
        /// </summary>
        /// <value>
        /// The invoice date to.
        /// </value>
        public string InvoiceDateTo { get; set; }

        /// <summary>
        /// Gets or sets the Patient Create Date from.
        /// </summary>
        /// <value>
        /// The Patient Create Date from.
        /// </value>
        public string PatientCreateDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Patient Create Date to.
        /// </summary>
        /// <value>
        /// The Patient Create Date to.
        /// </value>
        public string PatientCreateDateTo { get; set; }

        /// <summary>
        /// Gets or sets the Next Assessment Date from.
        /// </summary>
        /// <value>
        /// The Next Assessment Date from.
        /// </value>
        public string NextAssessmentDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Next Assessment Date to.
        /// </summary>
        /// <value>
        /// The Next Assessment Date to.
        /// </value>
        public string NextAssessmentDateTo { get; set; }
        /// <summary>
        /// Gets or sets the invoice number.
        /// </summary>
        /// <value>
        /// The invoice number.
        /// </value>
        public string InvoiceNumber { get; set; }


        /// <summary>
        /// Gets or sets the invoice number.
        /// </summary>
        /// <value>
        /// The invoice number to.
        /// </value>
        public string InvoiceNumberTo { get; set; }

        /// <summary>
        /// Gets or sets the name of the selected fields.
        /// </summary>
        /// <value>
        /// The name of the selected fields.
        /// </value>
        public string SelectedFieldsName { get; set; }

        /// <summary>
        /// Gets or sets the type of the patient.
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        public string PatientType { get; set; }

        /// <summary>
        /// Gets or sets the patient status.
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        public string PatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the customers list.
        /// </summary>
        /// <value>
        /// The customers list.
        /// </value>
        public SelectList CustomersList { get; set; }

        /// <summary>
        /// Gets or sets the care home list.
        /// </summary>
        /// <value>
        /// The care home list.
        /// </value>
        public SelectList CareHomeList { get; set; }

        /// <summary>
        /// Gets or sets the report format list.
        /// </summary>
        /// <value>
        /// The report format list.
        /// </value>
        public SelectList ReportFormatList { get; set; }

        /// <summary>
        /// Gets or sets the customer columns list.
        /// </summary>
        /// <value>
        /// The customer columns list.
        /// </value>
        public IEnumerable<SelectListItem> CustomerColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the patient columns list.
        /// </summary>
        /// <value>
        /// The patient columns list.
        /// </value>
        public IEnumerable<SelectListItem> PatientColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the prescription columns list.
        /// </summary>
        /// <value>
        /// The prescription columns list.
        /// </value>
        public IEnumerable<SelectListItem> PrescriptionColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the care home columns list.
        /// </summary>
        /// <value>
        /// The care home columns list.
        /// </value>
        public IEnumerable<SelectListItem> CareHomeColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the order columns list.
        /// </summary>
        /// <value>
        /// The order columns list.
        /// </value>
        public IEnumerable<SelectListItem> OrderColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the user main tenance columns list.
        /// </summary>
        /// <value>
        /// The user main tenance columns list.
        /// </value>
        public IEnumerable<SelectListItem> UserMainTenanceColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the clinical contacts columns list.
        /// </summary>
        /// <value>
        /// The clinical contacts columns list.
        /// </value>
        public IEnumerable<SelectListItem> ClinicalContactsColumnsList { get; set; }

        /// <summary>
        /// Gets or sets the report data list.
        /// </summary>
        /// <value>
        /// The report data list.
        /// </value>
        public List<ReportDataBusinessModel> ReportDataList { get; set; }

        /// <summary>
        /// Gets or sets the patient type list.
        /// </summary>
        /// <value>
        /// The patient type list.
        /// </value>
        public SelectList PatientTypeList { get; set; }

        /// <summary>
        /// Gets or sets the patient status list.
        /// </summary>
        /// <value>
        /// The patient status list.
        /// </value>
        public SelectList PatientStatusList { get; set; }

        /// <summary>
        /// Gets or sets the products list.
        /// </summary>
        /// <value>
        /// The products list.
        /// </value>
        public SelectList ProductsList { get; set; }

        /// <summary>
        /// Gets or sets the field customer columns.
        /// </summary>
        /// <value>
        /// The field customer columns.
        /// </value>
        public string FieldCustomerColumns { get; set; }

        /// <summary>
        /// Gets or sets the field customer parameter columns.
        /// </summary>
        /// <value>
        /// The field customer parameter columns.
        /// </value>
        public string FieldCustomerParameterColumns { get; set; }

        /// <summary>
        /// Gets or sets the field care home columns.
        /// </summary>
        /// <value>
        /// The field care home columns.
        /// </value>
        public string FieldCareHomeColumns { get; set; }

        /// <summary>
        /// Gets or sets the field patient columns.
        /// </summary>
        /// <value>
        /// The field patient columns.
        /// </value>
        public string FieldPatientColumns { get; set; }

        /// <summary>
        /// Gets or sets the field patient type matrix.
        /// </summary>
        /// <value>
        /// The field patient type matrix.
        /// </value>
        public string FieldPatientTypeMatrix { get; set; }

        /// <summary>
        /// Gets or sets the field order columns.
        /// </summary>
        /// <value>
        /// The field order columns.
        /// </value>
        public string FieldOrderColumns { get; set; }

        /// <summary>
        /// Gets or sets the name of the field report format.
        /// </summary>
        /// <value>
        /// The name of the field report format.
        /// </value>
        public string FieldReportFormatName { get; set; }

        /// <summary>
        /// Gets or sets the field prescription columns.
        /// </summary>
        /// <value>
        /// The field prescription columns.
        /// </value>
        public string FieldPrescriptionColumns { get; set; }

        /// <summary>
        /// Gets or sets the field clinical contacts columns.
        /// </summary>
        /// <value>
        /// The field clinical contacts columns.
        /// </value>
        public string FieldClinicalContactsColumns { get; set; }

        /// <summary>
        /// Gets or sets the field user maintenance columns.
        /// </summary>
        /// <value>
        /// The field user maintenance columns.
        /// </value>
        public string FieldUserMaintenanceColumns { get; set; }

        /// <summary>
        /// Gets or sets the report format business model.
        /// </summary>
        /// <value>
        /// The report format business model.
        /// </value>
        public ReportFormatBusinessModel ReportFormatBusinessModel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [user data only].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [user data only]; otherwise, <c>false</c>.
        /// </value>
        public bool UserDataOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enable user table].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable user table]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableUserTable { get; set; }

        /// <summary>
        /// Gets or sets the page NBR.
        /// </summary>
        /// <value>
        /// The page NBR.
        /// </value>
        public int PageNbr { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets the sort column.
        /// </summary>
        /// <value>
        /// The sort column.
        /// </value>
        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets the sort dir.
        /// </summary>
        /// <value>
        /// The sort dir.
        /// </value>
        public string SortDir { get; set; }

        /// <summary>
        /// Gets or sets the product ids.
        /// </summary>
        /// <value>
        /// The product ids.
        /// </value>
        public string ProductIds { get; set; }

        /// <summary>
        /// Gets or sets the total rows.
        /// </summary>
        /// <value>
        /// The total rows.
        /// </value>
        public decimal TotalRows { get; set; }

        /// <summary>
        /// Gets or sets the total pages.
        /// </summary>
        /// <value>
        /// The total pages.
        /// </value>
        public int TotalPages { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is download.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is download; otherwise, <c>false</c>.
        /// </value>
        public bool IsDownload { get; set; }
    }
}