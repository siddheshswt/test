﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// Interaction List View Model Class
    /// </summary>
    public class InteractionListViewModel
    {
        /// <summary>
        /// Gets or sets the sr no.
        /// </summary>
        /// <value>
        /// The sr no.
        /// </value>
        public int SrNo { get; set; }

        /// <summary>
        /// Gets or sets the interaction identifier.
        /// </summary>
        /// <value>
        /// The interaction identifier.
        /// </value>
        public long InteractionId { get; set; }

        /// <summary>
        /// Gets or sets the interaction sub type identifier.
        /// </summary>
        /// <value>
        /// The interaction sub type identifier.
        /// </value>
        public long InteractionSubTypeId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the status text.
        /// </summary>
        /// <value>
        /// The status text.
        /// </value>
        public string StatusText { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        public string CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the interaction category.
        /// </summary>
        /// <value>
        /// The interaction category.
        /// </value>
        public string InteractionCategory { get; set; }

        /// <summary>
        /// Gets or sets the name of the logistic provider.
        /// </summary>
        /// <value>
        /// The name of the logistic provider.
        /// </value>
        public string LogisticProviderName { get; set; }

        /// <summary>
        /// Gets or sets the sap customer number.
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        public string SAPCustomerNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the patient number.
        /// </summary>
        /// <value>
        /// The patient number.
        /// </value>
        public string PatientNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the display as alert.
        /// </summary>
        /// <value>
        /// The display as alert.
        /// </value>
        public bool? DisplayAsAlert { get; set; }

        /// <summary>
        /// Gets or sets the sap care home number.
        /// </summary>
        /// <value>
        /// The sap care home number.
        /// </value>
        public string SAPCareHomeNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the interaction sub type category.
        /// </summary>
        /// <value>
        /// The interaction sub type category.
        /// </value>
        public string InteractionSubTypeCategory { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified on.
        /// </summary>
        /// <value>
        /// The modified on.
        /// </value>
        public string ModifiedOn { get; set; }

        /// <summary>
        /// Gets Or Sets the IsInteractionViewed.
        /// </summary>
        public bool IsInteractionViewed { get; set; }
    }
}