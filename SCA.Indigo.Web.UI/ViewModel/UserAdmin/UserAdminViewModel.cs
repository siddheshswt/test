﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Basantakumar Sahoo
// Created          : 03-10-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-27-2015
// ***********************************************************************
// <copyright file="UserAdminViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel.UserAdmin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Configuration;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// User Admin View Model
    /// </summary>
    public class UserAdminViewModel
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserID { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the selected identifier.
        /// </summary>
        /// <value>
        /// The selected identifier.
        /// </value>
        public string SelectedId { get; set; }

        /// <summary>
        /// Gets or sets the user to customer items.
        /// </summary>
        /// <value>
        /// The user to customer items.
        /// </value>
        public IEnumerable<SelectListItem> UserToCustomerItems { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
        public IEnumerable<SelectListItem> Users { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        /// <value>
        /// The roles.
        /// </value>
        public IEnumerable<SelectListItem> Roles { get; set; }

        /// <summary>
        /// Gets or sets the users business model.
        /// </summary>
        /// <value>
        /// The users business model.
        /// </value>
        public List<UsersBusinessModel> UsersBusinessModel { get; set; }

        /// <summary>
        /// Gets or sets the customer count.
        /// </summary>
        /// <value>
        /// The customer count.
        /// </value>
        public int CustomerCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is first time.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is first time; otherwise, <c>false</c>.
        /// </value>
        public bool IsFirstTime { get; set; }
    }
}