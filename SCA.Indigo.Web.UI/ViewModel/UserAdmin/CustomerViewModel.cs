﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;

using SCA.Indigo.Business.BusinessModels;
using SCA.Indigo.Common;
using SCA.Indigo.Web.UI.Helper;
using SCA.Indigo.Web.UI.Resource;

namespace SCA.Indigo.Web.UI.ViewModel.UserAdmin
{
    /// <summary>
    /// Customer View model
    /// </summary>
    public class CustomerViewModel
    {
        /// <summary>
        /// Gets or sets the customer items.
        /// </summary>
        /// <value>
        /// The customer items.
        /// </value>
        public IEnumerable<SelectListItem> CustomerItems { get; set; }

        /// <summary>
        /// Gets or sets the customer status.
        /// </summary>
        /// <value>
        /// The customer status.
        /// </value>
        public IEnumerable<SelectListItem> CustomerStatus { get; set; }

        /// <summary>
        /// Gets or sets the customer parameter view model.
        /// </summary>
        /// <value>
        /// The customer parameter view model.
        /// </value>
        public CustomerParameterViewModel CustomerParameterViewModel { get; set; }

        /// <summary>
        /// Gets or sets the customer business model.
        /// </summary>
        /// <value>
        /// The customer business model.
        /// </value>
        public CustomerBusinessModel CustomerBusinessModel { get; set; }

        /// <summary>
        /// Gets or sets the patient type matrix view model.
        /// </summary>
        /// <value>
        /// The patient type matrix view model.
        /// </value>
        public PatientTypeMatrixViewModel PatientTypeMatrixViewModel { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact mapping view model.
        /// </summary>
        /// <value>
        /// The clinical contact mapping view model.
        /// </value>
        public ClinicalContactMappingViewModel ClinicalContactMappingViewModel { get; set; }

        /// <summary>
        /// Gets or sets the selected customer identifier.
        /// </summary>
        /// <value>
        /// The selected customer identifier.
        /// </value>
        public string SelectedCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the file count error message.
        /// </summary>
        /// <value>
        /// The file count error message.
        /// </value>
        public string FileCountErrorMessage { get; set; }

        /// <summary>
        /// Get the Show View Alert
        /// </summary>
        public bool ShowCustomerViewAlert { get; set; }

    }
}