﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Siddharth Dilpak
// Created          : 06-05-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="AutomaticOrderViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the AutomaticOrderViewModel file.</summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Automatic Order View Model
    /// </summary>
    public class AutomaticOrderViewModel
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the customers list.
        /// </summary>
        /// <value>
        /// The customers list.
        /// </value>
        public SelectList CustomersList { get; set; }
    }
}