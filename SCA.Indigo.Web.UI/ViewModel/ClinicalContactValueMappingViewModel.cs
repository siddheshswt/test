﻿// ***********************************************************************
// <copyright file="ClinicalContactValueMappingViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the ClinicalContactValueMappingViewModel file.</summary>
// ***********************************************************************

/// <summary>
/// The ViewModel namespace.
/// </summary>
namespace SCA.Indigo.Web.UI.ViewModel
{

    /// <summary>
    /// Class ClinicalContactValueMappingViewModel.
    /// </summary>
    public class ClinicalContactValueMappingViewModel
    {
        /// <summary>
        /// Gets or sets the clinical contact value mapping identifier.
        /// </summary>
        /// <value>
        /// The clinical contact value mapping identifier.
        /// </value>
        public long? ClinicalContactValueMappingId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the clinical contact role mapping identifier.
        /// </summary>
        /// <value>
        /// The clinical contact role mapping identifier.
        /// </value>
        public long? ClinicalContactRoleMappingId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the child value identifier.
        /// </summary>
        /// <value>
        /// The child value identifier.
        /// </value>
        public string ChildValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parent value identifier.
        /// </summary>
        /// <value>
        /// The parent value identifier.
        /// </value>
        public string ParentValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the child list identifier.
        /// </summary>
        /// <value>
        /// The child list identifier.
        /// </value>
        public long? ChildListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the child list.
        /// </summary>
        /// <value>
        /// The name of the child list.
        /// </value>
        public string ChildListName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the hierarchy list identifier.
        /// </summary>
        /// <value>
        /// The hierarchy list identifier.
        /// </value>
        public long? HierarchyListId
        {
            get;
            set;
        }

        /// <summary>
        /// gets or sets Hierarchy Name
        /// </summary>
        /// <value>
        /// The name of the hierarchy.
        /// </value>
        public string HierarchyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is removed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is removed; otherwise, <c>false</c>.
        /// </value>
        public bool IsRemoved
        {
            get;
            set;
        }

        /// <summary>
        /// gets or sets Parent List Id
        /// </summary>
        /// <value>
        /// The parent list identifier.
        /// </value>
        public long? ParentListId
        {
            get;
            set;
        }

        /// <summary>
        /// gets or sets Parent List Name
        /// </summary>
        /// <value>
        /// The name of the parent list.
        /// </value>
        public string ParentListName
        {
            get;
            set;
        }

        /// <summary>
        /// gets or sets Parent Value Id
        /// </summary>
        /// <value>
        /// The parent value identifier.
        /// </value>
        public long? ParentValueId
        {
            get;
            set;
        }

        /// <summary>
        /// gets or sets Child Value Id
        /// </summary>
        /// <value>
        /// The child value identifier.
        /// </value>
        public long? ChildValueId
        {
            get;
            set;
        }
    }
}