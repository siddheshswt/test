﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sagar Yerva 
// Created          : 02-02-2015
//
// Last Modified By : siddhsaw
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="GetSampleOrderViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// Sample Order View Model
    /// </summary>
    public class GetSampleOrderViewModel
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the town or city.
        /// </summary>
        /// <value>
        /// The town or city.
        /// </value>
        public string TownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the name of the sample house.
        /// </summary>
        /// <value>
        /// The name of the sample house.
        /// </value>
        public string SampleHouseName { get; set; }

        /// <summary>
        /// Gets or sets the sample house number.
        /// </summary>
        /// <value>
        /// The sample house number.
        /// </value>
        public string SampleHouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets the is called from edit.
        /// </summary>
        /// <value>
        /// The is called from edit.
        /// </value>
        public bool? IsCalledFromEdit { get; set; }        

        /// <summary>
        /// Gets or sets the selected identifier.
        /// </summary>
        /// <value>
        /// The selected identifier.
        /// </value>
        public string SelectedId { get; set; }

        /// <summary>
        /// Gets or sets the County Text
        /// </summary>
        /// <value>
        /// The county text.
        /// </value>
        public string CountyText { get; set; }

        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public IEnumerable<SelectListItem> Items { get; set; }

        /// <summary>
        /// Gets or sets Title
        /// </summary>
        public string Title { get; set; }
    }

    /// <summary>
    /// drop down selected value
    /// </summary>
    public partial class SampleSelectedvalue
    {
        /// <summary>
        /// Gets or sets the sample county selected value.
        /// </summary>
        /// <value>
        /// The sample county selected value.
        /// </value>
        public object SampleCountySelectedValue { get; set; }
    }
}