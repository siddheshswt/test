﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Prafull
// Created          : 11-01-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="ResetPasswordViewModel.cs" company="Capgemini">
//     Copyright © Capgemini 2014
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Reset password view model class.
    /// </summary>
    public class ResetPasswordViewModel
    {
        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        /// <value>
        /// The new password.
        /// </value>
        [Required(ErrorMessage = @"Please Enter New Password")]
        [RegularExpression("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordValidation")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        /// <value>
        /// The confirm password.
        /// </value>
        [Required(ErrorMessage = @"Please Confirm New Password")]
        [RegularExpression("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})",
            ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordValidation")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword",
            ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordMatch")]
        [Remote("ValidateNewPassword", "Account", AdditionalFields = "UserId", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblValidThreePassword")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Gets or sets the User ID.
        /// </summary>
        /// <value>
        /// The User ID.
        /// </value>
        public string UserId { get; set; }
        /// <summary>
        /// Gets or sets the User Name.
        /// </summary>
        /// <value>
        /// The User Name.
        /// </value>
        public string Username { get; set; }
    }
}
