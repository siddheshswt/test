﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// Product Order View Model
    /// </summary>
    public class ProductOrdersViewModel
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the carehome identifier.
        /// </summary>
        /// <value>
        /// The carehome identifier.
        /// </value>
        public long CarehomeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public string Quantity { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        public string DeliveryDate { get; set; }

        /// <summary>
        /// Gets or Sets SAP Carehome Id
        /// </summary>
        public string SAPCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets PurchaseOrderNumber
        /// </summary>
        public string PurchaseOrderNumber { get; set; }
    }
}