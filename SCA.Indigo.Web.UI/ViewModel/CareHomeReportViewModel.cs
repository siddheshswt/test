﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SCA.Indigo.Business.BusinessModels;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// CareHomeReportViewModel
    /// </summary>
    public class CareHomeReportViewModel
    {
        /// <summary>
        /// CustomerNameId
        /// </summary>       
        public string CustomerNameId { get; set; }

        /// <summary>
        /// CarehomeNDD
        /// </summary>
        public string CarehomeNDD { get; set; }

        /// <summary>
        /// AssessmentDate
        /// </summary>
        public string AssessmentDate { get; set; }

        /// <summary>
        /// CareHomeNameId
        /// </summary>
        public string CareHomeNameId { get; set; }

        /// <summary>
        /// CarehomeFrequency
        /// </summary>
        public string CarehomeFrequency { get; set; }

        /// <summary>
        /// AssessingNurese
        /// </summary>
        public string AssessingNurese { get; set; }

        /// <summary>
        /// CarehomeAddress
        /// </summary>
        public string CarehomeAddress { get; set; }

        /// <summary>
        /// CarehomePhoneNo
        /// </summary>
        public string CarehomePhoneNo { get; set; }

        /// <summary>
        /// Sugnature
        /// </summary>
        public string Sugnature { get; set; }

        /// <summary>
        /// DetailsData
        /// </summary>
        public List<CareHomeReportDetails> DetailsData { get; set; }


        /// <summary>
        /// PartialDetailsData
        /// </summary>
        public List<CareHomeReportDetails> PartialDetailsData { get; set; }
    }

    /// <summary>
    /// CareHomeReportDetails
    /// </summary>
    public class CareHomeReportDetails
    {

        /// <summary>
        /// PatientName
        /// </summary>
        public string PatientName { get; set; }

        /// <summary>
        /// PatientSAPId
        /// </summary>
        public string PatientSAPId { get; set; }

        /// <summary>
        /// DateOfBirth
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// PatientStatus
        /// </summary>
        public string PatientStatus { get; set; }

        /// <summary>
        /// LastAssessDate
        /// </summary>
        public string LastAssessDate { get; set; }

        /// <summary>
        /// NextAssessDate
        /// </summary>
        public string NextAssessDate { get; set; }

        /// <summary>
        /// SAPProductId
        /// </summary>
        public string SAPProductId { get; set; }

        /// <summary>
        /// ProductDescription
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// AssPPD
        /// </summary>
        public string AssPPD { get; set; }

        /// <summary>
        /// ActPPD
        /// </summary>
        public string ActPPD { get; set; }

        /// <summary>
        /// Frequency
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// NDD
        /// </summary>
        public string NDD { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// PatientDetailsReports
        /// </summary>
        public List<CarehomeMaintenanceReportBusinessModel> PatientDetailsReports { get; set; }
    }
}