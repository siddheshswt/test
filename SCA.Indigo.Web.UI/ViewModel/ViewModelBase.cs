﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="ViewModelBase.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// View Model Base
    /// </summary>
    public class ViewModelBase
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgFirstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgLastName")]
        public string LastName { get; set; }

        // [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgGender")]
        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgTitle")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the mobile no.
        /// </summary>
        /// <value>
        /// The mobile no.
        /// </value>
        [DataType(DataType.PhoneNumber)]
        public string MobileNo { get; set; }

        /// <summary>
        /// Gets or sets the phone no.
        /// </summary>
        /// <value>
        /// The phone no.
        /// </value>
        [DataType(DataType.PhoneNumber)]
        public string PhoneNo { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        [RegularExpression(@"[\w-]+@([\w-]+\.)+[\w-]+", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgEmailAddress")]   
        public string EmailAddress { get; set; }
    }
}