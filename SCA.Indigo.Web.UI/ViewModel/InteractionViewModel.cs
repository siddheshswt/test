﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : gaulad
// Created          : 04-07-2015
//
// Last Modified By : gaulad
// Last Modified On : 04-28-2015
// ***********************************************************************
// <copyright file="InteractionViewModel.cs" company="Capgemini">
//     Copyright © Capgemini 2014
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// Class InteractionViewModel.
    /// </summary>
    public class InteractionViewModel
    {
        /// <summary>
        /// Gets or sets the name of the parent form.
        /// </summary>
        /// <value>
        /// The name of the parent form.
        /// </value>
        public string ParentFormName { get; set; }

        /// <summary>
        /// Gets or sets the interaction identifier.
        /// </summary>
        /// <value>
        /// The interaction identifier.
        /// </value>
        public string InteractionId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the sap customer identifier.
        /// </summary>
        /// <value>
        /// The sap customer identifier.
        /// </value>
        public string SAPCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the sap patient identifier.
        /// </summary>
        /// <value>
        /// The sap patient identifier.
        /// </value>
        public string SAPPatientId { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public string PatientId { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        public string CarehomeName { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        public string CarehomeId { get; set; }

        /// <summary>
        /// Gets or sets the sap care home identifier.
        /// </summary>
        /// <value>
        /// The sap care home identifier.
        /// </value>
        public string SapCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the logistic provider.
        /// </summary>
        /// <value>
        /// The name of the logistic provider.
        /// </value>
        public string LogisticProviderName { get; set; }

        /// <summary>
        /// Gets or sets the interaction type identifier.
        /// </summary>
        /// <value>
        /// The interaction type identifier.
        /// </value>
        public string InteractionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the interaction sub.
        /// </summary>
        /// <value>
        /// The type of the interaction sub.
        /// </value>
        public string InteractionSubType { get; set; }

        /// <summary>
        /// Gets or sets the interaction sub type identifier.
        /// </summary>
        /// <value>
        /// The interaction sub type identifier.
        /// </value>
        public string InteractionSubTypeId { get; set; }

        /// <summary>
        /// Gets or sets the status identifier.
        /// </summary>
        /// <value>
        /// The status identifier.
        /// </value>
        public string StatusId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [display as alert].
        /// </summary>
        /// <value>
        /// <c>null</c> if [display as alert] contains no value, <c>true</c> if [display as alert]; otherwise, <c>false</c>.
        /// </value>
        public bool? DisplayAsAlert { get; set; }

        /// <summary>
        /// Gets or sets the assigned to.
        /// </summary>
        /// <value>
        /// The assigned to.
        /// </value>
        public string AssignedTo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [display only].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [display only]; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayOnly { get; set; }

        /// <summary>
        /// Gets or sets the assigned to list.
        /// </summary>
        /// <value>
        /// The assigned to list.
        /// </value>
        public SelectList AssignedToList { get; set; }

        /// <summary>
        /// Gets or sets the status list.
        /// </summary>
        /// <value>
        /// The status list.
        /// </value>
        public SelectList StatusList { get; set; }

        /// <summary>
        /// Gets or sets the interaction type list.
        /// </summary>
        /// <value>
        /// The interaction type list.
        /// </value>
        public SelectList InteractionTypeList { get; set; }

        /// <summary>
        /// Gets or sets the interactions.
        /// </summary>
        /// <value>
        /// The interactions.
        /// </value>
        public List<InteractionListViewModel> Interactions { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the name of the label.
        /// </summary>
        /// <value>
        /// The name of the label.
        /// </value>
        public string LabelName { get; set; }

        /// <summary>
        /// Gets or sets the saved status message.
        /// </summary>
        /// <value>
        /// The saved status message.
        /// </value>
        public string SavedStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the saved status.
        /// </summary>
        /// <value>
        /// The saved status.
        /// </value>
        public int SavedStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is double clicked.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is double clicked; otherwise, <c>false</c>.
        /// </value>
        public bool IsDoubleClicked { get; set; }

        /// <summary>
        /// Gets or sets the home page alert.
        /// </summary>
        /// <value>
        /// The home page alert.
        /// </value>
        public string HomePageAlert { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public string ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the is called from interaction menu.
        /// </summary>
        /// <value>
        /// The is called from interaction menu.
        /// </value>
        public string IsCalledFromInteractionMenu { get; set; }

        /// <summary>
        /// Gets or sets the open carehome flag.
        /// </summary>
        /// <value>
        /// The open carehome flag.
        /// </value>
        public string OpenCarehomeFlag { get; set; }

        /// <summary>
        /// Gets or sets the Resolved by.
        /// </summary>
        /// <value>
        /// The Resolved by.
        /// </value>
        public string ResolvedBy { get; set; }

        /// <summary>
        /// Gets or sets the Resolved Date.
        /// </summary>
        /// <value>
        /// The Resolved Date.
        /// </value>
        public string ResolvedDate { get; set; }

        /// <summary>
        /// Gets or sets the Closed Date.
        /// </summary>
        /// <value>
        /// The Closed Date.
        /// </value>
        public string ClosedDate { get; set; }

        /// <summary>
        /// Gets or sets the Closed By.
        /// </summary>
        /// <value>
        /// The Closed By.
        /// </value>
        public string ClosedBy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [Resolved].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [Resolved]; otherwise, <c>false</c>.
        /// </value>
        public bool Resolved { get; set; }

        /// <summary>
        /// Gets or sets the User.
        /// </summary>
        /// <value>
        /// The User.
        /// </value>
        public string User { get; set; }

        /// <summary>
        /// Gets or sets the User Type.
        /// </summary>
        /// <value>
        /// The UserType.
        /// </value>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets the User Id.
        /// </summary>
        /// <value>
        /// The UserId.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or Sets IsBtnEnabled
        /// </summary>
        public bool IsBtnEnabled { get; set; }
    }
}