﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="SearchViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Search View Model
    /// </summary>
    public class SearchViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public IEnumerable<SelectListItem> CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the view model patient analysis medical staff.
        /// </summary>
        /// <value>
        /// The view model patient analysis medical staff.
        /// </value>
        public PatientAnalysisMedicalStaffViewModel ViewModelPatientAnalysisMedicalStaff { get; set; }

        /// <summary>
        /// Gets or sets the view model patient prescription.
        /// </summary>
        /// <value>
        /// The view model patient prescription.
        /// </value>
        public PatientPrescriptionViewModel ViewModelPatientPrescription { get; set; }

        /// <summary>
        /// Gets or sets the view model patient information.
        /// </summary>
        /// <value>
        /// The view model patient information.
        /// </value>
        public PatientInfoViewModel ViewModelPatientInfo { get; set; }

        /// <summary>
        /// Gets or sets the is from order.
        /// </summary>
        /// <value>
        /// The is from order.
        /// </value>
        public string IsFromOrder { get; set; }

        /// <summary>
        /// Gets or sets the search parameter.
        /// </summary>
        /// <value>
        /// The search parameter.
        /// </value>
        public string SearchParam { get; set; }

        /// <summary>
        /// Gets or sets the type to search.
        /// </summary>
        /// <value>
        /// The type to search.
        /// </value>
        public string TypeToSearch { get; set; }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        /// <value>
        /// The customer.
        /// </value>
        public string Customer { get; set; }

        /// <summary>
        /// Gets or sets the search status.
        /// </summary>
        /// <value>
        /// The search status.
        /// </value>
        public string SearchStatus { get; set; }

        /// <summary>
        /// Gets or sets the type of the patient.
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        public string PatientType { get; set; }

        /// <summary>
        /// Gets or sets the order nubmer.
        /// </summary>
        /// <value>
        /// The order nubmer.
        /// </value>
        public string OrderNubmer { get; set; }

        /// <summary>
        /// Gets or sets the search identifier.
        /// </summary>
        /// <value>
        /// The search identifier.
        /// </value>
        public string SearchId { get; set; }

        /// <summary>
        /// Gets or sets the is called from search.
        /// </summary>
        /// <value>
        /// The is called from search.
        /// </value>
        public string IsCalledFromSearch { get; set; }

        /// <summary>
        /// Gets or sets the is called from care home.
        /// </summary>
        /// <value>
        /// The is called from care home.
        /// </value>
        public string IsCalledFromCareHome { get; set; }
    }
}