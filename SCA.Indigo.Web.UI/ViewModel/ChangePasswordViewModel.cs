﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="ChangePasswordViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// Change Password View Model
    /// </summary>
    public class ChangePasswordViewModel
    {
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [Required(ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reserrormsgloginPasswordRequired")]
        [RegularExpression("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordValidation")]
        [Remote("ValidateCurrentPassword", "Account", AdditionalFields = "OldPassword", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblCurrentPassword")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        /// <value>
        /// The new password.
        /// </value>
        [Required(ErrorMessage = @"Please Enter New Password")]
        [RegularExpression("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordValidation")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        /// <value>
        /// The confirm password.
        /// </value>
        [Required(ErrorMessage = @"Please Confirm New Password")]
        [RegularExpression("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})",
            ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordValidation")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword",
            ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblPasswordMatch")]
        [Remote("ValidateNewPassword", "Account", AdditionalFields = "UserId", ErrorMessageResourceType = typeof(Resource.Common), ErrorMessageResourceName = "reslblValidThreePassword")]
        public string ConfirmPassword { get; set; }
    }
}