﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sagar Yerva
// Created          : 02-02-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="SampleProductViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// Sample product view model
    /// </summary>
    public class SampleProductViewModel
    {
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity { get; set; }
    }
}