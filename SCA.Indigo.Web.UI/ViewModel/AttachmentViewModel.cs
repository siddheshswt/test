﻿/// <summary>
/// Attachment Helper 
/// </summary>
namespace SCA.Indigo.Web.UI.ViewModel
{
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using System.Web;

    /// <summary>
    /// Attachment View Model
    /// </summary>
    public class AttachmentViewModel
    {
        /// <summary>
        /// Gets or sets the type identifier.
        /// </summary>
        /// <value>
        /// The type identifier.
        /// </value>
        public string TypeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the attachment.
        /// </summary>
        /// <value>
        /// The type of the attachment.
        /// </value>
        public string AttachmentType { get; set; }

        /// <summary>
        /// Gets or sets the name of the parent form.
        /// </summary>
        /// <value>
        /// The name of the parent form.
        /// </value>
        public string ParentFormName { get; set; }

        /// <summary>
        /// Gets or sets the attachment files.
        /// </summary>
        /// <value>
        /// The attachment files.
        /// </value>
        public List<HttpPostedFileBase> AttachmentFiles { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public string Path { get; set; }

    }
}