﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCA.Indigo.Web.UI.ViewModel
{
    /// <summary>
    /// Contact Person View Model
    /// </summary>
    public class ContactPersonViewModel
    {
        /// <summary>
        /// Gets or sets AddressId
        /// </summary>
        /// <value>
        /// The address identifier.
        /// </value>
       
        public long? AddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedById
        /// </summary>
        /// <value>
        /// The created by identifier.
        /// </value>
       
        public string CreatedById
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDateTime
        /// </summary>
        /// <value>
        /// The created date time.
        /// </value>
       
        public string CreatedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
       
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FaxNumber
        /// </summary>
        /// <value>
        /// The fax number.
        /// </value>
       
        public string FaxNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FirstName
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
       
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Gender
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
       
        public string Gender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
       
        public bool IsActive
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Job Title
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
       
        public string JobTitle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastName
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
       
        public string LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets MobileNumber
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
       
        public string MobileNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
       
        public string ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedDate
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
       
        public string ModifiedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientID
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
       
        public string PatientID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient name
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
       
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonalInformationId
        /// </summary>
        /// <value>
        /// The personal information identifier.
        /// </value>
       
        public long PersonalInformationId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonalInformationIds
        /// </summary>
        /// <value>
        /// The personal information ids.
        /// </value>
       
        public List<string> PersonalInformationIds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TelephoneNumber
        /// </summary>
        /// <value>
        /// The telephone number.
        /// </value>
       
        public string TelephoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TitleId
        /// </summary>
        /// <value>
        /// The title identifier.
        /// </value>
       
        public long? TitleId
        {
            get;
            set;
        }

    }
}