﻿// ***********************************************************************
// <copyright file="PatientTypeMatrixViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the PatientTypeMatrixViewModel file.</summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// Patient Type Matrix ViewModel
    /// </summary>
    public class PatientTypeMatrixViewModel
    {
        /// <summary>
        /// Gets or Sets Patient Type
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        public SelectList PatientType { get; set; }

        /// <summary>
        /// Gets or Sets list
        /// </summary>
        /// <value>
        /// The patient type matrix list.
        /// </value>
        public List<PatientTypeListViewModel> PatientTypeMatrixList { get; set; }

        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or Sets File path
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        public string FilePath { get; set; }
    }
}