﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sachin Phapale
// Created          : 08-04-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="PatientTypeListViewModel.cs" company="Capgemini">
// Capgemini. All rights reserved.
// </copyright>
// <summary>This is the PatientTypeListViewModel file.</summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Patient Type View Model
    /// </summary>
    public class PatientTypeListViewModel
    {
        /// <summary>
        /// Gets or sets PatientTypeId
        /// </summary>
        /// <value>
        /// The patient type identifier.
        /// </value>
        public long? PatientTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientType
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PrescMinFrequency
        /// </summary>
        /// <value>
        /// The presc minimum frequency.
        /// </value>
        public long? PrescMinFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PrescMaxFrequency
        /// </summary>
        /// <value>
        /// The presc maximum frequency.
        /// </value>
        public long? PrescMaxFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is add from prescription allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is add from prescription allowed; otherwise, <c>false</c>.
        /// </value>
        public bool IsAddFromPrescriptionAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is add from product allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is add from product allowed; otherwise, <c>false</c>.
        /// </value>
        public bool IsAddFromProductAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AdvanceOrderActivationDays
        /// </summary>
        /// <value>
        /// The advance order activation days.
        /// </value>
        public long? AdvanceOrderActivationDays
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ChangeOrderNddDays
        /// </summary>
        /// <value>
        /// The change order NDD days.
        /// </value>
        public long? ChangeOrderNddDays
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is one off add from prescription allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is one off add from prescription allowed; otherwise, <c>false</c>.
        /// </value>
        public bool IsOneOffAddFromPrescriptionAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is one off add from product allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is one off add from product allowed; otherwise, <c>false</c>.
        /// </value>
        public bool IsOneOffAddFromProductAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsPrescriptionApprovalRequired
        /// </summary>
        /// <value>
        /// The is prescription approval required.
        /// </value>
        public bool? IsPrescriptionApprovalRequired
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AdultBillTo
        /// </summary>
        /// <value>
        /// The adult bill to.
        /// </value>
        public long? AdultBillTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PaedBillTo
        /// </summary>
        /// <value>
        /// The paed bill to.
        /// </value>
        public long? PaedBillTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IncrementFrequency
        /// </summary>
        /// <value>
        /// The increment frequency.
        /// </value>
        public long? IncrementFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public Guid? CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDateTime
        /// </summary>
        /// <value>
        /// The created date time.
        /// </value>
        public DateTime? CreatedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public Guid? ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedDateTime
        /// </summary>
        /// <value>
        /// The modified date time.
        /// </value>
        public DateTime? ModifiedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsRemove
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is remove; otherwise, <c>false</c>.
        /// </value>
        public bool IsRemove
        {
            get;
            set;
        }

        /// <summary>
        /// Patient Type Matrix ID
        /// </summary>
        /// <value>
        /// The patient type matrix identifier.
        /// </value>
        public long PatientTypeMatrixID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DefaultFrequency
        /// </summary>
        /// <value>
        /// The Default frequency.
        /// </value>
        public long? DefaultFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListIndex
        /// </summary>
        /// <value>
        /// The index of the list.
        /// </value>
        public int ListIndex
        {
            get;
            set;
        }
    }
}