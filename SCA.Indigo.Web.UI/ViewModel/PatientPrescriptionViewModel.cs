﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="PatientPrescriptionViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Prescription View Model
    /// </summary>
    public class PatientPrescriptionViewModel
    {
        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public int PatientID { get; set; }

        /// <summary>
        /// Gets or sets the index prescription product identifier.
        /// </summary>
        /// <value>
        /// The index prescription product identifier.
        /// </value>
        public int IDXPrescriptionProductId { get; set; }

        /// <summary>
        /// Gets or sets the prescription identifier.
        /// </summary>
        /// <value>
        /// The prescription identifier.
        /// </value>
        public int PrescriptionId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the assessed pads per day.
        /// </summary>
        /// <value>
        /// The assessed pads per day.
        /// </value>
        public int AssessedPadsPerDay { get; set; }

        /// <summary>
        /// Gets or sets the is overridden flag.
        /// </summary>
        /// <value>
        /// The is overridden flag.
        /// </value>
        public bool IsOverriddenFlag { get; set; }

        /// <summary>
        /// Gets or sets the actual pads per day.
        /// </summary>
        /// <value>
        /// The actual pads per day.
        /// </value>
        public int ActualPadsPerDay { get; set; }

        /// <summary>
        /// Gets or sets the d q1.
        /// </summary>
        /// <value>
        /// The d q1.
        /// </value>
        public int DQ1 { get; set; }

        /// <summary>
        /// Gets or sets the d q2.
        /// </summary>
        /// <value>
        /// The d q2.
        /// </value>
        public int DQ2 { get; set; }

        /// <summary>
        /// Gets or sets the d q3.
        /// </summary>
        /// <value>
        /// The d q3.
        /// </value>
        public int DQ3 { get; set; }

        /// <summary>
        /// Gets or sets the d q4.
        /// </summary>
        /// <value>
        /// The d q4.
        /// </value>
        public int DQ4 { get; set; }

        /// <summary>
        /// Gets or sets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public int Frequency { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the valid from date.
        /// </summary>
        /// <value>
        /// The valid from date.
        /// </value>
        public string ValidFromDate { get; set; }

        /// <summary>
        /// Gets or sets the valid to date.
        /// </summary>
        /// <value>
        /// The valid to date.
        /// </value>
        public string ValidToDate { get; set; }

        /// <summary>
        /// Gets or sets the flag d q1.
        /// </summary>
        /// <value>
        /// The flag d q1.
        /// </value>
        public int FlagDQ1 { get; set; }

        /// <summary>
        /// Gets or sets the flag d q2.
        /// </summary>
        /// <value>
        /// The flag d q2.
        /// </value>
        public int FlagDQ2 { get; set; }

        /// <summary>
        /// Gets or sets the flag d q3.
        /// </summary>
        /// <value>
        /// The flag d q3.
        /// </value>
        public int FlagDQ3 { get; set; }

        /// <summary>
        /// Gets or sets the flag d q4.
        /// </summary>
        /// <value>
        /// The flag d q4.
        /// </value>
        public int FlagDQ4 { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public int ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        public int ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the prescription authorization approval flag.
        /// </summary>
        /// <value>
        /// The prescription authorization approval flag.
        /// </value>
        public bool? PrescriptionAuthorizationApprovalFlag { get; set; }

        /// <summary>
        /// Gets or sets the row status.
        /// </summary>
        /// <value>
        /// The row status.
        /// </value>
        public string RowStatus { get; set; }

        /// <summary>
        /// Gets or sets the sales unit.
        /// </summary>
        /// <value>
        /// The sales unit.
        /// </value>
        public string SalesUnit { get; set; }

        /// <summary>
        /// Gets or sets the actual PPD.
        /// </summary>
        /// <value>
        /// The actual PPD.
        /// </value>
        public double? ActualPPD { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        public string DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the is removed.
        /// </summary>
        /// <value>
        /// The is removed.
        /// </value>
        public bool? IsRemoved
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the approval flag.
        /// </summary>
        /// <value>
        /// The approval flag.
        /// </value>
        public string ApprovalFlag { get; set; }

        /// <summary>
        /// Gets or sets the change NDD.
        /// </summary>
        /// <value>
        /// The change NDD.
        /// </value>
        public string ChangeNDD { get; set; }

        /// <summary>
        /// Gets or sets the product approval flag.
        /// </summary>
        /// <value>
        /// The product approval flag.
        /// </value>
        public bool? ProductApprovalFlag { get; set; }

        /// <summary>
        /// Gets or sets the product Id.
        /// </summary>
        /// <value>
        /// The product approval flag.
        /// </value>
        public string ProductDisplayId { get; set; }
    }
}
