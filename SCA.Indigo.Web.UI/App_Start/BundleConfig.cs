﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-02-2015
// ***********************************************************************
// <copyright file="BundleConfig.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI
{
    using System.Web;
    using System.Web.Optimization;

    /// <summary>
    /// Class BundleConfig.
    /// </summary>
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862

        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            if (bundles != null)
            {
                bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

                bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

                bundles.Add(new ScriptBundle("~/bundles/Search").Include(
                "~/Scripts/jquery-ui.js",
                "~/Scripts/js/validations/Search.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/FormValidation.js",
                "~/Scripts/jquery.multiselect.js"));

                bundles.Add(new ScriptBundle("~/bundles/Account").Include(
                "~/Scripts/js/validations/Common.js"));

                bundles.Add(new ScriptBundle("~/bundles/SampleOrder").Include(
                "~/Scripts/js/validations/SampleOrder.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/SampleProductList.js",
                "~/Scripts/js/validations/PostcodeChecker.js"));

                bundles.Add(new ScriptBundle("~/bundles/Prescription").Include(
                "~/Scripts/js/validations/Prescription.js",
                "~/Scripts/js/validations/ProductList.js"));

                bundles.Add(new ScriptBundle("~/bundles/MedicalAnalysisInfoPopup").Include(
                "~/Scripts/js/validations/MedicalStaffEditPopup.js"));

                bundles.Add(new ScriptBundle("~/bundles/PatientInfo").Include(
                "~/Scripts/js/validations/PatientInfo.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/Notes.js",
                "~/Scripts/js/validations/PostcodeChecker.js",
                "~/Scripts/jquery.multiselect.js"));

                bundles.Add(new ScriptBundle("~/bundles/Order").Include(
                "~/Scripts/js/validations/Order.js"));

                bundles.Add(new ScriptBundle("~/bundles/CustomerParamter").Include(
                "~/Scripts/js/validations/CustomerParameter.js",
                "~/Scripts/js/validations/CustomerInformation.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/PatientTypeMatrix.js",
				"~/Scripts/js/validations/CustomerProductList.js",
                "~/Scripts/print.js"
                    ));

                bundles.Add(new ScriptBundle("~/bundles/PatientDetails").Include(
                "~/Scripts/js/validations/PatientDetails.js",
                 "~/Scripts/jquery-ui.js",
                "~/Scripts/js/validations/Search.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/FormValidation.js",
                "~/Scripts/js/validations/PatientInfo.js",
                "~/Scripts/js/validations/Notes.js",
                "~/Scripts/js/validations/PostcodeChecker.js",
                "~/Scripts/jquery.multiselect.js"
                ));

                bundles.Add(new ScriptBundle("~/bundles/Validations").Include(
                "~/Scripts/jquery-1.10.2.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js"));

                bundles.Add(new ScriptBundle("~/bundles/CareHomeMaintenance").Include(
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/Carehome.js",
                "~/Scripts/js/validations/PostcodeChecker.js",
                "~/Scripts/js/validations/Notes.js",
                "~/Scripts/jquery.multiselect.js",
                "~/Scripts/js/validations/CarehomeMaintanceReport.js"
               ));

                bundles.Add(new ScriptBundle("~/bundles/AuditLog").Include(
                      "~/Scripts/js/validations/Common.js",
                       "~/Scripts/jquery-ui.js",
                        "~/Scripts/js/validations/AuditLog.js"));

                // Use the development version of Modernizr to develop with and learn from. Then, when you're
                // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
                bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

                bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

                bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));

                bundles.Add(new ScriptBundle("~/bundles/HolidayProcessAdmin").Include(
                "~/Scripts/common.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/jquery.jqGrid.min.js",
                "~/Scripts/js/validations/HolidayProcessAdmin.js"));

                bundles.Add(new ScriptBundle("~/bundles/PatientTypeMatrix").Include(
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/PatientTypeMatrix.js"));

                bundles.Add(new ScriptBundle("~/bundles/UserAdmin").Include(
                "~/Scripts/common.js",
                "~/Scripts/js/validations/MapUserToRole.js",
                "~/Scripts/js/validations/MapUserCustomer.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/tab/jquery.pwstabs-1.2.1.js"
                    //,"~/Scripts/tab/jquery.pwstabs-1.2.1.min.js"
                )
                );

                bundles.Add(new ScriptBundle("~/bundles/MapUserToCareHome").Include(
                 "~/Scripts/jquery.multiselect.js",
                 "~/Scripts/select-with-search.js",
                 "~/Scripts/js/validations/Common.js",
                 "~/Scripts/js/validations/CarehomeUsers.js"));

                bundles.Add(new StyleBundle("~/Content/MapUserToCareHome").Include(
                "~/Content/jquery.multiselect.css",
                "~/Content/css/select-with-search.css"));

                bundles.Add(new StyleBundle("~/bundles/tab").Include(
                "~/Scripts/tab/jquery.pwstabs-1.2.1.css",
                "~/Scripts/tab/tab-style.css"));

                bundles.Add(new ScriptBundle("~/bundles/ContactInfo").Include(
                "~/Scripts/js/validations/ContactInfo.js"));

                bundles.Add(new ScriptBundle("~/bundles/RecentOrders").Include(
                "~/Scripts/js/validations/RecentOrders.js"));

                bundles.Add(new ScriptBundle("~/bundles/Interaction").Include(
                "~/Scripts/jquery-ui.js",
                "~/Scripts/jquery.jqGrid.min.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/Interaction.js"));

                bundles.Add(new ScriptBundle("~/bundles/InteractionReport").Include(
                "~/Scripts/js/validations/InteractionReport.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/jquery.multiselect.js"));

                bundles.Add(new ScriptBundle("~/bundles/ClinicalContactMapping").Include(
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/tab/jquery.pwstabs-1.2.1.js",
                "~/Scripts/js/validations/ClinicalContactMapping.js"));

                bundles.Add(new ScriptBundle("~/bundles/MassDataChanges").Include(
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/UploadExcel.js",
                 "~/Scripts/jquery-ui.js",
                "~/Scripts/js/validations/MassDataChanges.js",
                 "~/Scripts/jquery.multiselect.js"));

                bundles.Add(new ScriptBundle("~/bundles/MassDataImport").Include(
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/UploadExcel.js",
                "~/Scripts/js/validations/MassDataImport.js"));

                bundles.Add(new ScriptBundle("~/bundles/Home").Include(
                "~/Scripts/js/validations/Home.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/Interaction.js"));

                bundles.Add(new ScriptBundle("~/bundles/Note").Include(
              "~/Scripts/js/validations/Notes.js"));

                bundles.Add(new ScriptBundle("~/bundles/Attachment").Include(
            "~/Scripts/js/validations/Attachment.js"));

                bundles.Add(new ScriptBundle("~/bundles/AutomaticOrder").Include(
                "~/Scripts/js/validations/AutomaticOrder.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/jquery.multiselect.js"));

                bundles.Add(new ScriptBundle("~/bundles/Attachment").Include(
                "~/Scripts/js/validations/Attachment.js"));

                bundles.Add(new ScriptBundle("~/bundles/ReportBuilder").Include(
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/ReportBuilder.js",
                "~/Scripts/jquery.multiselect.js",
                "~/Scripts/print.js"));

                bundles.Add(new ScriptBundle("~/bundles/ProductOrders").Include(
                "~/Scripts/js/validations/ProductOrders.js",
                "~/Scripts/js/validations/Order.js",
                "~/Scripts/js/validations/Common.js"));

                bundles.Add(new ScriptBundle("~/bundles/UserResetUnlock").Include(
                "~/Scripts/js/validations/ResetUnlockUsers.js",
                "~/Scripts/js/validations/Common.js"));

                bundles.Add(new ScriptBundle("~/bundles/CopyPatientDetails").Include(
                "~/Scripts/js/validations/PatientDetails.js",
                 "~/Scripts/jquery-ui.js",
                "~/Scripts/js/validations/Search.js",
                "~/Scripts/js/validations/Common.js",
                "~/Scripts/js/validations/FormValidation.js",
                "~/Scripts/js/validations/PatientInfo.js",
                "~/Scripts/js/validations/Notes.js",
                "~/Scripts/js/validations/ContactInfo.js",
                "~/Scripts/js/validations/PostcodeChecker.js",
                "~/Scripts/jquery.multiselect.js"
                ));

                // Set EnableOptimizations to false for debugging. For more information,
                // visit http://go.microsoft.com/fwlink/?LinkId=301862
                BundleTable.EnableOptimizations = false;
            }
        }
    }
}
