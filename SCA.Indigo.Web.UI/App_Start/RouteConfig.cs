﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="RouteConfig.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional });
            routes.MapRoute(
                name: "CareHome",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "CareHome", action = "GetCareHome", id = UrlParameter.Optional });
            routes.MapRoute(
               name: "Customer",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Customer", action = "CustomerInformation", id = UrlParameter.Optional });
            routes.MapRoute(
             name: "Patient",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Patient", action = "Details", id = UrlParameter.Optional });
        }
    }
}
