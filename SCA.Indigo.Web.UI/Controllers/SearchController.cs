﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="SearchController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;

    using SCAEnums = SCA.Indigo.Common.Enums.SCAEnums;
    using System.Configuration; 

    /// <summary>
    /// Search Controller
    /// </summary>
    [Authorize]
    public class SearchController : Controller
    {
        /// <summary>
        /// The is from oder
        /// </summary>
        private string isFromOder = string.Empty;
        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.CurrentCulture);
        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);

        /// <summary>
        /// Get function for searchData
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>
        /// Search view with populated search filters.
        /// </returns>
        [HttpPost]
        public ActionResult SearchData(int? patientId)
        {
			var guidKey = Guid.NewGuid();
	        LogHelper.LogAction("SearchDataPost Start - " + guidKey, "Search", "SearchDataPost Start", globalUserId);
	        
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }
            ClearPatientSession();
            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            Session[Constants.SessionPatientId] = patientId;
            ControllerHelper.ActionName = Constants.ActionNameSearchCustomer;
            ControllerHelper.ActionParam = new[] { globalUserId };
            var customerResponse = ControllerHelper.GetMethodServiceRequest<List<DropDownDataBusinessModel>>();
            ViewBag.Customer = new SelectList(customerResponse, Constants.Value, Constants.DisplayText);

            LogHelper.LogAction(Constants.ActionLoadCustomers, Constants.SearchController, Constants.ActionGetCustomers, globalUserId);

            var modelSearch = new SearchViewModel
            {
                ViewModelPatientPrescription = new PatientPrescriptionViewModel(),
                ViewModelPatientInfo = new PatientInfoViewModel(),
                ViewModelPatientAnalysisMedicalStaff = new PatientAnalysisMedicalStaffViewModel()
            };

            modelSearch.ViewModelPatientInfo.InteractionViewModel = new InteractionViewModel();

            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            var patientTypeListId = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
            var patientStatusListId = Convert.ToInt64(SCAEnums.ListType.PatientStatus, CultureInfo.InvariantCulture);
            var languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);

            var listIds = new[] { patientTypeListId, patientStatusListId };
            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(listIds), Convert.ToString(languageId, CultureInfo.InvariantCulture) };

            var patientData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();

            var patientType = patientData.Where(c => c.ListTypeId.Equals(3));
            var pateintStatus = patientData.Where(c => c.ListTypeId.Equals(4));

            ViewBag.PatientType = new SelectList(patientType, Constants.ListId, Constants.DisplayText);
            LogHelper.LogAction(Constants.ActionLoadPatientTypes, Constants.SearchController, Constants.ActionGetLists, globalUserId);

            var customerStatusListId = Convert.ToInt64(SCAEnums.ListType.CustomerStatus, CultureInfo.InvariantCulture);
            var carehomeStatusListId = Convert.ToInt64(SCAEnums.ListType.CareHomeStatus, CultureInfo.InvariantCulture);

            ControllerHelper.ActionName = Constants.ActionNameGetStatusDropDown;

            var statusIds = new[] { patientStatusListId, customerStatusListId, carehomeStatusListId };

            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(statusIds), globalUserId };
            var patientStatusData = ControllerHelper.GetMethodServiceRequestForList<List<SearchBusinessModel>>();

            ViewBag.PateintStatus = new SelectList(patientStatusData, Constants.StatusDropDownvalue, Constants.StatusDropDownText);
            //ViewBag.PateintStatus = new SelectList(patientStatusData, Constants.ListId, Constants.DisplayText);
            LogHelper.LogAction(Constants.ActionLoadPatientStatus, Constants.SearchController, Constants.ActionGetLists, globalUserId);
			LogHelper.LogAction("SearchDataPost End - " + guidKey, "Search", "SearchDataPost End", globalUserId);
            return View(Constants.ActionSearch, modelSearch);
        }

        /// <summary>
        /// Searches the data.
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchData()
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SearchData Start - " + guidKey, "Search", "SearchData Start", globalUserId);
			var modelSearch = new SearchViewModel
            {
                ViewModelPatientPrescription = new PatientPrescriptionViewModel(),
                ViewModelPatientInfo = new PatientInfoViewModel(),
                ViewModelPatientAnalysisMedicalStaff = new PatientAnalysisMedicalStaffViewModel(),
                IsFromOrder = Convert.ToString(Session[Constants.SessionRedirectFrom], CultureInfo.CurrentCulture),
                SearchParam = Convert.ToString(Session[Constants.SessionSearchParam], CultureInfo.CurrentCulture)
            };
            ClearPatientSession();
            modelSearch.ViewModelPatientInfo.FileCount = ConfigurationManager.AppSettings["FileCount"].ToString(CultureInfo.InvariantCulture);
            modelSearch.ViewModelPatientInfo.FileCountErrorMessage = string.Format(CultureInfo.InvariantCulture, Resource.Common.resFileCountErrorMessage, ConfigurationManager.AppSettings["FileCount"].ToString(CultureInfo.InvariantCulture));
            Session[Constants.SessionRedirectFrom] = null;
            ControllerHelper.ActionName = Constants.ActionNameSearchCustomer;
            ControllerHelper.ActionParam = new[] { globalUserId };
            var customerResponse = ControllerHelper.GetMethodServiceRequest<List<DropDownDataBusinessModel>>();
            ViewBag.Customer = new SelectList(customerResponse, Constants.Value, Constants.DisplayText);
            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            var patientTypeListId = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
            var patientStatusListId = Convert.ToInt64(SCAEnums.ListType.PatientStatus, CultureInfo.InvariantCulture);
            var languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);

            var listIds = new[] { patientTypeListId, patientStatusListId };
            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(listIds), Convert.ToString(languageId, CultureInfo.InvariantCulture) };

            var patientData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();

            var patientType = patientData.Where(c => c.ListTypeId.Equals(3));
            var pateintStatus = patientData.Where(c => c.ListTypeId.Equals(4));

            ViewBag.PatientType = new SelectList(patientType, Constants.ListId, Constants.DisplayText);
            LogHelper.LogAction(Constants.ActionLoadPatientTypes, Constants.SearchController, Constants.ActionGetLists, globalUserId);

            var customerStatusListId = Convert.ToInt64(SCAEnums.ListType.CustomerStatus, CultureInfo.InvariantCulture);
            var carehomeStatusListId = Convert.ToInt64(SCAEnums.ListType.CareHomeStatus, CultureInfo.InvariantCulture);

            ControllerHelper.ActionName = Constants.ActionNameGetStatusDropDown;

            var statusIds = new[] { patientStatusListId, customerStatusListId, carehomeStatusListId };

            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(statusIds), globalUserId };
            var patientStatusData = ControllerHelper.GetMethodServiceRequestForList<List<SearchBusinessModel>>();

            ViewBag.PateintStatus = new SelectList(patientStatusData, Constants.StatusDropDownvalue, Constants.StatusDropDownText);
            LogHelper.LogAction(Constants.ActionLoadPatientStatus, Constants.SearchController, Constants.ActionGetLists, globalUserId);

            modelSearch.IsCalledFromSearch = Convert.ToString(Session[Constants.SessionIsCalledFromSearch], CultureInfo.CurrentCulture);
            modelSearch.IsCalledFromCareHome = Convert.ToString(Session[Constants.SessionIsCalledFromCareHome], CultureInfo.CurrentCulture);
            modelSearch.OrderNubmer = Convert.ToString(Session[Constants.SessionSearchOrderNumber], CultureInfo.CurrentCulture);
            modelSearch.PatientType = Convert.ToString(Session[Constants.SessionSearchPatientType], CultureInfo.CurrentCulture);
            modelSearch.SearchId = Convert.ToString(Session[Constants.SessionSearchId], CultureInfo.CurrentCulture);
            modelSearch.SearchStatus = Convert.ToString(Session[Constants.SessionSearchStatus], CultureInfo.CurrentCulture);
            modelSearch.Customer = Convert.ToString(Session[Constants.SessionSearchCustomer], CultureInfo.CurrentCulture);
            modelSearch.TypeToSearch = Convert.ToString(Session[Constants.SessionTypeToSearch], CultureInfo.CurrentCulture);
			LogHelper.LogAction("SearchData End - " + guidKey, "Search", "SearchData End", globalUserId);
			return View(Constants.ActionSearch, modelSearch);
        }


        /// <summary>
        /// Clears the patient session.
        /// </summary>
        private void ClearPatientSession()
        {
            if (Session[Constants.SessionRedirectFrom] == null)
            {
                Session[Constants.SessionSearchParam] = null;
            }

            Session[Constants.SessionPatientId] = null;
            Session[Constants.SessionCustomerId] = null;
            Session[Constants.SessionCustomerName] = null;
            Session[Constants.SessionPatientName] = null;
            Session[Constants.SessionpatientDisplayName] = null;
            Session[Constants.SessionCustomerDisplayName] = null;
            Session[Constants.SessionCareHomeDisplayName] = null;
            Session[Constants.SessionCareHomeSAPId] = null;
            Session[Constants.SessionCareHomeName] = null;
            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            Session[Constants.SessionIsOrderActived] = null;
            Session[Constants.SessionCareHomeId] = null;
        }

        /// <summary>
        /// Json request to get the google search results
        /// </summary>
        /// <param name="term">input text to search</param>
        /// <returns>
        /// The Patient details for the search results
        /// </returns>
        public JsonResult DisplaySearchResult(string term)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("DisplaySearchResultGet Start - " + guidKey, "Search", "DisplaySearchResultGet Start", globalUserId);
			if (!string.IsNullOrEmpty(term))
            {
                term = term.Replace('/', '$');
            }

            ControllerHelper.ActionName = Constants.ActionNameGetSearchResult;
            ControllerHelper.ActionParam = new[] { term, globalUserId };

            var searchResponse = ControllerHelper.GetMethodServiceRequest<List<SearchBusinessModel>>();
            Session[Constants.SessionRedirectEvent] = Convert.ToInt64(SCAEnums.ClickEvent.OnCellSelect, CultureInfo.InvariantCulture);
			LogHelper.LogAction("DisplaySearchResultGet End - " + guidKey, "Search", "DisplaySearchResultGet End", globalUserId);
			return this.Json(searchResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// JSON request to get the search results to bind to the patient grid
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="sidx">The sidx.</param>
        /// <param name="sord">The sort.</param>
        /// <param name="customerId">List of Customers</param>
        /// <param name="patientStatus">List of Patient Status</param>
        /// <param name="patientType">List of Patient types</param>
        /// <param name="orderNo">Order Id to search</param>
        /// <param name="searchID">The search identifier.</param>
        /// <param name="searchText">Search text</param>
        /// <returns>
        /// The Patient details for the search results
        /// </returns>
        public ActionResult DisplayPatientDetails(int page, int rows, string sidx, string sord, string customerId, string patientStatus, string patientType, string orderNo, string searchID, string searchText)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("DisplayPatientDetails Start - " + guidKey, "Search", "DisplayPatientDetails Start", globalUserId);
            long totalRecords = 0;
            var totalPages = 0;

            // Generate Search Param 
            Session[Constants.SessionSearchParam] = string.Format(CultureInfo.CurrentCulture, "{0};{1};{2};{3};{4};{5}", customerId, patientStatus, patientType, orderNo, searchID, searchText);

            if ((string.IsNullOrEmpty(customerId) || customerId == "NULL")
                && (string.IsNullOrEmpty(patientStatus) || patientStatus == "-1" || patientStatus == "NULL")
                && (string.IsNullOrEmpty(patientType) || patientType == "-1")
                && (string.IsNullOrEmpty(orderNo) || orderNo == "NULL")
                && (string.IsNullOrEmpty(searchID) || searchID == "NULL")
                && (string.IsNullOrEmpty(searchText) || searchText == "NULL"))
            {
                LogHelper.LogAction("DisplayPatientDetails End - " + guidKey, "Search", "DisplayPatientDetails End", globalUserId);
                return null;
            }

            WebServiceHelper serviceHelper = new WebServiceHelper(Constants.ActionNameGetGoogleSearchResults);
            var userId = Convert.ToString(serviceHelper.CurrentUser.UserId, CultureInfo.InvariantCulture);

            SearchCriteriaBusinessModel objSearchCriteria = new SearchCriteriaBusinessModel() 
            {
                SearchText = searchText,
                SearchID = searchID,
                CustomerId = customerId,
                PatientStatus = patientStatus,
                PatientType = patientType,
                OrderID = orderNo,
                SortColumn = sidx,
                SortOrder = sord,
                PageNo = Convert.ToString(page, CultureInfo.InvariantCulture),
                Rows = Convert.ToString(rows, CultureInfo.InvariantCulture),
                UserID = userId
            };

            var patientResponse = serviceHelper.PostMethodServiceRequest<SearchCriteriaBusinessModel, List<SearchBusinessModel>>(objSearchCriteria);

            if (patientResponse != null && patientResponse.Count > 0)
            {
                totalRecords = patientResponse[0].TotalRecords;
                totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
            }

            var patientResponseData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = patientResponse,
            };
            LogHelper.LogAction("DisplayPatientDetails End - " + guidKey, Constants.SearchController, "DisplayPatientDetails End", globalUserId);
            return this.Json(patientResponseData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Displays the product details.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DisplayProductDetails(string searchText)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("DisplayProductDetails Start - " + guidKey, "Search", "DisplayPatientDetails Start", globalUserId);
			var customerId = default(long);
            if (Session[Constants.SessionCustomerId] != null)
            {
                customerId = Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.CurrentCulture);
            }
            ProductBusinessModel productBusinessModel = new ProductBusinessModel();
            productBusinessModel.SearchText = searchText;
            productBusinessModel.CustomerId = customerId;

            ControllerHelper.ActionName = Constants.ActionNameGetProductDetails;

            var patientResponse = ControllerHelper.PostMethodServiceRequest<ProductBusinessModel, List<ProductBusinessModel>>(productBusinessModel);
            LogHelper.LogAction(Constants.ActionLoadProductDetails, Constants.SearchController, Constants.ActionSearch, globalUserId);
			LogHelper.LogAction("DisplayProductDetails End - " + guidKey, "Search", "DisplayPatientDetails End", globalUserId);
            return this.Json(patientResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Displays the sample product details.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns></returns>
        public JsonResult DisplaySampleProductDetails(string searchText)
        {
            var customerId = "";
            SampleProductBusinessModel sampleProductBusinessModel = new SampleProductBusinessModel();
            if (Convert.ToBoolean(Session[Constants.SessionSampleFlag], CultureInfo.InvariantCulture)) //---Checking wether sample order is called from View/Edit Patient
            {
                if (Session[Constants.SessionCustomerId] != null)
                {
                    customerId = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
                    sampleProductBusinessModel.CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
                }
            }
            sampleProductBusinessModel.SearchText = searchText;
            ControllerHelper.ActionName = Constants.ActionNameGetSampleProductDetails;            
            var patientResponse = ControllerHelper.PostMethodServiceRequest<SampleProductBusinessModel, List<SampleProductBusinessModel>>(sampleProductBusinessModel);            
            LogHelper.LogAction(Constants.ActionLoadProductDetails, Constants.SearchController, Constants.ActionSearch, globalUserId);

            return this.Json(patientResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Displays the search result product.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <returns></returns>
        public JsonResult DisplaySearchResultProduct(string term)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("DisplaySearchResultProduct Start - " + guidKey, "Search", "DisplaySearchResultProduct Start", globalUserId);
			ControllerHelper.ActionName = Constants.ActionNameGetProductSearchDetails;
            ControllerHelper.ActionParam = new string[] { term };
            var searchResponse = ControllerHelper.GetMethodServiceRequest<List<ProductBusinessModel>>();
            LogHelper.LogAction(Constants.ActionLoadProductSearchDetails, Constants.SearchController, Constants.ActionSearch, globalUserId);
			LogHelper.LogAction("DisplaySearchResultProduct End - " + guidKey, "Search", "DisplaySearchResultProduct End", globalUserId);
            return this.Json(searchResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the holiday list.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetHolidayList(string fromDate, string toDate)
        {
            fromDate = fromDate.Replace('/', '-');
            toDate = toDate.Replace('/', '-');
            ControllerHelper.ActionName = Constants.ActionNameGetHolidayList;
            ControllerHelper.ActionParam = new[] { fromDate, toDate };
            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<HolidayBusinessModel>>();
            LogHelper.LogAction(Constants.ActionLoadCustomers, Constants.PatientController, Constants.ActionNameGetMedStaffAnalysisInfo, globalUserId);
            return Json(responseData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the type of the customer patient.
        /// </summary>
        /// <param name="Customerid">The customerid.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCustomerPatientType(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetCustomerPatientType Start - " + guidKey, "Search", "GetCustomerPatientType Start", globalUserId);
			bool bFlag = false;
            LogHelper.LogAction("Load Customer patient type for " + customerId, Constants.SearchController, Constants.ActionGetCustomerPatientType, globalUserId);
            ControllerHelper.ActionName = Constants.ActionGetCustomerPatientType;
            ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), this.globalUserId };
            var responsePatientType = ControllerHelper.GetMethodServiceRequestForList<List<PatientBusinessModel>>();
            if (responsePatientType == null)
            {
				LogHelper.LogAction("GetCustomerPatientType End - " + guidKey, "Search", "GetCustomerPatientType End", globalUserId);
				return this.Json(bFlag, JsonRequestBehavior.AllowGet);
            }

			LogHelper.LogAction("GetCustomerPatientType End - " + guidKey, "Search", "GetCustomerPatientType End", globalUserId);
            return this.Json(responsePatientType, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Sets the session for search.
        /// </summary>
        /// <param name="searchViewModel">The search view model.</param>
        /// <returns></returns>
        public ActionResult SetSessionForSearch(SearchViewModel searchViewModel)
        {
            Session[Constants.SessionSearchCustomer] = searchViewModel.Customer;
            Session[Constants.SessionSearchStatus] = searchViewModel.SearchStatus;
            Session[Constants.SessionSearchPatientType] = searchViewModel.PatientType;
            Session[Constants.SessionSearchOrderNumber] = searchViewModel.OrderNubmer;
            Session[Constants.SessionSearchId] = searchViewModel.SearchId;
            Session[Constants.SessionTypeToSearch] = searchViewModel.TypeToSearch;
            Session[Constants.SessionIsCalledFromSearch] = true;
            return this.Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Clears the search parameter from session.
        /// </summary>
        public void ClearSearchParamFromSession()
        {
            Session[Constants.SessionSearchParam] = string.Empty;
            Session[Constants.SessionSearchCustomer] = string.Empty;
            Session[Constants.SessionSearchStatus] = string.Empty;
            Session[Constants.SessionSearchPatientType] = string.Empty;
            Session[Constants.SessionSearchOrderNumber] = string.Empty;
            Session[Constants.SessionSearchId] = string.Empty;
            Session[Constants.SessionTypeToSearch] = string.Empty;
            Session[Constants.SessionIsCalledFromSearch] = false;
            Session[Constants.SessionIsCalledFromCareHome] = false;
        }
    }
}