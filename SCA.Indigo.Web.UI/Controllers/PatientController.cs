﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="PatientController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Linq;
	using System.Web.Mvc;

	using SCA.Indigo.Business.BusinessModels;
	using SCA.Indigo.Common;
	using SCA.Indigo.Common.Enums;
	using SCA.Indigo.Web.UI.Helper;
	using SCA.Indigo.Web.UI.ViewModel;
	using SCA.Indigo.Model;
	using System.Configuration;

	/// <summary>
	/// Class PatientController.
	/// </summary>
	[Authorize]
	public class PatientController : Controller
	{
		// Global varriable        
		/// <summary>
		/// The _user password expired
		/// </summary>
		private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.InvariantCulture);
		/// <summary>
		/// The _obj patient view model
		/// </summary>
		private readonly SearchViewModel globalObjPatientViewModel = new SearchViewModel();
		/// <summary>
		/// The _obj selectedvalue
		/// </summary>
		private readonly SelectedValue globalObjSelectedvalue = new SelectedValue();
		/// <summary>
		/// The _obj patient information view model
		/// </summary>
		private PatientInfoViewModel globalObjPatientInfoViewModel = new PatientInfoViewModel();
		/// <summary>
		/// The user identifier
		/// </summary>
		private string globalUserId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);
		/// <summary>
		/// The _user login name
		/// </summary>
		private readonly string userLoginName = Convert.ToString(ControllerHelper.CurrentUser.UserName, CultureInfo.CurrentCulture);
		/// <summary>
		/// The _user type
		/// </summary>
		private readonly string userType = Convert.ToString(ControllerHelper.CurrentUser.UserType, CultureInfo.CurrentCulture);
		/// <summary>
		/// The _ role name
		/// </summary>
		private readonly string roleName = Convert.ToString(ControllerHelper.CurrentUser.RoleName, CultureInfo.CurrentCulture);
		/// <summary>
		/// The _ role identifier
		/// </summary>
		private readonly long roleId = Convert.ToInt64(ControllerHelper.CurrentUser.RoleId, CultureInfo.CurrentCulture);
		/// <summary>
		/// The date format
		/// </summary>
		private const string DateFormat = "dd/MM/yyyy";

		private bool IsClinicalContactWithLinkage
		{
			get
			{
				if (Session[Constants.SessionCustomerParameters] != null)
				{
					var customerParameterValue = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
					return customerParameterValue.FirstOrDefault().IsClinicalContactWithLinkage;
				}
				return false; //This will be only if the session is expired. In ideal scenario, this will be never executed. But included the code to avoid any runtime exception.
			}
		}


		/// <summary>
		/// The customer helper
		/// </summary>
		CustomerHelper customerHelper = new CustomerHelper();
		/// <summary>
		/// The patient helper
		/// </summary>
		PatientHelper patientHelper = new PatientHelper();

		/// <summary>
		/// Get function for Patient Info
		/// </summary>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult PatientInfo()
		{
			return View(Constants.ViewPatientInfo);
		}

		/// <summary>
		/// Loads the drop downs.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult LoadDropDowns()
		{
			var guidKey = Guid.NewGuid();
			FillDropDownData(globalObjSelectedvalue);
			globalObjPatientInfoViewModel.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
			return Json(globalObjPatientInfoViewModel, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// This method will load patient information based on provided PatientId
		/// </summary>
		/// <param name="patientId">Patient Id to load the patient details</param>
		/// <param name="customerId">Customer id of the patient</param>
		/// <param name="PatientName">Name of the patient</param>
		/// <param name="CustomerName">Customer name of the patient</param>
		/// <param name="SAPCustomerNumber">Sap Customer Number</param>
		/// <param name="SAPPatientNumber">Sap Patient Number</param>
		/// <returns>
		/// Full details of the Patient along with patient's Medical staff and Prescription
		/// </returns>
		public ActionResult LoadPatientDetails(string patientId, string customerId, string PatientName, string CustomerName, string SAPCustomerNumber, string SAPPatientNumber)
		{
			var guidKey = Guid.NewGuid();

			Session[Constants.SessionIsLoadPrescription] = true;
			Session[Constants.SessionPatientId] = patientId;
			Session[Constants.SessionCustomerId] = customerId;
			Session[Constants.SessionPreviousCustomerId] = customerId;
			globalObjPatientViewModel.ViewModelPatientInfo = new PatientInfoViewModel();
			globalObjPatientInfoViewModel.PatientId = !string.IsNullOrEmpty(patientId) ? Convert.ToInt64(patientId, CultureInfo.InvariantCulture) : 0;

			FillPatientDetails();

			FillDropDownData(globalObjSelectedvalue);

			globalObjPatientViewModel.ViewModelPatientInfo = InitializePatientInfoViewModel(globalObjPatientInfoViewModel);
			Session[Constants.SessionPatientStatus] = globalObjSelectedvalue.PatientStatusSelectedValue;
			Session[Constants.SessionCopyPatientStatus] = globalObjSelectedvalue.PatientStatusSelectedValue;

			// Load PatientTypeMatrix
			var patientType = globalObjSelectedvalue.PatientTypeSelectedValue;
			LoadPatientTypeMatrix(Convert.ToInt64(globalObjPatientViewModel.ViewModelPatientInfo.CustomerId, CultureInfo.InvariantCulture), Convert.ToInt64(patientType, CultureInfo.InvariantCulture));

			if (Session[Constants.SessionisRedirectFromAuthorisation] != null && Convert.ToInt32(Session[Constants.SessionisRedirectFromAuthorisation], CultureInfo.InvariantCulture) == 1)
			{
				var patientDisplayName = patientId + " " + PatientName;
				var customerDisplayName = customerId + " " + CustomerName;
				SAPCustomerNumber = CommonHelper.TrimLeadingZeros(SAPCustomerNumber);
				customerDisplayName = !string.IsNullOrEmpty(SAPCustomerNumber) ? customerDisplayName.Contains(SAPCustomerNumber) ? customerDisplayName : customerDisplayName + "(" + SAPCustomerNumber + ")" : customerDisplayName;

				if (!string.IsNullOrEmpty(SAPPatientNumber))
				{
					patientDisplayName += " (" + SAPPatientNumber + ")";
				}

				Session[Constants.SessionpatientDisplayName] = Convert.ToString(patientDisplayName, CultureInfo.InvariantCulture);
				Session[Constants.SessionCustomerDisplayName] = Convert.ToString(customerDisplayName, CultureInfo.InvariantCulture);
			}
			if (Session[Constants.SessionCareHomeId] != null)
			{
				SetIsCarehomePatientProperty(globalObjPatientInfoViewModel);
			}
			return Json(globalObjPatientInfoViewModel, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Initializes the patient information view model.
		/// </summary>
		/// <param name="inforViewModel">The infor view model.</param>
		/// <returns></returns>
		protected PatientInfoViewModel InitializePatientInfoViewModel(PatientInfoViewModel inforViewModel)
		{
			if (inforViewModel == null)
			{
				inforViewModel.SAPPatientNumber = string.IsNullOrEmpty(inforViewModel.SAPPatientNumber) ? string.Empty : inforViewModel.SAPPatientNumber;
				inforViewModel.CreatedBy = string.Empty;
				inforViewModel.CreatedDate = DateTime.Today.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
			}

			return inforViewModel;
		}

		/// <summary>
		/// This method will Add patient information based
		/// </summary>
		/// <param name="patientViewModel">The patient view model.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpPost]
		public ActionResult AddDetails(PatientInfoViewModel patientViewModel)
		{
			globalObjPatientViewModel.ViewModelPatientInfo = globalObjPatientInfoViewModel;
			globalObjPatientViewModel.ViewModelPatientInfo.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
			if (Session[Constants.SessionCareHomeId] != null)
			{
				SetIsCarehomePatientProperty(globalObjPatientViewModel.ViewModelPatientInfo);
			}
			return View(Constants.ActionSearch, globalObjPatientViewModel);
		}

		/// <summary>
		/// Gets the care home by identifier.
		/// </summary>
		/// <param name="careHomeId">The care home identifier.</param>
		/// <returns></returns>

		[HttpGet]
		public ActionResult GetCareHomeById(string careHomeId)
		{
			if (!string.IsNullOrEmpty(careHomeId))
			{
				CareHomeViewModel objCareHomeViewModel = new CareHomeHelper().GetCareHome(careHomeId);
				globalObjPatientInfoViewModel.CareHomeId = Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture);
				globalObjPatientInfoViewModel.CustomerId = objCareHomeViewModel.CustomerId;
				globalObjPatientInfoViewModel.Address1 = objCareHomeViewModel.Address1;
				globalObjPatientInfoViewModel.Address2 = objCareHomeViewModel.Address2;
				globalObjPatientInfoViewModel.CareHomeName = objCareHomeViewModel.CareHomeName;
				globalObjPatientInfoViewModel.Country = objCareHomeViewModel.Country;
				globalObjPatientInfoViewModel.County = objCareHomeViewModel.County;
				globalObjPatientInfoViewModel.EmailAddress = objCareHomeViewModel.EmailAddress;
				globalObjPatientInfoViewModel.HouseName = objCareHomeViewModel.CareHomeName;
				globalObjPatientInfoViewModel.HouseNumber = objCareHomeViewModel.HouseNumber;
				globalObjPatientInfoViewModel.MobileNumber = objCareHomeViewModel.HouseNumber;
				globalObjPatientInfoViewModel.PostCode = objCareHomeViewModel.PostCode;
				globalObjPatientInfoViewModel.TelephoneNumber = objCareHomeViewModel.PhoneNo;
				globalObjPatientInfoViewModel.TownOrCity = objCareHomeViewModel.TownOrCity;
				globalObjPatientInfoViewModel.Round = objCareHomeViewModel.Round;
				globalObjPatientInfoViewModel.RoundId = objCareHomeViewModel.RoundId;
				globalObjPatientInfoViewModel.DeliveryFrequency = objCareHomeViewModel.DeliveryFrequency;
				globalObjPatientInfoViewModel.NextDeliveryDate = objCareHomeViewModel.NextDeliveryDate;
				globalObjPatientInfoViewModel.CommunicationFormat = objCareHomeViewModel.CommunicationFormat;
				globalObjPatientInfoViewModel.CareHomeStatus = objCareHomeViewModel.CareHomeStatus;
				return Json(globalObjPatientInfoViewModel, JsonRequestBehavior.AllowGet);
			}
			return null;
		}


		/// <summary>
		/// Service call to retrieve patient details
		/// </summary>
		private void FillPatientDetails()
		{
			var objPatient = new PatientBusinessModel { PatientId = globalObjPatientInfoViewModel.PatientId };
			var serviceHelper = new WebServiceHelper(Constants.ActionNameRetrievePatientDetails);
			var responseData = serviceHelper.PostMethodServiceRequest<PatientBusinessModel>(objPatient);
			BindPatientInfoViewModel(responseData);
			Session[Constants.SessionPatientStatus] = objPatient.PatientStatusId;
		}

		/// <summary>
		/// This method will set Patient information  from PatientBusinessModel to PatientInfoViewModel
		/// </summary>
		/// <param name="objPatientBusinessModel">It as instance of PatientBusinessModel</param>
		private void BindPatientInfoViewModel(PatientBusinessModel objPatientBusinessModel)
		{
			Session[Constants.SessionRoundId] = objPatientBusinessModel.RoundId;
			var customerID = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);

			// Commented for performance
			//SetCustomerSession(customerID);

			var loadCustomerParameters = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
			var postCodeMatrix = (List<PostCodeMatrixBusinessModel>)Session[Constants.SessionPostcodeMatrix];

			if (postCodeMatrix != null && postCodeMatrix.Any())
			{
				var firstRecord = postCodeMatrix.First();
				Session[Constants.SessionDay1] = firstRecord.Monday;
				Session[Constants.SessionDay2] = firstRecord.Tuesday;
				Session[Constants.SessionDay3] = firstRecord.Wednesday;
				Session[Constants.SessionDay4] = firstRecord.Thursday;
				Session[Constants.SessionDay5] = firstRecord.Friday;

				if (loadCustomerParameters.Any())
				{
					var customerParameter = loadCustomerParameters.First();
					customerParameter.Day1 = (bool)firstRecord.Monday;
					customerParameter.Day2 = (bool)firstRecord.Tuesday;
					customerParameter.Day3 = (bool)firstRecord.Wednesday;
					customerParameter.Day4 = (bool)firstRecord.Thursday;
					customerParameter.Day5 = (bool)firstRecord.Friday;
				}
			}

			if (objPatientBusinessModel != null)
			{
				//As per Roxy, this is not required as of now.
				objPatientBusinessModel.MarketingPreferences = null;
				objPatientBusinessModel.IsDataProtected = false;

				globalObjPatientInfoViewModel = new PatientInfoViewModel()
				{
					PatientId = objPatientBusinessModel.PatientId,
					SAPPatientNumber = objPatientBusinessModel.SAPPatientNumber,
					DeliveryDate = !string.IsNullOrEmpty(objPatientBusinessModel.DeliveryDate) ? Convert.ToDateTime(objPatientBusinessModel.DeliveryDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "",
					NextDeliveryDate = !string.IsNullOrEmpty(objPatientBusinessModel.NextDeliveryDate) ? Convert.ToDateTime(objPatientBusinessModel.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "",
					AssessmentDate = !string.IsNullOrEmpty(objPatientBusinessModel.AssessmentDate) ? Convert.ToDateTime(objPatientBusinessModel.AssessmentDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "",
					NextAssessmentDate = !string.IsNullOrEmpty(objPatientBusinessModel.NextAssessmentDate) ? Convert.ToDateTime(objPatientBusinessModel.NextAssessmentDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "",
					RoundId = objPatientBusinessModel.RoundId,
					Round = objPatientBusinessModel.Round,
					FirstName = objPatientBusinessModel.FirstName,
					LastName = objPatientBusinessModel.LastName,
					DateOfBirth = Convert.ToDateTime(objPatientBusinessModel.DateOfBirth, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture),
					CreatedDate = Convert.ToDateTime(objPatientBusinessModel.CreatedDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture),
					CreatedBy = objPatientBusinessModel.CreatedBy,
					NHSId = objPatientBusinessModel.NHSId,
					LocalId = objPatientBusinessModel.LocalId,
					PhoneNo = objPatientBusinessModel.PhoneNo,
					MobileNo = objPatientBusinessModel.MobileNo,
					EmailAddress = objPatientBusinessModel.EmailAddress,
					DeliveryFrequency = objPatientBusinessModel.DeliveryFrequency,
					CareHomeId = objPatientBusinessModel.CareHomeId,
					CareHomeName = objPatientBusinessModel.CareHomeName,
					HouseNumber = objPatientBusinessModel.HouseNumber,
					HouseName = objPatientBusinessModel.HouseName,
					Address1 = objPatientBusinessModel.Address1,
					Address2 = objPatientBusinessModel.Address2,
					TownOrCity = objPatientBusinessModel.City,
					PostCode = objPatientBusinessModel.PostCode,
					County = objPatientBusinessModel.County,
					Country = objPatientBusinessModel.Country,
					ADP1 = objPatientBusinessModel.ADP1,
					ADP2 = objPatientBusinessModel.ADP2,
					CareHomeNextDeliveryDate = !string.IsNullOrEmpty(objPatientBusinessModel.CareHomeNextDeliveryDate) ? Convert.ToDateTime(objPatientBusinessModel.CareHomeNextDeliveryDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "",
					SAPCustomerNumber = objPatientBusinessModel.SAPCustomerNumber,
					CustomerParameters = loadCustomerParameters,
					CareHomeStatus = objPatientBusinessModel.CareHomeStatus,
					//Set Correspondence Address 
					CorrespondenceSameAsDelivery = objPatientBusinessModel.CorrespondenceSameAsDelivery,
					CorrespondenceCareHomeName = objPatientBusinessModel.CorrespondenceCareHomeName,
					CorrespondenceAddress1 = objPatientBusinessModel.CorrespondenceAddress1,
					CorrespondenceAddress2 = objPatientBusinessModel.CorrespondenceAddress2,
					CorrespondenceTownOrCity = objPatientBusinessModel.CorrespondenceTownOrCity,
					CorrespondenceCounty = objPatientBusinessModel.CorrespondenceCounty,
					CorrespondencePostCode = objPatientBusinessModel.CorrespondencePostCode,
					//Set Preferences
					CommunicationPreferences = objPatientBusinessModel.CommunicationPreferences,
					MarketingPreferences = objPatientBusinessModel.MarketingPreferences,
					IsDataProtected = Convert.ToBoolean(objPatientBusinessModel.IsDataProtected),
					CareHomeSAPId = objPatientBusinessModel.CareHomeSAPId,
					RemovedStoppedDateTime = !string.IsNullOrEmpty(objPatientBusinessModel.RemovedStoppedDateTime) ? Convert.ToDateTime(objPatientBusinessModel.RemovedStoppedDateTime, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : ""
				};

				// Set Patient NDD in session
				Session[Constants.SessionPatientNDD] = globalObjPatientInfoViewModel.NextDeliveryDate;

				// Set Dropdown selected Value
				globalObjSelectedvalue.PatientTypeSelectedValue = objPatientBusinessModel.PatientTypeId;
				globalObjSelectedvalue.PatientStatusSelectedValue = objPatientBusinessModel.PatientStatusId;
				globalObjSelectedvalue.GenderSelectedValue = objPatientBusinessModel.Gender;
				globalObjSelectedvalue.ReasonCodeSelectedValue = objPatientBusinessModel.ReasonCodeId;
				globalObjSelectedvalue.TitleSelectedValue = objPatientBusinessModel.TitleId;
				globalObjSelectedvalue.CommunicationFormatSelectedValue = objPatientBusinessModel.CommunicationFormat;
				globalObjSelectedvalue.CountySelectedValue = objPatientBusinessModel.County;
				globalObjSelectedvalue.CustomerSelectedValue = Session[Constants.SessionCustomerId];

				if (objPatientBusinessModel.CareHomeId != 0 && objPatientBusinessModel.CareHomeId != null)
				{
					var carehomeDisplayName = objPatientBusinessModel.CareHomeId + " " + objPatientBusinessModel.CareHomeName + " " + objPatientBusinessModel.PostCode;
					if (!string.IsNullOrEmpty(objPatientBusinessModel.CareHomeSAPId))
					{
						carehomeDisplayName += " (" + objPatientBusinessModel.CareHomeSAPId + ")";
					}

					var oldSessionCareHomeId = Session[Constants.SessionCareHomeId] != null ? Convert.ToString(Session[Constants.SessionCareHomeId], CultureInfo.CurrentCulture).Trim() : string.Empty;
					if (Session[Constants.SessionCareHomeId] == null)
					{
						Session[Constants.SessionDoNotShowForCarehome] = "false";
					}
					else if (oldSessionCareHomeId != Convert.ToString(objPatientBusinessModel.CareHomeId, CultureInfo.CurrentCulture))
					{
						Session[Constants.SessionDoNotShowForCarehome] = "false";
					}

					Session[Constants.SessionCareHomeDisplayName] = Convert.ToString(carehomeDisplayName, CultureInfo.CurrentCulture);
					Session[Constants.SessionCareHomeId] = objPatientBusinessModel.CareHomeId;
					Session[Constants.SessionCareHomeSAPId] = objPatientBusinessModel.CareHomeSAPId;
					Session[Constants.SessionCareHomeName] = objPatientBusinessModel.CareHomeName;
				}
			}
		}

		/// <summary>
		/// Method to fill dropdown
		/// </summary>
		/// <param name="objSelectedvalue">The object selected value.</param>
		private void FillDropDownData(SelectedValue objSelectedvalue)
		{
			// Fill Patient Type and Patient Status
			var patientStatusListTypeId = Convert.ToInt64(SCAEnums.ListType.PatientStatus, CultureInfo.InvariantCulture);
			var patientTypeListTypeId = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
			var reasonCode = Convert.ToInt64(SCAEnums.ListType.ReasonCode, CultureInfo.InvariantCulture);
			var communicationFormat = Convert.ToInt64(SCAEnums.ListType.CommunicationFormat, CultureInfo.InvariantCulture);
			var titleId = Convert.ToInt64(SCAEnums.ListType.Title, CultureInfo.InvariantCulture);
			var actionParam = new[]
            {
                patientTypeListTypeId + "," + patientStatusListTypeId + "," + reasonCode + "," + communicationFormat +
                "," + titleId,
                string.Empty + Convert.ToInt32(SCAEnums.ListType.LanguageId,CultureInfo.InvariantCulture)
            };

			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetListType, actionParam);

			var patientType = new List<ListTypeBusinessModel>();
			var patientStatus = new List<ListTypeBusinessModel>();
			var varReasonCode = new List<ListTypeBusinessModel>();
			var varCommunicationFormat = new List<ListTypeBusinessModel>();
			var varTitle = new List<ListTypeBusinessModel>();

			var responseData = serviceHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
			if (responseData != null)
			{
				patientType = responseData.Where(a => a.ListTypeId == patientTypeListTypeId).ToList();
				patientStatus = responseData.Where(a => a.ListTypeId == patientStatusListTypeId).ToList();
				varReasonCode = responseData.Where(a => a.ListTypeId == reasonCode).ToList();
				varCommunicationFormat = responseData.Where(a => a.ListTypeId == communicationFormat).ToList();
				varTitle = responseData.Where(a => a.ListTypeId == titleId).ToList();
			}
			globalObjPatientInfoViewModel.ddlPatientType = new SelectList(patientType, Constants.ListId, Constants.DisplayText, objSelectedvalue.PatientTypeSelectedValue);

			globalObjPatientInfoViewModel.ddlPatientStatus = new SelectList(patientStatus, Constants.ListId, Constants.DisplayText, objSelectedvalue.PatientStatusSelectedValue);

			globalObjPatientInfoViewModel.ddlReasonCode = new SelectList(varReasonCode, Constants.ListId, Constants.DisplayText, objSelectedvalue.ReasonCodeSelectedValue);

			globalObjPatientInfoViewModel.ddlCommunicationFormat = new SelectList(varCommunicationFormat, Constants.ListId, Constants.DisplayText, objSelectedvalue.CommunicationFormatSelectedValue);

			globalObjPatientInfoViewModel.ddlTitle = new SelectList(varTitle, Constants.ListId, Constants.DisplayText, objSelectedvalue.TitleSelectedValue);

			// Bind County dropdown
			var countyList = new List<CountyBusinessModel>();
			serviceHelper = new WebServiceHelper(Constants.ActionNameGetCountyList);
			countyList = serviceHelper.GetMethodServiceRequestForList<List<CountyBusinessModel>>();
			if (countyList != null)
			{
				countyList = countyList.OrderBy(x => x.CountyText).ToList();
			}
			globalObjPatientInfoViewModel.ddlCounty = new SelectList(countyList, Constants.CountyCode, Constants.CountyText, objSelectedvalue.CountySelectedValue);

			serviceHelper = new WebServiceHelper(Constants.ActionNameSearchCustomer, new[] { globalUserId });
			var customerResponse = serviceHelper.GetMethodServiceRequest<List<DropDownDataBusinessModel>>();

			objSelectedvalue.CustomerSelectedValue = Session[Constants.SessionCustomerId];

			globalObjPatientInfoViewModel.ddlCustomer = new SelectList(customerResponse, Constants.Value, Constants.DisplayText, objSelectedvalue.CustomerSelectedValue);

			var genderId = Convert.ToInt64(SCAEnums.ListType.Gender, CultureInfo.InvariantCulture);

			actionParam = new[]
            {
                Convert.ToString(genderId, CultureInfo.InvariantCulture),
                string.Empty + Convert.ToInt32(SCAEnums.ListType.LanguageId,CultureInfo.InvariantCulture)
            };
			serviceHelper = new WebServiceHelper(Constants.ActionNameGetListType, actionParam);
			var genderResponse = serviceHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
			if (genderResponse != null && genderResponse.Any())
			{
				globalObjPatientInfoViewModel.ddlGender = new SelectList(genderResponse, Constants.ListId, Constants.DisplayText, objSelectedvalue.GenderSelectedValue);
			}
			globalObjPatientInfoViewModel.DropDownSelectedvalue = objSelectedvalue;
		}

		/// <summary>
		/// This method will load care home information based on provided customerId
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="rows">The rows.</param>
		/// <param name="CustomerId">The customer identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult GetCareHomeDetails(int page, int rows, string CustomerId)
		{
			var objCareHomeBusinessModel = new CareHomeBusinessModel();
			objCareHomeBusinessModel.CustomerId = Convert.ToInt64(CustomerId, CultureInfo.InvariantCulture);
			objCareHomeBusinessModel.LoggedInUserId = Guid.Parse(this.globalUserId);

			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetCareHomeDetails);
			var careHomeData = serviceHelper.PostMethodServiceRequest<CareHomeBusinessModel, List<CareHomeBusinessModel>>(objCareHomeBusinessModel);
			if (careHomeData != null)
			{
				careHomeData = careHomeData.OrderBy(x => x.CareHomeName).ToList();
			}
			return Json(careHomeData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get all carehome name 
		/// </summary>
		/// <param name="searchText">The search text.</param>
		/// <param name="CustomerId">The customer identifier.</param>
		/// <returns></returns>
		public JsonResult SearchCarehomeDetails(string searchText, string CustomerId)
		{
			var objCareHomeBusinessModel = new CareHomeBusinessModel();
			objCareHomeBusinessModel.CustomerId = Convert.ToInt64(CustomerId, CultureInfo.InvariantCulture);
			objCareHomeBusinessModel.LoggedInUserId = Guid.Parse(this.globalUserId);

			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetCareHomeDetails);
			var careHomeData = serviceHelper.PostMethodServiceRequest<CareHomeBusinessModel, List<CareHomeBusinessModel>>(objCareHomeBusinessModel);

			if (careHomeData != null)
			{
				if (searchText != "blank")
				{
					careHomeData = careHomeData.Where(q => (q.SapCareHomeNumber != null && q.SapCareHomeNumber.ToLower().Contains(searchText.ToLower())) || (q.CareHomeName != null && q.CareHomeName.ToLower().Contains(searchText.ToLower())))
						.OrderBy(x => x.CareHomeName).ToList();
				}
				else
				{
					careHomeData = careHomeData.OrderBy(x => x.CareHomeName).ToList();
				}
			}
			return Json(careHomeData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get Method for Details page
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult Details(string id)
		{
			if (userPasswordExpired)
			{
				return RedirectToAction("Login", "Account");
			}

			// Unless user is not saving Patient, Session should not be cleared
			Session[Constants.SessionisRedirectFromAuthorisation] = null;
			Session[Constants.SessionOrderSavedFromOneOff] = null;
			Session[Constants.SessionIsLoadPrescription] = false;
			Session[Constants.SessionIsOrderActived] = null;
			var modelPatient = new PatientViewModel
			{
				ViewModelPatientPrescription = new PatientPrescriptionViewModel(),
				ViewModelPatientInfo = new PatientInfoViewModel()
				{
					CreatedBy = userLoginName,
					CreatedDate = System.DateTime.Today.ToString(Constants.DateFormat, CultureInfo.InvariantCulture),
					CareHomeId = !string.IsNullOrEmpty(id) ? Convert.ToInt64(id, CultureInfo.InvariantCulture) : 0
				},

				ViewModelPatientAnalysisMedicalStaff = new PatientAnalysisMedicalStaffViewModel()
			};
			modelPatient.ViewModelPatientInfo.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
			if (Session[Constants.SessionCareHomeId] != null)
			{
				SetIsCarehomePatientProperty(modelPatient.ViewModelPatientInfo);
			}
			return View(Constants.ActionDetails, modelPatient);
		}

		/// <summary>
		/// Post Method for Prescription details page
		/// </summary>
		/// <param name="page">int page</param>
		/// <param name="rows">int rows</param>
		/// <param name="isremoved">int isremoved</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpPost]
		public ActionResult PrescriptionDetails(int page, int rows, int? isremoved)
		{
			var guidKey = Guid.NewGuid();
			var patientId = new long();

			// SessionIsLoadPrescription Check is performed when priscription need to be filled empty while adding new patient
			if (Session[Constants.SessionIsLoadPrescription] != null && Convert.ToBoolean(Session[Constants.SessionIsLoadPrescription], CultureInfo.InvariantCulture))
			{
				patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
			}

			var totalRecords = 0;
			var totalPages = 0;
			if (isremoved == null)
			{
				isremoved = 0;
			}
			Session[Constants.SessionPage] = page;
			Session[Constants.SessionRows] = rows;
			Session[Constants.SessionIsRemoved] = isremoved;

			var griddata = new List<PrescriptionBusinessModel>();

			// var patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
			var objPrescription = new PrescriptionBusinessModel() { Page = page, Rows = rows, IsRemoved = Convert.ToBoolean(isremoved, CultureInfo.InvariantCulture), PatientID = patientId };

			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetPrescriptionProductList);
			var prescriptionData = serviceHelper.PostMethodServiceRequest<PrescriptionBusinessModel, List<PrescriptionBusinessModel>>(objPrescription);
			if (prescriptionData != null)
			{
				griddata = prescriptionData.ToList();
				totalRecords = prescriptionData.Count;
				totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
			}

			var loadPrescriptionData = new
			{
				total = totalPages,
				page = page,
				records = totalRecords,
				rows = griddata,
			};
			return Json(loadPrescriptionData);
		}

		/// <summary>
		/// Load Product Information
		/// </summary>
		/// <param name="selectedCustomer">The selected customer.</param>
		/// <returns>
		/// Json Result
		/// </returns>
		public JsonResult ProductDetails(string selectedCustomer)
		{
			var customerId = "0";
			if (!string.IsNullOrEmpty(selectedCustomer) && selectedCustomer != "-1" && selectedCustomer != "Customer")
			{
				customerId = Convert.ToString(selectedCustomer, CultureInfo.InvariantCulture);
			}

			var serviceHelper = new WebServiceHelper(Constants.ActionGetProductList, new string[] { customerId });
			var productData = serviceHelper.GetMethodServiceRequestForList<List<ProductBusinessModel>>();
			return this.Json(productData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Samples the product details.
		/// </summary>
		/// <returns></returns>
		public JsonResult SampleProductDetails()
		{
			var customerId = "0"; string[] sampleOrderProductType; var sampleOrderCustomerProductType = ""; var sampleOrderTenaProductType = "";
			if (Convert.ToBoolean(Session[Constants.SessionSampleFlag], CultureInfo.InvariantCulture)) //---Checking wether sample order is called from View/Edit Patient
			{
				if (Session[Constants.SessionCustomerId] != null)
				{
					customerId = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
				}
			}
			sampleOrderProductType = Constants.SampleOrderProductType.Split('/');
			sampleOrderCustomerProductType = sampleOrderProductType[1];
			sampleOrderTenaProductType = sampleOrderProductType[0];

			var serviceHelper = new WebServiceHelper(Constants.ActionGetSampleProductList, new string[] { customerId, sampleOrderCustomerProductType, sampleOrderTenaProductType });
			var productData = serviceHelper.GetMethodServiceRequestForList<List<SampleProductBusinessModel>>();
			return this.Json(productData, JsonRequestBehavior.AllowGet);
		}

		#region MedicalsStaff and AnalysisInformation

		/// <summary>
		/// Get Search results on  PatientMedicalStaff , Patient Analysis Info Popup
		/// </summary>
		/// <param name="txtSearch">The text search.</param>
		/// <param name="listId">The list identifier.</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="listTypeId">The list type identifier.</param>
		/// <returns>
		/// Json Result
		/// </returns>
		public JsonResult DisplayMedicalStaffSearchResult(string txtSearch, string listId, string customerId, string listTypeId)
		{
			// Set the Business Model for Service
			MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel = new MedicalStaffAnalysisInfoBusinessModel()
			{
				TextSearch = string.IsNullOrEmpty(txtSearch) ? "-1" : txtSearch,
				ListId = Convert.ToInt64(listId, CultureInfo.InvariantCulture),
				CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture),
				UserId = Guid.Parse(globalUserId),
				IsClinicalContactWithLinkage = this.IsClinicalContactWithLinkage,
				ListTypeId = string.IsNullOrEmpty(listTypeId) ? 0 : Convert.ToInt64(listTypeId, CultureInfo.InvariantCulture)

			};
			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetMedicalStaffSearchResults);
			var searchResponse = serviceHelper.PostMethodServiceRequest<MedicalStaffAnalysisInfoBusinessModel, List<MedicalStaffAnalysisInfoBusinessModel>>(medicalStaffAnalysisInfoBusinessModel);

			return this.Json(searchResponse, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get Particular MedicalStaff/AnalysisInfromation assigned to the customer [Popup]
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="rows">The rows.</param>
		/// <param name="listId">The list identifier.</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="listTypeId">The list type identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpPost]
		public ActionResult MedicalStaffAnalysisInfo(int page, int rows, int? listId, long customerId, string listTypeId)
		{
			var guidKey = Guid.NewGuid();
			if (customerId != null && customerId != -1)
			{
				string userId = globalUserId;
				var serviceHelper = new WebServiceHelper(Constants.ActionNameGetMedStaffAnalysisInfo);
				// Set the Business Model for Service
				var medicalStaffAnalysisInfoBusinessModel = new MedicalStaffAnalysisInfoBusinessModel()
				{
					ListId = Convert.ToInt64(listId, CultureInfo.InvariantCulture),
					CustomerId = customerId,
					UserId = Guid.Parse(userId),
					IsClinicalContactWithLinkage = this.IsClinicalContactWithLinkage,
					ListTypeId = string.IsNullOrEmpty(listTypeId) ? 0 : Convert.ToInt64(listTypeId, CultureInfo.InvariantCulture)
				};

				var responseData = serviceHelper.PostMethodServiceRequest<MedicalStaffAnalysisInfoBusinessModel, List<MedicalStaffAnalysisInfoBusinessModel>>(medicalStaffAnalysisInfoBusinessModel);

				if (responseData != null)
				{
					var medicalStaffAnalysisInfoData = responseData.ToList();
					int totalRecords = responseData.Count;
					var totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
					var medicalStaffAnalysisInfojson = new
					{
						total = totalPages,
						page = page,
						records = totalRecords,
						rows = medicalStaffAnalysisInfoData,
					};
					return Json(medicalStaffAnalysisInfojson);
				}
			}
			return null;
		}

		/// <summary>
		/// Display Medical Staff and Analysis Information for patient
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="rows">The rows.</param>
		/// <param name="gridName">Name of the grid.</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <returns>
		/// ActionResult.
		/// </returns>
		[HttpPost]
		public ActionResult GetMedicalAnalysisInfo(int page, int rows, string gridName, long customerId)
		{
			var guidKey = Guid.NewGuid();
			bool isClinicalContactLinkage = this.IsClinicalContactWithLinkage;
			var patientId = new Int64();
			var medicalAnalysisInfoData = new List<MedicalStaffAnalysisInfoBusinessModel>();
			var totalRecords = 0;
			var totalPages = 0;

			if (customerId > 0)
			{
				if (Session[Constants.SessionIsLoadPrescription] != null && Convert.ToBoolean(Session[Constants.SessionIsLoadPrescription], CultureInfo.InvariantCulture))
				{
					if (Session[Constants.SessionIsCopyDetails] != null && Convert.ToBoolean(Session[Constants.SessionIsCopyDetails], CultureInfo.InvariantCulture) == true)
					{
						patientId = 0;
					}
					else
					{
						patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
					}
				}
				var objMedicalAnalysis = new MedicalStaffAnalysisInfoBusinessModel { PatientId = patientId, GridName = gridName, CustomerId = customerId, UserId = Guid.Parse(globalUserId), IsClinicalContactWithLinkage = isClinicalContactLinkage };
				var serviceHelper = new WebServiceHelper(Constants.ActionNameGetPatientStaffDetails);
				var responseMedical = serviceHelper.PostMethodServiceRequest<MedicalStaffAnalysisInfoBusinessModel, List<MedicalStaffAnalysisInfoBusinessModel>>(objMedicalAnalysis);

				if (responseMedical != null)
				{
					medicalAnalysisInfoData = responseMedical.ToList();
					totalRecords = responseMedical.Count;
					totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
				}

				var medicalAnalysisInfojson = new
				{
					total = totalPages,
					page = page,
					records = totalRecords,
					rows = medicalAnalysisInfoData,
				};
				return Json(medicalAnalysisInfojson, JsonRequestBehavior.AllowGet);
			}
			else
			{
				var medicalAnalysisInfojson = new
				{
					total = totalPages,
					page = page,
					records = totalRecords,
					rows = new List<MedicalStaffAnalysisInfoBusinessModel>(),
				};
				return Json(medicalAnalysisInfojson, JsonRequestBehavior.AllowGet);
			}
		}

		/// <summary>
		/// Insert new MedicalStaff/AnalysisInformation into CustomerMedicalStaffAnalysis through MedicalStaff Popup
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="listId">The list identifier.</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <returns>
		/// ActionResult.
		/// </returns>
		[HttpPost]
		public ActionResult AddMedicalStaffAnalysisInfo(string name, int listId, long customerId)
		{
			if (customerId != null && customerId != -1)
			{
				var objPatientInfo = new MedicalStaffAnalysisInfoBusinessModel()
				{
					StaffName = name,
					ListId = listId,
					PatientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture),
					CustomerId = customerId,
					UserId = ControllerHelper.CurrentUser.UserId,
					Description = Constants.AddCustomerDescription,
					ModifiedDate = DateTime.Now.ToString(CultureInfo.InvariantCulture)
				};

				var serviceHelper = new WebServiceHelper(Constants.ActionNameSaveCustomerInformation);
				var MedStaffAnalysisInfoData = serviceHelper.PostMethodServiceRequest(objPatientInfo);
				LogHelper.LogAction(Constants.ActionLogSaveCustomerInfo, Constants.PatientController, Constants.ActionNameSaveCustomerInformation, globalUserId);
				if (MedStaffAnalysisInfoData != null)
				{
					var customermedstaffId = MedStaffAnalysisInfoData.CustomerMedicalStaffAnalysisInfoId;
					return Json(customermedstaffId, JsonRequestBehavior.AllowGet);
				}

				return Json(-1, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		#endregion ////MedicalsStaff and AnalysisInformation

		#region Contact Info

		/// <summary>
		/// Show specific Patient Contact Details
		/// </summary>
		/// <param name="page">int page</param>
		/// <param name="rows">int rows</param>
		/// <param name="sidx">The sidx.</param>
		/// <param name="sord">The sord.</param>
		/// <param name="contactType">Type of the contact.</param>
		/// <param name="contactTypeId">The contact type identifier.</param>
		/// <returns>
		/// List of the Details of patient's Contact Persons
		/// </returns>
		public JsonResult GetPatientContactInfo(int page, int rows, string sidx, string sord, string patientId, string careHomeId, string customerId)
		{
			var patientContactInfoBusinessModel = new PatientContactInfoBusinessModel()
			{
				UserID = globalUserId,
				PatientID = patientId,
				CareHomeId = careHomeId,
				CustomerId = customerId,
				Page = page,
				SortType = sord,
				SortColumn = sidx,
				RowCount = rows
			};

			var totalRecords = 0;
			var totalPages = 0;

			var patientContactInfoData = customerHelper.GetContactInfoDetails(patientContactInfoBusinessModel, out totalRecords, out totalPages);

			var sortedPatientContactInfo = new
			{
				total = totalPages,
				page = page,
				records = totalRecords,
				rows = patientContactInfoData,
			};


			return this.Json(sortedPatientContactInfo, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Save patient contact info
		/// </summary>
		/// <param name="patientContactInfoBusinessModel">The patient contact information business model.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpPost]
		public ActionResult SaveContactInfoDetails(PatientContactInfoBusinessModel patientContactInfoBusinessModel)
		{
			bool response = false;
			if (patientContactInfoBusinessModel != null)
			{
				patientContactInfoBusinessModel.UserID = globalUserId;
			}
			response = customerHelper.SavePatientContactInfo(patientContactInfoBusinessModel);
			LogHelper.LogAction(Constants.ActionSaveContactInfo, Constants.PatientController, Constants.ActionSaveContactInfo, globalUserId);
			return Json(new { status = response }, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Remove Contact info details
		/// </summary>
		/// <param name="personalInformationIds">The personal information ids.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult RemoveContactInfoDetails(string[] personalInformationIds)
		{
			if (personalInformationIds == null)
			{
				return null;
			}

			var patientContactInfoBusinessModel = new PatientContactInfoBusinessModel()
			{
				PersonalInformationIds = personalInformationIds.ToList(),
				UserID = globalUserId
			};

			ControllerHelper.ActionName = Constants.ActionRemoveContactInfo;
			var response = ControllerHelper.GetMethodServiceRequestObject(patientContactInfoBusinessModel);
			return Json(new { status = response }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		/// <summary>
		/// Get method to display Data of Recent Order on popup
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="rows">The rows.</param>
		/// <param name="sidx">The sidx.</param>
		/// <param name="sord">The sord.</param>
		/// <param name="searchId">The search identifier.</param>
		/// <param name="recentOrderType">Type of the recent order.</param>
		/// <returns>
		/// List of the Products in patient's Recent orders sorted according to Order Date in descending order
		/// </returns>
		public JsonResult RecentOrders(int page, int rows, string sidx, string sord, string searchId, string recentOrderType)
		{
			if (searchId == null)
			{
				return null;
			}

			var patientOrdersBusinessModel = new PatientOrdersBusinessModel()
			{
				ModifiedBy = new Guid(globalUserId),
				RecentOrderType = recentOrderType,
				SearchId = searchId,
				Page = page,
				SortType = sord,
				SortColumn = sidx,
				RowCount = rows
			};

			var totalRecords = 0;
			var totalPages = 0;

			var recentOrdersData = customerHelper.GetRecentOrderDetails(patientOrdersBusinessModel, out totalRecords, out totalPages);

			recentOrdersData.ForEach(a =>
			{
				a.CustomerName = Session[Constants.SessionCustomerName] != null ? Session[Constants.SessionCustomerName].ToString() : string.Empty;
				a.PatientName = !string.IsNullOrEmpty(a.PatientName) ? a.PatientName : string.Empty;// Session[Constants.SessionPatientName] != null ? Session[Constants.SessionPatientName].ToString() : string.Empty;
			});

			var sortedRecentOrders = new
			{
				total = totalPages,
				page = page,
				records = totalRecords,
				rows = recentOrdersData,
			};
			return this.Json(sortedRecentOrders, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Add a new patient Note
		/// </summary>
		/// <param name="noteText">The note text.</param>
		/// <returns>
		/// True if Note is added
		/// </returns>
		[HttpPost]
		public string AddPatientNote(string noteText)
		{
			var userId = globalUserId;
			var objPatientNotesBusinessModel = new PatientNotesBusinessModel { PatientId = Convert.ToInt32(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture), NoteText = noteText, CreatedBy = userId, CreatedDate = DateTime.Now.ToString(CultureInfo.InvariantCulture) };

			var serviceHelper = new WebServiceHelper(Constants.ActionAddPatientNote);
			var addNoteResponse = (bool)serviceHelper.PostMethodServiceRequestObject(objPatientNotesBusinessModel);
			LogHelper.LogAction(Constants.ActionLogAddPatientNote, Constants.PatientController, Constants.ActionAddPatientNote, userId);
			return Convert.ToString(addNoteResponse, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Load the Patient Notes
		/// </summary>
		/// <param name="page">The page.</param>
		/// <param name="rows">The rows.</param>
		/// <returns>
		/// List of Patient's notes with the recent most note on top.
		/// </returns>
		public JsonResult LoadPatientNotes(int page, int rows)
		{
			var userId = globalUserId;
			var objPatientNotesBusinessModel = new PatientNotesBusinessModel { PatientId = Convert.ToInt32(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture), CreatedBy = userId };
			var serviceHelper = new WebServiceHelper(Constants.ActionLoadPatientNotes);
			var loadNoteResponse = serviceHelper.PostMethodServiceRequest<PatientNotesBusinessModel, List<PatientNotesBusinessModel>>(objPatientNotesBusinessModel);
			return this.Json(loadNoteResponse, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Save Patient DetailsRemovedStopped
		/// </summary>
		/// <param name="objPatientdetails">Patient Info ViewModel</param>
		/// <returns>
		/// The new StatusId of the patient
		/// </returns>
		[HttpPost]
		public ActionResult SavePatientDetailsRemovedStopped(PatientInfoViewModel objPatientdetails)
        {
            if (Session[Constants.SessionPatientId] == null)
            {
                return null;
            }

            var objPatientBusinessModel = new PatientBusinessModel
            {
                PatientId = objPatientdetails.PatientId,
                CustomerId = objPatientdetails.CustomerId,
                PatientStatusId = objPatientdetails.PatientStatusId,
                ReasonCodeId = objPatientdetails.ReasonCodeId,
                ModifiedBy = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture)
            };
			var serviceHelper = new WebServiceHelper(Constants.ActionUpdatePatientDetails);
            var response = serviceHelper.PostMethodServiceRequestObject(objPatientBusinessModel);
            return Json(new { status = response }, JsonRequestBehavior.AllowGet);
        }

		/// <summary>
		/// Save Patient Details
		/// </summary>
		/// <param name="objPatientdetails">object of Patient's Personal Informaion details</param>
		/// <param name="objPrescriptionDetails">List of Product details in Prescription of the patient</param>
		/// <param name="objMedicalstaffdetails">List of Medical Staff details assigned to patient</param>
		/// <param name="objPatientanlysisdetails">List of Analysis Information details assigned to patient</param>
		/// <param name="objNurseComment">Nurse details and comments (if any)</param>
		/// <returns>
		/// patient's Id, StatusId, ReasonCode id if patient is saved
		/// </returns>
		[HttpPost]
		public ActionResult SavePatientDetails(PatientInfoViewModel objPatientdetails, List<PatientPrescriptionViewModel> objPrescriptionDetails, List<PatientAnalysisMedicalStaffViewModel> objMedicalstaffdetails, List<PatientAnalysisMedicalStaffViewModel> objPatientanlysisdetails, NurseCommentsViewModel objNurseComment, bool isCopyDetailCall) // , NurseCommentsViewModel objNurseComment
		{
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SavePatientDetails Start - " + guidKey, "Patient", "SavePatientDetails Start", globalUserId);
			long patientId = 0;
			long patientForCopyDetails = 0;
			if (Session[Constants.SessionPatientId] != null)
			{
				patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
				patientForCopyDetails = patientId;
			}

			if (patientId == 0)
			{
				Session[Constants.SessionIsOrderActived] = null;
			}

			#region Procare Patient NDD Calculation if patient type is changes to procare
			if (objPatientdetails.RecalcuateNDDForProCare)
			{
				var nextDeliveryDate = GetNDDForProcare(Convert.ToString(objPatientdetails.CustomerId, CultureInfo.InvariantCulture), objPatientdetails.Round, objPatientdetails.RoundId, objPatientdetails.PostCode, objPatientdetails.ExistingPatientNDD);
				objPrescriptionDetails.Where(q => q.Status.ToLower(CultureInfo.InvariantCulture) == "active").ToList().ForEach(q => q.ChangeNDD = nextDeliveryDate.ToString(Constants.DateFormat));
				objPatientdetails.NextDeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat);
			}
			#endregion

			List<MedicalStaffAnalysisInfoBusinessModel> objmedical = new List<MedicalStaffAnalysisInfoBusinessModel>();
			if (objMedicalstaffdetails != null)
			{
				foreach (var item in objMedicalstaffdetails)
				{
					var objMedicalstaffdetailsLists = new MedicalStaffAnalysisInfoBusinessModel
					{
						ListTypeId = item.ListTypeId,
						ListId = item.ListId,
						CustomerMedicalStaffAnalysisInfoId = item.CustomerMedicalStaffAnalysisId,
						PatientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture),
						IDXPatientMedicalStaffId = item.IdxPatientMedicalStaffId
					};
					objmedical.Add(objMedicalstaffdetailsLists);
				}
			}

			List<MedicalStaffAnalysisInfoBusinessModel> objpatientanalysis = new List<MedicalStaffAnalysisInfoBusinessModel>();
			if (objPatientanlysisdetails != null)
			{
				foreach (var item in objPatientanlysisdetails)
				{
					var objPatientanlysisdetailsLists = new MedicalStaffAnalysisInfoBusinessModel
					{
						ListTypeId = item.ListTypeId,
						ListId = item.ListId,
						CustomerMedicalStaffAnalysisInfoId = item.CustomerMedicalStaffAnalysisId,
						PatientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture),
						IDXPatientMedicalStaffId = item.IdxPatientMedicalStaffId
					};
					objpatientanalysis.Add(objPatientanlysisdetailsLists);
				}
			}

			List<PrescriptionBusinessModel> objPrescription = new List<PrescriptionBusinessModel>();
			if (objPrescriptionDetails != null)
			{
				foreach (var item in objPrescriptionDetails)
				{
					var objPatientanlysisdetailsLists = new PrescriptionBusinessModel
					{
						IDXPrescriptionProductId = item.IDXPrescriptionProductId,
						ActualPadsPerDay = item.ActualPadsPerDay,
						AssessedPadsPerDay = item.AssessedPadsPerDay,
						ProductId = item.ProductId,
						DQ1 = item.DQ1,
						DQ2 = item.DQ2,
						DQ3 = item.DQ3,
						DQ4 = item.DQ4,
						Frequency = item.Frequency,
						Status = item.Status,
						ValidFromDate = item.ValidFromDate,
						ValidToDate = item.ValidToDate,
						ProductName = item.ProductName,
						PrescriptionAuthorizationApprovalFlag = item.PrescriptionAuthorizationApprovalFlag,
						ProductApprovalFlag = item.ProductApprovalFlag,
						RowStatus = item.RowStatus,
						SalesUnit = item.SalesUnit,
						DeliveryDate = item.DeliveryDate,
						ActualPPD = item.ActualPPD,
						IsRemoved = item.IsRemoved,
						ChangeNDD = item.ChangeNDD,
						IsOverridden = item.IsOverriddenFlag
					};
					objPrescription.Add(objPatientanlysisdetailsLists);
				}
			}

			// Set default Round Id if round is not assigned to Customer, It will be removed once roundId logic is fixed            
			//string strRoundId = string.IsNullOrEmpty(objPatientdetails.RoundId) ? "10" : objPatientdetails.RoundId;

			var NurseComment = new NurseCommentsBusinessModel();
			if (!string.IsNullOrEmpty(objNurseComment.Name))
			{
				NurseComment = new NurseCommentsBusinessModel
				{
					UserId = Guid.Parse(globalUserId),
					Name = objNurseComment.Name,
					Email = objNurseComment.Email,
					MobileNumber = objNurseComment.MobileNumber,
					Comments = objNurseComment.Comments,
					RoleId = roleId
				};
			}

			//As per Roxy, this is not required as of now.
			objPatientdetails.MarketingPreferences = null;
			objPatientdetails.IsDataProtected = false;

			var objPatientBusinessModel = new PatientBusinessModel
			{
				PatientId = objPatientdetails.PatientId,
				SAPPatientNumber = objPatientdetails.SAPPatientNumber,
				PersonId = objPatientdetails.PersonId,
				DateOfBirth = objPatientdetails.DateOfBirth,
				PatientStatusId = objPatientdetails.PatientStatusId,
				PatientTypeId = objPatientdetails.PatientTypeId,
				ReasonCodeId = objPatientdetails.ReasonCodeId,
				LastModifiedBy = objPatientdetails.LastModifiedBy,
				ContactPersonId = objPatientdetails.ContactPersonId,
				DeliveryInstructionText = objPatientdetails.DeliveryInstructionText,
				PrescriptionId = objPatientdetails.PrescriptionId,
				DeliveryFrequency = objPatientdetails.DeliveryFrequency,
				RoundId = objPatientdetails.RoundId,
				Round = objPatientdetails.Round,
				AlternateDeliveryPoint = objPatientdetails.AlternateDeliveryPoint,
				NHSId = objPatientdetails.NHSId,
				LocalId = objPatientdetails.LocalId,
				CustomerId = objPatientdetails.CustomerId,
				AssignedCustomer = objPatientdetails.AssignedCustomer,
				CareHomeAssignmentDate = objPatientdetails.CarehomeAssignmentDate,
				AssessmentDate = objPatientdetails.AssessmentDate,
				NextAssessmentDate = objPatientdetails.NextAssessmentDate,
				DeliveryDate = string.IsNullOrWhiteSpace(objPatientdetails.DeliveryDate) ? System.DateTime.Now.ToString(DateFormat, CultureInfo.InvariantCulture) : objPatientdetails.DeliveryDate,
				NextDeliveryDate = objPatientdetails.NextDeliveryDate,
				CommunicationFormat = objPatientdetails.CommunicationFormat,
				CareHomeId = objPatientdetails.CareHomeId,
				CareHomeName = objPatientdetails.CareHomeName,
				HouseNumber = objPatientdetails.HouseNumber,
				HouseName = objPatientdetails.HouseName,
				Address1 = objPatientdetails.Address1,
				Address2 = objPatientdetails.Address2,
				City = objPatientdetails.TownOrCity,
				PostCode = objPatientdetails.PostCode,
				POldPatientStatus = objPatientdetails.OldPatientStatus,
				POldPatientType = objPatientdetails.OldPatientType,
				Country = objPatientdetails.Country,
				County = objPatientdetails.County,
				ModifiedBy = globalUserId,
				ADP1 = objPatientdetails.ADP1,
				ADP2 = objPatientdetails.ADP2,
				PFirstName = objPatientdetails.FirstName,
				PTitleId = objPatientdetails.TitleId,
				PGender = objPatientdetails.Gender,
				PLastName = objPatientdetails.LastName,
				PTelephoneNumber = objPatientdetails.TelephoneNumber,
				PMobileNumber = objPatientdetails.MobileNumber,
				PEmail = objPatientdetails.Email,
				PAssignedCareHome = objPatientdetails.AssignedCareHome,
				BillTo = SetPatientBillTo(DateTime.ParseExact(objPatientdetails.DateOfBirth, DateFormat, CultureInfo.InvariantCulture)),
				MedicalstaffList = objmedical,
				PatientAnalysisList = objpatientanalysis,
				PrescriptionList = objPrescription,
				NurseComment = NurseComment,
				//Set Correspondence Address 
				CorrespondenceSameAsDelivery = objPatientdetails.CorrespondenceSameAsDelivery,
				CorrespondenceCareHomeName = objPatientdetails.CorrespondenceCareHomeName,
				CorrespondenceAddress1 = objPatientdetails.CorrespondenceAddress1,
				CorrespondenceAddress2 = objPatientdetails.CorrespondenceAddress2,
				CorrespondenceTownOrCity = objPatientdetails.CorrespondenceTownOrCity,
				CorrespondencePostCode = objPatientdetails.CorrespondencePostCode,
				CorrespondenceCounty = objPatientdetails.CorrespondenceCounty,
				CorrespondenceCountry = objPatientdetails.Country,
				//Set Communication Preferences
				CommunicationPreferences = objPatientdetails.CommunicationPreferences,
				MarketingPreferences = objPatientdetails.MarketingPreferences,
				IsDataProtected = objPatientdetails.IsDataProtected,

				// Clinical Contact & Analysis Info
				IsClinicalLinkage = objPatientdetails.IsClinicalLinkage,
				IsClinicalContactChanged = objPatientdetails.IsClinicalContactChanged,
				IsAnalysisInfoChanged = objPatientdetails.IsAnalysisInfoChanged
			};

			var serviceHelper = new WebServiceHelper(Constants.ActionAddPatientDetails);
			var addPatientResponse = serviceHelper.PostMethodServiceRequest<PatientBusinessModel>(objPatientBusinessModel);
			Session[Constants.SessionPatientId] = Convert.ToInt64(addPatientResponse.PatientId, CultureInfo.InvariantCulture);
			bool savedpatientstatus = (bool)addPatientResponse.PatientSaveStatus;
			string newpatientId = Convert.ToString(addPatientResponse.PatientId, CultureInfo.InvariantCulture);
			long newCustomerId = Convert.ToInt64(addPatientResponse.CustomerId, CultureInfo.InvariantCulture);
			long newPatientStatusId = Convert.ToInt64(addPatientResponse.PatientStatusId, CultureInfo.InvariantCulture);
			long newreasonCodeId = Convert.ToInt64(addPatientResponse.ReasonCodeId, CultureInfo.InvariantCulture);
			string newPatientFrequency = Convert.ToString(addPatientResponse.DeliveryFrequency, CultureInfo.InvariantCulture);
			string newPatientname = Convert.ToString(addPatientResponse.PatientName, CultureInfo.InvariantCulture);
			string newRound = Convert.ToString(addPatientResponse.Round, CultureInfo.InvariantCulture);
			string newRoundId = Convert.ToString(addPatientResponse.RoundId, CultureInfo.InvariantCulture);
			string newRemovedStoppedDateTime = !string.IsNullOrEmpty(addPatientResponse.RemovedStoppedDateTime) ? Convert.ToDateTime(addPatientResponse.RemovedStoppedDateTime, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "";
			string newFrequencyNDD = !string.IsNullOrEmpty(addPatientResponse.NextDeliveryDate) ? Convert.ToDateTime(addPatientResponse.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : "";
			if (savedpatientstatus == true)
			{
				Session[Constants.SessionCustomerId] = newCustomerId;
				serviceHelper = new WebServiceHelper(Constants.ActionGetCustomerParameter);
				var objcustomerParameterBusinessModel = new CustomerParameterBusinessModel
				{
					CustomerID = Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture)
				};
				var loadCustomerParameters = serviceHelper.PostMethodServiceRequest<CustomerParameterBusinessModel, List<CustomerParameterBusinessModel>>(objcustomerParameterBusinessModel);
				Session[Constants.SessionCustomerParameters] = loadCustomerParameters;
				Session[Constants.SessionPatientName] = newPatientname.Replace("'", "\\'");
				Session[Constants.SessionCustomerName] = objPatientBusinessModel.AssignedCustomer;
				Session[Constants.SessionPatientStatus] = newPatientStatusId;
				Session[Constants.SessionPatientId] = newpatientId;
				Session[Constants.SessionIsCopyDetails] = false;
				Session[Constants.SessionCopyPatientStatus] = newPatientStatusId;
				Session[Constants.SessionCopyPatientId] = newpatientId;
				var patientDisplayName = newpatientId + " " + newPatientname;
				var customerDisplayName = objPatientBusinessModel.AssignedCustomer;
				if (!string.IsNullOrEmpty(objPatientBusinessModel.SAPPatientNumber))
				{
					patientDisplayName += " (" + objPatientBusinessModel.SAPPatientNumber + ")";
				}

				Session[Constants.SessionpatientDisplayName] = Convert.ToString(patientDisplayName, CultureInfo.InvariantCulture);
				Session[Constants.SessionCustomerDisplayName] = Convert.ToString(customerDisplayName, CultureInfo.InvariantCulture);
				Session[Constants.SessionIsLoadPrescription] = true;

				if (objPatientdetails.AssignedCareHome != 0 && objPatientdetails.AssignedCareHome != null)
				{
					Session[Constants.SessionCareHomeDisplayName] = Convert.ToString(addPatientResponse.CareHomeName, CultureInfo.InvariantCulture);
					Session[Constants.SessionCareHomeId] = objPatientdetails.AssignedCareHome;
					Session[Constants.SessionCareHomeName] = objPatientdetails.CareHomeName;
					Session[Constants.SessionCareHomeSAPId] = objPatientdetails.CareHomeSAPId;
				}
				else
				{
					Session[Constants.SessionCareHomeDisplayName] = null;
					Session[Constants.SessionCareHomeId] = null;
					Session[Constants.SessionCareHomeName] = null;
					Session[Constants.SessionCareHomeSAPId] = null;
				}
				if (isCopyDetailCall)
				{
					if (patientId != 0)
					{
						SaveContactInfoDetailsForCopyPatient(Convert.ToString(patientForCopyDetails, CultureInfo.InvariantCulture), newpatientId);
						SaveCopyNotes(Convert.ToString(patientForCopyDetails, CultureInfo.InvariantCulture), Convert.ToInt64(newpatientId, CultureInfo.InvariantCulture), "false");
						SaveChangesToPreviousPatient(patientForCopyDetails, Convert.ToInt64(Session[Constants.SessionPreviousCustomerId]));
					}
				}
			}
			LogHelper.LogAction("SavePatientDetails End - " + guidKey, "Patient", "SavePatientDetails End", globalUserId);

			//return Json(addPatientResponse, JsonRequestBehavior.AllowGet);
			return Json(new { savestatus = savedpatientstatus, PatientId = newpatientId, PatientStatusId = newPatientStatusId, ReasonCodeId = newreasonCodeId, Frequency = newPatientFrequency, Round = newRound, RoundId = newRoundId, RemovedStoppedDateTime = newRemovedStoppedDateTime, FrequencyNDD = newFrequencyNDD }, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Set Patient BillTo
		/// </summary>
		/// <param name="dateOfBirth">Patient's Date of Birth</param>
		/// <returns>
		/// Integer Id which tells whether the bill should go to Adult or Paediatric
		/// </returns>
		protected int? SetPatientBillTo(DateTime dateOfBirth)
		{
			int? billTo = null;
			if (Session[Constants.SessionCustomerParameters] != null && Session[Constants.SessionPatientTypeMatrix] != null)
			{
				var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];

				var checkAge = customerParameter.Select(q => q.CheckAge).FirstOrDefault();
				if (checkAge)
				{
					var customerMinimumAge = customerParameter.Select(q => q.Age).FirstOrDefault();
					if (customerMinimumAge != null && customerMinimumAge != 0)
					{
						int age = ControllerHelper.AgeInYears(dateOfBirth, DateTime.Today);
						var patientTypeMatrix = (PatientTypeMatrixBusinessModel)Session[Constants.SessionPatientTypeMatrix];

						if (age >= customerMinimumAge)
						{
							billTo = Convert.ToInt32(patientTypeMatrix.AdultBillTo, CultureInfo.InvariantCulture);
						}
						else
						{
							billTo = Convert.ToInt32(patientTypeMatrix.PaedBillTo, CultureInfo.InvariantCulture);
						}
					}
				}
			}

			return billTo;
		}

		/// <summary>
		/// Load Reason Code
		/// </summary>
		/// <param name="Patientstatus">string Patient status</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult LoadReasonCode(string Patientstatus, long customerId)
		{
			var languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);

			var PatientStatusID = Convert.ToInt64(Patientstatus, CultureInfo.InvariantCulture);

			if (Patientstatus == Constants.ActiveReasonCodeId)
			{
				PatientStatusID = Convert.ToInt32(SCAEnums.ListType.ActivePatient, CultureInfo.InvariantCulture);
			}
			else
			{
				if (Patientstatus == Constants.StoppedReasonCodeId)
				{
					PatientStatusID = Convert.ToInt32(SCAEnums.ListType.StoppedPatient, CultureInfo.InvariantCulture);
				}
				else
				{
					PatientStatusID = Convert.ToInt32(SCAEnums.ListType.RemovedPatient, CultureInfo.InvariantCulture);
				}
			}
			WebServiceHelper serviceHelper = null;
			if (PatientStatusID == (long)SCAEnums.ListType.RemovedPatient)
			{
				serviceHelper = new WebServiceHelper(Constants.ActionNameGetRemovedPatientReasonCode,
									 new[] { customerId.ToString(CultureInfo.InvariantCulture), Convert.ToString(languageId, CultureInfo.InvariantCulture) });
			}
			else if (PatientStatusID == (long)SCAEnums.ListType.StoppedPatient)
			{
				serviceHelper = new WebServiceHelper(Constants.ActionNameGetStoppedPatientReasonCode,
									 new[] { customerId.ToString(CultureInfo.InvariantCulture), Convert.ToString(languageId, CultureInfo.InvariantCulture) });
			}
			else
			{
				serviceHelper = new WebServiceHelper(Constants.ActionNameGetListType,
									new[] { PatientStatusID.ToString(CultureInfo.InvariantCulture), Convert.ToString(languageId, CultureInfo.InvariantCulture) });
			}

			var responseReasonCode = serviceHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
			return this.Json(responseReasonCode, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Load Title
		/// </summary>
		/// <param name="Gender">string Gender</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult LoadTitle(string Gender)
		{
			var languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);
			var genderID = 0;
			if (!string.IsNullOrEmpty(Gender))
			{
				genderID = Convert.ToInt32(Gender, CultureInfo.InvariantCulture);
			}

			var genderTitleId = 0;
			if (genderID == (long)SCAEnums.Gender.Male)
			{
				genderTitleId = Convert.ToInt32(SCAEnums.ListType.MaleTitle, CultureInfo.InvariantCulture);
			}
			else if (genderID == (long)SCAEnums.Gender.Female)
			{
				genderTitleId = Convert.ToInt32(SCAEnums.ListType.FemaleTitle, CultureInfo.InvariantCulture);
			}
						
			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetListType, new[] { genderTitleId.ToString(CultureInfo.InvariantCulture), Convert.ToString(languageId, CultureInfo.InvariantCulture) });
			var responseGender = serviceHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
			return this.Json(responseGender, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Set Customer Session
		/// </summary>
		/// <param name="customerid">string customerid</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult SetCustomerSession(string customerid)
		{
			if (customerid != "-1")
			{
				var objcustomerParameterBusinessModel = new CustomerParameterBusinessModel
				{
					CustomerID = Convert.ToInt64(customerid, CultureInfo.InvariantCulture),
					UserID = globalUserId
				};
				var serviceHelper = new WebServiceHelper(Constants.ActionGetCustomerParameter);
				var loadCustomerParameters = serviceHelper.PostMethodServiceRequest<CustomerParameterBusinessModel, List<CustomerParameterBusinessModel>>(objcustomerParameterBusinessModel);
				Session[Constants.SessionCustomerParameters] = loadCustomerParameters;

				// Set the Clinical Contact With Linkage value in Controller Helper.
				if (loadCustomerParameters != null)
				{
					Session[Constants.SessionCustomerId] = customerid; //---Setting customer Id for Searching the product from Add product Button for Add Patient menu
				}

				return this.Json(loadCustomerParameters, JsonRequestBehavior.AllowGet);
			}
			else
			{
				Session[Constants.SessionCustomerId] = 0;
				Session[Constants.SessionCustomerParameters] = null;
				Session[Constants.SessionPostcodeMatrix] = null;
			}

			return null;
		}

		/// <summary>
		/// Get ViewPatient Access
		/// </summary>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpGet]
		public ActionResult GetViewPatientAccess()
		{
			bool bFlag = false;
			if ((userType == Constants.SCAUserType && roleName == Constants.DisplayRoleName) ||
				(userType == Constants.ExternalUserType && roleName == Constants.DisplayRoleName) ||
				(userType == Constants.ExternalUserType && roleName == Constants.CareHomeRoleName))
			{
				bFlag = true;
			}

			return this.Json(bFlag, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Render patient
		/// </summary>
		/// <param name="PatientId">string Patient</param>
		/// <param name="CustomerId">string CustomerId</param>
		/// <param name="PatientName">string PatientName</param>
		/// <param name="CustomerName">string CustomerName</param>
		/// <param name="RedirectFrom">string RedirectFrom</param>
		/// <returns>
		/// return bool
		/// </returns>
		public bool Renderpatient(string PatientId, string CustomerId, string PatientName, string CustomerName, string RedirectFrom)
		{
			string oldSessionPatientId = Session[Constants.SessionPatientId] != null ? Convert.ToString(Session[Constants.SessionPatientId], CultureInfo.CurrentCulture).Trim() : string.Empty;
			if (Session[Constants.SessionPatientId] == null)
				Session[Constants.SessionDoNotShowForPatient] = "false";
			else if (oldSessionPatientId != (!string.IsNullOrEmpty(PatientId) ? PatientId.Trim() : PatientId))
				Session[Constants.SessionDoNotShowForPatient] = "false";

			Session[Constants.SessionApprovePatientId] = PatientId;
			Session[Constants.SessionApproveCustomerId] = CustomerId;
			Session[Constants.SessionApprovePatientName] = PatientName.Replace(PatientId, string.Empty).Trim();
			Session[Constants.SessionApproveCustomerName] = CustomerName.Replace(CustomerId, string.Empty).Trim();
			Session[Constants.SessionRedirectFrom] = RedirectFrom;

			Session[Constants.SessionPatientId] = PatientId;
			Session[Constants.SessionCustomerId] = CustomerId;
			Session[Constants.SessionPatientName] = PatientName.Replace(PatientId, string.Empty).Replace("'", "\\'").Trim();
			Session[Constants.SessionCustomerName] = CustomerName.Replace(CustomerId, string.Empty).Trim();

			if (RedirectFrom == "Authorisation")
			{
				Session[Constants.SessionisRedirectFromAuthorisation] = 1;
				Session[Constants.SessionOrderSavedFromOneOff] = "False";
			}
			else if (RedirectFrom == "OneOffOrder" || RedirectFrom == "CreateInteraction")
			{
				Session[Constants.SessionisRedirectFromAuthorisation] = 0;
				Session[Constants.SessionOrderSavedFromOneOff] = "True";
			}
			else
			{
				Session[Constants.SessionisRedirectFromAuthorisation] = 0;
				Session[Constants.SessionOrderSavedFromOneOff] = "False";
			}

			return true;
		}

		/// <summary>
		/// Patient Details
		/// </summary>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult PatientDetails()
		{
			Session[Constants.SessionIsLoadPrescription] = true;
			var modelPatient = new PatientViewModel
			{
				ViewModelPatientPrescription = new PatientPrescriptionViewModel(),
				ViewModelPatientInfo = new PatientInfoViewModel(),
				ViewModelPatientAnalysisMedicalStaff = new PatientAnalysisMedicalStaffViewModel()
			};

			modelPatient.ViewModelPatientInfo.PatientInteractionId = Convert.ToString(Session[Constants.InteractionId], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.IsCalledFromHomePage = Convert.ToString(Session[Constants.IsCalledFromHomePage], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.IsCalledFromSearchPage = Convert.ToString(Session[Constants.SessionIsCalledFromSearch], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.SearchCustomer = Convert.ToString(Session[Constants.SessionSearchCustomer], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.SearchPatientStatus = Convert.ToString(Session[Constants.SessionSearchStatus], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.SearchPatientType = Convert.ToString(Session[Constants.SessionSearchPatientType], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.SearchOrderNumber = Convert.ToString(Session[Constants.SessionSearchOrderNumber], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.SearchId = Convert.ToString(Session[Constants.SessionSearchId], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.SearchType = Convert.ToString(Session[Constants.SessionTypeToSearch], CultureInfo.InvariantCulture);
			modelPatient.ViewModelPatientInfo.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
			if (Session[Constants.SessionCareHomeId] != null)
			{
				SetIsCarehomePatientProperty(modelPatient.ViewModelPatientInfo);
			}
			return View(Constants.ActionDetails, modelPatient);
		}

		/// <summary>
		/// Clear Prescription Approval Cache
		/// </summary>
		/// <returns>
		/// True if the Session values related to Authorization are cleared.
		/// </returns>
		public bool ClearPrescriptionApprovalCache()
		{
			bool isCacheCleared = false;
			try
			{
				Session[Constants.SessionApprovePatientId] = null;
				Session[Constants.SessionApproveCustomerId] = null;
				Session[Constants.SessionApprovePatientName] = null;
				Session[Constants.SessionApproveCustomerName] = null;
				Session[Constants.SessionisRedirectFromAuthorisation] = 0;
				isCacheCleared = true;
			}
			catch (InvalidOperationException e)
			{
				LogHelper.LogException(e, globalUserId);
				isCacheCleared = false;
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, globalUserId);
				isCacheCleared = false;
			}

			return isCacheCleared;
		}

		/// <summary>
		/// Check Product Exist
		/// </summary>
		/// <param name="productId">int productId</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult CheckProductExist(int productId)
		{
			string patientId = string.Empty;

			// SessionIsLoadPrescription Check is performed when priscription need to be filled empty while adding new patient
			if (Session[Constants.SessionIsLoadPrescription] != null && Convert.ToBoolean(Session[Constants.SessionIsLoadPrescription], CultureInfo.InvariantCulture))
			{
				patientId = Convert.ToString(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
			}

			if (!string.IsNullOrEmpty(patientId))
			{
				var PatientPrescriptionViewModel = new PrescriptionBusinessModel()
				{
					ProductId = productId,
					PatientID = Convert.ToInt64(patientId, CultureInfo.InvariantCulture)
				};

				ControllerHelper.ActionName = Constants.ActionCheckProductExist;
				var response = ControllerHelper.GetMethodServiceRequestObject(PatientPrescriptionViewModel);
				return Json(new { status = response }, JsonRequestBehavior.AllowGet);
			}

			return null;
		}

		/// <summary>
		/// Get Patient Sections Edit Access
		/// </summary>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpGet]
		public ActionResult GetPatientSectionsEditAccess()
		{
			// Get the PATIENT INFO, MEDICAL STAFF AND PRESCRIPTION SECTION FOR PATIENT SCREEN.
			bool isPatientInfoEditable = false;
			bool isMedicalInfoEditable = false;
			bool isPrescriptionEditable = false;

			// Set 1 = Full Rights, 0 = View Only , blank = No Acess
			string analysisInfoAccess = string.Empty;
			string clinicalContactAccess = string.Empty;
			string contactInfoAccess = string.Empty;
			string interactionAccess = string.Empty;
			string attachmentAccess = string.Empty;
			string notesAccess = string.Empty;
			string recentOrdersAccess = string.Empty;
			string sampleOrderAccess = string.Empty;
			string auditLogAccess = string.Empty;

			List<UserMenuDetailsBusinessModel> allRoleMenuDetails = (List<UserMenuDetailsBusinessModel>)Session[Constants.SessionAuthorizedUrl];
			//var patientInfoMenuObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.PatientScreenSectionMenu.PatientInfoSection && d.FullRights == false);
			//if (patientInfoMenuObj != null)
			//{
			//    isPatientInfoEditable = Convert.ToBoolean(patientInfoMenuObj.ViewOnly, CultureInfo.InvariantCulture);
			//}

			//var medicalInfoMenuObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.PatientScreenSectionMenu.MedicalInfoSection && d.FullRights == false);
			//if (medicalInfoMenuObj != null)
			//{
			//    isMedicalInfoEditable = Convert.ToBoolean(medicalInfoMenuObj.ViewOnly, CultureInfo.InvariantCulture);
			//}

			//var prescriptionMenuObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.PatientScreenSectionMenu.PrescriptionSection && d.FullRights == false);
			//if (prescriptionMenuObj != null)
			//{
			//    isPrescriptionEditable = Convert.ToBoolean(prescriptionMenuObj.ViewOnly, CultureInfo.InvariantCulture);
			//}

			/*
			 * I have modified super developeres code to make role mapping work. Based on their half wrongly understood and implemented logic.
			 * It is not possible to change entire logic as it has impact on multiple areas. so continueing with the below understanding.
			 * Here is the understanding and cahnges to super developers code:-
			 * if fullright is true then pass false, because in JS if it is false, then it will remove disable class for the section/ making it editable.
			 * if viewonly is true or object is null then pass true, because in JS, if it is true, then it will assign disable class/ making it non editable.
			 * "isPrescriptionEditable/isMedicalInfoEditable = false" means, user is having rights to edit the section else it is just view only/can not edit.                         
			 */

			var patientInfoMenuObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.PatientScreenSectionMenu.PatientInfoSection);
			if (patientInfoMenuObj != null)
			{
				if (patientInfoMenuObj.FullRights)
					isPatientInfoEditable = false;
				else if (patientInfoMenuObj.ViewOnly)
					isPatientInfoEditable = true;
			}
			else
			{
				isPatientInfoEditable = true;
			}

			var medicalInfoMenuObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.PatientScreenSectionMenu.MedicalInfoSection);
			if (medicalInfoMenuObj != null)
			{
				if (medicalInfoMenuObj.FullRights)
					isMedicalInfoEditable = false;
				else if (medicalInfoMenuObj.ViewOnly)
					isMedicalInfoEditable = true;
			}
			else
			{
				isMedicalInfoEditable = true;
			}

			var prescriptionMenuObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.PatientScreenSectionMenu.PrescriptionSection);
			if (prescriptionMenuObj != null)
			{
				if (prescriptionMenuObj.FullRights)
					isPrescriptionEditable = false;
				else if (prescriptionMenuObj.ViewOnly)
					isPrescriptionEditable = true;
			}
			else
			{
				isPrescriptionEditable = true;
			}

			// Check for the Patient Sub Module Access

			var analysisInfoObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.AddNewAnalysisInfo);
			if (analysisInfoObj != null)
			{
				if (analysisInfoObj.FullRights)
				{
					analysisInfoAccess = "1";
				}
				else if (analysisInfoObj.ViewOnly)
				{
					analysisInfoAccess = "0";
				}
			}


			var clinicalContactInfoObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.AddNewClinicalContact);
			if (clinicalContactInfoObj != null)
			{
				if (clinicalContactInfoObj.FullRights)
				{
					clinicalContactAccess = "1";
				}
				else if (clinicalContactInfoObj.ViewOnly)
				{
					clinicalContactAccess = "0";
				}
			}


			var contactInfoObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientContactInfo);
			if (contactInfoObj != null)
			{
				if (contactInfoObj.FullRights)
				{
					contactInfoAccess = "1";
				}
				else if (contactInfoObj.ViewOnly)
				{
					contactInfoAccess = "0";
				}
			}
			var interactionObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientInteraction);
			if (interactionObj != null)
			{
				if (interactionObj.FullRights)
				{
					interactionAccess = "1";
				}
				else if (interactionObj.ViewOnly)
				{
					interactionAccess = "0";
				}
			}
			var attachmentObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientAttachments);
			if (attachmentObj != null)
			{
				if (attachmentObj.FullRights)
				{
					attachmentAccess = "1";
				}
				else if (attachmentObj.ViewOnly)
				{
					attachmentAccess = "0";
				}
			}

			var notesObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientNotes);
			if (notesObj != null)
			{
				if (notesObj.FullRights)
				{
					notesAccess = "1";
				}
				else if (notesObj.ViewOnly)
				{
					notesAccess = "0";
				}
			}

			var recentOrdersObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientRecentOrders);
			if (recentOrdersObj != null)
			{
				if (recentOrdersObj.FullRights)
				{
					recentOrdersAccess = "1";
				}
				else if (recentOrdersObj.ViewOnly)
				{
					recentOrdersAccess = "0";
				}
			}

			var sampleOrdersObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientSampleOrders);
			if (sampleOrdersObj != null)
			{
				if (sampleOrdersObj.FullRights)
				{
					sampleOrderAccess = "1";
				}
				else if (sampleOrdersObj.ViewOnly)
				{
					sampleOrderAccess = "0";
				}
			}

			var auditLogObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientAuditLog);
			if (auditLogObj != null)
			{
				if (auditLogObj.FullRights)
				{
					auditLogAccess = "1";
				}
				else if (auditLogObj.ViewOnly)
				{
					auditLogAccess = "0";
				}
			}

			return this.Json(new
			{
				RoleName = roleName,
				IsPatientInfoEditable = isPatientInfoEditable,
				IsMedicalInfoEditable = isMedicalInfoEditable,
				IsPrescriptionEditable = isPrescriptionEditable,
				ContactInfoAccess = contactInfoAccess,
				InteractionAccess = interactionAccess,
				AttachmentAccess = attachmentAccess,
				NotesAccess = notesAccess,
				RecentOrdersAccess = recentOrdersAccess,
				SampleOrderAccess = sampleOrderAccess,
				AuditLogAccess = auditLogAccess,
				AnalysisInfoAccess = analysisInfoAccess,
				ClinicalContactAccess = clinicalContactAccess
			}, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get Customer PatientType
		/// </summary>
		/// <param name="Customerid">string Customerid</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpGet]
		public ActionResult GetCustomerPatientType(string customerId)
		{
			bool bFlag = false;
			ControllerHelper.ActionName = Constants.ActionGetCustomerPatientType;
			ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), this.globalUserId };
			var responsePatientType = ControllerHelper.GetMethodServiceRequestForList<List<PatientBusinessModel>>();
			if (responsePatientType == null)
			{
				return this.Json(bFlag, JsonRequestBehavior.AllowGet);
			}

			return this.Json(responsePatientType, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get Prescription NDD
		/// </summary>
		/// <param name="LDD">string LDD</param>
		/// <param name="frequency">string frequency</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpGet]
		public ActionResult GetPrescriptionNDD(string LDD, string frequency)
		{
			if (string.IsNullOrEmpty(LDD))
			{
				LDD = DateTime.Now.ToString(Constants.DateFormat, CultureInfo.CurrentCulture);
			}

			var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];

			var nextDeliveryDate = new DateTime();
			DateTime dtCalculated = DateTime.ParseExact(LDD, Constants.DateFormat, null);
			int iIncremenetdDays = Convert.ToInt32(frequency, CultureInfo.CurrentCulture) * 7;
			nextDeliveryDate = dtCalculated.AddDays(iIncremenetdDays);
			var CalculatedNDD = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.CurrentCulture);
			var MinFrequencyDate = "";
			if (customerParameter != null && customerParameter.Any())
			{
				var firstRecord = customerParameter.First();

				if (nextDeliveryDate < DateTime.Today.AddDays(firstRecord.OrderLeadTime))
				{
					CalculatedNDD = "MinFrequency";
					MinFrequencyDate = DateTime.Today.AddDays(firstRecord.OrderLeadTime).ToString(Constants.DateFormat, CultureInfo.CurrentCulture);
				}
			}

			return Json(new { MinFrequency = CalculatedNDD, MinFrequencyDate = MinFrequencyDate }, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get Patient NDD
		/// </summary>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpGet]
		public DateTime GetPatientNDD(bool isPatientTypeChanged, string round, string roundId, string postCode, bool reCalculate = true)
		{
            if (!reCalculate && !isPatientTypeChanged)
            {
                //Get Current NDD
                var patientId = Convert.ToString(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
                ControllerHelper.ActionName = Constants.ActionGetPatientCurrentNDD;
                ControllerHelper.ActionParam = new[] { patientId, this.globalUserId };
                var responsePatientNDD = ControllerHelper.GetMethodServiceRequest<string>();
                if (!string.IsNullOrEmpty(responsePatientNDD))
                {
                    return DateTime.ParseExact(responsePatientNDD, CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture);
                }
            }

            var customerId = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
			
			int countryId = Convert.ToInt32(SCAEnums.Country.UnitedKingdom);
			int orderLeadTime = Constants.DefaultLeadTime;

			if (Session[Constants.SessionCustomerParameters] != null)
			{
				var custParameter = ((List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters]).First();
				countryId = custParameter.CountryId;
				orderLeadTime = custParameter.OrderLeadTime;
			}

			if (string.IsNullOrEmpty(postCode))
			{
				postCode = "null";
			}

			var serviceHelper = new WebServiceHelper(Constants.ActionGetNDDForPatient);

			var nddCalculationBusinessModel = new NDDParametersBusinessModel()
			{
				CountryId = Convert.ToString(countryId, CultureInfo.InvariantCulture),
				LeadTime = Convert.ToString(orderLeadTime, CultureInfo.InvariantCulture),
				CustomerId = customerId,
				Round = round,
				RoundID = roundId,
				PostCode = postCode,
				UserId = serviceHelper.CurrentUser.UserId.ToString()
			};
			var responseDate = Convert.ToString(serviceHelper.GetMethodServiceRequestObject(nddCalculationBusinessModel), CultureInfo.InvariantCulture);
            return DateTime.ParseExact(responseDate, CommonConstants.DateFormatddMMyyyy, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// NDD calculation for Procare Patient if patient type is change to procare
		/// </summary>
		/// <param name="customerId"></param>
		/// <param name="round"></param>
		/// <param name="roundId"></param>
		/// <param name="postCode"></param>
		/// <param name="pateintNDD"></param>
		/// <returns></returns>
		public DateTime GetNDDForProcare(string customerId, string round, string roundId, string postCode, string pateintNDD)
		{
			int OrderLeadTime = Session[Constants.SessionCustomerParameters] != null ? ((List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters]).First().OrderLeadTime : Constants.DefaultLeadTime;

			var calculatedPatientNDD = DateTime.Now.Date;
			var patientNextDeliveryDate = Convert.ToDateTime(pateintNDD, CultureInfo.InvariantCulture);
			calculatedPatientNDD = GenericHelper.AddWorkingDays(calculatedPatientNDD, OrderLeadTime);

			var objPostCode = GenericHelper.GetPostCode(customerId, round, roundId, postCode);

			if (calculatedPatientNDD < patientNextDeliveryDate)
				calculatedPatientNDD = patientNextDeliveryDate;

			calculatedPatientNDD = GenericHelper.ValidatePostcodeMatrix(calculatedPatientNDD, objPostCode);

			return calculatedPatientNDD;
		}

		/// <summary>
		/// Add Business Days
		/// </summary>
		/// <param name="datetime">DateTime datetime</param>
		/// <param name="nDays">int nDays</param>
		/// <returns>
		/// Date Time after adding the input days as per business requirement to input date
		/// </returns>
		public DateTime AddBusinessDays(DateTime datetime, int nDays)
		{
			int weeks = nDays / 5;
			nDays %= 5;
			while (datetime.DayOfWeek == DayOfWeek.Saturday || datetime.DayOfWeek == DayOfWeek.Sunday)
			{
				datetime = datetime.AddDays(1);
			}

			while (nDays-- > 0)
			{
				datetime = datetime.AddDays(1);
				if (datetime.DayOfWeek == DayOfWeek.Saturday)
				{
					datetime = datetime.AddDays(2);
				}
			}

			return datetime.AddDays(weeks * 7);
		}

		/// <summary>
		/// CheckPostCodeMatchDay
		/// </summary>
		/// <param name="nextDeliveryDate"></param>
		/// <param name="postCodeMatrix"></param>
		/// <param name="datematched"></param>
		/// <returns></returns>
		private static bool CheckPostCodeMatchDay(DateTime nextDeliveryDate, PostCodeMatrixBusinessModel postCodeMatrix, bool datematched)
		{
			var dayOfWeek = nextDeliveryDate.DayOfWeek;
			if (!datematched && dayOfWeek == DayOfWeek.Monday && postCodeMatrix.Monday == true)
			{
				datematched = true;
			}

			if (!datematched && dayOfWeek == DayOfWeek.Tuesday && postCodeMatrix.Tuesday == true)
			{
				datematched = true;
			}

			if (!datematched && dayOfWeek == DayOfWeek.Wednesday && postCodeMatrix.Wednesday == true)
			{
				datematched = true;
			}

			if (!datematched && dayOfWeek == DayOfWeek.Thursday && postCodeMatrix.Thursday == true)
			{
				datematched = true;
			}

			if (!datematched && dayOfWeek == DayOfWeek.Friday && postCodeMatrix.Friday == true)
			{
				datematched = true;
			}
			return datematched;
		}

		/// <summary>
		/// Get Working Day After Holiday
		/// </summary>
		/// <param name="holidaylist">List of HolidayBusinessModel holidaylist</param>
		/// <param name="nextDeliveryDate">DateTime nextDeliveryDate</param>
		/// <returns>
		/// Next working day after holiday[/s]
		/// </returns>
		private DateTime GetWorkingDayAfterHoliday(List<HolidayBusinessModel> holidaylist, DateTime nextDeliveryDate)
		{
			bool holidayFlag = true;
			while (holidayFlag)
			{
				holidayFlag = false;
				if (holidaylist.Where(q => q.HolidayDate.Date == nextDeliveryDate.Date).Any())
				{
					nextDeliveryDate = nextDeliveryDate.AddDays(1);
					holidayFlag = true;
				}
			}

			return nextDeliveryDate;
		}

		/// <summary>
		/// Get Method for Details page
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult ViewDetails(string id)
		{
			if (userPasswordExpired)
			{
				return RedirectToAction("Login", "Account");
			}

			var modelPatient = new PatientViewModel
			{
				ViewModelPatientPrescription = new PatientPrescriptionViewModel(),
				ViewModelPatientInfo = new PatientInfoViewModel() { CreatedBy = userLoginName, CreatedDate = System.DateTime.Today.ToString(Constants.DateFormat, CultureInfo.CurrentCulture), FileCount = ConfigurationManager.AppSettings["SharedLocation"].ToString(CultureInfo.InvariantCulture), CareHomeId = !string.IsNullOrEmpty(id) ? Convert.ToInt64(id, CultureInfo.InvariantCulture) : 0 },
				ViewModelPatientAnalysisMedicalStaff = new PatientAnalysisMedicalStaffViewModel(),
			};
			Session[Constants.SessionIsCopyDetails] = false;
			modelPatient.ViewModelPatientInfo.FileCountErrorMessage = string.Format(CultureInfo.InvariantCulture, Resource.Common.resFileCountErrorMessage, ConfigurationManager.AppSettings["FileCount"].ToString(CultureInfo.InvariantCulture));
			modelPatient.ViewModelPatientInfo.InteractionViewModel = new InteractionViewModel();
			modelPatient.ViewModelPatientInfo.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
			if (Session[Constants.SessionCareHomeId] != null)
			{
				SetIsCarehomePatientProperty(modelPatient.ViewModelPatientInfo);
			}
			return View(Constants.ActionDetails, modelPatient);
		}

		/// <summary>
		/// This function is used to set IsCarehomePatient property value
		/// </summary>
		/// <param name="viewModelPatientInfo"></param>
		private void SetIsCarehomePatientProperty(PatientInfoViewModel viewModelPatientInfo)
		{
			string careHomeId = Convert.ToString(Session[Constants.SessionCareHomeId], CultureInfo.CurrentCulture);
			if (!string.IsNullOrEmpty(careHomeId) && careHomeId != "0")
			{
				viewModelPatientInfo.IsCarehomePatient = true;
			}
			else
			{
				viewModelPatientInfo.IsCarehomePatient = false;
			}
		}

		/// <summary>
		/// Get Patient Session
		/// </summary>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpGet]
		public ActionResult GetPatientSession()
		{
			if (Session[Constants.SessionPatientId] != null &&
			   Session[Constants.SessionCustomerId] != null &&
			   Session[Constants.SessionPatientName] != null &&
			   Session[Constants.SessionCustomerName] != null)
			{
				var sessionData = new
				{
					patientId = Convert.ToString(Session[Constants.SessionPatientId], CultureInfo.CurrentCulture),
					customerid = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.CurrentCulture),
					PatientName = Convert.ToString(Session[Constants.SessionPatientName], CultureInfo.CurrentCulture),
					CustomerName = Convert.ToString(Session[Constants.SessionCustomerName], CultureInfo.CurrentCulture),
					IsOrderActived = Convert.ToString(Session[Constants.SessionIsOrderActived], CultureInfo.CurrentCulture),
					PatientStatus = Convert.ToString(Session[Constants.SessionPatientStatus], CultureInfo.CurrentCulture),
					SAPCustomerNumber = Convert.ToString(Session[Constants.SessionSAPCustomerNumber], CultureInfo.CurrentCulture),
					SAPPatientNumber = Convert.ToString(Session[Constants.SessionSAPPatientNumber], CultureInfo.CurrentCulture)
				};
				return Json(sessionData, JsonRequestBehavior.AllowGet);
			}

			return null;
		}

		/// <summary>
		/// Clear the session of SessionIsRedirectFromProductOrder
		/// </summary>
		/// <returns></returns>
		public bool ClearIsRedirectFromProductOrderSession()
		{
			Session[Constants.SessionIsRedirectFromProductOrder] = "0";
			return true;
		}

		/// <summary>
		/// Set Patient Session
		/// </summary>
		/// <param name="patientId">string patientId</param>
		/// <param name="customerId">string customerId</param>
		/// <param name="PatientName">string PatientName</param>
		/// <param name="CustomerName">string CustomerName</param>
		/// <param name="SAPCustomerNumber">string SAPCustomerNumber</param>
		/// <param name="SAPPatientNumber">string SAPPatientNumber</param>
		/// <param name="IsOrderActived">string IsOrderActived</param>
		/// <param name="PatientStatus">string PatientStatus</param>
		/// <param name="CareHomeID">string CareHomeID</param>
		/// <param name="CareHomeName">string CareHomeName</param>
		/// <param name="PostCode">string PostCode</param>
		/// <param name="isRedirectFromProductOrder">The is redirect from product order.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpPost]
		public ActionResult SetPatientSession(string patientId, string customerId, string PatientName, string CustomerName, string SAPCustomerNumber, string SAPPatientNumber, string IsOrderActived, string PatientStatus, string CareHomeID, string CareHomeName, string PostCode, string isRedirectFromProductOrder)
		{

			Session[Constants.SessionIsOrderActived] = IsOrderActived;
			Session[Constants.SessionPatientStatus] = PatientStatus;
			Session[Constants.SessionIsCopyDetails] = false;
			if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromSearch], CultureInfo.InvariantCulture))
			{
				Session[Constants.SessionDoNotShowForPatient] = "false";
				Session[Constants.SessionDoNotShowForCarehome] = "false";
			}
			else if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromCareHome], CultureInfo.InvariantCulture))
			{
				string oldSessionPatientId = Session[Constants.SessionPatientId] != null ? Convert.ToString(Session[Constants.SessionPatientId], CultureInfo.CurrentCulture).Trim() : string.Empty;
				if (Session[Constants.SessionPatientId] == null)
					Session[Constants.SessionDoNotShowForPatient] = "false";
				else if (oldSessionPatientId != (!string.IsNullOrEmpty(patientId) ? patientId.Trim() : patientId))
					Session[Constants.SessionDoNotShowForPatient] = "false";
			}


			// Session[Constants.SessionIsLoadPrescription] = true;
			if ((patientId != null && patientId != "0"))
			{
				var objPatient = new PatientBusinessModel { PatientId = Convert.ToInt64(patientId, CultureInfo.InvariantCulture) };
				ControllerHelper.ActionName = Constants.ActionNameRetrievePatientDetails;
				var responseData = ControllerHelper.PostMethodServiceRequest<PatientBusinessModel>(objPatient);

				LoadPatientTypeMatrix(Convert.ToInt64(customerId, CultureInfo.InvariantCulture), Convert.ToInt64(responseData.PatientTypeId));

				Session[Constants.SessionPatientStatus] = responseData.PatientStatusId;

				Session[Constants.SessionPatientId] = patientId;
				Session[Constants.SessionPatientName] = patientId + " " + PatientName.Replace(patientId, string.Empty).Replace("'", "\\'").Trim();
				Session[Constants.SessionSAPPatientNumber] = SAPPatientNumber;
				Session[Constants.SessionIsCopyActiveOrder] = IsOrderActived.ToLower(CultureInfo.InvariantCulture);
				Session[Constants.SessionCopyPatientStatus] = responseData.PatientStatusId;
				var patientDisplayName = patientId + " " + PatientName;
				if (!string.IsNullOrEmpty(SAPPatientNumber))
				{
					patientDisplayName += " (" + SAPPatientNumber + ")";
				}
				Session[Constants.SessionpatientDisplayName] = Convert.ToString(patientDisplayName, CultureInfo.CurrentCulture);
			}

			if ((customerId != null && customerId != "0"))
			{
				Session[Constants.SessionCustomerId] = customerId;
				Session[Constants.SessionCustomerName] = customerId + " " + CustomerName.Replace(customerId, string.Empty).Trim();
				Session[Constants.SessionSAPCustomerNumber] = SAPCustomerNumber;

				var customerDisplayName = customerId + " " + CustomerName;
				SAPCustomerNumber = CommonHelper.TrimLeadingZeros(SAPCustomerNumber);
				customerDisplayName = !string.IsNullOrEmpty(SAPCustomerNumber) ? customerDisplayName.Contains(SAPCustomerNumber) ? customerDisplayName : customerDisplayName + "(" + SAPCustomerNumber + ")" : customerDisplayName;

				Session[Constants.SessionCustomerDisplayName] = Convert.ToString(customerDisplayName, CultureInfo.CurrentCulture);
				SetCustomerSession(customerId);
			}

			if (!(CareHomeID != null && CareHomeID.Length == 0) && !(CareHomeName != null && CareHomeName.Length == 0))
			{
				var carehomeDisplayName = CareHomeID + " " + CareHomeName + " " + PostCode;
				if (!string.IsNullOrEmpty(SAPPatientNumber))//for carehome sapcarehome number will store in SAPPatientNumber
				{
					carehomeDisplayName += " (" + SAPPatientNumber + ")";
				}
				Session[Constants.SessionCareHomeDisplayName] = carehomeDisplayName;
				Session[Constants.SessionCareHomeId] = CareHomeID;
				Session[Constants.SessionCareHomeSAPId] = SAPPatientNumber;
				Session[Constants.SessionCareHomeName] = CareHomeName;

			}

			if (!string.IsNullOrEmpty(isRedirectFromProductOrder))
			{
				Session[Constants.SessionIsRedirectFromProductOrder] = "1";
			}

			return Json(new { Message = 1 }, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// This function will set session value for DoNotStop functionality
		/// </summary>
		/// <param name="displayAsAlert"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult SetSessionForDoNotStop(string displayAsAlert)
		{
			if (!string.IsNullOrEmpty(displayAsAlert))
			{
				Session[Constants.SessionDoNotShowForPatient] = displayAsAlert;
			}
			return Json(new { Message = 1 }, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Load PatientType Matrix
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="patientTypeId">long patientTypeId</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult LoadPatientTypeMatrix(long customerId, long patientTypeId)
		{
			var patientTypes = new List<long>() { customerId, patientTypeId };
			ControllerHelper.ActionName = Constants.ActionGetPatientTypeMatrix;
			var responseData = ControllerHelper.PostMethodServiceRequest<List<long>, List<PatientTypeMatrixBusinessModel>>(patientTypes);
			if (responseData != null && responseData.Any() && responseData.Where(q => q.customerID == customerId).Any())
			{
				var patientTypeMatrix = responseData.Where(q => q.customerID == customerId).First();
				Session[Constants.SessionPatientTypeMatrix] = patientTypeMatrix;
				//return Json(responseData, JsonRequestBehavior.AllowGet);
				return Json(patientTypeMatrix, JsonRequestBehavior.AllowGet);
			}

			return null;
		}

		/// <summary>
		/// Load Patient Type matrix and save in Session
		/// </summary>
		/// <param name="patientTypeEnum">The patient type enum.</param>
		/// <param name="customerId">The customer identifier.</param>
		/// <returns>
		/// Patient type matrix
		/// </returns>
		public ActionResult LoadPatientTypeMatrixAction(long patientTypeEnum, long customerId)
		{
			return LoadPatientTypeMatrix(customerId, patientTypeEnum);
		}

		/// <summary>
		/// Get Order Activated Session
		/// </summary>
		/// <returns>
		/// Session value of the variable isOrderActivated
		/// </returns>
		public string GetOrderActivatedSession()
		{
			ControllerHelper.ActionName = Constants.ActionGetIsPatientOrderActivated;

			var objPatientBusinessModel = new PatientBusinessModel { PatientId = Convert.ToInt32(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture), UserId = globalUserId };
			Session[Constants.SessionIsOrderActived] = (bool)ControllerHelper.PostMethodServiceRequestObject(objPatientBusinessModel);
			return Convert.ToString(Session[Constants.SessionIsOrderActived], CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Fill Patient Details
		/// </summary>
		/// <param name="patientId">long patientId</param>
		public void FillPatientDetails(long patientId)
		{
			var objPatient = new PatientBusinessModel { PatientId = patientId };
			ControllerHelper.ActionName = Constants.ActionNameRetrievePatientDetails;
			var responseData = ControllerHelper.PostMethodServiceRequest<PatientBusinessModel>(objPatient);

			Session[Constants.SessionPatientId] = responseData.PatientId;
			Session[Constants.SessionCustomerId] = responseData.CustomerId;

			Session[Constants.SessionPatientStatus] = responseData.PatientStatusId;
			Session[Constants.SessionpatientDisplayName] = Convert.ToString(responseData.PatientId + " " + responseData.FirstName + " " + responseData.LastName + " (" + responseData.SAPPatientNumber + ") ", CultureInfo.CurrentCulture);
			Session[Constants.SessionCustomerDisplayName] = Convert.ToString(responseData.CustomerId + " " + responseData.AssignedCustomer + " (" + responseData.SAPCustomerNumber + ") ", CultureInfo.CurrentCulture);

			BindPatientInfoViewModel(responseData);
		}

		/// <summary>
		/// Get Patient Details
		/// </summary>
		/// <param name="patientId">long patientId</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult GetPatientDetails(long patientId)
		{
			// PatientDetails();
			var guidKey = Guid.NewGuid();
			globalObjPatientViewModel.ViewModelPatientInfo = new PatientInfoViewModel();
			globalObjPatientViewModel.ViewModelPatientInfo.InteractionViewModel = new InteractionViewModel();
			if (patientId != 0 && patientId != default(long))
			{
				globalObjPatientInfoViewModel.PatientId = Convert.ToInt64(patientId, CultureInfo.InvariantCulture);

				// Retrieves and binds PatientInfo except dropdowns
				FillPatientDetails(patientId);

				// Retrieves and binds dropdowns in PatientInfo
				FillDropDownData(globalObjSelectedvalue);
				globalObjPatientViewModel.ViewModelPatientInfo = InitializePatientInfoViewModel(globalObjPatientInfoViewModel);

				// Load PatientTypeMatrix
				var patientType = globalObjSelectedvalue.PatientTypeSelectedValue;
				LoadPatientTypeMatrix(Convert.ToInt64(globalObjPatientViewModel.ViewModelPatientInfo.CustomerId, CultureInfo.InvariantCulture), Convert.ToInt64(patientType, CultureInfo.CurrentCulture));
				return Json(globalObjPatientInfoViewModel, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		/// <summary>
		/// Set Click Event In Session
		/// </summary>
		/// <param name="redirectEvent">string redirectEvent</param>
		public void SetClickEventInSession(string redirectEvent)
		{
			Session[Constants.SessionRedirectEvent] = redirectEvent == Convert.ToString(SCAEnums.ClickEvent.OndblClickRow, CultureInfo.CurrentCulture) ? Convert.ToInt64(SCAEnums.ClickEvent.OndblClickRow, CultureInfo.InvariantCulture) : Convert.ToInt64(SCAEnums.ClickEvent.OnCellSelect, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Get Click Event From Session
		/// </summary>
		/// <returns>
		/// Session value of Click event variable
		/// </returns>
		public string GetClickEventFromSession()
		{
			return Convert.ToString(Session[Constants.SessionRedirectEvent], CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Get Redirect Page
		/// </summary>
		/// <returns>
		/// Session value of the variable SessionRedirectFrom
		/// </returns>
		public string GetRedirectPage()
		{
			var redirectPage = Convert.ToString(Session[Constants.SessionRedirectFrom], CultureInfo.CurrentCulture);

			// clear the session 
			Session[Constants.SessionRedirectFrom] = null;
			return redirectPage;
		}

		/// <summary>
		/// Set Redirect Page66
		/// </summary>
		/// <param name="redirectFrom">Page from where Redirected</param>
		public void SetRedirectPage(string redirectFrom)
		{
			Session[Constants.SessionRedirectFrom] = redirectFrom;
		}

		/// <summary>
		/// Load Nurse Details
		/// </summary>
		/// <param name="patientId">string patientId</param>
		/// <returns>
		/// Json Result
		/// </returns>
		public JsonResult LoadNurseDetails(string patientId)
		{
			if (!string.IsNullOrEmpty(patientId))
			{
				ControllerHelper.ActionName = Constants.ActionLoadNurseDetails;
				ControllerHelper.ActionParam = new[] { patientId };
				var nurseDetails = ControllerHelper.GetMethodServiceRequest<NurseCommentsBusinessModel>();
				return Json(nurseDetails, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return null;
			}
		}

		#region Patient and CareHome Interaction

		/// <summary>
		/// Gets the interaction summary.
		/// </summary>
		/// <param name="interactionViewModel">The interaction view model.</param>
		/// <returns>
		/// Returns the interaction summary partial view.
		/// </returns>
		public ActionResult GetInteractionSummary(InteractionViewModel interactionViewModel)
		{
			return this.PartialView(Constants.InteractionSummary, interactionViewModel);
		}

		/// <summary>
		/// Gets the interaction summary details.
		/// </summary>
		/// <param name="interactionViewModel">The interaction view model.</param>
		/// <returns>
		/// Returns the interaction records
		/// </returns>
		public ActionResult GetInteractionSummaryDetails(InteractionViewModel interactionViewModel)
		{
			int homePageAlert = Convert.ToInt32(interactionViewModel.HomePageAlert, CultureInfo.InvariantCulture);
			if (!string.IsNullOrEmpty(interactionViewModel.HomePageAlert))
			{
				interactionViewModel.AssignedTo = this.globalUserId;
			}
			interactionViewModel.UserId = this.globalUserId;
			interactionViewModel = this.patientHelper.GetInteractionSummary(interactionViewModel);
			return this.Json(interactionViewModel, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Opens the interaction.
		/// </summary>
		/// <param name="interactionViewModel">The interaction view model.</param>
		/// <returns>
		/// Returns the open interaction view
		/// </returns>
		[HttpPost]
		public ActionResult OpenInteraction(InteractionViewModel interactionViewModel)
		{
			if (this.userPasswordExpired)
			{
				return this.RedirectToAction("Login", "Account");
			}
			interactionViewModel.SapCarehomeId = Convert.ToString(Session[Constants.SessionCareHomeSAPId], CultureInfo.InvariantCulture);
			interactionViewModel.CarehomeName = !string.IsNullOrEmpty(interactionViewModel.SapCarehomeId) ? Convert.ToString(Session[Constants.SessionCareHomeName], CultureInfo.InvariantCulture) + "(" + interactionViewModel.SapCarehomeId + ")"
																					  : Convert.ToString(Session[Constants.SessionCareHomeName], CultureInfo.InvariantCulture);
			interactionViewModel.User = userLoginName;
			interactionViewModel.UserType = userType;
			interactionViewModel.CustomerName = interactionViewModel.CustomerName;
			interactionViewModel.PatientName = interactionViewModel.PatientName;
			interactionViewModel.UserId = globalUserId;
			interactionViewModel = PatientHelper.OpenInteraction(interactionViewModel);
			interactionViewModel.CustomerName = !string.IsNullOrEmpty(interactionViewModel.SAPCustomerId) ? interactionViewModel.CustomerName.Contains(interactionViewModel.SAPCustomerId) ? interactionViewModel.CustomerName : interactionViewModel.CustomerName + "(" + interactionViewModel.SAPCustomerId + ")" : interactionViewModel.CustomerName;
			interactionViewModel.PatientName = !string.IsNullOrEmpty(interactionViewModel.SAPPatientId) ? interactionViewModel.PatientName.Contains(interactionViewModel.SAPPatientId) ? interactionViewModel.PatientName : interactionViewModel.PatientName + "(" + interactionViewModel.SAPPatientId + ")" : interactionViewModel.PatientName;
			if (!string.IsNullOrEmpty(interactionViewModel.CarehomeId))
			{
				Session[Constants.SessionCareHomeId] = interactionViewModel.CarehomeId;
			}

			return this.PartialView(Constants.CreateInteraction, interactionViewModel);
		}

		/// <summary>
		/// Gets the type of the interaction sub.
		/// </summary>
		/// <param name="interactionTypeId">The interaction type identifier.</param>
		/// <returns>
		/// Gets the interaction Subtype based on interaction type identifier.
		/// </returns>
		[HttpGet]
		public ActionResult GetInteractionSubType(string interactionTypeId)
		{
			if (this.userPasswordExpired)
			{
				return this.RedirectToAction("Login", "Account");
			}

			SelectList interactionSubTypeList = PatientHelper.GetInteractionSubtypeList(interactionTypeId);
			return this.Json(interactionSubTypeList, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Saves the interaction details.
		/// </summary>
		/// <param name="interactionViewModel">The interaction view model.</param>
		/// <returns>
		/// Returns status of save operation.
		/// </returns>
		[HttpPost]
		public ActionResult SaveInteractionDetails(InteractionViewModel interactionViewModel)
		{
			var interactionresponse = this.patientHelper.SaveInteractionDetails(interactionViewModel);
			LogHelper.LogAction(Constants.ActionNameSaveInteractionDetails, Constants.PatientController, Constants.ActionNameSaveInteractionDetails, globalUserId);
			return this.Json(new { interactionresponse }, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Gets the home page flag.
		/// </summary>
		/// <returns></returns>        
		[HttpPost]
		public ActionResult GetHomePageFlag()
		{
			if (Convert.ToBoolean(Session[Constants.IsCalledFromHomePage], CultureInfo.InvariantCulture))
			{
				return this.Json(Url.Action(Constants.ActionWelcome, Constants.HomeController));
			}
			else if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromSearch], CultureInfo.InvariantCulture))
			{
				return this.Json(Url.Action(Constants.ActionSearchData, Constants.SearchController));
			}
			else if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromCareHome], CultureInfo.InvariantCulture))
			{
				return this.Json(Url.Action(Constants.ActionGetCareHome, Constants.CareHomeController));
			}
			else if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromInteractionMenu], CultureInfo.InvariantCulture))
			{
				return this.Json(Url.Action(Constants.ActionNameOpenInteractionMenu, Constants.InteractionController));
			}
			else if (Convert.ToBoolean(Session[Constants.SessionisRedirectFromAuthorisation], CultureInfo.InvariantCulture))
			{
				return this.Json(Url.Action(Constants.ActionPrescription, Constants.AuthorizeController));
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Sets the session for interaction.
		/// </summary>
		/// <param name="interactionViewModel">The interaction view model.</param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult SetSessionForInteraction(InteractionViewModel interactionViewModel)
		{
			if (Convert.ToBoolean(interactionViewModel.HomePageAlert, CultureInfo.InvariantCulture))
			{
				Session[Constants.IsCalledFromHomePage] = true;
			}

			if (interactionViewModel.ParentFormName == Constants.InteractionMenu)
			{
				Session[Constants.SessionIsCalledFromInteractionMenu] = true;
				Session[Constants.SessionIsCalledFromSearch] = false;
				Session[Constants.SessionInteractionPatientName] = interactionViewModel.PatientName;
				Session[Constants.SessionInteractionCareHomeName] = interactionViewModel.CarehomeName;
				Session[Constants.SessionInteractionCustomerName] = interactionViewModel.CustomerName;
				Session[Constants.SessionOpenCarehomeFlag] = interactionViewModel.OpenCarehomeFlag;
				Session[Constants.InteractionId] = interactionViewModel.InteractionId;
			}

			Session[Constants.InteractionId] = interactionViewModel.InteractionId;
			return this.Json(true, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Gets the interaction access.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult GetInteractionAccess()
		{

			// Set 1 = Full Rights, 0 = View Only , blank = No Acess
			string interactionAccess = string.Empty;
			bool patientInteractionFullRights = false;
			bool carehomeInteractionFullRights = false;
			bool patientInteractionViewOnly = false;
			bool carehomeInteractionViewOnly = false;

			List<UserMenuDetailsBusinessModel> allRoleMenuDetails = (List<UserMenuDetailsBusinessModel>)Session[Constants.SessionAuthorizedUrl];

			// Chck for Patient Interaction Access
			var patientInteractionObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.PatientInteraction);

			// Check for Carehome Interaction Access
			var carehomeInteractionObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CareHomeInteraction);

			if (patientInteractionObj != null)
			{
				patientInteractionFullRights = patientInteractionObj.FullRights;
				patientInteractionViewOnly = patientInteractionObj.ViewOnly;
			}

			if (carehomeInteractionObj != null)
			{
				carehomeInteractionFullRights = carehomeInteractionObj.FullRights;
				carehomeInteractionViewOnly = carehomeInteractionObj.ViewOnly;
			}

			if (patientInteractionFullRights || carehomeInteractionFullRights)
			{
				interactionAccess = "1";
			}
			else if (patientInteractionViewOnly || carehomeInteractionViewOnly)
			{
				interactionAccess = "0";
			}

			return this.Json(new
			{
				InteractionAccess = interactionAccess

			}, JsonRequestBehavior.AllowGet);
		}

		//[HttpPost]
		//public ActionResult ClearSessionForInteraction()
		//{
		//    Session[Constants.IsCalledFromHomePage] = "false";
		//    Session[Constants.InteractionId] = Constants.Zero;
		//    return this.Json(true, JsonRequestBehavior.AllowGet);
		//}

		#endregion

		#region GetHierarchyLinkData

		/// <summary>
		/// Gets the hierarchy link data.
		/// </summary>
		/// <param name="customerId">The customer identifier.</param>
		/// <param name="infoId">The information identifier.</param>
		/// <param name="hierarchyId">The hierarchy identifier.</param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult GetHierarchyLinkData(long customerId, string infoId, string hierarchyId)
		{
			if (customerId > 0)
			{
				string userId = globalUserId;
				ControllerHelper.ActionName = Constants.ActionMethodGetHierarchyLinkData;

				// Set the Business Model for Service
				MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel = new MedicalStaffAnalysisInfoBusinessModel()
				{
					CustomerId = customerId,
					UserId = Guid.Parse(userId),
					CustomerMedicalStaffAnalysisInfoId = Convert.ToInt64(infoId, CultureInfo.InvariantCulture),
					HierarchyListId = Convert.ToInt64(hierarchyId, CultureInfo.InvariantCulture)
				};

				var responseData = ControllerHelper.PostMethodServiceRequest<MedicalStaffAnalysisInfoBusinessModel, List<DropDownDataBusinessModel>>(medicalStaffAnalysisInfoBusinessModel);

				if (responseData != null)
				{
					return Json(responseData.ToList());
				}
			}

			return null;
		}

		#endregion


		public DateTime AddWorkingDays(string date, long noOfDays)
		{
			int CountryId = Session[Constants.SessionCustomerParameters] != null ? ((List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters]).First().CountryId : Convert.ToInt32(SCAEnums.Country.UnitedKingdom);
			DateTime dateToAdd = Convert.ToDateTime(date, CultureInfo.InvariantCulture);
			var response = GenericHelper.AddWorkingDays(dateToAdd, noOfDays, CountryId);
			return response.Date;
			//return DateTime.Now;
		}

		public ActionResult GetPrescriptionNDDRelatedDetails(int customerId, int carehomeId, int patientId)
		{
			NddDetailsBusinessModel nddDetailsBusinessModel = new NddDetailsBusinessModel();

            var actionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), patientId.ToString(CultureInfo.InvariantCulture), carehomeId.ToString(CultureInfo.InvariantCulture), this.globalUserId};
            var serviceHelper = new WebServiceHelper(Constants.ActionNameGetPostCodeMatrix, actionParam);
            var postCodeMatrix = serviceHelper.GetMethodServiceRequestForList<List<PostCodeMatrixBusinessModel>>();
            
			if (postCodeMatrix != null && postCodeMatrix.Any())
			{
				nddDetailsBusinessModel.Day1 = postCodeMatrix.First().Monday == true ? 0 : 1;
				nddDetailsBusinessModel.Day2 = postCodeMatrix.First().Tuesday == true ? 0 : 2;
				nddDetailsBusinessModel.Day3 = postCodeMatrix.First().Wednesday == true ? 0 : 3;
				nddDetailsBusinessModel.Day4 = postCodeMatrix.First().Thursday == true ? 0 : 4;
				nddDetailsBusinessModel.Day5 = postCodeMatrix.First().Friday == true ? 0 : 5;
				nddDetailsBusinessModel.RoundID = postCodeMatrix.First().RoundID;
			}
			else
			{
				//to avoid any exception related to no post code matrix for patient or service unable to retrive postcode matrix due to fail call
				nddDetailsBusinessModel.Day1 = 0;
				nddDetailsBusinessModel.Day2 = 0;
				nddDetailsBusinessModel.Day3 = 0;
				nddDetailsBusinessModel.Day4 = 0;
				nddDetailsBusinessModel.Day5 = 0;
				nddDetailsBusinessModel.RoundID = "0";
			}
			return this.Json(nddDetailsBusinessModel, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Get Method for Details page
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public ActionResult CopyDetails(string id)
		{
			if (userPasswordExpired)
			{
				return RedirectToAction("Login", "Account");
			}

			Session[Constants.SessionIsCopyDetails] = true;
			var modelPatient = new PatientViewModel
			{
				ViewModelPatientPrescription = new PatientPrescriptionViewModel(),
				ViewModelPatientInfo = new PatientInfoViewModel() { CreatedBy = userLoginName, CreatedDate = System.DateTime.Today.ToString(Constants.DateFormat, CultureInfo.CurrentCulture), FileCount = ConfigurationManager.AppSettings["SharedLocation"].ToString(CultureInfo.InvariantCulture), CareHomeId = !string.IsNullOrEmpty(id) ? Convert.ToInt64(id, CultureInfo.InvariantCulture) : 0 },
				ViewModelPatientAnalysisMedicalStaff = new PatientAnalysisMedicalStaffViewModel(),
			};

			modelPatient.ViewModelPatientInfo.FileCountErrorMessage = string.Format(CultureInfo.InvariantCulture, Resource.Common.resFileCountErrorMessage, ConfigurationManager.AppSettings["FileCount"].ToString(CultureInfo.InvariantCulture));
			modelPatient.ViewModelPatientInfo.InteractionViewModel = new InteractionViewModel();
			modelPatient.ViewModelPatientInfo.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
			return View(Constants.ActionCopyDetails, modelPatient);
		}


		/// <summary>
		/// Save Status as Removed for Pervious Patient Details
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		public void SaveChangesToPreviousPatient(long patientId, long? customerId)
		{
			var SAPPatientNumber = 0;
			if (!string.IsNullOrEmpty(Session[Constants.SessionSAPPatientNumber].ToString()))
			{
				SAPPatientNumber = Convert.ToInt32(Session[Constants.SessionSAPPatientNumber], CultureInfo.InvariantCulture);
			}
			var objPatient = new PatientBusinessModel
			{
				PatientId = patientId,
				CustomerId = customerId,
				PatientStatusId = (long)SCAEnums.PatientStatus.Removed,
				#region 1.7.5 Uncomment For Sprint 2
				//ReasonCodeId = (long)SCAEnums.RemovedPatientReasonCode.Copied,
				#endregion
				ModifiedBy = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture),
				Remarks = "Copied from - Previous SAP ID " + SAPPatientNumber
			};
			ControllerHelper.ActionName = Constants.ActionUpdatePatientDetails;
			var responseData = ControllerHelper.PostMethodServiceRequestObject(objPatient);
		}

		/// <summary>
		/// Show specific Patient Contact Details For Copy Patient
		/// </summary>
		/// <param name="page">int page</param>
		/// <param name="rows">int rows</param>
		/// <param name="sidx">The sidx.</param>
		/// <param name="sord">The sord.</param>
		/// <param name="contactType">Type of the contact.</param>
		/// <param name="contactTypeId">The contact type identifier.</param>
		/// <returns>
		/// List of the Details of patient's Contact Persons
		/// </returns>       
		public List<PatientContactInfoBusinessModel> GetPatientContactInfoForCopyPatient(int page, int rows, string sidx, string sord, string patientId, string careHomeId, string customerId)
		{
			var patientContactInfoBusinessModel = new PatientContactInfoBusinessModel()
			{
				UserID = globalUserId,
				PatientID = patientId,
				CareHomeId = careHomeId,
				CustomerId = customerId,
				Page = page,
				SortType = sord,
				SortColumn = sidx,
				RowCount = rows
			};

			var totalRecords = 0;
			var totalPages = 0;

			var patientContactInfoData = customerHelper.GetContactInfoDetails(patientContactInfoBusinessModel, out totalRecords, out totalPages);

			return patientContactInfoData;
		}

		/// <summary>
		/// Save patient contact info for Copy Patient
		/// </summary>
		/// <param name="patientContactInfoBusinessModel">The patient contact information business model.</param>
		/// <returns>
		/// Action Result
		/// </returns>        
		public void SaveContactInfoDetailsForCopyPatient(string patientId, string newPatientId)
		{
			List<PatientContactInfoBusinessModel> patientContactInfoBusinessModel = GetPatientContactInfoForCopyPatient(1, 50, "PatientName", "asc", patientId, "", "");
			List<PatientContactInfoBusinessModel> objPatientContactInfo = new List<PatientContactInfoBusinessModel>();

			if (patientContactInfoBusinessModel != null)
			{
				foreach (var data in patientContactInfoBusinessModel)
				{
					var _splitName = data.PatientName.Split(' ');
					data.ContactPersonId = 0;
					data.UserID = globalUserId;
					data.PatientID = newPatientId;
					data.LastName = _splitName[1];
					objPatientContactInfo.Add(data);
				}
				customerHelper.SavePatientContactInfoForCopyPatient(objPatientContactInfo);
			}
			LogHelper.LogAction(Constants.ActionSaveContactInfo, Constants.PatientController, Constants.ActionSaveContactInfo, globalUserId);
		}

		/// <summary>
		/// Save Note Details for Copy Patient
		/// </summary>
		/// <param name="objNoteViewModel">Object of Note ViewModel</param>
		/// <returns>
		/// returns note id
		/// </returns>
		public void SaveCopyNotes(string noteTypeId, long newPatientId, string isAlert)
		{
			//----Getting the notes data of selected patient
			var noteType = Convert.ToInt32(SCAEnums.NoteType.PatientNote, CultureInfo.InvariantCulture);
			ControllerHelper.ActionName = Constants.ActionGetNotes;
			ControllerHelper.ActionParam = new[] { Convert.ToString(noteType), noteTypeId, globalUserId, isAlert };
			var notedata = ControllerHelper.GetMethodServiceRequestForList<List<NotesBusinessModel>>();
			List<NotesBusinessModel> noteDetails = new List<NotesBusinessModel>();
			if (notedata != null)
			{
				var NoteId = new object();
				foreach (var data in notedata)
				{
					if (notedata != null && !string.IsNullOrEmpty(data.FormattedNote))
					{
						var NoteText = data.FormattedNote.Split('>')[2].Replace("\r\n", "");
						ControllerHelper.ActionName = Constants.ActionSaveNoteForCopyPatient;
						var objNotesBusinessModel = new NotesBusinessModel
						{
							NoteId = data.NoteId,
							NoteType = data.NoteType,
							NoteTypeId = newPatientId,
							NoteText = NoteText,
							CreatedBy = globalUserId,
							IsDisplayAsAlert = data.IsDisplayAsAlert,
						};
						noteDetails.Add(objNotesBusinessModel);
					}
				}
				ControllerHelper.PostMethodServiceRequestObject(noteDetails);
				LogHelper.LogAction(Constants.ActionLogSaveNote, Constants.PatientController, Constants.ActionSaveNote, globalUserId);
			}
		}
		/// <summary>
		/// Checking Prescription Product Exist for Customers for Copy Patient
		/// </summary>
		/// <param name="PrescriptionBusinessModel">The patient Prescription information business model.</param>
		/// <returns>
		/// Action Result
		/// </returns>
		[HttpPost]
		public ActionResult CheckPrescriptionProductExistForCustomer(List<PatientPrescriptionViewModel> objPrescriptionDetails, long? patientId, string customerId)
		{
			var productName = new List<string>();

			//---getting the products---
			ControllerHelper.ActionName = Constants.ActionGetProductList;
			ControllerHelper.ActionParam = new string[] { customerId };
			var productData = ControllerHelper.GetMethodServiceRequestForList<List<ProductBusinessModel>>();

			//---getting the prescription products---            
			var prescriptionInfo = new List<PatientPrescriptionViewModel>();
			foreach (var prescriptionData in objPrescriptionDetails) // only not removed product are added
			{
				if (prescriptionData.IsRemoved == false)
				{
					prescriptionInfo.Add(prescriptionData);
				}
			}
			var NotMatchedProduct = (prescriptionInfo != null) ?
										from p in prescriptionInfo
										where !productData.Any(x => x.ProductDisplayID == p.ProductDisplayId)
										select p.ProductDisplayId : null;
			return Json(NotMatchedProduct, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Calls WCF service for NDD calculation when added new product
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult GetPatientNDDForFirstProduct(string patientId)
		{
            var serviceHelper = new WebServiceHelper(Constants.ActionGetPatientNDDForAddProduct, new[] { patientId, this.globalUserId });
            var responseDate = serviceHelper.GetMethodServiceRequest<string>();

			return Json(responseDate, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Calls WCF service for NDD calculation for New Community Patient
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult GetNDDForNewCommunityPatient(string patientId, string patientType, string round, string roundId, string postCode)
		{
			var customerId = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
			var leadTime = string.Empty;
			var countryId = string.Empty;
			if (Session[Constants.SessionCustomerParameters] != null)
			{
				var customerParameters = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
				if (customerParameters != null && customerParameters.Any())
				{
					leadTime = Convert.ToString(customerParameters.First().OrderLeadTime, CultureInfo.InvariantCulture);
					countryId = Convert.ToString(customerParameters.First().CountryId, CultureInfo.InvariantCulture);
				}
			}
			var serviceHelper = new WebServiceHelper(Constants.ActionGetNDDForNewCommunityPatient, new[] { patientId, patientType });

			var nddCalculationBusinessModel = new NDDParametersBusinessModel()
			{
				Round = round,
				RoundID = roundId,
				PostCode = postCode,
				CustomerId = customerId,
				LeadTime = leadTime,
				CountryId = countryId,
				UserId = serviceHelper.CurrentUser.UserId.ToString()
			};						
			var responseDate = serviceHelper.GetMethodServiceRequestObject(nddCalculationBusinessModel);

			return Json(responseDate, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Calls WCF service for NDD calculation for Community Procare patient
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult GetNDDForCommunityProcarePatient(string round, string roundId, string postCode)
		{
			var customerId = Convert.ToString(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
			var leadTime = string.Empty;
			var countryId = string.Empty;
			if (Session[Constants.SessionCustomerParameters] != null)
			{
				var customerParameters = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
				if (customerParameters != null && customerParameters.Any())
				{
					leadTime = Convert.ToString(customerParameters.First().OrderLeadTime, CultureInfo.InvariantCulture);
					countryId = Convert.ToString(customerParameters.First().CountryId, CultureInfo.InvariantCulture);
				}
			}
			var serviceHelper = new WebServiceHelper(Constants.ActionGetNDDForPatient);

			var nddCalculationBusinessModel = new NDDParametersBusinessModel()
			{
				Round = round,
				RoundID = roundId,
				PostCode = postCode,
				CustomerId = customerId,
				LeadTime = leadTime,
				CountryId = countryId,
				UserId = serviceHelper.CurrentUser.UserId.ToString()
			};
			var responseDate = serviceHelper.GetMethodServiceRequestObject(nddCalculationBusinessModel);

			return Json(responseDate, JsonRequestBehavior.AllowGet);
		}
	}
}