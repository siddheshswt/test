﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind Nishad
// Created          : 24-04-2015
// ***********************************************************************
// <copyright file="CommonController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.Resource;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;    
    using System.Web.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    
    /// <summary>
    /// Common Controller
    /// </summary>
    [Authorize]
    public class CommonController : Controller
    {
        // GET: Common
        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Clear Patient session once user land to Master pages e.g. Carehome,Customer,HolidayProcess
        /// </summary>
        /// <returns></returns>
        public ActionResult ClearPatientSession()
        {
            if (Session[Constants.SessionRedirectFrom] == null)
            {
                Session[Constants.SessionSearchParam] = null;
            }

            Session[Constants.SessionPatientId] = null;
            Session[Constants.SessionCustomerId] = null;
            Session[Constants.SessionCustomerName] = null;
            Session[Constants.SessionPatientName] = null;
            Session[Constants.SessionpatientDisplayName] = null;
            Session[Constants.SessionCustomerDisplayName] = null;
            Session[Constants.SessionCareHomeDisplayName] = null;
            Session[Constants.SessionCareHomeName] = null;
            Session[Constants.SessionCareHomeSAPId] = null;
            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            Session[Constants.SessionIsOrderActived] = null;            
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Holiday List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetHolidayList()
        {
            var countryId = 0;
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
            if (customerParameter != null && customerParameter.Count > 0)
            {
                countryId = customerParameter[0].CountryId;
            }
            List<HolidayBusinessModel> HolidayList = GenericHelper.GetHolidayList(countryId);
            return Json(HolidayList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the Note information
        /// </summary>
        /// <param name="page">page number</param>
        /// <param name="rows">no of rows</param>
        /// <param name="noteType">Note Type e.g. PatientNote,Carehome Note,Order Note</param>
        /// <param name="noteTypeId">Id of carehome\patient\order</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetNotes(int page, int rows, string noteType, string noteTypeId, string alertNotes = "false")
        {
            ControllerHelper.ActionName = Constants.ActionGetNotes;
            ControllerHelper.ActionParam = new[] { noteType, noteTypeId, userId, alertNotes };
            var notedata = ControllerHelper.GetMethodServiceRequestForList<List<NotesBusinessModel>>();
            if (notedata != null)
            {
                string noteFrom = "noteFrom";
                string noteFromSyntax = "(noteFrom)";
                foreach (var data in notedata)
                {
                    switch (data.NoteType)
                    {
                        case (long)SCAEnums.NoteType.PatientNote:
                            data.FormattedNote = data.FormattedNote.Replace(noteFrom, Common.resNoteFromPatientInformation).Trim();
                            break;
                        case (long)SCAEnums.NoteType.OrderNote:
                            data.FormattedNote = data.FormattedNote.Replace(noteFrom, Common.resNoteFromOrder).Trim();
                            break;
                        case (long)SCAEnums.NoteType.PrescriptionNote:
                            data.FormattedNote = data.FormattedNote.Replace(noteFrom, Common.resNoteFromPrescription).Trim();
                            break;
                        case (long)SCAEnums.NoteType.SampleOrderNote:
                            data.FormattedNote = data.FormattedNote.Replace(noteFrom, Common.resNoteFromSampleOrder).Trim();
                            break;
                        default:
                            data.FormattedNote = data.FormattedNote.Replace(noteFromSyntax, string.Empty).Trim();
                            break;
                    }
                }
            }
            return this.Json(notedata, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Note Details
        /// </summary>
        /// <param name="objNoteViewModel">Object of Note ViewModel</param>
        /// <returns>
        /// returns note id
        /// </returns>
        [HttpPost]
        public ActionResult AddNotes(NoteViewModel objNoteViewModel)
        {
            if (objNoteViewModel != null && !string.IsNullOrEmpty(objNoteViewModel.NoteText))
            {
                ControllerHelper.ActionName = Constants.ActionSaveNote;
                var objNotesBusinessModel = new NotesBusinessModel
                {
                    NoteId = objNoteViewModel.NoteId,
                    NoteType = objNoteViewModel.NoteType,
                    NoteTypeId = objNoteViewModel.NoteTypeId,
                    NoteText = objNoteViewModel.NoteText,
                    CreatedBy = userId,
                };
                var NoteId = ControllerHelper.PostMethodServiceRequestObject(objNotesBusinessModel);
                LogHelper.LogAction(Constants.ActionLogSaveNote, Constants.PatientController, Constants.ActionSaveNote, userId);
                return this.Json(NoteId, JsonRequestBehavior.AllowGet);
            }
            return null;
        }        

        /// <summary>
        /// Save note alert details
        /// </summary>
        /// <param name="objNoteViewModel">List object of NoteViewModel class</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveAlert(List<NoteViewModel> objNoteViewModel)
        {
            if (objNoteViewModel != null && objNoteViewModel.Any())
            {
                var objNotesBusinessModel = new List<NotesBusinessModel>();
                for (int index = 0; index < objNoteViewModel.Count; index++)
                {
                    var noteViewModel = objNoteViewModel[index];
                    objNotesBusinessModel.Add(new NotesBusinessModel()
                    {
                        NoteId = noteViewModel.NoteId,
                        IsDisplayAsAlert = noteViewModel.IsDisplayAsAlert,
                        ModifiedBy = userId
                    });
                }
                ControllerHelper.ActionName = Constants.ActionSaveAlert;
                var result = ControllerHelper.PostMethodServiceRequestObject(objNotesBusinessModel);
                LogHelper.LogAction(Constants.ActionLogSaveAlert, Constants.CommonController, Constants.ActionSaveAlert, userId);
                return this.Json(result, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="SearchTable">The search table.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns></returns>
        public List<CommonSearchBusinessModel> GetSearchResults(string term, string searchTable, long? customerId)
        {
            var SearchResults = CommonSearchResults(term, searchTable, customerId);
            return SearchResults;
        }

        /// <summary>
        /// Gets the Common search results for customer/patient/carehome/product/user
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="SearchTable">The search table.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns></returns>
        public List<CommonSearchBusinessModel> CommonSearchResults(string term, string SearchTable, long? CustomerId)
        {
            var objSearchRequest = new CommonSearchBusinessModel
            {
                SearchTerm = term,
                TableName = SearchTable,
                CustomerId = Convert.ToInt64(CustomerId, CultureInfo.InvariantCulture),
                UserId = Guid.Parse(userId),
            };
            ControllerHelper.ActionName = Constants.ActionNameGetAuditLogSearchResults;
            var customSearchResults = ControllerHelper.PostMethodServiceRequest<CommonSearchBusinessModel, List<CommonSearchBusinessModel>>(objSearchRequest);
            LogHelper.LogAction(Constants.ActionLogGetAuditLogSearchResults, Constants.AuditLogController, Constants.ActionGetAuditLogSearchResults, userId);
            if (customSearchResults != null)
            {
                return customSearchResults;
            }
            else
            {
                return new List<CommonSearchBusinessModel>();
            }
        }

        /// <summary>
        /// Save Note Details
        /// </summary>
        /// <param name="objNoteViewModel">Object of Note ViewModel</param>
        /// <returns>
        /// returns note id
        /// </returns>
        [HttpPost]
        public void LogException(ExceptionViewModel objExceptionViewModel)
        {
            Exception ajaxException=new Exception(objExceptionViewModel.ErrorMessage);
            ajaxException.Source = "javascript file : " + objExceptionViewModel.JsFileName + " javascript method:" + objExceptionViewModel.JsMethodName + " Status:" + objExceptionViewModel.ErrorStatus;
            LogHelper.LogException(ajaxException, userId);           
        }


    }
}