﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="OrderController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;
    using System.Configuration;
    using System.IO;

    /// <summary>
    /// Order Controller
    /// </summary>
    [Authorize]
    public class OrderController : Controller
    {
        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.CurrentCulture);
        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);
        /// <summary>
        /// The _obj sample selectedvalue
        /// </summary>
        private readonly SampleSelectedvalue objSampleSelectedvalue = new SampleSelectedvalue();
        /// <summary>
        /// The rush NDD
        /// </summary>
        private static DateTime rushNDD;
        /// <summary>
        /// The standard NDD
        /// </summary>
        private static DateTime standardNDD;
        /// <summary>
        /// The arranged NDD
        /// </summary>
        private DateTime arrangedNDD;

        /// <summary>
        /// The automatic order view model
        /// </summary>
        AutomaticOrderViewModel automaticOrderViewModel = new AutomaticOrderViewModel();

        /// <summary>
        /// The Order Helper
        /// </summary>
        OrderHelper orderHelper = new OrderHelper();

        #region List for non selfcare patient type
        long[] selfcarePatient = new long[] {(long)SCAEnums.PatientType.SelfCare,
                                                    (long)SCAEnums.PatientType.ChildSelfCare,
                                                    (long)SCAEnums.PatientType.ContinuingCare,
                                                    (long)SCAEnums.PatientType.NursingSelfcare, 
                                                    (long)SCAEnums.PatientType.ResidentialSelfcare };
        #endregion

        #region List for carehome patient type
        long[] careHomePatient = new long[] {(long)SCAEnums.PatientType.Residential,
                                                    (long)SCAEnums.PatientType.ResidentialSelfcare,
                                                    (long)SCAEnums.PatientType.Nursing,
                                                    (long)SCAEnums.PatientType.NursingSelfcare, 
                                                    (long)SCAEnums.PatientType.Hospital,
                                                    (long)SCAEnums.PatientType.ContinuingCare};

        #endregion


        // GET: Order
        /// <summary>
        /// Communities the orders.
        /// </summary>
        /// <returns></returns>
        public ActionResult CommunityOrders()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }

            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            standardNDD = new DateTime();
            GetNDDDateRangeForPatient(false, false);
            GetPostcodeMatrixInSession();
            return View();
        }

        #region AutomaticOrdersCriteria

        /// <summary>
        /// Automatic Order Get Function
        /// </summary>
        /// <returns></returns>
        public ActionResult AutomaticOrders()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }

            automaticOrderViewModel.CustomersList = GenericHelper.GetCustomersListForDropdown(globalUserId);
            return View(automaticOrderViewModel);
        }

        /// <summary>
        /// Generates the automatic order criteria.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GenerateAutomaticOrderCriteria(AutomaticOrderCriteriaBusinessModel model)
        {
            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }

            ControllerHelper.ActionName = Constants.ActionNameGenerateAutomaticOrderCriteria;
            var result = ControllerHelper.PostMethodServiceRequest<AutomaticOrderCriteriaBusinessModel, List<AutomaticOrderStatusBusinessModel>>(model);
            result.ForEach(a => a.Message = UI.Resource.Common.ResourceManager.GetString("resAOS" + a.Message));

            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Automatics the order criteria export to excel.
        /// </summary>
        /// <param name="gridData">The grid data.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AutomaticOrderCriteriaExportToExcel(List<AutomaticOrderStatusBusinessModel> gridData)
        {
            // Export to excel - start
            var filePath = string.Empty;
            string automaticOrderCriteriaServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.AutomaticOrderCriteriaSharedLocation);
            string fileNamePrefix = Constants.AutomaticOrderCriteriaPrefix;
            var columnsToRemove = new List<string>() { "CustomerId", "PatientId", "PatientTypeId", "CareHomeId", "PatientName" };

            if (gridData.Count > 0)
            {
                filePath = ExcelHelper.ExportToExcel(gridData.ToList(), automaticOrderCriteriaServerDirectory, fileNamePrefix, columnsToRemove);
            }
            // Export to excel - end
            return this.Json(filePath, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// Gets the post code matrix in session.
        /// </summary>
        protected void GetPostcodeMatrixInSession()
        {
            var customerParameter = (List<PostCodeMatrixBusinessModel>)Session[Constants.SessionPostcodeMatrix];
            if (customerParameter != null && customerParameter.Any())
            {
                var firstRecord = customerParameter.First();
                Session[Constants.SessionRoundId] = firstRecord.RoundID;
                Session[Constants.SessionDay1] = firstRecord.Monday;
                Session[Constants.SessionDay2] = firstRecord.Tuesday;
                Session[Constants.SessionDay3] = firstRecord.Wednesday;
                Session[Constants.SessionDay4] = firstRecord.Thursday;
                Session[Constants.SessionDay5] = firstRecord.Friday;
            }
        }

        /// <summary>
        /// Residentials the orders.
        /// </summary>
        /// <returns></returns>
        public ActionResult ResidentialOrders()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }
            if (Session[Constants.SessionCustomerId] == null || Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture) == 0)
            {
                return View();
            }
            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            standardNDD = new DateTime();
            GetNDDDateRangeForPatient(false, false);
            GetPostcodeMatrixInSession();
            return View();
        }

        // GET: Order
        /// <summary>
        /// Products the orders.
        /// </summary>
        /// <returns></returns>
        public ActionResult ProductOrders()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }
            ProductOrdersViewModel objProductOrdersViewModel = new ProductOrdersViewModel();
            if (Session[Constants.SessionCustomerId] == null || Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture) == 0)
            {
                return View(objProductOrdersViewModel);
            }

            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            standardNDD = new DateTime();
            GetNDDDateRangeForPatient(false, true);
            GetPostcodeMatrixInSession();

            var careHomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);
            ControllerHelper.ActionName = Constants.ActionNameGetCareHomeDetailsForProductOrder;
            CareHomeBusinessModel objCareHomeBM = new CareHomeBusinessModel();
            objCareHomeBM.CareHomeId = careHomeId;
            objCareHomeBM.UserId = globalUserId;
            var careHomeDetails = ControllerHelper.PostMethodServiceRequest<CareHomeBusinessModel>(objCareHomeBM);

            if (careHomeDetails != null)
            {
                objProductOrdersViewModel.PurchaseOrderNumber = careHomeDetails.PurchaseOrderNo;
            }
            return View(objProductOrdersViewModel);
        }

        #region Sample Order
        /// <summary>
        /// Loads sample order view
        /// </summary>
        /// <returns>
        /// view sample order
        /// </returns>
        public ActionResult Sample()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            long patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);

            GetSampleOrderViewModel getSampleOrderViewModel = new GetSampleOrderViewModel();
            getSampleOrderViewModel.IsCalledFromEdit = true;
            Session[Constants.SessionSampleFlag] = true;

			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetCountyList);
			var countyList = serviceHelper.GetMethodServiceRequestForList<List<CountyBusinessModel>>();
            if (countyList != null)
            {
                countyList = countyList.OrderBy(x => x.CountyText).ToList();
				getSampleOrderViewModel.Items = new SelectList(countyList, Constants.CountyCode, Constants.CountyText, objSampleSelectedvalue.SampleCountySelectedValue);
			}


			var objPatient = new PatientBusinessModel { PatientId = patientId };
			serviceHelper = new WebServiceHelper(Constants.ActionNameRetrievePatientDetails);
			var responseData = serviceHelper.PostMethodServiceRequest<PatientBusinessModel>(objPatient);
            if (responseData != null)
            {
                getSampleOrderViewModel.Title = responseData.Title;
                getSampleOrderViewModel.FirstName = responseData.FirstName;
                getSampleOrderViewModel.LastName = responseData.LastName;
                getSampleOrderViewModel.Address1 = responseData.Address1;
                getSampleOrderViewModel.Address2 = responseData.Address2;
                getSampleOrderViewModel.TownOrCity = responseData.City;
                getSampleOrderViewModel.PostCode = responseData.PostCode;
                getSampleOrderViewModel.SampleHouseName = responseData.HouseName;
                getSampleOrderViewModel.SampleHouseNumber = responseData.HouseNumber;
                getSampleOrderViewModel.SelectedId = responseData.County;
                getSampleOrderViewModel.CountyText = (from a in countyList
                                                      where a.CountyCode == responseData.County
                                                      select a.CountyText).FirstOrDefault();
            }

            return View("SampleOrder", getSampleOrderViewModel);
        }

        /// <summary>
        /// Load sample order
        /// </summary>
        /// <returns>
        /// sample order view
        /// </returns>
        public ActionResult SampleOrder()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            var getSampleOrderViewModel = new GetSampleOrderViewModel();
            getSampleOrderViewModel.IsCalledFromEdit = false;
            Session[Constants.SessionSampleFlag] = false;

			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetCountyList);
			var countyList = serviceHelper.GetMethodServiceRequestForList<List<CountyBusinessModel>>();
            if (countyList != null && countyList.Any())
            {
                countyList = countyList.OrderBy(x => x.CountyText).ToList();
				getSampleOrderViewModel.Items = new SelectList(countyList, Constants.CountyCode, Constants.CountyText, objSampleSelectedvalue.SampleCountySelectedValue);
            }
            return View("SampleOrder", getSampleOrderViewModel);
        }

        /// <summary>
        /// Check sample order
        /// </summary>
        /// <param name="objSampleOrderPatientBusinessModel">The object sample order patient business model.</param>
        /// <param name="objProductList">The object product list.</param>
        /// <returns>
        /// response data
        /// </returns>
        [HttpPost]
        public ActionResult CheckSampleOrders(SampleOrderPatientBusinessModel objSampleOrderPatientBusinessModel, List<SampleProductBusinessModel> objProductList)
        {			
			
            if (objSampleOrderPatientBusinessModel != null)
            {
                objSampleOrderPatientBusinessModel.UserId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);
                objSampleOrderPatientBusinessModel.SampleProductBusinessModel = objProductList;
                objSampleOrderPatientBusinessModel.PatientID = Convert.ToInt32(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
                objSampleOrderPatientBusinessModel.IsCalledFromEdit = Convert.ToBoolean(Session[Constants.SessionSampleFlag], CultureInfo.InvariantCulture);
            }
            if (objSampleOrderPatientBusinessModel.YesToSave)
            {
                // Send order
                ControllerHelper.ActionName = Constants.ActionNameSendSampleOrders;
            }
            else
            {
                // Check Data in the Helix
                ControllerHelper.ActionName = Constants.ActionNameCheckSampleOrdersInHelix;
            }

            var responseData = ControllerHelper.PostMethodServiceRequest<SampleOrderPatientBusinessModel>(objSampleOrderPatientBusinessModel);

            LogHelper.LogAction(Constants.ActionNameCheckSampleOrdersInHelix, Constants.OrderController, Constants.ActionNameCheckSampleOrdersInHelix, ControllerHelper.CurrentUser.UserId.ToString());

            return this.Json(responseData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// save sample order
        /// </summary>
        /// <param name="objSampleOrderPatientBusinessModel">The object sample order patient business model.</param>
        /// <param name="objProductList">The object product list.</param>
        /// <param name="objHelixId">The object helix identifier.</param>
        /// <returns>
        /// response data
        /// </returns>
        [HttpPost]
        public ActionResult SaveSampleOrderDetails(SampleOrderPatientBusinessModel objSampleOrderPatientBusinessModel, List<SampleProductBusinessModel> objProductList, string objHelixId)
        {
        
			if (objSampleOrderPatientBusinessModel != null)
            {
				var customerParameter = Session[Constants.SessionCustomerParameters] != null ? ((List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters]).First() : null;

				int CountryId = customerParameter != null ? customerParameter.CountryId : Convert.ToInt32(SCAEnums.Country.UnitedKingdom, CultureInfo.InvariantCulture);
				long CustomerID = customerParameter != null ? customerParameter.CustomerID : 0;

				objSampleOrderPatientBusinessModel.SampleOrderProductType = customerParameter != null ? customerParameter.SampleOrderProductType : "TENA";				
                objSampleOrderPatientBusinessModel.UserId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture);
                objSampleOrderPatientBusinessModel.SampleProductBusinessModel = objProductList;
                objSampleOrderPatientBusinessModel.PatientID = Convert.ToInt32(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
                objSampleOrderPatientBusinessModel.IsCalledFromEdit = Convert.ToBoolean(Session[Constants.SessionSampleFlag], CultureInfo.InvariantCulture);
                objSampleOrderPatientBusinessModel.HelixOrderID = Convert.ToString(objHelixId, CultureInfo.InvariantCulture);
                objSampleOrderPatientBusinessModel.NextDeliveryDate = Convert.ToString(AddWorkingDays(DateTime.Today, Constants.SampleOrderWorkingDays, CountryId), CultureInfo.InvariantCulture);
                objSampleOrderPatientBusinessModel.CustomerID = CustomerID;
            }

            // Save Data in Orders, IDXOrderProduct
            ControllerHelper.ActionName = Constants.ActionNameSaveSampleOrders;
            var responseData = ControllerHelper.PostMethodServiceRequestObject(objSampleOrderPatientBusinessModel);
            LogHelper.LogAction(Constants.ActionSaveSampleOrderFailed, Constants.AccountController, Constants.ActionSaveSampleOrderDetails, ControllerHelper.CurrentUser.UserId.ToString());
            return Json(responseData);
        }

        /// <summary>
        /// this function will use to generate sample letter
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">object of SampleOrderPatientBusinessModel class</param>
        /// <returns></returns>
        public ActionResult GenerateSampleLetter(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel) 
        {
            string filepath = string.Empty;
            if (sampleOrderPatientBusinessModel != null)
            {
                string serverDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.SampleOrderSharedLocation);
                string fileNamePrefix = Constants.SampleLetterPrefix;
                filepath = WordHelper.CreateWordFile(sampleOrderPatientBusinessModel, serverDirectory, fileNamePrefix);
                return Json(filepath, JsonRequestBehavior.AllowGet);
            }
            return Json(filepath, JsonRequestBehavior.AllowGet);
        }
        #endregion

        /// <summary>
        /// Gets the NDD date range for patient.
        /// </summary>
        /// <param name="isOneOff">if set to <c>true</c> [is one off].</param>
        public void GetNDDDateRangeForPatient(bool isOneOff, bool isProductOrder)
        {
            GetNDDDateRangeForPatient(false, isOneOff, Convert.ToString(isProductOrder, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Gets the NDD date range for patient.
        /// </summary>
        /// <param name="isCustomerLevel">if set to <c>true</c> [is customer level].</param>
        /// <param name="isOneOff">if set to <c>true</c> [is one off].</param>
        public void GetNDDDateRangeForPatient(bool isCustomerLevel, bool isOneOff, string isProductOrder)
        {
            //This is common menthod for four scenarios.
            //1. Select Carehome --> Select Carehome order activation
            //2. Select Carehome Patient --> Select Carehome order activation
            //3. Select non carehome patient --> Select Community order activation
            //4. Select patient/carehome/carehome patient --> Select one-off order activation

            #region get id's from session
            var patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);  // 0 if carehome selected
            var customerId = Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
            var careHomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);
            #endregion

            #region Get post code matrix  for Carehome/Carehome patient/non carehome patient

            ControllerHelper.ActionName = Constants.ActionNameGetPostCodeMatrix;
            ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), patientId.ToString(CultureInfo.InvariantCulture), careHomeId.ToString(CultureInfo.InvariantCulture), ControllerHelper.CurrentUser.UserId.ToString() };
            var postCodeMatrix = ControllerHelper.GetMethodServiceRequestForList<List<PostCodeMatrixBusinessModel>>();
            if (postCodeMatrix != null && postCodeMatrix.Any())
            {
                Session[Constants.SessionPostcodeMatrix] = postCodeMatrix;
            }
            else
            {
                //to avoid any exception related to no post code matrix for patient or service unable to retrive postcode matrix due to fail call
                var dummyPostcodeMatrix = new List<PostCodeMatrixBusinessModel>();
                Session[Constants.SessionPostcodeMatrix] = dummyPostcodeMatrix.Select(q => new PostCodeMatrixBusinessModel
                                                            {
                                                                CustomerID = customerId,
                                                                PostcodeMatrixID = 0,
                                                                Monday = true,
                                                                Tuesday = true,
                                                                Wednesday = true,
                                                                Thursday = true,
                                                                Friday = true,
                                                                RoundID = "0",
                                                                Round = "0",
                                                                Postcode = "0"
                                                            }).ToList();

            }
            #endregion

            //Get patient/carehome details            
            if ((patientId != 0 || careHomeId != 0) && (standardNDD == null || standardNDD == (new DateTime())))
            {
                ControllerHelper.ActionName = Constants.ActionNameGetPatientBasicDetails;
                if (!isCustomerLevel)
                {
                    customerId = 0;
                }

                //If carehome or carehome patient, get carehome details
                //If non carehome patient, get patient details
                if (isOneOff && isProductOrder.ToLower() != "true")
                {
                    ControllerHelper.ActionParam = new[] { patientId.ToString(CultureInfo.InvariantCulture), "0", Convert.ToString(isProductOrder, CultureInfo.InvariantCulture), ControllerHelper.CurrentUser.UserId.ToString() };
                }
                else
                {
                    ControllerHelper.ActionParam = new[] { patientId.ToString(CultureInfo.InvariantCulture), careHomeId.ToString(CultureInfo.InvariantCulture), Convert.ToString(isProductOrder, CultureInfo.InvariantCulture), ControllerHelper.CurrentUser.UserId.ToString() };
                }
                var responseData = ControllerHelper.GetMethodServiceRequestForList<List<PatientBusinessModel>>();
                if (responseData != null && responseData.Any())
                {
                    var patientInfo = responseData.First();
                    var patientTypes = responseData.Select(q => Convert.ToInt64(q.PatientTypeId, CultureInfo.InvariantCulture)).Distinct().ToList();
                    var changeNDDbyXDays = GetChangeNDDbyXDays(patientTypes);//Get changeNDDbyXDays for selected carehome

                    //If carehome or carehome patient then NextDeliveryDate will be carehomes NDD
                    //If non carehome patient, then NextDeliveryDate will be patients NDD
                    var nextDeliveryDate = DateTime.ParseExact(patientInfo.NextDeliveryDate, Constants.DateFormat, CultureInfo.InvariantCulture);

                    //If one-off NDD will be always current date
                    if (isOneOff && !Convert.ToBoolean(isProductOrder, CultureInfo.InvariantCulture)) //community one off
                    {
                        nextDeliveryDate = DateTime.Today; // Setting to Today for OneOff, so on CalCulateNextDeliverydate , it returns DateTime.Today + OrderLeadTime
                    }

                    nextDeliveryDate = CalculateNextDeliveryDate(nextDeliveryDate); //calculate NDD for carehome
                    Session[Constants.SessionCareHomeStatus] = patientInfo.CareHomeStatus;
                    Session[Constants.IsCheckCarehomeStatus] = false;
                    Session[Constants.SessionCareHomeOrderType] = patientInfo.CareHomeOrderType;
                    var position = Array.IndexOf(selfcarePatient, Convert.ToInt64(patientTypes[0]));
                    var advanceOrderActivationDays = GetAdvanceOrderActivationDate(patientTypes);

                    if (nextDeliveryDate != default(DateTime))
                    {
                        Session[Constants.SessionCanActivateOrder] = true;
                        Session[Constants.SessionDateForOrderActivation] = DateTime.Today.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                        if (position > -1)
                        {
                            Session[Constants.SessionDateForOrderActivation] = nextDeliveryDate.AddDays(0 - advanceOrderActivationDays).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                            Session[Constants.SessionCanActivateOrder] = DateTime.Today >= nextDeliveryDate.AddDays(0 - advanceOrderActivationDays) ? true : false;
                        }

                        GetNDDStartEndDateForCalendar(nextDeliveryDate, changeNDDbyXDays); //calculate start date for selected carehome
                        Session[Constants.SessionPatientNDD] = patientInfo.NextDeliveryDate; // This is to validate the patient NDD. Not the calculated NDD
                        Session[Constants.SessionPatientType] = patientInfo.PatientTypeId;
                        Session[Constants.SessionHasActiveOrder] = patientInfo.AnyActiveOrder;
                        Session[Constants.SessionIsOrderActived] = patientInfo.AnyActiveOrder;
                        Session[Constants.SessionNextDeliveryDate] = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                    }
                }
            }
        }

        /// <summary>
        /// CheckforOrderActivation
        /// </summary>
        /// <param name="DateForOrderActivation">The date for order activation.</param>
        /// <returns></returns>
        public bool CheckforOrderActivation(DateTime DateForOrderActivation)
        {
            return DateTime.Today >= DateForOrderActivation ? true : false;
        }

        #region One-Off Order
        /// <summary>
        /// Called when [off orders].
        /// </summary>
        /// <returns></returns>
        public ActionResult OneOffOrders()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }
            string defaultCutOffTime = "15:00";
            var carehomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);
            var patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
            Session[Constants.SessionOrderId] = null;
            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            standardNDD = new DateTime();
            if (carehomeId != 0 && patientId == 0)
            {
                GetNDDDateRangeForPatient(true, true);
            }
            else
            {
                GetNDDDateRangeForPatient(true, false);
            }
            Session[Constants.SessionOneOffNDD] = Session[Constants.SessionNDDEndDate];

            var cutOffTime = Session[Constants.SessionCustomerParameters] != null ? ((List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters]).FirstOrDefault().OrderCutOffTime : defaultCutOffTime;
            Session[Constants.SessionCutOffTime] = null;
            Session[Constants.SessionCutOffTime] = orderHelper.ConvertTimeToUkTimezone(cutOffTime);
            Session[Constants.SessionIsLoadPrescription] = true;
            return View();
        }

        /// <summary>
        /// Get Community Activated Orders
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <returns>
        /// List of Community Activated orders.
        /// </returns>
        public JsonResult GetOneOffOrderActivated(bool isRushOrder)
        {
            var isRush = "FALSE";
            if (isRushOrder)
            {
                isRush = "TRUE";
            }

            var carehomeId = Convert.ToString(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);
            var patientId = Convert.ToString(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
            var isProductOneOff = !string.IsNullOrEmpty(carehomeId) && string.IsNullOrEmpty(patientId) ? "TRUE" : "FALSE";
            ControllerHelper.ActionName = Constants.ActionGetActivatedOrders;
            ControllerHelper.ActionParam = new[] { string.IsNullOrEmpty(patientId) ? "0" : patientId, string.IsNullOrEmpty(carehomeId) ? "0" : carehomeId, globalUserId, isRush, isProductOneOff, "TRUE" };

            long orderStatus = 0;
            var gridData = new List<PatientOrdersBusinessModel>();
            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<PatientOrdersBusinessModel>>();
            DateTime nextDeliveryDate = new DateTime();
            if (responseData != null && responseData.Any())
            {
                var firstRecord = responseData.First();
                nextDeliveryDate = string.IsNullOrEmpty(firstRecord.NextDeliveryDate) ? new DateTime() : DateTime.ParseExact(firstRecord.NextDeliveryDate, Constants.DateFormat, CultureInfo.InvariantCulture);


                if (nextDeliveryDate != default(DateTime))
                {
                    Session[Constants.SessionCanActivateOrder] = true;
                    Session[Constants.SessionDateForOrderActivation] = DateTime.Today.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                }
                Session[Constants.SessionOrderId] = firstRecord.OrderId;
                orderStatus = firstRecord.OrderStatus;

                LogHelper.LogAction("Loading One Off Activated Orders", Constants.OrderController, Constants.ActionGetActivatedOrders, globalUserId);
                gridData = responseData.ToList();
            }
            else
            {
                Session[Constants.SessionOrderId] = null;
            }
            bool isSentToSAP = orderStatus == (long)SCAEnums.OrderStatus.SentToSAP ? true : false;

            var jsonData = new
            {
                isSentToSAP = isSentToSAP,
                rows = gridData,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the NDD for standard one off orders.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetNDDForStandardOneOffOrders()
        {
            var patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
            var carehomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);

            if ((standardNDD == null || standardNDD == (new DateTime())) || Session[Constants.SessionOneOffNDD] != null)
            {
                if (carehomeId != 0 && patientId == 0)
                {
                    // get NDD for carehome one off 
                    GetNDDDateRangeForPatient(true, true);
                }
                else
                {
                    if (patientId != 0) //get NDD for community one off
                        GetNDDDateRangeForPatient(true, false);
                }

                if (Session[Constants.SessionNextDeliveryDate] != null)
                {
                    standardNDD = DateTime.ParseExact(Convert.ToString(Session[Constants.SessionNextDeliveryDate], CultureInfo.InvariantCulture), Constants.DateFormat, CultureInfo.InvariantCulture);

                    var jsonData = new
                    {
                        NDD = standardNDD.ToString(Constants.DateFormat, CultureInfo.InvariantCulture)
                    };

                    Session[Constants.SessionOneOffNDD] = standardNDD.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                    return Json(jsonData);
                }

            }

            return null;
        }

        /// <summary>
        /// Get Next Delivery Day for Rush One off orders depending upon the cut off time
        /// </summary>
        /// <returns>
        /// Next Delivery Date for the One off Rush Orders
        /// </returns>
        public JsonResult GetNDDForRushOneOffOrders()
        {
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
            if (customerParameter != null && customerParameter.Any())
            {
                var orderCutOffTime = customerParameter.Select(q => q.OrderCutOffTime).First();
                var cutoffTimeSpan = TimeSpan.Parse(orderCutOffTime, CultureInfo.InvariantCulture);
                if (cutoffTimeSpan >= DateTime.Now.TimeOfDay)
                {
                    //Add 1 day
                    rushNDD = DateTime.Today.AddDays(1);
                    rushNDD = orderHelper.GetNDDAfterWeekendsOrHoliday(rushNDD, customerParameter.First().CountryId);
                }
                else
                {
                    //Add 2 days
                    int dayCount = 1;
                    rushNDD = DateTime.Today;
                    while (dayCount < 3)
                    {
                        rushNDD = rushNDD.AddDays(1);
                        rushNDD = orderHelper.GetNDDAfterWeekendsOrHoliday(rushNDD, customerParameter.First().CountryId);
                        dayCount++;
                    }
                }
            }
            Session[Constants.SessionDerivedNDD] = rushNDD.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
            Session[Constants.SessionOneOffNDD] = rushNDD.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
            return Json(rushNDD.ToString(Constants.DateFormat, CultureInfo.InvariantCulture));
        }

        #endregion

        #region CommunityOrders
        /// <summary>
        /// Get Community Activated Orders
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <returns>
        /// List of Community Activated orders.
        /// </returns>
        public ActionResult GetCommunityActivated(int page, int rows, bool isProductOrder = false)
        {
            Session[Constants.SessionOrderId] = null;
            if (!this.ModelState.IsValid)
            {
                return View();
            }

            if (Session[Constants.SessionPatientId] == null || Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture) == 0)
            {
                return View();
            }

            long patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);

            #region check for existing activated order, if true get details else create new order
            bool isActivatedOrder = false;
            if (Session[Constants.SessionHasActiveOrder] != null)
            {
                isActivatedOrder = (bool)Session[Constants.SessionHasActiveOrder];
            }

            if (isActivatedOrder == true)
            {
                ControllerHelper.ActionName = Constants.ActionGetActivatedOrders;
                ControllerHelper.ActionParam = new[] { patientId.ToString(CultureInfo.InvariantCulture), "0", globalUserId, "FALSE", Convert.ToString(isProductOrder, CultureInfo.InvariantCulture), "FALSE" };
            }
            else
            {
                ControllerHelper.ActionName = Constants.ActionGetCommunityActivated;
                ControllerHelper.ActionParam = new[] { patientId.ToString(CultureInfo.InvariantCulture), globalUserId };
            }          
            #endregion

            int totalRecords = 0;
            var totalPages = 0;
            long orderStatus = 0;
            var gridData = new List<PatientOrdersBusinessModel>();
            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<PatientOrdersBusinessModel>>();
            if (responseData != null && responseData.Any())
            {
                //1. Check the type of selected patient
                //2. Check if selfcae patient of carehome                
                //3. Check for advanceOrderActivationDays                
                //4. check for GetChangeNDDbyXDays
                var patientTypes = responseData.Select(q => Convert.ToInt64(q.PatientTypeId, CultureInfo.InvariantCulture)).Distinct().ToList();
                var position = Array.IndexOf(selfcarePatient, Convert.ToInt64(patientTypes[0], CultureInfo.InvariantCulture));
                var advanceOrderActivationDays = GetAdvanceOrderActivationDate(patientTypes);
                var changeNDDbyXDays = GetChangeNDDbyXDays(patientTypes);
                var firstRecord = responseData.First();
                var nextDeliveryDate = string.IsNullOrEmpty(firstRecord.NextDeliveryDate) ? new DateTime() : DateTime.ParseExact(firstRecord.NextDeliveryDate, Constants.DateFormat, CultureInfo.InvariantCulture);
                nextDeliveryDate = CalculateNextDeliveryDate(nextDeliveryDate); //calculate NDD for new community order

                if (nextDeliveryDate != default(DateTime))
                {
                    Session[Constants.SessionCanActivateOrder] = true;
                    Session[Constants.SessionDateForOrderActivation] = DateTime.Today.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                    if (position > -1)
                    {
                        Session[Constants.SessionDateForOrderActivation] = nextDeliveryDate.AddDays(0 - advanceOrderActivationDays).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                        Session[Constants.SessionCanActivateOrder] = DateTime.Today >= nextDeliveryDate.AddDays(0 - advanceOrderActivationDays) ? true : false;
                    }
                    GetNDDStartEndDateForCalendar(nextDeliveryDate, changeNDDbyXDays); //calculate start date for selected non-carehome patient
                }

                if (!isActivatedOrder)
                {
                    responseData.ForEach(a => { a.DeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture); a.NextDeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture); });
                    Session[Constants.SessionNextDeliveryDate] = string.Empty;
                    Session[Constants.SessionNextDeliveryDate] = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                }
                else
                {
                    Session[Constants.SessionNextDeliveryDate] = firstRecord.NextDeliveryDate;
                    Session[Constants.SessionOrderId] = firstRecord.OrderId;
                }

                orderStatus = firstRecord.OrderStatus;

                LogHelper.LogAction(Constants.ActionLoadCustomers, Constants.PatientController, Constants.ActionNameGetCommunityOrders, globalUserId);
                gridData = responseData.ToList();

                totalRecords = responseData.Count;
                totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
            }

            bool isSentToSAP = orderStatus == (long)SCAEnums.OrderStatus.SentToSAP ? true : false;

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                isSentToSAP = isSentToSAP,
                rows = gridData,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// Save Action result for Community and resedential orders
        /// </summary>
        /// <param name="orderList">The order list.</param>
        /// <returns>
        /// True if the order is saved.
        /// </returns>
        [HttpPost]
        public ActionResult SaveOrder(List<PatientOrdersBusinessModel> orderList)
        {
            if (orderList != null && orderList.Any())
            {
                var patientOrdersBusinessModel = orderList.First();
                var DerivedNDD = string.Empty;
                if (Session[Constants.SessionDerivedNDD] != null)
                {
                    DerivedNDD = Convert.ToString(Session[Constants.SessionDerivedNDD], CultureInfo.InvariantCulture);
                }
                else
                {
                    DerivedNDD = patientOrdersBusinessModel.DeliveryDate;
                }
                var orderId = Convert.ToInt64(Session[Constants.SessionOrderId], CultureInfo.InvariantCulture);
                var customerId = Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture);
                var careHomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);

                bool isActivatedOrder = false;
                if (Session[Constants.SessionHasActiveOrder] != null)
                {
                    isActivatedOrder = (bool)Session[Constants.SessionHasActiveOrder];
                }
                orderList.ForEach(q => q.IsActivatedOrder = isActivatedOrder);

				WebServiceHelper serviceHelper = new WebServiceHelper(Constants.ActionNameSaveOrders);
				var currentUserId = serviceHelper.CurrentUser.UserId;
                if (orderList.First().PatientID == 0 && !orderList.First().IsProductLevel)
                {
                    // Selfcare community order
                    var patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
					orderList.ForEach(o => { o.ModifiedBy = currentUserId; o.PatientID = patientId; o.OrderId = orderId; o.DerivedNDD = DerivedNDD; o.CustomerID = customerId; o.CareHomeID = (careHomeId == 0 ? (long?)null : careHomeId); });
                }
                else
                {
                    // Selfcare carehome order
					orderList.ForEach(o => { o.ModifiedBy = currentUserId; o.DerivedNDD = DerivedNDD; o.CustomerID = customerId; });
                }

				
				var responseData = (bool)serviceHelper.PostMethodServiceRequestObject(orderList);
                if (responseData)
                {
					LogHelper.LogAction(Constants.ActionSaveOrderSuccess, Constants.AccountController, Constants.ActionSaveCommunityOrder, currentUserId.ToString());
                    Session[Constants.SessionHasActiveOrder] = true;

                    if (!orderList.Where(q => q.OrderType == (long)SCAEnums.OrderType.ZHDF || q.OrderType == (long)SCAEnums.OrderType.ZHDR).Any())
                    {
                        Session[Constants.SessionIsOrderActived] = "True";
                    }

                    return Json(true);
                }

				LogHelper.LogAction(Constants.ActionSaveOrderFailed, Constants.AccountController, Constants.ActionSaveCommunityOrder, currentUserId.ToString());
            }

            return Json(false);
        }

       

        /// <summary>
        /// Adds the working days.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="noOfDays">The no of days.</param>
        /// <param name="countryId">Holiday as per country id</param>
        /// <returns></returns>
        protected DateTime AddWorkingDays(DateTime date, long noOfDays, int countryId)
        {
            var days = noOfDays;

            while (days != 0)
            {
                date = date.AddDays(1);
                days--;

                while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || GenericHelper.IsHoliday(date, countryId))
                    date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// Calculate the next delivery date and upadte the customer parameter related sessin parameters
        /// </summary>
        /// <param name="deliveryDate">The delivery date.</param>
        /// <returns>
        /// Next Delivery Date.
        /// </returns>
        protected DateTime CalculateNextDeliveryDate(DateTime deliveryDate)
        {
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
            var nextDeliveryDate = new DateTime();
            if (customerParameter != null && customerParameter.Any())
            {
                var firstRecord = customerParameter.First();
                if (deliveryDate < AddWorkingDays(DateTime.Today, firstRecord.OrderLeadTime, firstRecord.CountryId))
                {
                    nextDeliveryDate = AddWorkingDays(DateTime.Today, firstRecord.OrderLeadTime, firstRecord.CountryId);
                }
                else
                {
                    nextDeliveryDate = deliveryDate;
                }

                if (nextDeliveryDate == new DateTime())
                {
                    nextDeliveryDate = DateTime.Today.AddDays(1);
                }

                nextDeliveryDate = orderHelper.GetNextWorkingDay(nextDeliveryDate, firstRecord.CountryId);
                nextDeliveryDate = ValidatePostcodeMatrix(nextDeliveryDate, firstRecord.CountryId);

                Session[Constants.SessionAllowIncreaseOrderQty] = firstRecord.AllowIncreaseOrderQty;

                // Session[Constants.SessionAddFromPrescription] = firstRecord.AddFromPrescription;
                // Session[Constants.SessionAddFromProduct] = firstRecord.AddFromProduct;
                Session[Constants.SessionOrderCreationAllowed] = firstRecord.OrderCreationAllowed;
            }

            return nextDeliveryDate;
        }

        /// <summary>
        /// Validates the postcode matrix.
        /// </summary>
        /// <param name="nextDeliveryDate">The next delivery date.</param>
        /// <returns></returns>
        private DateTime ValidatePostcodeMatrix(DateTime nextDeliveryDate, int countryId)
        {
            if (Session[Constants.SessionPostcodeMatrix] != null)
            {
                var postCodeMatrixList = (List<PostCodeMatrixBusinessModel>)Session[Constants.SessionPostcodeMatrix];
                if (postCodeMatrixList.Any())
                {
                    var postCodeMatrix = postCodeMatrixList.First();

                    //check if all days are false
                    if (postCodeMatrix.Monday == false &&
                        postCodeMatrix.Tuesday == false &&
                        postCodeMatrix.Wednesday == false &&
                            postCodeMatrix.Thursday == false &&
                                postCodeMatrix.Friday == false
                        )
                    {
                        Session[Constants.SessionDerivedNDD] = nextDeliveryDate.Date.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);

                        var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(nextDeliveryDate, postCodeMatrix.CustomerID);
                        //Whenever derived date is a bank holiday & 
                        //an arranged date is found in Holiday Process table then use next available working date as a delivery date
                        if (isArrangedDateFound)
                        {
                            nextDeliveryDate = arrangedNDD;
                        }
                    }
                    else
                    {
                        var dayOfWeek = nextDeliveryDate.DayOfWeek;
                        var isValidDate = false;
                        while (!isValidDate)
                        {
                            isValidDate = true;
                            if (dayOfWeek == DayOfWeek.Monday && postCodeMatrix.Monday == false)
                            {
                                isValidDate = false;
                                nextDeliveryDate = nextDeliveryDate.AddDays(1);
                            }

                            if (dayOfWeek == DayOfWeek.Tuesday && postCodeMatrix.Tuesday == false)
                            {
                                isValidDate = false;
                                nextDeliveryDate = nextDeliveryDate.AddDays(1);
                            }

                            if (dayOfWeek == DayOfWeek.Wednesday && postCodeMatrix.Wednesday == false)
                            {
                                isValidDate = false;
                                nextDeliveryDate = nextDeliveryDate.AddDays(1);
                            }

                            if (dayOfWeek == DayOfWeek.Thursday && postCodeMatrix.Thursday == false)
                            {
                                isValidDate = false;
                                nextDeliveryDate = nextDeliveryDate.AddDays(1);
                            }

                            if (dayOfWeek == DayOfWeek.Friday && postCodeMatrix.Friday == false)
                            {
                                isValidDate = false;
                                nextDeliveryDate = nextDeliveryDate.AddDays(1);
                            }

                            if (!isValidDate)
                            {
                                nextDeliveryDate = orderHelper.GetNextWorkingDay(nextDeliveryDate, countryId);
                                //holiday table is not required
                                //if (GenericHelper.IsHoliday(nextDeliveryDate))
                                //{
                                //    nextDeliveryDate = nextDeliveryDate.AddDays(1);
                                //}
                                dayOfWeek = nextDeliveryDate.DayOfWeek;
                            }
                        }
                        nextDeliveryDate = orderHelper.GetNextWorkingDay(nextDeliveryDate, countryId);
                        Session[Constants.SessionDerivedNDD] = nextDeliveryDate.Date.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                        var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(nextDeliveryDate, postCodeMatrix.CustomerID);
                        //Whenever derived date is a bank holiday & 
                        //an arranged date is found in Holiday Process table then use next available working date as a delivery date
                        if (isArrangedDateFound)
                        {
                            nextDeliveryDate = arrangedNDD;
                        }
                    }

                }
            }

            return nextDeliveryDate;
        }

        /// <summary>
        /// Checks the date availability in holiday process.
        /// </summary>
        /// <param name="nextDeliveryDate">The next delivery date.</param>
        /// <param name="CustomerID">The customer identifier.</param>
        /// <returns></returns>
        private bool CheckDateAvailabilityInHolidayProcess(DateTime nextDeliveryDate, long CustomerID)
        {
            var objHolidayBusinessModel = new HolidayProcessAdminBusinessModels();
            objHolidayBusinessModel.CustomerId = CustomerID;
            objHolidayBusinessModel.DerivedNDD = Convert.ToString(nextDeliveryDate, CultureInfo.InvariantCulture);
            objHolidayBusinessModel.OrderCreationTime = Convert.ToString(DateTime.Now.ToString(Constants.TimeFormat3, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            objHolidayBusinessModel.UserId = ControllerHelper.CurrentUser.UserId;
            ControllerHelper.ActionName = Constants.ActionNameGetArrangedDeliveryDate;
            var strArrangedDate = (string)ControllerHelper.PostMethodServiceRequestObject(objHolidayBusinessModel);
            if (strArrangedDate != null)
            {
                arrangedNDD = Convert.ToDateTime(strArrangedDate, CultureInfo.InvariantCulture);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the residential activated.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="CustomerParameter">The customer parameter.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetResidentialActivated(int page, int rows, string sidx, string sord, object CustomerParameter, bool isProductOrder = false)
        {
            Session[Constants.SessionOrderId] = null;

            if (!this.ModelState.IsValid)
            {
                return View();
            }

            #region Get carehome and patient id from session
            var careHomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);
            long patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
            var patientOrdersBusinessModel = new PatientOrdersBusinessModel()
            {
                Page = page,
                SortType = sord,
                SortColumn = sidx,
                RowCount = rows,
                PatientID = patientId,
                CareHomeID = careHomeId,
                UserId = globalUserId,
                IsProductLevel = isProductOrder
            };
            #endregion

            int totalRecords = 0;
            var totalPages = 0;
            var responseData = new List<PatientOrdersBusinessModel>();

            #region check for existing activated order, if true get details else create new order
            bool isActivatedOrder = false;
            if (Session[Constants.SessionHasActiveOrder] != null)
            {
                isActivatedOrder = (bool)Session[Constants.SessionHasActiveOrder];
            }

            if (isActivatedOrder == true && isProductOrder)
            {
                responseData = orderHelper.GetActivatedOrders(patientOrdersBusinessModel, out totalRecords, out totalPages);
            }
            else
            {
                responseData = orderHelper.GetCareHomeOrders(patientOrdersBusinessModel, out totalRecords, out totalPages);
            }
            #endregion

            long orderStatus = 0;
            if (responseData != null && responseData.Any())
            {
                //1. Check the type of selected patient
                //2. Check if selfcae patient of carehome
                //3. Check carehome patient type for automatic order check

                var firstRecord = responseData.First();
                orderStatus = firstRecord.OrderStatus;
                var nextDeliveryDate = DateTime.ParseExact(firstRecord.NextDeliveryDate, Constants.DateFormat, CultureInfo.InvariantCulture);
                nextDeliveryDate = CalculateNextDeliveryDate(nextDeliveryDate); //calculate NDD for new carehome order
                if (careHomeId != 0)
                {
                    Session[Constants.IsCheckCarehomeStatus] = true;
                }

                if (!isActivatedOrder)
                {
                    responseData.ForEach(a => { a.DeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture); a.NextDeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture); });
                    Session[Constants.SessionNextDeliveryDate] = string.Empty;
                    Session[Constants.SessionNextDeliveryDate] = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                }
                else
                {
                    Session[Constants.SessionNextDeliveryDate] = firstRecord.NextDeliveryDate;
                    Session[Constants.SessionOrderId] = firstRecord.OrderId;
                }

                LogHelper.LogAction(Constants.ActionLoadCustomers, Constants.PatientController, Constants.ActionNameGetResidentialOrders, globalUserId);
            }

            bool isSentToSAP = orderStatus == (long)SCAEnums.OrderStatus.SentToSAP ? true : false;
            if (isProductOrder)
            {
                responseData = ConvertToProductLevel(responseData, sidx, sord);
                totalRecords = responseData != null ? responseData.Count : 0;
                totalPages = (int)Math.Ceiling((double)responseData.Count / (double)patientOrdersBusinessModel.RowCount);

            }

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = responseData,
                isSentToSAP = isSentToSAP
            };
            return Json(jsonData);
        }

        /// <summary>
        /// Gets the Carehome product activated order.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="CustomerParameter">The customer parameter.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCareHomeProductActivated(int page, int rows, string sidx, string sord, object CustomerParameter, bool isProductOrder = false)
        {
            Session[Constants.SessionOrderId] = null;

            if (!this.ModelState.IsValid)
            {
                return View();
            }

            #region Get carehome and patient id from session
            var careHomeId = Convert.ToInt64(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture);
            long patientId = Convert.ToInt64(Session[Constants.SessionPatientId], CultureInfo.InvariantCulture);
            var patientOrdersBusinessModel = new PatientOrdersBusinessModel()
            {
                Page = page,
                SortType = sord,
                SortColumn = sidx,
                RowCount = rows,
                PatientID = patientId,
                CareHomeID = careHomeId,
                UserId = globalUserId,
                IsProductLevel = isProductOrder
            };
            #endregion

            int totalRecords = 0;
            var totalPages = 0;
            var responseData = new List<PatientOrdersBusinessModel>();

            #region check for existing activated order, if true get details else create new order
            bool isActivatedOrder = false;
            if (Session[Constants.SessionHasActiveOrder] != null)
            {
                isActivatedOrder = (bool)Session[Constants.SessionHasActiveOrder];
            }

            if (isActivatedOrder == true && isProductOrder)
            {
                responseData = orderHelper.GetActivatedOrders(patientOrdersBusinessModel, out totalRecords, out totalPages);
            }
            else
            {
                responseData = orderHelper.GetCareHomeOrders(patientOrdersBusinessModel, out totalRecords, out totalPages);
            }            
            #endregion

            long orderStatus = 0;
            if (responseData != null && responseData.Any())
            {
                //1. Check the type of selected patient
                //2. Check if selfcae patient of carehome
                //3. Check carehome patient type for automatic order check

                var firstRecord = responseData.First();
                orderStatus = firstRecord.OrderStatus;
                var nextDeliveryDate = DateTime.ParseExact(firstRecord.NextDeliveryDate, Constants.DateFormat, CultureInfo.InvariantCulture);
                if (isProductOrder && !isActivatedOrder)
                    nextDeliveryDate = CalculateNextDeliveryDate(nextDeliveryDate); //calculate NDD for new carehome order
                if (careHomeId != 0)
                {
                    Session[Constants.IsCheckCarehomeStatus] = true;
                }

                if (!isActivatedOrder)
                {
                    responseData.ForEach(a => { a.DeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture); a.NextDeliveryDate = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture); });
                    Session[Constants.SessionNextDeliveryDate] = string.Empty;
                    Session[Constants.SessionNextDeliveryDate] = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                }
                else
                {
                    Session[Constants.SessionNextDeliveryDate] = firstRecord.NextDeliveryDate;
                    Session[Constants.SessionOrderId] = firstRecord.OrderId;
                }

                LogHelper.LogAction(Constants.ActionLoadCustomers, Constants.PatientController, Constants.ActionNameGetResidentialOrders, globalUserId);
            }

            bool isSentToSAP = orderStatus == (long)SCAEnums.OrderStatus.SentToSAP ? true : false;
            if (isProductOrder)
            {
                responseData = ConvertToProductLevel(responseData, sidx, sord);
                totalRecords = responseData != null ? responseData.Count : 0;
                totalPages = (int)Math.Ceiling((double)responseData.Count / (double)patientOrdersBusinessModel.RowCount);
            }

            var jsonData = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = responseData,
                isSentToSAP = isSentToSAP
            };
            return Json(jsonData);
        }

        /// <summary>
        /// Method to group by order data
        /// </summary>
        /// <param name="gridData"></param>
        /// <returns></returns>
        private List<PatientOrdersBusinessModel> ConvertToProductLevel(List<PatientOrdersBusinessModel> gridData, string sidx, string sord)
        {
            var groupData = gridData
           .GroupBy(x => new { x.ProductId, x.DeliveryDate, x.ProductDescription, x.OrderId, x.PurchaseOrderNum, x.ProductDisplayId, x.IsOrderActivated })
           .Select(group => new PatientOrdersBusinessModel()
           {
               ProductId = group.Key.ProductId,
               DeliveryDate = group.Key.DeliveryDate,
               Quantity = group.Sum(x => x.Quantity),
               ProductDescription = group.Key.ProductDescription,
               OrderId = group.Key.OrderId,
               PurchaseOrderNum = group.Key.PurchaseOrderNum,
               ProductDisplayId = group.Key.ProductDisplayId,
               IsOrderActivated = group.Key.IsOrderActivated
           }).ToList<PatientOrdersBusinessModel>();
            switch (sidx.ToLower(CultureInfo.InvariantCulture))
            {
                #region ProductLevelOrder

                case "productdescription":
                    if (sord.ToLower(CultureInfo.InvariantCulture) == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.ProductDescription).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.ProductDescription).ToList();
                    break;
                case "quantity":
                    if (sord.ToLower(CultureInfo.InvariantCulture) == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.Quantity).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.Quantity).ToList();
                    break;
                case "deliverydate":
                    if (sord.ToLower(CultureInfo.InvariantCulture) == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.DeliveryDate).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.DeliveryDate).ToList();
                    break;
                default:
                    gridData = gridData.OrderBy(x => x.ProductDisplayId).ToList();
                    break;

                #endregion
            }
            return groupData;
        }

        /// <summary>
        /// GetNDDStartEndDateForCalendar
        /// </summary>
        /// <param name="nextDeliveryDate"></param>
        /// <param name="changeNDDbyXDays"></param>
        private void GetNDDStartEndDateForCalendar(DateTime nextDeliveryDate, long changeNDDbyXDays)
        {
            var NDDStartDate = nextDeliveryDate.AddDays(0 - changeNDDbyXDays);
            var orderLeadTime = 0;
            int countryId = 1;
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];

            if (customerParameter != null && customerParameter.Any())
            {
                orderLeadTime = customerParameter.First().OrderLeadTime;
                countryId = customerParameter.First().CountryId;
            }

            if (NDDStartDate < AddWorkingDays(DateTime.Today, orderLeadTime, countryId))
            {
                NDDStartDate = AddWorkingDays(DateTime.Today, orderLeadTime, countryId);
            }
            Session[Constants.SessionNDDStartDate] = NDDStartDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
            //End date will be always satrt date + 98 calendar days
            int numberofDays = Convert.ToInt32(ConfigurationManager.AppSettings["AddDaysInStartDateForChangeNDD"], CultureInfo.InvariantCulture);
            Session[Constants.SessionNDDEndDate] = NDDStartDate.AddDays(numberofDays).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// GetAdvanceOrderActivationDate
        /// </summary>
        /// <param name="patientTypes"></param>
        /// <returns></returns>
        protected long GetAdvanceOrderActivationDate(List<long> patientTypes)
        {
            long orderActivationDays = 0;
            ControllerHelper.ActionName = Constants.ActionGetPatientTypeMatrix;
            var responseData = ControllerHelper.PostMethodServiceRequest<List<long>, List<PatientTypeMatrixBusinessModel>>(patientTypes);
            if (responseData != null && responseData.Any())
            {
                responseData = responseData.Where(q => q.customerID == Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture)).ToList();
                orderActivationDays = Convert.ToInt64(responseData.Min(q => q.AdvanceOrderActivationDays), CultureInfo.InvariantCulture);
            }

            return orderActivationDays;
        }

        /// <summary>
        /// Gets the change nd dby x days.
        /// </summary>
        /// <param name="patientTypes">The patient types.</param>
        /// <returns></returns>
        protected long GetChangeNDDbyXDays(List<long> patientTypes)
        {
            long changeOrderNddDays = 0;
            ControllerHelper.ActionName = Constants.ActionGetPatientTypeMatrix;
            var responseData = ControllerHelper.PostMethodServiceRequest<List<long>, List<PatientTypeMatrixBusinessModel>>(patientTypes);
            if (responseData != null && responseData.Any())
            {
                responseData = responseData.Where(q => q.customerID == Convert.ToInt64(Session[Constants.SessionCustomerId], CultureInfo.InvariantCulture)).ToList();
                changeOrderNddDays = Convert.ToInt64(responseData.Min(q => q.ChangeOrderNddDays), CultureInfo.InvariantCulture);
            }

            return changeOrderNddDays;
        }

        /// <summary>
        /// Gets the holiday list.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IsHoliday(string date)
        {
            var isHoliday = true;
            var countryId = 0;
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
            if (customerParameter != null && customerParameter.Any())
            {
                countryId = customerParameter.First().CountryId;
            }
            var dateObj = DateTime.ParseExact(date, Constants.DateFormat, CultureInfo.InvariantCulture);
            isHoliday = GenericHelper.IsHoliday(dateObj, countryId);
            return Json(isHoliday, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// GetHolidayList
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetHolidayList(string fromDate, string toDate)
        {
            var countryId = 0;
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
            if (customerParameter != null && customerParameter.Any())
            {
                countryId = customerParameter.First().CountryId;
            }

            var holidayList = new List<HolidayBusinessModel>();
            holidayList = GenericHelper.GetHolidayList(countryId);

            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                DateTime fromDateTime = DateTime.ParseExact(fromDate, Constants.DateFormat, CultureInfo.InvariantCulture);
                DateTime toDateTime = DateTime.ParseExact(toDate, Constants.DateFormat, CultureInfo.InvariantCulture);
                var holidays = holidayList.Where(q => q.HolidayDate >= fromDateTime && q.HolidayDate <= toDateTime).ToList();
                return Json(holidays, JsonRequestBehavior.AllowGet);
            }

            return Json(holidayList, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Gets the dates for prescription.
        /// </summary>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDatesForPrescription(string endDate)
        {
            var patientTypeMatrix = new PatientTypeMatrixBusinessModel();
            var nddEndDate = DateTime.Now;
            var nddStartDate = DateTime.Now;            
            if (!string.IsNullOrEmpty(endDate))
            {
                if (Session[Constants.SessionPatientTypeMatrix] != null)
                {
                    patientTypeMatrix = (PatientTypeMatrixBusinessModel)Session[Constants.SessionPatientTypeMatrix];
                    var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
                    if (customerParameter != null && customerParameter.Any())
                    {
                        var firstRecord = customerParameter.First();                   
                        //new logic
                        nddStartDate = AddWorkingDays(DateTime.Today, firstRecord.OrderLeadTime, firstRecord.CountryId);
                        int numberofDays = Convert.ToInt32(ConfigurationManager.AppSettings["AddDaysInStartDateForChangeNDD"], CultureInfo.InvariantCulture);
                        nddEndDate = nddStartDate.AddDays(numberofDays); //add 98 calender days in start date.
                    }
                }

                return Json(new
                {
                    StartDate = nddStartDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture),
                    EndDate = nddEndDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture)
                },
                                  JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        /// <summary>
        /// Gets the NDD related details.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetNDDRelatedDetails()
        {
            NddDetailsBusinessModel nddDetailsBusinessModel = new NddDetailsBusinessModel();
            if (Session[Constants.SessionPatientTypeMatrix] != null)
            {
                var patientTypeMatrix = (PatientTypeMatrixBusinessModel)Session[Constants.SessionPatientTypeMatrix];
                nddDetailsBusinessModel.ChangeOrderNddDays = Convert.ToInt64(patientTypeMatrix.ChangeOrderNddDays, CultureInfo.InvariantCulture);
            }

            if (Session[Constants.SessionPostcodeMatrix] != null)
            {
                var postCodeMatrix = (List<PostCodeMatrixBusinessModel>)Session[Constants.SessionPostcodeMatrix];
                if (postCodeMatrix.Any())
                {
                    nddDetailsBusinessModel.Day1 = postCodeMatrix.First().Monday == true ? 0 : 1;
                    nddDetailsBusinessModel.Day2 = postCodeMatrix.First().Tuesday == true ? 0 : 2;
                    nddDetailsBusinessModel.Day3 = postCodeMatrix.First().Wednesday == true ? 0 : 3;
                    nddDetailsBusinessModel.Day4 = postCodeMatrix.First().Thursday == true ? 0 : 4;
                    nddDetailsBusinessModel.Day5 = postCodeMatrix.First().Friday == true ? 0 : 5;
                    nddDetailsBusinessModel.RoundID = postCodeMatrix.First().RoundID;
                }
            }

            return Json(nddDetailsBusinessModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Removes the product.
        /// </summary>
        /// <param name="productIdList">The product identifier list.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveProduct(List<long> productIdList)
        {
            if (Session[Constants.SessionOrderId] == null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            var orderId = Convert.ToInt64(Session[Constants.SessionOrderId], CultureInfo.InvariantCulture);
            if (orderId == 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            Tuple<long, List<long>> requestObj = new Tuple<long, List<long>>(orderId, productIdList);
            ControllerHelper.ActionName = Constants.ActionNameRemoveOrderProduct;
            var response = ControllerHelper.GetMethodServiceRequestObject(requestObj);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Determines whether [is NDD allowed].
        /// </summary>
        /// <returns></returns>
        public ActionResult IsNddAllowed()
        {
            if (Session[Constants.SessionCustomerParameters] != null)
            {
                var customerParameters = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
                if (customerParameters.Any())
                {
                    return Json(customerParameters.First().IsNDDAllow, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the order Details : ADP & Note
        /// </summary>
        /// <param name="PatientID"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public ActionResult GetOrderActivationDetails(string patientId, string customerId)
        {
            OrderActivationDetailsBusinessModel objCommunityOrderDetails = new OrderActivationDetailsBusinessModel();

            if (!string.IsNullOrEmpty(patientId) && !string.IsNullOrEmpty(customerId))
            {
                objCommunityOrderDetails.PatientID = patientId;
                objCommunityOrderDetails.CustomerID = customerId;
                objCommunityOrderDetails.OrderID = Convert.ToString(Session[Constants.SessionOrderId], CultureInfo.InvariantCulture);
                objCommunityOrderDetails.UserID = globalUserId;
                objCommunityOrderDetails.UserType = Convert.ToString(ControllerHelper.CurrentUser.UserType, CultureInfo.InvariantCulture);
                objCommunityOrderDetails.CarehomeId = Session[Constants.SessionCareHomeId] != null ? Convert.ToString(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture) : string.Empty;
                ControllerHelper.ActionName = Constants.ActionNameGetOrderActivationDetails;
                var responseData = ControllerHelper.PostMethodServiceRequest<OrderActivationDetailsBusinessModel>(objCommunityOrderDetails);
                return Json(responseData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objCommunityOrderDetails.OrderID = Convert.ToString(Session[Constants.SessionOrderId], CultureInfo.InvariantCulture);
                objCommunityOrderDetails.UserID = globalUserId;
                objCommunityOrderDetails.UserType = Convert.ToString(ControllerHelper.CurrentUser.UserType, CultureInfo.InvariantCulture);
                objCommunityOrderDetails.CarehomeId = Session[Constants.SessionCareHomeId] != null ? Convert.ToString(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture) : string.Empty;       
                return Json(objCommunityOrderDetails, JsonRequestBehavior.AllowGet);
            }
        }
    }
}