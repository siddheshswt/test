﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sachin
// Created          : 09-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassChangesController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel.MassChanges;
    using System.Web.Mvc;
    using System.Web;

    /// <summary>
    /// Mass Data Changes Controller
    /// </summary>
    [Authorize]  
    public class MassDataChangesController : Controller
    {
        #region Private Properties

        /// <summary>
        /// The Mass Changes helper
        /// </summary>
        MassDataChangesHelper massChangesHelper = new MassDataChangesHelper();

        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Object of CommonController
        /// </summary>
        private CommonController commonController = new CommonController();

        #endregion

        // GET: MassChanges
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
       
        #region MasterMassDataChanges
        /// <summary>
        /// Masters the mass data change.
        /// </summary>
        /// <returns>mass data change view</returns>
        public ActionResult MasterMassDataChange()
        {
            MasterMassDataChangeViewModel masterMassDataChangeViewModel = new MasterMassDataChangeViewModel();
            masterMassDataChangeViewModel = this.massChangesHelper.GetMasterMassDataChange();
            LogHelper.LogAction(Constants.ActionMethodMasterMassDataChange, Constants.MassDataChangesController, Constants.ActionMethodMasterMassDataChange, this.userId);
            return View(Constants.ActionMasterMassDataChange, masterMassDataChangeViewModel);
        }

        /// <summary>
        /// Download Carehome data for Mass Update
        /// </summary>
        /// <param name="masterMassDataChangeViewModel"></param>
        /// <returns>view model</returns>
        [HttpPost]
        public JsonResult DownloadMasterMassDataChange(MasterMassDataChangeViewModel masterMassDataChangeViewModel)
        {
            MasterMassDataChangeViewModel returnViewModel = this.massChangesHelper.DownloadMasterMassDataChange(masterMassDataChangeViewModel);
            LogHelper.LogAction(Constants.ActionMethodDownloadMasterMassDataChange, Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, this.userId);
            return this.Json(returnViewModel, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Prescription Mass Update
        /// <summary>
        /// Prescriptions the mass update.
        /// </summary>
        /// <returns>
        /// Prescriptions View
        /// </returns>
        public ActionResult PrescriptionMassUpdate()
        {
            PrescriptionMassUpdateViewModel prescriptionMassUpdateViewModel = new PrescriptionMassUpdateViewModel();
            prescriptionMassUpdateViewModel = this.massChangesHelper.GetPrescriptionData();
            LogHelper.LogAction(Constants.ActionMethodPrescriptionMassUpdate, Constants.MassDataChangesController, Constants.ActionMethodPrescriptionMassUpdate, this.userId);
            return View(Constants.ActionPrescriptionMassUpdate, prescriptionMassUpdateViewModel);
        }

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="SearchTable">The search table.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns>return result</returns>
        public JsonResult GetSearchResults(string term, string searchTable, long? customerId)
        {
            var SearchResults = commonController.GetSearchResults(term, searchTable, customerId);
            LogHelper.LogAction(Constants.ActionLogSearchFromPrescriptionUpdate, Constants.MassDataChangesController, Constants.ActionPrescriptionMassUpdateSearch, this.userId);      
            return Json(SearchResults, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// Upload Mass Update Data
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <param name="selectedTemplate"></param>
        /// <param name="isImport"></param>
        /// <returns>return result</returns>
        [HttpPost]
        public JsonResult UploadMassUpdateData(HttpPostedFileBase uploadedFile, int selectedTemplate, bool isImport)
        {
            var objMassDataChangesHelper = new MassDataChangesHelper();
            var objMasterMassDataChangeViewModel = new MasterMassDataChangeViewModel();
            objMasterMassDataChangeViewModel.UploadedFile = uploadedFile;
            objMasterMassDataChangeViewModel.SelectedTemplate = selectedTemplate;

            objMassDataChangesHelper.UploadMassUpdateData(objMasterMassDataChangeViewModel, isImport);
            return this.Json(new
            {
                Status = objMasterMassDataChangeViewModel.Status,
                Message = objMasterMassDataChangeViewModel.Message,
                LogFilePath = objMasterMassDataChangeViewModel.LogFilePath,
                validationRows = objMasterMassDataChangeViewModel.TemplateValidationRows
            }, JsonRequestBehavior.AllowGet);
        }
    }
}