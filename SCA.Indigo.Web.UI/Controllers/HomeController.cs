﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="HomeController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Globalization;
    using System.Web.Mvc;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;

    /// <summary>
    /// Home Controller
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.InvariantCulture);

        /// <summary>
        /// Welcomes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Welcome()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            HomeViewModel homeViewModelObj = new HomeViewModel();
            homeViewModelObj.IsCalledFromHomePage = Convert.ToString(Session[Constants.IsCalledFromHomePage], CultureInfo.InvariantCulture);
            homeViewModelObj.InteractionId = Convert.ToString(Session[Constants.InteractionId], CultureInfo.InvariantCulture);
            Session[Constants.SessionisRedirectFromAuthorisation] = null;

            #region 1.5.4 Uncomment for Sprint(Ajax Method)
            //ControllerHelper.ActionName = Constants.ActionNameTrackHomePageVisit;
            //var responseData = (bool)ControllerHelper.PostMethodServiceRequestObject(Convert.ToString(ControllerHelper.CurrentUser.UserId,CultureInfo.InvariantCulture));
            #endregion
			homeViewModelObj.IsInternalUser = Convert.ToString(ControllerHelper.CurrentUser.IsInternalUser, CultureInfo.InvariantCulture);

            return View("Home", homeViewModelObj);
        }               
    }
}