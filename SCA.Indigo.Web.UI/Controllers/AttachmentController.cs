﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Gaurav Lad
// Created          : 19-05-2015
//
// Last Modified By : Gaurav Lad
// Last Modified On : 19-05-2015
// ***********************************************************************
// <copyright file="AttachmentController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;

    /// <summary>
    /// Attachment Controller
    /// </summary>
    [Authorize]
    public class AttachmentController : Controller
    {
        #region Attachment

        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// The attachment helper
        /// </summary>
        private AttachmentHelper attachmentHelper = new AttachmentHelper();

        /// <summary>
        /// Displays the attachment.
        /// </summary>
        /// <param name="attachmentViewModel">The attachment view model.</param>
        /// <returns>
        /// return attachment business view model object
        /// </returns>
        [HttpGet]
        public ActionResult DisplayAttachment(AttachmentViewModel attachmentViewModel)
        {
            List<AttachmentBusinessViewModel> attachmentBusinessViewModel = new List<AttachmentBusinessViewModel>();
            attachmentBusinessViewModel = attachmentHelper.DisplayAttachment(attachmentViewModel);
            LogHelper.LogAction(Constants.ActionNameDisplayAttachment, Constants.AttachmentController, Constants.ActionNameDisplayAttachment, this.userId);
            return this.Json(attachmentBusinessViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Uploads the attachment.
        /// </summary>
        /// <param name="attachmentViewModel">The attachment view model.</param>
        /// <returns>
        /// return status of uploaded attachment
        /// </returns>
        [HttpPost]
        public string UploadAttachment(AttachmentViewModel attachmentViewModel)
        {
            string message = "false";

            attachmentViewModel.AttachmentFiles = new List<HttpPostedFileBase>();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files["file" + i] as HttpPostedFileBase;
                attachmentViewModel.AttachmentFiles.Add(file);
            }


            message = attachmentHelper.UploadAttachment(attachmentViewModel);
            LogHelper.LogAction(Constants.ActionNameUploadAttachment, Constants.AttachmentController, Constants.ActionNameUploadAttachment, this.userId);
            
            return message;
        }

        /// <summary>
        /// Removes the attachment.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>
        /// return status of remove attachment
        /// </returns>
        [HttpPost]
        public bool RemoveAttachment(List<string> attachmentListId)
        {
            bool isSuccess = false;
            List<string> attachmentLoaction = this.attachmentHelper.GetAttachmentLocation(attachmentListId);

            foreach (var file in attachmentLoaction)
            {
                string filePath = Server.MapPath('~' + file);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }

            isSuccess = this.attachmentHelper.RemoveAttachment(attachmentListId);
            LogHelper.LogAction(Constants.ActionNameRemoveAttachment, Constants.AttachmentController, Constants.ActionNameRemoveAttachment, userId);
            return isSuccess;
        }

        /// <summary>
        /// Downs the load attachment result.
        /// </summary>
        /// <param name="attachmentIdList">The attachment identifier list.</param>
        /// <returns>
        /// return file
        /// </returns>
        [HttpPost]
        public ActionResult DownloadAttachmentResult(List<string> attachmentIdList)
        {
            var response = this.attachmentHelper.DownloadAttachment(attachmentIdList);
            LogHelper.LogAction(Constants.ActionNameGetAttachmentPath, Constants.AttachmentController, Constants.ActionNameGetAttachmentPath, userId);            
            return this.Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}