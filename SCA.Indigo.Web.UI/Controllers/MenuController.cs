﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="MenuController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Common;
    using System;

    /// <summary>
    /// Menu Controller
    /// </summary>
    public class MenuController : Controller
    {
        // GET: Menu
        /// <summary>
        /// The _user login name
        /// </summary>
        private string userLoginName = ControllerHelper.CurrentUser.UserName;
        /// <summary>
        /// The is authorized
        /// </summary>
        private bool isAuthorized = false;

        /// <summary>
        /// Get function to LoadMenu
        /// </summary>
        /// <returns>
        /// Load Menu View.
        /// </returns>
        public ActionResult LoadMenu()
        {
            long languageId = (int)SCAEnums.OtherConstants.LanguageId;
            ControllerHelper.ActionName = Constants.ActionNameGetUserMenuDetails;
            ControllerHelper.ActionParam = new[] { userLoginName, languageId.ToString(CultureInfo.InvariantCulture) };
            Session[Constants.SessionAuthorizedUrl] = ControllerHelper.GetMethodServiceRequestForList<List<UserMenuDetailsBusinessModel>>();
            
            var data = (List<UserMenuDetailsBusinessModel>)Session[Constants.SessionAuthorizedUrl];
          
            // If there are not mapping in User Role table rturn to the Error Page
            if (data == null)
            {
                return View(Constants.MenuController);
            }

            var controllerName = Request.Url.AbsoluteUri.Split('/')[3].ToString(CultureInfo.InvariantCulture);
			var actionName = Request.Url.AbsoluteUri.Split('/')[4].ToString(CultureInfo.InvariantCulture).Split('?')[0];

            if (controllerName.ToUpper(CultureInfo.InvariantCulture) == Constants.PatientController.ToUpper(CultureInfo.InvariantCulture) && actionName.ToUpper(CultureInfo.InvariantCulture) == Constants.ActionPatientDetails.ToUpper(CultureInfo.InvariantCulture))
            {
                actionName = Constants.ActionDetails;
            }

            UserMenuDetailsBusinessModel resultData = data.Find(d => d.ActionName == actionName && d.ControllerName == controllerName);            

            if (resultData != null ||
                actionName.ToUpper(CultureInfo.InvariantCulture) == Constants.ActionChangePassword.ToUpper(CultureInfo.InvariantCulture) ||
                actionName.ToUpper(CultureInfo.InvariantCulture) == Constants.ActionLogOff.ToUpper(CultureInfo.InvariantCulture)        ||
                actionName.ToUpper(CultureInfo.InvariantCulture) == Constants.ActionMasterMassDataChange.ToUpper(CultureInfo.InvariantCulture))                
            {
                isAuthorized = true;
            }

            if (isAuthorized)
            {
                ViewBag.userMenu = data.Where(q => q.IsDisplay == true).OrderBy(m => m.ParentMenuId).ToList();
                ViewBag.userSubMenu = data.Where(m => m.ParentMenuId != 0 && m.IsDisplay == true).OrderBy(m => m.DisplayIndex).ToList();
                if (resultData != null)
                {
                    ControllerHelper.CurrentActionClicked = resultData.MenuName;
                }
            }
            return View(Constants.MenuController);
        }

        /// <summary>
        /// LogUser
        /// </summary>
        /// <param name="menuName"></param>
        /// <returns></returns>
        public JsonResult LogUser(string menuName)
        {
            string UserId = ControllerHelper.CurrentUser.UserId.ToString();
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("Menu " + menuName + " clicked" + guidKey, " Menu", menuName, UserId);
            return this.Json("true", JsonRequestBehavior.AllowGet);      
        }
    }
}