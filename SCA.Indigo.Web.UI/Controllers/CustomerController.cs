﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="CustomerController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;

    using SCA.Indigo.Web.UI.ViewModel.UserAdmin;
    using SCA.Indigo.Common.Enums;
    using System.Globalization;
    using System;
    using SCA.Indigo.Web.UI.ViewModel;
    using System.Configuration;
    using System.Data;
    using System.IO;
	using System.Linq;

    /// <summary>
    /// Class CustomerController.
    /// </summary>
    [Authorize]
    public class CustomerController : Controller
    {
        #region Public Properties

        /// <summary>
        /// The _customer view model
        /// </summary>
        private readonly CustomerViewModel globalCustomerViewModel = new CustomerViewModel();
        /// <summary>
        /// The user admin helper
        /// </summary>
        UserAdminHelper userAdminHelper = new UserAdminHelper();

        #endregion

        #region Private Properties

        // GET: Customer
        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();
        /// <summary>
        /// The customer helper
        /// </summary>
        CustomerHelper customerHelper = new CustomerHelper();

        #endregion

        #region Customer Information

        /// <summary>
        /// Customers the information.
        /// </summary>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [OutputCache(Duration = 0)]
		public ActionResult CustomerInformation()
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("CustomerInformationGet Start - " + guidKey, "Customer", "CustomerInformationGet Start", userId);
			long customerId = 0;
            // Get the Customer Id from the Request.
            if (Session[Constants.SessionCustomerId] == null)
            {
                globalCustomerViewModel.ShowCustomerViewAlert = true;
            }
            else
            {
                customerId = Convert.ToInt64(Session[Constants.SessionCustomerId].ToString(), CultureInfo.InvariantCulture);
            }
            var mapCustomers = customerHelper.GetCustomerInformation(customerId);

            globalCustomerViewModel.PatientTypeMatrixViewModel = mapCustomers.PatientTypeMatrixViewModel;
            globalCustomerViewModel.CustomerParameterViewModel = mapCustomers.CustomerParameterViewModel;
            globalCustomerViewModel.ClinicalContactMappingViewModel = mapCustomers.ClinicalContactMappingViewModel;
            globalCustomerViewModel.CustomerItems = mapCustomers.CustomerItems;
            globalCustomerViewModel.CustomerStatus = mapCustomers.CustomerStatus;
            globalCustomerViewModel.CustomerBusinessModel = mapCustomers.CustomerBusinessModel;
            globalCustomerViewModel.CustomerParameterViewModel = customerHelper.GetCustomerParameter(string.Empty);
            globalCustomerViewModel.SelectedCustomerId = Convert.ToString(customerId, CultureInfo.InvariantCulture);
            globalCustomerViewModel.FileCountErrorMessage = string.Format(CultureInfo.InvariantCulture, Resource.Common.resFileCountErrorMessage, ConfigurationManager.AppSettings["FileCount"].ToString(CultureInfo.InvariantCulture));
            LogHelper.LogAction(Constants.ActionNameGetCustomerInformation, Constants.CustomerController, Constants.ActionNameGetCustomerInformation, this.userId);
			LogHelper.LogAction("CustomerInformationGet End - " + guidKey, "Customer", "CustomerInformationGet End", userId);
			return View(globalCustomerViewModel);
        }

        /// <summary>
        /// Gets the customer details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [HttpGet]
		public ActionResult GetCustomerDetails(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetCustomerDetails Start - " + guidKey, "Customer", "Customer Start", userId);

			CustomerViewModel customerDetails = customerHelper.GetCustomerDetailsById(customerId);
            //LogHelper.LogAction(Constants.ActionNameGetCustomerInformation, Constants.CustomerController, Constants.ActionNameGetCustomerInformation, this.UserId);

			LogHelper.LogAction("GetCustomerDetails End - " + guidKey, "Customer", "Customer Start", userId);
            return this.Json(customerDetails, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the prescription approval.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [HttpGet]
        public ActionResult GetPrescriptionApproval(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetPrescriptionApproval Start - " + guidKey, "Customer", "GetPrescriptionApproval Start", userId);

			CustomerViewModel customerDetails = customerHelper.GetPrescriptionApproval(customerId);
            LogHelper.LogAction(Constants.ActionNameGetCustomerInformation, Constants.CustomerController, Constants.ActionNameGetCustomerInformation, this.userId);

			LogHelper.LogAction("GetPrescriptionApproval Start - " + guidKey, "Customer", "GetPrescriptionApproval Start", userId);

			return this.Json(customerDetails, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the prescription approval.
        /// </summary>
        /// <param name="objUserList">The object user list.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult SavePrescriptionApproval(string objUserList, string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SavePrescriptionApproval Start - " + guidKey, "Customer", "SavePrescriptionApproval Start", userId);

			var saveResponse = customerHelper.SavePrescriptionApproval(objUserList, customerId);
      //      LogHelper.LogAction(Constants.ActionNameGetCustomerInformation, Constants.CustomerController, Constants.ActionNameSaveCustomerPrescriptionApprovals, this.UserId);
			LogHelper.LogAction("SavePrescriptionApproval End - " + guidKey, "Customer", "SavePrescriptionApproval End", userId);
            return this.Json(saveResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Removes the prescription approval.
        /// </summary>
        /// <param name="mappingIds">The mapping ids.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult RemovePrescriptionApproval(string mappingIds)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("RemovePrescriptionApproval Start - " + guidKey, "Customer", "RemovePrescriptionApproval Start", userId);

			var response = customerHelper.RemovePrescriptionApproval(mappingIds);
            LogHelper.LogAction(Constants.ActionNameGetCustomerInformation, Constants.CustomerController, Constants.ActionNameGetCustomerInformation, this.userId);
			LogHelper.LogAction("RemovePrescriptionApproval End - " + guidKey, "Customer", "RemovePrescriptionApproval End", userId);
			return this.Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the customer details.
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult SaveCustomerDetails(CustomerBusinessModel customerBusinessModel)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SaveCustomerDetails Start - " + guidKey, "Customer", "SaveCustomerDetails Start", userId);

			int saveResponse = customerHelper.SaveCustomerInformationDetails(customerBusinessModel);
            //LogHelper.LogAction(Constants.ActionNameSaveCustomerInformationDetails, Constants.CustomerController, Constants.ActionNameSaveCustomerInformationDetails, this.UserId);
			LogHelper.LogAction("SaveCustomerDetails End - " + guidKey, "Customer", "SaveCustomerDetails End", userId);
            return this.Json(saveResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Checks the active patients by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// JsonResult.
        /// </returns>
        public JsonResult CheckActivePatientsByCustomer(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("CheckActivePatientsByCustomer Start - " + guidKey, "Customer", "CheckActivePatientsByCustomer Start", userId);

			int saveResponse = customerHelper.CheckActivePatientsByCustomer(customerId);
            //LogHelper.LogAction(Constants.ActionNameCheckActivePatientsByCustomer, Constants.CustomerController, Constants.ActionNameCheckActivePatientsByCustomer, this.UserId);
			LogHelper.LogAction("CheckActivePatientsByCustomer End - " + guidKey, "Customer", "CheckActivePatientsByCustomer End", userId);
            return this.Json(saveResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the user details by user identifier.
        /// </summary>
        /// <param name="userToAuthorize">The user to authorize.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        public ActionResult GetUserDetailsByUserId(string userToAuthorize)
        {
            CustomerViewModel customerDetails = customerHelper.GetUserDetailsByUserId(userToAuthorize);
            LogHelper.LogAction(Constants.ActionNameGetCustomerInformation, Constants.CustomerController, Constants.ActionNameGetUserDetailsByUserId, this.userId);
            return this.Json(customerDetails, JsonRequestBehavior.AllowGet);
        }

        #region CustomerParameter

        /// <summary>
        /// Loads the customer postcode.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// JsonResult.
        /// </returns>
        public JsonResult LoadCustomerPostcode(int page, int rows, string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("LoadCustomerPostcode Start - " + guidKey, "Customer", "LoadCustomerPostcode Start", userId);

			ControllerHelper.ActionName = Constants.ActionNameGetCustomerPostcodeMatrix;
            ControllerHelper.ActionParam = new[] { customerId, userId };
            var loadCustomerPostcodematrix = ControllerHelper.GetMethodServiceRequestForList<List<PostCodeMatrixBusinessModel>>();
            if (loadCustomerPostcodematrix == null)
            {
                loadCustomerPostcodematrix = new List<PostCodeMatrixBusinessModel>();
            }
            //LogHelper.LogAction(Constants.ActionLogLoadCustomerPostcode, Constants.CustomerController, Constants.ActionLoadCustomerPostcode, UserId);
			LogHelper.LogAction("LoadCustomerPostcode End - " + guidKey, "Customer", "LoadCustomerPostcode End", userId);
            return this.Json(loadCustomerPostcodematrix, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the customer postcode.
        /// </summary>
        /// <param name="postCodeMatrixList">The post code matrix list.</param>
        /// <returns>
        /// JsonResult.
        /// </returns>
        public JsonResult SaveCustomerPostcode(List<PostCodeMatrixBusinessModel> postCodeMatrixList)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SaveCustomerPostcode Start - " + guidKey, "Customer", "SaveCustomerPostcode Start", userId);
			bool isPostcodeSaved = false;
            if (postCodeMatrixList != null)
            {
                ControllerHelper.ActionName = Constants.ActionNameSaveCustomerPostcodeMatrix;
                LogHelper.LogAction(Constants.ActionLogSaveCustomerPostcode, Constants.CustomerController, Constants.ActionSaveCustomerPostcode, userId);
                if (!string.IsNullOrEmpty(userId))
                {
                    postCodeMatrixList.ForEach(q => q.UserId = userId);
                    isPostcodeSaved = (bool)ControllerHelper.PostMethodServiceRequestObject(postCodeMatrixList);
                }
            }
	        LogHelper.LogAction("SaveCustomerPostcode End - " + guidKey, "Customer", "SaveCustomerPostcode End", userId);
            return Json(isPostcodeSaved, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the customer parameter.
        /// </summary>
        /// <param name="customerParameter">The customer parameter.</param>
        /// <returns>
        /// ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult SaveCustomerParameter(CustomerParameterBusinessModel customerParameter)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SaveCustomerParameter Start - " + guidKey, "Customer", "SaveCustomerParameter Start", userId);
			bool result = false;
            if (customerParameter != null)
            {
                customerParameter.UserID = ControllerHelper.CurrentUser.UserId.ToString();
                result = customerHelper.SaveCustomerParameter(customerParameter);
                if (result)
                {
                    LogHelper.LogAction(Constants.ActionLogCPSaveSuccess + customerParameter.CustomerID.ToString(CultureInfo.InvariantCulture), Constants.CustomerController, Constants.ActionCPSaveCustomerParameter, ControllerHelper.CurrentUser.UserId.ToString());
                }
                else
                {
                    LogHelper.LogAction(Constants.ActionLogCPSaveFailed + customerParameter.CustomerID.ToString(CultureInfo.InvariantCulture), Constants.CustomerController, Constants.ActionCPSaveCustomerParameter, ControllerHelper.CurrentUser.UserId.ToString());
                }
            }
			LogHelper.LogAction("SaveCustomerParameter End - " + guidKey, "Customer", "SaveCustomerParameter End", userId);
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loads the customer parameter.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// JsonResult.
        /// </returns>
        public JsonResult LoadCustomerParameter(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("LoadCustomerParameter Start - " + guidKey, "Customer", "LoadCustomerParameter Start", userId);
            globalCustomerViewModel.CustomerParameterViewModel = customerHelper.GetCustomerParameter(customerId);
			LogHelper.LogAction("LoadCustomerParameter End - " + guidKey, "Customer", "LoadCustomerParameter End", userId);
            return this.Json(globalCustomerViewModel.CustomerParameterViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Exports the customer parameter.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="isExportToExcel">if set to <c>true</c> [is export to excel].</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ExportCustomerParameter(string customerId, bool isExportToExcel)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("ExportCustomerParameter Start - " + guidKey, "Customer", "ExportCustomerParameter Start", userId);
			dynamic customerParameterData = customerHelper.ExportCustomerParameter(customerId, isExportToExcel);
            if (isExportToExcel)
            {
                if (customerParameterData != null)
                {
                    string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);
                    string reportBuilderServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.CustomerParameterSharedLocation);
                    var FileName = Constants.CustomerParameter + Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture) + DateTime.Now.ToString(Constants.DateTimeFormatyy5, CultureInfo.InvariantCulture) + ".xlsx";
                    var FolderPath = Path.Combine(serverPath, reportBuilderServerDirectory);
                    CommonHelper.CreateDirectoryIfNotExists(FolderPath);
                    var FilePath = Path.Combine(serverPath, reportBuilderServerDirectory + '\\' + FileName);
                    var isExcelCreated = ExcelHelper.CreateExcelDocument(customerParameterData, FilePath);
                    if (isExcelCreated)
                    {
                        var TempPath = Path.Combine(GenericHelper.TempAddress, FileName);
                        System.IO.File.Copy(FilePath, TempPath, true);
                        FilePath = GenericHelper.DownloadUrl + '\\' + GenericHelper.UploadDirectory + '\\' + FileName;
                        return this.Json(FilePath, JsonRequestBehavior.AllowGet);
                    }
                }
            }
			LogHelper.LogAction("ExportCustomerParameter End - " + guidKey, "Customer", "ExportCustomerParameter End", userId);
            return this.Json(customerParameterData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loads the customer parameter.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="reasonCodeId">The reason code identifier.</param>
        /// <returns>
        /// JsonResult.
        /// </returns>
        public JsonResult RemoveReasonCodeMapping(string customerId, string reasonCodeId)
        {
          //  return this.Json(false, JsonRequestBehavior.AllowGet);
            var canRemove = customerHelper.RemoveReasonCodeMapping(customerId, reasonCodeId);
            return this.Json(canRemove, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// This will return view
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// all users
        /// </returns>
        public ActionResult LoadUserCustomer(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("LoadUserCustomer Start - " + guidKey, "Customer", "LoadUserCustomer Start", userId);
            var UserDetails = userAdminHelper.GetAllUsers(customerId, Convert.ToString(true, CultureInfo.InvariantCulture));
			LogHelper.LogAction("LoadUserCustomer End - " + guidKey, "Customer", "LoadUserCustomer End", userId);
			return this.Json(UserDetails, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Patient Type Matrix
        /// <summary>
        /// Get patient type matrix
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// action result
        /// </returns>
        public ActionResult GetPatientTypeMatrix(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetPatientTypeMatrix Start - " + guidKey, "Customer", "GetPatientTypeMatrix Start", userId);

            PatientTypeMatrixViewModel patientTypeMatrixViewModel = new PatientTypeMatrixViewModel();
            patientTypeMatrixViewModel = this.customerHelper.GetPatientTypeMatrix(customerId);
            LogHelper.LogAction(Constants.ActionMethodGetPatientTypeMatrix, Constants.CustomerController, Constants.ActionMethodGetPatientTypeMatrix, this.userId);
			LogHelper.LogAction("GetPatientTypeMatrix End - " + guidKey, "Customer", "GetPatientTypeMatrix End", userId);
			return this.Json(patientTypeMatrixViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Export patient type matrix details.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// file path
        /// </returns>
        public ActionResult ExportPatientTypeMatrix(string customerId)
        {
            string filePath = this.customerHelper.ExportPatientTypeMatrix(customerId);
            LogHelper.LogAction(Constants.ActionMethodExportPatientTypeMatrix, Constants.CustomerController, Constants.ActionMethodExportPatientTypeMatrix, this.userId);
            return this.Json(filePath, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save patient type matrix
        /// </summary>
        /// <param name="objPatientMatrixList">The object patient matrix list.</param>
        /// <param name="objCustomerId">The object customer identifier.</param>
        /// <returns>
        /// true or false
        /// </returns>
        [HttpPost]
        public ActionResult SavePatientTypeMatrix(List<PatientTypeListViewModel> objPatientMatrixList, string objCustomerId)
        {
            int savePatientTypeMatrixResponse = customerHelper.SavePatientTypeMatrix(objPatientMatrixList, objCustomerId);
            LogHelper.LogAction(Constants.ActionNameSavePatientTypeMatrixDetails, Constants.CustomerController, Constants.ActionNameSavePatientTypeMatrixDetails, this.userId);
            return this.Json(savePatientTypeMatrixResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Patient Type Associated With Patient
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientType">Type of the patient.</param>
        /// <returns>
        /// view model
        /// </returns>
        [HttpGet]
        public JsonResult GetPatientTypeAssociatedWithPatient(string customerId, string patientType)
        {
            // Get Details for Role Mapping
            PatientTypeMatrixViewModel patientTypeMatrixViewModel = new PatientTypeMatrixViewModel();
            patientTypeMatrixViewModel = this.customerHelper.GetPatientTypeAssociatedWithPatient(customerId, patientType);
            LogHelper.LogAction(Constants.ActionMethodGetPatientTypeAssociatedWithPatient, Constants.CustomerController, Constants.ActionMethodGetPatientTypeAssociatedWithPatient, this.userId);
            return this.Json(patientTypeMatrixViewModel, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Clinical Contact
        /// <summary>
        /// Clinical the contact mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// Action Result.
        /// </returns>
        public ActionResult ClinicalContactMapping(long customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("ClinicalContactMapping Start - " + guidKey, "Customer", "ClinicalContactMapping Start", userId);

            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ClinicalContactMappingViewModel();
            clinicalContactMappingViewModel = this.customerHelper.GetClinicalContactMapping(customerId);
            //LogHelper.LogAction(Constants.ActionClinicalContactMapping, Constants.CustomerController, Constants.ActionClinicalContactMapping, this.UserId);
			LogHelper.LogAction("ClinicalContactMapping End - " + guidKey, "Customer", "ClinicalContactMapping End", userId);
			return this.Json(clinicalContactMappingViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Refresh Role Type dropdown on Save Role button..
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// view model
        /// </returns>
        [HttpGet]
        public JsonResult RefreshRoleTypeDropDown(string customerId)
        {
            // Get Details for Role Mapping
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = this.customerHelper.RefreshRoleTypeDropDown(customerId);
            LogHelper.LogAction(Constants.ActionMethodRefreshRoleTypeDropDown, Constants.CustomerController, Constants.ActionMethodRefreshRoleTypeDropDown, this.userId);
            return this.Json(clinicalContactMappingViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the clinical contact value mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="roleType">Type of the role.</param>
        /// <returns>
        /// return result
        /// </returns>
        [HttpGet]
        public JsonResult GetClinicalContactValueMapping(string customerId, string roleType)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetClinicalContactValueMapping Start - " + guidKey, "Customer", "GetClinicalContactValueMapping Start", userId);

			// Get Details for Role Mapping
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = this.customerHelper.GetClinicalContactValueMapping(customerId, roleType);
          //  LogHelper.LogAction(Constants.ActionMethodGetClinicalContactValueMapping, Constants.CustomerController, Constants.ActionMethodGetClinicalContactValueMapping, this.UserId);
			LogHelper.LogAction("GetClinicalContactValueMapping End - " + guidKey, "Customer", "GetClinicalContactValueMapping End", userId);
            return this.Json(clinicalContactMappingViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the clinical contact role mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// Json Result.
        /// </returns>
        [HttpGet]
        public JsonResult GetClinicalContactRoleMapping(string customerId)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetClinicalContactRoleMapping Start - " + guidKey, "Customer", "GetClinicalContactRoleMapping Start", userId);

            // Get Details for Role Mapping
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = this.customerHelper.GetClinicalContactRoleMapping(customerId);
            //LogHelper.LogAction(Constants.ActionMethodGetClinicalContactRoleMapping, Constants.CustomerController, Constants.ActionMethodGetClinicalContactRoleMapping, this.UserId);
			LogHelper.LogAction("GetClinicalContactRoleMapping End - " + guidKey, "Customer", "GetClinicalContactRoleMapping End", userId);
            return this.Json(clinicalContactMappingViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// saving clinical role mapping details
        /// </summary>
        /// <param name="objClinicalRoleMapping">The object clinical role mapping.</param>
        /// <returns>
        /// return result
        /// </returns>
        [HttpPost]
        public ActionResult SaveClinicalRoleMapping(List<ClinicalContactRoleMappingViewModel> objClinicalRoleMapping)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SaveClinicalRoleMapping Start - " + guidKey, "Customer", "SaveClinicalRoleMapping Start", userId);
			int isaveResponse = this.customerHelper.SaveClinicalRoleMapping(objClinicalRoleMapping);
            //LogHelper.LogAction(Constants.ActionMethodSaveClinicalRoleMapping, Constants.CustomerController, Constants.ActionMethodSaveClinicalRoleMapping, this.UserId);
			LogHelper.LogAction("SaveClinicalRoleMapping End - " + guidKey, "Customer", "SaveClinicalRoleMapping End", userId);
            return this.Json(isaveResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Parent Value
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="childId">The child identifier.</param>
        /// <returns>
        /// return result
        /// </returns>
        [HttpGet]
        public JsonResult GetParentValueForSelectedRoleType(string customerId, string childId)
        {
            // Get Details for Role Mapping
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetParentValueForSelectedRoleType Start - " + guidKey, "Customer", "GetParentValueForSelectedRoleType Start", userId);
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = this.customerHelper.GetParentValueForSelectedRoleType(customerId, childId);
            LogHelper.LogAction(Constants.ActionMethodGetParentValueForSelectedRoleType, Constants.CustomerController, Constants.ActionMethodGetParentValueForSelectedRoleType, this.userId);
			LogHelper.LogAction("GetParentValueForSelectedRoleType End - " + guidKey, "Customer", "GetParentValueForSelectedRoleType End", userId);
            return this.Json(clinicalContactMappingViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Clinical Value
        /// </summary>
        /// <param name="objClinicalContactValues">The object clinical contact values.</param>
        /// <returns>
        /// return result
        /// </returns>
        [HttpPost]
        public ActionResult SaveClinicalContactValue(List<ClinicalContactValueMappingViewModel> objClinicalContactValues)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("SaveClinicalContactValue Start - " + guidKey, "Customer", "SaveClinicalContactValue Start", userId);
            int saveResponse = this.customerHelper.SaveClinicalContactValue(objClinicalContactValues);
         //   LogHelper.LogAction(Constants.ActionNameSaveClinicalContactValueMapping, Constants.CustomerController, Constants.ActionNameSaveClinicalContactValueMapping, this.UserId);
			LogHelper.LogAction("SaveClinicalContactValue End - " + guidKey, "Customer", "SaveClinicalContactValue End", userId);
            return this.Json(saveResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Remove Clinical Contact Analysis Mapping
        /// </summary>
        /// <param name="medicalStaffAnalysisInfo">The medical staff analysis information.</param>
        /// <returns>
        /// Json Result.
        /// </returns>
        public JsonResult RemoveClinicalContactAnalysisMapping(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfo)
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("RemoveClinicalContactAnalysisMapping Start - " + guidKey, "Customer", "RemoveClinicalContactAnalysisMapping Start", userId);
            var canRemove = this.customerHelper.RemoveClinicalContactAnalysisMapping(medicalStaffAnalysisInfo);
            //LogHelper.LogAction(Constants.ActionMethodRemoveClinicalContactAnalysisMapping, Constants.CustomerController, Constants.ActionMethodRemoveClinicalContactAnalysisMapping, this.UserId);

			LogHelper.LogAction("RemoveClinicalContactAnalysisMapping End - " + guidKey, "Customer", "RemoveClinicalContactAnalysisMapping End", userId);
            return this.Json(canRemove, JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        #region CustomerSubModule Access

        /// <summary>
        /// Gets the customer sub module access.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCustomerSubModuleAccess()
        {
			var guidKey = Guid.NewGuid();
			LogHelper.LogAction("GetCustomerSubModuleAccess Start - " + guidKey, "Customer", "GetCustomerSubModuleAccess Start", userId);
            // Set 1 = Full Rights, 0 = View Only , blank = No Acess
            string contactInfoAccess = string.Empty;
            string attachmentAccess = string.Empty;

            List<UserMenuDetailsBusinessModel> allRoleMenuDetails = (List<UserMenuDetailsBusinessModel>)Session[Constants.SessionAuthorizedUrl];

            // Check for the Patient Sub Module Access
            var contactInfoObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CustomerContactInfo);
            if (contactInfoObj != null)
            {
                if (contactInfoObj.FullRights)
                {
                    contactInfoAccess = "1";
                }
                else if (contactInfoObj.ViewOnly)
                {
                    contactInfoAccess = "0";
                }
            }
            
            var attachmentObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CustomerAttachments);
            if (attachmentObj != null)
            {
                if (attachmentObj.FullRights)
                {
                    attachmentAccess = "1";
                }
                else if (attachmentObj.ViewOnly)
                {
                    attachmentAccess = "0";
                }
            }
			LogHelper.LogAction("GetCustomerSubModuleAccess End - " + guidKey, "Customer", "GetCustomerSubModuleAccess End", userId);

            return this.Json(new
            {
                ContactInfoAccess = contactInfoAccess,
                AttachmentAccess = attachmentAccess

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

	}

	 
}