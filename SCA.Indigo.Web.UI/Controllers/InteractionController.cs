﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="InteractionController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System.Web.Mvc;
    using System.Collections.Generic;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;
    using SCA.Indigo.Business.BusinessModels;
    using System;
    using System.Globalization;
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using System.IO;
    using SCA.Indigo.Web.UI.Resource;

    /// <summary>
    /// Interaction Controller
    /// </summary>
    [Authorize]
    public class InteractionController : Controller
    {
        private string interactionUserId = string.Empty;
        /// <summary>
        /// constructor will check for userid else it will redirect to login page
        /// </summary>
        public InteractionController()
        {
            if (ControllerHelper.CurrentUser != null)
            {
                if (!string.IsNullOrEmpty(ControllerHelper.CurrentUser.UserId.ToString()))
                {
                    interactionUserId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);
                }
                else
                {
                    RedirectToAction(Constants.ActionLogin, Constants.AccountController);
                }
            }
            else
            {
                RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }
        }

        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.CurrentCulture);

        /// <summary>
        /// Opens the interaction menu.
        /// </summary>
        /// <returns>action result</returns>
        [HttpGet]
        public ActionResult OpenInteractionMenu()
        {
            InteractionViewModel interactionViewModel = new InteractionViewModel();
            interactionViewModel.IsCalledFromInteractionMenu = Convert.ToString(Session[Constants.SessionIsCalledFromInteractionMenu], CultureInfo.InvariantCulture);
            interactionViewModel.PatientName = Convert.ToString(Session[Constants.SessionInteractionPatientName], CultureInfo.InvariantCulture);
            interactionViewModel.CarehomeName = Convert.ToString(Session[Constants.SessionInteractionCareHomeName], CultureInfo.InvariantCulture);
            interactionViewModel.CustomerName = Convert.ToString(Session[Constants.SessionInteractionCustomerName], CultureInfo.InvariantCulture);
            interactionViewModel.InteractionId = Convert.ToString(Session[Constants.InteractionId], CultureInfo.InvariantCulture);
            interactionViewModel.OpenCarehomeFlag = Convert.ToString(Session[Constants.SessionOpenCarehomeFlag], CultureInfo.InvariantCulture);
            return this.View(Constants.ActionNameOpenInteractionMenu, interactionViewModel);
        }

        /// <summary>
        /// Open Interaction Report.
        /// </summary>
        /// <returns>action result</returns>
        [HttpGet]
        public ActionResult InteractionReport()
        {            
            InteractionReportViewModel interactionreportViewModel = new InteractionReportViewModel();
            interactionreportViewModel.UserId = interactionUserId;
            interactionreportViewModel = InteractionHelper.GetDropDownListValues(interactionreportViewModel);
            return View(interactionreportViewModel);
        }

        /// <summary>
        /// This function will get interaction report data from database in json format.
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="rows">No of records count</param>
        /// <param name="sidx">Sorting index</param>
        /// <param name="sord">Sort by (Asc or Desc)</param>
        /// <param name="isExport">Is export or not (True or False)</param>
        /// <param name="CustomersIds">Customer ids</param>
        /// <param name="PatientIds">Patient ids</param>
        /// <param name="CareHomeIds">Carehome ids</param>
        /// <param name="InteractionTypeId">Interaction type id</param>
        /// <param name="InteractionSubTypeId">Ineraction sub type id</param>
        /// <param name="StatusId">Status id</param>
        /// <param name="CreatedBy">Created by</param>
        /// <param name="CreatedDateFrom">Created date from</param>
        /// <param name="CreatedDateTo">Created date to</param>
        /// <param name="ModifiedDateFrom">Modified date from</param>
        /// <param name="ModifiedDateTo">Modified date to</param>
        /// <returns>Action result</returns>
        public ActionResult GetInteractionReport(long page, long rows, string sidx, string sord,
                                                 bool isExport, string CustomersIds, string PatientIds, string CareHomeIds,
                                                 string InteractionTypeId, string InteractionSubTypeId, string StatusId, string CreatedBy,
                                                 string CreatedDateFrom, string CreatedDateTo, string ModifiedDateFrom, string ModifiedDateTo)
        {
            long totalRecords = 0;
            var totalPages = 0;

            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }
            InteractionReportDetailsBusinessModel interReportDetailsModel = new InteractionReportDetailsBusinessModel
            {
                TotalRecords = rows,
                RowNumber = page,
                IsExport = isExport,
                CustomersIds = CustomersIds,
                PatientIds = PatientIds,
                CareHomeIds = CareHomeIds,
                InteractionTypeId = InteractionTypeId,
                InteractionSubTypeId = InteractionSubTypeId,
                StatusId = StatusId,
                CreatedBy = CreatedBy,
                CreatedDateFrom = CreatedDateFrom,
                CreatedDateTo = CreatedDateTo,
                ModifiedDateFrom = ModifiedDateFrom,
                ModifiedDateTo = ModifiedDateTo,
                SortColumn = sidx,
                SortBy = sord,
                UserId = interactionUserId
            };
            var interactionReportDataList = GetInteractionReportData(interReportDetailsModel);
            if (isExport)
            {
                if (interactionReportDataList != null && interactionReportDataList.Count > 0)
                {
                    var returnpath = string.Empty;
                    string interactionReportServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.InteractionReportSharedLocation);
                    string fileNamePrefix = Constants.InteractionReportPrefix;
                    var columnsToRemove = new List<string>() { "TotalRecords", "RowNumber" };

                    if (interactionReportDataList.Count > 0)
                    {
                        returnpath = ExcelHelper.ExportToExcel(interactionReportDataList, interactionReportServerDirectory, fileNamePrefix, columnsToRemove);
                    }
                    return Json(returnpath, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (interactionReportDataList != null && interactionReportDataList.Count > 0)
                {
                    totalRecords = interactionReportDataList[0].TotalRecords;
                    totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
                }
                var interactionReportJson = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = interactionReportDataList,
                };
                return this.Json(interactionReportJson, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the Interaction Sub Type.
        /// </summary>
        /// <param name="Customerid">The customerid.</param>
        /// <returns>Action result</returns>
        //[HttpGet]
        //public ActionResult GetInteractionSubType(string customerId)
        //{
        //    var guidKey = Guid.NewGuid();
        //    bool bFlag = false;
        //    ControllerHelper.ActionName = Constants.ActionGetCustomerPatientType;
        //    ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture) };
        //    var responsePatientType = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
        //    if (responsePatientType == null)
        //    {
        //        return this.Json(bFlag, JsonRequestBehavior.AllowGet);
        //    }

        //    return this.Json(responsePatientType, JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// Gets the user ids based on customer ids.
        /// </summary>
        /// <param name="customerIds">The customer identifier.</param>
        /// <returns>
        /// Gets the user ids (Createdby values) based on customer identifier.
        /// </returns>
        [HttpGet]
        public ActionResult GetCreatedby(string customerIds)
        {
            if (userPasswordExpired)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }

            SelectList createdByList = GenericHelper.GetInterationReportListForUsers(interactionUserId, customerIds);
            return this.Json(createdByList, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This function will generate pdf file for interaction report data.
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="rows">No of records count</param>
        /// <param name="isExport">Is export or not (True or False)</param>
        /// <param name="CustomersIds">Customer ids</param>
        /// <param name="PatientIds">Patient ids</param>
        /// <param name="CareHomeIds">Carehome ids</param>
        /// <param name="InteractionTypeId">Interaction type id</param>
        /// <param name="InteractionSubTypeId">Ineraction sub type id</param>
        /// <param name="StatusId">Status id</param>
        /// <param name="CreatedBy">Created by</param>
        /// <param name="CreatedDateFrom">Created date from</param>
        /// <param name="CreatedDateTo">Created date to</param>
        /// <param name="ModifiedDateFrom">Modified date from</param>
        /// <param name="ModifiedDateTo">Modified date to</param>
        /// <returns>Action result</returns>
        public ActionResult GenerateInteractionReportPdf(long page, long rows, bool isExport, string CustomersIds,
                                                         string PatientIds, string CareHomeIds, string InteractionTypeId,
                                                         string InteractionSubTypeId, string StatusId, string CreatedBy,
                                                         string CreatedDateFrom, string CreatedDateTo, string ModifiedDateFrom,
                                                         string ModifiedDateTo)
        {
            string pdfPath = string.Empty;
            InteractionReportDetailsBusinessModel interReportDetailsModel = new InteractionReportDetailsBusinessModel
            {
                TotalRecords = rows,
                RowNumber = page,
                IsExport = isExport,
                CustomersIds = CustomersIds,
                PatientIds = PatientIds,
                CareHomeIds = CareHomeIds,
                InteractionTypeId = InteractionTypeId,
                InteractionSubTypeId = InteractionSubTypeId,
                StatusId = StatusId,
                CreatedBy = CreatedBy,
                CreatedDateFrom = CreatedDateFrom,
                CreatedDateTo = CreatedDateTo,
                ModifiedDateFrom = ModifiedDateFrom,
                ModifiedDateTo = ModifiedDateTo,
                UserId = interactionUserId
            };
            var interactionReportDataList = GetInteractionReportData(interReportDetailsModel);
            if (interactionReportDataList != null && interactionReportDataList.Count > 0)
            {
                string interactionReportServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.PDFSharedLocation + "\\" + GenericHelper.InteractionReportSharedLocation);
                string fileNamePrefix = Constants.InteractionReportPrefix;
                var columnsToRemove = new List<string>() { "TotalRecords", "RowNumber" };

                var pdfHeaderName = new List<string>() {
                        Common.resInterReportCustName,Common.resInterReportCustid, 
                        Common.resInterReportPatName,Common.resInterReportPatID, 
                        Common.resInterReportCHName, Common.resInterReportCHId, 
                        Common.resInterReportInteractiontype,Common.resInterReportInteractionSubtype,
                        Common.resInterReportStatus, Common.resInterReportCreatedOn, 
                        Common.resInterReportCreatedBy, Common.resInterReportModifiedOn,
                        Common.resInterReportModifiedBy, Common.resInterReportDescription,
                        Common.resInterReportAlert, Common.resInterReportAssignedTo
                    };

                if (interactionReportDataList.Count > 0)
                {
                    pdfPath = PDFHelper.CreateInteractionReportPDF(interactionReportDataList, interactionReportServerDirectory, fileNamePrefix, columnsToRemove, pdfHeaderName);
                }
                return Json(pdfPath, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("-1", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This function will get interaction report data from database.
        /// </summary>
        /// <param name="interReportDetailsModel">Object of class "InteractionReportDetailsBusinessModel"</param>
        /// <returns></returns>
        private List<InteractionReportSummaryBusinessModel> GetInteractionReportData(InteractionReportDetailsBusinessModel interReportDetailsModel)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetInteractionReportSummary;
            return ControllerHelper.PostMethodServiceRequest<InteractionReportDetailsBusinessModel, List<InteractionReportSummaryBusinessModel>>(interReportDetailsModel);
        }
    }
}