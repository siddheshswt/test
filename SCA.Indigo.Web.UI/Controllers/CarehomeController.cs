﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind Nishad
// Created          : 03-04-2015
//
// ***********************************************************************
// <copyright file="CarehomeController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Carehome maintenance</summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Globalization;
    using System.Configuration;
    using SCA.Indigo.Common;
    using System.IO;
    using SCA.Indigo.Web.UI.Resource;

    /// <summary>
    /// Care Home Controller
    /// </summary>
    [Authorize]
    public class CareHomeController : Controller
    {
        // Global varriable        
        /// <summary>
        /// The object care home view model
        /// </summary>
        CareHomeViewModel objCareHomeViewModel = new CareHomeViewModel();
        /// <summary>
        /// The object care home helper
        /// </summary>
        CareHomeHelper objCareHomeHelper = new CareHomeHelper();

        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// NewCarehomeRound
        /// </summary>
        private string newCarehomeRound = ConfigurationManager.AppSettings["NewCarehomeRound"].ToUpper();

        /// <summary>
        /// NewCarehomeRoundId
        /// </summary>
        private string newCarehomeRoundId = ConfigurationManager.AppSettings["NewCarehomeRoundId"].ToUpper();
        // Global varriable        
        /// <summary>
        /// The object care home report view model
        /// </summary>
        CareHomeReportDetails objCareHomeReportDetails = new CareHomeReportDetails();
        /// <summary>
        /// Load Default view for carehome
        /// </summary>
        /// <returns>
        /// return carehome view
        /// </returns>
        [HttpGet]
        public ActionResult CareHomeMaintenance()
        {
            if (ControllerHelper.CurrentUser.UserName != null)
            {
                InitializeCarehome();
                objCareHomeViewModel.CreatedBy = ControllerHelper.CurrentUser.UserName;
                objCareHomeViewModel.CreatedDate = DateTime.Now.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                objCareHomeViewModel.Round = newCarehomeRound;
                objCareHomeViewModel.RoundId = newCarehomeRoundId;
                objCareHomeViewModel.HeaderData = new CareHomeReportViewModel() { DetailsData = new List<CareHomeReportDetails>() };
            }
            return View(objCareHomeViewModel);
        }


        /// <summary>
        /// Set Customer Session
        /// </summary>
        /// <param name="customerid">string customerid</param>
        /// <returns>
        /// Action Result
        /// </returns>
        public ActionResult SetCustomerParameter(long customerid)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("SetCustomerParameter Start - " + guidKey, "CareHome", "SetCustomerParameter Start", globalUserId);
            var customerParameters = GetCustomerParameter(customerid);
            SetNDDStartAndEndDate(customerParameters);
            LogHelper.LogAction("SetCustomerParameter End - " + guidKey, "CareHome", "SetCustomerParameter End", globalUserId);
            return this.Json(objCareHomeViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// SetNDDStartAndEndDate
        /// </summary>
        /// <param name="customerParameter"></param>
        private void SetNDDStartAndEndDate(List<CustomerParameterBusinessModel> customerParameter)
        {
            var orderLeadTime = 0;
            var countryId = 0;
            if (customerParameter != null && customerParameter.Any())
            {
                orderLeadTime = customerParameter.First().OrderLeadTime;
                countryId = customerParameter.First().CountryId;
            }
            DateTime startdate = GenericHelper.AddWorkingDays(DateTime.Now, orderLeadTime, countryId);
            objCareHomeViewModel.NddStartDate = startdate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
            int numberofDays = Convert.ToInt32(ConfigurationManager.AppSettings["AddDaysInStartDateForChangeNDD"], CultureInfo.InvariantCulture);
            objCareHomeViewModel.NddEndDate = startdate.AddDays(numberofDays).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);

        }


        /// <summary>
        /// GetCustomerParameter
        /// </summary>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public List<CustomerParameterBusinessModel> GetCustomerParameter(long customerid)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetCustomerParameter Start - " + guidKey, "CareHome", "GetCustomerParameter Start", globalUserId);

            if (customerid > 0)
            {
                ControllerHelper.ActionName = Constants.ActionGetCustomerParameter;
                var objcustomerParameterBusinessModel = new CustomerParameterBusinessModel
                {
                    CustomerID = customerid,
                    UserID = globalUserId
                };
                var customerParameters =
                    ControllerHelper.PostMethodServiceRequest<CustomerParameterBusinessModel, List<CustomerParameterBusinessModel>>(
                        objcustomerParameterBusinessModel);
                LogHelper.LogAction("GetCustomerParameter End - " + guidKey, "CareHome", "GetCustomerParameter End", globalUserId);
                return customerParameters;
            }
            LogHelper.LogAction("GetCustomerParameter End - " + guidKey, "CareHome", "GetCustomerParameter End", globalUserId);
            return null;
        }


        /// <summary>
        /// Get carehome details
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult GetCareHome()
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetCareHome Start - " + guidKey, "CareHome", "GetCareHome Start", globalUserId);
            objCareHomeViewModel = new CareHomeViewModel();
            objCareHomeViewModel.ShowViewAlert = true;
            if (Session[Constants.SessionCareHomeId] != null)
            {
                objCareHomeViewModel = objCareHomeHelper.GetCareHome(Convert.ToString(Session[Constants.SessionCareHomeId], CultureInfo.InvariantCulture));
                objCareHomeViewModel.ShowViewAlert = false;

                Session[Constants.SessionCustomerId] = objCareHomeViewModel.CustomerId;
                var customerDisplayName = objCareHomeViewModel.CustomerId + " " + objCareHomeViewModel.CustomerName;
                objCareHomeViewModel.SAPCustomerNumber = CommonHelper.TrimLeadingZeros(objCareHomeViewModel.SAPCustomerNumber);
                customerDisplayName = !string.IsNullOrEmpty(objCareHomeViewModel.SAPCustomerNumber) ? customerDisplayName.Contains(objCareHomeViewModel.SAPCustomerNumber) ? customerDisplayName : customerDisplayName + "(" + objCareHomeViewModel.SAPCustomerNumber + ")" : customerDisplayName;
                Session[Constants.SessionCustomerDisplayName] = Convert.ToString(customerDisplayName, CultureInfo.CurrentCulture);

                var carehomeDisplayName = objCareHomeViewModel.CareHomeId + " " + objCareHomeViewModel.CareHomeName + " " + objCareHomeViewModel.PostCode;
                if (!string.IsNullOrEmpty(objCareHomeViewModel.SapCareHomeNumber))
                {
                    carehomeDisplayName += " (" + objCareHomeViewModel.SapCareHomeNumber + ")";
                }
                Session[Constants.SessionCareHomeDisplayName] = Convert.ToString(carehomeDisplayName, CultureInfo.CurrentCulture);
                Session[Constants.SessionCareHomeId] = objCareHomeViewModel.CareHomeId;
            }
            InitializeCarehome();
            objCareHomeViewModel.FileCountErrorMessage = string.Format(CultureInfo.InvariantCulture, Resource.Common.resFileCountErrorMessage, ConfigurationManager.AppSettings["FileCount"].ToString(CultureInfo.InvariantCulture));
            objCareHomeViewModel.IsCalledFromHomePage = Convert.ToString(Session[Constants.IsCalledFromHomePage], CultureInfo.InvariantCulture);
            objCareHomeViewModel.CareHomeInteractionId = Convert.ToString(Session[Constants.InteractionId], CultureInfo.InvariantCulture);
            objCareHomeViewModel.IsCalledFromSearchPage = Convert.ToString(Session[Constants.SessionIsCalledFromSearch], CultureInfo.InvariantCulture);
            objCareHomeViewModel.IsCalledFromInteractionMenu = Convert.ToString(Session[Constants.SessionIsCalledFromInteractionMenu], CultureInfo.InvariantCulture);
            if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromCareHome], CultureInfo.InvariantCulture))
            {
                Session[Constants.SessionIsCalledFromSearch] = true;
                objCareHomeViewModel.IsCalledFromSearchPage = Convert.ToString(Session[Constants.SessionIsCalledFromSearch], CultureInfo.InvariantCulture);
            }
            GetNDDDateRangeForCarehome(objCareHomeViewModel.CareHomeId, objCareHomeViewModel.CustomerId);
            LogHelper.LogAction("GetCareHome End - " + guidKey, "CareHome", "GetCareHome End", globalUserId);

            SetCarehomeReportHeader(objCareHomeViewModel);
            objCareHomeViewModel.HeaderData = new CareHomeReportViewModel() { DetailsData = new List<CareHomeReportDetails>() };

            return View("CareHomeMaintenance", objCareHomeViewModel);
        }

        /// <summary>
        /// Set carehome report header
        /// </summary>
        /// <param name="carehomeReportHeader"></param>
        public void SetCarehomeReportHeader(CareHomeViewModel baseObject)
        {
            CareHomeHelper.CarehomeHeader objcarehomeHeader = new CareHomeHelper.CarehomeHeader();

            objcarehomeHeader.CustomerNameID = baseObject.CustomerName + " (" + baseObject.SAPCustomerNumber + ")";
            objcarehomeHeader.CarehomeNameID = baseObject.CareHomeName + " (" + baseObject.SapCareHomeNumber + ")";
            objcarehomeHeader.CarehomeAddress = baseObject.Address1 + ", " + baseObject.Address2 + ", " + baseObject.PostCode;
            objcarehomeHeader.CarehomePhoneNo = baseObject.PhoneNo;
            objcarehomeHeader.CarehomeNDD = baseObject.NextDeliveryDate;
            objcarehomeHeader.CarehomeFrequency = baseObject.DeliveryFrequency;

            Session[Constants.SessionCarehomeHeaderData] = objcarehomeHeader;
            //Session["CustomerNameID"] = baseObject.CustomerName + " (" + baseObject.SAPCustomerNumber + ")";
            //Session["CareHomeNameID"] = baseObject.CareHomeName + " (" + baseObject.SapCareHomeNumber + ")";
            //Session["CareHomeAddress"] = baseObject.Address1 + ", " + baseObject.Address2 + ", " + baseObject.PostCode;
            //Session["CareHomePhoneNo"] = baseObject.PhoneNo;
            //Session["CareHomeNDD"] = baseObject.NextDeliveryDate;
            //Session["CareHomeFrequency"] = baseObject.DeliveryFrequency;
        }

        /// <summary>
        /// Get NDD Date Range For Carehome
        /// </summary>
        /// <param name="careHomeId">careHomeId</param>
        /// <param name="customerId">customerId</param>
        private void GetNDDDateRangeForCarehome(long careHomeId, long customerId)
        {
            if (careHomeId > 0)
            {
                //get post code matrix                
                ControllerHelper.ActionName = Constants.ActionNameGetPostCodeMatrix;
                ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), "0", careHomeId.ToString(CultureInfo.InvariantCulture), ControllerHelper.CurrentUser.UserId.ToString() };
                var postCodeMatrix = ControllerHelper.GetMethodServiceRequestForList<List<PostCodeMatrixBusinessModel>>();
                if (postCodeMatrix == null)
                {
                    //to avoid any exception related to no post code matrix for patient or service unable to retrive postcode matrix due to fail call
                    postCodeMatrix = new List<PostCodeMatrixBusinessModel>();
                    postCodeMatrix.Select(q => new PostCodeMatrixBusinessModel
                    {
                        CustomerID = customerId,
                        PostcodeMatrixID = 0,
                        Monday = true,
                        Tuesday = true,
                        Wednesday = true,
                        Thursday = true,
                        Friday = true,
                        RoundID = "0",
                        Round = "0",
                        Postcode = "0"
                    }).ToList();

                }

                // var deliveryDate = DateTime.ParseExact(patientInfo.DeliveryDate, DateFormat, CultureInfo.InvariantCulture);
                var nextDeliveryDate = DateTime.ParseExact(objCareHomeViewModel.NextDeliveryDate, Constants.DateFormat, CultureInfo.InvariantCulture);

                var customerParameter = GetCustomerParameter(customerId);
                nextDeliveryDate = GenericHelper.CalculateNextDeliveryDate(nextDeliveryDate, postCodeMatrix, customerParameter);

                if (nextDeliveryDate != default(DateTime))
                {
                    var NDDStartDate = nextDeliveryDate;
                    var orderLeadTime = 0;
                    var countryId = 0;
                    if (customerParameter != null && customerParameter.Any())
                    {
                        orderLeadTime = customerParameter.First().OrderLeadTime;
                        countryId = customerParameter.First().CountryId;
                    }

                    if (NDDStartDate < GenericHelper.AddWorkingDays(DateTime.Today, orderLeadTime, countryId))
                    {
                        NDDStartDate = GenericHelper.AddWorkingDays(DateTime.Today, orderLeadTime, countryId);
                    }

                    objCareHomeViewModel.NddStartDate = NDDStartDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);

                    //Session[Constants.SessionNDDEndDate] = nextDeliveryDate.ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                    //End date will be always satrt date + 98 calendar days
                    int numberofDays = Convert.ToInt32(ConfigurationManager.AppSettings["AddDaysInStartDateForChangeNDD"], CultureInfo.InvariantCulture);
                    objCareHomeViewModel.NddEndDate = NDDStartDate.AddDays(numberofDays).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                }

            }
        }


        /// <summary>
        /// Save carehome details
        /// </summary>
        /// <param name="objCareHomeViewModel">CareHome ViewModel</param>
        /// <returns>
        /// return json
        /// </returns>
        [HttpPost]
        public ActionResult SaveCarehome(CareHomeViewModel careHomeViewModel)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("SaveCarehome Start - " + guidKey, "CareHome", "SaveCarehome Start", globalUserId);

            long careHomeId = objCareHomeHelper.SaveCareHome(careHomeViewModel);

            Session[Constants.SessionCustomerId] = careHomeViewModel.CustomerId;
            var customerDisplayName = careHomeViewModel.CustomerId + " " + careHomeViewModel.CustomerName;
            Session[Constants.SessionCustomerDisplayName] = Convert.ToString(customerDisplayName, CultureInfo.CurrentCulture);

            var carehomeDisplayName = careHomeId + " " + careHomeViewModel.CareHomeName + " " + careHomeViewModel.PostCode;
            if (!string.IsNullOrEmpty(careHomeViewModel.SapCareHomeNumber))
            {
                carehomeDisplayName += " (" + careHomeViewModel.SapCareHomeNumber + ")";
            }
            Session[Constants.SessionCareHomeDisplayName] = Convert.ToString(carehomeDisplayName, CultureInfo.CurrentCulture);

            Session[Constants.SessionCareHomeId] = Convert.ToString(careHomeId, CultureInfo.CurrentCulture);
            Session[Constants.SessionCareHomeName] = careHomeViewModel.CareHomeName;
            Session[Constants.SessionCareHomeSAPId] = careHomeViewModel.SapCareHomeNumber;
            LogHelper.LogAction("SaveCarehome End - " + guidKey, "CareHome", "SaveCarehome End", globalUserId);
            return this.Json(careHomeId, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// Sets the care home session.
        /// </summary>
        [HttpPost]
        public void SetCareHomeSession()
        {
            Session[Constants.SessionIsCalledFromCareHome] = true;
            Session[Constants.SessionIsCalledFromSearch] = false;
            Session[Constants.IsCalledFromHomePage] = false;
            Session[Constants.SessionIsCalledFromInteractionMenu] = false;
        }


        /// <summary>
        /// Get all patient attached with carehome
        /// </summary>
        /// <param name="careHomeID">carehome id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCareHomePatientList(string careHomeId)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetCareHomePatientList Start - " + guidKey, "CareHome", "GetCareHomePatientList Start", globalUserId);

            var patientData = new CareHomeHelper().GetCareHomePatientList(careHomeId);
            LogHelper.LogAction("GetCareHomePatientList End - " + guidKey, "CareHome", "GetCareHomePatientList End", globalUserId);
            return this.Json(patientData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Initialize carehome view
        /// </summary>
        private void InitializeCarehome()
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("InitializeCarehome Start - " + guidKey, "CareHome", "InitializeCarehome Start", globalUserId);

            FillDropDown();
            //Set User Type
            objCareHomeViewModel.UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);
            LogHelper.LogAction("InitializeCarehome End - " + guidKey, "CareHome", "InitializeCarehome End", globalUserId);
        }

        /// <summary>
        /// Gets the home page flag.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetHomePageFlag()
        {
            if (Convert.ToBoolean(Session[Constants.IsCalledFromHomePage], CultureInfo.InvariantCulture))
            {
                return this.Json(Url.Action(Constants.ActionWelcome, Constants.HomeController));
            }
            else if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromSearch], CultureInfo.InvariantCulture))
            {
                return this.Json(Url.Action(Constants.ActionSearchData, Constants.SearchController));
            }
            else if (Convert.ToBoolean(Session[Constants.SessionIsCalledFromInteractionMenu], CultureInfo.InvariantCulture))
            {
                return this.Json(Url.Action(Constants.ActionNameOpenInteractionMenu, Constants.InteractionController));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Fill All dropdowns value
        /// </summary>
        private void FillDropDown()
        {
            //Fill Customer Dropdown
            ControllerHelper.ActionName = Constants.ActionNameGetCustomerList;
            ControllerHelper.ActionParam = new[] { globalUserId };
            var customerResponse = ControllerHelper.GetMethodServiceRequest<List<ListTypeBusinessModel>>();
            objCareHomeViewModel.CustomersList = new SelectList(customerResponse, Constants.ListId, Constants.DisplayText, objCareHomeViewModel.CustomerId);

            // Fill County Dropdown
            var countyList = new List<CountyBusinessModel>();
            ControllerHelper.ActionName = Constants.ActionNameGetCountyList;
            countyList = ControllerHelper.GetMethodServiceRequestForList<List<CountyBusinessModel>>();
            if (countyList != null)
            {
                countyList = countyList.OrderBy(x => x.CountyText).ToList();
            }
            objCareHomeViewModel.CountyList = new SelectList(countyList, Constants.CountyCode, Constants.CountyText, objCareHomeViewModel.County);

            // Get List Type Details for Carehome
            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            //var CareHomeTypeId = Convert.ToInt64(SCAEnums.ListType.CareHomeType);
            var CommunicationFormatId = Convert.ToInt64(SCAEnums.ListType.CommunicationFormat, CultureInfo.InvariantCulture);
            var CareHomeStatusId = Convert.ToInt64(SCAEnums.ListType.CareHomeStatus, CultureInfo.InvariantCulture);
            var CareHomeOrderTypeId = Convert.ToInt64(SCAEnums.ListType.CareHomeOrderType, CultureInfo.InvariantCulture);
            ControllerHelper.ActionParam = new[]
            {
                CommunicationFormatId + "," + CareHomeStatusId + "," + CareHomeOrderTypeId , string.Empty + Convert.ToInt32(SCAEnums.ListType.LanguageId,CultureInfo.InvariantCulture)
            };

            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();

            var CommunicationFormat = responseData.Where(a => a.ListTypeId == CommunicationFormatId).ToList();

            var CareHomeStatus = responseData.Where(a => a.ListTypeId == CareHomeStatusId).ToList();

            var CareHomeOrderType = responseData.Where(a => a.ListTypeId == CareHomeOrderTypeId).ToList();

            //Fill CareHome Status Dropdown
            if (objCareHomeViewModel.CareHomeId < 1)
            {
                objCareHomeViewModel.CarehomeStatusList = new SelectList(CareHomeStatus, Constants.ListId, Constants.DisplayText, Convert.ToInt64(SCAEnums.CareHomeStatus.Active, CultureInfo.InvariantCulture));
            }
            else
            {
                objCareHomeViewModel.CarehomeStatusList = new SelectList(CareHomeStatus, Constants.ListId, Constants.DisplayText, objCareHomeViewModel.CareHomeStatus);
            }
            //Fill CareHome Order Type Dropdown            
            objCareHomeViewModel.CarehomeOrderTypeList = new SelectList(CareHomeOrderType, Constants.ListId, Constants.DisplayText, objCareHomeViewModel.OrderType);
            // Fill Communication Format
            objCareHomeViewModel.CommunicationFormatList = new SelectList(CommunicationFormat, Constants.ListId, Constants.DisplayText, objCareHomeViewModel.CommunicationFormat);
        }

        /// <summary>
        /// Gets the care home sub module access.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCareHomeSubModuleAccess()
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetCareHomeSubModuleAccess Start - " + guidKey, "CareHome", "GetCareHomeSubModuleAccess Start", globalUserId);

            // Set 1 = Full Rights, 0 = View Only , blank = No Acess
            string contactInfoAccess = string.Empty;
            string interactionAccess = string.Empty;
            string attachmentAccess = string.Empty;
            string notesAccess = string.Empty;
            string recentOrdersAccess = string.Empty;
            string auditLogAccess = string.Empty;
            string UserType = ControllerHelper.CurrentUser.UserType.ToString(CultureInfo.InvariantCulture);

            List<UserMenuDetailsBusinessModel> allRoleMenuDetails = (List<UserMenuDetailsBusinessModel>)Session[Constants.SessionAuthorizedUrl];

            // Check for the Patient Sub Module Access
            var contactInfoObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CareHomeContactInfo);
            if (contactInfoObj != null)
            {
                if (contactInfoObj.FullRights)
                {
                    contactInfoAccess = "1";
                }
                else if (contactInfoObj.ViewOnly)
                {
                    contactInfoAccess = "0";
                }
            }
            var interactionObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CareHomeInteraction);
            if (interactionObj != null)
            {
                if (interactionObj.FullRights)
                {
                    interactionAccess = "1";
                }
                else if (interactionObj.ViewOnly)
                {
                    interactionAccess = "0";
                }
            }
            var attachmentObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CareHomeAttachments);
            if (attachmentObj != null)
            {
                if (attachmentObj.FullRights)
                {
                    attachmentAccess = "1";
                }
                else if (attachmentObj.ViewOnly)
                {
                    attachmentAccess = "0";
                }
            }

            var notesObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CareHomeNotes);
            if (notesObj != null)
            {
                if (notesObj.FullRights)
                {
                    notesAccess = "1";
                }
                else if (notesObj.ViewOnly)
                {
                    notesAccess = "0";
                }
            }

            var recentOrdersObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CareHomeRecentOrders);
            if (recentOrdersObj != null)
            {
                if (recentOrdersObj.FullRights)
                {
                    recentOrdersAccess = "1";
                }
                else if (recentOrdersObj.ViewOnly)
                {
                    recentOrdersAccess = "0";
                }
            }

            var auditLogObj = allRoleMenuDetails.Find(d => d.MenuId == (int)SCAEnums.Menu.CarehomeAuditLog);
            if (auditLogObj != null)
            {
                if (auditLogObj.FullRights)
                {
                    auditLogAccess = "1";
                }
                else if (auditLogObj.ViewOnly)
                {
                    auditLogAccess = "0";
                }
            }

            LogHelper.LogAction("GetCareHomeSubModuleAccess End - " + guidKey, "CareHome", "GetCareHomeSubModuleAccess End", globalUserId);
            return this.Json(new
            {
                ContactInfoAccess = contactInfoAccess,
                InteractionAccess = interactionAccess,
                AttachmentAccess = attachmentAccess,
                NotesAccess = notesAccess,
                RecentOrdersAccess = recentOrdersAccess,
                AuditLogAccess = auditLogAccess,
                UserType

            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets NDD related details for carehome
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="carehomeId"></param>
        /// <returns></returns>
        public ActionResult GetCarehomeNDDRelatedDetails(int customerId, int carehomeId)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetCarehomeNDDRelatedDetails Start - " + guidKey, "CareHome", "GetCarehomeNDDRelatedDetails Start", globalUserId);

            NddDetailsBusinessModel nddDetailsBusinessModel = new NddDetailsBusinessModel();
            var patientId = 0;//set as 0 as it is for carehome
            ControllerHelper.ActionName = Constants.ActionNameGetPostCodeMatrix;
            ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), patientId.ToString(CultureInfo.InvariantCulture), carehomeId.ToString(CultureInfo.InvariantCulture), ControllerHelper.CurrentUser.UserId.ToString() };
            var postCodeMatrix = ControllerHelper.GetMethodServiceRequestForList<List<PostCodeMatrixBusinessModel>>();
            if (postCodeMatrix != null && postCodeMatrix.Any())
            {
                nddDetailsBusinessModel.Day1 = postCodeMatrix.First().Monday == true ? 0 : 1;
                nddDetailsBusinessModel.Day2 = postCodeMatrix.First().Tuesday == true ? 0 : 2;
                nddDetailsBusinessModel.Day3 = postCodeMatrix.First().Wednesday == true ? 0 : 3;
                nddDetailsBusinessModel.Day4 = postCodeMatrix.First().Thursday == true ? 0 : 4;
                nddDetailsBusinessModel.Day5 = postCodeMatrix.First().Friday == true ? 0 : 5;
                nddDetailsBusinessModel.RoundID = postCodeMatrix.First().RoundID;
            }
            else
            {
                //to avoid any exception related to no post code matrix for patient or service unable to retrive postcode matrix due to fail call
                nddDetailsBusinessModel.Day1 = 0;
                nddDetailsBusinessModel.Day2 = 0;
                nddDetailsBusinessModel.Day3 = 0;
                nddDetailsBusinessModel.Day4 = 0;
                nddDetailsBusinessModel.Day5 = 0;
                nddDetailsBusinessModel.RoundID = "0";
            }
            LogHelper.LogAction("GetCarehomeNDDRelatedDetails End - " + guidKey, "CareHome", "GetCarehomeNDDRelatedDetails End", globalUserId);
            return this.Json(nddDetailsBusinessModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get holidaylist for carehome module based on customerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult GetHolidayListForCarehome(long customerId)
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetHolidayListForCarehome Start - " + guidKey, "CareHome", "GetHolidayListForCarehome Start", globalUserId);

            var countryId = 0;
            var customerParameter = (List<CustomerParameterBusinessModel>)Session[Constants.SessionCustomerParameters];
            if (customerParameter != null && customerParameter.Count > 0)
            {
                countryId = customerParameter[0].CountryId;
            }
            else
            {
                var customerParameters = GetCustomerParameter(customerId);
                countryId = customerParameters[0].CountryId;
            }
            List<HolidayBusinessModel> HolidayList = GenericHelper.GetHolidayList(countryId);
            LogHelper.LogAction("GetHolidayListForCarehome End - " + guidKey, "CareHome", "GetHolidayListForCarehome End", globalUserId);
            return Json(HolidayList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get carehome report data
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCareHomeReportHeaderData()
        {
            var guidKey = Guid.NewGuid();
            LogHelper.LogAction("GetCareHomePatientList Start - " + guidKey, "CareHome", "GetCareHomeReportData Start", globalUserId);

            CareHomeViewModel carehomeReportData = new CareHomeViewModel() { HeaderData = new CareHomeReportViewModel() };

            if (Session[Constants.SessionCareHomeId] != null)
            {
                var carehomeHeaderData = (CareHomeHelper.CarehomeHeader)Session[Constants.SessionCarehomeHeaderData];
                carehomeReportData.HeaderData.CustomerNameId = carehomeHeaderData.CustomerNameID;
                carehomeReportData.HeaderData.CareHomeNameId = carehomeHeaderData.CarehomeNameID;
                carehomeReportData.HeaderData.CarehomeAddress = carehomeHeaderData.CarehomeAddress;
                carehomeReportData.HeaderData.CarehomePhoneNo = carehomeHeaderData.CarehomePhoneNo;
                carehomeReportData.HeaderData.CarehomeNDD = carehomeHeaderData.CarehomeNDD;
                carehomeReportData.HeaderData.CarehomeFrequency = carehomeHeaderData.CarehomeFrequency;
            }

            LogHelper.LogAction("GetCareHomePatientList End - " + guidKey, "CareHome", "GetCareHomeReportData End", globalUserId);
            return Json(carehomeReportData.HeaderData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get carehome Details In JSON
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="rows">No of records count</param>
        /// <param name="sidx">Sort Index</param>
        /// <param name="sord">Sort Order</param>
        /// <param name="isExport">Export To Excel</param>
        /// <param name="careHomeId">Care Home Ids</param>
        /// <param name="patientUserId">Patient User Id</param>
        /// <param name="auditSearchUserId">Audit Search User Id</param>
        /// <returns></returns>
        public JsonResult GetCareHomeReportDetailsData(long page, long rows, string sidx, string sord, bool isExport, string careHomeId, string patientUserId, string auditSearchUserId)
        {
            long totalRecords = 0;
            var totalPages = 0;

            Guid UserId;
            Guid.TryParse(auditSearchUserId, out UserId);

            var carehomeInfoData = new List<CarehomeMaintenanceReportBusinessModel>();
            PatientBusinessModel PatientDetail = new PatientBusinessModel
            {
                IsExport = isExport,
                RecordsPerPage = page,
                CareHomeId = Convert.ToInt64(careHomeId, CultureInfo.InvariantCulture),
                PageNumber = rows
            };

            var carehomereportResponse = objCareHomeHelper.GetCareHomeMaintanaceReportData(careHomeId, isExport, page, rows, sidx, sord);

            if (isExport)
            {
                if (carehomereportResponse.PatientDetailsReports != null && Session[Constants.SessionCareHomeId] != null && carehomereportResponse.PatientDetailsReports.Count > 0)
                {

                    var returnpath = string.Empty;
                    string auditLogServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.CareHomeSharedLocation);
                    string fileNamePrefix = Constants.CareHomePrefix;

                    CareHomeViewModel carehomeReportData = new CareHomeViewModel() { HeaderData = new CareHomeReportViewModel() };

                    var Headername = new List<string>() {
                          Common.resCarehomeHeadCustomerNameORID
						, Common.resCarehomeHeadCarehomeNDD
						, Common.resCarehomeHeadAssessmentDate
						, Common.resCarehomeHeadCarehomeNameORID
						, Common.resCarehomeHeadCarehomeFrequency
						, Common.resCarehomeHeadAssessingNurse
						, Common.resCarehomeHeadCarehomeAdd
						, Common.resCarehomeHeadCarehomePhoneNumber
						, Common.resCarehomeHeadSignature
                    };

                    var carehomeHeaderData = (CareHomeHelper.CarehomeHeader)Session[Constants.SessionCarehomeHeaderData];
                    carehomeReportData.HeaderData.CustomerNameId = carehomeHeaderData.CustomerNameID;
                    carehomeReportData.HeaderData.CareHomeNameId = carehomeHeaderData.CarehomeNameID;
                    carehomeReportData.HeaderData.CarehomeAddress = carehomeHeaderData.CarehomeAddress;
                    carehomeReportData.HeaderData.CarehomePhoneNo = carehomeHeaderData.CarehomePhoneNo;
                    carehomeReportData.HeaderData.CarehomeNDD = carehomeHeaderData.CarehomeNDD;
                    carehomeReportData.HeaderData.CarehomeFrequency = carehomeHeaderData.CarehomeFrequency;

                    var columnsToRemoveHDR = new List<string>() { "DetailsData", "PartialDetailsData" };
                    var hdrdt = ExcelHelper.ObjectToDataTable(carehomeReportData.HeaderData, columnsToRemoveHDR, false, Headername);
                    var columnsToRemoveDTL = new List<string>() { "TotalRecords", "PatientId" };
                    var dtldt = ExcelHelper.ListToDataTable(carehomereportResponse.PatientDetailsReports.ToList(), columnsToRemoveDTL, false);
                    dtldt.Columns[Constants.FrequencyColumnName].ColumnName = Common.resExcelHeaderFrequency;
                    var dt = ExcelHelper.MergeHeaderDetail(hdrdt, dtldt, Constants.hdrRow, Constants.hdrCol);
                    returnpath = ExcelHelper.ExportToExcel(dt, auditLogServerDirectory, fileNamePrefix);

                    return Json(returnpath, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (carehomereportResponse != null && carehomereportResponse.PatientDetailsReports.Count > 0)
                {
                    carehomeInfoData = carehomereportResponse.PatientDetailsReports.ToList();
                    totalRecords = carehomereportResponse.PatientDetailsReports[0].TotalRecords;
                    totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
                }

                var auditLogResponseJson = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = carehomeInfoData,
                };
                return Json(auditLogResponseJson, JsonRequestBehavior.AllowGet);
            }
            return Json(new CareHomeReportDetails(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get carehome Details For PDF 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCarhomeDetailsinPDF()
        {
            bool isExport = true;
            if (Session[Constants.SessionCareHomeId] != null)
            {
                string careHomeId = Session[Constants.SessionCareHomeId].ToString();
                var carehomereportResponse = objCareHomeHelper.GetCareHomeMaintanaceReportData(careHomeId, isExport, 1, 0, null, "DESC");

                if (carehomereportResponse.PatientDetailsReports != null &&
                    Session[Constants.SessionCareHomeId] != null &&
                    carehomereportResponse.PatientDetailsReports.Count > 0)
                {
                    var returnpath = string.Empty;
                    string PDFServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.PDFSharedLocation + "\\" + GenericHelper.CareHomeSharedLocation);
                    string fileNamePrefix = Constants.CareHomePrefix;

                    CareHomeViewModel carehomeReportData = new CareHomeViewModel() { HeaderData = new CareHomeReportViewModel() };

                    var carehomeHeaderData = (CareHomeHelper.CarehomeHeader)Session[Constants.SessionCarehomeHeaderData];
                    carehomeReportData.HeaderData.CustomerNameId = carehomeHeaderData.CustomerNameID;
                    carehomeReportData.HeaderData.CareHomeNameId = carehomeHeaderData.CarehomeNameID;
                    carehomeReportData.HeaderData.CarehomeAddress = carehomeHeaderData.CarehomeAddress;
                    carehomeReportData.HeaderData.CarehomePhoneNo = carehomeHeaderData.CarehomePhoneNo;
                    carehomeReportData.HeaderData.CarehomeNDD = carehomeHeaderData.CarehomeNDD;
                    carehomeReportData.HeaderData.CarehomeFrequency = carehomeHeaderData.CarehomeFrequency;

                    var Headername = new List<string>() {
                          Common.resCarehomeHeadCustomerNameORID
						, Common.resCarehomeHeadCarehomeNDD
						, Common.resCarehomeHeadAssessmentDate
						, Common.resCarehomeHeadCarehomeNameORID
						, Common.resCarehomeHeadCarehomeFrequency
						, Common.resCarehomeHeadAssessingNurse
						, Common.resCarehomeHeadCarehomeAdd
						, Common.resCarehomeHeadCarehomePhoneNumber
						, Common.resCarehomeHeadSignature
                    };
                    var PdfHeadername = new List<string>() { 
                          Common.respatientname 
                        , Common.resGrdPatientID 
                        , Common.resGrdPatientDOB
                        , Common.resPatientStatus
                        , Common.resGrdAssessmentDate
                        , Common.resGrdNextAssessmentDate
                        , Common.resGrdProductID
                        , Common.resGrdDescription
                        , Common.resGrdresAssPPD
                        , Common.resGrdresActPPD
                        , Common.resGrdPrescriptionFrequency 
                        , Common.resGrdPrescriptionNextDelieveryDate
                        , Common.resGrdPatientComment
                    };
                    var columnsToRemoveHDR = new List<string>() { "DetailsData", "PartialDetailsData" };
                    var hdrdt = ExcelHelper.ObjectToDataTable(carehomeReportData.HeaderData, columnsToRemoveHDR, false);

                    var columnsToRemoveDTL = new List<string>() { "TotalRecords" };
                    returnpath = PDFHelper.CreateCarehomeReportPDF(hdrdt, carehomereportResponse.PatientDetailsReports.ToList(), columnsToRemoveDTL, Headername, PdfHeadername, PDFServerDirectory, fileNamePrefix);

                    return Json(returnpath, JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }

        /// <summary>
        /// This function will set session value for DoNotStop functionality
        /// </summary>
        /// <param name="displayAsAlert"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetSessionForDoNotStop(string displayAsAlert)
        {
            if (!string.IsNullOrEmpty(displayAsAlert))
            {
                Session[Constants.SessionDoNotShowForCarehome] = displayAsAlert;
            }
            return Json(new { Message = 1 }, JsonRequestBehavior.AllowGet);
        }
    }
}