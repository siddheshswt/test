﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI.Controllers
// Author           : sphapale / bsahoo
// Created          : 09-03-2015
//
// Last Modified By : Basanta
// Last Modified On : 13- Mar-2015
// ***********************************************************************
// <copyright file="UserAdminController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel.UserAdmin;

    /// <summary>
    /// User Admin Controller
    /// </summary>
    [Authorize]
    public class UserAdminController : Controller
    {
        #region Map Role To User Functionality

        #region Constants

        /// <summary>
        /// User Id
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// User Admin Helper
        /// </summary>
        UserAdminHelper userAdminHelper = new UserAdminHelper();

        #endregion

        /// <summary>
        /// Get Map User Roles
        /// </summary>
        /// <returns>
        /// return action result
        /// </returns>
        public ActionResult MapUserRoles()
        {            
            UserAdminViewModel userAdminViewModel = this.userAdminHelper.GetMapUserRoles();
            return this.View(Constants.ViewMapUserRoles, userAdminViewModel);
        }

        /// <summary>
        /// Get Role To Functionality Details
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>
        /// return action result
        /// </returns>
        [HttpGet]
        public ActionResult GetRoleToFunctionality(string roleId)
        {
            List<UserMenuDetailsBusinessModel> rolesToFunctionalityMapping = this.userAdminHelper.GetRolesToFunctionality(roleId);
            return this.Json(rolesToFunctionalityMapping, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Role To Functionality
        /// </summary>
        /// <param name="roleMenuMappingList">The role menu mapping list.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>
        /// return action result
        /// </returns>
        [HttpPost]
        public ActionResult SaveRoleToFunctionality(List<UserMenuDetailsBusinessModel> roleMenuMappingList, string roleId)
        {
            int saveResponse = this.userAdminHelper.SaveRolesToFunctionality(roleMenuMappingList, roleId);
            LogHelper.LogAction(Constants.ActionNameSaveUserCustomerMapping, Constants.UserAdminController, Constants.ActionNameSaveUserCustomerMapping, this.userId);
            return this.Json(saveResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Role To User
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="sapCustomerId">The sap customer identifier.</param>
        /// <returns>
        /// return action result
        /// </returns>
        [HttpGet]
        public ActionResult GetRoleToUser(string userName, string roleId, string sapCustomerId)
        {
            List<UserMenuDetailsBusinessModel> roleToUserMapping = this.userAdminHelper.GetRoleToUser(userName, roleId, sapCustomerId);
            return this.Json(roleToUserMapping, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Role To User
        /// </summary>
        /// <param name="userRoleMappingList">The user role mapping list.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>
        /// return action result
        /// </returns>
        [HttpPost]
        public ActionResult SaveRoleToUser(List<UserMenuDetailsBusinessModel> userRoleMappingList, string roleId)
        {
            int saveRoleToUserResponse = this.userAdminHelper.SaveRoleToUser(userRoleMappingList, roleId);
            LogHelper.LogAction(Constants.ActionNameSaveUserCustomerMapping, Constants.UserAdminController, Constants.ActionNameSaveUserCustomerMapping, this.userId);
            return this.Json(saveRoleToUserResponse, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save New Role
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="isSCA">if set to <c>true</c> [is sca].</param>
        /// <returns>
        /// return action result
        /// </returns>
        [HttpPost]
        public ActionResult SaveNewRole(string roleName, bool isSCA)
        {
            int saveNewRoleResponse = this.userAdminHelper.SaveNewRole(roleName, isSCA);
            LogHelper.LogAction(Constants.ActionNameSaveUserCustomerMapping, Constants.UserAdminController, Constants.ActionNameSaveUserCustomerMapping, this.userId);
            return this.Json(saveNewRoleResponse, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Map User to Customer functionality

        /// <summary>
        /// This will return view
        /// </summary>
        /// <returns>
        /// all users
        /// </returns>
        public ActionResult MapUserCustomer()
        {            
            UserAdminViewModel mapUserCustomerViewModel = new UserAdminViewModel();
            mapUserCustomerViewModel = this.userAdminHelper.GetUserCustomerMapping();
            return this.View(Constants.ViewMapUserCustomer, mapUserCustomerViewModel);
        }

        /// <summary>
        /// This will user to get customers when dropdown gets selected.
        /// </summary>
        /// <param name="userID">The user identifier.</param>
        /// <param name="isFirstTime">if set to <c>true</c> [is first time].</param>
        /// <returns>
        /// Customers for User
        /// </returns>
        [HttpGet]        
        public ActionResult GetUserToAllCustomer(string userID, bool isFirstTime)
        {
            UserAdminViewModel objUserAdminViewModel = this.userAdminHelper.GetUserToAllCustomer(userID, isFirstTime);
            return this.Json(objUserAdminViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save User customer User mapping
        /// </summary>
        /// <param name="objCustomerList">The object customer list.</param>
        /// <param name="objUserId">The object user identifier.</param>
        /// <returns>
        /// response data
        /// </returns>
        [HttpPost]
        public ActionResult SaveUserToCustomers(List<UsersBusinessModel> objCustomerList, string objUserId)
        {
            int saveUserToCustomerResponse = this.userAdminHelper.SaveUserToCustomer(objCustomerList, objUserId);
            LogHelper.LogAction(Constants.ActionNameSaveUserCustomerMapping, Constants.UserAdminController, Constants.ActionNameSaveUserCustomerMapping, this.userId);
            return this.Json(saveUserToCustomerResponse, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Carehome Users
        /// <summary>
        /// Load User Carehome Screen
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MapUserToCareHome()
        {
            CareHomeUserViewModel careHomeUserViewModel = new CareHomeUserViewModel();
            careHomeUserViewModel.UserId = this.userId;
            careHomeUserViewModel.Customers = GenericHelper.GetCustomersListForDropdown(careHomeUserViewModel.UserId);

            return View(Constants.ViewMapUserCareHome, careHomeUserViewModel);
        }

        /// <summary>
        /// Gets Carehome or Users Details for DropDown
        /// </summary>
        /// <param name="typeId">1 For Carehome & 2 for Users Details</param>
        /// <param name="valueId">Gets Customer Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDDLForCareHomeUser(string typeId, string valueId)
        {
            SelectList ddlList = null;
            var strtypeId = "1";

            if (typeId == strtypeId)
                ddlList = GenericHelper.GetCareHomeOnCustomerId(valueId, userId);
            else
                ddlList= GenericHelper.GetCareHomeUsers(valueId, userId);

            return this.Json(ddlList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves Mapped Users to Carehome 
        /// </summary>
        /// <param name="objUserCarehomeBusinessModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveCareHomeUsers(UserCareHomeBusinessModel objUserCareHomeBusinessModel)
        {
            int response = userAdminHelper.SaveCareHomeUsers(objUserCareHomeBusinessModel);
            return this.Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}