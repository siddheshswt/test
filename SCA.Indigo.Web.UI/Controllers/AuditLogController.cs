﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sagar
// Created          : 19-05-2015
//
// Last Modified By : Sagar
// Last Modified On : 19-05-2015
// ***********************************************************************
// <copyright file="AuditLogController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.ViewModel;
    using SCA.Indigo.Web.UI.Helper;
    using System.Globalization;
    using System.Configuration;
    using System.IO;

    /// <summary>
    /// Audit Log Controller
    /// </summary>    
    [Authorize]
    public class AuditLogController : Controller
    {
        /// <summary>
        /// The _audit log view model
        /// </summary>
        private AuditLogViewModel auditLogViewModel = new AuditLogViewModel();

        /// <summary>
        /// The audit log helper
        /// </summary>
        private AuditLogHelper auditLogHelper = new AuditLogHelper();

        /// <summary>
        /// Object of CommonController
        /// </summary>
        private CommonController commonController = new CommonController();

        // GET: AuditLog
        /// <summary>
        /// Views the audit log.
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewAuditLog()
        {            
            return View(auditLogViewModel);
        }

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="SearchTable">The search table.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns></returns>
        public JsonResult GetSearchResults(string term, string searchTable, long? customerId)
        {            
            var searchResults = commonController.GetSearchResults(term, searchTable, customerId);
            return Json(searchResults, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the audit log.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="sidx">The sidx.</param>
        /// <param name="sord">The sord.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="auditLogType">Type of the audit log.</param>
        /// <param name="isExport">if set to <c>true</c> [is export].</param>
        /// <param name="auditSearchUserId">The audit search user identifier.</param>
        /// <returns></returns>
        public JsonResult GetAuditLog(long page, long rows, string sidx, string sord ,long customerId, long? careHomeId, long? patientId, string fromDate, string toDate, long auditLogType, bool isExport, string auditSearchUserId)
        {            
            long totalRecords = 0;
            var totalPages = 0;
            int defaultTimeSpan = Convert.ToInt32(ConfigurationManager.AppSettings["AuditLogDefaultTimeSpan"],CultureInfo.InvariantCulture);
          
            fromDate = string.IsNullOrEmpty(fromDate) ? DateTime.Now.Date.AddMonths(-defaultTimeSpan).ToString(Constants.DateFormat4, CultureInfo.InvariantCulture) : DateTime.ParseExact(fromDate, Constants.DateFormat4, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture);

            toDate = string.IsNullOrEmpty(toDate) ? DateTime.Now.Date.ToString(Constants.DateFormat4, CultureInfo.InvariantCulture) :
                DateTime.ParseExact(toDate, Constants.DateFormat4, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture);

           Guid UserId;
           Guid.TryParse(auditSearchUserId, out UserId);

            var auditLogInfoData = new List<AuditLogReportBusinessModel>();
           
            var auditLogResponse = auditLogHelper.GetAuditLog(page, rows, customerId, careHomeId, patientId, fromDate, toDate, auditLogType,isExport,UserId);

            if (isExport)
            {
                if (auditLogResponse.AuditLogs.Count > 0)
                {
                    // Export to excel - start  
                    var returnpath = string.Empty;
                    string auditLogServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.AuditLogSharedLocation);
                    string fileNamePrefix = Constants.AuditLogPrefix;
                    var columnsToRemove = new List<string>() { "TotalRecords", "AuditLogId" };

                    if (auditLogResponse.AuditLogs.Count > 0)
                    {
                        returnpath = ExcelHelper.ExportToExcel(auditLogResponse.AuditLogs.ToList(), auditLogServerDirectory, fileNamePrefix, columnsToRemove);
                    }
                    // Export to excel - end
                    return Json(returnpath, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (auditLogResponse.AuditLogs != null && auditLogResponse.AuditLogs.Count > 0)
                {
                    switch(sidx)
                    {
                        case "Name" :
                            switch(sord)
                            {
                                case "asc" :
                                    auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.Name).ToList();
                                  break;
                                case "desc" :
                                  auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.Name).ToList();
                                  break;
                            }
                                break;
                        case  "Action" :
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.Action).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.Action).ToList();
                                        break;
                                }
                                break;
                        case "Field":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.Field).ToList();
                                        break;
                                    case "desc":
                                       auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.Field).ToList();     
                                        break;
                                }
                                break;                            

                        case "Id":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.Id).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.Id).ToList();
                                        break;
                                }
                                break;
                        case "ModifiedBy":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.ModifiedBy).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.ModifiedBy).ToList();
                                        break;
                                }
                                break;

                        case "ModifiedOn":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.ModifiedOn).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.ModifiedOn).ToList();
                                        break;
                                }
                                break;

                        case "OldValue":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.OldValue).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.OldValue).ToList();
                                        break;
                                }
                                break;

                        case "NewValue":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.NewValue).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.NewValue).ToList();
                                        break;
                                }
                                break;

                        case "TableName":
                                switch (sord)
                                {
                                    case "asc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderBy(q => q.TableName).ToList();
                                        break;
                                    case "desc":
                                        auditLogResponse.AuditLogs = auditLogResponse.AuditLogs.OrderByDescending(q => q.TableName).ToList();
                                        break;
                                }
                                break;

						default:
								// do the default action
								break;
					}


                    auditLogInfoData = auditLogResponse.AuditLogs.ToList();
                    totalRecords = auditLogResponse.AuditLogs[0].TotalRecords;
                    totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
                }

                var auditLogResponseJson = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = auditLogInfoData,
                };

                return Json(auditLogResponseJson, JsonRequestBehavior.AllowGet);
            }
            return Json(new AuditLogViewModel(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the remarks.
        /// </summary>
        /// <param name="AuditLogs">The audit logs.</param>
        /// <returns></returns>
        [HttpPost]
        public bool SaveRemarks (List<AuditLogReportBusinessModel> AuditLogs)
        {            
            ControllerHelper.ActionName = Constants.ActionNameSaveAuditLogRemarks;
            var isSaved = (bool) ControllerHelper.PostMethodServiceRequestObject(AuditLogs);
            return isSaved;
        }
    }
}