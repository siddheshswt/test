﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sachin Phapale
// Created          : 03-25-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-27-2015
// ***********************************************************************
// <copyright file="HolidayProcessController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web.Mvc;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;

    /// <summary>
    /// Holiday Process Controller
    /// </summary>
    [Authorize]    
    public class HolidayProcessController : Controller
    {
        /// <summary>
        /// User Id
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// View Model
        /// </summary>
        private HolidayProcessViewModel holidayProcessViewModel = new HolidayProcessViewModel();

        /// <summary>
        /// User Admin Helper
        /// </summary>
        UserAdminHelper userAdminHelper = new UserAdminHelper();

        /// <summary>
        /// Load Holiday Process Admin View
        /// </summary>
        /// <returns>
        /// return action result
        /// </returns>
        public ActionResult HolidayProcessAdmin()
        {
            // get all customers
            this.FillDropDownData();
            
            LogHelper.LogAction(Constants.ActionNameHolidayProcessAdmin, Constants.HolidayProcessController, Constants.ActionNameHolidayProcessAdmin, ControllerHelper.CurrentUser.UserId.ToString());
            this.holidayProcessViewModel.NDD = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            this.holidayProcessViewModel.ArrangedNDD = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            return this.View(Constants.ActionNameHolidayProcessAdmin, this.holidayProcessViewModel);
        }

        /// <summary>
        /// Method to fill dropdown
        /// </summary>
        public void FillDropDownData()
        {
            ControllerHelper.ActionName = Constants.ActionNameGetCustomerList;
            ControllerHelper.ActionParam = new[] { this.userId };
            var customerResponse = ControllerHelper.GetMethodServiceRequest<List<ListTypeBusinessModel>>();
            this.holidayProcessViewModel.Customers = new SelectList(customerResponse, Constants.ListId, Constants.DisplayText);

            this.holidayProcessViewModel.Hours = GenericHelper.GetHoursOrMinuteSelectList(Constants.HourFormat1);
            this.holidayProcessViewModel.Minutes = GenericHelper.GetHoursOrMinuteSelectList(Constants.MinuteFormat1);
        }

        /// <summary>
        /// get customer details
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// return customer details
        /// </returns>
        [HttpGet]
        public ActionResult GetCustomerDetails(string customerId)
        {
            HolidayProcessViewModel holidayProcessViewModel = new HolidayProcessViewModel();
            holidayProcessViewModel = this.userAdminHelper.GetCustomerDetails(customerId);
            return this.Json(holidayProcessViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save holiday process
        /// </summary>
        /// <param name="objCustomerList">The object customer list.</param>
        /// <param name="objCustomerId">The object customer identifier.</param>
        /// <returns>
        /// Action Result.
        /// </returns>
        [HttpPost]
        public ActionResult SaveHolidayProcess(List<HolidayProcessAdminDetails> objCustomerList, string objCustomerId)
        {
            int saveHolidayProcessResponse = this.userAdminHelper.SaveHolidayProcess(objCustomerList, objCustomerId);
            LogHelper.LogAction(Constants.ActionNameSaveHolidayProcessData, Constants.HolidayProcessController, Constants.ActionNameSaveHolidayProcessData, this.userId);
            return this.Json(saveHolidayProcessResponse, JsonRequestBehavior.AllowGet);
        }
    }
}