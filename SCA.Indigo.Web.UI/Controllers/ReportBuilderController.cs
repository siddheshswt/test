﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : jagdish
// Created          : 12-03-2014
//
// Last Modified By : jagdish
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="OrderController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
	using SCA.Indigo.Web.UI.Helper;
	using SCA.Indigo.Web.UI.ViewModel;
	using System;
	using System.Data;
	using System.Globalization;
	using System.IO;
	using System.Net;
	using System.Web.Mvc;

    /// <summary>
    /// Report Builder Controller Class
    /// </summary>
    [Authorize]
    public class ReportBuilderController : Controller
    {
        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.InvariantCulture);

        /// <summary>
        /// The report builder helper
        /// </summary>
        ReportBuilderHelper reportBuilderHelper = new ReportBuilderHelper();

        #region private variables

        /// <summary>
        /// The temporary address
        /// </summary>
        private string tempAddress = GenericHelper.TempAddress;

        /// <summary>
        /// The server path
        /// </summary>
        private string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);

        /// <summary>
        /// The report builder server directory
        /// </summary>
        private string reportBuilderServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.ReportBuilderSharedLocation);

        /// <summary>
        /// The download url
        /// </summary>
        private string downloadUrl = GenericHelper.DownloadUrl;

        /// <summary>
        /// The upload directory
        /// </summary>
        private string uploadDirectory = GenericHelper.UploadDirectory;

        /// <summary>
        /// The file name prefix
        /// </summary>
        private string fileNamePrefix = Constants.ReportBuilderPrefix;

        #endregion

        /// <summary>
        /// Opens the report builder.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenReportBuilder()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            ReportBuilderViewModel reportBuilderViewModel = new ReportBuilderViewModel();
            reportBuilderHelper.OpenReportBuilder(reportBuilderViewModel);
            return this.View(Constants.ReportBuilder, reportBuilderViewModel);
        }

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="sidx">The sort column.</param>
        /// <param name="sord">The sort order.</param>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns>
        /// Report Data List
        /// </returns>
        [HttpPost]
        public ActionResult GetReportData(int page, int rows, string sidx, string sord, ReportBuilderViewModel reportBuilderViewModel)
        {			
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }
            reportBuilderViewModel.PageNbr = page;
            reportBuilderViewModel.PageSize = rows;
            reportBuilderViewModel.SortColumn = sidx;
            reportBuilderViewModel.SortDir = sord;
			reportBuilderHelper.GetReportData(reportBuilderViewModel);

			if(reportBuilderViewModel.ReportDataList != null && reportBuilderViewModel.ReportDataList.Count > 0 && reportBuilderViewModel.ReportDataList[0].IsExceptionOccurred == "true")
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
			var jsonData = new
			{
				total = reportBuilderViewModel.TotalPages,
				page = page,
				records = reportBuilderViewModel.TotalRows,
				rows = reportBuilderViewModel.ReportDataList
			};			
            return this.Json(jsonData, JsonRequestBehavior.AllowGet);
        }		

        /// <summary>
        /// Saves the report format.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns>
        /// Success or failure
        /// </returns>
        [HttpPost]
        public ActionResult SaveReportFormat(ReportBuilderViewModel reportBuilderViewModel)
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            reportBuilderHelper.SaveReportFormat(reportBuilderViewModel);
            return this.Json(reportBuilderViewModel);
        }

        /// <summary>
        /// Gets the report format.
        /// </summary>
        /// <param name="reportFormatId">The report format identifier.</param>
        /// <returns>
        /// Report format
        /// </returns>
        [HttpGet]
        public ActionResult GetReportFormat(string reportFormatId)
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            ReportBuilderViewModel reportBuilderViewModel = new ReportBuilderViewModel();
            reportBuilderViewModel = reportBuilderHelper.GetReportFormat(reportFormatId);
            return this.Json(reportBuilderViewModel.ReportFormatBusinessModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Downloads the report.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadReport(ReportBuilderViewModel reportBuilderViewModel)
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            /// create directory on the server
            Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);

            /// create directory on the virtual directory.
            Common.CommonHelper.CreateDirectoryIfNotExists(tempAddress);

            var reportBuilderDirectory = Path.Combine(serverPath, reportBuilderServerDirectory);
            if (!Directory.Exists(reportBuilderDirectory))
            {
                Directory.CreateDirectory(reportBuilderDirectory);
            }

            var FileName = fileNamePrefix + Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture) + DateTime.Now.ToString(Constants.DateTimeFormatyy5, CultureInfo.InvariantCulture) + ".xlsx";
            var FilePath = Path.Combine(serverPath, reportBuilderServerDirectory + '\\' + FileName);
            var TempPath = Path.Combine(tempAddress, FileName);

            reportBuilderViewModel.IsDownload = true;
            reportBuilderHelper.GetReportData(reportBuilderViewModel);
            DataTable reportTable = CreateReportTable(reportBuilderViewModel);
            if (reportTable.Rows.Count > 0)
            {
                var isExcelCreated = ExcelHelper.CreateExcelDocument(reportTable, FilePath);
                if (isExcelCreated)
                {
                    System.IO.File.Copy(FilePath, TempPath, true);
                    FilePath = downloadUrl + '\\' + uploadDirectory + '\\' + FileName;
                    return this.Json(FilePath, JsonRequestBehavior.AllowGet);
                }

                return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the report table.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns>
        /// Data table
        /// </returns>
        private DataTable CreateReportTable(ReportBuilderViewModel reportBuilderViewModel)
        {
            using (DataTable reportTable = new DataTable())
            {
                if (reportBuilderViewModel.ReportDataList != null && reportBuilderViewModel.ReportDataList.Count > 0)
                {
                    string[] columns = reportBuilderViewModel.ColumnNames.Split(',');
                    string[] columnsHeaders = reportBuilderViewModel.ColumnHeaders.Split(',');
                    for (int i = 0; i < columnsHeaders.Length; i++)
                    {
                        reportTable.Columns.Add(columnsHeaders[i]);
                    }

                    DataRow dataRow;
                    foreach (var item in reportBuilderViewModel.ReportDataList)
                    {
                        dataRow = reportTable.NewRow();
                        for (int i = 0; i < columns.Length; i++)
                        {
                            if (columns[i].ToLower().Contains("date"))
                            {
                                dataRow[columnsHeaders[i]] = ChangeDateFormat(Convert.ToString(item[columns[i]], CultureInfo.InvariantCulture));
                            }
                            else
                            {
                                dataRow[columnsHeaders[i]] = item[columns[i]];
                            }
                        }

                        reportTable.Rows.Add(dataRow);
                    }
                }

                return reportTable;
            }
        }

        /// <summary>
        /// Gets the products on customer identifier.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns>
        /// Products List
        /// </returns>
        [HttpGet]
        public ActionResult GetProductsOnCustomerId(string customerIds)
        {
            if (this.userPasswordExpired)
            {
                return this.RedirectToAction("Login", "Account");
            }

            SelectList productList = reportBuilderHelper.GetProductsOnCustomerId(customerIds);
            return this.Json(productList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the carehome on customer identifier.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns>
        /// Products List
        /// </returns>
        [HttpGet]
        public ActionResult GetCareHomeOnCustomerId(string customerIds)
        {
            if (this.userPasswordExpired)
            {
                return this.RedirectToAction("Login", "Account");
            }

            SelectList productList = reportBuilderHelper.GetCareHomeOnCustomerId(customerIds);
            return this.Json(productList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the PatientTypes on customer identifier.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPatientTypeOnCustomerId(string customerIds)
        {
            if (this.userPasswordExpired)
            {
                return this.RedirectToAction("Login", "Account");
            }
            SelectList patientTypeList = reportBuilderHelper.GetPatientTypeOnCustomerId(customerIds);
            return this.Json(patientTypeList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Changes the date format.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>Date in dd/MM/yyyy format</returns>
        private string ChangeDateFormat(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                DateTime date;
                bool isDate;
                string[] dateParts = data.Split(' ');
                string[] formats = { Constants.DateFormat1, Constants.DateFormat4, Constants.DateFormat2, Constants.DateFormat3 };
                isDate = DateTime.TryParseExact(dateParts[0], formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                return isDate == true ? date.ToString(Constants.DateFormat, CultureInfo.InvariantCulture) : data;
            }

            return string.Empty;
        }
    }
}