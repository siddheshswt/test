﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="AboutController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System.Web.Mvc;
    using SCA.Indigo.Web.UI.Helper;
    using System;
    using System.Globalization;

    [Authorize]
    public class AboutController : Controller
    {
        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.InvariantCulture);

        // GET: About
        public ActionResult About()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            return View();
        }
    }
}