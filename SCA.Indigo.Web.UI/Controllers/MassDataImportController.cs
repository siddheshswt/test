﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sachin
// Created          : 12-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassDataImportController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Globalization;
    using System.Web.Mvc;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel.MassChanges;
    using SCAEnums = SCA.Indigo.Common.Enums.SCAEnums;
    using LinqToExcel;
    using System.Collections.Generic;
    using System.Web;
    using System.IO;
    using SCA.Indigo.Common;

    /// <summary>
    /// Mass Data Import Controller
    /// </summary>
    [Authorize]
    public class MassDataImportController : Controller
    {
        /// <summary>
        /// mass Data Import Helper
        /// </summary>
        MassDataImportHelper massDataImportHelper = new MassDataImportHelper();

        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        // GET: MassDataImport
        public ActionResult Index()
        {
            return View();
        }

        #region MasterMassDataImport

        /// <summary>
        /// Uploads the File.d
        /// </summary>
        /// <param name="attachmentViewModel">The attachment view model.</param>e
        /// <returns>
        /// return status of uploaded File
        /// </returns>
        [HttpPost]
        public ActionResult ImportData(HttpPostedFileBase uploadedFile, int selectedTemplate, bool isImport)
        {
            var objMassDataImportHelper = new MassDataImportHelper();
            var objMasterMassDataImportViewModel = new MasterMassDataImportViewModel();
            objMasterMassDataImportViewModel.UploadedFile = uploadedFile;
            objMasterMassDataImportViewModel.SelectedTemplate = selectedTemplate;
            objMasterMassDataImportViewModel.IsImport = isImport;
            objMasterMassDataImportViewModel = objMassDataImportHelper.ImportData(objMasterMassDataImportViewModel);

            LogHelper.LogAction(Constants.ActionMethodImportData, Constants.MassDataImportController, Constants.ActionMethodImportData, this.userId);

            return this.Json(new
            {
                Status = objMasterMassDataImportViewModel.Status,
                Message = objMasterMassDataImportViewModel.Message,
                LogFilePath = objMasterMassDataImportViewModel.LogFilePath,
                validationRows = objMasterMassDataImportViewModel.TemplateValidationRows
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Mass Data Import Details
        /// </summary>
        /// <returns>action result</returns>
        public ActionResult GetMassDataImportDetails()
        {
            MasterMassDataImportViewModel masterMassDataImportViewModel = new MasterMassDataImportViewModel();
            var importTempletListId = Convert.ToInt64(SCAEnums.ListType.MassDataTemplate, CultureInfo.InvariantCulture);
            long languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);

            var listIds = new[] { importTempletListId };
            masterMassDataImportViewModel = this.massDataImportHelper.GetImportTemplateDropDown(listIds, languageId);

            LogHelper.LogAction(Constants.ActionMethodGetMassDataImportDetails, Constants.MassDataImportController, Constants.ActionMethodGetMassDataImportDetails, this.userId);
            return View(Constants.ActionMassDataImport, masterMassDataImportViewModel);
        }

        #region Dowmload Template
        /// <summary>
        /// Download Mass Data Template
        /// </summary>
        /// <param name="selectedTemplateValue"></param>
        /// <returns>action result</returns>
        [HttpPost]
        public ActionResult DownloadMassDataTemplate(int selectedTemplateValue)
        {
            string templateFilePath = string.Empty;
            templateFilePath = massDataImportHelper.GetTemplateFilePath(selectedTemplateValue, true);
            LogHelper.LogAction(Constants.ActionMethodDownloadMassDataTemplate, Constants.MassDataImportController, Constants.ActionMethodDownloadMassDataTemplate, this.userId);
            return Json(templateFilePath, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}