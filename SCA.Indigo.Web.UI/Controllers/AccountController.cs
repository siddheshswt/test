﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="AccountController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using System.Web.UI;
    using System.Linq;
    using System.Configuration;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.Helper;
    using SCA.Indigo.Web.UI.ViewModel;
    using System.Globalization;
    using System.Collections.Generic;

    /// <summary>
    /// Account Controller
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// The ssourl referrer
        /// </summary>
        private const string SSOURLReferrer = "SSOURLReferrer";

        private string userID = "";

        /// <summary>
        /// sso url
        /// </summary>
        private string ssoURLString = ConfigurationManager.AppSettings[SSOURLReferrer].ToString(CultureInfo.InvariantCulture);

        /// <summary>
        /// constructor will check for userid else it will redirect to login page
        /// </summary>
        public AccountController()
        {
            if (ControllerHelper.CurrentUser != null)
            {
                if (!string.IsNullOrEmpty(ControllerHelper.CurrentUser.UserId.ToString()))
                {
                    userID = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);
                }
                else
                {
                    RedirectToAction(Constants.ActionLogin, Constants.AccountController);
                }
            }
            else
            {
                RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }
        }

        /// <summary>
        /// Logins this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            Session.Clear();
            Session.Abandon();
            LoginViewModel loginViewModel = new LoginViewModel();
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains(Constants.LoginCookies))
            {
                loginViewModel.UserName = this.ControllerContext.HttpContext.Request.Cookies[Constants.LoginCookies][Constants.UserName];
                loginViewModel.RememberMe = Convert.ToBoolean(this.ControllerContext.HttpContext.Request.Cookies[Constants.LoginCookies][Constants.RememberMe], CultureInfo.InvariantCulture);
            }
            return View(loginViewModel);
        }

        /// <summary>
        /// Ssoes the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SSO(string token)
        {
            if (!this.ModelState.IsValid || string.IsNullOrEmpty(token))
            {
                return View();
            }

            string userName = string.Empty;

            token = token.Split(new string[] { "token=" }, StringSplitOptions.RemoveEmptyEntries)[0];
            string decryptData = new SSOHelper().Decrypt(token);

            if (!string.IsNullOrEmpty(decryptData))
            {
                userName = decryptData.Split(new string[] { "UserName=" }, StringSplitOptions.RemoveEmptyEntries)[0];
            }

            var objUsers = new UsersBusinessModel { UserName = userName, SSOAccess = true };

            // Set Request Body
            var serviceHelper = new WebServiceHelper(Constants.ActionNameGetUserDetails);
            var responseUser = serviceHelper.PostMethodServiceRequest(objUsers);

            if (responseUser != null)
            {
                responseUser.IsInternalUser = false;

                if (responseUser.UserName != null && !responseUser.IsAccountActive)
                {
                    ModelState.AddModelError(Constants.ErrAccountInActive, Resource.Common.resloginAccountInActive);
                    return View("ErrorAccountAccessForSSO");
                }

                responseUser.IsExpired = false;
                if (responseUser.IsAccountLocked.GetValueOrDefault())
                {
                    ModelState.AddModelError(Constants.ErrAccountLocked, Resource.Common.resloginAccountIslocked);
                    return View("ErrorAccountAccessForSSO");
                }

                if (responseUser.UserName != null)
                {
                    // Get Current User Role 
                    serviceHelper.CurrentUserRoleId = responseUser.RoleId;
                    serviceHelper.CurrentUser = responseUser;
                    FormsAuthentication.SetAuthCookie(responseUser.UserName, true);

                    return RedirectToAction(Constants.ActionWelcome, Constants.HomeController);
                }

                ModelState.AddModelError(Constants.ErrInvalidUser, Resource.Common.resloginInvalidUserMassege);
            }

            return View();
        }

        /// <summary>
        /// Login Post method
        /// </summary>
        /// <param name="loginViewModel">LoginViewModel with username and password fields updated.</param>
        /// <returns>
        /// Login View
        /// </returns>
        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid || loginViewModel == null)
            {
                return View();
            }

            string encryptedPassword = CommonHelper.Encrypt(loginViewModel.Password);
            var objUsers = new UsersBusinessModel { UserName = loginViewModel.UserName, Password = encryptedPassword };

            // Set Request Body
            ControllerHelper.ActionName = Constants.ActionNameGetUserDetails;
            var responseUser = ControllerHelper.PostMethodServiceRequest(objUsers);
            if (responseUser != null)
            {
                responseUser.IsInternalUser = true;

                if (responseUser.UserName != null && !responseUser.IsAccountActive)
                {
                    ModelState.AddModelError(Constants.ErrAccountInActive, Resource.Common.resloginAccountInActive);
                    return View();
                }

                if (responseUser.IsAccountLocked.GetValueOrDefault())
                {
                    ModelState.AddModelError(Constants.ErrAccountLocked, Resource.Common.resloginAccountIslocked);
                    return View();
                }

                if (responseUser.UserName != null && responseUser.Password != null)
                {
                    Session[Constants.SessionUser] = responseUser;
                    FormsAuthentication.SetAuthCookie(responseUser.UserName, true);

                    ControllerHelper.CurrentUser = responseUser;
                    ControllerHelper.SSOURLReferrer = string.Empty;

                    // Get Current User Role 
                    ControllerHelper.CurrentUserRoleId = responseUser.RoleId;

                    Session[Constants.SessionPatientTypeMatrix] = new PatientTypeMatrixBusinessModel();
                    if (responseUser.IsNewUser == null || responseUser.IsNewUser == true || responseUser.IsExpired == true)
                    {
                        return RedirectToAction(Constants.ActionChangePassword, Constants.AccountController);
                    }

                    HttpCookie loginCookie = new HttpCookie(Constants.LoginCookies);
                    if (loginViewModel.RememberMe)
                    {
                        loginCookie.Values.Add(Constants.UserName, loginViewModel.UserName);
                        loginCookie.Values.Add(Constants.Password, loginViewModel.Password);
                        loginCookie.Values.Add(Constants.RememberMe, loginViewModel.RememberMe ? "true" : "false");
                        this.ControllerContext.HttpContext.Response.Cookies.Add(loginCookie);
                    }
                    else
                    {
                        if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains(Constants.LoginCookies))
                        {
                            loginCookie.Expires = DateTime.Now.AddDays(-1d);
                            Response.Cookies.Add(loginCookie);
                        }
                    }

                    return RedirectToAction(Constants.ActionWelcome, Constants.HomeController);
                }


                ModelState.AddModelError(Constants.ErrInvalidUser, Resource.Common.resloginInvalidUserMassege);
            }

            return View();
        }

		/// <summary>
		/// Session Expired from the application
		/// </summary>
		/// <returns>
		/// Redirect to Login
		/// </returns>
		public ActionResult SessionExpiredOrLogOut(string isInternalUser)
		{		
			Session[Constants.SessionUser] = null;
			Session.Clear();
			Session.Abandon();

			// disable browsers back buttons.
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
			Response.Cache.SetNoStore();

			FormsAuthentication.SignOut();

			if (isInternalUser.ToLower() == "true")
				return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
			else
				return Redirect(ssoURLString);			
		}

        /// <summary>
        /// Logout from the application
		/// This function is Depreciated now. Please do not use it.
        /// </summary>
        /// <returns>
        /// Redirect to Login
        /// </returns>
        public ActionResult Logout()
        {
            var user = (UsersBusinessModel)Session[Constants.SessionUser];
			var isInternaluser = "true";
			if(User != null)
			{
				isInternaluser = Convert.ToString(user.IsInternalUser, CultureInfo.InvariantCulture);
			}             
            return RedirectToAction("SessionExpiredOrLogOut", Constants.AccountController, isInternaluser);
        }

        /// <summary>
        /// Change password get method
        /// </summary>
        /// <returns>
        /// Change Password View
        /// </returns>
        [Authorize]
        public ActionResult ChangePassword()
        {
            if (ControllerHelper.CurrentUser == null)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }

            return View();
        }

        /// <summary>
        /// Change Password Post Method
        /// The actual password change logic resites here.
        /// </summary>
        /// <param name="model">Change password view Model</param>
        /// <returns>
        /// Change Password View
        /// </returns>
        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (!this.ModelState.IsValid || model == null)
            {
                return View();
            }

            var currentUser = ControllerHelper.CurrentUser;

            var encyptedPass = CommonHelper.Encrypt(model.NewPassword);
            var objUsers = new UsersBusinessModel { UserId = currentUser.UserId, Password = encyptedPass };
            ControllerHelper.ActionName = Constants.ActionNameChangePassword;
            var responseData = (bool)ControllerHelper.PostMethodServiceRequestObject(objUsers);
            if (responseData)
            {
                currentUser.IsExpired = false;
                currentUser.Password = encyptedPass;
                currentUser.IsNewUser = false;
                currentUser.IsAccountLocked = false;
                ControllerHelper.CurrentUser = currentUser;

                string strLogMessage = currentUser.UserId + Constants.ActionChangePasswordSuccess;
                LogHelper.LogAction(strLogMessage, Constants.AccountController, Constants.ActionChangePassword, currentUser.UserId.ToString());
                return RedirectToAction(Constants.ActionWelcome, Constants.HomeController);
            }

            LogHelper.LogAction(Constants.ActionChangePasswordFailed, Constants.AccountController, Constants.ActionChangePassword, currentUser.UserId.ToString());
            return View();
        }

        /// <summary>
        /// Validates the current password with the actual password of the user
        /// </summary>
        /// <param name="model">Change Password ViewModel</param>
        /// <returns>
        /// Json value
        /// </returns>
        [Authorize]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ValidateCurrentPassword(ChangePasswordViewModel model)
        {
            if (model != null && !string.IsNullOrEmpty(model.Password) &&
                !string.IsNullOrEmpty(ControllerHelper.CurrentUser.Password))
            {
                if (ControllerHelper.CurrentUser.Password.Equals(CommonHelper.Encrypt(model.Password), StringComparison.Ordinal))
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Validate the new password for the last 3 passwords.
        /// </summary>
        /// <param name="model">Change Password ViewModel</param>
        /// <returns>
        /// Json Password validation
        /// </returns>
        [Authorize]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ValidateNewPassword(ChangePasswordViewModel model)
        {
            if (model == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            var encryptedPass = CommonHelper.Encrypt(model.ConfirmPassword);
            var objUser = new UsersBusinessModel() { UserId = ControllerHelper.CurrentUser.UserId, Password = encryptedPass };

            ControllerHelper.ActionName = Constants.ActionNameValidateNewPassword;

            var responseData = (bool)ControllerHelper.GetMethodServiceRequestObject(objUser);
            if (!responseData)
            {
                LogHelper.LogAction(Constants.ActionInvalidNewPassword, Constants.AccountController, Constants.ActionValidateNewPassword, ControllerHelper.CurrentUser.UserId.ToString());
            }
            TempData["IsConformPasswordValid"] = responseData;
            return Json(responseData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Redirect to error page
        /// </summary>
        /// <returns>
        /// Error view.
        /// </returns>
        public ActionResult Error()
        {
            //Session.Clear();
            //Session.Abandon();
            return View("Error");
        }

        /// <summary>
        /// Reset Password Post Method
        /// The actual password reset logic resites here.
        /// </summary>
        /// <param name="resetPasswordViewModel">Change password view Model</param>
        /// <returns>
        /// json result true or false
        /// </returns>
        [HttpPost]
        [Authorize]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ResetPassword(ResetPasswordViewModel resetPasswordViewModel)
        {          
            var currentUser = ControllerHelper.CurrentUser;
            if (TempData["IsConformPasswordValid"] != null) {
                if (TempData["IsConformPasswordValid"].ToString().ToLower() == "false") { return View(); }
            }
            if (!this.ModelState.IsValid || resetPasswordViewModel == null)
            {
                return View();
            }
            
            if (currentUser == null)
            {
                return RedirectToAction(Constants.ActionLogin, Constants.AccountController);
            }
            if (!string.IsNullOrEmpty(resetPasswordViewModel.UserId))
            {
                Guid userGuid = Guid.Parse(resetPasswordViewModel.UserId);
                var encyptedPass = CommonHelper.Encrypt(resetPasswordViewModel.NewPassword);
                var usersBusinessModel = new UsersBusinessModel { UserId = userGuid, Password = encyptedPass };
                ControllerHelper.ActionName = Constants.ActionNameResetPassword;
                var response = ControllerHelper.PostMethodServiceRequestObject(usersBusinessModel);
                if (response != null)
                {                    
                    return Json(response, JsonRequestBehavior.AllowGet);                    
                }

            }            
            return this.Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// User 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ManageAccount()
        {
            ViewBag.UserId = userID;
            return View("ResetUnlockUsers");
        }

        /// <summary>
        /// GetUsersForResetUnlock
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public ActionResult GetUsersForResetUnlock(long page, long rows, string sidx, string sord, string searchText)
        {
            long totalRecords = 0;
            var totalPages = 0;

            UserResetUnlockDetailsBusinessModel userResetUnlockDetails = new UserResetUnlockDetailsBusinessModel 
            {
                TotalRecords = rows,
                RowNumber = page,
                searchText = searchText,
                SortColumn = sidx,
                SortBy = sord,
                UserId = userID
            };

            ControllerHelper.ActionName = Constants.ActionNameGetUsersResetUnlock;
            var userResetUnlockDataList = ControllerHelper.PostMethodServiceRequest<UserResetUnlockDetailsBusinessModel, List<UserResetUnlockSummaryBusinessModel>>(userResetUnlockDetails);

            if (userResetUnlockDataList != null && userResetUnlockDataList.Count > 0)
            {
                totalRecords = userResetUnlockDataList[0].TotalRecords;
                totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
            }
            var interactionReportJson = new
            {
                total = totalPages,
                page = page,
                records = totalRecords,
                rows = userResetUnlockDataList,
            };
            return this.Json(interactionReportJson, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// LockUnlcokUser
        /// </summary>
        /// <param name="userIdTOLockUnlock"></param>
        /// <param name="isLocked"></param>
        /// <returns></returns>
        public ActionResult LockUnlcokUser(string userIdTOLockUnlock, bool isLocked)
        {
            if (userIdTOLockUnlock == null)
                return Json(false, JsonRequestBehavior.AllowGet);

            LockUnlockBusinessModel objLockUnlock = new LockUnlockBusinessModel() { LockUnlockUserId = userIdTOLockUnlock, userId = userID, isAccountLocked = isLocked };

            ControllerHelper.ActionName = Constants.ActionNameLockUnlockUser;
            var responseData = (bool)ControllerHelper.PostMethodServiceRequestObject(objLockUnlock);
            if (responseData)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        #region 1.5.4 Uncomment for Sprint 2(Ajax Method)
        ///// <summary>
        ///// Check For Interaction Assigned For Notification
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //public ActionResult CheckForNotification(string userId)
        //{
        //    if (string.IsNullOrEmpty(userId))
        //        return Json(false, JsonRequestBehavior.AllowGet);

        //    ControllerHelper.ActionName = Constants.ActionNameCheckForNotification;
        //    ControllerHelper.ActionParam = new[] { userId };

        //    var responseData = ControllerHelper.GetMethodServiceRequestForList<List<NotificationBusinessModel>>();

        //    return Json(responseData, JsonRequestBehavior.AllowGet);
        //}
        #endregion
    }
}