﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-08-2015
// ***********************************************************************
// <copyright file="AuthorizeController.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.Helper;

    /// <summary>
    /// Authorization Controller
    /// </summary>
    [Authorize]
    public class AuthorizeController : Controller
    {
        /// <summary>
        /// The _user password expired
        /// </summary>
        private readonly bool userPasswordExpired = Convert.ToBoolean(ControllerHelper.CurrentUser.IsExpired, CultureInfo.CurrentCulture);
        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.CurrentCulture);

        // GET: Authorization
        /// <summary>
        /// Prescriptions this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Prescription()
        {
            if (userPasswordExpired)
            {
                return RedirectToAction("Login", "Account");
            }

            Session[Constants.SessionisRedirectFromAuthorisation] = null;
            return View();
        }

        /// <summary>
        /// Get Products under the current User's Customer which require Authorization
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <returns>
        /// Prescription JsonResult .
        /// </returns>
        public JsonResult GetPrescription(int page, int rows)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                int totalRecords = 0;
                int totalPages = 0;
                var LoadAuthorizePrescriptionData = new List<PrescriptionAuthorizationBusinessModel>();
                string userName = "Null";
                var User = (UsersBusinessModel)Session[Constants.SessionUser];
                if (User != null)
                {
                    if (User.UserId != null &&
                        !(User.UserType.ToUpper() == SCAEnums.UserType.SCA.ToString(CultureInfo.InvariantCulture).ToUpper() && User.RoleName.ToUpper() == SCAEnums.Role.Administrator.ToString(CultureInfo.InvariantCulture).ToUpper()))
                    {
                        userName = Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture);
                    }
                }

                ControllerHelper.ActionName = Constants.ActionAuthorizePrescription;
                ControllerHelper.ActionParam = new[] { userName, Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture) };
                var AuthorizePrescriptionData = ControllerHelper.GetMethodServiceRequestForList<List<PrescriptionAuthorizationBusinessModel>>();
                LogHelper.LogAction(Constants.ActionLogAuthorizePrescription, Constants.AuthorizeController, Constants.ActionGetPrescription, userId);

                if (AuthorizePrescriptionData != null)
                {
                    LoadAuthorizePrescriptionData = AuthorizePrescriptionData.ToList();
                    totalRecords = AuthorizePrescriptionData.Count;
                    totalPages = (int)Math.Ceiling((double)totalRecords / (double)rows);
                }

                var PrescriptionDatajson = new
                {
                    total = totalPages,
                    page = page,
                    records = totalRecords,
                    rows = LoadAuthorizePrescriptionData,
                };
                return this.Json(PrescriptionDatajson, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        /// <summary>
        /// Approve the Prescription of selected Patient
        /// </summary>
        /// <param name="PatientId">Patient's id.</param>
        /// <param name="PrescriptionNote">Prescription note.</param>
        /// <returns>
        /// True if Prescription is Approved and Patient status is set to Active
        /// </returns>
        [HttpPost]
        public ActionResult ApprovePrescription(long PatientId, string PrescriptionNote)
        {
            if (string.IsNullOrEmpty(PrescriptionNote))
            {
                PrescriptionNote = "blank";
            }

            if (!string.IsNullOrEmpty(Constants.SessionUser))
            {
                ControllerHelper.ActionName = Constants.ActionApprovePrescriptionProducts;
                var objPrescriptionNote = new PrescriptionNoteBusinessModel
                {
                        CreatedBy = Guid.Parse(userId),
                        Note = PrescriptionNote,
                        PatientId = PatientId,
             };
                var responseData = (bool)ControllerHelper.PostMethodServiceRequestObject(objPrescriptionNote);
                LogHelper.LogAction(Constants.ActionApprovePrescriptionProducts, Constants.AuthorizeController, Constants.ActionApprovePrescription, userId);
                if (responseData == true)
                {
                    Session[Constants.SessionPatientStatus] = SCAEnums.PatientStatus.Active;
                    if (objPrescriptionNote.Note != "blank")
                    {
                        InsertPrescriptionNote(objPrescriptionNote);
                    }
                }
                return Json(responseData, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        /// <summary>
        /// Inserts the prescription note.
        /// </summary>
        /// <param name="objPrescriptionNote">The object prescription note.</param>
        protected void InsertPrescriptionNote(PrescriptionNoteBusinessModel objPrescriptionNote)
        {
            if (objPrescriptionNote != null)
            {
                var objNoteViewModel = new NotesBusinessModel();
                ControllerHelper.ActionName = Constants.ActionSaveNote;
                var objNotesBusinessModel = new NotesBusinessModel
                {
                    NoteId = objNoteViewModel.NoteId,
                    NoteType = Convert.ToInt32(SCAEnums.NoteType.PrescriptionNote, CultureInfo.InvariantCulture),
                    NoteTypeId = objPrescriptionNote.PatientId,
                    NoteText = objPrescriptionNote.Note,
                    CreatedBy = userId,
                };
                var NoteId = ControllerHelper.PostMethodServiceRequestObject(objNotesBusinessModel);
                LogHelper.LogAction(Constants.ActionLogSaveNote, Constants.PatientController, Constants.ActionSaveNote, userId);
            }        
        }

        /// <summary>
        /// Validates the user authorization pin.
        /// </summary>
        /// <param name="PIN">The pin.</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ValidateUserAuthorizationPin(string PIN)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetUserAuthorizationPin;
            ControllerHelper.ActionParam = new[] { userId.ToString(CultureInfo.InvariantCulture), PIN };
            var isPinValid = ControllerHelper.GetMethodServiceRequest<bool>();
            return Json(isPinValid, JsonRequestBehavior.AllowGet);
        }
    }
}