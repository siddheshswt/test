﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Siddhesh
// Created          : 12-03-2014
//
// Last Modified By : mamatha
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="GenericHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using System.Web.UI.WebControls;
    using System.Runtime.Serialization;
    using System.IO;
    using System.Web;
    using SCA.Indigo.Web.UI.Resource;
    /// <summary>
    /// Generic Helper for UI
    /// </summary>
    public static class GenericHelper
    {
        #region Common Web.Config entries

        //Common for every one                
        /// <summary>
        /// The server path
        /// </summary>
        private static string serverPath = ConfigurationManager.AppSettings["ServerPath"];
        public static string ServerPath { get { return serverPath; } set { serverPath = value; } }

        /// <summary>
        /// The server URL
        /// </summary>
        private static string serverUrl = ConfigurationManager.AppSettings["ServerUrl"];
        public static string ServerUrl { get { return serverUrl; } set { serverUrl = value; } }

        /// <summary>
        /// The upload directory
        /// </summary>
        private static string uploadDirectory = ConfigurationManager.AppSettings["SharedLocation"];
        public static string UploadDirectory { get { return uploadDirectory; } set { uploadDirectory = value; } }

        /// <summary>
        /// The ip address
        /// </summary>
        private static string iPAddress = ConfigurationManager.AppSettings["IPAddress"];
        public static string IPAddress { get { return iPAddress; } set { iPAddress = value; } }

        /// <summary>
        /// The server address
        /// </summary>
        private static string serverAddress = ConfigurationManager.AppSettings["ServerAddress"];
        public static string ServerAddress { get { return serverAddress; } set { serverAddress = value; } }
        /// <summary>
        /// The temporary address
        /// </summary>
        //public static string TempAddress = ConfigurationManager.AppSettings["TempAddress"];
        private static string tempAddress = System.Web.Hosting.HostingEnvironment.MapPath("\\" + GenericHelper.UploadDirectory);
        public static string TempAddress { get { return tempAddress; } set { tempAddress = value; } }

        /// <summary>
        /// The temporary address
        /// </summary>
        //public static string PDFTempAddress = ConfigurationManager.AppSettings["PDFTempAddress"];
        private static string pdfTempAddress = System.Web.Hosting.HostingEnvironment.MapPath("\\" + GenericHelper.PDFSharedLocation);
        public static string PDFTempAddress { get { return pdfTempAddress; } set { pdfTempAddress = value; } }

        /// <summary>
        /// The Download URL
        /// </summary>
        private static string downloadUrl = HttpContext.Current.Request.Url.Scheme + "://" +
                                           HttpContext.Current.Request.Url.Host +
                                           (String.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Request.Url.Port, CultureInfo.InvariantCulture)) ? String.Empty : ":" + Convert.ToString(HttpContext.Current.Request.Url.Port, CultureInfo.InvariantCulture));
        public static string DownloadUrl { get { return downloadUrl; } set { downloadUrl = value; } }

        //specific to audit log
        /// <summary>
        /// The audit log shared location
        /// </summary>
        private static string auditLogSharedLocation = ConfigurationManager.AppSettings["AuditLogSharedLocation"];
        public static string AuditLogSharedLocation { get { return auditLogSharedLocation; } set { auditLogSharedLocation = value; } }

        //specific to Interaction report
        /// <summary>
        /// The Intercation report shared location
        /// </summary>
        private static string interactionReportSharedLocation = ConfigurationManager.AppSettings["InteractionReportSharedLocation"];
        public static string InteractionReportSharedLocation { get { return interactionReportSharedLocation; } set { interactionReportSharedLocation = value; } }

        //specific to Care Home
        /// <summary>
        /// The Care Home shared location
        /// </summary>
        private static string careHomeSharedLocation = ConfigurationManager.AppSettings["CareHomeSharedLocation"];
        public static string CareHomeSharedLocation { get { return careHomeSharedLocation; } set { careHomeSharedLocation = value; } }

        /// <summary>
        /// The audit log default time span
        /// </summary>
        private static int auditLogDefaultTimeSpan = Convert.ToInt32(ConfigurationManager.AppSettings["AuditLogDefaultTimeSpan"], CultureInfo.InvariantCulture);
        public static int AuditLogDefaultTimeSpan { get { return auditLogDefaultTimeSpan; } set { auditLogDefaultTimeSpan = value; } }
        //specific to automatic order
        /// <summary>
        /// The automatic order criteria shared location
        /// </summary>
        private static string automaticOrderCriteriaSharedLocation = ConfigurationManager.AppSettings["AutomaticOrderCriteriaSharedLocation"];
        public static string AutomaticOrderCriteriaSharedLocation { get { return automaticOrderCriteriaSharedLocation; } set { automaticOrderCriteriaSharedLocation = value; } }
        //specific to reportbuilder
        /// <summary>
        /// The report builder shared location
        /// </summary>
        private static string reportBuilderSharedLocation = ConfigurationManager.AppSettings["ReportBuilderSharedLocation"];
        public static string ReportBuilderSharedLocation { get { return reportBuilderSharedLocation; } set { reportBuilderSharedLocation = value; } }

        /// <summary>
        /// The customer parameter shared location
        /// </summary>
        private static string customerParameterSharedLocation = ConfigurationManager.AppSettings["CustomerParameterSharedLocation"];
        public static string CustomerParameterSharedLocation { get { return customerParameterSharedLocation; } set { customerParameterSharedLocation = value; } }

        //specific to attachment
        /// <summary>
        /// The attachment shared location
        /// </summary>
        private static string attachmentSharedLocation = ConfigurationManager.AppSettings["AttachmentSharedLocation"];
        public static string AttachmentSharedLocation { get { return attachmentSharedLocation; } set { attachmentSharedLocation = value; } }

        //patient type matrix
        /// <summary>
        /// The patient type matrix shared location
        /// </summary>
        private static string patientTypeMatrixSharedLocation = ConfigurationManager.AppSettings["PatientTypeMatrixSharedLocation"];
        public static string PatientTypeMatrixSharedLocation { get { return patientTypeMatrixSharedLocation; } set { patientTypeMatrixSharedLocation = value; } }

        private static string masterMassChangesDownloadSharedLocation = ConfigurationManager.AppSettings["MasterMassChangesDownloadSharedLocation"];
        public static string MasterMassChangesDownloadSharedLocation { get { return masterMassChangesDownloadSharedLocation; } set { masterMassChangesDownloadSharedLocation = value; } }

        private static string massChangesUploads = ConfigurationManager.AppSettings["MassChangesUploads"];
        public static string MassChangesUploads { get { return massChangesUploads; } set { massChangesUploads = value; } }

        private static string massChangesTemplate = ConfigurationManager.AppSettings["MassChangesTemplate"];
        public static string MassChangesTemplate { get { return massChangesTemplate; } set { massChangesTemplate = value; } }

        //specific to PDF
        /// <summary>
        /// The PDF shared location
        /// </summary>
        private static string pdfSharedLocation = ConfigurationManager.AppSettings["PDFSharedLocation"];
        public static string PDFSharedLocation { get { return pdfSharedLocation; } set { pdfSharedLocation = value; } }

        //specific to sample order
        /// <summary>
        /// The sample order shared location
        /// </summary>
        private static string sampleOrderSharedLocation = ConfigurationManager.AppSettings["SampleOrderSharedLocation"];
        public static string SampleOrderSharedLocation { get { return sampleOrderSharedLocation; } set { sampleOrderSharedLocation = value; } }
        #endregion

        /// <summary>
        /// Arranged NDD
        /// </summary>
        private static DateTime ArrangedNDD;

        /// <summary>
        /// Generic methods to Fill Dropdown from ListType
        /// </summary>
        /// <param name="listTypeId">List Type Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns></returns>
        public static SelectList GetListDataForDropdown(string listTypeId, string languageId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            ControllerHelper.ActionParam = new[] { listTypeId, languageId };

            var response = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            return new SelectList(response, Constants.ListId, Constants.DisplayText);
        }

        /// <summary>
        /// Get Clinical Contact
        /// </summary>
        /// <param name="listTypeId">The list type identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>
        /// list of object
        /// </returns>
        public static SelectList GetClinicalContactForDropdown(string listTypeId, string languageId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetListTypeForClinicalContact;
            ControllerHelper.ActionParam = new[] { listTypeId, languageId };

            var response = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            return new SelectList(response, Constants.ListId, Constants.DisplayText);
        }

        /// <summary>
        /// Get Hours and Minutes values
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns>
        /// hour list
        /// </returns>
        public static SelectList GetHoursOrMinuteSelectList(int format)
        {
            List<ListItem> items = new List<ListItem>();
            for (int i = 0; i <= format; i++)
            {
                items.Add(new ListItem
                {
                    Text = i.ToString(Constants.MinuteFormat2, CultureInfo.InvariantCulture),
                    Value = i.ToString(Constants.MinuteFormat2, CultureInfo.InvariantCulture)
                });
            }

            return new SelectList(items);
        }

        /// <summary>
        /// Generic methods to Fill Dropdown from ListType
        /// </summary>
        /// <param name="listTypeId">List Type Id</param>
        /// <param name="listId">The list identifier.</param>
        /// <param name="languageId">Language Id</param>
        /// <returns></returns>
        public static SelectList GetListDataForDropdown(string listTypeId, string listId, string languageId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetListTypeForInteraction;
            ControllerHelper.ActionParam = new[] { listTypeId, listId, languageId };

            var response = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            return new SelectList(response, Constants.ListId, Constants.DisplayText);
        }

        /// <summary>
        /// Load Master holiday List
        /// </summary>
        ///         
        public static List<HolidayBusinessModel> GetHolidayList(int countryId)
        {
            var userId = ControllerHelper.CurrentUser.UserId.ToString();
            ControllerHelper.ActionName = Constants.ActionNameGetHolidayList;
            ControllerHelper.ActionParam = new[] { userId, Convert.ToString(countryId, CultureInfo.InvariantCulture) };
            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<HolidayBusinessModel>>();
            return responseData;
        }

        /// <summary>
        /// Determines whether the specified date is holiday.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static bool IsHoliday(DateTime date, int countryId)
        {
            var holidayList = new List<HolidayBusinessModel>();
            holidayList = GetHolidayList(countryId);
            if (holidayList.Where(q => q.HolidayDate == date).Any())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the customers list for dropdown.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetCustomersListForDropdown(string userId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetCustomersForDropDown;
            ControllerHelper.ActionParam = new[] { userId };
            var response = ControllerHelper.GetMethodServiceRequestForList<List<DropDownDataBusinessModel>>();
            if (response != null)
            {
                return new SelectList(response, Constants.Value, Constants.DisplayText);
            }
            return new SelectList(new List<DropDownDataBusinessModel>(), Constants.Value, Constants.DisplayText);
        }

        /// <summary>
        /// Gets the users list for Interaction report dropdown.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetInterationReportListForUsers(string userId, string customerIds)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetUsersOnInteraction;
            ControllerHelper.ActionParam = new[] { userId, customerIds };
            var response = ControllerHelper.GetMethodServiceRequestForList<List<InteractionReportUsersBusinessModel>>();
            if (response != null)
            {
                return new SelectList(response, Constants.Value, Constants.DisplayText);
            }
            return new SelectList(new List<InteractionReportUsersBusinessModel>(), Constants.Value, Constants.DisplayText);
        }

        /// <summary>
        /// Gets the care home list for dropdown.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetCareHomeListForDropdown(string userId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetCareHomesForDropDown;
            ControllerHelper.ActionParam = new[] { userId };
            var response = ControllerHelper.GetMethodServiceRequestForList<List<DropDownDataBusinessModel>>();
            if (response != null)
            {
                return new SelectList(response, Constants.Value, Constants.DisplayText);
            }

            return new SelectList(new List<DropDownDataBusinessModel>(), Constants.Value, Constants.DisplayText);
        }

        /// <summary>
        /// Gets the description from enum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string GetDescriptionFromEnumValue(Enum value)
        {
            EnumMemberAttribute attribute = value.GetType()
            .GetField(value.ToString())
            .GetCustomAttributes(typeof(EnumMemberAttribute), false)
            .SingleOrDefault() as EnumMemberAttribute;
            return attribute == null ? value.ToString() : attribute.Value;
        }

        /// <summary>
        /// Gets the products on customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetProductsOnCustomerId(string customerId, string userId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetProductsOnCustomerId;
            ControllerHelper.ActionParam = new[] { customerId, userId };
            var response = ControllerHelper.GetMethodServiceRequestForList<List<DropDownDataBusinessModel>>();
            if (response != null)
            {
                return new SelectList(response, Constants.Value, Constants.DisplayText);
            }

            return new SelectList(new List<DropDownDataBusinessModel>(), Constants.Value, Constants.DisplayText); ;
        }

        /// <summary>
        /// Gets the carehome on customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetCareHomeOnCustomerId(string customerId, string userId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetCareHomeOnCustomerId;
            ControllerHelper.ActionParam = new[] { customerId, userId };
            var response = ControllerHelper.GetMethodServiceRequestForList<List<DropDownDataBusinessModel>>();
            if (response != null)
            {
                return new SelectList(response, Constants.Value, Constants.DisplayText);
            }

            return new SelectList(new List<DropDownDataBusinessModel>(), Constants.Value, Constants.DisplayText); ;
        }

        /// <summary>
        /// Gets the PatientType on customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetPatientTypeOnCustomerId(string customerId, string userId)
        {
            ControllerHelper.ActionName = Constants.ActionGetCustomerPatientType;
            ControllerHelper.ActionParam = new[] { customerId.ToString(CultureInfo.InvariantCulture), userId};
            var responsePatientType = ControllerHelper.GetMethodServiceRequestForList<List<PatientBusinessModel>>();
            if (responsePatientType != null)
            {
                return new SelectList(responsePatientType, Constants.ListId, Constants.DisplayText);
            }
            return new SelectList(new List<DropDownDataBusinessModel>(), Constants.ListId, Constants.DisplayText); ;
        }

        /// <summary>
        /// Gets the carehome users on customer identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public static SelectList GetCareHomeUsers(string customerId, string userId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetCareHomeUsers;
            ControllerHelper.ActionParam = new[] { customerId, userId };
            var response = ControllerHelper.GetMethodServiceRequestForList<List<DropDownBusinessModel>>();
            if (response != null)
            {
                return new SelectList(response, Constants.Value, Constants.DisplayText);
            }

            return new SelectList(new List<DropDownBusinessModel>(), Constants.Value, Constants.DisplayText); ;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns>
        /// return list
        /// </returns>
        public static SelectList GetAuthorizeCustomersForDropdown()
        {
            var userId = ControllerHelper.CurrentUser.UserId.ToString();
            ControllerHelper.ActionName = Constants.ActionNameSearchCustomer;
            ControllerHelper.ActionParam = new[] { userId };

            var customerResponse = ControllerHelper.GetMethodServiceRequest<List<DropDownDataBusinessModel>>();
            if (customerResponse != null)
            {
                return new SelectList(customerResponse, Constants.Value, Constants.DisplayText);
            }

            return new SelectList(new List<DropDownDataBusinessModel>(), Constants.Value, Constants.DisplayText);
        }

        // Add days to a date (Exclude Saturday and Sunday)
        /// <summary>
        /// Adds the working days.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="noOfDays">The no of days.</param>
        /// <returns></returns>
        public static DateTime AddWorkingDays(DateTime date, long noOfDays, int countryId)
        {
            var days = noOfDays;
            while (days != 0)
            {
                date = date.AddDays(1);
                days = days - 1;
                while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || GenericHelper.IsHoliday(date, countryId))
                    date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// AddWorkingDays including Saturday & Sunday only
        /// </summary>
        /// <param name="date"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        public static DateTime AddWorkingDays(DateTime date, long noOfDays)
        {
            var days = noOfDays;
            while (days != 0)
            {
                date = date.AddDays(1);
                days = days - 1;
                while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                    date = date.AddDays(1);
            }
            return date;
        }

        /// <summary>
        /// Return the next working week day (Exclude Saturday, Sunday)
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>Next Working day after the given input date.</returns>
        public static DateTime GetNextWorkingDay(DateTime date, int countryId)
        {
            while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || GenericHelper.IsHoliday(date, countryId))
            {
                date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// GetNextWorkingDay inlcuding Saturday & Sunday only
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetNextWorkingDay(DateTime date)
        {
            while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
            }
            return date;
        }

        /// <summary>
        /// Calculate the next delivery date and upadte the customer parameter related sessin parameters
        /// </summary>
        /// <param name="deliveryDate">The delivery date.</param>
        /// <returns>Next Delivery Date.</returns>
        public static DateTime CalculateNextDeliveryDate(DateTime deliveryDate, List<PostCodeMatrixBusinessModel> postCodeMatrix, List<CustomerParameterBusinessModel> customerParameter)
        {
            var nextDeliveryDate = new DateTime();
            if (customerParameter != null && customerParameter.Any())
            {
                var firstRecord = customerParameter.First();
                if (deliveryDate < GenericHelper.AddWorkingDays(DateTime.Today, firstRecord.OrderLeadTime, customerParameter.First().CountryId))
                {
                    nextDeliveryDate = GenericHelper.AddWorkingDays(DateTime.Today, firstRecord.OrderLeadTime, customerParameter.First().CountryId);
                }
                else
                {
                    nextDeliveryDate = deliveryDate;
                }

                if (nextDeliveryDate == new DateTime())
                {
                    nextDeliveryDate = DateTime.Today.AddDays(1);
                }

                nextDeliveryDate = GenericHelper.GetNextWorkingDay(nextDeliveryDate, customerParameter.First().CountryId);
                nextDeliveryDate = ValidatePostcodeMatrix(nextDeliveryDate, postCodeMatrix, customerParameter.First().CountryId);
            }

            return nextDeliveryDate;
        }

        /// <summary>
        /// Validate Postcode Matrix
        /// </summary>
        /// <param name="nextDeliveryDate">nextDeliveryDate</param>
        /// <param name="postCodeMatrixList">postCodeMatrixList</param>
        /// <returns></returns>
        private static DateTime ValidatePostcodeMatrix(DateTime nextDeliveryDate, List<PostCodeMatrixBusinessModel> postCodeMatrixList, int countryId)
        {
            if (postCodeMatrixList.Any())
            {
                var postCodeMatrix = postCodeMatrixList.First();

                //check if all days are false
                if (postCodeMatrix.Monday == false &&
                    postCodeMatrix.Tuesday == false &&
                    postCodeMatrix.Wednesday == false &&
                        postCodeMatrix.Thursday == false &&
                            postCodeMatrix.Friday == false
                    )
                {
                    var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(nextDeliveryDate, postCodeMatrix.CustomerID);
                    //Whenever derived date is a bank holiday & 
                    //an arranged date is found in Holiday Process table then use next available working date as a delivery date
                    if (isArrangedDateFound)
                    {
                        nextDeliveryDate = ArrangedNDD;
                    }
                }
                else
                {
                    var dayOfWeek = nextDeliveryDate.DayOfWeek;
                    var isValidDate = false;
                    while (!isValidDate)
                    {
                        isValidDate = true;
                        if (dayOfWeek == DayOfWeek.Monday && postCodeMatrix.Monday == false)
                        {
                            isValidDate = false;
                            nextDeliveryDate = nextDeliveryDate.AddDays(1);
                        }

                        if (dayOfWeek == DayOfWeek.Tuesday && postCodeMatrix.Tuesday == false)
                        {
                            isValidDate = false;
                            nextDeliveryDate = nextDeliveryDate.AddDays(1);
                        }

                        if (dayOfWeek == DayOfWeek.Wednesday && postCodeMatrix.Wednesday == false)
                        {
                            isValidDate = false;
                            nextDeliveryDate = nextDeliveryDate.AddDays(1);
                        }

                        if (dayOfWeek == DayOfWeek.Thursday && postCodeMatrix.Thursday == false)
                        {
                            isValidDate = false;
                            nextDeliveryDate = nextDeliveryDate.AddDays(1);
                        }

                        if (dayOfWeek == DayOfWeek.Friday && postCodeMatrix.Friday == false)
                        {
                            isValidDate = false;
                            nextDeliveryDate = nextDeliveryDate.AddDays(1);
                        }

                        if (!isValidDate)
                        {
                            nextDeliveryDate = GenericHelper.GetNextWorkingDay(nextDeliveryDate, countryId);
                            //holiday table is not required
                            //if (GenericHelper.IsHoliday(nextDeliveryDate))
                            //{
                            //    nextDeliveryDate = nextDeliveryDate.AddDays(1);
                            //}
                            dayOfWeek = nextDeliveryDate.DayOfWeek;
                        }
                    }
                    nextDeliveryDate = GenericHelper.GetNextWorkingDay(nextDeliveryDate, countryId);
                    var isArrangedDateFound = CheckDateAvailabilityInHolidayProcess(nextDeliveryDate, postCodeMatrix.CustomerID);
                    //Whenever derived date is a bank holiday & 
                    //an arranged date is found in Holiday Process table then use next available working date as a delivery date
                    if (isArrangedDateFound)
                    {
                        nextDeliveryDate = ArrangedNDD;
                    }
                }
            }
            return nextDeliveryDate;
        }

        /// <summary>
        /// Validate PostCodeMatrix excluding Saturday & Sunday Only
        /// </summary>
        /// <param name="nextDeliveryDate"></param>
        /// <param name="postCodeMatrix"></param>
        /// <returns></returns>
        public static DateTime ValidatePostcodeMatrix(DateTime nextDeliveryDate, PostCodeMatrixBusinessModel postCodeMatrix)
        {
            if (!(postCodeMatrix.Monday == false &&
                postCodeMatrix.Tuesday == false &&
                postCodeMatrix.Wednesday == false &&
                    postCodeMatrix.Thursday == false &&
                        postCodeMatrix.Friday == false
                ))
            {
                var dayOfWeek = nextDeliveryDate.DayOfWeek;
                var isValidDate = false;
                while (!isValidDate)
                {
                    isValidDate = true;
                    if (dayOfWeek == DayOfWeek.Monday && postCodeMatrix.Monday == false)
                    {
                        isValidDate = false;
                        nextDeliveryDate = nextDeliveryDate.AddDays(1);
                    }

                    if (dayOfWeek == DayOfWeek.Tuesday && postCodeMatrix.Tuesday == false)
                    {
                        isValidDate = false;
                        nextDeliveryDate = nextDeliveryDate.AddDays(1);
                    }

                    if (dayOfWeek == DayOfWeek.Wednesday && postCodeMatrix.Wednesday == false)
                    {
                        isValidDate = false;
                        nextDeliveryDate = nextDeliveryDate.AddDays(1);
                    }

                    if (dayOfWeek == DayOfWeek.Thursday && postCodeMatrix.Thursday == false)
                    {
                        isValidDate = false;
                        nextDeliveryDate = nextDeliveryDate.AddDays(1);
                    }

                    if (dayOfWeek == DayOfWeek.Friday && postCodeMatrix.Friday == false)
                    {
                        isValidDate = false;
                        nextDeliveryDate = nextDeliveryDate.AddDays(1);
                    }

                    if (!isValidDate)
                    {
                        nextDeliveryDate = GenericHelper.GetNextWorkingDay(nextDeliveryDate);
                        dayOfWeek = nextDeliveryDate.DayOfWeek;
                    }
                }
            }
            nextDeliveryDate = GenericHelper.GetNextWorkingDay(nextDeliveryDate);
            return nextDeliveryDate;
        }

        /// <summary>
        /// Check Date Availability In Holiday Process
        /// </summary>
        /// <param name="nextDeliveryDate">nextDeliveryDate</param>
        /// <param name="CustomerID">CustomerID</param>
        /// <returns></returns>
        private static bool CheckDateAvailabilityInHolidayProcess(DateTime nextDeliveryDate, long CustomerID)
        {
            var objHolidayBusinessModel = new HolidayProcessAdminBusinessModels();
            objHolidayBusinessModel.CustomerId = CustomerID;
            objHolidayBusinessModel.DerivedNDD = Convert.ToString(nextDeliveryDate, CultureInfo.InvariantCulture);
            objHolidayBusinessModel.OrderCreationTime = Convert.ToString(DateTime.Now.ToString(Constants.TimeFormat3, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            objHolidayBusinessModel.UserId = ControllerHelper.CurrentUser.UserId;
            ControllerHelper.ActionName = Constants.ActionNameGetArrangedDeliveryDate;
            var strArrangedDate = (string)ControllerHelper.PostMethodServiceRequestObject(objHolidayBusinessModel);
            if (strArrangedDate != null)
            {
                ArrangedNDD = Convert.ToDateTime(strArrangedDate, CultureInfo.InvariantCulture);
                return true;
            }
            return false;
        }

        /// <summary>
        /// The user identifier
        /// </summary>
        private static string userId = ControllerHelper.CurrentUser.UserId.ToString();


        /// <summary>
        /// Gets the audit log search results.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="SearchTable">The search table.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns></returns>
        public static List<CommonSearchBusinessModel> CommonSearchResults(string term, string searchTable, long? customerId)
        {
            var objSearchRequest = new CommonSearchBusinessModel
            {
                SearchTerm = term,
                TableName = searchTable,
                CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture),
                UserId = Guid.Parse(userId),
            };
            ControllerHelper.ActionName = Constants.ActionNameGetAuditLogSearchResults;
            var customSearchResults = ControllerHelper.PostMethodServiceRequest<CommonSearchBusinessModel, List<CommonSearchBusinessModel>>(objSearchRequest);
            LogHelper.LogAction(Constants.ActionLogGetAuditLogSearchResults, Constants.AuditLogController, Constants.ActionGetAuditLogSearchResults, userId);
            if (customSearchResults != null)
            {
                return customSearchResults;
            }
            else
            {
                return new List<CommonSearchBusinessModel>();
            }
        }

        /// <summary>
        /// ConvertBoolToYesNo
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ConvertBoolToYesNo(bool? data)
        {
            string yesNo = data.Value == true ? Common.resGeneralYes : Common.resGeneralNo;
            return yesNo;
        }

        /// <summary>
        /// Checks wheather user is carehome user or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool IsCareHomeUser(string userId)
        {
            ControllerHelper.ActionName = Constants.ActionNameIsCareHomeUser;
            ControllerHelper.ActionParam = new[] { userId };
            var response = (bool)ControllerHelper.PostMethodServiceRequestObject(userId);
            return response;
        }

        /// <summary>
        /// Get Postcode Matrix based on CustomerId, Round, RoundId, Postcode
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="round"></param>
        /// <param name="roundId"></param>
        /// <param name="postCode"></param>
        /// <returns></returns>
        public static PostCodeMatrixBusinessModel GetPostCode(string customerId, string round, string roundId, string postCode)
        {
            ControllerHelper.ActionName = Constants.ActionNamePostCodeMatrix;

            if (string.IsNullOrEmpty(postCode))
                postCode = "null";

            ControllerHelper.ActionParam = new[] { customerId, round, roundId, postCode, ControllerHelper.CurrentUser.UserId.ToString() };
            var postCodeMatrix = ControllerHelper.GetMethodServiceRequest<PostCodeMatrixBusinessModel>();
            return postCodeMatrix;
        }
    }
}