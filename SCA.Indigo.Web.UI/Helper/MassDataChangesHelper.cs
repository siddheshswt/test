﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Sachin
// Created          : 09-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassDataChangesHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.ViewModel.MassChanges;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using SCA.Indigo.Common;
    using LinqToExcel;
    using System.Web.Mvc;
    using System.Linq;

    public class MassDataChangesHelper
    {
		/// <summary>
		/// The user identifier
		/// </summary>
		private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Mass Update Template File's Path
        /// </summary>
		private string templateFilePath = string.Empty;
       
        /// <summary>
        /// Get Master Mass Data Change details
        /// </summary>
        /// <returns>view model</returns>
        public MasterMassDataChangeViewModel GetMasterMassDataChange()
        {
            MasterMassDataChangeViewModel masterMassDataChangeViewModel = new MasterMassDataChangeViewModel();
            var languageId = Convert.ToInt64(SCAEnums.OtherConstants.LanguageId, CultureInfo.InvariantCulture);
            masterMassDataChangeViewModel.Customer = GenericHelper.GetAuthorizeCustomersForDropdown();

            var patientTypeId = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
            var patientTypeStatusId = Convert.ToInt64(SCAEnums.ListType.PatientStatus, CultureInfo.InvariantCulture);
            var orderTypeId = Convert.ToInt64(SCAEnums.ListType.OrderType, CultureInfo.InvariantCulture);
            var downloadTypeId = Convert.ToInt64(SCAEnums.ListType.MassDataTemplate, CultureInfo.InvariantCulture);
            var careHomeStatusId = Convert.ToInt64(SCAEnums.ListType.CareHomeStatus, CultureInfo.InvariantCulture);
            var careHomeOrderTypeId = Convert.ToInt64(SCAEnums.ListType.CareHomeOrderType, CultureInfo.InvariantCulture);
            var orderStatusId = Convert.ToInt64(SCAEnums.ListType.OrderStatus, CultureInfo.InvariantCulture);
            var listTypeIds = new[] { patientTypeId, patientTypeStatusId, orderTypeId, downloadTypeId, careHomeStatusId, careHomeOrderTypeId, orderStatusId };

            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(listTypeIds), Convert.ToString(languageId, CultureInfo.InvariantCulture) };
            var listData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();

            var patientType = listData.Where(c => c.ListTypeId.Equals(patientTypeId));
            var patientTypeStatus = listData.Where(c => c.ListTypeId.Equals(patientTypeStatusId));
            var orderType = listData.Where(c => c.ListTypeId.Equals(orderTypeId));
            var downloadType = listData.Where(c => c.ListTypeId.Equals(downloadTypeId));
            var careHomeStatus = listData.Where(c => c.ListTypeId.Equals(careHomeStatusId));
            var careHomeOrderType = listData.Where(c => c.ListTypeId.Equals(careHomeOrderTypeId));
            var orderStatus = listData.Where(c => c.ListTypeId.Equals(orderStatusId));
            orderStatus = orderStatus.Where(q => q.ListId == Convert.ToInt64(SCAEnums.OrderStatus.Activated, CultureInfo.InvariantCulture) || q.ListId == Convert.ToInt64(SCAEnums.OrderStatus.Cancelled, CultureInfo.InvariantCulture)).ToList();///Show Only Activated and Cancelled Orders

            masterMassDataChangeViewModel.PatientType = new SelectList(patientType.ToList(), Constants.ListId, Constants.DisplayText);
            masterMassDataChangeViewModel.PatientStatus = new SelectList(patientTypeStatus.ToList(), Constants.ListId, Constants.DisplayText);
            masterMassDataChangeViewModel.OrderType = new SelectList(orderType.ToList(), Constants.ListId, Constants.ListCode);
            masterMassDataChangeViewModel.DownloadType = new SelectList(downloadType.ToList(), Constants.ListId, Constants.DisplayText);
            masterMassDataChangeViewModel.CareHomeStatus = new SelectList(careHomeStatus.ToList(), Constants.ListId, Constants.DisplayText);
            masterMassDataChangeViewModel.CareHomeOrderType = new SelectList(careHomeOrderType.ToList(), Constants.ListId, Constants.DisplayText);
            masterMassDataChangeViewModel.OrderStatus = new SelectList(orderStatus.ToList(), Constants.ListId, Constants.DisplayText);
            
            return masterMassDataChangeViewModel;
        }

        /// <summary>
        /// Download Carehome data for Mass Update
        /// </summary>
        /// <param name="masterMassDataChangeViewModel"></param>
        /// <returns>view model</returns>
        public MasterMassDataChangeViewModel DownloadMasterMassDataChange(MasterMassDataChangeViewModel masterMassDataChangeViewModel)
        {
            string userId = ControllerHelper.CurrentUser.UserId.ToString();
            ControllerHelper.ActionName = Constants.ActionDownloadMasterMassDataChangeForCarehome;

            /// Set the Business Model for Service
            /// Currently passing all values in single view model, we can simplify more here later.
            MasterMassDataChangeBusinessModel MasterMassDataChangeBusinessModel = new MasterMassDataChangeBusinessModel()
            {
                CustomerId = masterMassDataChangeViewModel.CustomerId,
                UserId = userId,
                OrderTypeId = masterMassDataChangeViewModel.OrderTypeId,
                CarehomeId = masterMassDataChangeViewModel.CarehomeId,
                PatientId = masterMassDataChangeViewModel.PatientId,
                PatientStatus = masterMassDataChangeViewModel.PatientStatusId,
                PatientType = masterMassDataChangeViewModel.PatientTypeId,
                DownloadTypeId = masterMassDataChangeViewModel.DownloadTypeId,
                ContactPersonType = masterMassDataChangeViewModel.ContactPersonType,
                ContactPatientCarehomeId = masterMassDataChangeViewModel.ContactPatientCarehomeId,
                ProductId = masterMassDataChangeViewModel.ProductId,
                PatientTypesForOrderUpdate = masterMassDataChangeViewModel.PatientTypesForOrderUpdate,
                CareHomeOrderTypesForOrderUpdate = masterMassDataChangeViewModel.CareHomeOrderTypesForOrderUpdate,
                OrderTypesForOrderUpdate = masterMassDataChangeViewModel.OrderTypesForOrderUpdate,
                PatientStatusForOrderUpdate = masterMassDataChangeViewModel.PatientStatusForOrderUpdate,
                CareHomeStatusForOrderUpdate = masterMassDataChangeViewModel.CareHomeStatusForOrderUpdate,
                OrderStatusForOrderUpdate = masterMassDataChangeViewModel.OrderStatusForOrderUpdate,
                PatientsForOrderUpdate = masterMassDataChangeViewModel.PatientsForOrderUpdate,
                CareHomesForOrderUpdate = masterMassDataChangeViewModel.CareHomesForOrderUpdate,
                OrderIdsForOrderUpdate = masterMassDataChangeViewModel.OrderIdsForOrderUpdate,
                CustomerIdForPostCodeMatrix = masterMassDataChangeViewModel.CustomerIdForPostCodeMatrix,
                OrderDataDownloadFor = masterMassDataChangeViewModel.OrderDataDownloadFor, ///Tells data to be downlaoded for patient/carehome/customer
                SAPProductIdForOrderUpdate = masterMassDataChangeViewModel.SAPProductIdForOrderUpdate,
                CareHomeStatusForCareHome = masterMassDataChangeViewModel.CareHomeStatusForCareHome,
                LanguageId = Convert.ToInt64(SCAEnums.OtherConstants.LanguageId, CultureInfo.InvariantCulture)
            };

            var returnListData = ControllerHelper.PostMethodServiceRequest<MasterMassDataChangeBusinessModel>(MasterMassDataChangeBusinessModel);

            if (returnListData != null)
            {
                string downloadMasterMassServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.MasterMassChangesDownloadSharedLocation);
                string fileNamePrefix = string.Empty;
                var columnsToRemove = new List<string>() { };

                /// start
                switch (masterMassDataChangeViewModel.DownloadTypeId)
                {
                    case (int)SCAEnums.MassDataTemplate.Patient:
                        {
                            /// download logic for Patient
                            if (returnListData.PatientDetailsList == null || returnListData.PatientDetailsList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.PatientDetailsList.Count;
                                /// Export to excel - start  
                                columnsToRemove = new List<string>() { "HouseNumber", "FaxNumber", "UserId", "ErrorMessage", "SelectedTemplate", "FilePath", "IsImport", "TransactionStatus", "UILogIPAddress", "Age", "SAPCarehomeID" };
                                fileNamePrefix = Constants.MasterMassChangesPatientPrefix;
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.PatientDetailsList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.Patient.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }
                    case (int)SCAEnums.MassDataTemplate.Carehome:
                        {
                            /// download logic for Carehome
                            if (returnListData.CarehomeDetailsList == null || returnListData.CarehomeDetailsList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.CarehomeDetailsList.Count;
                                columnsToRemove = new List<string>() { "CarehomeName", "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "TransactionStatus", "CareHomeStatusId", "CareHomeTypeId", "OrderTypeId", "UILogIPAddress" };
                                /// Export to excel - start  
                                fileNamePrefix = Constants.MasterMassChangesCarehomePrefix;
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.CarehomeDetailsList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.Carehome.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.Prescription:
                        {
                            /// download logic for Prescription
                            if (returnListData.PrescriptionList == null || returnListData.PrescriptionList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.PrescriptionList.Count;

                                /// Export to excel - start  

                                columnsToRemove = new List<string>() { "SelectedTemplate", "FilePath", "IsImport", "TransactionStatus", "ErrorMessage", "UserId", "UILogIPAddress" };

                                fileNamePrefix = Constants.MasterMassChangesPrescriptionPrefix;
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.PrescriptionList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove, true);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.Prescription.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.PatientClinicalContacts:
                        {
                            if (returnListData.ClinicalContactAnalysisList == null || returnListData.ClinicalContactAnalysisList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.ClinicalContactAnalysisList.Count;
                                /// Export to excel - start  
                                columnsToRemove = new List<string>() { "SAPPatientNumber", "AnalysisInformationTypeId", "AnalysisInformationType", "TransactionStatus", "AnalysisInformationValue", "AnalysisInformationValueId", "SAPCustomerNumber", "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                                fileNamePrefix = SCAEnums.MassDataTemplate.PatientClinicalContacts.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.ClinicalContactAnalysisList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.PatientClinicalContacts.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.PatientAnalysisInformation:
                        {
                            if (returnListData.ClinicalContactAnalysisList == null || returnListData.ClinicalContactAnalysisList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.ClinicalContactAnalysisList.Count;
                                /// Export to excel - start  
                                columnsToRemove = new List<string>() { "SAPPatientNumber", "ClinicalContactTypeId", "ClinicalContactType", "TransactionStatus", "ClinicalContactValue", "ClinicalContactValueId", "SAPCustomerNumber", "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                                fileNamePrefix = SCAEnums.MassDataTemplate.PatientAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.ClinicalContactAnalysisList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.PatientAnalysisInformation.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.CustomerClinicalContacts:
                        {
                            /// Write download logic for Customer Clinical Contacts
                            if (returnListData.ClinicalContactAnalysisList == null || returnListData.ClinicalContactAnalysisList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.ClinicalContactAnalysisList.Count;
                                /// Export to excel - start  
                                columnsToRemove = new List<string>() { "IndigoPortalPatientId", "AnalysisInformationTypeId", "AnalysisInformationType", "TransactionStatus", "AnalysisInformationValue", "AnalysisInformationValueId", "SAPCustomerNumber", "PatientId", "SAPPatientNumber", "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                                fileNamePrefix = SCAEnums.MassDataTemplate.CustomerClinicalContacts.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.ClinicalContactAnalysisList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.CustomerClinicalContacts.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.CustomerAnalysisInformation:
                        {
                            /// Write download logic for Customer Analysis Information
                            if (returnListData.ClinicalContactAnalysisList == null || returnListData.ClinicalContactAnalysisList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.ClinicalContactAnalysisList.Count;
                                /// Export to excel - start  
                                columnsToRemove = new List<string>() { "IndigoPortalPatientId", "ClinicalContactTypeId", "ClinicalContactType", "TransactionStatus", "ClinicalContactValue", "ClinicalContactValueId", "SAPCustomerNumber", "PatientId", "SAPPatientNumber", "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "UILogIPAddress" };
                                fileNamePrefix = SCAEnums.MassDataTemplate.CustomerAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.ClinicalContactAnalysisList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.CustomerAnalysisInformation.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.PostCodeMatrix:
                        {
                            /// download logic for Post Code Matrix
                            if (returnListData.PostCodeMatrixList == null || returnListData.PostCodeMatrixList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.PostCodeMatrixList.Count;
                                columnsToRemove = new List<string>() { "PostcodeMatrixID", "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "TransactionStatus", "UILogIPAddress" };
                                /// Export to excel - start  
                                fileNamePrefix = SCAEnums.MassDataTemplate.PostCodeMatrix.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.PostCodeMatrixList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.PostCodeMatrix.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }
                    case (int)SCAEnums.MassDataTemplate.CommunicationFormat:
                        {
                            /// Write download logic for Communication Format
                            if (returnListData.CommunicationInfoList == null || returnListData.CommunicationInfoList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.CommunicationInfoList.Count;
                                /// Export to excel - start                          
                                columnsToRemove = new List<string>() {"UserId", "SelectedTemplate", "FilePath",
                                "IsImport", "ErrorMessage", "TransactionStatus", "CommunicationPrefListId", "MarkettingPrefListId", "IsPatient" , "UILogIPAddress"};
                                fileNamePrefix = SCAEnums.MassDataTemplate.CommunicationFormat.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.CommunicationInfoList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.CommunicationFormat.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.ContactPerson:
                        {
                            /// download logic for Contact Person
                            if (returnListData.ContactPersonInfoList == null || returnListData.ContactPersonInfoList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.ContactPersonInfoList.Count;
                                /// Export to excel - start                          
                                columnsToRemove = new List<string>() { "ErrorMessage", "TransactionStatus", "UserId", "SelectedTemplate", "FilePath",
                                "IsImport", "UILogIPAddress"};
                                fileNamePrefix = SCAEnums.MassDataTemplate.ContactPerson.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.ContactPersonInfoList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.ContactPerson.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }

                    case (int)SCAEnums.MassDataTemplate.Order:
                        {
                            if (returnListData.OrderList == null || returnListData.OrderList.Count == 0)
                            {
                                masterMassDataChangeViewModel.Count = 0;
                            }
                            else
                            {
                                masterMassDataChangeViewModel.Count = returnListData.OrderList.Count;
                                columnsToRemove = new List<string>() { "ErrorMessage", "UserId", "SelectedTemplate", "FilePath", "IsImport", "TransactionStatus", "lngOrderType", "UILogIPAddress" };
                                /// Export to excel - start  
                                fileNamePrefix = SCAEnums.MassDataTemplate.Order.ToString(CultureInfo.InvariantCulture);
                                masterMassDataChangeViewModel.Returnpath = ExcelHelper.ExportToExcel(returnListData.OrderList, downloadMasterMassServerDirectory, fileNamePrefix, columnsToRemove);
                                LogHelper.LogAction(Constants.ActionLogDownloadMessage + SCAEnums.MassDataTemplate.Order.ToString(CultureInfo.InvariantCulture), Constants.MassDataChangesController, Constants.ActionMethodDownloadMasterMassDataChange, userId);
                                /// Export to excel - end 
                            }
                            break;
                        }
                    default:
                        break;
                }
            }
            
            /// end
            return masterMassDataChangeViewModel;
        }

        /// <summary>
        /// Get Import Initial Data
        /// </summary>
        /// <returns>view model</returns>
        public MasterMassDataChangeViewModel GetImportInitialData()
        {
            MasterMassDataChangeViewModel masterMassDataChangeViewModel = new MasterMassDataChangeViewModel();
            masterMassDataChangeViewModel.Customer = GenericHelper.GetAuthorizeCustomersForDropdown();
            return masterMassDataChangeViewModel;
        }

        /// <summary>
        /// Get Prescription Data
        /// </summary>
        /// <returns>view model</returns>
        public PrescriptionMassUpdateViewModel GetPrescriptionData()
        {
            PrescriptionMassUpdateViewModel prescriptionMassUpdateViewModel = new PrescriptionMassUpdateViewModel();
            prescriptionMassUpdateViewModel.Customer = GenericHelper.GetAuthorizeCustomersForDropdown();
            return prescriptionMassUpdateViewModel;
        }

        /// <summary>
        /// Upload Mass Update Data
        /// </summary>
        /// <param name="objMasterMassDataChangeViewModel"></param>
        /// <param name="isImport"></param>
        /// <returns>view model</returns>
        public MasterMassDataChangeViewModel UploadMassUpdateData(MasterMassDataChangeViewModel objMasterMassDataChangeViewModel, bool isImport)
        {            
            var objFileHelper = new FileHelper();
            bool status = false;
            if (objMasterMassDataChangeViewModel.UploadedFile != null)
            {

				templateFilePath = objFileHelper.GetTemplateFileName(objMasterMassDataChangeViewModel.SelectedTemplate, isImport);                
                objFileHelper = objFileHelper.UploadFile(objMasterMassDataChangeViewModel.UploadedFile, templateFilePath);                
                if (!objFileHelper.IsUplaodFailed)
                {                    
                    ExcelQueryFactory excelFactory;
                    templateFilePath = Path.Combine( "//" + GenericHelper.MassChangesTemplate, templateFilePath) + ".xls" ;
                    templateFilePath = System.Web.Hosting.HostingEnvironment.MapPath(templateFilePath);//+ string.Format(".{0}", Path.GetExtension(TemplateFilePath))); 
                    objMasterMassDataChangeViewModel.TemplateValidationRows = objFileHelper.ValidateExcelTemplate(templateFilePath, objFileHelper.FilePath, out excelFactory);

					/// Return the Message and Status
                    if (objMasterMassDataChangeViewModel.TemplateValidationRows.Count == 0)
                    {
                        status = true;

                        /// Call the Business Model for the File Name and Select Template Id
                        MassDataBusinessModel massChangesBusinessModel = new Business.BusinessModels.MassDataBusinessModel()
                        {
                            UserId = userId,
                            SelectedTemplate = objMasterMassDataChangeViewModel.SelectedTemplate,
                            IsImport = isImport, /// if true then Mass Data Import else Mass Data changes....
                            FilePath = objFileHelper.FilePath,
                            UILogIPAddress= GenericHelper.IPAddress
                        };
                        ControllerHelper.ActionName = Constants.ActionProcessUpdateData;
                        var uploadResponseData = ControllerHelper.PostMethodServiceRequestObject(massChangesBusinessModel);
						objMasterMassDataChangeViewModel.LogFilePath = Convert.ToString(uploadResponseData, CultureInfo.InvariantCulture);
						if (!string.IsNullOrEmpty(objMasterMassDataChangeViewModel.LogFilePath))
                        {
                            objMasterMassDataChangeViewModel.Message = Resource.Common.resDataUploadedSuccess;                                                        
                            objMasterMassDataChangeViewModel.LogFilePath = objFileHelper.CopyToVirtualLocation(objMasterMassDataChangeViewModel.LogFilePath, userId);
                        }
                        else 
                        {
                            objMasterMassDataChangeViewModel.Message = Resource.Common.resGenericErrorForWrong;
                        }
                    }
                    else
                    {
                        objFileHelper.RemoveFile(objFileHelper.FilePath);
                    }					
                }                
                objMasterMassDataChangeViewModel.Status = status;
            }
            return objMasterMassDataChangeViewModel;           
        }
       
        #region Download
        /// <summary>
        /// Get Template File Path
        /// </summary>
        /// <param name="selectedTemplateValue"></param>
        /// <param name="isImport"></param>
        /// <returns>file path</returns>
        public string GetTemplateFilePath(int selectedTemplateValue, bool isImport)
        {
            var objFileHelper = new FileHelper();
			string templateFileName = objFileHelper.GetTemplateFileName(selectedTemplateValue, isImport);
            string templateFilePath = string.Empty;
            templateFilePath = Path.Combine(GenericHelper.MassChangesTemplate, templateFileName) + ".xls";
            return templateFilePath;
        }      

        #endregion

    }
}