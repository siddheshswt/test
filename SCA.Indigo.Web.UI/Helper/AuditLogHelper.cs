﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : Gaurav Lad
// Last Modified On : 19-05-2015
// ***********************************************************************
// <copyright file="AuditLogHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************\

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.ViewModel;
    using System.Globalization;

    /// <summary>
    /// Audit Log Helper
    /// </summary>
    public class AuditLogHelper
    {
        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Gets the audit log search results.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="SearchTable">The search table.</param>
        /// <param name="CustomerId">The customer identifier.</param>
        /// <returns></returns>
        public List<CommonSearchBusinessModel> GetAuditLogSearchResults(string term, string searchTable, long? customerId)
        {
            var objSearchRequest = new CommonSearchBusinessModel 
            { 
                SearchTerm = term,
                TableName = searchTable,
                CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture),
                UserId = Guid.Parse(globalUserId),
            };
            ControllerHelper.ActionName = Constants.ActionNameGetAuditLogSearchResults;
           var customSearchResults = ControllerHelper.PostMethodServiceRequest<CommonSearchBusinessModel, List<CommonSearchBusinessModel>>(objSearchRequest);
           LogHelper.LogAction(Constants.ActionLogGetAuditLogSearchResults, Constants.AuditLogController, Constants.ActionGetAuditLogSearchResults, globalUserId);
           if (customSearchResults != null)
           {
               return customSearchResults;
           }
           else
           {
               return new List<CommonSearchBusinessModel>();
           }
        }

        /// <summary>
        /// Gets the audit log.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="auditLogType">Type of the audit log.</param>
        /// <param name="isExport">if set to <c>true</c> [is export].</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public AuditLogViewModel GetAuditLog(long page, long rows, long customerId, long? careHomeId, long? patientId, string fromDate, string toDate, long auditLogType, bool isExport, Guid userId)
        {

            if (customerId != null)
            {
                ControllerHelper.ActionName = Constants.ActionNameGetPatientOrCareHomeAuditLog;
                AuditLogBusinessModel postData = new AuditLogBusinessModel
                {
                    PageNumber = page,
                    RecordsPerPage = rows,
                    CustomerId = customerId,
                    CareHomeId = careHomeId,
                    PatientId = Convert.ToInt64(patientId, CultureInfo.InvariantCulture),
                    LoggedInUserId = Guid.Parse(globalUserId),
                    Fromdate = fromDate,
                    Todate = toDate,
                    IsExport = isExport,
                    AuditLogType = auditLogType,
                    SearchedUserId = userId
                };
                var PatientAuditLog = ControllerHelper.PostMethodServiceRequest<AuditLogBusinessModel>(postData);
                LogHelper.LogAction(Constants.ActionLogGetAuditLog, Constants.AuditLogController, Constants.ActionGetAuditLogSearchResults, globalUserId);

                AuditLogViewModel auditLogViewModel = new AuditLogViewModel
                {
                    PatientId = PatientAuditLog.PatientId,
                    PatientName = PatientAuditLog.PatientName,
                    PatientSAPId = PatientAuditLog.PatientSAPId != null ? PatientAuditLog.PatientSAPId : string.Empty,
                    CustomerId = PatientAuditLog.CustomerId,
                    CustomerName = PatientAuditLog.CustomerName,
                    CustomerSAPId = PatientAuditLog.CustomerSAPId != null ? PatientAuditLog.CustomerSAPId : string.Empty,
                    CareHomeId = PatientAuditLog.CareHomeId,
                    CareHomeName = PatientAuditLog.CareHomeName,
                    CareHomeSAPId = PatientAuditLog.CareHomeSAPId != null ? PatientAuditLog.CareHomeSAPId : string.Empty,
                    UserId = PatientAuditLog.LoggedInUserId,
                    UserName = PatientAuditLog.UserName,
                    AuditLogs = PatientAuditLog.Logs,
                    TotalRecords = PatientAuditLog.TotalRecords
                };
                return auditLogViewModel;
            }
            else
            {
                return new AuditLogViewModel();
            }

        }
    }
}