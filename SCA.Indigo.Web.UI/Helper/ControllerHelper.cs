﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : syerva
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="ControllerHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Web;
    using System.Web.Mvc;

    using Newtonsoft.Json.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using System.Threading.Tasks;

	/// <summary>
	/// Depreciated. Please do not use this class for new implementations. Eventually, this class file will be removed.
	/// Use WebServiceHelper instead of ControllerHelper
	/// </summary>
    public static class ControllerHelper
    {
        /// <summary>
        /// BaseUrl
        /// </summary>
        public static string BaseUrl { get; set; }

        /// <summary>
        /// ActionName
        /// </summary>
        public static string ActionName { get; set; }

        /// <summary>
        /// ActionParam
        /// </summary>
        public static string[] ActionParam { get; set; }

        /// <summary>
        /// ActionParamInt
        /// </summary>
        public static int[] ActionParamInt { get; set; }

        /// <summary>
        /// MethodType
        /// </summary>
        public static string MethodType { get; set; }

        /// <summary>
        /// currentUser
        /// </summary>
        private static UsersBusinessModel currentUser;

        /// <summary>
        /// CurrentUser
        /// </summary>
        public static UsersBusinessModel CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session[Constants.SessionUser] != null)
                {
                    currentUser = (UsersBusinessModel)HttpContext.Current.Session[Constants.SessionUser];
                }
                else
                {
                    currentUser = new UsersBusinessModel();
                }

                return currentUser;
            }

            set
            {
                currentUser = value;
                HttpContext.Current.Session[Constants.SessionUser] = currentUser;
            }
        }

        /// <summary>
        /// CurrentActionClicked
        /// </summary>
        public static string CurrentActionClicked { get; set; }

        /// <summary>
        /// ControllerHelper
        /// </summary>
        static ControllerHelper()
        {
            // Set the Base Url for servive from Configuration file
            BaseUrl = Convert.ToString(ConfigurationManager.AppSettings["ServiceBaseUrl"], CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Method to post the request.
        /// User only in case if return types are bool or string or long ...
        /// </summary>
        /// <param name="contract">The contract.</param>
        /// <returns>object.</returns>
        public static object PostMethodServiceRequestObject(object contract)
        {
            ServiceHelper objServiceHelper = null;
            try
            {
              //  ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
                var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
                object resultContract = null;
                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        resultContract = response.Content.ReadAsAsync<object>().Result;
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }

                ResetAccountParameters();
                return resultContract;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// Method to post the request async.        
        /// </summary>
        /// <param name="contract">The contract.</param>        
        public static void PostMethodServiceRequestAsync(object contract, string userId)
        {            
            ServiceHelper objServiceHelper = null;
            try
            {
               // ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
                var response = new HttpResponseMessage();

                Task.Factory.StartNew(() =>
                {
                    response = objServiceHelper.GetHttpResponse(contract, Guid.Parse(userId));
                });
            }
            catch (Exception e) 
            {
                LogHelper.LogException(e, userId);
            }
        }

        // to return single class
        /// <summary>
        /// PostMethodServiceRequest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="contract"></param>
        /// <returns></returns>
        public static T PostMethodServiceRequest<T>(T contract)
        {
            ServiceHelper objServiceHelper = null;
            try
            {
              //  ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
                var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
                T responseData = default(T);
                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        responseData = response.Content.ReadAsAsync<T>().Result;
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }

                    ResetAccountParameters();
                }
                return responseData;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// Method to post the request.
        /// User if return types are BOM objects
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T">The type of T.</typeparam>
        /// <param name="contract">The contract.</param>
        /// <returns>T.</returns>
        public static T PostMethodServiceRequest<T1, T>(T1 contract)
        {
            ServiceHelper objServiceHelper = null;
            try
            {
             //   ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
                var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
                T responseData = default(T);
                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(result))
                        {
                            if (result.StartsWith("{", StringComparison.InvariantCulture))
                            {
                                var jObj = JObject.Parse(result);
                                if (jObj.First != null && jObj.First.First != null)
                                {
                                    responseData = jObj.First.First.ToObject<T>();
                                }
                            }
                            else if (result.StartsWith("[", StringComparison.InvariantCulture))
                            {
                                var jArray = JArray.Parse(result);
                                if (jArray.Count > 0)
                                {
                                    responseData = jArray.ToObject<T>();
                                }
                            }
                        }
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
                ResetAccountParameters();
                return responseData;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// GetMethodServiceRequestForList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetMethodServiceRequestForList<T>()
        {
            ServiceHelper objServiceHelper = null;
            try
            {
              //  ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
                var actionParam = string.Empty;
                if (ActionParam != null)
                {
                    actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
                }

                objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);
                var response = objServiceHelper.GetHttpResponse(CurrentUser.UserId);
                T responseData = default(T);
                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (result.StartsWith("{", StringComparison.Ordinal))
                        {
                            var jObj = JObject.Parse(result);
                            if (jObj.First != null && jObj.First.First != null)
                            {
                                responseData = jObj.First.First.ToObject<T>();
                            }
                        }
                        else if (result.StartsWith("[", StringComparison.Ordinal))
                        {
                            var jArray = JArray.Parse(result);
                            if (jArray.Count > 0)
                            {
                                responseData = jArray.ToObject<T>();
                            }
                        }
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
                ResetAccountParameters();
                return responseData;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// GetMethodServiceRequest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="contract"></param>
        /// <returns></returns>
        public static T GetMethodServiceRequest<T>(object contract)
        {
            ServiceHelper objServiceHelper = null;
            try
            {
              //  ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper();

                var actionParam = string.Empty;
                if (ActionParam != null)
                {
                    actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
                }

                objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);

                var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
                T responseData = default(T);
                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                        if (jObj.First != null && jObj.First.First != null)
                        {
                            responseData = jObj.First.First.ToObject<T>();
                        }
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }
                ResetAccountParameters();               
                return responseData;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// GetMethodServiceRequestObject
        /// </summary>
        /// <param name="contract"></param>
        /// <returns></returns>
        public static object GetMethodServiceRequestObject(object contract)
        {
            ServiceHelper objServiceHelper = null;
            try
            {
               // ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };

                if (ActionParam != null)
                {
                    var actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));

                    objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);
                }

                HttpResponseMessage response;

                if (contract != null)
                {
                    response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
                }
                else
                {
                    response = objServiceHelper.GetHttpResponse(CurrentUser.UserId);
                }

                object resultContract = null;

                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        resultContract = response.Content.ReadAsAsync<object>().Result;
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }

                ResetAccountParameters();                

                return resultContract;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// GetMethodServiceRequest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetMethodServiceRequest<T>()
        {
            ServiceHelper objServiceHelper = null;
            try
            {
             //   ServiceHelper.BaseUrl = new Uri(BaseUrl);
                objServiceHelper = new ServiceHelper();

                var actionParam = string.Empty;
                if (ActionParam != null)
                {
                    actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
                }

                objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);

                var response = objServiceHelper.GetHttpResponse(CurrentUser.UserId);
                T responseData = default(T);
                if (response != null)
                {
                    /// TODO: Need to add more checks about correct response.
                    if (response.IsSuccessStatusCode)
                    {
                        var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                        if (jObj.First != null && jObj.First.First != null)
                        {
                            responseData = jObj.First.First.ToObject<T>();
                        }
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                    }
                }

                ResetAccountParameters();              

                return responseData;
            }
            finally
            {
                if (objServiceHelper != null)
                {
                    objServiceHelper.Dispose();
                }
            }
        }

        /// <summary>
        /// ResetAccountParameters
        /// </summary>
        private static void ResetAccountParameters()
        {
            ActionParam = new string[] { };
            ActionName = string.Empty;
        }

        /// <summary>
        /// GetJsonResult
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static JsonResult GetJsonResult(object obj)
        {
            return new JsonResult()
           {
               Data = obj,
               ContentType = "application/json",
               ContentEncoding = System.Text.Encoding.UTF8,
               JsonRequestBehavior = JsonRequestBehavior.AllowGet,
               MaxJsonLength = int.MaxValue
           };
        }

        /// <summary>
        /// ParseEnum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// AgeInYears
        /// </summary>
        /// <param name="birthday"></param>
        /// <param name="today"></param>
        /// <returns></returns>
        public static int AgeInYears(DateTime birthday, DateTime today)
        {
            return ((today.Year - birthday.Year) * 372 + (today.Month - birthday.Month) * 31 + (today.Day - birthday.Day)) / 372;
        }

        public static string SSOURLReferrer { get; set; }

        public static long CurrentUserRoleId { get; set; }      
    }


    /// <summary>
    /// ISessionState
    /// </summary>
    public interface ISessionState
    {
        /// <summary>
        /// Clear
        /// </summary>
        void Clear();

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="key"></param>
        void Delete(string key);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object Get(string key);

        /// <summary>
        /// Store
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void Store(string key, object value);
    }

    /// <summary>
    /// DefaultSessionState
    /// </summary>
    public class DefaultSessionState : ISessionState
    {

        /// <summary>
        /// session
        /// </summary>
        private readonly HttpSessionStateBase session;

        /// <summary>
        /// DefaultSessionState
        /// </summary>
        /// <param name="session"></param>
        public DefaultSessionState(HttpSessionStateBase session)
        {
            this.session = session;
        }

        /// <summary>
        /// Clear
        /// </summary>
        public void Clear()
        {
            session.RemoveAll();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="key"></param>
        public void Delete(string key)
        {
            session.Remove(key);
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object Get(string key)
        {
            return session[key];
        }

        /// <summary>
        /// Store
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Store(string key, object value)
        {
            session[key] = value;
        }
    }

    /// <summary>
    /// SessionExtensions
    /// </summary>
    public static class SessionExtensions
    {
        /// <summary>
        ///  Get<T>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sessionState"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(this ISessionState sessionState, string key) where T : class
        {
            return sessionState.Get(key) as T;
        }
    }
}