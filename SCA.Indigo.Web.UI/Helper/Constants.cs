﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : aminikam
// Created          : 11-28-2014
//
// Last Modified By : aminikam
// Last Modified On : 01-10-2015
// ***********************************************************************
// <copyright file="Constants.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using SCA.Indigo.Common.Enums;
using System;
using System.Globalization;
namespace SCA.Indigo.Web.UI.Helper
{
    public static class Constants
    {
		#region SessionName
        // Session Names
        public const string SessionDoNotShowForPatient = "IsDoNotShowForPatient";
        public const string SessionDoNotShowForCarehome = "IsDoNotShowForCarehome";
        public const string SessionUser = "User";
        public const string CurrentUserRoleId = "CurrentUserRoleId";
        public const string SessionPatientId = "patientId";        
        public const string SessionSampleFlag = "SampleFlag";
        public const string SessionCustomerId = "customerId";
        public const string SessionSearchCustomerId = "SearchCustomerId";
        public const string SessionPatientName = "PatientName";
        public const string SessionSearchPatientName = "SearchPatientName";
        public const string SessionCustomerName = "CustomerName";
        public const string SessionSearchCustomerName = "SearchCustomerName";
        public const string SessionSAPCustomerNumber = "SAPCustomerNumber";
        public const string SessionSearchSAPCustomerNumber = "SearchSAPCustomerNumber";
        public const string SessionSAPPatientNumber = "SAPPatientNumber";        
        public const string SessionIsOrderActived = "IsOrderActived";
        public const string SessionSearchIsOrderActived = "SearchIsOrderActived";
        public const string SessionCustomerParameters = "CustomerParameters";
        public const string SessionPostcodeMatrix = "PostCodeMatrix";
        public const string SessionAllowIncreaseOrderQty = "AllowIncreaseOrderQty";
        public const string IsCalledFromEdit = "IsCalledFromEdit";
        public const string SessionPatientStatus = "PatientStatus";
        public const string SessionIsLoadPrescription = "IsLoadPrescription";
        public const string SessionSearchPatientStatus = "SearchPatientStatus";
        public const string SessionNDDStartDate = "NDDStartDate";
        public const string SessionNDDEndDate = "NDDEndDate";
        public const string SessionOneOffNDD = "OneOffNDD";
        public const string SessionAddFromPrescription = "AddFromPrescription";
        public const string SessionAddFromProduct = "AddFromProduct";
        public const string SessionHasActiveOrder = "HasActiveOrder";
        public const string SessionCanActivateOrder = "CanActivateOrder";
        public const string SessionDateForOrderActivation = "DateForOrderActivation";
        public const string SessionIsTypeAutomaticOrder = "IsTypeAutomaticOrder";
        public const string SessionPatientType = "PatientType";
        public const string SessionOrderCreationAllowed = "OrderCreationAllowed";
        public const string SessionRoundId = "RoundID";
        public const string SessionDay1 = "Day1";
        public const string SessionDay2 = "Day2";
        public const string SessionDay3 = "Day3";
        public const string SessionDay4 = "Day4";
        public const string SessionDay5 = "Day5";
        public const string SessionApprovePatientId = "ApprovePatientId";
        public const string SessionApproveCustomerId = "ApproveCustomerId";
        public const string SessionApprovePatientName = "ApprovePatientName";
        public const string SessionApproveCustomerName = "ApproveCustomerName";
        public const string SessionisRedirectFromAuthorisation = "isRedirectFromApproval";
        public const string SessionOrderId = "OrderID";
        public const string SessionDerivedNDD = "DerivedNDD";
        public const string SessionAuthorizedUrl = "AuthorizedUrl";
        public const string SessionOrderSavedFromOneOff = "OrderSavedFromOneOff";
        public const string SessionNextDeliveryDate = "NextDeliveryDate";
        public const string SessionHolidayList = "HolidayList";
        public const string SessionAllCountyList = "CountyList";
        public const string SessionPatientNDD = "PatientNDD";
        public const string SessionpatientDisplayName = "PatientDisplayName";
        public const string SessionCustomerDisplayName = "CustomerDisplayName";
        public const string SessionPatientTypeMatrix = "PatientTypeMatrix";
        public const string SessionSearchParam = "SearchParameter";
        public const string SessionRedirectEvent = "RedirectEvent";
        public const string SessionRedirectFrom = "RedirectFrom";
        public const string SessionCareHomeDisplayName = "CareHomeDisplayName";
        public const string SessionCareHomeId = "CareHomeId";
        public const string SessionCareHomeSAPId = "CareHomeSAPId";
        public const string SessionCareHomeName = "CareHomeName";
        public const string SessionCareHomeStatus = "CareHomeStatus";
        public const string SessionCareHomeOrderType = "CareHomeOrderType";
        public const string LoginCookies = "LoginCookies";
        public const string IsCheckCarehomeStatus = "IsCheckCarehomeStatus";
        public const string IsCalledFromHomePage = "IsCalledFromHomePage";
        public const string SessionIsCalledFromCareHome = "SessionIsCalledFromCareHome";
        public const string InteractionId = "InteractionId";
        public const string SessionSearchOrderNumber = "SessionSearchOrderNubmer";
        public const string SessionIsCalledFromSearch = "SessionIsCalledFromSearch";
        public const string SessionSearchCustomer = "SessionSearchCustomer";
        public const string SessionSearchStatus = "SessionSearchStatus";
        public const string SessionSearchPatientType = "SessionSearchPatientType";
        public const string SessionSearchId = "SearchId";
        public const string SessionTypeToSearch = "SessionTypeToSearch";
        public const string SessionIsRedirectFromProductOrder = "IsRedirectFromProductOrder";
        public const string SessionIsCalledFromInteractionMenu = "SessionIsCalledFromInteractionMenu";
        public const string SessionInteractionPatientName = "SessionInteractionPatientName";
        public const string SessionInteractionCareHomeName = "SessionInteractionCareHomeName";
        public const string SessionInteractionCustomerName = "SessionInteractionCustomerName";
        public const string SessionOpenCarehomeFlag = "SessionOpenCarehomeFlag";
        public const string SessionCarehomeHeaderData = "CarehomeHeaderData";
        public const string SessionIsCopyDetails = "CopyDetails";
        public const string SessionPage = "Page";
        public const string SessionRows = "Rows";
        public const string SessionIsRemoved = "isRemoved";
        public const string SessionPreviousCustomerId = "PreviousCustomerId";
        public const string SessionCopyNextDeliveryDate = "NextDeliveryDate";
        public const string SessionIsCopyActiveOrder = "PreviousPatientActiveOrder";
        public const string SessionCopyPatientStatus = "CopyPreviousPatientStatus";
        public const string SessionCopyPatientId = "CopyPatientId";
        public const string SessionCutOffTime = "CutOffTime";
		#endregion


		#region LogAction
        // Controller Names for Log Action
        public const string AccountController = "Account";
        public const string HomeController = "Home";
        public const string SearchController = "Search";
        public const string MenuController = "Menu";
        public const string PatientController = "Patient";
        public const string OrderController = "Order";
        public const string AuthorizeController = "Authorize";
        public const string UserAdminController = "UserAdmin";
        public const string HolidayProcessController = "HolidayProcess";
        public const string CustomerController = "Customer";
        public const string CareHomeController = "CareHome";
        public const string CommonController = "Common";
        public const string AttachmentController = "Attachment";
        public const string AuditLogController = "AuditLog";
        public const string InteractionController = "Interaction";
        public const string InteractionMenu = "interactionmenu";

        public const string ViewEditPatientDetails = "View/Edit Patient";
        public const string ViewEditCareHomeDetails = "View/Edit Carehome";
        public const string ViewEditCustomerDetails = "Customer Maintenance";
        public const string SampleOrderMenuText = "Sample Order";
        public const string MassDataChangesController = "MassDataChanges";
        public const string MassDataImportController = "MassDataChanges";
        public const string ActionPrescriptionMassUpdate = "PrescriptionMassUpdate";
        public const string ActionMethodPrescriptionMassUpdate = "PrescriptionMassUpdate";
        public const string ActionPrescriptionMassUpdateSearch = "GetSearchResults";
        
        public const string ActionMethodGetClinicalContactValueMapping = "GetClinicalContactValueMapping";
        public const string ActionMethodGetClinicalContactRoleMapping = "GetClinicalContactRoleMapping";
        public const string ActionMethodSaveClinicalRoleMapping = "SaveClinicalRoleMapping";
        public const string ActionMethodSaveClinicalContactValue = "SaveClinicalContactValue";
        public const string ActionMethodGetParentValueForSelectedRoleType = "GetParentValueForSelectedRoleType";
        public const string ActionMethodClinicalContactMapping = "ClinicalContactMapping";
        public const string ActionMethodRefreshRoleTypeDropDown = "RefreshRoleTypeDropDown";
        public const string ActionNameGetCareHomeDetailsForProductOrder = "Order/GetCareHomeDetailsForProductOrder";

		// Actions for Log Action
		public const string ActionValidateNewPassword = " Validate New Password";
		public const string ActionGetCustomers = "GetCustomers";
		public const string ActionGetLists = "GetLists";
		public const string ActionGetAuditLogSearchResults = "GetAuditLogSearchResults";
		public const string ActionGetAuditLog = "GetAuditLog";

        // Log Action Messages
        public const string ActionChangePasswordSuccess = " Changed the password successfully.";
        public const string ActionChangePasswordFailed = " Password change failed";
        public const string ActionInvalidNewPassword = " Invalid new password";
        public const string ActionLoadCustomers = "Customer List Loaded";
        public const string ActionLoadPatientTypes = "Patient Type List Loaded";
        public const string ActionLoadPatientStatus = "Patient Status List Loaded";
        public const string ActionLoadMedicalStaffInfo = "Medical Staff and Analysis Info Loaded";
        public const string ActionLoadMedicalStaffInfoForPatient = "Medical Staff and Analysis Info Loaded For Patient ";
        public const string ActionLoadPatientDetails = "Patient Details Loaded";
        public const string ActionLoadCareHomeDetails = "CareHome Details Loaded";
        public const string ActionFillDropDownData = "Dropdown data Filled";
        public const string ActionLoadPrescriptionProduct = "Prescription Product Loaded";
        public const string ActionBindPatientInfo = "Bound PatientInfo";
        public const string ActionLoadMedicalStaffSearchResults = "Medical StaffSearch Results Loaded"; 
        public const string ActionLoadSearchResults = "Loaded search results";
        public const string ActionLoadPrescriptionDetails = "Prescription List Loaded";
        public const string ActionLogLoadPatientNotes = "Patient Notes loaded";
        public const string ActionLogLoadNotes = "Notes loaded";
        public const string ActionLogAddPatientNote = "New Patient Note Added";
        public const string ActionLogSaveNote = "New Note Added for ";
        public const string ActionLogSaveAlert = "Updated alert value for note";
        public const string ActionPatientContactDetails = "Patient Contact Loaded";
        public const string ActionLogSaveCustomerInfo = "Save Customer Information into database";
        public const string ActionLoadProductDetails = "Load Product Details";
        public const string ActionLoadProductSearchDetails = "Load Product Search Details";
        public const string ActionLogAuthorizePrescription = "Products Loaded for Prescription Authorization";
        public const string ActionLogApproveProducts = "Precription products approved.";
        public const string ActionSaveOrderSuccess = "Order Activated Successfully";
        public const string ActionSaveOrderFailed = "Order Save/Activation failed";
        public const string ActionLoadHolidayList = "Holiday List Loaded";
        public const string ActionLogLoadCustomerPostcode = "Postcode Matrix loaded for Customer";
        public const string ActionLogSaveCustomerPostcode = "Postcode Matrix Saved for Customer";
        public const string ActionLogMedicalStaffMasterList = "Medical Staff master list loaded";
        public const string ActionLogCPSaveSuccess = "Customer Parameter Saved Successfully for Customer ID :- ";
        public const string ActionLogCPSaveFailed = "Error while saving the Customer Parameter for Customer ID :- ";
        public const string ActionLogClinicalContactAnalysis = "Clinical Contact and Analysis information save request for Customer Id:- ";
        public const string ActionLogGetAuditLogSearchResults = "AuditLog search performed";
        public const string ActionLogGetAuditLog = "AuditLogs loaded";
        public const string ActionLogSearchFromPrescriptionUpdate = "Prescription Mass Update search loaded";        
        public const string ActionLogDownloadMessage = "File Downloaded for ";
        
        // Interaction Log Action Message
        public const string ActionLogInteractionGetPartialView = "Loads the Partial View of Interaction";

        public const string ActionSaveSampleOrderSuccess = "Sample Order Activated Successfully";
        public const string ActionSaveSampleOrderFailed = "Sample Order Save/Activation failed";

        public const string ActionSaveCareHomeMessage = "Save CareHome details";
        public const string ActionGetCareHomeMessage = "Get carehome details";
        public const string ActionGetCareHomePatientListMessage = "Get carehome patient list";
        public const string ActionGetCareHomeDataMessage = "Get carehome report data";
		public const string ActionSaveCommunityOrder = " Save community Orders";


		public const string ActionSaveSampleOrderDetails = "SaveSampleOrderDetails";

		//MassData
		public const string ActionMethodImportData = "ImportData";
		public const string ActionMethodGetMassDataImportDetails = "GetMassDataImportDetails";
		public const string ActionMethodDownloadMassDataTemplate = "DownloadMassDataTemplate";

		#endregion

		#region ViewName
       // View Name for controller
        public const string ActionChangePassword = "ChangePassword";
        public const string ActionWelcome = "Welcome";
        public const string ActionLogin = "Login";
        public const string ActionSearch = "Search";
        public const string ActionDetails = "Details";
        public const string ActionPatientDetails = "PatientDetails";
        public const string ActionSample = "Sample";        
        public const string ActionError = "Error";
        public const string ActionLogOff = "LogOff";
        public const string ActionAboutUs = "About";
        public const string ViewMapUserRoles = "MapUserRoles";
        public const string ViewMapUserCustomer = "MapUserCustomer";        
        public const string ViewMapUserCareHome = "MapUserCareHome";
        public const string ActionGetCareHome = "GetCareHome";
        public const string ActionSaveCareHome = "SaveCareHome";
        public const string ActionClinicalContactMapping = "ClinicalContactMapping";
        public const string ActionClinicalContactEditValue = "ClinicalContactEditValue";
        public const string ActionMasterMassDataChange = "MasterMassDataChange";
        public const string ActionMassDataImport = "MassDataImport";
        public const string ActionCopyDetails = "CopyDetails";
        public const string ActionSearchData = "SearchData";
        public const string ActionViewPatient = "ViewDetails";
		public const string ActionNameHolidayProcessAdmin = "HolidayProcessAdmin";
		#endregion

		#region AJAXURL
        public const string ActionResidentialActivated = "GetResidentialActivated";
		public const string ActionLoadPatientNote = "LoadPatientNotes";
		public const string ActionNextDeliveryAmendmentOrders = "GetNextDeliveryAmendmentOrders";
        public const string ActionCareHomeProductActivated = "GetCareHomeProductActivated";
        public const string ActionNameGetMedicalStaffAnalysisInfo = "MedicalStaffAnalysisInfo";
        public const string ActionGetPrescription = "GetPrescription";
        public const string ActionApprovePrescription = "ApprovePrescription";
        public const string ActionNameLoadPatientDetails = "LoadPatientDetails";
        public const string ActionRecentOrders = "RecentOrders";
        public const string ActionSampleOrder = "SampleOrder";
        public const string ActionSampleProductDetails = "SampleProductDetails";
        public const string ActionSaveCustomerInfo = "AddMedicalStaffAnalysisInfo";
        public const string ActionGetHolidayList = "GetHolidayList";
        public const string ActionGetPatientDetailsForOrder = "GetPatientDetailsForOrder";
        public const string ActionDisplayAttachment = "DisplayAttachment";
        public const string ActionPrescription = "Prescription";
        public const string ActionValidateUserAuthorizationPin = "ValidateUserAuthorizationPin";
        public const string ActionClearPrescriptionApprovalCache = "ClearPrescriptionApprovalCache";
        public const string ActionLoadCustomerPostcode = "LoadCustomerPostcode";
        public const string ActionSaveCustomerPostcode = "SaveCustomerPostcode";               
        public const string ActionLoadCustomerParameter = "LoadCustomerParameter";
        public const string ActionSaveCustomerParameter = "SaveCustomerParameter";
        public const string ActionPrintCustomerParameter = "PrintCustomerParameter";

		#region CustomerProductList
		public const string ActionControllerMethodGetCustomerProductList = "GetCustomerProductList";
		public const string ActionControllerMethodSaveCustomerProductList = "SaveCustomerProducts";
		#endregion

		#endregion

		#region ErrorMessage
        // Error Messages
        public const string ErrAccountLocked = "AccountLocked";
        public const string ErrAccountInActive = "AccountInActive";
        public const string ErrInvalidUser = "InvalidUser";
		#endregion
        
		#region WCFServiceNames
        // Service Action Names
		public const string ActionGetPatientDetails = "GetPatientDetails";
        public const string ActionNameResetPassword = "AccountManagement/ResetPassword";
        public const string ActionNameChangePassword = "AccountManagement/ChangePassword";
        public const string ActionNameLockUnlockUser = "AccountManagement/LockUnlcokUser";
        public const string ActionNameValidateNewPassword = "AccountManagement/ValidateNewPassword";
        public const string ActionNameGetUserDetails = "AccountManagement/GetUserDetails";
        public const string ActionNameTrackHomePageVisit = "AccountManagement/TrackHomePageVisit";
        public const string ActionNameCheckForNotification = "AccountManagement/CheckForNotification";
        public const string ActionNameSearchCustomer = "Search/GetAuthorizeCustomers";
        public const string ActionNameGetCustomerList = "CommonService/CustomerList";
        public const string ActionNameGetListType = "CommonService/GetListType";
        public const string ActionNameGetListTypeForClinicalContact = "CommonService/GetListTypeForClinicalContact";
        public const string ActionNameGetRoleTypeForClinicalContact = "CustomerParameter/GetRoleTypeForClinicalContact";
        public const string ActionNameGetRemovedPatientReasonCode = "CustomerParameter/GetRemovedPatientReasonCode";
        public const string ActionNameGetStoppedPatientReasonCode = "CustomerParameter/GetStoppedPatientReasonCode";
        public const string ActionNameGetListTypeForInteraction = "CommonService/GetListTypeForInteraction";
        public const string ActionNameSaveInteractionDetails = "Interaction/SaveInteractionDetails";
        public const string ActionNameGetCountyList = "CommonService/GetCountyList";        
        public const string ActionNameGetCareHomeDetails = "Patient/GetCareHomeDetails";
        public const string ActionNameSearchCareHomeDetails = "Patient/SearchCareHomeDetails";
        public const string ActionNameGetPatientStaffDetails = "Patient/GetpatientStaffDetails";
        public const string ActionNameGetSearchResult = "Search/GetSearchResult";
        public const string ActionNameGetPatientDetails = "Search/GetPatientDetails";
		public const string ActionNameGetGoogleSearchResults = "GoogleSearch/GetGoogleSearchResults";
        public const string ActionNameGetStatusDropDown = "Search/GetStatusDropdown";
        public const string ActionNameGetMedicalStaffSearchResults = "Patient/GetStaffSearchResult";
        public const string ActionNameGetUserMenuDetails = "Menu/GetUserMenuDetails";
        public const string ActionNameRetrievePatientDetails = "Patient/GetPatientDetails";
        public const string ActionNameGetPrescriptionProductList = "Patient/ProductList";      
        public const string ActionNameGetPrescriptionDetails = "Prescription/ProductList";
        public const string ActionNameGetMedStaffAnalysisInfo = "Patient/GetCustomerForMedicalAnalysis";
        public const string ActionGetMedicalStaffSearchResults = "DisplayMedicalStaffSearchResult";
        public const string ActionGetMedicalAnalysisInfo = "GetMedicalAnalysisInfo";
        public const string ActionNameGetCustomerDetails = "GetCustomerDetails";
        public const string ActionLoadPatientNotes = "Patient/LoadPatientNotes";
        public const string ActionGetNotes = "Notes/GetNotes";
        public const string ActionGetAlertNotes = "Notes/GetAlertNotes";
        public const string ActionAddPatientNote = "Patient/AddPatientNote";
        public const string ActionSaveNote = "Notes/SaveNote";
        public const string ActionSaveNoteForCopyPatient = "Notes/SaveNoteForCopyPatient";
        public const string ActionSaveAlert = "Notes/SaveAlert";
        public const string ActionGetProductDetails = "ProductDetails";
        public const string ActionGetProductList = "Product/GetProduct";
        public const string ActionGetSampleProductList = "Product/GetSampleProduct";
        public const string ActionNamePrescriptionDetails = "PrescriptionDetails";
        public const string ActionGetPatientContactInfo = "GetPatientContactInfo";
        public const string ActionGetPatientInfo = "Patient/GetPatientContacts";
        public const string ActionNameGetCommunityOrders = "Order/GetCommunityOrders";
        public const string ActionNameGetResidentialOrders = "Order/GetResidentialOrders";
        public const string ActionNameSaveOrders = "Order/SaveOrders";
        public const string ActionNameGetArrangedDeliveryDate = "CommonService/GetArrangedDeliveryDate";
        public const string ActionPatientTypeMatrix = "CustomerParameter/GetPatientTypeMatrixByCustomerId";
        public const string ActionSavePatientTypeMatrix = "SavePatientTypeMatrix";
        public const string ActionGetPatientAssociatedWithPatient = "CustomerParameter/GetPatientAssociatedWithPatient";
        public const string ActionCPGetCustomerParameter = "CustomerParameter/GetCustomerParameter";
        public const string ActionCPSaveCustomerParameter = "CustomerParameter/SaveCustomerParameter";
        public const string ActionNameGetCreateInteractionDetails = "Patient/GetCreateInteractionDetails";
        public const string ActionNameGetAutomaticOrderData = "Order/GetAutomaticOrderData";
        public const string ActionNameGenerateAutomaticOrderCriteria = "Order/GenerateAutomaticOrderCriteria";
        public const string ActionCPRemoveReasonCodeMapping = "CustomerParameter/RemoveReasonCodeMapping";
        public const string ActionMassUpdatePrescription = "MassDataChanges/MassUpdatePrescription";
        public const string ActionDownloadPrescription = "MassDataChanges/DownloadPrescription";
        public const string ActionMassImportPostCodeMatrix = "MassDataImport/MassImportPostCodeMatrix";
        public const string ActionMassImportCareHome = "MassDataImport/MassInsertUpdateCareHome";
        public const string ActionMassImportCustomerClinicalContactAnalysis = "MassDataImport/MassInsertCustomerClinicalContactAnalysis";
        public const string ActionProcessImportData = "MassDataImport/ProcessImportData";
		public const string ActionProcessUpdateData = "MassDataChanges/ProcessMassUpdate";

        public const string ActionNameAutomaticOrderExportToExcel = "AutomaticOrderCriteriaExportToExcel";

        public const string ActionGetClinicalContactValueMapping = "CustomerParameter/GetClinicalContactValueMappingByCustomerId";
        public const string ActionGetParentValueForSelectedRole = "CustomerParameter/GetParentValueForSelectedRole";        
        public const string ActionGetRoleNameForClinicalContactByChildId = "CustomerParameter/GetRoleNameForClinicalContactByChildId";
        public const string ActionGetClinicalContactRoleMapping = "CustomerParameter/GetClinicalContactRoleMappingByCustomerId";
        public const string ActionNameSaveClinicalRoleMapping = "CustomerParameter/SaveClinicalRoleMapping";
        public const string ActionNameRemoveClinicalContactAnalysisMapping = "CustomerParameter/RemoveClinicalContactAnalysisMapping";
        public const string ActionMethodRemoveClinicalContactAnalysisMapping = "RemoveClinicalContactAnalysisMapping";
		public const string ActionGetCustomerProducts = "Customer/GetCustomerProductList";
		public const string ActionSaveCustomerProducts = "Customer/SaveCustomerProductList";

        public const string ActionNameSaveSampleOrders = "Order/SaveSampleOrders";
        public const string ActionNameSendSampleOrders = "Order/SendOrder";
        public const string ActionNameCheckSampleOrdersInHelix = "Order/CheckSampleOrdersInHelix";
        public const string ActionNameGetPatientBasicDetails = "Patient/GetPatientBasicDetails";
        public const string ActionNameGetPostCodeMatrix = "CommonService/GetPostCodeMatrixForPatient";
        public const string ActionNamePostCodeMatrix = "CommonService/GetPostcodeMatrix";
        public const string ActionNameRemovePrescription = "Patient/RemovePrescription";
        public const string ActionNameGetHolidayList = "CommonService/GetHolidayList";
        public const string ActionNameGetAllHolidayList = "CommonService/GetAllHolidayList";
        public const string ActionNameRemoveOrderProduct = "Order/RemoveProductFromOrder";
        public const string ActionNameGetOrderActivationDetails = "Order/GetOrderActivationDetails";
        
        public const string ActionSaveContactInfo = "Patient/SaveContactInfo";
        public const string ActionSaveContactInfoForCopyPatient = "Patient/SaveContactInfoForCopyPatient";
        public const string ActionSaveCustomerContactInfo = "Patient/SaveCustomerContactInfo";
        public const string ActionSaveCareHomeContactInfo = "Patient/SaveCareHomeContactInfo";
		public const string ActionGetPatientCurrentNDD = "Patient/GetPatientCurrentNDD";
        public const string ActionGetPatientNDDForAddProduct = "Patient/GetPatientNDDForFirstProduct";

		public const string ActionGetNDDForNewCommunityPatient = "NDDCalculation/GetNDDForNewCommunityPatient";
		public const string ActionGetNDDForPatient = "NDDCalculation/GetNDDForPatient";

        public const string ActionRemoveContactInfo = "Patient/RemoveContactInfo";
        public const string ActionCheckProductExist = "Patient/CheckProductExist";
        public const string ActionGetCareHomeOrders = "Order/GetCareHomeOrders";
        public const string ActionGetCommunityActivated = "Order/GetCommunityOrders";
        public const string ActionGetActivatedOrders = "Order/GetActivatedOrders";
        public const string ActionGetOneActivatedOrders = "Order/GetOneOffActivatedOrders";

        public const string ActionGetPatientEditDetails = "Patient/GetPatientEditDetails";
        public const string ActionGetIsPatientOrderActivated = "Patient/IsPatientOrderActivated";
        public const string ActionGetCustomerPatientType = "Patient/GetCustomerPatientType";
        public const string ActionGetPatientTypeMatrix = "CommonService/GetPatientTypeMatrix";
        public const string ActionMethodGetPatientTypeMatrix = "GetPatientTypeMatrix";
        public const string ActionMethodExportPatientTypeMatrix = "ExportPatientTypeMatrix";        
        public const string ActionMethodGetPatientTypeAssociatedWithPatient = "GetPatientTypeAssociatedWithPatient";
        public const string ActionGetProductLevelActivated = "Order/GetProductLevelActivated";
        
        public const string ActionLoadNurseDetails = "Patient/LoadNurseDetails";        

        public const string ActionNameSaveCustomerInformation = "Patient/AddMedStaffAnalysisInfo";
        public const string ActionNameGetRecentOrders = "Order/GetRecentOrders";
        public const string ActionNameGetProductDetails = "Product/GetProductDetails";
        public const string ActionNameGetSampleProductDetails = "Product/GetSampleProductDetails";
        public const string ActionNameGetProductSearchDetails = "Product/GetSearchResultProduct";
        public const string ActionAddPatientDetails = "Patient/SavePatientDetails";
        public const string ActionUpdatePatientDetails = "Patient/SavePatientDetailsRemovedStopped";
        
        public const string ActionAuthorizePrescription = "AuthorizePrescription/GetAuthorizePrescription";
        public const string ActionApprovePrescriptionProducts = "AuthorizePrescription/ApprovePrescription";
        public const string ActionNameGetUserAuthorizationPin = "AuthorizePrescription/ValidateUserAuthorizationPin";

        public const string ActionGetCustomerParameter = "CommonService/GetCustomerParameter";
        public const string GetCareHomeAddressDetails = "Patient/GetCareHomeAddressdetails";

        public const string ActionNameGetAllUser = "UserAdminService/GetAllUsers";
        public const string ActionNameGetAllRoles = "UserAdminService/GetAllRoles";
        public const string ActionNameGetMappedUserToCustomer = "UserAdminService/GetMappedUserToCustomer";
        public const string ActionNameGetMenuDetails = "UserAdminService/GetMenuDetails";
        public const string ActionNameGetAdminUserId = "UserAdminService/GetAdminUserId";
        public const string ActionNameSaveUserCustomerMapping = "UserAdminService/SaveUserCustomerMapping";
        public const string ActionNameSaveUserCareHomeMapping = "UserAdminService/SaveUserCareHomeMapping";
        public const string ActionNameGetAllUserCustomerMapping = "UserAdminService/GetAllUserCustomerMapping";
        public const string ActionNameSaveRoleMenuMapping = "UserAdminService/SaveRoleMenuMapping";
        public const string ActionNameSaveUserRoleMapping = "UserAdminService/SaveUserRoleMapping";
        public const string ActionGetUserToAllCustomer = "GetUserToAllCustomer";
        public const string ActionGetCustomerDetails = "UserAdminService/GetCustomerDetails";
        public const string ActionNameSaveHolidayProcessData = "UserAdminService/SaveHolidayProcessData";
        public const string ActionNameSaveNewRole = "UserAdminService/SaveNewRole";
        public const string ActionNameSaveNewRoleMenuMapping = "UserAdminService/SaveNewRoleMenuMapping";
        public const string ActionNameGetCustomerPostcodeMatrix = "CustomerParameter/GetCustomerPostcodeMatrix";
        public const string ActionNameGetMasterListById = "CustomerParameter/GetMasterListById";
        public const string ActionNameGetInteractionSummary = "Interaction/GetInteractionSummaryDetails";
        public const string ActionNameGetLogisticProviderName = "Interaction/GetLogisticProviderName";
        public const string ActionNameSavePatientTypeMatrixDetails = "CustomerParameter/SavePatientTypeMatrixDetails";
        public const string ActionNameSaveClinicalContactValueMapping = "CustomerParameter/SaveClinicalContactValueMapping";        
        public const string ActionNameSaveCustomerPostcodeMatrix = "CustomerParameter/SaveCustomerPostcode";
        public const string ActionNameGetCreateInteractionByIntreactionId = "Interaction/GetCreateInteractionByInteractionId";        
        public const string ActionNameGetCustomersForDropDown = "CommonService/GetCustomersForDropDown";
        public const string ActionNameGetCareHomesForDropDown = "CommonService/GetCareHomesForDropDown";
        public const string ActionNameGetProductsOnCustomerId = "Product/GetProductsOnCustomerId";
        public const string ActionNameGetCareHomeOnCustomerId = "CareHome/GetCareHomeOnCustomerId";
        public const string ActionNameGetUsersOnInteraction = "CommonService/GetUsersOnInteractionForDropDown";
        public const string ActionNameGetCareHomeUsers = "CommonService/GetCareHomeUsersOnCustomerId";
        public const string ActionNameIsCareHomeUser = "CommonService/IsCareHomeUser";

        public const string ActionNameGetInteractionReportSummary = "Interaction/GetInteractionReportSummary";
        public const string ActionNameGetUsersResetUnlock = "AccountManagement/GetUserResetUnlockSummary";

		public const string ActionNameUploadAttachment = "Attachment/UploadAttachment";
		public const string ActionNameRemoveAttachment = "Attachment/RemoveAttachment";
		public const string ActionNameGetAttachmentPath = "Attachment/GetAttachmentPath";
		public const string ActionNameDisplayAttachment = "Attachment/DisplayAttachment";

		#region AuditLog

		public const string ActionNameGetPatientOrCareHomeAuditLog = "AuditLog/GetPatientOrCareHomeAuditLog";
		public const string ActionNameGetAuditLogSearchResults = "AuditLog/GetAuditLogSearchResults";
		public const string ActionNameSaveAuditLogRemarks = "AuditLog/SaveAuditLogRemarks";

		#endregion

		#endregion


        // Mass Changes
        public const string ActionMethodMasterMassDataChange = "MasterMassDataChange";
        public const string ActionMethodDownloadMasterMassDataChange = "DownloadMasterMassDataChange";
        public const string ActionDownloadMasterMassDataChangeForCarehome = "MassDataChanges/DownloadMasterMassDataChange";
        public const string ActionMethodMassDataImport = "MassDataImport/DownloadMasterMassDataChange";


        // Report Builder
        public const string ReportBuilderFormatId = "ReportBuilderFormatId";
        public const string ReportFormatName = "ReportFormatName";
        public const string ActionNameSaveReportBuilder = "ReportBuilderService/SaveReportBuilder";
        public const string ActionNameGetReportFormat = "ReportBuilderService/GetReportFormat";
        public const string ActionNameGetReportFormatDropDown = "ReportBuilderService/GetReportFormatDropDown";

        // CareHome Information
        public const string ActionNameSaveCareHome = "CareHome/SaveCareHome";
        public const string DefaultCountry = "GB";
        public const string ActionNameGetCareHome = "CareHome/GetCareHome";
        public const string ActionNameGetCareHomePatientList = "CareHome/GetPatientDetails";
        public const string ActionNameGetCareHomeReportData = "CareHome/GetCareHomeReportData";
        public const string ActionNameGetCareHomeReportMaintenanceData = "CareHome/GetCareHomeReportMaintenaceData";

        // Customer Information
        public const string ActionNameGetCustomerInformation = "Customer/GetCustomerInformation";
        public const string ActionCustomerInformation = "CustomerInformation";
        public const string ActionNameGetCustomerPrescriptionInformation = "Customer/GetCustomerPrescriptionApprovals";
        public const string ActionNameSaveCustomerInformationDetails = "Customer/SaveCustomerInformation";
        public const string ActionNameSaveCustomerPrescriptionApprovals = "UserAdminService/SaveCustomerByUserMapping";
        public const string ActionNameSaveCustomerAuthorisedUserPrescriptionApprovals = "Customer/SaveAuthorisedUserPrescription";
        public const string ActionNameRemoveCustomerPrescriptionApprovals = "Customer/RemoveCustomerPrescriptionApproval";
        public const string ActionNameCheckActivePatientsByCustomer = "Customer/CheckActivePatientsByCustomer";
        public const string ActionNameGetUserDetailsByUserId = "Customer/GetUserDetailsByUserId";
        public const string ActionMethodGetHierarchyLinkData = "Customer/GetHierarchyLinkData";

		//Report Builder Actions
        public const string ActionNameOpenReportBuilder = "OpenReportBuilder";
        public const string ActionNameGetReportData = "ReportBuilderService/GetReportData";
        public const string ActionNameOpenInteractionMenu = "OpenInteractionMenu";        

		#region Keys
        // Keys/parameters
        public const string CustomerId = "CustomerId";
        public const string CustomerName = "CustomerName";
        public const string ListId = "ListId";
        public const string DisplayText = "DisplayText";
        public const string ListCode = "ListCode";
        public const string Value = "Value";
        public const string Analysis = "Analysis";
        public const string Staff = "Staff";
        public const string AddCustomerDescription = "Description";
        public const string Selfcare = "Selfcare";
        public const string ChildListId = "ChildListId";
        public const string ChildValue = "ChildValue";
        public const string ChildValueId = "ChildValueId";
        public const string StatusDropDownvalue = "StatusDropDownvalue";
        public const string StatusDropDownText = "StatusDropDownText";

        // View Names
        public const string ViewPatientInfo = "_PatientInfo";
        public const string ViewPatientDetails = "details";
		public const string InteractionSummary = "_InteractionSummaryPopup";
        public const string CreateInteraction = "_CreateInteractionPopup";
        public const string AttachmentView = "_AttachmentPopup";
        public const string ReportBuilder = "ReportBuilder";

        // Default GUID
        public const string DefaultGuid = "00000000-0000-0000-0000-000000000000";

        // Deafult lead time
        public const int DefaultLeadTime = 3;

        // Format Provider
        public const string DateFormat = "dd/MM/yyyy";
        public const string DateFormat4 = "MM/dd/yyyy";
        public const string DateFormat1 = "M/dd/yyyy";
        public const string DateFormat2 = "M/d/yyyy";
        public const string DateFormat3 = "MM/d/yyyy";
        public const string DateTimeFormat = "dd/MM/yyyy HH:mm";
        public const string DateTimeFormatyy5 = "yyyyMMddhhmmss";

        public const string TimeFormat = "h:mm tt";
        public const string TimeFormat1 = "HH:MM tt";
        public const string TimeFormat3 = "HH:mm";       

        //TimeZone
        public const string CetTimeZone = "Central European Standard Time";
        public const string BritishTimeZone = "GMT Standard Time";

        // url for service
        public const string GetListCustomerDataObjectServiceUrl = "GetListCustomerDataObject/{ListId}/{UserId}";
        public const string GetMedicalStaffSearchResultsServiceUrl = "GetMedicalstaffList/{txtSearch}/{customerId}/{listId}/{userId}";

        // js undefined
        public const string Undefined = "undefined";
        
        // User Role
        public const string SCAUserType = "SCA";
        public const string DisplayRoleName = "Display";
        public const string CareHomeRoleName = "Care Home";
        public const string ExternalUserType = "External";
        public const string ReportsRoleName = "Reports";
        public const string NurseRoleName = "Nurse";

        //User
        public const string AdminUser = "SCAAdmin";

        // reason code
        public const string ActiveReasonCode = "Active";
        public const string StoppedReasonCode = "Stopped";

        // reason Code list Type Id
        public const string ActiveReasonCodeId = "10020";
        public const string StoppedReasonCodeId = "10021";
        public const string RemovedReasonCodeId = "10022";

        // gender
        public const string MaleGender = "Male";
        public const string FemaleGender = "Female";

        // County
        public const string CountyCode = "CountyCode";
        public const string CountyText = "CountyText";

        // User
        public const string UserId = "UserId";
        public const string UserName = "UserName";
        public const string Password = "Password";

        //Roles
        public const string RoleId = "RoleId";
        public const string RoleName = "RoleName";
        public const string RememberMe = "RememberMe";

        //County
        public const int HourFormat1 = 12;
        public const int HourFormat2 = 23;
        public const int MinuteFormat1 = 59;
        public const string MinuteFormat2 = "00";
        public const string Zero = "0";

        // ContactInforParents
        public const string PatientContactInfo = "PatientContactInfo";
        public const string CustomerContactInfo = "CustomerContactInfo";
        public const string CareHomeContactInfo = "CareHomeContactInfo";

        //  CustomerParameter
        public const string CustomerParameter = "CustomerParameter";
        
        //File name prefix
        public const string AuditLogPrefix = "AuditLog";
        public const string AutomaticOrderCriteriaPrefix = "AutomaticOrderCriteria";
        public const string ReportBuilderPrefix = "ReportBuilder";
        public const string PatientTypeMatrixPrefix = "PatientTypeMatrix";
        public const string MasterMassChangesCarehomePrefix = "MasterMassChangesCarehome";
        public const string MasterMassChangesPatientPrefix = "Patient";
        public const string MasterMassChangesPrescriptionPrefix = "MasterMassChangesPrescription";
        public const string MasterMassChangesPostCodeMatrixPrefix = "MasterMassChangesPostCodeMatrix";
        public const string MasterMassChangesContactPersonPrefix = "MasterMassChangesContactPerson";
        public const string InteractionReportPrefix = "InteractionReport";
        public const string CareHomePrefix = "CareHome";
        public const string CarehomePDFPrefix = "CarehomePDF";
        public const string SampleLetterPrefix = "SampleLetter";

        //File name prefix
        public const string FileFormat = ".xlsx";
        public const string PdfFileExtension = ".pdf";
        public const string WordFileFormat = ".doc";

		public const string FrequencyColumnName = "Frequency";

        // seperator
        public const string Underscore = "_";

        // Number of Header rows and Header column
		public const int hdrRow = 3;
        public const int hdrCol = 3;


        //PDF Section

        public const string PDFDocumentFont = "Arial";
        public const int PDFDocumentFontSize = 9;
        public const string PDFCareHomeReportHeaderPosition = "20 20 770 590";
        public const string PDFCareHomeReportContentPosition = "20 30 770 ";
        public const string PDFPageNoPosition = "20 10 770 20";
        public const string PDFAlternateRowColor = "242 242 242";
        public const int PDFCareHomeReportHeaderCellPadding = 3;
        public const int PDFCareHomeReportHeaderColumnCount = 6;
        public const int PDFCareHomeReportDetailsColumnCount = 13;
        public const int PDFInteractionReportFirstPageColumnCount = 9;
        public const int PDFInteractionReportSecondPageColumnCount = 8;
        public const int PDFCareHomeReportProductDescriptionLength = 70;
        public const int PDFIntercationReportDescriptionLength = 1800;
        public const int PDFCareHomeReportMinLastRowPos = 50;
        public const int PDFCareHomeReportContentSpacing = 30;
        public const int PDFCareHomeReportHeaderRowCount = 3;
        public const int PDFInteractionReportHeaderCellPadding = 3;
        public const double PDFCareHomeReportDetailsCellPadding = 2.5;
        public const int PDFInteractionReportInset = 20;
        public static double[] PDFInteractionReportHeaderColWidth = new double[] { 3, 0.9, 2, 0.9, 3, 0.9, 1.5, 2, 1.1 };
		public static double[] PDFInteractionReportHeaderColWidthP = new double[] { 0.7, 0.8, 1, 0.8, 1, 5, 0.5, 1 };
        public static double[] PDFCareHomeReportHeaderColumnWidth = new double[] { 0.85, 2, 0.9, 1, 1, 1 };
        public static double[] PDFCareHomeReportDetailsColumnWidth = new double[] { 0.9, 0.5, 0.6, 0.35, 0.6, 0.65, 0.5, 2, 0.45, 0.4, 0.4, 0.6, 0.5 };
        private static double pdfCareHomeReportTableBorderWidth = 0.2;
		public static double PDFCareHomeReportTableBorderWidth
		{
			get
			{
				return pdfCareHomeReportTableBorderWidth;
			}
			set
			{
				pdfCareHomeReportTableBorderWidth = value;
			}
		}
        private static double pdfLeftAlign = 0;
		public static double PDFLeftAlign
		{
			get
			{
				return pdfLeftAlign;
			}
			set
			{
				pdfLeftAlign = value;
			}
		}
        private static double pdfCenterAlign = 0.5;
		public static double PDFCenterAlign
		{
			get
			{
				return pdfCenterAlign;
			}
			set
			{
				pdfCenterAlign = value;
			}
		}
        private static double pdfRightAlign = 1;
		public static double PDFRightAlign
		{
			get
			{
				return pdfRightAlign;
			}
			set { pdfRightAlign = value; }
		}
        // Working days
        public const int SampleOrderWorkingDays = 5;

        // Sample Product Type
        public const string SampleOrderProductType = "tena/customer";

        /// <summary>
        /// Selfcare Patient Type for NDD calculation
        /// </summary>
		public readonly static string SelfcarePatientType = Convert.ToString((long)SCAEnums.PatientType.SelfCare, CultureInfo.InvariantCulture) + "," +
															Convert.ToString((long)SCAEnums.PatientType.PaediatricSelfcare, CultureInfo.InvariantCulture) + "," +
															Convert.ToString((long)SCAEnums.PatientType.ChildSelfCare, CultureInfo.InvariantCulture) + "," +
															Convert.ToString((long)SCAEnums.PatientType.CommunitySelfcareNursing, CultureInfo.InvariantCulture) + "," +
															Convert.ToString((long)SCAEnums.PatientType.CommunitySelfcareResidential, CultureInfo.InvariantCulture);
        /// <summary>
        /// Procare Patient type for NDD calculation
        /// </summary>
		public readonly static string ProcarePatientType = Convert.ToString((long)SCAEnums.PatientType.Procare, CultureInfo.InvariantCulture) + "," +
														   Convert.ToString((long)SCAEnums.PatientType.PaediatricProcare, CultureInfo.InvariantCulture) + "," +
														   Convert.ToString((long)SCAEnums.PatientType.Bulk, CultureInfo.InvariantCulture);


		#endregion

		#region NeedCleanUp
		public const string ActionGetPatients = "GetPatients";
		public const string ActionGetPrescriptions = "GetPrescriptions";
		public const string ActionGetCareHomeDetails = "GetCareHomeDetails";
		public const string ActionLoadPatientsDropDown = "LoadPatientsDropdown";
		public const string ActionGetPrescriptionProduct = "GetPrescriptionProduct";
		public const string ActionLoadPatientInfo = "BindPatientInfo";
		public const string ActionCommunityActivated = "GetCommunityActivated";
		public const string ActionProductActivated = "GetProductLevelActivated";
		public const string ActionRemoveCommunityActivated = "RemoveCommunityActivated";
		public const string ActionSearchCarehomeDetails = "SearchCarehomeDetails";
		public const string ActionSaveContactInfoDetails = "SaveContactInfoDetails";
		public const string ActionRemoveContactInfoDetails = "RemoveContactInfoDetails";
		#endregion
    }
}