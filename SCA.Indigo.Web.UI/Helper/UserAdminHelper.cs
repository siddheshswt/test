﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           :Sahoo Basantakumar
// Created          : 03-16-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-27-2015
// ***********************************************************************
// <copyright file="UserAdminHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web.Mvc;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Web.UI.ViewModel;
    using SCA.Indigo.Web.UI.ViewModel.UserAdmin;
    using SCA.Indigo.Common.Enums;

    /// <summary>
    /// User Admin Helper
    /// </summary>
    public class UserAdminHelper
    {
        #region User To Role Mappings

        #region Constants

        /// <summary>
        /// User Id
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Sca User
        /// </summary>
        private const string SCAUSER = "SCA";

        /// <summary>
        /// External User
        /// </summary>
        private const string EXTERNALUSER = "External";

        #endregion

        /// <summary>
        /// Get All Users
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="customerUser">The customer user.</param>
        /// <returns>
        /// return list of users
        /// </returns>
        public List<UsersBusinessModel> GetAllUsers(string customerId, string customerUser)
        {
            string isInteraction = Convert.ToString(false, CultureInfo.InvariantCulture);
            ControllerHelper.ActionName = Constants.ActionNameGetAllUser;
            ControllerHelper.ActionParam = new[] { customerId, customerUser, userId, isInteraction };
            var userResponse = ControllerHelper.GetMethodServiceRequestForList<List<UsersBusinessModel>>();
            return userResponse;
        }

        /// <summary>
        /// Get Map User Role
        /// </summary>
        /// <returns>
        /// return view model
        /// </returns>
        public UserAdminViewModel GetMapUserRoles()
        {
            UserAdminViewModel userAdminViewModel = new UserAdminViewModel();

            // Map All Roles
            ControllerHelper.ActionName = Constants.ActionNameGetAllRoles;
            var userResponse = ControllerHelper.GetMethodServiceRequestForList<List<UsersBusinessModel>>();
            userAdminViewModel.Roles = new SelectList(userResponse, Constants.RoleId, Constants.RoleName);

            return userAdminViewModel;
        }

        /// <summary>
        /// Get Roles To Functionality
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>
        /// return list
        /// </returns>
        public List<UserMenuDetailsBusinessModel> GetRolesToFunctionality(string roleId)
        {
            long languageId = (int)SCAEnums.OtherConstants.LanguageId;
            ControllerHelper.ActionName = Constants.ActionNameGetMenuDetails;
            ControllerHelper.ActionParam = new[] { roleId, languageId.ToString(CultureInfo.InvariantCulture) };
            List<UserMenuDetailsBusinessModel> rolesToFunctionalityMapping = ControllerHelper.GetMethodServiceRequestForList<List<UserMenuDetailsBusinessModel>>();

            return rolesToFunctionalityMapping;
        }

        /// <summary>
        /// Save Role to functionality
        /// </summary>
        /// <param name="roleMenuMappingList">The role menu mapping list.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        public int SaveRolesToFunctionality(List<UserMenuDetailsBusinessModel> roleMenuMappingList, string roleId)
        {
            UserAdminBusinessModel userAdminBusinessModel = new UserAdminBusinessModel();
            userAdminBusinessModel.RoleId = roleId;
            userAdminBusinessModel.UserID = this.userId;
            userAdminBusinessModel.userMenuBusinessModel = roleMenuMappingList;

            // Save Data in IDXRoleMenu
            ControllerHelper.ActionName = Constants.ActionNameSaveRoleMenuMapping;
            var responseData = ControllerHelper.PostMethodServiceRequestObject(userAdminBusinessModel);
            return Convert.ToInt32(responseData, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get Role To User
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="sapCustomerId">The sap customer identifier.</param>
        /// <returns>
        /// return list
        /// </returns>
        public List<UserMenuDetailsBusinessModel> GetRoleToUser(string userName, string roleId, string sapCustomerId)
        {
            List<UserMenuDetailsBusinessModel> roleToUserMapping = new List<UserMenuDetailsBusinessModel>();
            ControllerHelper.ActionName = Constants.ActionNameGetAllUserCustomerMapping;
            ControllerHelper.ActionParam = new[] { userName, roleId, sapCustomerId };
            roleToUserMapping = ControllerHelper.GetMethodServiceRequestForList<List<UserMenuDetailsBusinessModel>>();
            if (roleToUserMapping == null)
            {
                roleToUserMapping = new List<UserMenuDetailsBusinessModel>();
            }

            return roleToUserMapping;
        }

        /// <summary>
        /// Save Role to User
        /// </summary>
        /// <param name="userRoleMappingList">The user role mapping list.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        public int SaveRoleToUser(List<UserMenuDetailsBusinessModel> userRoleMappingList, string roleId)
        {
            UserAdminBusinessModel userAdminBusinessModel = new UserAdminBusinessModel();
            userAdminBusinessModel.RoleId = roleId;
            userAdminBusinessModel.UserID = this.userId;
            userAdminBusinessModel.userMenuBusinessModel = userRoleMappingList;

            // Save Data in IDXRoleMenu
            ControllerHelper.ActionName = Constants.ActionNameSaveUserRoleMapping;
            var saveRoleToUserResponse = ControllerHelper.PostMethodServiceRequestObject(userAdminBusinessModel);
            return Convert.ToInt32(saveRoleToUserResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Save New Role
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="isSCA">if set to <c>true</c> [is sca].</param>
        /// <returns>
        /// return true or false
        /// </returns>
        public int SaveNewRole(string roleName, bool isSca)
        {
            UsersBusinessModel usersBusinessModel = new UsersBusinessModel();
            usersBusinessModel.RoleName = roleName;
            usersBusinessModel.UserType = isSca ? SCAUSER : EXTERNALUSER;
            usersBusinessModel.UserId = Guid.Parse(this.userId);

            // Save Data in IDXRoleMenu
            ControllerHelper.ActionName = Constants.ActionNameSaveNewRole;
            var saveNewRoleResponse = ControllerHelper.PostMethodServiceRequestObject(usersBusinessModel);
            return Convert.ToInt32(saveNewRoleResponse, CultureInfo.InvariantCulture);
        }

        #endregion

        #region User To Customer Mapping

        /// <summary>
        /// Get User Customer Mapping
        /// </summary>
        /// <returns>
        /// return view model
        /// </returns>
        public UserAdminViewModel GetUserCustomerMapping()
        {
            UserAdminViewModel mapUserCustomerViewModel = new UserAdminViewModel();

            var userList = this.GetAllUsers(Constants.Zero, Convert.ToString(false, CultureInfo.InvariantCulture));
            mapUserCustomerViewModel.UserToCustomerItems = new SelectList(userList, Constants.UserId, Constants.UserName, null);
            return mapUserCustomerViewModel;
        }

        /// <summary>
        /// Get User To All Customer
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFirstTime">if set to <c>true</c> [is first time].</param>
        /// <returns>
        /// return view model
        /// </returns>
        public UserAdminViewModel GetUserToAllCustomer(string userId, bool isFirstTime)
        {
            if (isFirstTime)
            {
                // Retrive admin user to get all customers for the first time
                var objUsers = new UsersBusinessModel { UserName = Constants.AdminUser };
                ControllerHelper.ActionName = Constants.ActionNameGetAdminUserId;
                var responseUser = ControllerHelper.PostMethodServiceRequest(objUsers);
                userId = Convert.ToString(responseUser.UserId, CultureInfo.InvariantCulture);
            }

            UserAdminViewModel objUserAdminViewModel = new UserAdminViewModel();
            ControllerHelper.ActionName = Constants.ActionNameGetMappedUserToCustomer;
            ControllerHelper.ActionParam = new[] { userId };
            objUserAdminViewModel.UsersBusinessModel = ControllerHelper.GetMethodServiceRequestForList<List<UsersBusinessModel>>();
            objUserAdminViewModel.IsFirstTime = isFirstTime;
            if (objUserAdminViewModel.UsersBusinessModel == null)
            {
                objUserAdminViewModel.CustomerCount = 0;
            }
            else
            {
                objUserAdminViewModel.CustomerCount = objUserAdminViewModel.UsersBusinessModel.Count;
            }

            LogHelper.LogAction(Constants.ActionNameGetMappedUserToCustomer, Constants.UserAdminController, Constants.ActionNameGetMappedUserToCustomer, userId);

            return objUserAdminViewModel;
        }

        /// <summary>
        /// Save User to Customer mappings
        /// </summary>
        /// <param name="objCustomerList">The object customer list.</param>
        /// <param name="objUserId">The object user identifier.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        public int SaveUserToCustomer(List<UsersBusinessModel> objCustomerList, string objUserId)
        {
            UserAdminBusinessModel userAdminBusinessModel = new UserAdminBusinessModel();
            userAdminBusinessModel.UserID = objUserId;
            userAdminBusinessModel.UsersBusinessModel = objCustomerList;

            // Save Data in IDXUserCustomer
            ControllerHelper.ActionName = Constants.ActionNameSaveUserCustomerMapping;
            var saveUserToCustomerResponse = ControllerHelper.PostMethodServiceRequestObject(userAdminBusinessModel);
            return Convert.ToInt32(saveUserToCustomerResponse, CultureInfo.InvariantCulture);
        }

        #endregion

        #region Holiday Process Admin
        /// <summary>
        /// Get Customer Details
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// return view model
        /// </returns>
        public HolidayProcessViewModel GetCustomerDetails(string customerId)
        {
            HolidayProcessViewModel objHolidayProcessViewModel = new HolidayProcessViewModel();
            ControllerHelper.ActionName = Constants.ActionGetCustomerDetails;
            ControllerHelper.ActionParam = new[] { customerId };
            objHolidayProcessViewModel.HolidayProcessAdminDetails = ControllerHelper.GetMethodServiceRequestForList<List<HolidayProcessAdminDetails>>();
            if (objHolidayProcessViewModel.HolidayProcessAdminDetails == null)
            {
                objHolidayProcessViewModel.CustomerCount = 0;
            }
            else
            {
                objHolidayProcessViewModel.CustomerCount = objHolidayProcessViewModel.HolidayProcessAdminDetails.Count;
                foreach (var obj in objHolidayProcessViewModel.HolidayProcessAdminDetails)
                {
                    obj.NDD = Convert.ToDateTime(obj.NDD, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                    obj.ArrangedNDD = Convert.ToDateTime(obj.ArrangedNDD, CultureInfo.InvariantCulture).ToString(Constants.DateFormat, CultureInfo.InvariantCulture);
                    obj.StartTime = Convert.ToDateTime(obj.StartTime, CultureInfo.InvariantCulture).ToString(Constants.TimeFormat3, CultureInfo.InvariantCulture);
                    obj.EndTime = Convert.ToDateTime(obj.EndTime, CultureInfo.InvariantCulture).ToString(Constants.TimeFormat3, CultureInfo.InvariantCulture);
                }
            }

            LogHelper.LogAction(Constants.ActionGetCustomerDetails, Constants.UserAdminController, Constants.ActionGetCustomerDetails, this.userId);

            return objHolidayProcessViewModel;
        }

        /// <summary>
        /// Save Holiday Process
        /// </summary>
        /// <param name="objCustomerList">The object customer list.</param>
        /// <param name="objCustomerId">The object customer identifier.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        public int SaveHolidayProcess(List<HolidayProcessAdminDetails> objCustomerList, string objCustomerId)
        {
            HolidayProcessAdminBusinessModels holidayProcessAdminBusinessModels = new HolidayProcessAdminBusinessModels();
            holidayProcessAdminBusinessModels.CustomerId = Convert.ToInt64(objCustomerId, CultureInfo.InvariantCulture);
            holidayProcessAdminBusinessModels.HolidayProcessAdminDetails = objCustomerList;
            holidayProcessAdminBusinessModels.UserID = this.userId;

            // Save Data in HolidayProcesAdmin
            ControllerHelper.ActionName = Constants.ActionNameSaveHolidayProcessData;
            var saveHolidayProcessResponse = ControllerHelper.PostMethodServiceRequestObject(holidayProcessAdminBusinessModels);
            LogHelper.LogAction(Constants.ActionNameSaveHolidayProcessData, Constants.UserAdminController, Constants.ActionNameSaveHolidayProcessData, this.userId);
            return Convert.ToInt32(saveHolidayProcessResponse, CultureInfo.InvariantCulture);
        }
        #endregion

        #region Carehome User

        /// <summary>
        /// Save Details for Carehome Users
        /// </summary>
        /// <param name="objUserCarehomeBusinessModel"></param>
        /// <returns></returns>
        public int SaveCareHomeUsers(UserCareHomeBusinessModel objUserCareHomeBusinessModel)
        {
            if (objUserCareHomeBusinessModel == null)
                return 0;

            objUserCareHomeBusinessModel.UserId = this.userId;

            ControllerHelper.ActionName = Constants.ActionNameSaveUserCareHomeMapping;
            var saveUserToCustomerResponse = ControllerHelper.PostMethodServiceRequestObject(objUserCareHomeBusinessModel);
            return Convert.ToInt32(saveUserToCustomerResponse, CultureInfo.InvariantCulture);
        }

        #endregion
    }
}