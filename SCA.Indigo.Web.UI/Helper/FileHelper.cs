﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : Gaurav Lad
// Last Modified On : 19-05-2015
// ***********************************************************************
// <copyright file="AttachmentHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.ViewModel;
    using System.Globalization;
    using System.Web;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using LinqToExcel;
    using System.Text;
    using SCA.Indigo.Web.UI.ViewModel.MassChanges;

    /// <summary>
    /// Attachment Helper class
    /// </summary>
    public class FileHelper
    {
        #region private Variable
        /// <summary>
        /// The attachment log server directory
        /// </summary>
        private string MassChangesUploadDirectory = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress, GenericHelper.UploadDirectory, GenericHelper.MassChangesUploads);

        private string patientTemplateFilePath = "Patient";
        private string carehomeTemplateFilePath = "Carehome";
        private string orderTemplateFilePath = "OrderUpdateTemplate";
        private string prescriptionUpdateTemplateFilePath = "PrescriptionUpdateTemplate";
		private string prescriptionImportTemplateFilePath = "PrescriptionImportTemplate";
        private string contactPersonTemplateFilePath = "ContactPerson";
        private string communicationformatTemplateFilePath = "Communicationformat";        
        private string returnLogFilePath = string.Empty;        
        private static string massChangesLogs = ConfigurationManager.AppSettings["MassChangesLogs"];
        private static string uploadDirectory = ConfigurationManager.AppSettings["SharedLocation"];
        /// <summary>
        /// Maximum data limit for Mass Update
        /// </summary>
        private static string massUploadMaxLimit = ConfigurationManager.AppSettings["MassUploadMaxLimit"];

        /// <summary>
        /// Maximum data limit for Patient Mass Update
        /// </summary>
        private static string patientUploadMaxLimit = ConfigurationManager.AppSettings["PatientUploadMaxLimit"];

        #region Constants

        string newLine = "<br/>";
        string tableStart = "<table style='border: 1px solid black'>";
        string tableEnd = "</table>";
        string trStart = "</tr>";
        string trEnd = "</tr>";
        string tdStart = "<td style='border: 1px solid black'>";
        string tdEnd = "</td>";
        string templateColumnName = "<b>Template Column Name</b>";
        string uploadColumnName = "<b>Upload Column Name</b>";
        string InvalidColumnName = "Invalid Column Name";
        
        #endregion

        /// <summary>
        /// Uploaded filePath
        /// </summary>
        private string filePath = string.Empty;
        public string FilePath { get { return filePath; } set { filePath = value; } }

        /// <summary>
        /// Error Message during file upload
        /// </summary>
        private string errorMessage = string.Empty;
        public string ErrorMessage { get { return errorMessage; } set { errorMessage = value; } }

        /// <summary>
        /// Boolean Indication whether file is uploaded
        /// </summary>
        private bool isUplaodFailed = false;
        public bool IsUplaodFailed { get { return isUplaodFailed; } set { isUplaodFailed = value; } }

        #endregion

        #region FileHelper


        /// <summary>
       /// Saves the uploaded file for further processing
       /// </summary>
       /// <param name="UploadedFile">File that is to be saved for processing</param>
       /// <param name="strFileName">File Name</param>
       /// <returns>Object of FileHelper class</returns>
        public FileHelper UploadFile(HttpPostedFileBase UploadedFile, string strFileName)
        {
            FileHelper objFileHelper = new FileHelper();
            try
            {
                string supportedFileFormat = ConfigurationManager.AppSettings["UploadFileFormat"].ToString(CultureInfo.InvariantCulture);
                if (UploadedFile != null)
                {

                    List<string> supportedTypes = supportedFileFormat.Split(',').ToList();
                    var fileExt = "." + System.IO.Path.GetExtension(UploadedFile.FileName).Substring(1);
                    if (!supportedTypes.Contains(fileExt))
                    {
                        objFileHelper.ErrorMessage = Resource.Common.resFileFormatNotSupported;
                        objFileHelper.IsUplaodFailed = true;
                        return objFileHelper;
                    }
                    CommonHelper.CreateDirectoryIfNotExists(MassChangesUploadDirectory);
                    string fileName = string.Concat(strFileName+"_", Guid.NewGuid(), fileExt);
                    objFileHelper.FilePath = Path.Combine(MassChangesUploadDirectory, fileName);                    
                    UploadedFile.SaveAs(objFileHelper.FilePath);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objFileHelper;
        }

        /// <summary>
        /// Removes the attachment.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>
        /// return status of remove attachment
        /// </returns>
        public bool RemoveFile(string filepath)
        {
            bool issuccess = false;
            try
            {
                if (System.IO.File.Exists(FilePath))
                {
                    System.IO.File.Delete(FilePath);
                    issuccess = true;
                }
            }
            catch
            {
                issuccess = false;
            }
            return issuccess;
        }

        /// <summary>
        /// Gets the attachment location.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>
        /// fetch the attachment based on attachment identifier
        /// </returns>
        public List<string> GetAttachmentLocation(List<string> attachmentListId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetAttachmentPath;
            List<string> attachmentLoaction = ControllerHelper.PostMethodServiceRequest<List<string>>(attachmentListId);
            return attachmentLoaction;
        }

        /// <summary>
        /// Downs the load attachment.
        /// </summary>
        /// <param name="attachmentIdList">The attachment identifier list.</param>
        /// <returns>
        /// gets the path of the file
        /// </returns>
        public List<string> DownloadAttachment(List<string> attachmentIdList)
        {
            List<string> downLoadFilePath = new List<string>();
            //ControllerHelper.ActionName = Constants.ActionNameGetAttachmentPath;
            //var response = ControllerHelper.PostMethodServiceRequest<List<string>>(attachmentIdList);

            //// Creates the Attachment folder in Virtual Directory
            //Common.CommonHelper.CreateUploadDirectoryIfNotExists(tempPath, uploadDirectory);
            //var attachmentLogDirectory = Path.Combine(serverPath, MassChangesServerDirectory);
            //if (!Directory.Exists(attachmentLogDirectory))
            //{
            //    Directory.CreateDirectory(attachmentLogDirectory);
            //}

            //foreach (var item in response)
            //{
            //    string filePath = Path.Combine(serverPath, MassChangesServerDirectory + '\\' + item);
            //    string virtualTempPath = Path.Combine(tempPath, uploadDirectory + '\\' + item);
            //    string downLoadPath = serverUrl + '\\' + uploadDirectory + '\\' + item;

            //    if (!File.Exists(virtualTempPath))
            //    {
            //        System.IO.File.Copy(filePath, virtualTempPath, true);
            //    }

            //    downLoadFilePath.Add(downLoadPath);
            //}

            return downLoadFilePath;
        }

		/// <summary>
        /// Get the TemplateFile's name that is stored in the project
        /// </summary>
        /// <param name="TemplateId">Template Id of the file</param>
		/// <param name="isImport">true if isImport else false</param>
        /// <returns>string Template File Name</returns>
        public string GetTemplateFileName(int TemplateId, bool isImport)
        {
            switch (TemplateId)
            {
                case (int)SCAEnums.MassDataTemplate.Patient:
                    return patientTemplateFilePath;
                case (int)SCAEnums.MassDataTemplate.Carehome:
                    return carehomeTemplateFilePath;
                case (int)SCAEnums.MassDataTemplate.Prescription:
					if (isImport)
					{
						return prescriptionImportTemplateFilePath;
					}
					else
					{
						return prescriptionUpdateTemplateFilePath;
					}
                case (int)SCAEnums.MassDataTemplate.Order:
                    return orderTemplateFilePath;
                case (int)SCAEnums.MassDataTemplate.ContactPerson:
                    return contactPersonTemplateFilePath;
                case (int)SCAEnums.MassDataTemplate.CommunicationFormat:
                    return communicationformatTemplateFilePath;
                case (int)SCAEnums.MassDataTemplate.CustomerClinicalContacts:
                    return SCAEnums.MassDataTemplate.CustomerClinicalContacts.ToString(CultureInfo.InvariantCulture);
                case (int)SCAEnums.MassDataTemplate.CustomerAnalysisInformation:
                    return SCAEnums.MassDataTemplate.CustomerAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                case (int)SCAEnums.MassDataTemplate.PatientClinicalContacts:
                    return SCAEnums.MassDataTemplate.PatientClinicalContacts.ToString(CultureInfo.InvariantCulture);
                case (int)SCAEnums.MassDataTemplate.PatientAnalysisInformation:
                    return SCAEnums.MassDataTemplate.PatientAnalysisInformation.ToString(CultureInfo.InvariantCulture);
                case (int) SCAEnums.MassDataTemplate.PostCodeMatrix:
                    return SCAEnums.MassDataTemplate.PostCodeMatrix.ToString(CultureInfo.InvariantCulture);
                default:
                    break;
            }
            return string.Empty;
        }

        #region Excel Data Validation

		/// <summary>
		/// Validates the Excel Template
		/// </summary>
		/// <param name="templateFilePath">Original Template File Path</param>
		/// <param name="massDataFilePath">Uploaded Template</param>
		/// <param name="excelFactory">ExcelFactory</param>
		/// <returns>Result</returns>
        public List<TemplateValidationViewModel> ValidateExcelTemplate(string templateFilePath, string massDataFilePath, out ExcelQueryFactory excelFactory)
        {
            StringBuilder errorMessage = new StringBuilder();
            excelFactory = null;
            ExcelQueryFactory templateExcel = null;
            ExcelQueryFactory massUploadExcel = null;
            var validationMessageList = new List<TemplateValidationViewModel>();

            try
            {
                // Read the Excel File
                 templateExcel = new ExcelQueryFactory(templateFilePath);
                 massUploadExcel = new ExcelQueryFactory(massDataFilePath);

                validationMessageList = ValidateExcelHeaders(ref templateExcel, ref massUploadExcel);
                if (string.IsNullOrEmpty(errorMessage.ToString()))
                {
                    excelFactory = massUploadExcel;
                }
            }
            catch (Exception ex)
            {
                errorMessage.Append(ex.Message);
            }
            finally
            {
                if (templateExcel != null)
                {
                    templateExcel.Dispose();
                }
                if (massUploadExcel != null)
                {
                    massUploadExcel.Dispose();
                }
            }

            return validationMessageList;
        }

        /// <summary>
        /// Validate the excel header
        /// </summary>
		/// <param name="templateExcel">Original Template File</param>
		/// <param name="massUploadExcel">Uploaded File</param>
        /// <returns>Result</returns>
		private List<TemplateValidationViewModel> ValidateExcelHeaders(ref ExcelQueryFactory templateExcel, ref ExcelQueryFactory massUploadExcel)
		{
			StringBuilder errorMessage = new StringBuilder();
			TemplateValidationViewModel validationRow = null;
			List<TemplateValidationViewModel> validationrowList = new List<TemplateValidationViewModel>();

			var templateHeaderData = from x in templateExcel.WorksheetNoHeader(0)
									 select x;
			var firstTemplateRowData = templateHeaderData.FirstOrDefault().Where(q => q.Value != DBNull.Value).ToList();

			var massUploadHeaderData = from x in massUploadExcel.WorksheetNoHeader(0)
									   select x;

			var blankRowList = GetNonblankRows(massUploadExcel);
			if (!(blankRowList > 0))
			{
				validationRow = new TemplateValidationViewModel();
				validationRow.message = Resource.Common.resBlankExcelMessage;
				validationrowList.Add(validationRow);
				return validationrowList;
			}

			var firstMassRowData = massUploadHeaderData.FirstOrDefault().Where(q => q.Value != DBNull.Value).ToList();
			if (firstTemplateRowData.Count != firstMassRowData.Count)
			{
				validationRow = new TemplateValidationViewModel();
				validationRow.message = Resource.Common.resDataImportInvalidHeaderCount;
				validationrowList.Add(validationRow);
				return validationrowList;
			}
			else
			{
				for (int i = 0; i < firstMassRowData.Count; i++)
				{
					if (firstMassRowData[i].ToString().Trim() != firstTemplateRowData[i].ToString().Trim())
					{
						validationRow = new TemplateValidationViewModel();
						validationRow.TemplateColumnName = string.IsNullOrEmpty(firstTemplateRowData[i]) ? string.Empty : firstTemplateRowData[i].ToString();
						validationRow.UploadColumnName = string.IsNullOrEmpty(firstMassRowData[i]) ? string.Empty : firstMassRowData[i].ToString();
						validationrowList.Add(validationRow);
					}
				}
			}
			return validationrowList;
		}

        /// <summary>
        /// GenNonBlank Rows
        /// </summary>
        /// <param name="queryFactory">ExcelFactory with file attached</param>
        /// <returns>No of Non Blank Rows</returns>
		private long GetNonblankRows(ExcelQueryFactory queryFactory)
        {
            List<Object> onlyNonBlankRows = queryFactory.Worksheet<Object>(0).ToList()
                                    .Select((typedRow, index) => new { typedRow, index })                
                                    .Join(
                                        queryFactory.Worksheet(0).ToList()
                                                    .Select(
                                                        (untypedRow, indexForUntypedRow) =>
                                                        new { untypedRow, indexForUntypedRow }),                
                                        arg => arg.index, arg => arg.indexForUntypedRow,
                                        (a, b) => new { a.index, a.typedRow, b.untypedRow })                
                                    .Where(x => x.untypedRow.Any(cell => cell.Value != DBNull.Value))
                                    .Select(joined => joined.typedRow).ToList();
            return onlyNonBlankRows.Count;
        }

        #endregion

        /// <summary>
        /// Copies to virtual location.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>return the file Path of log file</returns>
        public string CopyToVirtualLocation(string path, string userId)
        {
            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    string downloadUrl = GenericHelper.DownloadUrl;
                    string filePath = path;
                    string serverPath = System.Web.Hosting.HostingEnvironment.MapPath("\\" + uploadDirectory + "\\" + massChangesLogs);
                    Common.CommonHelper.CreateDirectoryIfNotExists(serverPath);
                    var fileName = Path.GetFileName(path);
                    serverPath = serverPath + "\\" + fileName;
                    System.IO.File.Copy(filePath, serverPath, true);
                    returnLogFilePath = downloadUrl + "\\" + uploadDirectory + "\\" + massChangesLogs + "\\" + fileName;
                }               
            }
            catch (Exception e) 
            {
                LogHelper.LogException(e, userId);
            }
            
            return returnLogFilePath;
        }

        #endregion
    }
}