﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Siddharth
// Created          : 30-06-2015
// ***********************************************************************
// <copyright file="OrderHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// Class OrderHelper
    /// </summary>
    public class OrderHelper
    {
        /// <summary>
        /// Get Care Home Orders
        /// </summary>
        /// <param name="patientOrdersBusinessModel"></param>
        /// <param name="totalRecords"></param>
        /// <param name="totalPages"></param>
        /// <returns>list of values</returns>
        public List<PatientOrdersBusinessModel> GetCareHomeOrders(PatientOrdersBusinessModel patientOrdersBusinessModel, out int totalRecords, out int totalPages)
        {
            ControllerHelper.ActionName = Constants.ActionGetCareHomeOrders;
            ControllerHelper.ActionParam = new[] { Convert.ToString(patientOrdersBusinessModel.CareHomeID, CultureInfo.InvariantCulture), Convert.ToString(patientOrdersBusinessModel.IsProductLevel, CultureInfo.InvariantCulture), Convert.ToString(patientOrdersBusinessModel.UserId, CultureInfo.InvariantCulture) };
            bool isProductLevelOrder = patientOrdersBusinessModel.IsProductLevel;
            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<PatientOrdersBusinessModel>>();
            if (responseData == null)
            {
                responseData = new List<PatientOrdersBusinessModel>();
            }

            List<PatientOrdersBusinessModel> gridData = new List<PatientOrdersBusinessModel>();
            if (responseData != null)
            {
                gridData = responseData.Skip((patientOrdersBusinessModel.Page - 1) * patientOrdersBusinessModel.RowCount).Take(patientOrdersBusinessModel.RowCount).ToList();
                gridData = SortResidentialGridData(gridData, patientOrdersBusinessModel.SortColumn.ToLower(CultureInfo.InvariantCulture), patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture));
            }
            totalRecords = responseData != null ? responseData.Count : 0;
            totalPages = (int)Math.Ceiling((double)responseData.Count / (double)patientOrdersBusinessModel.RowCount);
            
              if (!isProductLevelOrder && string.IsNullOrEmpty(patientOrdersBusinessModel.SortColumn.Trim()))
                gridData = gridData.OrderBy(x => x.PatientLastName).ToList();
            return gridData;
        }

        /// <summary>
        /// Get Activated Orders
        /// </summary>
        /// <param name="patientOrdersBusinessModel"></param>
        /// <param name="totalRecords"></param>
        /// <param name="totalPages"></param>
        /// <returns>list of values</returns>
        public List<PatientOrdersBusinessModel> GetActivatedOrders(PatientOrdersBusinessModel patientOrdersBusinessModel, out int totalRecords, out int totalPages)
        {
            ControllerHelper.ActionName = Constants.ActionGetActivatedOrders;
            ControllerHelper.ActionParam = new[] { Convert.ToString(patientOrdersBusinessModel.PatientID, CultureInfo.InvariantCulture), Convert.ToString(patientOrdersBusinessModel.CareHomeID, CultureInfo.InvariantCulture), Convert.ToString(patientOrdersBusinessModel.UserId, CultureInfo.InvariantCulture), "FALSE", Convert.ToString(patientOrdersBusinessModel.IsProductLevel, CultureInfo.InvariantCulture), "FALSE" };
            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<PatientOrdersBusinessModel>>();
            if (responseData == null)
            {
                responseData = new List<PatientOrdersBusinessModel>();
            }
            List<PatientOrdersBusinessModel> gridData = new List<PatientOrdersBusinessModel>();
            if (responseData != null)
            {
                gridData = responseData.Skip((patientOrdersBusinessModel.Page - 1) * patientOrdersBusinessModel.RowCount).Take(patientOrdersBusinessModel.RowCount).ToList();
                gridData = SortResidentialGridData(gridData, patientOrdersBusinessModel.SortColumn.ToLower(CultureInfo.InvariantCulture), patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture));
            }
            totalRecords = responseData != null ? responseData.Count : 0;
            totalPages = (int)Math.Ceiling((double)responseData.Count / (double)patientOrdersBusinessModel.RowCount);
            return gridData;
        }

        /// <summary>
        /// Sort Residential Grid Data
        /// </summary>
        /// <param name="gridData"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortOrder"></param>
        /// <returns>list of values</returns>
        private List<PatientOrdersBusinessModel> SortResidentialGridData(List<PatientOrdersBusinessModel> gridData, string sortIndex, string sortOrder)
        {
            switch (sortIndex)
            {
                #region Carehome/Hospital Orders

                case "patientname":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.PatientName).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.PatientName).ToList();
                    break;
                case "patienttype":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.PatientType).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.PatientType).ToList();
                    break;
                case "productdisplayid":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.ProductDisplayId).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.ProductDisplayId).ToList();
                    break;
                case "productname":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.ProductName).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.ProductName).ToList();
                    break;


                #endregion

                #region ProductLevelOrder

                case "productdescription":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.ProductDescription).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.ProductDescription).ToList();
                    break;
                case "quantity":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.Quantity).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.Quantity).ToList();
                    break;
                case "deliverydate":
                    if (sortOrder == "asc")
                    {
                        gridData = gridData.OrderBy(x => x.DeliveryDate).ToList();
                    }
                    else
                        gridData = gridData.OrderByDescending(x => x.DeliveryDate).ToList();
                    break;

                #endregion

                default:
                    gridData = gridData.OrderBy(x => x.ProductDisplayId).ToList();
                    break;
            }
            return gridData;
        }

        /// <summary>
        /// Get NDD After Weekends Or Holiday
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public DateTime GetNDDAfterWeekendsOrHoliday(DateTime rushNDD, int countryId)
        {
            rushNDD = GetNextWorkingDay(rushNDD, countryId);
            while (GenericHelper.IsHoliday(rushNDD, countryId))
            {
                rushNDD = rushNDD.AddDays(1);
                rushNDD = GetNextWorkingDay(rushNDD, countryId);
            }
            return rushNDD;
        }

        // <summary>
        /// Return the next working week day (Exclude Saturday, Sunday)
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// Next Working day after the given input date.
        /// </returns>
        public DateTime GetNextWorkingDay(DateTime date, int countryId)
        {
            while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || GenericHelper.IsHoliday(date, countryId))
            {
                date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// This function will convert one timezone to another
        /// </summary>
        /// <param name="cutOffTime">time</param>
        /// <returns></returns>
        public string ConvertTimeToUkTimezone(string cutOffTime)
        {
            string convertedTime = string.Empty;
            if (!string.IsNullOrEmpty(cutOffTime))
            {
                DateTime serverDate = Convert.ToDateTime(cutOffTime);
                var sourceTimeZone = TimeZoneInfo.FindSystemTimeZoneById(Constants.CetTimeZone);
                var destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(Constants.BritishTimeZone);
                var newDate = TimeZoneInfo.ConvertTime(serverDate, sourceTimeZone, destinationTimeZone);
                convertedTime = newDate != null ? newDate.ToString(Constants.TimeFormat, CultureInfo.InvariantCulture) : string.Empty;
            }
            return convertedTime;
        }
    }
}