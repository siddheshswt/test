﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : Gaurav Lad
// Last Modified On : 19-05-2015
// ***********************************************************************
// <copyright file="AttachmentHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.ViewModel;
    using System.Globalization;
    using SCA.Indigo.Common;

    /// <summary>
    /// Attachment Helper class
    /// </summary>
    public class AttachmentHelper
    {
        #region private Variable                
        /// <summary>
        /// The server path
        /// </summary>
        private string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);
        /// <summary>
        /// The temporary path
        /// </summary>
        private string tempPath = GenericHelper.TempAddress;
        /// <summary>
        /// The attachment log server directory
        /// </summary>
        private string attachmentLogServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.AttachmentSharedLocation);
        /// <summary>
        /// The upload directory
        /// </summary>
        private string uploadDirectory = GenericHelper.UploadDirectory;
        /// <summary>
        /// The download url
        /// </summary>
        private string downloadUrl = GenericHelper.DownloadUrl;
        #endregion

        #region Attachment
        /// <summary>
        /// Displays the attachment.
        /// </summary>
        /// <param name="attachmentViewModel">The attachment view model.</param>
        /// <returns>
        /// Return Attachment Business view model
        /// </returns>
        public List<AttachmentBusinessViewModel> DisplayAttachment(AttachmentViewModel attachmentViewModel)
        {
            List<AttachmentBusinessViewModel> attachmentBusinessViewModel = new List<AttachmentBusinessViewModel>();
            if (attachmentViewModel != null)
            {
                ControllerHelper.ActionName = Constants.ActionNameDisplayAttachment;
                ControllerHelper.ActionParam = new[] { attachmentViewModel.TypeId, attachmentViewModel.AttachmentType, ControllerHelper.CurrentUser.UserId.ToString() };
                attachmentBusinessViewModel = ControllerHelper.GetMethodServiceRequestForList<List<AttachmentBusinessViewModel>>();
            }

            if (attachmentBusinessViewModel == null)
            {
                attachmentBusinessViewModel = new List<AttachmentBusinessViewModel>();
            }

            return attachmentBusinessViewModel;
        }

        /// <summary>
        /// Uploads the attachment.
        /// </summary>
        /// <param name="attachmentViewModel">The attachment view model.</param>
        /// <returns>
        /// return status of upload
        /// </returns>
        public string UploadAttachment(AttachmentViewModel attachmentViewModel)
        {
            string response = "false";
            try
            {
                string supportedFileFormat = ConfigurationManager.AppSettings["SupportedFileFormat"].ToString(CultureInfo.InvariantCulture);                
                List<AttachmentBusinessViewModel> requestAttachmentFiles = new List<AttachmentBusinessViewModel>();
                AttachmentBusinessViewModel newAttachmentBusinessViewModel;
                Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);
                var attachmentLogDirectory = Path.Combine(serverPath, attachmentLogServerDirectory);
                if (!Directory.Exists(attachmentLogDirectory))
                {
                    Directory.CreateDirectory(attachmentLogDirectory);
                }

                if (attachmentViewModel != null)
                {
                    foreach (var file in attachmentViewModel.AttachmentFiles)
                    {
                        newAttachmentBusinessViewModel = new AttachmentBusinessViewModel();
                        if (file != null)
                        {
                            double max_size = double.Parse(ConfigurationManager.AppSettings["MaxAttachSizeInMB"].ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
                            double max_size_byte = max_size * 1024 * 1024;
                            if (file.ContentLength > max_size_byte || file.ContentLength <= 0)
                            {
                                response = string.Format(CultureInfo.InvariantCulture,Resource.Common.resFileSize, max_size);
                                return response;
                            }

                            List<string> supportedTypes = supportedFileFormat.Split(',').ToList();
                            var fileExt = "." + System.IO.Path.GetExtension(file.FileName).Substring(1);
                            if (!supportedTypes.Contains(fileExt))
                            {
                                response = Resource.Common.resFileFormatNotSupported;
                                return response;
                            }

                            string fileName = string.Concat(Constants.AttachmentController, Guid.NewGuid(), fileExt);
                            attachmentViewModel.Path = Path.Combine(serverPath, attachmentLogServerDirectory, fileName);
                            newAttachmentBusinessViewModel.TypeId = attachmentViewModel.TypeId;
                            newAttachmentBusinessViewModel.AttachmentLocation = fileName;
                            newAttachmentBusinessViewModel.FileName = file.FileName;
                            newAttachmentBusinessViewModel.AttachmentType = attachmentViewModel.AttachmentType;
                            newAttachmentBusinessViewModel.CreatedBy = ControllerHelper.CurrentUser.UserId.ToString();
                            requestAttachmentFiles.Add(newAttachmentBusinessViewModel);
                            file.SaveAs(attachmentViewModel.Path);
                        }
                    }

                    ControllerHelper.ActionName = Constants.ActionNameUploadAttachment;
                    response = (string)ControllerHelper.PostMethodServiceRequestObject(requestAttachmentFiles);
                    if (!Convert.ToBoolean(response, CultureInfo.InvariantCulture))
                    {
                        return response.ToString(CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return response.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Removes the attachment.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>
        /// return status of remove attachment
        /// </returns>
        public bool RemoveAttachment(List<string> attachmentListId)
        {
            List<string> attachmentLoaction = GetAttachmentLocation(attachmentListId);
            ControllerHelper.ActionName = Constants.ActionNameRemoveAttachment;
            var isSuccess = (bool)ControllerHelper.PostMethodServiceRequestObject(attachmentListId);
            if (Convert.ToBoolean(isSuccess)) 
            {
                foreach (var file in attachmentLoaction)
                {
                    string filePath = Path.Combine(serverPath, attachmentLogServerDirectory, file);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
            }

            return isSuccess;
        }

        /// <summary>
        /// Gets the attachment location.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns>
        /// fetch the attachment based on attachment identifier
        /// </returns>
        public List<string> GetAttachmentLocation(List<string> attachmentListId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetAttachmentPath;
            List<string> attachmentLoaction = ControllerHelper.PostMethodServiceRequest<List<string>>(attachmentListId);
            return attachmentLoaction;
        }

        /// <summary>
        /// Downs the load attachment.
        /// </summary>
        /// <param name="attachmentIdList">The attachment identifier list.</param>
        /// <returns>
        /// gets the path of the file
        /// </returns>
        public List<string> DownloadAttachment(List<string> attachmentIdList)
        {
           List<string> downLoadFilePath = new List<string>();
            ControllerHelper.ActionName = Constants.ActionNameGetAttachmentPath;
            var response = ControllerHelper.PostMethodServiceRequest<List<string>>(attachmentIdList);  
          
            /// create directory on the server
            Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);

            /// create directory on the virtual directory.
            Common.CommonHelper.CreateDirectoryIfNotExists(tempPath);

            var attachmentLogDirectory = Path.Combine(serverPath, uploadDirectory);
            Common.CommonHelper.CreateUploadDirectoryIfNotExists(attachmentLogDirectory, attachmentLogServerDirectory);
                      

            foreach (var item in response)
            {
                string filePath = Path.Combine(serverPath, attachmentLogServerDirectory + '\\' + item);
                string virtualTempPath = Path.Combine(tempPath, item);                
                string downLoadPath = downloadUrl + '\\' + uploadDirectory + '\\' + item;
                if (!File.Exists(virtualTempPath))
                {
                    try 
                    {
                        System.IO.File.Copy(filePath, virtualTempPath, true);    
                    }
                    catch (Exception e) 
                    {
                        LogHelper.LogException(e);
                        downLoadPath = string.Empty;
                    }                    
                }

                downLoadFilePath.Add(downLoadPath);
            }

            return downLoadFilePath;
        }

        #endregion
    }
}