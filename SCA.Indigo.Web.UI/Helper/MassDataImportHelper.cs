﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Avinash
// Created          : 12-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassDataImportHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using SCA.Indigo.Web.UI.ViewModel.MassChanges;
    using SCA.Indigo.Business.BusinessModels;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web.Mvc;
    using SCA.Indigo.Common;
    using LinqToExcel;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// Mass Data Import Helper
    /// </summary>
    public class MassDataImportHelper
    {
        #region Private Properties

        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Template File Path
        /// </summary>
        private string templateFilePath = string.Empty;
        #endregion

        #region Load Mass Import
        /// <summary>
        /// Get Import Template Dropdown
        /// </summary>
        /// <param name="importTemplateListId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public MasterMassDataImportViewModel GetImportTemplateDropDown(long[] importTemplateListId, long languageId)
        {
            MasterMassDataImportViewModel massDataImport = new MasterMassDataImportViewModel();
            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(importTemplateListId), Convert.ToString(languageId, CultureInfo.InvariantCulture) };

            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            massDataImport.ImportTemplate = new SelectList(responseData, Constants.ListId, Constants.DisplayText);
            return massDataImport;
        }

        #endregion

        #region Process Mass Import
        /// <summary>
        /// Import Data
        /// </summary>
        /// <param name="objMasterMassDataImportViewModel"></param>
        /// <returns>view model</returns>
        public MasterMassDataImportViewModel ImportData(MasterMassDataImportViewModel objMasterMassDataImportViewModel)
        {
            var objFileHelper = new FileHelper();
            bool status = false;
            if (objMasterMassDataImportViewModel.UploadedFile != null)
            {

                templateFilePath = objFileHelper.GetTemplateFileName(objMasterMassDataImportViewModel.SelectedTemplate, objMasterMassDataImportViewModel.IsImport);

                objFileHelper = objFileHelper.UploadFile(objMasterMassDataImportViewModel.UploadedFile, templateFilePath);
                if (!string.IsNullOrEmpty(objFileHelper.ErrorMessage))
                {
                    objMasterMassDataImportViewModel.Message = objFileHelper.ErrorMessage;
                }
                /// If File uploaded successfully then do the Column Level Validation
                if (!objFileHelper.IsUplaodFailed)
                {
                    /// Send to the Helper to Processing
                    ExcelQueryFactory excelFactory;

                    templateFilePath = Path.Combine("//" + GenericHelper.MassChangesTemplate, templateFilePath) + ".xls";

                    /// Validate Excel Template and Get the Excel Query Factory
                    templateFilePath = System.Web.Hosting.HostingEnvironment.MapPath(templateFilePath);
                    objMasterMassDataImportViewModel.TemplateValidationRows = objFileHelper.ValidateExcelTemplate(templateFilePath, objFileHelper.FilePath, out excelFactory);

                    /// Return the Message and Status
                    if (objMasterMassDataImportViewModel.TemplateValidationRows.Count==0)
                    {
                        status = true;

                        /// Call the Business Model for the File Name and Select Template Id
                        MassDataBusinessModel massChangesBusinessModel = new Business.BusinessModels.MassDataBusinessModel()
                        {
                            UserId = userId,
                            SelectedTemplate = objMasterMassDataImportViewModel.SelectedTemplate,
                            IsImport = objMasterMassDataImportViewModel.IsImport, /// if true then Mass Data Import else Mass Data changes....
                            FilePath = objFileHelper.FilePath,
                            UILogIPAddress= GenericHelper.IPAddress
                        };

                        ControllerHelper.ActionName = Constants.ActionProcessImportData;
                        string uploadResponseData = string.Empty;
                        if (objMasterMassDataImportViewModel.IsImport)
                        {
                            ControllerHelper.PostMethodServiceRequestAsync(massChangesBusinessModel, userId);
                        }
                        else
                        {
                            uploadResponseData = Convert.ToString(ControllerHelper.PostMethodServiceRequestObject(massChangesBusinessModel), CultureInfo.InvariantCulture);
                        }                        
                        objMasterMassDataImportViewModel.LogFilePath = uploadResponseData;                        
                        if (!objMasterMassDataImportViewModel.IsImport && !string.IsNullOrEmpty(objMasterMassDataImportViewModel.LogFilePath))
                        {
                            objMasterMassDataImportViewModel.Message = Resource.Common.resDataUploadedSuccess;

                            /// Move this code here as considered, required for mass changes only.
                            objMasterMassDataImportViewModel.LogFilePath = objFileHelper.CopyToVirtualLocation(objMasterMassDataImportViewModel.LogFilePath, userId);
                        }
                        else if (objMasterMassDataImportViewModel.IsImport)
                        {
                            objMasterMassDataImportViewModel.Message = Resource.Common.resDataUploadedSuccess;
                        }
                        else
                        {
                            objMasterMassDataImportViewModel.Message = Resource.Common.resGenericErrorForWrong;
                        }
                    }
                    else
                    {
                        objFileHelper.RemoveFile(objFileHelper.FilePath);
                    }
                }

                objMasterMassDataImportViewModel.Status = status;
            }
            return objMasterMassDataImportViewModel;
        }
        #endregion

        #region Download
        /// <summary>
        /// Get Template File Path
        /// </summary>
        /// <param name="selectedTemplateValue"></param>
        /// <param name="isImport"></param>
        /// <returns>file path</returns>
        public string GetTemplateFilePath(int selectedTemplateValue, bool isImport)
        {
            var objFileHelper = new FileHelper();
            string templateFileName = objFileHelper.GetTemplateFileName(selectedTemplateValue, isImport);
            string templateFilePath = string.Empty;
            templateFilePath = Path.Combine("\\" + GenericHelper.MassChangesTemplate, templateFileName) + ".xls";
            return templateFilePath;
        }

        #endregion
    }
}