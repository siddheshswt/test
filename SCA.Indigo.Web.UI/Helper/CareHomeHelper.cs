﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : syerva
// Created          : 12-03-2014
//
// Last Modified By : Arvind
// Last Modified On : 19-05-2015
// ***********************************************************************
// <copyright file="AttachmentHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Care Home Helper
    /// </summary>
    public class CareHomeHelper
    {
        #region Variables
        /// <summary>
        /// The user identifier
        /// </summary>
        private string userId = ControllerHelper.CurrentUser.UserId.ToString();
        /// <summary>
        /// The date format
        /// </summary>
        private const string DateFormat = "dd/MM/yyyy";
        /// <summary>
        /// The date time format
        /// </summary>
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm";
        #endregion

        #region ******CareHome*****

        /// <summary>
        /// Save Carehome Details
        /// </summary>
        /// <param name="objCareHomeViewModel">Object of CareHomeViewModel</param>
        /// <returns>
        /// returns carehome Id
        /// </returns>
        internal long SaveCareHome(CareHomeViewModel objCareHomeViewModel)
        {
            try
            {
                var objCareHomeBusinessModel = new CareHomeBusinessModel();
                objCareHomeBusinessModel.CareHomeId = objCareHomeViewModel.CareHomeId;
                objCareHomeBusinessModel.CareHomeName = objCareHomeViewModel.CareHomeName;
                objCareHomeBusinessModel.Address1 = objCareHomeViewModel.Address1;
                objCareHomeBusinessModel.Address2 = objCareHomeViewModel.Address2;
                objCareHomeBusinessModel.City = objCareHomeViewModel.TownOrCity;
                objCareHomeBusinessModel.Country = Constants.DefaultCountry;
                objCareHomeBusinessModel.County = objCareHomeViewModel.County;
                objCareHomeBusinessModel.CustomerId = objCareHomeViewModel.CustomerId;
                objCareHomeBusinessModel.DeliveryFrequency = objCareHomeViewModel.DeliveryFrequency;
                objCareHomeBusinessModel.EmailAddress = objCareHomeViewModel.EmailAddress;
                objCareHomeBusinessModel.CareHomeType = objCareHomeViewModel.CareHomeType;
                objCareHomeBusinessModel.OrderType = objCareHomeViewModel.OrderType;
                objCareHomeBusinessModel.CareHomeStatus = objCareHomeViewModel.CareHomeStatus;
                objCareHomeBusinessModel.CommunicationFormat = objCareHomeViewModel.CommunicationFormat;
                objCareHomeBusinessModel.CommunicationPreferences = objCareHomeViewModel.CommunicationPreferences;
                objCareHomeBusinessModel.PurchaseOrderNo = objCareHomeViewModel.PurchaseOrderNo;
                objCareHomeBusinessModel.HouseName = objCareHomeViewModel.HouseName;
                objCareHomeBusinessModel.HouseNumber = objCareHomeViewModel.HouseNumber;
                objCareHomeBusinessModel.MobileNo = objCareHomeViewModel.MobileNo;
                objCareHomeBusinessModel.ModifiedDate = DateTime.Now.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
                objCareHomeBusinessModel.NextDeliveryDate = objCareHomeViewModel.NextDeliveryDate;
                objCareHomeBusinessModel.PhoneNo = objCareHomeViewModel.PhoneNo;
                objCareHomeBusinessModel.PostCode = objCareHomeViewModel.PostCode;
                objCareHomeBusinessModel.RoundId = objCareHomeViewModel.RoundId;
                objCareHomeBusinessModel.Round = objCareHomeViewModel.Round;
                objCareHomeBusinessModel.UserId = userId;
                // Save CareHome details
                ControllerHelper.ActionName = Constants.ActionNameSaveCareHome;
                var carehomeId = ControllerHelper.PostMethodServiceRequestObject(objCareHomeBusinessModel);
                LogHelper.LogAction(Constants.ActionSaveCareHomeMessage, Constants.CareHomeController, Constants.ActionNameSaveCareHome, userId);
                if (carehomeId != null)
                {
                    return Convert.ToInt64(carehomeId, CultureInfo.InvariantCulture);
                }

                return 0;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get carehome details
        /// </summary>
        /// <param name="carehomeId">Carehome Id</param>
        /// <returns>
        /// Object of CareHomeViewModel
        /// </returns>
        internal CareHomeViewModel GetCareHome(string carehomeId)
        {
			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetCareHome, new[] { carehomeId, userId });
			var objCareHomeBusinessModel = serviceHelper.GetMethodServiceRequest<CareHomeBusinessModel>();
            LogHelper.LogAction(Constants.ActionGetCareHomeMessage, Constants.CareHomeController, Constants.ActionNameGetCareHome, userId);
            
			if (objCareHomeBusinessModel != null)
            {
                var objCareHomeViewModel = new CareHomeViewModel();
                objCareHomeViewModel.CareHomeId = objCareHomeBusinessModel.CareHomeId;
                objCareHomeViewModel.SapCareHomeNumber = objCareHomeBusinessModel.SapCareHomeNumber;
                objCareHomeViewModel.CustomerId = objCareHomeBusinessModel.CustomerId;
                objCareHomeViewModel.CustomerName = objCareHomeBusinessModel.CustomerName;
                objCareHomeViewModel.PersonalInformationId = objCareHomeBusinessModel.PersonalInformationId;
                objCareHomeViewModel.DeliveryFrequency = objCareHomeBusinessModel.DeliveryFrequency;
                objCareHomeViewModel.RoundId = objCareHomeBusinessModel.RoundId;
                objCareHomeViewModel.NextDeliveryDate = objCareHomeBusinessModel.NextDeliveryDate != null ? Convert.ToDateTime(objCareHomeBusinessModel.NextDeliveryDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture) : string.Empty;
                objCareHomeViewModel.CreatedDate = objCareHomeBusinessModel.ModifiedDate != null ? Convert.ToDateTime(objCareHomeBusinessModel.ModifiedDate, CultureInfo.InvariantCulture).ToString(DateFormat, CultureInfo.InvariantCulture) : string.Empty;
                objCareHomeViewModel.CreatedBy = objCareHomeBusinessModel.UserId;
                objCareHomeViewModel.Round = objCareHomeBusinessModel.Round;
                objCareHomeViewModel.BillTo = objCareHomeBusinessModel.BillTo;
                //objCareHomeViewModel.CareHomeType = objCareHomeBusinessModel.CareHomeType;
                objCareHomeViewModel.OrderType = objCareHomeBusinessModel.OrderType;
                objCareHomeViewModel.PurchaseOrderNo = objCareHomeBusinessModel.PurchaseOrderNo;
                objCareHomeViewModel.CareHomeStatus = objCareHomeBusinessModel.CareHomeStatus;
                objCareHomeViewModel.CommunicationFormat = objCareHomeBusinessModel.CommunicationFormat;
                objCareHomeViewModel.CommunicationPreferences = objCareHomeBusinessModel.CommunicationPreferences != null ? objCareHomeBusinessModel.CommunicationPreferences : new string[] { };
                objCareHomeViewModel.CarehomeCommunicationPreferences = objCareHomeBusinessModel.CarehomeCommunicationPreferences;
                objCareHomeViewModel.CareHomeName = objCareHomeBusinessModel.CareHomeName;
                objCareHomeViewModel.PhoneNo = objCareHomeBusinessModel.PhoneNo;
                objCareHomeViewModel.MobileNo = objCareHomeBusinessModel.MobileNo;
                objCareHomeViewModel.FaxNumber = objCareHomeBusinessModel.FaxNumber;
                objCareHomeViewModel.EmailAddress = objCareHomeBusinessModel.EmailAddress;

                objCareHomeViewModel.AddressId = objCareHomeBusinessModel.AddressId;
                objCareHomeViewModel.HouseNumber = objCareHomeBusinessModel.HouseNumber;
                objCareHomeViewModel.HouseName = objCareHomeBusinessModel.HouseName;
                objCareHomeViewModel.Address1 = objCareHomeBusinessModel.Address1;
                objCareHomeViewModel.Address2 = objCareHomeBusinessModel.Address2;
                objCareHomeViewModel.TownOrCity = objCareHomeBusinessModel.City;
                objCareHomeViewModel.County = objCareHomeBusinessModel.CountyCode;
                objCareHomeViewModel.PostCode = objCareHomeBusinessModel.PostCode;
                objCareHomeViewModel.Country = objCareHomeBusinessModel.Country;
                objCareHomeViewModel.SAPCustomerNumber = objCareHomeBusinessModel.SAPCustomerNumber;
                objCareHomeViewModel.HasAnyActiveOrderForOrderType = objCareHomeBusinessModel.HasAnyActiveOrderForOrderType;
                objCareHomeViewModel.OldCarehomeStatus = objCareHomeBusinessModel.CareHomeStatus;
                return objCareHomeViewModel;
            }
            return null;
        }

        /// <summary>
        /// Get carehome patient list
        /// </summary>
        /// <param name="careHomeID">Carehome Id</param>
        /// <returns></returns>
        internal List<PatientBusinessModel> GetCareHomePatientList(string careHomeID)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetCareHomePatientList;
            ControllerHelper.ActionParam = new string[] { careHomeID, userId };
            var patientList = ControllerHelper.GetMethodServiceRequestForList<List<PatientBusinessModel>>();
            LogHelper.LogAction(Constants.ActionGetCareHomePatientListMessage, Constants.CareHomeController, Constants.ActionNameGetCareHomePatientList, userId);
            return patientList;
        }  
      
        /// <summary>
        /// CarehomeHeader
        /// </summary>

        [Serializable]
        internal class CarehomeHeader
        {
            /// <summary>
            /// CustomerNameID
            /// </summary>
            public string CustomerNameID { get; set; }

            /// <summary>
            /// CarehomeNameID
            /// </summary>
            public string CarehomeNameID { get; set; }

            /// <summary>
            /// CarehomeAddress
            /// </summary>
            public string CarehomeAddress { get; set; }

            /// <summary>
            /// CarehomePhoneNo
            /// </summary>
            public string CarehomePhoneNo { get; set; }

            /// <summary>
            /// CarehomeNDD
            /// </summary>
            public string CarehomeNDD { get; set; }

            /// <summary>
            /// CarehomeFrequency
            /// </summary>
            public string CarehomeFrequency { get; set; }

        }

        /// <summary>
        /// GetCareHomeMaintanaceReportData
        /// </summary>
        /// <param name="careHomeID"></param>
        /// <param name="isExport"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <returns></returns>
        public CareHomeReportDetails GetCareHomeMaintanaceReportData(string careHomeID, bool isExport, long page, long rows, string sidx, string sord)
        {
            if (careHomeID != null)
            {
                ControllerHelper.ActionName = Constants.ActionNameGetCareHomeReportMaintenanceData;                
                PatientBusinessModel postData = new PatientBusinessModel
                {
                    CareHomeId = long.Parse(careHomeID),
                    RecordsPerPage=page,
                    IsExport=isExport,
                    PageNumber=rows,
                    SortColumn = sidx,
                    SortBy=sord                    
                };
                var PatientDetailsList = ControllerHelper.PostMethodServiceRequest<PatientBusinessModel>(postData);
                LogHelper.LogAction(Constants.ActionGetCareHomeDataMessage, Constants.CareHomeController, Constants.ActionNameGetCareHomeReportMaintenanceData, userId);
                
                CareHomeReportDetails objcarehomereportdata = new CareHomeReportDetails
                {
                    PatientName = PatientDetailsList.FirstName,
                    PatientSAPId = PatientDetailsList.SAPPatientNumber,
                    DateOfBirth = PatientDetailsList.DateOfBirth,
                    PatientStatus = PatientDetailsList.PatientStatusCode,
                    LastAssessDate = PatientDetailsList.AssessmentDate,
                    NextAssessDate = PatientDetailsList.NextAssessmentDate,
                    SAPProductId = PatientDetailsList.SAPProductId,
                    ProductDescription = PatientDetailsList.ProductDescription,
                    AssPPD = PatientDetailsList.AssessedPadsPerDay,
                    ActPPD = PatientDetailsList.ActPPD,
                    Frequency = PatientDetailsList.DeliveryFrequency,
                    NDD = PatientDetailsList.NextDeliveryDate,
                    Comment = PatientDetailsList.Comment,
                    PatientDetailsReports = PatientDetailsList.CarehomeMaintanceReport
                };
                return objcarehomereportdata;
            }
            else
            {
                return new CareHomeReportDetails();
            }
        }

        /// <summary>
        /// Group data patient wise for excel report
        /// </summary>
        /// <param name="list"></param>
        /// <param name="columnsToRemove"></param>
        /// <param name="isTransformBool"></param>
        /// <returns></returns>
        public static DataTable ListToDataTableForCareHome(List<CarehomeMaintenanceReportBusinessModel> list, List<string> columnsToRemove, bool isTransformBool)
        {
            var data = list.GroupBy(x => x.PatientSAPId).Select(q => q).ToList();
            List<CarehomeMaintenanceReportBusinessModel> datalist = new List<CarehomeMaintenanceReportBusinessModel>();
            for (int i = 0; i < data.Count; i++)
            {
                var listobj = data[i].ToList();
                for (int k = 0; k < listobj.Count; k++)
                {
                    if (k == 0)
                    {
                        datalist.Add(listobj[k]);
                        continue;
                    }

                    CarehomeMaintenanceReportBusinessModel obj = listobj[k];

                    obj.PatientName = "";
                    obj.PatientSAPId = "";
                    obj.DateOfBirth = "";
                    obj.PatientStatus = "";
                    obj.LastAssessDate = "";
                    obj.NextAssessDate = "";

                    datalist.Add(obj);
                }
            }
            return ExcelHelper.ListToDataTable(datalist, columnsToRemove, false);
        }
        #endregion
    }
}