﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Jagdish
// Created          : 12-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="ReportBuilderHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using SCA.Indigo.Common.Enums;
    using SCA.Indigo.Web.UI.ViewModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    /// <summary>
    /// Report Builder Helper Class
    /// </summary>
    public class ReportBuilderHelper
    {
        /// <summary>
        /// The user type
        /// </summary>
        private readonly string userType = ControllerHelper.CurrentUser.UserType;
        /// <summary>
        /// The role name
        /// </summary>
        private readonly string roleName = ControllerHelper.CurrentUser.RoleName;
        /// <summary>
        /// The user identifier
        /// </summary>
        private readonly string userId = ControllerHelper.CurrentUser.UserId.ToString();

        /// <summary>
        /// Opens the report builder.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns></returns>
        public ReportBuilderViewModel OpenReportBuilder(ReportBuilderViewModel reportBuilderViewModel)
        {
            PopulateDropDowns(reportBuilderViewModel);
            if (userType == Constants.SCAUserType && (roleName == SCAEnums.Role.Administrator.ToString(CultureInfo.InvariantCulture) || roleName == SCAEnums.Role.KeyUser.ToString(CultureInfo.InvariantCulture)))
            {
                reportBuilderViewModel.EnableUserTable = true;
            }

            return reportBuilderViewModel;
        }

        /// <summary>
        /// Populates the drop downs.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns>
        /// Drop down data
        /// </returns>
        private ReportBuilderViewModel PopulateDropDowns(ReportBuilderViewModel reportBuilderViewModel)
        {

            string userId = ControllerHelper.CurrentUser.UserId.ToString();
            reportBuilderViewModel.CustomersList = GenericHelper.GetCustomersListForDropdown(userId);

            IEnumerable<SCAEnums.CustomerTable> customerColumnsList = Enum.GetValues(typeof(SCAEnums.CustomerTable)).Cast<SCAEnums.CustomerTable>();
            reportBuilderViewModel.CustomerColumnsList = from column in customerColumnsList
                                                         select new SelectListItem
                                                         {
                                                             Text = GenericHelper.GetDescriptionFromEnumValue(column),
                                                             Value = column.ToString(),
                                                         };

            IEnumerable<SCAEnums.PatientTable> patientColumnsList = Enum.GetValues(typeof(SCAEnums.PatientTable)).Cast<SCAEnums.PatientTable>();
            reportBuilderViewModel.PatientColumnsList = from patientColumn in patientColumnsList
                                                        select new SelectListItem
                                                        {
                                                            Text = GenericHelper.GetDescriptionFromEnumValue(patientColumn),
                                                            Value = patientColumn.ToString(),
                                                        };

            IEnumerable<SCAEnums.CareHomeTable> careHomeColumnsList = Enum.GetValues(typeof(SCAEnums.CareHomeTable)).Cast<SCAEnums.CareHomeTable>();
            reportBuilderViewModel.CareHomeColumnsList = from column in careHomeColumnsList
                                                         select new SelectListItem
                                                         {
                                                             Text = GenericHelper.GetDescriptionFromEnumValue(column),
                                                             Value = column.ToString(),
                                                         };

            IEnumerable<SCAEnums.OrderTable> orderColumnsList = Enum.GetValues(typeof(SCAEnums.OrderTable)).Cast<SCAEnums.OrderTable>();
            reportBuilderViewModel.OrderColumnsList = from column in orderColumnsList
                                                      select new SelectListItem
                                                      {
                                                          Text = GenericHelper.GetDescriptionFromEnumValue(column),
                                                          Value = column.ToString(),
                                                      };

            IEnumerable<SCAEnums.UserMaintenanceTable> userMainTenanceColumnsList = Enum.GetValues(typeof(SCAEnums.UserMaintenanceTable)).Cast<SCAEnums.UserMaintenanceTable>();
            reportBuilderViewModel.UserMainTenanceColumnsList = from column in userMainTenanceColumnsList
                                                                select new SelectListItem
                                                                {
                                                                    Text = GenericHelper.GetDescriptionFromEnumValue(column),
                                                                    Value = column.ToString(),
                                                                };

            IEnumerable<SCAEnums.PrescriptionTable> prescriptionColumnsList = Enum.GetValues(typeof(SCAEnums.PrescriptionTable)).Cast<SCAEnums.PrescriptionTable>();
            reportBuilderViewModel.PrescriptionColumnsList = from column in prescriptionColumnsList
                                                             select new SelectListItem
                                                             {
                                                                 Text = GenericHelper.GetDescriptionFromEnumValue(column),
                                                                 Value = column.ToString(),
                                                             };

            IEnumerable<SCAEnums.ClinicalContactsTable> clinicalContactsColumnsList = Enum.GetValues(typeof(SCAEnums.ClinicalContactsTable)).Cast<SCAEnums.ClinicalContactsTable>();
            reportBuilderViewModel.ClinicalContactsColumnsList = from column in clinicalContactsColumnsList
                                                                 select new SelectListItem
                                                                 {
                                                                     Text = GenericHelper.GetDescriptionFromEnumValue(column),
                                                                     Value = column.ToString(),
                                                                 };

            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            var patientTypeListId = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
            var patientStatusListId = Convert.ToInt64(SCAEnums.ListType.PatientStatus, CultureInfo.InvariantCulture);
            var languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);

            var listIds = new[] { patientTypeListId, patientStatusListId };
            ControllerHelper.ActionParam = new[] { CommonHelper.ConvertToCommaSeparatedString(listIds), Convert.ToString(languageId, CultureInfo.InvariantCulture) };

            var patientData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();

            var patientType = patientData.Where(c => c.ListTypeId.Equals(3));
            var pateintStatus = patientData.Where(c => c.ListTypeId.Equals(4));

            if (ControllerHelper.CurrentUser.IsCareHomeUser)
                patientType = patientType.Where(q => Enum.IsDefined(typeof(SCAEnums.CarehomePatientType),
                                                                       Convert.ToInt32(q.ListId, CultureInfo.InvariantCulture))).ToList();

            if (patientType != null)    
            {
                reportBuilderViewModel.PatientTypeList = new SelectList(patientType.ToList(), Constants.ListId, Constants.DisplayText);
            }

            if (pateintStatus != null)
            {
                reportBuilderViewModel.PatientStatusList = new SelectList(pateintStatus.ToList(), Constants.ListId, Constants.DisplayText);
            }

            /// Get Report Format DropDown
            ControllerHelper.ActionName = Constants.ActionNameGetReportFormatDropDown;
            var reportFormatList = ControllerHelper.GetMethodServiceRequestForList<List<ReportFormatBusinessModel>>();
            if (reportFormatList != null)
            {
                reportBuilderViewModel.ReportFormatList = new SelectList(reportFormatList.ToList(), Constants.ReportBuilderFormatId, Constants.ReportFormatName);
            }

            return reportBuilderViewModel;
        }

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns>
        /// Report data list
        /// </returns>
        public ReportBuilderViewModel GetReportData(ReportBuilderViewModel reportBuilderViewModel)
        {
            ReportBuilderCriteria reportBuilderCriteria = new ReportBuilderCriteria();
            reportBuilderCriteria.UserId = ControllerHelper.CurrentUser.UserId.ToString();
            reportBuilderCriteria.ColumnNames = reportBuilderViewModel.ColumnNames;
            reportBuilderCriteria.CustomerIds = reportBuilderViewModel.CustomerIds;
            reportBuilderCriteria.PatientNameId = reportBuilderViewModel.PatientNameId;
            reportBuilderCriteria.CareHomeIds = reportBuilderViewModel.CareHomeIds;
            reportBuilderCriteria.PatientDeliveryDateFrom = reportBuilderViewModel.PatientDeliveryDateFrom;
            reportBuilderCriteria.PatientDeliveryDateTo = reportBuilderViewModel.PatientDeliveryDateTo;
            reportBuilderCriteria.OrderDateFrom = reportBuilderViewModel.OrderDateFrom;
            reportBuilderCriteria.OrderDateTo = reportBuilderViewModel.OrderDateTo;
            reportBuilderCriteria.InvoiceDateFrom = reportBuilderViewModel.InvoiceDateFrom;
            reportBuilderCriteria.InvoiceDateTo = reportBuilderViewModel.InvoiceDateTo;
            reportBuilderCriteria.PatientCreateDateFrom = reportBuilderViewModel.PatientCreateDateFrom;
            reportBuilderCriteria.PatientCreateDateTo = reportBuilderViewModel.PatientCreateDateTo;
            reportBuilderCriteria.NextAssessmentDateFrom = reportBuilderViewModel.NextAssessmentDateFrom;
            reportBuilderCriteria.NextAssessmentDateTo = reportBuilderViewModel.NextAssessmentDateTo;
            reportBuilderCriteria.InvoiceNumber = reportBuilderViewModel.InvoiceNumber;
            reportBuilderCriteria.InvoiceNumberTo = reportBuilderViewModel.InvoiceNumberTo;
            reportBuilderCriteria.PatientType = reportBuilderViewModel.PatientType;
            reportBuilderCriteria.PatientStatus = reportBuilderViewModel.PatientStatus;
            reportBuilderCriteria.UserDataOnly = reportBuilderViewModel.UserDataOnly;
            reportBuilderCriteria.CustomerColumns = reportBuilderViewModel.FieldCustomerColumns;
            reportBuilderCriteria.PatientColumns = reportBuilderViewModel.FieldPatientColumns;
            reportBuilderCriteria.PrescriptionColumns = reportBuilderViewModel.FieldPrescriptionColumns;
            reportBuilderCriteria.ClinicalContactsColumns = reportBuilderViewModel.FieldClinicalContactsColumns;
            reportBuilderCriteria.CareHomeColumns = reportBuilderViewModel.FieldCareHomeColumns;
            reportBuilderCriteria.OrderColumns = reportBuilderViewModel.FieldOrderColumns;
            reportBuilderCriteria.UserMaintenanceColumns = reportBuilderViewModel.FieldUserMaintenanceColumns;
            reportBuilderCriteria.ProductIds = reportBuilderViewModel.ProductIds;
            reportBuilderCriteria.LanguageId = Convert.ToInt32(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture);
            reportBuilderCriteria.PageNbr = reportBuilderViewModel.PageNbr;
            reportBuilderCriteria.PageSize = reportBuilderViewModel.PageSize;
            reportBuilderCriteria.SortDir = reportBuilderViewModel.SortDir;
            reportBuilderCriteria.SortColumn = reportBuilderViewModel.SortColumn;
            reportBuilderCriteria.IsDownload = reportBuilderViewModel.IsDownload;
            ControllerHelper.ActionName = Constants.ActionNameGetReportData;
			var result = ControllerHelper.PostMethodServiceRequest<ReportBuilderCriteria, List<ReportDataBusinessModel>>(reportBuilderCriteria);
         
            if (result != null && result.Any())
            {
				reportBuilderViewModel.ReportDataList = result;
				reportBuilderViewModel.TotalRows = result[0].TotalRows;
                if (reportBuilderViewModel.TotalRows != 0)
                {
                    reportBuilderViewModel.TotalPages = (int)Math.Ceiling(decimal.Parse(Convert.ToString(reportBuilderViewModel.ReportDataList[0].TotalRows / reportBuilderViewModel.PageSize, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture));
                }
            }

            return reportBuilderViewModel;
        }

        /// <summary>
        /// Saves the report format.
        /// </summary>
        /// <param name="reportBuilderViewModel">The report builder view model.</param>
        /// <returns>
        /// Success or failure
        /// </returns>
        public string SaveReportFormat(ReportBuilderViewModel reportBuilderViewModel)
        {
            string reportBuilderResponse = string.Empty;
            ReportFormatBusinessModel reportFormatBusinessModel = new ReportFormatBusinessModel();
            reportFormatBusinessModel.ReportBuilderFormatId = Convert.ToInt64(reportBuilderViewModel.ReportBuilderId, CultureInfo.InvariantCulture);
            reportFormatBusinessModel.PatientColumn = reportBuilderViewModel.FieldPatientColumns;
            reportFormatBusinessModel.CustomerColumn = reportBuilderViewModel.FieldCustomerColumns;
            reportFormatBusinessModel.CareHomeColumn = reportBuilderViewModel.FieldCareHomeColumns;
            reportFormatBusinessModel.CustomerParameterColumn = reportBuilderViewModel.FieldCustomerParameterColumns;
            reportFormatBusinessModel.OrderColumn = reportBuilderViewModel.FieldOrderColumns;
            reportFormatBusinessModel.PerscriptionColumn = reportBuilderViewModel.FieldPrescriptionColumns;
            reportFormatBusinessModel.ReportFormatName = reportBuilderViewModel.FieldReportFormatName;
            reportFormatBusinessModel.UserMaintainanceColumn = reportBuilderViewModel.FieldUserMaintenanceColumns;
            reportFormatBusinessModel.ClinicalContactColumn = reportBuilderViewModel.FieldClinicalContactsColumns;
            reportFormatBusinessModel.UserId = ControllerHelper.CurrentUser.UserId.ToString();
            ControllerHelper.ActionName = Constants.ActionNameSaveReportBuilder;
            reportBuilderResponse = (string)ControllerHelper.PostMethodServiceRequestObject(reportFormatBusinessModel);
            return reportBuilderResponse;
        }

        /// <summary>
        /// Gets the report format.
        /// </summary>
        /// <param name="reportFormatId">The report format identifier.</param>
        /// <returns>
        /// Report Format
        /// </returns>
        public ReportBuilderViewModel GetReportFormat(string reportFormatId)
        {
            ReportBuilderViewModel reportBuilderViewModel = new ReportBuilderViewModel();
            ControllerHelper.ActionParam = new[] { reportFormatId };
            ControllerHelper.ActionName = Constants.ActionNameGetReportFormat;
            var reportFormatList = ControllerHelper.GetMethodServiceRequestForList<List<ReportFormatBusinessModel>>();
            if (reportFormatList != null)
            {
                reportBuilderViewModel.ReportFormatBusinessModel = reportFormatList.FirstOrDefault();
            }

            return reportBuilderViewModel;
        }

        /// <summary>
        /// Gets the products on customer identifier.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns>
        /// Products List
        /// </returns>
        public SelectList GetProductsOnCustomerId(string customerIds)
        {
            return GenericHelper.GetProductsOnCustomerId(customerIds, userId);
        }

        /// <summary>
        /// Gets the carehome on customer identifier.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns>
        /// Products List
        /// </returns>
        public SelectList GetCareHomeOnCustomerId(string customerIds)
        {
            return GenericHelper.GetCareHomeOnCustomerId(customerIds, userId);
        }
        
        /// <summary>
        /// Gets the carehome on customer identifier.
        /// </summary>
        /// <param name="customerIds">The customer ids.</param>
        /// <returns>
        /// PatientType List
        /// </returns>
        public SelectList GetPatientTypeOnCustomerId(string customerIds)
        {
            return GenericHelper.GetPatientTypeOnCustomerId(customerIds, userId);
        }
    }
}