﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using SCA.Indigo.Business.BusinessModels;
using System.Configuration;
using System.Globalization;
using SCA.Indigo.Common;
using SCA.Indigo.Common.Enums;
using WebSupergoo.ABCpdf10;
using System.Text;

namespace SCA.Indigo.Web.UI.Helper
{
	public class PDFHelper
	{
		/// <summary>
		/// Private default Constructor
		/// </summary>
		private PDFHelper() { }
		/// <summary>
		/// Creates PDF Document with default settings
		/// </summary>
		/// <param name="pdfFormat"></param>
		/// <returns></returns>
		public static Doc GetPDFDocument(SCAEnums.PDFFormat pdfFormat)
		{
			Doc pdfDoc = new Doc();
			pdfDoc.FontSize = Constants.PDFDocumentFontSize;
			pdfDoc.Font = pdfDoc.AddFont(Constants.PDFDocumentFont);

            switch (pdfFormat)
            {
                case SCAEnums.PDFFormat.Landscape:
                    // apply a rotation transform
                    double w = pdfDoc.MediaBox.Width;
                    double h = pdfDoc.MediaBox.Height;
                    double l = pdfDoc.MediaBox.Left;
                    double b = pdfDoc.MediaBox.Bottom;
                    pdfDoc.Transform.Rotate(90, l, b);
                    pdfDoc.Transform.Translate(w, 0);

                    // rotate our rectangle
                    pdfDoc.Rect.Width = h;
                    pdfDoc.Rect.Height = w;

                    int theID = pdfDoc.GetInfoInt(pdfDoc.Root, "Pages");
                    pdfDoc.SetInfo(theID, "/Rotate", "90");
                    break;

                case SCAEnums.PDFFormat.Potraite:
                    break;
                default:
                    break;
            }
			return pdfDoc;
		}

		/// <summary>
		/// Creates PDF Report for Care Home
		/// </summary>
		/// <param name="hdrDT">Datatable for Header</param>
		/// <param name="dtlDT">List For Detail Data</param>
		/// <param name="columnsToRemoveDTL">Columns to remove from Detail Data</param>
		/// <param name="resHeadername">Header section header name From resource file</param>
		/// <param name="resDetailName">Detail Header from resource file</param>
		/// <param name="serverDirectory"></param>
		/// <param name="fileNamePrefix"></param>
		/// <returns></returns>
		public static string CreateCarehomeReportPDF(DataTable hdrDT, List<CarehomeMaintenanceReportBusinessModel> lstDtl, List<string> columnsToRemoveDTL,
													 List<string> resHeadername, List<string> resDetailName, string serverDirectory, string fileNamePrefix)
		{
			string fileName = "";
			string filePath = SetupPDFFileDirectory(serverDirectory, fileNamePrefix, ref fileName);
            bool isPatientHeaderRow = true;

			Doc pdfDoc = GetPDFDocument(SCAEnums.PDFFormat.Landscape);

            pdfDoc.Page = pdfDoc.AddPage();
            int pageNo = pdfDoc.PageNumber;
            pdfDoc.Width = Constants.PDFCareHomeReportTableBorderWidth;

            pdfDoc = GetCareHomeReportHeader(hdrDT, pdfDoc, resHeadername);

            string strY = (pdfDoc.Pos.Y - Constants.PDFCareHomeReportContentSpacing).ToString();
            pdfDoc.Rect.String = Constants.PDFCareHomeReportContentPosition + strY;

            PDFTable pdfTableDtl = new PDFTable(pdfDoc, Constants.PDFCareHomeReportDetailsColumnCount);
            pdfTableDtl.SetColumnWidths(Constants.PDFCareHomeReportDetailsColumnWidth);
            pdfTableDtl.CellPadding = Constants.PDFCareHomeReportDetailsCellPadding;

            pdfTableDtl.HorizontalAlignment = Constants.PDFCenterAlign;
            pdfTableDtl.AddHtml(GetBoldHeader(resDetailName));
            pdfTableDtl.FrameHeader = true;
            pdfTableDtl.RepeatHeader = true;
            
			var data = lstDtl.GroupBy(x => x.PatientId).Select(q => q).ToList();

            bool isColor = true;
            for (int i = 0; i < data.Count; i++, isColor = !isColor)
			{
                List<CarehomeMaintenanceReportBusinessModel> lstCareHome = data[i].ToList();
                
                for (int k = 0; k < lstCareHome.Count; k++)
                {
                    if (pageNo != pdfDoc.PageNumber)
                    {
                        pageNo = pdfDoc.PageNumber;
                        pdfDoc = GetCareHomeReportHeader(hdrDT, pdfDoc, resHeadername);
                        strY = (pdfDoc.Pos.Y - Constants.PDFCareHomeReportContentSpacing).ToString();
                        pdfDoc.Rect.String = Constants.PDFCareHomeReportContentPosition + strY;
                        pdfTableDtl.HorizontalAlignment = Constants.PDFCenterAlign;
                    }

                    string strProdD= lstCareHome[k].ProductDescription;
                    lstCareHome[k].ProductDescription = strProdD.Length > Constants.PDFCareHomeReportProductDescriptionLength ?
                        strProdD.Substring(0, Constants.PDFCareHomeReportProductDescriptionLength) : strProdD;

                    if (isPatientHeaderRow || k == 0)
                    {
                        isPatientHeaderRow = false;
                        var dataRow = new string[] 
						{
							SetAlign(lstCareHome[k].PatientName, Constants.PDFLeftAlign),
							lstCareHome[k].PatientSAPId,
							lstCareHome[k].DateOfBirth,
                            lstCareHome[k].PatientStatus,
							lstCareHome[k].LastAssessDate,
							lstCareHome[k].NextAssessDate,
                            lstCareHome[k].SAPProductId,
							SetAlign(lstCareHome[k].ProductDescription, Constants.PDFLeftAlign),
							lstCareHome[k].AssPPD,
                            lstCareHome[k].ActPPD,
							lstCareHome[k].Frequency,
							lstCareHome[k].NDD,
                            lstCareHome[k].Comment
						};
                        pdfTableDtl.AddHtml(dataRow);
                    }
                    else
                    {
                        var datarow = new string[] 
                        { 
                            "", "", "", "", "", "",
                            lstCareHome[k].SAPProductId,
							SetAlign(lstCareHome[k].ProductDescription, Constants.PDFLeftAlign),
							lstCareHome[k].AssPPD,
                            lstCareHome[k].ActPPD,
							lstCareHome[k].Frequency,
							lstCareHome[k].NDD,
                            lstCareHome[k].Comment
                        };
                        pdfTableDtl.AddHtml(datarow);
                    }
                    if (pdfDoc.Pos.Y < Constants.PDFCareHomeReportMinLastRowPos)
                    {
                        isPatientHeaderRow = true;
                    }

                    if (isColor)
                        pdfTableDtl.FillRow(Constants.PDFAlternateRowColor, pdfTableDtl.Row);
                }
				pdfTableDtl.Frame();
			}

            for (int p = 1; p <= pdfDoc.PageCount; p++)
            {
                pdfDoc.PageNumber = p;
                pdfDoc.Rect.String = Constants.PDFPageNoPosition;
                pdfDoc.AddHtml(SetAlign("<b>Page No. " + p.ToString() + " of " + pdfDoc.PageCount.ToString() + "</b>",Constants.PDFRightAlign));
            }

			for (int p = 1; p <= pdfDoc.PageCount; p++)
			{
				pdfDoc.PageNumber = p;
				pdfDoc.Flatten();
			}

			filePath = SavePdfDocument(filePath, fileName, pdfDoc);
			return filePath;
		}

		/// <summary>
		/// Creates Header for Care Home Report
		/// </summary>
		/// <param name="hdrDT">Header data table</param>
		/// <param name="pdfDoc">PDF Doc Object</param>
		/// <param name="headerName">Resource Name for Header</param>
		/// <returns>PDF document with header</returns>
		public static Doc GetCareHomeReportHeader(DataTable hdrDT, Doc pdfDoc, List<string> headerName)
		{
			pdfDoc.Rect.String = Constants.PDFCareHomeReportHeaderPosition;

			PDFTable pdfTableHdr = new PDFTable(pdfDoc, Constants.PDFCareHomeReportHeaderColumnCount);
			pdfTableHdr.SetColumnWidths(Constants.PDFCareHomeReportHeaderColumnWidth);
			pdfTableHdr.HorizontalAlignment = Constants.PDFLeftAlign;

			pdfTableHdr.CellPadding = Constants.PDFCareHomeReportHeaderCellPadding;
			
			for (int i = 0, l = 0; i < Constants.PDFCareHomeReportHeaderRowCount; i++)
			{
				pdfTableHdr.NextRow();

				string[] arr = new string[Constants.PDFCareHomeReportHeaderColumnCount];
				for (int k = 0; k < arr.Length; k++, l++)
				{
					arr[k] = GetBoldLabel(headerName[l]);
					k++;
					arr[k] = hdrDT.Rows[0][l].ToString();
				}
				pdfTableHdr.AddHtml(arr);
			}
			pdfTableHdr.Frame();
			return pdfDoc;
		}

		/// <summary>
		/// Text Bold
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static List<string> GetBoldHeader(List<string> list)
		{
			for (int i = 0; i < list.Count; i++)
			{
                if (list[i].Trim() == "")
                    continue;
				list[i] = "<b>" + list[i] + "</b>";
			}
			return list;
		}

		/// <summary>
		/// Text Bold With :
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetBoldLabel(string value)
		{
            if (value.Trim() == "")
                return "";
			return "<b>" + value + " : </b>";
		}

        /// <summary>
        /// Set Alignment
        /// </summary>
        /// <param name="value"></param>
        /// <param name="alignPos"></param>
        /// <returns></returns>
		public static string SetAlign(string value, double alignPos)
		{
            if (value.Trim() == "")
                return "";
			return String.Format("<stylerun hpos='{0}'>{1}<stylerun>", alignPos, value);
		}

		/// <summary>
		/// Created File Directory
		/// </summary>
		/// <param name="serverDirectory">Server Common Directory</param>
		/// <param name="fileNamePrefix">File Name Prefix</param>
		/// <param name="fileName">Call By Reference Filename</param>
		/// <returns>Common File path on server</returns>
		public static string SetupPDFFileDirectory(string serverDirectory, string fileNamePrefix, ref string fileName)
		{
			// set variables 
			var filePath = string.Empty;
			string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);
			string tempPath = GenericHelper.TempAddress;

			string uploadDirectory = GenericHelper.UploadDirectory;
			string downloadUrl = GenericHelper.DownloadUrl;

			/// create directory on the server
			Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);

			/// create directory on the virtual directory.
			Common.CommonHelper.CreateDirectoryIfNotExists(tempPath);

			var directory = Path.Combine(serverPath, serverDirectory);
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}

			/// build pdf file path
			StringBuilder sbFileName = new StringBuilder();
			sbFileName.Append(fileNamePrefix).Append(Constants.Underscore);
			sbFileName.Append(Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture)).Append(Constants.Underscore);
			sbFileName.Append(DateTime.Now.ToString(Constants.DateTimeFormatyy5, CultureInfo.InvariantCulture)).Append(Constants.PdfFileExtension);
			
			fileName = sbFileName.ToString();
			filePath = Path.Combine(serverPath, serverDirectory + '\\' + fileName);

			return filePath;
		}

		/// <summary>
		/// Create an PDF file, and Save it to virtual Directory.
		/// </summary>
		/// <param name="ds">DataSet containing the data to be written to the Excel.</param>
		/// <param name="excelFilename">Name of file to be written.</param>
		/// <returns>
		/// True if successful, false if something went wrong.
		/// </returns>
		public static string SavePdfDocument(string filePath, string fileName, Doc pdfDoc)
		{
			try
			{
				pdfDoc.Save(filePath);
				pdfDoc.Clear();

				string uploadDirectory = GenericHelper.UploadDirectory;
				string downloadUrl = GenericHelper.DownloadUrl;

				var filePhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath("\\" + uploadDirectory + "\\" + fileName);
				System.IO.File.Copy(filePath, filePhysicalPath, true);
				filePath = downloadUrl + '\\' + uploadDirectory + '\\' + fileName;

				return filePath;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// This function will generate interaction report data into pdf file.
		/// </summary>
		/// <param name="interactionReportDataList"> list of interaction report data</param>
		/// <param name="interactionReportDirectory">The server directory.</param>
		/// <param name="fileNamePrefix">The file name prefix.</param>
		/// <param name="columnsToRemove">The columns to remove.</param>
		/// <returns>It will return file path for pdf</returns>
		public static string CreateInteractionReportPDF(List<InteractionReportSummaryBusinessModel> interactionReportDataList, string interactionReportDirectory, string fileNamePrefix, List<string> columnsToRemove, List<string> resPdfHeaderName)
		{
			string fileName = string.Empty;
			string filePath = string.Empty;
			try
			{
                for (int i = 0;
                        i < (Constants.PDFInteractionReportFirstPageColumnCount + Constants.PDFInteractionReportSecondPageColumnCount - 1);
                        i++)
                {
                    switch(i)
                    {
                        case 0:
                        case 2:
                        case 4:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 15:
                            resPdfHeaderName[i] = SetAlign(resPdfHeaderName[i], Constants.PDFLeftAlign);
                            break;
                        case 16:
                            resPdfHeaderName[i] = SetAlign(resPdfHeaderName[i], Constants.PDFRightAlign);
                            break;
                        default:
                            resPdfHeaderName[i] = SetAlign(resPdfHeaderName[i], Constants.PDFCenterAlign);
                            break;
                    }
                }

                List<string> lst1 = new List<string>();
                for (int i = 0; i < Constants.PDFInteractionReportFirstPageColumnCount; i++)
                    lst1.Add(resPdfHeaderName[i]);

                List<string> lst2 = new List<string>();
                lst2.Add(resPdfHeaderName[3]);
                for (int i = Constants.PDFInteractionReportFirstPageColumnCount;
                         i < (Constants.PDFInteractionReportFirstPageColumnCount + Constants.PDFInteractionReportSecondPageColumnCount - 1);
                         i++)
                    lst2.Add(resPdfHeaderName[i]);

				filePath = SetupPDFFileDirectory(interactionReportDirectory, fileNamePrefix, ref fileName);

				Doc pdfDoc = GetPDFDocument(SCAEnums.PDFFormat.Landscape);

                Doc pdfDoc1 = GetPDFDocument(SCAEnums.PDFFormat.Landscape);
                pdfDoc1.Rect.Inset(Constants.PDFInteractionReportInset, Constants.PDFInteractionReportInset);
                PDFTable pdfTable1 = new PDFTable(pdfDoc1, Constants.PDFInteractionReportFirstPageColumnCount);
                pdfTable1.CellPadding = Constants.PDFInteractionReportHeaderCellPadding;
                pdfTable1.SetColumnWidths(Constants.PDFInteractionReportHeaderColWidth);
                pdfTable1.HorizontalAlignment = Constants.PDFCenterAlign;
                pdfTable1.CAddHtml(GetBoldHeader(lst1));
                pdfTable1.HorizontalAlignment = Constants.PDFLeftAlign;
                pdfTable1.FrameHeader = true;
                pdfTable1.RepeatHeader = true;

                Doc pdfDoc2 = GetPDFDocument(SCAEnums.PDFFormat.Landscape);
                pdfDoc2.Rect.Inset(Constants.PDFInteractionReportInset, Constants.PDFInteractionReportInset);
                PDFTable pdfTable2 = new PDFTable(pdfDoc2, Constants.PDFInteractionReportSecondPageColumnCount);
                pdfTable2.CellPadding = Constants.PDFInteractionReportHeaderCellPadding;
                pdfTable2.SetColumnWidths(Constants.PDFInteractionReportHeaderColWidthP);
                pdfTable2.HorizontalAlignment = Constants.PDFCenterAlign;
                pdfTable2.CAddHtml(GetBoldHeader(lst2));
                pdfTable2.HorizontalAlignment = Constants.PDFLeftAlign;
                pdfTable2.FrameHeader = true;
                pdfTable2.RepeatHeader = true;

                bool isNextPage = false;
                bool isColor = true;

                foreach (InteractionReportSummaryBusinessModel obj in interactionReportDataList)
                {

                AddRows:

                    string strDesc = obj.Description.Replace("\n", "</br>").Replace("</br></br>", "</br>");
                    obj.Description = strDesc.Length > Constants.PDFIntercationReportDescriptionLength ?
                        strDesc.Substring(0, Constants.PDFIntercationReportDescriptionLength) : strDesc;

                    var datarow = new string[] 
                    {
                        SetAlign(obj.PatientId,Constants.PDFCenterAlign),
                        obj.CreatedOn,
                        obj.CreatedBy,
                        obj.ModifiedOn,
                        obj.ModifiedBy,
                        obj.Description,
                        SetAlign(obj.Alert,Constants.PDFCenterAlign),
                        obj.AssignedTo
                    };

                    isNextPage = pdfTable2.CAddHtml(datarow);

                    if (isNextPage)
                    {
                        pdfTable2.DeleteLastRow();
                        goto PageBreak;
                    }

                    var lastRowSize = pdfTable2.getLastRowSize();

                    datarow = new string[] 
                    { 
                        obj.CustomerName,
                        SetAlign(obj.CustomerId,Constants.PDFCenterAlign),
                        obj.PatientName,
                        SetAlign(obj.PatientId,Constants.PDFCenterAlign),
                        obj.CareHomeName,
                        SetAlign(obj.CareHomeId,Constants.PDFCenterAlign),
                        obj.InteractionType,
                        obj.SubType,
                        obj.StatusId
                    };

                    isNextPage = pdfTable1.CAddHtml(datarow, lastRowSize);
                    if (isNextPage)
                    {
                        pdfTable2.DeleteLastRow();
                        pdfTable1.DeleteLastRow();
                        goto PageBreak;
                    }

                    
                    goto lblContinue;

                PageBreak:
                    pdfTable1.FrameRows();
                    pdfTable2.CFrameRows();

                    pdfDoc.Append(pdfDoc1);
                    pdfDoc.Append(pdfDoc2);

                    pdfDoc1 = GetPDFDocument(SCAEnums.PDFFormat.Landscape);
                    pdfDoc1.Rect.Inset(Constants.PDFInteractionReportInset, Constants.PDFInteractionReportInset);
                    pdfTable1 = new PDFTable(pdfDoc1, Constants.PDFInteractionReportFirstPageColumnCount);
                    pdfTable1.CellPadding = Constants.PDFInteractionReportHeaderCellPadding;
                    pdfTable1.SetColumnWidths(Constants.PDFInteractionReportHeaderColWidth);
                    pdfTable1.HorizontalAlignment = Constants.PDFCenterAlign;
                    pdfTable1.CAddHtml(lst1);
                    pdfTable1.HorizontalAlignment = Constants.PDFLeftAlign;
                    pdfTable1.FrameHeader = true;
                    pdfTable1.RepeatHeader = true;

                    pdfDoc2 = GetPDFDocument(SCAEnums.PDFFormat.Landscape);
                    pdfDoc2.Rect.Inset(Constants.PDFInteractionReportInset, Constants.PDFInteractionReportInset);
                    pdfTable2 = new PDFTable(pdfDoc2, Constants.PDFInteractionReportSecondPageColumnCount);
                    pdfTable2.CellPadding = Constants.PDFInteractionReportHeaderCellPadding;
                    pdfTable2.SetColumnWidths(Constants.PDFInteractionReportHeaderColWidthP);
                    pdfTable2.HorizontalAlignment = Constants.PDFCenterAlign;
                    pdfTable2.CAddHtml(lst2);
                    pdfTable2.HorizontalAlignment = Constants.PDFLeftAlign;
                    pdfTable2.FrameHeader = true;
                    pdfTable2.RepeatHeader = true;

                    isNextPage = false;

                    goto AddRows;

                lblContinue:
                    if (isColor)
                    {
                        pdfTable1.FillRow(Constants.PDFAlternateRowColor, pdfTable1.Row);
                        pdfTable2.FillRow(Constants.PDFAlternateRowColor, pdfTable2.Row);
                    }
                    isColor = !isColor;
                    continue;
                }

                pdfTable1.FrameRows();
                pdfTable2.FrameRows();

                for (int p = 1; p <= pdfDoc.PageCount; p++)
                {
                    pdfDoc.PageNumber = p;
                    pdfDoc.Rect.String = Constants.PDFPageNoPosition;
                    pdfDoc.AddHtml(SetAlign("<b>Page No. " + p.ToString() + " of " + pdfDoc.PageCount.ToString() + "</b>", Constants.PDFRightAlign));
                }

				for (int p = 1; p <= pdfDoc.PageCount; p++)
				{
					pdfDoc.PageNumber = p;
					pdfDoc.Flatten();
				}
				filePath = SavePdfDocument(filePath, fileName, pdfDoc);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return filePath;
		}

		/// <summary>
		/// This function will return PDF Doc object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dataList">List of data.</param>
		/// <param name="columnsToRemove">The columns to remove.</param>
		/// <param name="setColumnWidths">Set columns widths.</param>
		/// <param name="cellPadding">Cell Padding</param>
		/// <param name="pdfDoc">Pdf Doc object</param>
		/// <returns>Doc object</returns>
		private static Doc GetPDF<T>(List<T> dataList, List<string> columnsToRemove, double[] setColumnWidths, int cellPadding, Doc pdfDoc, List<string> resPdfHeaderName)
		{
			pdfDoc.Page = pdfDoc.AddPage();
			pdfDoc.Rect.Inset(Constants.PDFInteractionReportInset, Constants.PDFInteractionReportInset);

			DataTable dt = ExcelHelper.ListToDataTable(dataList, columnsToRemove, false);
			PDFTable pdfTableDt = new PDFTable(pdfDoc, dt.Columns.Count);
			pdfTableDt.SetColumnWidths(setColumnWidths);
			pdfTableDt.CellPadding = cellPadding;
			
			pdfTableDt.AddHtml(GetBoldHeader(resPdfHeaderName));
			pdfTableDt.FrameHeader = true;
			pdfTableDt.RepeatHeader = true;
			bool isColor = true;
			for (int i = 0; i < dt.Rows.Count; i++)
			{
				pdfTableDt.AddHtml(dt.Rows[i].ItemArray);
				if (isColor)
					pdfTableDt.FillRow(Constants.PDFAlternateRowColor, pdfTableDt.Row);
				isColor = !isColor;
			}
			pdfTableDt.Frame();
			return pdfDoc;
		}
	}
}