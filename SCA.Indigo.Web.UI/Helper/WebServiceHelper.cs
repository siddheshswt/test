﻿namespace SCA.Indigo.Web.UI.Helper
{
	using System;
	using System.Configuration;
	using System.Globalization;
	using System.Linq;
	using System.Net.Http;
	using System.Web;
	using System.Web.Mvc;

	using Newtonsoft.Json.Linq;
	using SCA.Indigo.Business.BusinessModels;
	using SCA.Indigo.Common;
	using System.Threading.Tasks;

	/// <summary>
	/// This is a class used to invoke the WCF service.
	/// This is a replacement to ControllerHelper
	/// </summary>
	public class WebServiceHelper
	{
		/// <summary>
		/// BaseUrl
		/// </summary>
		public string BaseUrl { get; set; }

		/// <summary>
		/// ActionName
		/// </summary>
		public string ActionName { get; set; }

		/// <summary>
		/// ActionParam
		/// </summary>
		public string[] ActionParam { get; set; }

		/// <summary>
		/// currentUser
		/// </summary>
		private UsersBusinessModel currentUser;

		/// <summary>
		/// CurrentUser
		/// </summary>
		public UsersBusinessModel CurrentUser
		{
			get
			{
				if (HttpContext.Current.Session[Constants.SessionUser] != null)
				{
					currentUser = (UsersBusinessModel)HttpContext.Current.Session[Constants.SessionUser];
				}
				else
				{
					currentUser = new UsersBusinessModel();
				}

				return currentUser;
			}
			set
			{
				currentUser = value;
				HttpContext.Current.Session[Constants.SessionUser] = currentUser;
			}
		}

        private long currentUserRoleId;

        public long CurrentUserRoleId
        {
            get
            {
                currentUserRoleId = HttpContext.Current.Session[Constants.CurrentUserRoleId] != null ? Convert.ToInt64(HttpContext.Current.Session[Constants.CurrentUserRoleId], CultureInfo.InvariantCulture) : 0;
                return currentUserRoleId;
            }
            set
            {
                currentUserRoleId = value;
                HttpContext.Current.Session[Constants.CurrentUserRoleId] = currentUserRoleId;
            }
        }

		public WebServiceHelper()
		{
			BaseUrl = Convert.ToString(ConfigurationManager.AppSettings["ServiceBaseUrl"], CultureInfo.InvariantCulture);
		}

		public WebServiceHelper(string actionName)
			: this()
		{
			ActionName = actionName;
			ActionParam = null;
		}

		public WebServiceHelper(string actionName, string[] actionParam)
			: this(actionName)
		{
			ActionParam = actionParam;
		}

		#region GetMethods

		/// <summary>
		/// GetMethodServiceRequestObject
		/// </summary>
		/// <param name="contract"></param>
		/// <returns></returns>
		public object GetMethodServiceRequestObject(object contract)
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
				if (ActionParam != null)
				{
					var actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
					objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);
				}

				HttpResponseMessage response;
				if (contract != null)
				{
					response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
				}
				else
				{
					response = objServiceHelper.GetHttpResponse(CurrentUser.UserId);
				}

				object resultContract = null;
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						resultContract = response.Content.ReadAsAsync<object>().Result;
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}
				return resultContract;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}

		/// <summary>
		/// GetMethodServiceRequest
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T GetMethodServiceRequest<T>()
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper();

				var actionParam = string.Empty;
				if (ActionParam != null)
				{
					actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
				}

				objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);

				var response = objServiceHelper.GetHttpResponse(CurrentUser.UserId);
				T responseData = default(T);
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
						if (jObj.First != null && jObj.First.First != null)
						{
							responseData = jObj.First.First.ToObject<T>();
						}
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}
				return responseData;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}
		
		/// <summary>
		/// GetMethodServiceRequestForList
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T GetMethodServiceRequestForList<T>()
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
				var actionParam = string.Empty;
				if (ActionParam != null)
				{
					actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
				}

				objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);
				var response = objServiceHelper.GetHttpResponse(CurrentUser.UserId);
				T responseData = default(T);
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						var result = response.Content.ReadAsStringAsync().Result;
						if (result.StartsWith("{", StringComparison.Ordinal))
						{
							var jObj = JObject.Parse(result);
							if (jObj.First != null && jObj.First.First != null)
							{
								responseData = jObj.First.First.ToObject<T>();
							}
						}
						else if (result.StartsWith("[", StringComparison.Ordinal))
						{
							var jArray = JArray.Parse(result);
							if (jArray.Count > 0)
							{
								responseData = jArray.ToObject<T>();
							}
						}
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}
				return responseData;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}

		/// <summary>
		/// GetMethodServiceRequest
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="contract"></param>
		/// <returns></returns>
		public T GetMethodServiceRequest<T>(object contract)
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper();

				var actionParam = string.Empty;
				if (ActionParam != null)
				{
					actionParam = ActionParam.Aggregate(string.Empty, (current, strparam) => current + ("/" + strparam));
				}

				objServiceHelper.RequestUrl = new Uri(BaseUrl + "/" + ActionName + actionParam);

				var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
				T responseData = default(T);
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						var jObj = JObject.Parse(response.Content.ReadAsStringAsync().Result);
						if (jObj.First != null && jObj.First.First != null)
						{
							responseData = jObj.First.First.ToObject<T>();
						}
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}
				return responseData;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}

		#endregion


		#region PostMethods
		/// <summary>
		/// Method to post the request.
		/// User only in case if return types are bool or string or long ...
		/// </summary>
		/// <param name="contract">The contract.</param>
		/// <returns>object.</returns>
		public object PostMethodServiceRequestObject(object contract)
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
				var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
				object resultContract = null;
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						resultContract = response.Content.ReadAsAsync<object>().Result;
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}

				return resultContract;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}

		/// <summary>
		/// Method to post the request async.        
		/// </summary>
		/// <param name="contract">The contract.</param>        
		public void PostMethodServiceRequestAsync(object contract, string userId)
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
				var response = new HttpResponseMessage();

				Task.Factory.StartNew(() =>
				{
					response = objServiceHelper.GetHttpResponse(contract, Guid.Parse(userId));
				});
			}
			catch (Exception e)
			{
				LogHelper.LogException(e, userId);
			}
		}

		
		/// <summary>
		/// PostMethodServiceRequest
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="contract"></param>
		/// <returns></returns>
		public T PostMethodServiceRequest<T>(T contract)
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
				var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
				T responseData = default(T);
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						responseData = response.Content.ReadAsAsync<T>().Result;
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}
				return responseData;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}

		/// <summary>
		/// Method to post the request.
		/// User if return types are BOM objects
		/// </summary>
		/// <typeparam name="T1">The type of the 1.</typeparam>
		/// <typeparam name="T">The type of T.</typeparam>
		/// <param name="contract">The contract.</param>
		/// <returns>T.</returns>
		public T PostMethodServiceRequest<T1, T>(T1 contract)
		{
			ServiceHelper objServiceHelper = null;
			try
			{
				objServiceHelper = new ServiceHelper { RequestUrl = new Uri(BaseUrl + "/" + ActionName) };
				var response = objServiceHelper.GetHttpResponse(contract, CurrentUser.UserId);
				T responseData = default(T);
				if (response != null)
				{
					if (response.IsSuccessStatusCode)
					{
						var result = response.Content.ReadAsStringAsync().Result;
						if (!string.IsNullOrEmpty(result))
						{
							if (result.StartsWith("{", StringComparison.InvariantCulture))
							{
								var jObj = JObject.Parse(result);
								if (jObj.First != null && jObj.First.First != null)
								{
									responseData = jObj.First.First.ToObject<T>();
								}
							}
							else if (result.StartsWith("[", StringComparison.InvariantCulture))
							{
								var jArray = JArray.Parse(result);
								if (jArray.Count > 0)
								{
									responseData = jArray.ToObject<T>();
								}
							}
						}
					}
					else
					{
						response.EnsureSuccessStatusCode();
					}
				}
				return responseData;
			}
			finally
			{
				if (objServiceHelper != null)
				{
					objServiceHelper.Dispose();
				}
			}
		}



		#endregion
	}
}