﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Gaurav
// Created          : 12-Jun-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="PatientHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.ViewModel;
    using SCA.Indigo.Common.Enums;
    using System.Web.Mvc;
    using SCA.Indigo.Common;

    /// <summary>
    /// Class PatientHelper.
    /// </summary>
    public class PatientHelper
    {
        #region Interaction

        /// <summary>
        /// Gets the interaction summary.
        /// </summary>
        /// <param name="inputInteractionViewModel">The input interaction view model.</param>
        /// <returns>
        /// InteractionViewModel.
        /// </returns>
        public InteractionViewModel GetInteractionSummary(InteractionViewModel inputInteractionViewModel)
        {
            InteractionViewModel returnViewModel = new InteractionViewModel();
            ControllerHelper.ActionName = Constants.ActionNameGetInteractionSummary;
            if (string.IsNullOrEmpty(inputInteractionViewModel.InteractionId))
            {
                inputInteractionViewModel.InteractionId = Constants.Zero;
            }

            if (string.IsNullOrEmpty(inputInteractionViewModel.AssignedTo))
            {
                inputInteractionViewModel.AssignedTo = Constants.DefaultGuid;
            }

            if (string.IsNullOrEmpty(inputInteractionViewModel.PatientId))
            {
                inputInteractionViewModel.PatientId = Constants.Zero;
            }

            if (string.IsNullOrEmpty(inputInteractionViewModel.CarehomeId))
            {
                inputInteractionViewModel.CarehomeId = Constants.Zero;
            }

            if (string.IsNullOrEmpty(inputInteractionViewModel.CustomerId))
            {
                inputInteractionViewModel.CustomerId = Constants.Zero;
            }

            ControllerHelper.ActionParam = new[] { inputInteractionViewModel.PatientId, inputInteractionViewModel.CarehomeId, inputInteractionViewModel.CustomerId, inputInteractionViewModel.InteractionId, inputInteractionViewModel.AssignedTo };

            var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<InteractionBusinessModel>>();

            // Parent Form Integration 
            if (!string.IsNullOrEmpty(inputInteractionViewModel.ParentFormName))
            {
                if (inputInteractionViewModel.ParentFormName.ToUpper(CultureInfo.InvariantCulture) == Constants.PatientContactInfo.ToUpper(CultureInfo.InvariantCulture))
                {
                    returnViewModel.LabelName = Resource.Common.respatientname;
                }
                else
                {
                    returnViewModel.LabelName = Resource.Common.resCarehomeName;
                }
            }
            if (returnListData == null)
            {
                returnViewModel.Count = 0;
            }
            else
            {
                returnViewModel.Count = returnListData.Count;
                returnViewModel.Interactions = new List<InteractionListViewModel>();

                // Home Page Sorting by modified date
                if (!string.IsNullOrEmpty(inputInteractionViewModel.HomePageAlert))
                    returnListData = returnListData.OrderByDescending(q => q.ModifiedDate ?? q.CreatedDate).ThenByDescending(q => q.CreatedDate).ToList();

                var srNo = 0;
                InteractionListViewModel interactionModel = null;
                foreach (var obj in returnListData)
                {
                    interactionModel = new InteractionListViewModel();
                    interactionModel.SrNo = ++srNo;
                    interactionModel.InteractionId = obj.InteractionId;
                    interactionModel.InteractionCategory = obj.InteractionTypeCategory;
                    interactionModel.InteractionSubTypeId = Convert.ToInt64(obj.InteractionSubtypeId, CultureInfo.InvariantCulture);
                    interactionModel.Description = obj.Description;
                    interactionModel.StatusText = obj.Statustext;
                    interactionModel.CreatedOn = obj.CreatedDate.HasValue ? obj.CreatedDate.Value.ToString(Constants.DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
                    interactionModel.LogisticProviderName = obj.LogisticProviderName;
                    if (!string.IsNullOrEmpty(obj.SAPPatientId))
                    {
                        interactionModel.PatientNumber = obj.SAPPatientId.TrimStart(new char[] { '0' });
                    }
                    interactionModel.PatientName = obj.PatientName;
                    interactionModel.CareHomeName = obj.CareHomeName;
                    if (!string.IsNullOrEmpty(obj.SAPCustomerId))
                    {
                        interactionModel.SAPCustomerNumber = obj.SAPCustomerId.TrimStart(new char[] { '0' });
                    }
                    interactionModel.CustomerName = obj.CustomerName;
                    interactionModel.DisplayAsAlert = obj.DisplayAs;
                    interactionModel.InteractionSubTypeCategory = obj.InteractionSubtypeCategory;
                    interactionModel.InteractionId = obj.InteractionId;
                    if (!string.IsNullOrEmpty(obj.SAPCareHomeNumber))
                    {
                        interactionModel.SAPCareHomeNumber = obj.SAPCareHomeNumber.TrimStart(new char[] { '0' });
                    }
                    interactionModel.CreatedBy = obj.CreatedBy;
                    interactionModel.CareHomeName = obj.CareHomeName;
                    interactionModel.ModifiedBy = obj.ModifiedBy;
                    interactionModel.ModifiedOn = obj.ModifiedDate.HasValue ? obj.ModifiedDate.Value.ToString(Constants.DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
                    interactionModel.IsInteractionViewed = inputInteractionViewModel.UserId == obj.AssignedId ? obj.IsInteractionViewed : true;
                    returnViewModel.Interactions.Add(interactionModel);
                }
            }

            return returnViewModel;
        }

        /// <summary>
        /// Opens the interaction.
        /// </summary>
        /// <param name="interactionViewModel">The interaction view model.</param>
        /// <returns>
        /// InteractionViewModel.
        /// </returns>
        public static InteractionViewModel OpenInteraction(InteractionViewModel interactionViewModel)
        {
            if (interactionViewModel != null)
            {
                string interactionId = interactionViewModel.InteractionId;
                if (string.IsNullOrEmpty(interactionViewModel.InteractionId) || interactionViewModel.InteractionId == Constants.Zero)
                {
                    interactionViewModel.LogisticProviderName = GetLogisticProviderName(interactionViewModel.CustomerId);
                    interactionViewModel.IsBtnEnabled = true;
                }
                else
                {
                    ControllerHelper.ActionName = Constants.ActionNameGetCreateInteractionByIntreactionId;
                    interactionViewModel.AssignedTo = Constants.DefaultGuid;
                    ControllerHelper.ActionParam = new[] { interactionViewModel.InteractionId, interactionViewModel.AssignedTo, interactionViewModel.UserId };
                    var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<InteractionBusinessModel>>();
                    if (returnListData != null)
                    {
                        foreach (var data in returnListData)
                        {
                            if (!string.IsNullOrEmpty(data.SAPCustomerId))
                            {
                                interactionViewModel.SAPCustomerId = CommonHelper.TrimLeadingZeros(data.SAPCustomerId);
                            }

                            interactionViewModel.InteractionId = interactionId;
                            interactionViewModel.CustomerId = data.CustomerId;
                            interactionViewModel.CustomerName = data.CustomerName;
                            interactionViewModel.InteractionTypeId = data.InteractionTypeId;
                            interactionViewModel.InteractionSubTypeId = data.InteractionSubtypeId;
                            interactionViewModel.LogisticProviderName = data.LogisticProviderName;
                            interactionViewModel.DisplayOnly = data.DisplayAs != null ? data.DisplayAs.Value : false;
                            interactionViewModel.AssignedTo = data.AssignedId;
                            interactionViewModel.StatusId = Convert.ToString(data.StatusId, CultureInfo.InvariantCulture);
                            interactionViewModel.PatientName = data.PatientName;
                            interactionViewModel.PatientId = data.PatientId;
                            if (!string.IsNullOrEmpty(data.SAPPatientId))
                            {
                                interactionViewModel.SAPPatientId = CommonHelper.TrimLeadingZeros(data.SAPPatientId);
                            }
                            if (!string.IsNullOrEmpty(data.SAPCareHomeNumber))
                            {
                                interactionViewModel.SapCarehomeId = CommonHelper.TrimLeadingZeros(data.SAPCareHomeNumber);
                            }
                            interactionViewModel.CarehomeName = data.CareHomeName;
                            interactionViewModel.CarehomeId = data.CareHomeId;
                            interactionViewModel.Description = data.Description;
                            interactionViewModel.StatusId = data.StatusId.ToString(CultureInfo.InvariantCulture);
                            interactionViewModel.CreatedBy = data.CreatedBy;
                            interactionViewModel.CreatedDate = data.CreatedDate.HasValue ? data.CreatedDate.Value.ToString(Constants.DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
                            interactionViewModel.ModifiedBy = data.ModifiedBy;
                            interactionViewModel.ModifiedDate = data.ModifiedDate.HasValue ? data.ModifiedDate.Value.ToString(Constants.DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
                            interactionViewModel.Resolved = data.Resolved != null ? data.Resolved.Value : false;
                            interactionViewModel.ResolvedBy = data.ResolvedBy;
                            interactionViewModel.ResolvedDate = data.ResolvedDate.HasValue ? data.ResolvedDate.Value.ToString(Constants.DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
                            interactionViewModel.ClosedBy = data.ClosedBy;
                            interactionViewModel.ClosedDate = data.ClosedDate.HasValue ? data.ClosedDate.Value.ToString(Constants.DateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
                            interactionViewModel.IsBtnEnabled = data.IsBtnEnabled;
                        }
                    }
                }

                FillDropDownData(interactionViewModel);
            }
            return interactionViewModel;
        }

        /// <summary>
        /// Fills the drop down data.
        /// </summary>
        /// <param name="interactionViewModel">The interaction view model.</param>
        private static void FillDropDownData(InteractionViewModel interactionViewModel)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            var interactionStatusListTypeId = Convert.ToInt64(SCAEnums.ListType.InteractionStatus, CultureInfo.InvariantCulture);
            var interactionTypeListTypeId = Convert.ToInt64(SCAEnums.ListType.InteractionType, CultureInfo.InvariantCulture);
            ControllerHelper.ActionParam = new[]
            {
                interactionStatusListTypeId + "," + interactionTypeListTypeId,
                string.Empty + Convert.ToInt32(SCAEnums.ListType.LanguageId,CultureInfo.InvariantCulture)
            };

            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            var interactionStatusList = responseData.Where(a => a.ListTypeId == interactionStatusListTypeId).ToList();
            var interactionTypeList = responseData.Where(a => a.ListTypeId == interactionTypeListTypeId).ToList();
            
            ControllerHelper.ActionName = Constants.ActionNameGetAllUser;
            var customerUser = Convert.ToString(true, CultureInfo.InvariantCulture);
            var isInteraction = Convert.ToString(true, CultureInfo.InvariantCulture);
            ControllerHelper.ActionParam = new[] { interactionViewModel.CustomerId, customerUser, ControllerHelper.CurrentUser.UserId.ToString(), isInteraction };
            var userResponse = ControllerHelper.GetMethodServiceRequestForList<List<UsersBusinessModel>>();
            var userlist = new List<UsersBusinessModel>();
            if(userResponse != null)
                userlist = userResponse.ToList();

            if (interactionViewModel.AssignedTo != ControllerHelper.CurrentUser.UserId.ToString())
            {
                var index = userlist.FindIndex(q => q.UserId == ControllerHelper.CurrentUser.UserId);
                if (index > 0 && index < userlist.Count)
                    userlist.RemoveAt(index);
            }

            interactionViewModel.StatusList = new SelectList(interactionStatusList, Constants.ListId, Constants.DisplayText);
            interactionViewModel.InteractionTypeList = new SelectList(interactionTypeList, Constants.ListId, Constants.DisplayText);
            interactionViewModel.AssignedToList = new SelectList(userlist, Constants.UserId, Constants.UserName);
        }

        /// <summary>
        /// Gets the interaction sub type list.
        /// </summary>
        /// <param name="interactionTypeId">The interaction type identifier.</param>
        /// <returns>
        /// SelectList.
        /// </returns>
        public static SelectList GetInteractionSubtypeList(string interactionTypeId)
        {
            var interactionSubTypeId = Convert.ToInt64(SCAEnums.ListType.InteractionSubType, CultureInfo.InvariantCulture);
            var languageId = Convert.ToInt64(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture);
            SelectList interactionSubTypeList = GenericHelper.GetListDataForDropdown(interactionSubTypeId.ToString(CultureInfo.InvariantCulture), interactionTypeId, languageId.ToString(CultureInfo.InvariantCulture));
            return interactionSubTypeList;
        }

        /// <summary>
        /// Saves the interaction details.
        /// </summary>
        /// <param name="interactionViewModel">The interaction view model.</param>
        /// <returns>
        /// Interaction ViewModel.
        /// </returns>
        public InteractionViewModel SaveInteractionDetails(InteractionViewModel interactionViewModel)
        {
            InteractionBusinessModel interactionBusinessModel = new InteractionBusinessModel();
            if (interactionViewModel != null)
            {
                interactionBusinessModel.InteractionId = Convert.ToInt64(interactionViewModel.InteractionId, CultureInfo.InvariantCulture);
                interactionBusinessModel.CustomerId = interactionViewModel.CustomerId;
                interactionBusinessModel.PatientId = interactionViewModel.PatientId;
                interactionBusinessModel.Description = interactionViewModel.Description;
                interactionBusinessModel.InteractionSubtypeId = interactionViewModel.InteractionSubTypeId;
                interactionBusinessModel.UserId = ControllerHelper.CurrentUser.UserId.ToString();
                interactionBusinessModel.StatusId = Convert.ToInt64(interactionViewModel.StatusId, CultureInfo.InvariantCulture);
                interactionBusinessModel.InteractionTypeId = interactionViewModel.InteractionTypeId;                
                interactionBusinessModel.Resolved = interactionViewModel.Resolved;
                
                if (interactionBusinessModel.StatusId == Convert.ToInt64(SCAEnums.InteractionStatus.Completed, CultureInfo.InvariantCulture))
                {
                    interactionBusinessModel.DisplayAs = false;
                    interactionBusinessModel.AssignedId = null;
                }
                else
                {
                    interactionBusinessModel.DisplayAs = interactionViewModel.DisplayAsAlert;
                    interactionBusinessModel.AssignedId = interactionViewModel.AssignedTo;
                }

                interactionBusinessModel.CareHomeId = interactionViewModel.CarehomeId;

                // Save Interaction
                ControllerHelper.ActionName = Constants.ActionNameSaveInteractionDetails;
                var saveInteractionDetailResponse = ControllerHelper.PostMethodServiceRequestObject(interactionBusinessModel);
                if (Convert.ToBoolean(saveInteractionDetailResponse, CultureInfo.InvariantCulture))
                {
                    interactionViewModel.SavedStatusMessage = Resource.Common.resInteractionSavedSuccessfully;
                    interactionViewModel.SavedStatus = 1;
                }
                else
                {
                    interactionViewModel.SavedStatusMessage = Resource.Common.resDataSavedFailedMessage;
                    interactionViewModel.SavedStatus = 0;
                }
            }
            return interactionViewModel;
        }

        /// <summary>
        /// Gets the name of the logistic provider.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// System.String.
        /// </returns>
        public static string GetLogisticProviderName(string customerId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetLogisticProviderName;
            ControllerHelper.ActionParam = new[] { customerId };
            var logisticProviderName = ControllerHelper.GetMethodServiceRequestObject(null);
            return Convert.ToString(logisticProviderName, CultureInfo.InvariantCulture);
        }

        #endregion
    }
}