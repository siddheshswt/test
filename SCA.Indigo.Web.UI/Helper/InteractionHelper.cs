﻿namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.ViewModel;
    using SCA.Indigo.Common.Enums;
    using System.Web.Mvc;

	public class InteractionHelper
    {
		/// <summary>
		/// Private default constructor
		/// </summary>
		private InteractionHelper() { }
        /// <summary>
        /// Fills the drop down data.
        /// </summary>
        /// <param name="interactionViewModel">The interaction view model.</param>
        public static InteractionReportViewModel GetDropDownListValues(InteractionReportViewModel interactionreportViewModel)
        {            
            ControllerHelper.ActionName = Constants.ActionNameGetListType;
            var interactionStatusListTypeId = Convert.ToInt64(SCAEnums.ListType.InteractionStatus, CultureInfo.InvariantCulture);
            var interactionTypeListTypeId = Convert.ToInt64(SCAEnums.ListType.InteractionType, CultureInfo.InvariantCulture);

			ControllerHelper.ActionParam = new[]
            {
                interactionStatusListTypeId + "," + interactionTypeListTypeId,
                string.Empty + Convert.ToInt32(SCAEnums.ListType.LanguageId, CultureInfo.InvariantCulture)
            };

            var responseData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            var interactionStatusList = responseData.Where(a => a.ListTypeId == interactionStatusListTypeId).ToList();
            var interactionTypeList = responseData.Where(a => a.ListTypeId == interactionTypeListTypeId).ToList();

            interactionreportViewModel.StatusList = new SelectList(interactionStatusList, Constants.ListId, Constants.DisplayText, interactionreportViewModel.StatusId);

            interactionreportViewModel.InteractionTypeList = new SelectList(interactionTypeList, Constants.ListId, Constants.DisplayText, interactionreportViewModel.InteractionTypeId);

            interactionreportViewModel.CustomersList = GenericHelper.GetCustomersListForDropdown(interactionreportViewModel.UserId);
            return interactionreportViewModel;
        }
    }
}