﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : gaurav
// Created          : 12-03-2014
//
// Last Modified By : sagar
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="ControllerHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Globalization;
namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Diagnostics;
    using System.Data;
    using System.Reflection;
    using System.IO;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using DocumentFormat.OpenXml;

    /// <summary>
    /// Excel Helper
    /// </summary>
    public class ExcelHelper
    {
        /// <summary>
        /// Private default construction
        /// </summary>
		private ExcelHelper()
		{
		}
		
		#region Common method for Export to Excel Functionality
        /// <summary>
        /// Export to Excel functionality.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exportDataList">The export data list.</param>
        /// <param name="serverDirectory">The server directory.</param>
        /// <param name="fileNamePrefix">The file name prefix.</param>
        /// <param name="columnsToRemove">The columns to remove.</param>
        /// <returns>
        /// file path
        /// </returns>
        public static string ExportToExcel<T>(List<T> exportDataList, string serverDirectory, string fileNamePrefix, List<string> columnsToRemove, bool isTransformBool)
        {
            // set variables 
            var filePath = string.Empty;
            string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);
            string tempPath = GenericHelper.TempAddress;

            string uploadDirectory = GenericHelper.UploadDirectory;
            string downloadUrl = GenericHelper.DownloadUrl;
            
            /// create directory on the server
            Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);

            /// create directory on the virtual directory.
            Common.CommonHelper.CreateDirectoryIfNotExists(tempPath);

            var directory = Path.Combine(serverPath, serverDirectory);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            
            /// build xls file path
            var fileName = fileNamePrefix + Constants.Underscore + Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture) + Constants.Underscore + DateTime.Now.ToString(Constants.DateTimeFormatyy5, CultureInfo.InvariantCulture) + Constants.FileFormat;
            filePath = Path.Combine(serverPath, serverDirectory + '\\' + fileName);

            /// create excel document
            DataSet ds = new DataSet();
            ds.Tables.Add(ListToDataTable(exportDataList, columnsToRemove, isTransformBool));

            var isExcelCreated = CreateExcelDocument(ds, filePath);
            if (isExcelCreated)
            {  
				var filePhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath("\\" + uploadDirectory + "\\" + fileName);
				System.IO.File.Copy(filePath, filePhysicalPath, true);				
                filePath = downloadUrl + '\\' + uploadDirectory + '\\' + fileName;
            }
            if (ds != null)
            {
                ds.Dispose();
            }
            return filePath;
        }

        public static string ExportToExcel(DataTable dt, string serverDirectory, string fileNamePrefix)
        {
            // set variables 
            var filePath = string.Empty;
            string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);
            string tempPath = GenericHelper.TempAddress;

            string uploadDirectory = GenericHelper.UploadDirectory;
            string downloadUrl = GenericHelper.DownloadUrl;

            /// create directory on the server
            Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);

            /// create directory on the virtual directory.
            Common.CommonHelper.CreateDirectoryIfNotExists(tempPath);

            var directory = Path.Combine(serverPath, serverDirectory);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            /// build xls file path
            var fileName = fileNamePrefix + Constants.Underscore + Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture) + Constants.Underscore + DateTime.Now.ToString(Constants.DateTimeFormatyy5, CultureInfo.InvariantCulture) + Constants.FileFormat;
            filePath = Path.Combine(serverPath, serverDirectory + '\\' + fileName);

            /// create excel document
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            var isExcelCreated = CreateExcelDocument(ds, filePath,true);
            if (isExcelCreated)
            {
                var filePhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath("\\" + uploadDirectory + "\\" + fileName);
                System.IO.File.Copy(filePath, filePhysicalPath, true);
                filePath = downloadUrl + '\\' + uploadDirectory + '\\' + fileName;
            }
            if (ds != null)
            {
                ds.Dispose();
            }
            return filePath;
        }

		/// <summary>
		/// Overloaded function of ExportToExcel
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="exportDataList"></param>
		/// <param name="serverDirectory"></param>
		/// <param name="fileNamePrefix"></param>
		/// <param name="columnsToRemove"></param>
		/// <returns></returns>
		public static string ExportToExcel<T>(List<T> exportDataList, string serverDirectory, string fileNamePrefix, List<string> columnsToRemove)
		{
			return ExportToExcel<T>(exportDataList, serverDirectory, fileNamePrefix, columnsToRemove, false); 
		}

        /// <summary>
        /// Lists to data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="columnsToRemove">The columns to remove.</param>
        /// <returns></returns>
        public static DataTable ListToDataTable<T>(List<T> list, List<string> columnsToRemove, bool isTransformBool)
        {
            DataTable dt = new DataTable();
            try
            {
                foreach (PropertyInfo info in typeof(T).GetProperties())
                {
					if (isTransformBool && GetNullableType(info.PropertyType) == typeof(bool))
					{
						dt.Columns.Add(new DataColumn(info.Name, GetNullableType(typeof(string))));
					}
					else
					{
						dt.Columns.Add(new DataColumn(info.Name, GetNullableType(info.PropertyType)));
					}
                }

                foreach (T t in list)
                {
                    DataRow row = dt.NewRow();
                    foreach (PropertyInfo info in typeof(T).GetProperties())
                    {
                        if(isTransformBool && GetNullableType(info.PropertyType) == typeof(bool))
						{
							row[info.Name] = Convert.ToBoolean(info.GetValue(t, null), CultureInfo.InvariantCulture) == true ? "Y" : "N";
						}
						else if (!IsNullableType(info.PropertyType))
						{
							row[info.Name] = info.GetValue(t, null);
						}
						else
						{
							row[info.Name] = (info.GetValue(t, null) ?? DBNull.Value);
						}
                    }
                    dt.Rows.Add(row);

                }

                if (columnsToRemove != null && columnsToRemove.Count > 0)  
                {
                    foreach (var item in columnsToRemove)
                    {
                        dt.Columns.Remove(item);
                    }
                }
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
            return new DataTable();
        }

        /// <summary>
        /// Object to data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="columnsToRemove">The columns to remove.</param>
        /// <returns></returns>
        public static DataTable ObjectToDataTable(object obj, List<string> columnsToRemove, bool isTransformBool, List<string> headerName = null)
        {
            try
            {
                if (obj != null)
                {
                    Type t = obj.GetType();
                    DataTable dt = new DataTable(t.Name);
                    int i = 0;
                    foreach (PropertyInfo info in t.GetProperties())
                    {
                        if (isTransformBool && GetNullableType(info.PropertyType) == typeof(bool))
                        {
                            dt.Columns.Add(new DataColumn(headerName != null && headerName.ElementAtOrDefault(i) != null ? headerName[i] : info.Name,
                                GetNullableType(typeof(string))));
                        }
                        else
                        {
                            dt.Columns.Add(new DataColumn(headerName != null && headerName.ElementAtOrDefault(i) != null ? headerName[i] : info.Name, 
                                GetNullableType(info.PropertyType)));
                        }
                        i++;
                    }

                    i = 0;
                    DataRow dr = dt.NewRow();
                    foreach (PropertyInfo info in t.GetProperties())
                    {
                        if (isTransformBool && GetNullableType(info.PropertyType) == typeof(bool))
                        {
                            dr[headerName != null && headerName.ElementAtOrDefault(i) != null ? headerName[i] : info.Name] = 
                                Convert.ToBoolean(info.GetValue(obj, null), CultureInfo.InvariantCulture) == true ? "Y" : "N";
                        }
                        else if (!IsNullableType(info.PropertyType))
                        {
                            dr[headerName != null && headerName.ElementAtOrDefault(i) != null ? headerName[i] : info.Name] = info.GetValue(obj, null);
                        }
                        else
                        {
                            dr[headerName != null && headerName.ElementAtOrDefault(i) != null ? headerName[i] : info.Name] = (info.GetValue(obj, null) ?? DBNull.Value);
                        }
                        i++;
                    }
                    dt.Rows.Add(dr);

                    if (columnsToRemove != null && columnsToRemove.Count > 0)
                    {
                        foreach (var item in columnsToRemove)
                        {
                            dt.Columns.Remove(item);
                        }
                    }

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        /// <summary>
        /// Get nullable type
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>
        /// nullable type
        /// </returns>
        private static Type GetNullableType(Type t)
        {
            Type returnType = t;
            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                returnType = Nullable.GetUnderlyingType(t);
            }
            return returnType;
        }

        /// <summary>
        /// check if nullable
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// nullable type
        /// </returns>
        private static bool IsNullableType(Type type)
        {
            return (type == typeof(string) ||
                    type.IsArray ||
                    (type.IsGenericType &&
                     type.GetGenericTypeDefinition().Equals(typeof(Nullable<>))));
        }

        #endregion
     
        #region Create and Write in the excel file

        /// <summary>
        /// Create an Excel file, and write it to a file.
        /// </summary>
        /// <param name="ds">DataSet containing the data to be written to the Excel.</param>
        /// <param name="excelFilename">Name of file to be written.</param>
        /// <returns>
        /// True if successful, false if something went wrong.
        /// </returns>
        public static bool CreateExcelDocument(DataSet ds, string excelFilename, bool isRemoveHeading = false)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(excelFilename, SpreadsheetDocumentType.Workbook))
                {
                    WriteExcelFile(ds, document, isRemoveHeading);
                }
                Trace.WriteLine("Successfully created: " + excelFilename);
                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Create an Excel file, and write it to a file.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <param name="xlsxFilePath">The XLSX file path.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public static bool CreateExcelDocument(DataTable dt, string xlsxFilePath)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            bool result = CreateExcelDocument(ds, xlsxFilePath);
            ds.Tables.Remove(dt);
            if (ds != null)
            {
                ds.Dispose();
            }
            return result;
        }

        /// <summary>
        /// Main function to write data in Excel file
        /// </summary>
        /// <param name="ds">The ds.</param>
        /// <param name="spreadsheet">The spreadsheet.</param>
        private static void WriteExcelFile(DataSet ds, SpreadsheetDocument spreadsheet, bool isRemoveHeading = false)
        {
            ///  Create the Excel file contents.  This function is used when creating an Excel file either writing 
            ///  to a file, or writing to a MemoryStream.
            spreadsheet.AddWorkbookPart();
            spreadsheet.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            spreadsheet.WorkbookPart.Workbook.Append(new BookViews(new WorkbookView()));

            ///  If we don't add a "WorkbookStylesPart", OLEDB will refuse to connect to this .xlsx file !
            WorkbookStylesPart workbookStylesPart = spreadsheet.WorkbookPart.AddNewPart<WorkbookStylesPart>("rIdStyles");
            Stylesheet stylesheet = new Stylesheet();
            workbookStylesPart.Stylesheet = stylesheet;

            ///  Loop through each of the DataTables in our DataSet, and create a new Excel Worksheet for each.
            uint worksheetNumber = 1;
            foreach (DataTable dt in ds.Tables)
            {
                ///  For each worksheet you want to create
                WorksheetPart newWorksheetPart = spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
                newWorksheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet();

                /// create sheet data
                newWorksheetPart.Worksheet.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.SheetData());

                /// save worksheet
                WriteDataTableToExcelWorksheet(dt, newWorksheetPart,isRemoveHeading);
                
                newWorksheetPart.Worksheet.Save();

                /// create the worksheet to workbook relation
                if (worksheetNumber == 1)
                    spreadsheet.WorkbookPart.Workbook.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheets());

                spreadsheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>().AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheet()
                {
                    Id = spreadsheet.WorkbookPart.GetIdOfPart(newWorksheetPart),
                    SheetId = (uint)worksheetNumber,
                    Name = dt.TableName
                });

                worksheetNumber++;
            }
            spreadsheet.WorkbookPart.Workbook.Save();
        }

        /// <summary>
        /// Sub function to write data table to excel worksheet.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <param name="worksheetPart">The worksheet part.</param>
        private static void WriteDataTableToExcelWorksheet(DataTable dt, WorksheetPart worksheetPart,bool isRemoveHeading = false)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();

            var numericTypes = new[] { typeof(Byte), typeof(Decimal), typeof(Double),
        typeof(Int16), typeof(Int32), typeof(Int64), typeof(SByte),
        typeof(Single), typeof(UInt16), typeof(UInt32), typeof(UInt64)};

            string cellValue = "";

            ///  Create a Header Row in our Excel file, containing one header for each Column of data in our DataTable.     
            ///  We'll also create an array, showing which type each column of data is (Text or Numeric), so when we come to write the actual
            ///  cells of data, we'll know if to write Text values or Numeric cell values.
            int numberOfColumns = dt.Columns.Count;
            bool[] IsNumericColumn = new bool[numberOfColumns];

            string[] excelColumnNames = new string[numberOfColumns];
            for (int n = 0; n < numberOfColumns; n++)
                excelColumnNames[n] = GetExcelColumnName(n);

            ///  Create the Header row in our Excel Worksheet
            uint rowIndex = 0;

            if (!isRemoveHeading)
            {
                ++rowIndex;
                var headerRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
                sheetData.Append(headerRow);

                for (int colInx = 0; colInx < numberOfColumns; colInx++)
                {
                    DataColumn col = dt.Columns[colInx];
                    AppendTextCell(excelColumnNames[colInx] + "1", col.ColumnName, headerRow);
                    IsNumericColumn[colInx] = numericTypes.Contains(col.DataType);
                }
            }

            ///  Now, step through each row of data in our DataTable...            
            double cellNumericValue = 0;
            foreach (DataRow dr in dt.Rows)
            {
                /// ...create a new row, and append a set of this row's data to it.
                ++rowIndex;
                var newExcelRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
                sheetData.Append(newExcelRow);

                for (int colInx = 0; colInx < numberOfColumns; colInx++)
                {
                    cellValue = dr.ItemArray[colInx].ToString();

                    /// Create cell with data
                    if (IsNumericColumn[colInx])
                    {
                        ///  For numeric cells, make sure our input data IS a number, then write it out to the Excel file.
                        ///  If this numeric value is NULL, then don't write anything to the Excel file.
                        cellNumericValue = 0;
                        if (double.TryParse(cellValue, out cellNumericValue))
                        {
                            cellValue = cellNumericValue.ToString(CultureInfo.InvariantCulture);
                            AppendNumericCell(excelColumnNames[colInx] + rowIndex.ToString(CultureInfo.InvariantCulture), cellValue, newExcelRow);
                        }
                    }
                    else
                    {
                        ///  For text cells, just write the input data straight out to the Excel file.
                        AppendTextCell(excelColumnNames[colInx] + rowIndex.ToString(CultureInfo.InvariantCulture), cellValue, newExcelRow);
                    }
                }
            }
        }

        /// <summary>
        /// Append text into cell
        /// </summary>
        /// <param name="cellReference">The cell reference.</param>
        /// <param name="cellStringValue">The cell string value.</param>
        /// <param name="excelRow">The excel row.</param>
        private static void AppendTextCell(string cellReference, string cellStringValue, Row excelRow)
        {
            //  Add a new Excel Cell to our Row 
            Cell cell = new Cell() { CellReference = cellReference, DataType = CellValues.String };
            CellValue cellValue = new CellValue();
            cellValue.Text = cellStringValue;
            cell.Append(cellValue);
            excelRow.Append(cell);
        }

        /// <summary>
        /// append numeric into cell
        /// </summary>
        /// <param name="cellReference">The cell reference.</param>
        /// <param name="cellStringValue">The cell string value.</param>
        /// <param name="excelRow">The excel row.</param>
        private static void AppendNumericCell(string cellReference, string cellStringValue, Row excelRow)
        {
            //  Add a new Excel Cell to our Row 
            Cell cell = new Cell() { CellReference = cellReference, DataType = CellValues.Number };
            CellValue cellValue = new CellValue();
            cellValue.Text = cellStringValue;
            cell.Append(cellValue);
            excelRow.Append(cell);
        }

        /// <summary>
        /// Get excel column name
        /// </summary>
        /// <param name="columnIndex">Index of the column.</param>
        /// <returns></returns>
        private static string GetExcelColumnName(int columnIndex)
        {
            //  Convert a zero-based column index into an Excel column reference  (A, B, C.. Y, Y, AA, AB, AC... AY, AZ, B1, B2..)
            if (columnIndex < 26)
                return ((char)('A' + columnIndex)).ToString(CultureInfo.InvariantCulture);

            char firstChar = (char)('A' + (columnIndex / 26) - 1);
            char secondChar = (char)('A' + (columnIndex % 26));

            return string.Format(CultureInfo.InvariantCulture, "{0}{1}", firstChar, secondChar);
        }

        /// <summary>
        /// Creates Header Detail Datatable
        /// </summary>
        /// <param name="hdr">Header Section</param>
        /// <param name="dtl">Detail Section</param>
        /// <param name="hdrRow">Header Row</param>
        /// <param name="hdrCol">Header Columns</param>
        /// <returns>Merge of Header & details</returns>
        public static DataTable MergeHeaderDetail(DataTable hdr, DataTable dtl, Int32 hdrRow, Int32 hdrCol)
        {
            DataTable dt = new DataTable();
            Int32 colCount = 0;

            if (hdrCol * 3 > dtl.Columns.Count)
                colCount = hdrCol * 3;
            else
                colCount = dtl.Columns.Count;

            //Adds columns in new table 
            for (int i = 0; i < colCount; i++)
            {
                dt.Columns.Add(new DataColumn(i.ToString(), typeof(string)));
            }

            int hdrColC = 0;
            foreach (DataRow dr in hdr.Rows)
            {
                for (int row = 0; row < hdrRow; row++)
                {
                    DataRow newrow = dt.NewRow();
                    for (int col = 0; col < hdrCol * 3; col = col + 3)
                    {
                        newrow[col] = dr.Table.Columns[hdrColC].ColumnName.ToString();
                        newrow[col + 1] = dr[hdrColC].ToString();
                        newrow[col + 2] = " ";
                        hdrColC++;
                        if (hdrColC >= hdr.Columns.Count)
                            break;
                    }
                    dt.Rows.Add(newrow);
                    dt.Rows.Add(dt.NewRow());
                }
            }

            DataRow dtlHeading = dt.NewRow();
            for (int i = 0; i < colCount; i++)
            {
                dtlHeading[i] = dtl.Columns[i].ColumnName.ToString();
            }
            dt.Rows.Add(dtlHeading);

            foreach (DataRow dr in dtl.Rows)
            {
                dt.Rows.Add(dr.ItemArray);
            }
            return dt;
        }
        #endregion
    }
}
