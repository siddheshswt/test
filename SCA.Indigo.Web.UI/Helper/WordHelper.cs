﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Prafull
// Created          : 23-05-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="WordHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Web.UI.Helper
{
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Common;
    using Spire.Doc;
    using Spire.Doc.Documents;
    using Spire.Doc.Fields;
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;

    /// <summary>
    /// Word Helper
    /// </summary>
    public class WordHelper
    {
		/// <summary>
		/// Private Default Constructor
		/// </summary>
        private WordHelper()
		{
		}
		
		/// <summary>
        /// this function will generate download url(filePath) for word document 
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">object of SampleOrderPatientBusinessModel class</param>
        /// <param name="serverDirectory">the server directory</param>
        /// <param name="fileNamePrefix">the file name prefix</param>
        /// <returns>string variable filePath</returns>
        public static string CreateWordFile(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel, string serverDirectory, string fileNamePrefix)
        {
            // set variables 
            string filePath = string.Empty;
            string serverPath = @"\\" + Path.Combine(GenericHelper.IPAddress, GenericHelper.ServerAddress);
            string tempPath = GenericHelper.TempAddress;
            string uploadDirectory = GenericHelper.UploadDirectory;
            string downloadUrl = GenericHelper.DownloadUrl;
            string backSlash = "\\";
            /// create directory on the server
            Common.CommonHelper.CreateUploadDirectoryIfNotExists(serverPath, uploadDirectory);

            /// create directory on the virtual directory.
            Common.CommonHelper.CreateDirectoryIfNotExists(tempPath);

            var directory = Path.Combine(serverPath, serverDirectory);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            /// build word file path
            var fileName = fileNamePrefix + Constants.Underscore + Convert.ToString(ControllerHelper.CurrentUser.UserId, CultureInfo.InvariantCulture) + Constants.Underscore + DateTime.Now.ToString(Constants.DateTimeFormatyy5, CultureInfo.InvariantCulture) + Constants.WordFileFormat;
            filePath = Path.Combine(serverPath, serverDirectory + backSlash + fileName);

            /// create word document
            var isExcelCreated = CreateWordDocument(sampleOrderPatientBusinessModel,filePath);
            if (isExcelCreated)
            {
                var filePhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath(backSlash + uploadDirectory + backSlash + fileName);
                System.IO.File.Copy(filePath, filePhysicalPath, true);
                filePath = downloadUrl + backSlash + uploadDirectory + backSlash + fileName;
            }
            return filePath;
        }

        /// <summary>
        /// this function will generate word document
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">object of SampleOrderPatientBusinessModel class</param>
        /// <param name="wordFilePath">file path for word document</param>
        /// <returns>true/false</returns>
        private static bool CreateWordDocument(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel, string wordFilePath)
        {
            string userid = ControllerHelper.CurrentUser.UserId.ToString();
            //string imagepath = "~/images/tana-delivery-logo-word_04.png";
            string footerStyle = "Footer Style";
            string fontName = "Calibri";
            string pagenumber = "page number";
            string numberofpages = "number of pages";
            
            try
            {
                if (sampleOrderPatientBusinessModel.SampleProductBusinessModel != null)
                {
                    var sampleProductBusinessModel = sampleOrderPatientBusinessModel.SampleProductBusinessModel;

                    //Create word document  
                    using (Document document = new Document())
                    {
                        Section section = document.AddSection();
                        section.PageSetup.Orientation = PageOrientation.Portrait;
                        
                        //Insert Footer
                        HeaderFooter footer = section.HeadersFooters.Footer;
                        Paragraph footerParagraph = footer.AddParagraph();
                        footerParagraph.AppendField(pagenumber, FieldType.FieldPage);
                        footerParagraph.AppendText("/");
                        footerParagraph.AppendField(numberofpages, FieldType.FieldNumPages);
                        
                        //Format
                        ParagraphStyle style = new ParagraphStyle(document);
                        style.Name = footerStyle;
                        style.CharacterFormat.FontName = fontName;
                        style.CharacterFormat.FontSize = 11;
                        document.Styles.Add(style);
                        footerParagraph.ApplyStyle(style.Name);
                        footerParagraph.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Right;

                        //new paragraph
                        Paragraph paragraph = section.AddParagraph();
                        paragraph.AppendText(Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                        if (!string.IsNullOrEmpty(sampleOrderPatientBusinessModel.Title))
                            paragraph.AppendText(sampleOrderPatientBusinessModel.Title + " ");
                        paragraph.AppendText(sampleOrderPatientBusinessModel.FirstName + " " + sampleOrderPatientBusinessModel.LastName + Environment.NewLine);
                        if (!string.IsNullOrEmpty(sampleOrderPatientBusinessModel.SampleHouseName))
                            paragraph.AppendText(sampleOrderPatientBusinessModel.SampleHouseName + Environment.NewLine);
                        paragraph.AppendText(sampleOrderPatientBusinessModel.Address1 + Environment.NewLine);
                        if (!string.IsNullOrEmpty(sampleOrderPatientBusinessModel.Address2))
                            paragraph.AppendText(sampleOrderPatientBusinessModel.Address2 + Environment.NewLine);
                        paragraph.AppendText(sampleOrderPatientBusinessModel.TownOrCity + Environment.NewLine);
                        paragraph.AppendText(!string.IsNullOrEmpty(sampleOrderPatientBusinessModel.CountyText)? sampleOrderPatientBusinessModel.CountyText + ", " + sampleOrderPatientBusinessModel.PostCode : sampleOrderPatientBusinessModel.PostCode);

                        //new paragraph
                        paragraph = section.AddParagraph();
                        paragraph.AppendText(Environment.NewLine + Environment.NewLine + Environment.NewLine);
                        paragraph.AppendText(Resource.Common.resSampleLetterPara + " " + sampleOrderPatientBusinessModel.FirstName + " " + sampleOrderPatientBusinessModel.LastName + ",");

                        //new paragraph
                        paragraph = section.AddParagraph();
                        paragraph.AppendText(Environment.NewLine);
                        TextRange txtRange = paragraph.AppendText(Resource.Common.resSampleLetterDefaultText);
                        txtRange.CharacterFormat.Bold = false;

                        //new paragraph
                        paragraph = section.AddParagraph();
                        if (sampleProductBusinessModel.Any())
                        {
                            StringBuilder productList = new StringBuilder();
                            productList.Append("<ol>");
                            for (int i = 0; i < sampleProductBusinessModel.Count; i++)
                            {
                                var productDetails = sampleProductBusinessModel[i];
                                productList.Append("<li>" + productDetails.HelixProductId + " - " + productDetails.DescriptionUI + " - " + productDetails.MaxQuantity + Resource.Common.resQuntityText + "</li>");
                            }
                            productList.Append("</ol>");
                            paragraph.AppendHTML(productList.ToString());
                            //set the Spacing Before
                            paragraph.Format.BeforeSpacing = 20;
                        }
                        //Save doc file.
                        document.SaveToFile(wordFilePath, FileFormat.Doc);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, userid);
                throw;
            }
        }
    }
}