﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Siddhesh
// Created          : 12-03-2014
//
// Last Modified By : mamatha
// Last Modified On : 01-09-2015
// ***********************************************************************
// <copyright file="CustomerHelper.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Web.UI.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using System.IO;
    using SCA.Indigo.Business.BusinessModels;
    using SCA.Indigo.Web.UI.ViewModel;
    using SCA.Indigo.Web.UI.ViewModel.UserAdmin;
    using SCA.Indigo.Common.Enums;
    using System.Data;
    using SCA.Indigo.Web.UI.Resource;

    /// <summary>
    /// Class CustomerHelper.
    /// </summary>
    public class CustomerHelper
    {
        #region Constants

        /// <summary>
        /// The user identifier
        /// </summary>
        private string globalUserId = ControllerHelper.CurrentUser.UserId.ToString();

        #endregion

        #region Contact Info

        /// <summary>
        /// Gets the contact information details.
        /// </summary>
        /// <param name="patientContactInfoBusinessModel">The patient contact information business model.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="totalPages">The total pages.</param>
        /// <returns>
        /// List&lt;PatientContactInfoBusinessModel&gt;.
        /// </returns>
        public List<PatientContactInfoBusinessModel> GetContactInfoDetails(PatientContactInfoBusinessModel patientContactInfoBusinessModel, out int totalRecords, out int totalPages)
        {
            ControllerHelper.ActionName = Constants.ActionGetPatientInfo;
            var patientContactInfoData = ControllerHelper.PostMethodServiceRequest<PatientContactInfoBusinessModel, List<PatientContactInfoBusinessModel>>(patientContactInfoBusinessModel);
            if (patientContactInfoData == null)
            {
                patientContactInfoData = new List<PatientContactInfoBusinessModel>();
            }

            List<PatientContactInfoBusinessModel> gridData = new List<PatientContactInfoBusinessModel>();
            if (patientContactInfoData != null)
            {
                gridData = patientContactInfoData.Skip((patientContactInfoBusinessModel.Page - 1) * patientContactInfoBusinessModel.RowCount).Take(patientContactInfoBusinessModel.RowCount).ToList();
                switch (patientContactInfoBusinessModel.SortColumn.ToLower(CultureInfo.InvariantCulture))
                {
                    case "patientname":
                        if (patientContactInfoBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            gridData = gridData.OrderBy(x => x.PatientName).ToList();
                        else
                            gridData = gridData.OrderByDescending(x => x.PatientName).ToList();
                        break;
                    case "jobtitle":
                        if (patientContactInfoBusinessModel.SortType.ToLower() == "asc")
                            gridData = gridData.OrderBy(x => x.JobTitle).ToList();
                        else
                            gridData = gridData.OrderByDescending(x => x.JobTitle).ToList();
                        break;
                    case "telephonenumber":
                        if (patientContactInfoBusinessModel.SortType.ToLower() == "asc")
                            gridData = gridData.OrderBy(x => x.TelephoneNumber).ToList();
                        else
                            gridData = gridData.OrderByDescending(x => x.TelephoneNumber).ToList();
                        break;
                    case "mobilenumber":
                        if (patientContactInfoBusinessModel.SortType.ToLower() == "asc")
                            gridData = gridData.OrderBy(x => x.MobileNumber).ToList();
                        else
                            gridData = gridData.OrderByDescending(x => x.MobileNumber).ToList();
                        break;
                    case "email":
                        if (patientContactInfoBusinessModel.SortType.ToLower() == "asc")
                            gridData = gridData.OrderBy(x => x.Email).ToList();
                        else
                            gridData = gridData.OrderByDescending(x => x.Email).ToList();
                        break;
                    default:
                        gridData = gridData.OrderBy(x => x.PatientName).ToList();
                        break;
                }
            }

            totalRecords = patientContactInfoData != null ? patientContactInfoData.Count : 0;
            totalPages = (int)Math.Ceiling((double)patientContactInfoData.Count / (double)patientContactInfoBusinessModel.RowCount);
            return gridData;
        }

        /// <summary>
        /// Saves the patient contact information.
        /// </summary>
        /// <param name="patientContactInfoBusinessModel">The patient contact information business model.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public bool SavePatientContactInfo(PatientContactInfoBusinessModel patientContactInfoBusinessModel)
        {
            ControllerHelper.ActionName = Constants.ActionSaveContactInfo;
            var response = ControllerHelper.GetMethodServiceRequestObject(patientContactInfoBusinessModel);
            return Convert.ToBoolean(response, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Saves the patient contact information for Copy Patient.
        /// </summary>
        /// <param name="patientContactInfoBusinessModel">The patient contact information business model.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public bool SavePatientContactInfoForCopyPatient(List<PatientContactInfoBusinessModel> patientContactInfoBusinessModel)
        {
            ControllerHelper.ActionName = Constants.ActionSaveContactInfoForCopyPatient;
            var response = ControllerHelper.GetMethodServiceRequestObject(patientContactInfoBusinessModel);
            return Convert.ToBoolean(response, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Removes the contact information.
        /// </summary>
        /// <param name="patientContactInfoBusinessModel">The patient contact information business model.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public bool RemoveContactInfo(PatientContactInfoBusinessModel patientContactInfoBusinessModel)
        {
            ControllerHelper.ActionName = Constants.ActionRemoveContactInfo;
            var response = ControllerHelper.GetMethodServiceRequestObject(patientContactInfoBusinessModel);
            return Convert.ToBoolean(response, CultureInfo.InvariantCulture);
        }

        #endregion

        #region Customer Information

        /// <summary>
        /// Saves the customer parameter.
        /// </summary>
        /// <param name="customerParameter">The customer parameter.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public bool SaveCustomerParameter(CustomerParameterBusinessModel customerParameter)
        {
            ControllerHelper.ActionName = Constants.ActionCPSaveCustomerParameter;
            var response = (bool)ControllerHelper.PostMethodServiceRequestObject(customerParameter);
            return response;
        }

        /// <summary>
        /// Saves the customer parameter.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="reasonCodeId">The reason code identifier.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.
        /// </returns>
        public bool RemoveReasonCodeMapping(string customerId, string reasonCodeId)
        {
            ControllerHelper.ActionName = Constants.ActionCPRemoveReasonCodeMapping;
            ControllerHelper.ActionParam = new[] { customerId, reasonCodeId, globalUserId };
            var response = ControllerHelper.GetMethodServiceRequest<bool>();

            return response;
        }

        /// <summary>
        /// Gets the customer parameter.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// CustomerParameterViewModel.
        /// </returns>
        public CustomerParameterViewModel GetCustomerParameter(string customerId)
        {
            var customerParameterViewModel = new CustomerParameterViewModel()
            {
                CutOffHoursList = GenericHelper.GetHoursOrMinuteSelectList(Constants.HourFormat2),
                CutOffMinutesList = GenericHelper.GetHoursOrMinuteSelectList(Constants.MinuteFormat1),
                CutOffSecondsList = GenericHelper.GetHoursOrMinuteSelectList(Constants.MinuteFormat1)
            };
            if (string.IsNullOrEmpty(customerId))
            {
                return customerParameterViewModel;
            }
            ControllerHelper.ActionName = Constants.ActionCPGetCustomerParameter;
            ControllerHelper.ActionParam = new[] { customerId, globalUserId };
            var customerParameter = ControllerHelper.GetMethodServiceRequest<CustomerParameterBusinessModel>();
            if (customerParameter != null)
            {

                customerParameterViewModel.CustomerID = customerParameter.CustomerID;
                customerParameterViewModel.CustomerName = customerParameter.CustomerName;
                customerParameterViewModel.SAPCustomerID = customerParameter.SAPCustomerID;
                customerParameterViewModel.OrderLeadTime = customerParameter.OrderLeadTime;
                customerParameterViewModel.IsOrderCreationAllowed = customerParameter.OrderCreationAllowed;

                TimeSpan cutOffTime = TimeSpan.Parse(customerParameter.OrderCutOffTime, CultureInfo.InvariantCulture);
                customerParameterViewModel.CutOffHour = cutOffTime.Hours.ToString("00", CultureInfo.InvariantCulture);
                customerParameterViewModel.CutOffMinute = cutOffTime.Minutes.ToString("00", CultureInfo.InvariantCulture);
                customerParameterViewModel.CutOffSecond = cutOffTime.Seconds.ToString("00", CultureInfo.InvariantCulture);

                customerParameterViewModel.AgeBillTo = customerParameter.CheckAge;
                customerParameterViewModel.PaedAgeLimit = customerParameter.Age;
                customerParameterViewModel.MaxPPD = customerParameter.MaxPadPerDay;
                customerParameterViewModel.DefaultNextAssessmentDate = customerParameter.NextAssessmentDate;
                customerParameterViewModel.IsNDDAllow = customerParameter.IsNDDAllow;
                customerParameterViewModel.AllowIncreaseOrderQuantity = customerParameter.AllowIncreaseOrderQty;
                customerParameterViewModel.IsCollectionApplicable = customerParameter.IsCollectionApplicable;

                //Mandatory fields
                customerParameterViewModel.IsADPMandatory = customerParameter.IsADPMandatory;
                customerParameterViewModel.IsNHSIDMandatory = customerParameter.IsNHSIDMandatory;
                customerParameterViewModel.IsTelephoneMandatory = customerParameter.IsTelephoneMandatory;
                customerParameterViewModel.IsNextAssessmentDateMandatory = customerParameter.IsNextAssessmentDateMandatory;

                customerParameterViewModel.MedicalStaff = customerParameter.MedicalAnalysisInformation.Where(q => q.ListTypeId == (long)SCAEnums.ListType.MedicalStaff).ToList();
                customerParameterViewModel.AnalysisInformation = customerParameter.MedicalAnalysisInformation.Where(q => q.ListTypeId == (long)SCAEnums.ListType.AnalysisInformation).ToList();
                customerParameterViewModel.RemovePatientReasonCodes = customerParameter.RemovePatientReasonCodes;
                customerParameterViewModel.StopPatientReasonCodes = customerParameter.StopPatientReasonCodes;
                customerParameterViewModel.IsClinicalContactWithLinkage = customerParameter.IsClinicalContactWithLinkage;
                customerParameterViewModel.SampleOrderProductType = customerParameter.SampleOrderProductType;
            }
            return customerParameterViewModel;
        }

        /// <summary>
        /// Exports the customer parameter.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="isExportToExcel">if set to <c>true</c> [is export to excel].</param>
        /// <returns>Customer Parameter Details</returns>
        public dynamic ExportCustomerParameter(string customerId, bool isExportToExcel)
        {
            DataTable exportTable = new DataTable();
            CustomerParameterViewModel customerParameterViewModel = this.GetCustomerParameter(customerId);

            if (customerParameterViewModel != null)
            {
                string column = " ";
                string isActive = string.Empty;
                string isMandatory = string.Empty;
                for (int i = 1; i <= 5; i++)
                {
                    exportTable.Columns.Add(column);
                    column += column;
                }

                exportTable.Rows.Add(CreateDataRow(exportTable, true, Common.resCustomerParameter));
                exportTable.Rows.Add(CreateDataRow(exportTable));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.resCustomerName, customerParameterViewModel.CustomerName, Common.resGeneralCustomerId, customerParameterViewModel.SAPCustomerID));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblAllowOrderCreation, GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsOrderCreationAllowed), Common.reslblAgeBillTo, GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.AgeBillTo)));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblOrderCreationLeadTime, Convert.ToString(customerParameterViewModel.OrderLeadTime, CultureInfo.InvariantCulture), Common.reslblPaedAgeLimit, Convert.ToString(customerParameterViewModel.PaedAgeLimit, CultureInfo.InvariantCulture)));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblOrderCutoffTime, customerParameterViewModel.CutOffHour + " hr : " + customerParameterViewModel.CutOffMinute + " min : " + customerParameterViewModel.CutOffSecond + " sec", Common.resCPAllowNDD, GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsNDDAllow)));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.resCPCollectionApplicable, GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsCollectionApplicable), Common.resCPAllowIncreaseOrderQty, GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.AllowIncreaseOrderQuantity)));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.resCPSampleOrders, (Convert.ToString(customerParameterViewModel.SampleOrderProductType)).ToUpper() == "CUSTOMER" ? Common.resCPCustomerProductList : Common.resCPTENANetList));
                exportTable.Rows.Add(CreateDataRow(exportTable));
                exportTable.Rows.Add(CreateDataRow(exportTable, true, Common.resCPPatientRemovalReasonCode));
                if (customerParameterViewModel.RemovePatientReasonCodes != null)
                {
                    foreach (var item in customerParameterViewModel.RemovePatientReasonCodes)
                    {
                        exportTable.Rows.Add(CreateDataRow(exportTable, false, item.DisplayText, GenericHelper.ConvertBoolToYesNo(item.IsActive)));
                    }
                }

                exportTable.Rows.Add(CreateDataRow(exportTable));
                exportTable.Rows.Add(CreateDataRow(exportTable, true, Common.resCPPatientStoppedReasonCode));
                if (customerParameterViewModel.StopPatientReasonCodes != null)
                {
                    foreach (var item in customerParameterViewModel.StopPatientReasonCodes)
                    {
                        exportTable.Rows.Add(CreateDataRow(exportTable, false, item.DisplayText, GenericHelper.ConvertBoolToYesNo(item.IsActive)));
                    }
                }

                exportTable.Rows.Add(CreateDataRow(exportTable));
                exportTable.Rows.Add(CreateDataRow(exportTable, true, Common.resHeaderPatientAnalysisInfo));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, string.Empty, "Active", "Mandatory"));
                if (customerParameterViewModel.AnalysisInformation != null)
                {
                    foreach (var item in customerParameterViewModel.AnalysisInformation)
                    {
                        isActive = GenericHelper.ConvertBoolToYesNo(item.IsActive);
                        isMandatory = GenericHelper.ConvertBoolToYesNo(item.IsMandatory);
                        exportTable.Rows.Add(CreateDataRow(exportTable, false, item.DisplayText, isActive, isMandatory));
                    }
                }

                exportTable.Rows.Add(CreateDataRow(exportTable));
                exportTable.Rows.Add(CreateDataRow(exportTable, true, Common.resHeaderPatientMedicalStaff));
                string withLinkage = Common.resLblWithLinkage;
                string withoutLinkage = Common.resLblWithoutLinkage;
                if (customerParameterViewModel.IsClinicalContactWithLinkage)
                {
                    withLinkage += ": " + Common.resGeneralYes;
                    withoutLinkage += ": " + Common.resGeneralNo;
                }
                else
                {
                    withLinkage += ": " + Common.resGeneralNo;
                    withoutLinkage += ": " + Common.resGeneralYes;
                }
                exportTable.Rows.Add(CreateDataRow(exportTable, false, withLinkage, withoutLinkage));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, string.Empty, "Active", "Mandatory"));
                if (customerParameterViewModel.MedicalStaff != null)
                {
                    foreach (var item in customerParameterViewModel.MedicalStaff)
                    {
                        isActive = GenericHelper.ConvertBoolToYesNo(item.IsActive);
                        isMandatory = GenericHelper.ConvertBoolToYesNo(item.IsMandatory);
                        exportTable.Rows.Add(CreateDataRow(exportTable, false, item.DisplayText, isActive, isMandatory));
                    }
                }

                exportTable.Rows.Add(CreateDataRow(exportTable));
                exportTable.Rows.Add(CreateDataRow(exportTable, true, Common.reslblMandatoryField));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblCPPatient, Common.reslblCPPrescription, Common.reslblDefaultRules));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblNHSID + ": " + GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsNHSIDMandatory), Common.reslblMaxPPD + ": " + customerParameterViewModel.MaxPPD, Common.reslblDefaultNextAssessmentDt + ": " + customerParameterViewModel.DefaultNextAssessmentDate));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblCPADP + ": " + GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsADPMandatory)));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblTelephoneNumber + ": " + GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsTelephoneMandatory)));
                exportTable.Rows.Add(CreateDataRow(exportTable, false, Common.reslblCPNextAssessmentDate + ": " + GenericHelper.ConvertBoolToYesNo(customerParameterViewModel.IsNextAssessmentDateMandatory)));

                if (!isExportToExcel)
                {
                    List<PrintCustomerParameterBusinessModel> printList = new List<PrintCustomerParameterBusinessModel>();
                    PrintCustomerParameterBusinessModel printCustomerParameterBusinessModel;
                    for (int i = 0; i < exportTable.Rows.Count; i++)
                    {
                        printCustomerParameterBusinessModel = new PrintCustomerParameterBusinessModel();
                        printCustomerParameterBusinessModel.ColumnOne = Convert.ToString(exportTable.Rows[i][0], CultureInfo.InvariantCulture).Trim();
                        printCustomerParameterBusinessModel.ColumnTwo = Convert.ToString(exportTable.Rows[i][1], CultureInfo.InvariantCulture).Trim();
                        printCustomerParameterBusinessModel.ColumnThree = Convert.ToString(exportTable.Rows[i][2], CultureInfo.InvariantCulture).Trim();
                        printCustomerParameterBusinessModel.ColumnFour = Convert.ToString(exportTable.Rows[i][3], CultureInfo.InvariantCulture).Trim();
                        printCustomerParameterBusinessModel.IsBold = Convert.ToBoolean(exportTable.Rows[i][4], CultureInfo.InvariantCulture);
                        printList.Add(printCustomerParameterBusinessModel);
                    }

                    return printList;
                }
            }

            exportTable.Columns.Remove("                ");
            return exportTable;
        }

        /// <summary>
        /// Creates the data row.
        /// </summary>
        /// <param name="exportTable">The export table.</param>
        /// <param name="isBold">if set to <c>true</c> [is bold].</param>
        /// <param name="columnOneData">The column one data.</param>
        /// <param name="columnTwoData">The column two data.</param>
        /// <param name="columnThreeData">The column three data.</param>
        /// <param name="columnFourData">The column four data.</param>
        /// <returns>Data row</returns>
        private DataRow CreateDataRow(DataTable exportTable, bool isBold = false, string columnOneData = "", string columnTwoData = "", string columnThreeData = "", string columnFourData = "")
        {
            string column = " ";
            DataRow dataRow = exportTable.NewRow();
            dataRow[column] = columnOneData;
            column += column;
            dataRow[column] = columnTwoData;
            column += column;
            dataRow[column] = columnThreeData;
            column += column;
            dataRow[column] = columnFourData;
            column += column;
            dataRow[column] = isBold;
            return dataRow;
        }

        /// <summary>
        /// Gets the customer information.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// CustomerViewModel.
        /// </returns>
        public CustomerViewModel GetCustomerInformation(long customerId)
        {
            ControllerHelper.ActionName = Constants.ActionNameSearchCustomer;
            ControllerHelper.ActionParam = new[] { globalUserId };
            var customerResponse = ControllerHelper.GetMethodServiceRequest<List<DropDownDataBusinessModel>>();

            PatientTypeMatrixViewModel patientTypeMatrixViewModel = new ViewModel.PatientTypeMatrixViewModel();
            patientTypeMatrixViewModel.PatientType = GetAllPatientType();

            // Get clinical contact master data.
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ViewModel.ClinicalContactMappingViewModel();
            var languageId = Convert.ToInt64(SCAEnums.OtherConstants.LanguageId, CultureInfo.InvariantCulture);
            // Retrieve role type.. 
            var roleListTypeId = Convert.ToInt64(SCAEnums.ListType.MedicalStaff, CultureInfo.InvariantCulture);
            clinicalContactMappingViewModel.MedicalStaffMasterList = GetRoleTypeForClinicalContactForDropdown(customerId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            // Retrieve hierarchy.. 
            var hierarchyListTypeId = Convert.ToInt64(SCAEnums.ListType.Hierarchy, CultureInfo.InvariantCulture);
            clinicalContactMappingViewModel.HierarchyMasterList = GenericHelper.GetListDataForDropdown(hierarchyListTypeId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            // Get Details for Role Mapping
            clinicalContactMappingViewModel.ClinicalChildList = GenericHelper.GetClinicalContactForDropdown(roleListTypeId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            clinicalContactMappingViewModel.ClinicalParentList = GenericHelper.GetClinicalContactForDropdown(roleListTypeId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            clinicalContactMappingViewModel.ParentLinkItems = new SelectList(new List<ClinicalContactValueMappingBusinessModel>(), Constants.ChildListId, Constants.ChildValue);

            var mapCustomers = new CustomerViewModel
            {
                PatientTypeMatrixViewModel = patientTypeMatrixViewModel,
                ClinicalContactMappingViewModel = clinicalContactMappingViewModel,
                CustomerParameterViewModel = new CustomerParameterViewModel(),
                CustomerItems = new SelectList(customerResponse, Constants.Value, Constants.DisplayText)
            };

            // Fill the Status Drop down
            mapCustomers.CustomerStatus = GenericHelper.GetListDataForDropdown(Convert.ToString(Convert.ToInt64(SCAEnums.ListType.CustomerStatus, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture), Convert.ToString(languageId, CultureInfo.InvariantCulture));

            return mapCustomers;
        }

        /// <summary>
        /// Gets the customer details by identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// CustomerViewModel.
        /// </returns>
        public CustomerViewModel GetCustomerDetailsById(string customerId)
        {
            CustomerViewModel customerViewModel = new CustomerViewModel();

            ControllerHelper.ActionName = Constants.ActionNameGetCustomerInformation;
            ControllerHelper.ActionParam = new[] { globalUserId, customerId };
            var customerResponse = ControllerHelper.GetMethodServiceRequestForList<List<CustomerBusinessModel>>();
            customerViewModel.CustomerBusinessModel = customerResponse.FirstOrDefault();
            return customerViewModel;
        }

        /// <summary>
        /// Gets the prescription approval.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// CustomerViewModel.
        /// </returns>
        public CustomerViewModel GetPrescriptionApproval(string customerId)
        {
            CustomerViewModel customerViewModel = new CustomerViewModel();

            ControllerHelper.ActionName = Constants.ActionNameGetCustomerPrescriptionInformation;
            ControllerHelper.ActionParam = new[] { globalUserId, customerId };
            var customerResponse = ControllerHelper.GetMethodServiceRequestForList<List<CustomerPrescriptionApprovalBusinessModel>>();
            customerViewModel.CustomerBusinessModel = new CustomerBusinessModel() { PrescriptionApprovals = customerResponse };
            return customerViewModel;
        }

        /// <summary>
        /// Saves the prescription approval.
        /// </summary>
        /// <param name="objUserList">The object user list.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// System.Int32.
        /// </returns>
        public int SavePrescriptionApproval(string objUserList, string customerId)
        {
            List<CustomerPrescriptionApprovalBusinessModel> lstAuthorisedUsersPrescription = new List<CustomerPrescriptionApprovalBusinessModel>();
            if (!string.IsNullOrEmpty(objUserList))
            {
                List<string> userIds = objUserList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                userIds.ForEach(d =>
                {
                    lstAuthorisedUsersPrescription.Add(new CustomerPrescriptionApprovalBusinessModel() { UserId = d });
                });
            }
            else
            {
                return 0;
            }

            CustomerBusinessModel customerBusinessModel = new CustomerBusinessModel();
            customerBusinessModel.CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture);
            customerBusinessModel.UserId = globalUserId;
            customerBusinessModel.PrescriptionApprovals = lstAuthorisedUsersPrescription;

            ControllerHelper.ActionName = Constants.ActionNameSaveCustomerAuthorisedUserPrescriptionApprovals;
            var saveUserToCustomerResponse = ControllerHelper.PostMethodServiceRequestObject(customerBusinessModel);
            return Convert.ToInt32(saveUserToCustomerResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Removes the prescription approval.
        /// </summary>
        /// <param name="mappingIds">The mapping ids.</param>
        /// <returns>
        /// System.Int32.
        /// </returns>
        public int RemovePrescriptionApproval(string mappingIds)
        {
            ControllerHelper.ActionName = Constants.ActionNameRemoveCustomerPrescriptionApprovals;
            CustomerBusinessModel CustomerBusinessModel = new CustomerBusinessModel()
            {
                UserId = globalUserId,
                PrescriptionIds = mappingIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList()
            };

            var customerResponse = ControllerHelper.GetMethodServiceRequestObject(CustomerBusinessModel);

            return Convert.ToInt32(customerResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Saves the customer information details.
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns>
        /// System.Int32.
        /// </returns>
        public int SaveCustomerInformationDetails(CustomerBusinessModel customerBusinessModel)
        {
            if (customerBusinessModel != null)
            {
                customerBusinessModel.UserId = globalUserId;
            }

            ControllerHelper.ActionName = Constants.ActionNameSaveCustomerInformationDetails;
            var customerResponse = ControllerHelper.PostMethodServiceRequestObject(customerBusinessModel);
            return Convert.ToInt32(customerResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Checks the active patients by customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// System.Int32.
        /// </returns>
        public int CheckActivePatientsByCustomer(string customerId)
        {
            CustomerBusinessModel customerBusinessModel = new CustomerBusinessModel() { UserId = globalUserId, CustomerId = Convert.ToInt64(customerId, CultureInfo.InvariantCulture) };
            ControllerHelper.ActionName = Constants.ActionNameCheckActivePatientsByCustomer;
            var customerResponse = ControllerHelper.PostMethodServiceRequestObject(customerBusinessModel);
            return Convert.ToInt32(customerResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the user details by user identifier.
        /// </summary>
        /// <param name="userToAuthorize">The user to authorize.</param>
        /// <returns>
        /// CustomerViewModel.
        /// </returns>
        public CustomerViewModel GetUserDetailsByUserId(string userToAuthorize)
        {
            CustomerViewModel customerViewModel = new CustomerViewModel();

            ControllerHelper.ActionName = Constants.ActionNameGetUserDetailsByUserId;
            ControllerHelper.ActionParam = new[] { userToAuthorize, globalUserId };
            var customerResponse = ControllerHelper.GetMethodServiceRequestForList<List<CustomerPrescriptionApprovalBusinessModel>>();
            customerViewModel.CustomerBusinessModel = new CustomerBusinessModel() { PrescriptionApprovals = customerResponse };
            return customerViewModel;
        }

        #endregion

        #region Patient Type Matrix
        /// <summary>
        /// Get patient type matrix
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// view model
        /// </returns>
        public PatientTypeMatrixViewModel GetPatientTypeMatrix(string customerId)
        {
            PatientTypeMatrixViewModel patientTypeMatrixViewModel = new PatientTypeMatrixViewModel();
            var patientTypeList = new List<PatientTypeListViewModel>();

            ControllerHelper.ActionName = Constants.ActionPatientTypeMatrix;
            ControllerHelper.ActionParam = new[] { customerId };

            var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<PatientTypeMatrixBusinessModel>>();
            PatientTypeListViewModel patientTypeViewModel = null;

            if (returnListData == null)
            {
                patientTypeMatrixViewModel.Count = 0;
            }
            else
            {
                patientTypeMatrixViewModel.Count = returnListData.Count;

                foreach (var obj in returnListData)
                {
                    patientTypeViewModel = new PatientTypeListViewModel();
                    patientTypeViewModel.PatientTypeMatrixID = obj.PatientTypeMatrixID;
                    patientTypeViewModel.PatientTypeId = Convert.ToInt64(obj.PatientType, CultureInfo.InvariantCulture);
                    patientTypeViewModel.PatientType = obj.PatientTypeText;
                    patientTypeViewModel.PrescMinFrequency = obj.PrescMinFrequency;
                    patientTypeViewModel.PrescMaxFrequency = obj.PrescMaxFrequency;
                    patientTypeViewModel.IncrementFrequency = obj.IncrementFrequency;
                    patientTypeViewModel.AdultBillTo = obj.AdultBillTo;
                    patientTypeViewModel.PaedBillTo = obj.PaedBillTo;
                    patientTypeViewModel.AdvanceOrderActivationDays = obj.AdvanceOrderActivationDays;
                    patientTypeViewModel.ChangeOrderNddDays = obj.ChangeOrderNddDays;
                    patientTypeViewModel.IsAddFromPrescriptionAllowed = obj.IsAddFromPrescriptionAllowed;
                    patientTypeViewModel.IsAddFromProductAllowed = obj.IsAddFromProductAllowed;
                    patientTypeViewModel.IsOneOffAddFromPrescriptionAllowed = obj.IsOneOffAddFromPrescriptionAllowed;
                    patientTypeViewModel.IsOneOffAddFromProductAllowed = obj.IsOneOffAddFromProductAllowed;
                    patientTypeViewModel.IsPrescriptionApprovalRequired = obj.IsPrescriptionApprovalRequired;
                    patientTypeViewModel.IsRemove = obj.IsRemove;
                    patientTypeViewModel.DefaultFrequency = obj.DefaultFrequency;
                    patientTypeViewModel.ListIndex = obj.ListIndex;
                    patientTypeList.Add(patientTypeViewModel);
                }
            }

            patientTypeMatrixViewModel.PatientTypeMatrixList = patientTypeList.OrderBy(q => q.ListIndex).ToList();
            return patientTypeMatrixViewModel;
        }

        /// <summary>
        /// Export to excel patient type matrix details..
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// file path
        /// </returns>
        public string ExportPatientTypeMatrix(string customerId)
        {
            var filePath = string.Empty;

            // get patient type matrix data.
            PatientTypeMatrixViewModel patientTypeMatrixViewModel = new PatientTypeMatrixViewModel();
            patientTypeMatrixViewModel = this.GetPatientTypeMatrix(customerId);

            // Export to excel - start
            string patientTypeMatrixServerDirectory = Path.Combine(GenericHelper.UploadDirectory, GenericHelper.PatientTypeMatrixSharedLocation);
            string fileNamePrefix = Constants.PatientTypeMatrixPrefix;

            if (patientTypeMatrixViewModel.Count > 0)
            {
                var columnsToRemove = new List<string>() { "PatientTypeId", "CreatedBy", "CreatedDateTime", "ModifiedBy", "ModifiedDateTime", "IsRemove", "PatientTypeMatrixID" };
                filePath = ExcelHelper.ExportToExcel(patientTypeMatrixViewModel.PatientTypeMatrixList.ToList(), patientTypeMatrixServerDirectory, fileNamePrefix, columnsToRemove);
                // Export to excel - end
            }

            return filePath;
        }

        /// <summary>
        /// Get all list type
        /// </summary>
        /// <returns>
        /// select list
        /// </returns>
        public static SelectList GetAllPatientType()
        {
            var patientType = Convert.ToInt64(SCAEnums.ListType.PatientType, CultureInfo.InvariantCulture);
            ControllerHelper.ActionName = Constants.ActionNameGetMasterListById;
            string userId = ControllerHelper.CurrentUser.UserId.ToString();
			ControllerHelper.ActionParam = new[] { patientType.ToString(CultureInfo.InvariantCulture), userId };
            var listData = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            if (listData != null && listData.Any())
            {
                return new SelectList(listData.Where(o => o.ListId != (long)SCAEnums.PatientType.Prospect).ToList(), Constants.ListId, Constants.DisplayText);
            }
            return new SelectList(new List<ListTypeBusinessModel>(), Constants.ListId, Constants.DisplayText);
        }

        /// <summary>
        /// Save patient type matrix
        /// </summary>
        /// <param name="objPatientMatrixList">The object patient matrix list.</param>
        /// <param name="objCustomerId">The object customer identifier.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public int SavePatientTypeMatrix(List<PatientTypeListViewModel> objPatientMatrixList, string objCustomerId)
        {
            PatientTypeMatrixMainBusinessModel patientTypeMatrixMainBusinessModel = new PatientTypeMatrixMainBusinessModel();
            patientTypeMatrixMainBusinessModel.CustomerId = Convert.ToInt64(objCustomerId, CultureInfo.InvariantCulture);

            var patientMatrixList = new List<PatientTypeMatrixBusinessModel>();
            foreach (var item in objPatientMatrixList)
            {
                var viewModel = new PatientTypeMatrixBusinessModel();
                viewModel.PatientTypeMatrixID = item.PatientTypeMatrixID;
                viewModel.PatientTypeId = item.PatientTypeId;
                viewModel.PrescMinFrequency = item.PrescMinFrequency;
                viewModel.PrescMaxFrequency = item.PrescMaxFrequency;
                viewModel.IncrementFrequency = item.IncrementFrequency;
                viewModel.AdultBillTo = item.AdultBillTo;
                viewModel.PaedBillTo = item.PaedBillTo;
                viewModel.AdvanceOrderActivationDays = item.AdvanceOrderActivationDays;
                viewModel.ChangeOrderNddDays = item.ChangeOrderNddDays;
                viewModel.IsAddFromPrescriptionAllowed = item.IsAddFromPrescriptionAllowed;
                viewModel.IsAddFromProductAllowed = item.IsAddFromProductAllowed;
                viewModel.IsOneOffAddFromPrescriptionAllowed = item.IsOneOffAddFromPrescriptionAllowed;
                viewModel.IsOneOffAddFromProductAllowed = item.IsOneOffAddFromProductAllowed;
                viewModel.IsPrescriptionApprovalRequired = item.IsPrescriptionApprovalRequired;
                viewModel.IsRemove = item.IsRemove;
                viewModel.DefaultFrequency = item.DefaultFrequency;

                patientMatrixList.Add(viewModel);
            }
            patientTypeMatrixMainBusinessModel.PatientTypeMatrixList = patientMatrixList;
            patientTypeMatrixMainBusinessModel.UserId = this.globalUserId;

            //// Save Data in IDXCustomerPatientType, PatientTypeMatrix
            ControllerHelper.ActionName = Constants.ActionNameSavePatientTypeMatrixDetails;
            var savePatientMatrixResponse = ControllerHelper.PostMethodServiceRequestObject(patientTypeMatrixMainBusinessModel);
            return Convert.ToInt32(savePatientMatrixResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get Patient Type Associated With Patient
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="patientType">Type of the patient.</param>
        /// <returns>
        /// view model
        /// </returns>
        public PatientTypeMatrixViewModel GetPatientTypeAssociatedWithPatient(string customerId, string patientType)
        {
            PatientTypeMatrixViewModel patientTypeMatrixViewModel = new PatientTypeMatrixViewModel();
            ControllerHelper.ActionName = Constants.ActionGetPatientAssociatedWithPatient;
            ControllerHelper.ActionParam = new[] { customerId, patientType, this.globalUserId };

            var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<PatientTypeMatrixBusinessModel>>();

            if (returnListData == null)
            {
                patientTypeMatrixViewModel.Count = 0;
            }
            else
            {
                patientTypeMatrixViewModel.Count = returnListData.Count;
            }

            return patientTypeMatrixViewModel;
        }
        #endregion

        #region Recent Orders

        /// <summary>
        /// Gets the recent order details.
        /// </summary>
        /// <param name="patientOrdersBusinessModel">The patient orders business model.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="totalPages">The total pages.</param>
        /// <returns>
        /// List&lt;PatientOrdersBusinessModel&gt;.
        /// </returns>
        public List<PatientOrdersBusinessModel> GetRecentOrderDetails(PatientOrdersBusinessModel patientOrdersBusinessModel, out int totalRecords, out int totalPages)
        {
			var serviceHelper = new WebServiceHelper(Constants.ActionNameGetRecentOrders, new[] { patientOrdersBusinessModel.SearchId, globalUserId, patientOrdersBusinessModel.RecentOrderType });
            var recentOrderData = serviceHelper.GetMethodServiceRequest<List<PatientOrdersBusinessModel>>();
			if (recentOrderData == null)
			{
				recentOrderData = new List<PatientOrdersBusinessModel>();
			}
			else
			{
				recentOrderData.ForEach(o => { o.OrderDateDisplay = Convert.ToDateTime(o.OrderDate); });
			}

            List<PatientOrdersBusinessModel> griddata = new List<PatientOrdersBusinessModel>();
            var data = new List<IGrouping<long?, PatientOrdersBusinessModel>>();
            bool isColor = true;
            if (recentOrderData != null)
            {
                griddata = recentOrderData.Skip((patientOrdersBusinessModel.Page - 1) * patientOrdersBusinessModel.RowCount).Take(patientOrdersBusinessModel.RowCount).ToList();                                
                data = griddata.GroupBy(x => x.OrderId).Select(q => q).ToList();
                
                for (int i = 0; i < data.Count; i++, isColor = !isColor) //---Set color for recent order
                {
                    List<PatientOrdersBusinessModel> lstOrder = data[i].ToList();
                    if (isColor)
                        griddata.Where(x => x.OrderId == lstOrder[0].OrderId).ToList().ForEach(o => o.SetColor = "RecentOrderRow");
                    else
                        griddata.Where(x => x.OrderId == lstOrder[0].OrderId).ToList().ForEach(o => o.SetColor = "RecentOrderAltRow");
                }

                switch (patientOrdersBusinessModel.SortColumn.ToLower(CultureInfo.InvariantCulture))
                {
                    case "saporderno":
                        if (patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            griddata = griddata.OrderBy(x => x.SAPOrderNo).ToList();
                        else
                            griddata = griddata.OrderByDescending(x => x.SAPOrderNo).ToList();
                        break;
                    case "sapdeliveryno":
                        if (patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            griddata = griddata.OrderBy(x => x.SAPDeliveryNo).ToList();
                        else
                            griddata = griddata.OrderByDescending(x => x.SAPDeliveryNo).ToList();
                        break;
                    case "productdescription":
                        if (patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            griddata = griddata.OrderBy(x => x.ProductDescription).ToList();
                        else
                            griddata = griddata.OrderByDescending(x => x.ProductDescription).ToList();
                        break;
                    case "quantity":
                        if (patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            griddata = griddata.OrderBy(x => x.Quantity).ToList();
                        else
                            griddata = griddata.OrderByDescending(x => x.Quantity).ToList();
                        break;
                    case "orderdate":
                        if (patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            griddata = griddata.OrderBy(x => x.OrderDateDisplay).ToList();
                        else
                            griddata = griddata.OrderByDescending(x => x.OrderDateDisplay).ToList();
                        break;
                    case "invoicenumber":
                        if (patientOrdersBusinessModel.SortType.ToLower(CultureInfo.InvariantCulture) == "asc")
                            griddata = griddata.OrderBy(x => x.InvoiceNumber).ToList();
                        else
                            griddata = griddata.OrderByDescending(x => x.InvoiceNumber).ToList();
                        break;
                    default:
                        griddata = griddata.OrderByDescending(x => x.OrderDateDisplay).ToList();
                        break;
                }
            }
            totalRecords = recentOrderData != null ? recentOrderData.Count : 0;
            totalPages = (int)Math.Ceiling((double)recentOrderData.Count / (double)patientOrdersBusinessModel.RowCount);
            return griddata;
        }

        #endregion

        #region Clinical Contact Role / Value Mapping

        /// <summary>
        /// Gets the clinical contact mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// ClinicalContactMapping ViewModel.
        /// </returns>
        public ClinicalContactMappingViewModel GetClinicalContactMapping(long customerId)
        {
            // Common to both Role and Value Mapping
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ClinicalContactMappingViewModel();
            clinicalContactMappingViewModel.CustomerId = customerId;
            var languageId = Convert.ToInt64(SCAEnums.OtherConstants.LanguageId, CultureInfo.InvariantCulture);

            // Retrieve master data.. 
            var roleListTypeId = Convert.ToInt64(SCAEnums.ListType.MedicalStaff, CultureInfo.InvariantCulture);
            clinicalContactMappingViewModel.MedicalStaffMasterList = GetRoleTypeForClinicalContactForDropdown(customerId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));

            var hierarchyListTypeId = Convert.ToInt64(SCAEnums.ListType.Hierarchy, CultureInfo.InvariantCulture);
            clinicalContactMappingViewModel.HierarchyMasterList = GenericHelper.GetListDataForDropdown(hierarchyListTypeId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));

            // Get Details for Role Mapping
            clinicalContactMappingViewModel.ClinicalChildList = GenericHelper.GetClinicalContactForDropdown(roleListTypeId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            clinicalContactMappingViewModel.ClinicalParentList = GenericHelper.GetClinicalContactForDropdown(roleListTypeId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            clinicalContactMappingViewModel.ParentLinkItems = new SelectList(new List<ClinicalContactValueMappingBusinessModel>(), Constants.ChildListId, Constants.ChildValue);

            return clinicalContactMappingViewModel;
        }

        /// <summary>
        /// Get Role Type For Clinical Contact For Dropdown
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>
        /// list of values
        /// </returns>
        private static SelectList GetRoleTypeForClinicalContactForDropdown(string customerId, string languageId)
        {
            ControllerHelper.ActionName = Constants.ActionNameGetRoleTypeForClinicalContact;
            string userId = ControllerHelper.CurrentUser.UserId.ToString();
            ControllerHelper.ActionParam = new[] { customerId, languageId, userId };

            var response = ControllerHelper.GetMethodServiceRequestForList<List<ListTypeBusinessModel>>();
            return new SelectList(response, Constants.ListId, Constants.DisplayText);
        }

        /// <summary>
        /// Refresh Role Type dropdown on Save Role button..
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// view model
        /// </returns>
        public ClinicalContactMappingViewModel RefreshRoleTypeDropDown(string customerId)
        {
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ClinicalContactMappingViewModel();
            var languageId = Convert.ToInt64(SCAEnums.OtherConstants.LanguageId, CultureInfo.InvariantCulture);

            // Retrieve master data.. 
            clinicalContactMappingViewModel.MedicalStaffMasterList = GetRoleTypeForClinicalContactForDropdown(customerId.ToString(CultureInfo.InvariantCulture), languageId.ToString(CultureInfo.InvariantCulture));
            return clinicalContactMappingViewModel;
        }

        /// <summary>
        /// Gets the clinical contact value mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="roleType">Type of the role.</param>
        /// <returns>
        /// Clinical Contact Mapping View Model.
        /// </returns>
        public ClinicalContactMappingViewModel GetClinicalContactValueMapping(string customerId, string roleType)
        {
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ClinicalContactMappingViewModel();
            var valueMappingList = new List<ClinicalContactValueMappingViewModel>();

            ControllerHelper.ActionName = Constants.ActionGetClinicalContactValueMapping;
            ControllerHelper.ActionParam = new[] { customerId, roleType, this.globalUserId };

            var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<ClinicalContactValueMappingBusinessModel>>();
            if (returnListData == null)
            {
                clinicalContactMappingViewModel.Count = 0;
            }
            else
            {
                clinicalContactMappingViewModel.Count = returnListData.Count;
                foreach (var obj in returnListData)
                {
                    var viewModel = new ClinicalContactValueMappingViewModel();
                    var hierarchy = Enum.GetName(typeof(SCAEnums.HierarchyType), Convert.ToInt64(obj.HierarchyListId, CultureInfo.InvariantCulture));

                    viewModel.ClinicalContactRoleMappingId = (long)obj.ClinicalContactRoleMappingId;
                    viewModel.ClinicalContactValueMappingId = (long)obj.ClinicalContactValueMappingId;
                    viewModel.ChildListId = (long)obj.ChildListId;
                    viewModel.ChildValue = obj.ChildValue;
                    viewModel.ParentValue = obj.ParentValue;
                    viewModel.HierarchyListId = (long)obj.HierarchyListId;
                    viewModel.HierarchyName = hierarchy;
                    viewModel.IsRemoved = obj.IsRemoved;
                    viewModel.ChildListName = obj.ChildRoleName;
                    viewModel.ParentListName = obj.ParentRoleName;
                    viewModel.ParentValueId = obj.ParentValueId;
                    viewModel.ChildValueId = obj.ChildValueId;
                    valueMappingList.Add(viewModel);
                }
            }

            clinicalContactMappingViewModel.ClinicalContactValueMappingList = valueMappingList.OrderBy(q => q.ChildValue).ToList();
            return clinicalContactMappingViewModel;
        }

        /// <summary>
        /// Get parent value
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="childId">The child identifier.</param>
        /// <returns>
        /// view model
        /// </returns>
        public ClinicalContactMappingViewModel GetParentValueForSelectedRoleType(string customerId, string childId)
        {
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ClinicalContactMappingViewModel();
            var valueMappingList = new List<ClinicalContactValueMappingViewModel>();
            var parentName = string.Empty;
            var hierarchy = string.Empty;
            long clinicalContactRoleMappingId = 0;

            ControllerHelper.ActionName = Constants.ActionGetParentValueForSelectedRole;
            ControllerHelper.ActionParam = new[] { customerId, childId, this.globalUserId };
            var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<ClinicalContactValueMappingBusinessModel>>();
            if (returnListData == null)
            {
                clinicalContactMappingViewModel.Count = 0;

                // Get parent name by child id to dispaly parent name if not available
                ControllerHelper.ActionName = Constants.ActionGetRoleNameForClinicalContactByChildId;
                ControllerHelper.ActionParam = new[] { childId, this.globalUserId, customerId };
                parentName = ControllerHelper.GetMethodServiceRequestForList<string>();
            }
            else
            {
                clinicalContactMappingViewModel.Count = returnListData.Count;
                foreach (var obj in returnListData)
                {
                    var viewModel = new ClinicalContactValueMappingViewModel();
                    hierarchy = Enum.GetName(typeof(SCAEnums.HierarchyType), Convert.ToInt64(obj.HierarchyListId, CultureInfo.InvariantCulture));
                    parentName = obj.ParentRoleName;
                    viewModel.ChildListId = (long)obj.ChildListId;
                    viewModel.ChildValue = obj.ChildValue;
                    viewModel.ChildValueId = obj.ChildValueId;
                    viewModel.ParentListId = obj.ParentListId;
                    viewModel.ParentListName = obj.ParentRoleName;
                    viewModel.HierarchyName = hierarchy;
                    clinicalContactRoleMappingId = obj.ClinicalContactRoleMappingId;
                    valueMappingList.Add(viewModel);
                }
            }

            clinicalContactMappingViewModel.ParentLinkItems = new SelectList(valueMappingList.ToList(), Constants.ChildValueId, Constants.ChildValue);
            clinicalContactMappingViewModel.ParentListName = parentName;
            clinicalContactMappingViewModel.HierarchyName = hierarchy;
            clinicalContactMappingViewModel.ClinicalContactRoleMappingId = clinicalContactRoleMappingId;
            return clinicalContactMappingViewModel;
        }

        /// <summary>
        /// Gets the clinical contact role mapping.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// Clinical Contact Mapping View Model.
        /// </returns>
        public ClinicalContactMappingViewModel GetClinicalContactRoleMapping(string customerId)
        {
            ClinicalContactMappingViewModel clinicalContactMappingViewModel = new ClinicalContactMappingViewModel();
            var valueMappingList = new List<ClinicalContactRoleMappingViewModel>();

            ControllerHelper.ActionName = Constants.ActionGetClinicalContactRoleMapping;
            ControllerHelper.ActionParam = new[] { customerId };

            var returnListData = ControllerHelper.GetMethodServiceRequestForList<List<ClinicalContactMappingBusinessModel>>();
            if (returnListData == null)
            {
                clinicalContactMappingViewModel.Count = 0;
            }
            else
            {
                foreach (var obj in returnListData)
                {
                    var viewModel = new ClinicalContactRoleMappingViewModel();
                    viewModel.ClinicalContactRoleMappingId = (long)obj.ClinicalContactRoleMappingId;
                    viewModel.ChildListId = (long)obj.ChildListId;
                    viewModel.Child = obj.Child;
                    viewModel.ParentListId = (long)obj.ParentListId;
                    viewModel.Parent = obj.Parent;
                    viewModel.HierarchyListId = (long)obj.HierarchyListId;
                    viewModel.Hierarchy = obj.Hierarchy;
                    valueMappingList.Add(viewModel);
                }
            }

            clinicalContactMappingViewModel.ClinicalContactRoleMappingList = valueMappingList.OrderBy(q => q.HierarchyListId).ToList();
            return clinicalContactMappingViewModel;

        }

        /// <summary>
        /// Save Clinical Contact Role Mapping Data
        /// </summary>
        /// <param name="objClinicalRoleMapping">The object clinical role mapping.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public int SaveClinicalRoleMapping(List<ClinicalContactRoleMappingViewModel> objClinicalRoleMapping)
        {
            //ClinicalContactMappingBusinessModel clinicalContactMappingBusinessModel new ClinicalContactMappingBusinessModel();

            var clinicalContactMappingBusinessModel = new List<ClinicalContactMappingBusinessModel>();
            foreach (var item in objClinicalRoleMapping)
            {
                var viewModel = new ClinicalContactMappingBusinessModel();
                viewModel.ClinicalContactRoleMappingId = item.ClinicalContactRoleMappingId;
                viewModel.CustomerId = item.CustomerId;
                viewModel.HierarchyListId = item.HierarchyListId;
                viewModel.ChildListId = item.ChildListId;
                viewModel.ParentListId = item.ParentListId;
                viewModel.IsParentExist = item.IsParentExist;
                viewModel.UserId = this.globalUserId;
                clinicalContactMappingBusinessModel.Add(viewModel);
            }

            ControllerHelper.ActionName = Constants.ActionNameSaveClinicalRoleMapping;
            var clinicalContactResponse = ControllerHelper.PostMethodServiceRequestObject(clinicalContactMappingBusinessModel);
            return Convert.ToInt32(clinicalContactResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Save Clinical Contact Data
        /// </summary>
        /// <param name="objClinicalContactValues">The object clinical contact values.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public int SaveClinicalContactValue(List<ClinicalContactValueMappingViewModel> objClinicalContactValues)
        {
            ClinicalContactMappingBusinessModel clinicalContactMappingBusinessModel = new ClinicalContactMappingBusinessModel();
            var clinicalContactValueMappingBusinessModel = new List<ClinicalContactValueMappingBusinessModel>();
            foreach (var item in objClinicalContactValues)
            {
                var viewModel = new ClinicalContactValueMappingBusinessModel();
                viewModel.ClinicalContactRoleMappingId = (long)item.ClinicalContactRoleMappingId;
                viewModel.ClinicalContactValueMappingId = (long)item.ClinicalContactValueMappingId;
                viewModel.ChildListId = item.ChildListId;
                viewModel.ChildValue = item.ChildValue;
                viewModel.ParentValue = item.ParentValue;
                viewModel.IsRemoved = item.IsRemoved;
                viewModel.ParentValueId = item.ParentValueId;

                clinicalContactValueMappingBusinessModel.Add(viewModel);
            }

            clinicalContactMappingBusinessModel.ClinicalContactValueMappingList = clinicalContactValueMappingBusinessModel;
            clinicalContactMappingBusinessModel.UserId = this.globalUserId;

            //// Save Data in ClinicalContactValueMapping
            ControllerHelper.ActionName = Constants.ActionNameSaveClinicalContactValueMapping;
            var saveResponse = ControllerHelper.PostMethodServiceRequestObject(clinicalContactMappingBusinessModel);
            return Convert.ToInt32(saveResponse, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Removes the clinical contact analysis mapping.
        /// </summary>
        /// <param name="medicalStaffAnalysisInfo">The medical staff analysis information.</param>
        /// <returns></returns>
        public bool RemoveClinicalContactAnalysisMapping(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfo)
        {
            medicalStaffAnalysisInfo.UserId = Guid.Parse(this.globalUserId);
            ControllerHelper.ActionName = Constants.ActionNameRemoveClinicalContactAnalysisMapping;
            var removeClinicalResponse = ControllerHelper.PostMethodServiceRequestObject(medicalStaffAnalysisInfo);
            return Convert.ToBoolean(removeClinicalResponse, CultureInfo.InvariantCulture);
        }

        #endregion
    }

	

	public class OrderData
    {
        /// <summary>
        /// Gets or sets Customer ID
        /// </summary>       
        public long CustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Patient No
        /// </summary>

        public string SAPPatientNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient ID
        /// </summary>

        public long PatientID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHome ID
        /// </summary>

        public long? CareHomeID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Type
        /// </summary>

        public long? OrderType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Id
        /// </summary>

        public long? OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Display Id
        /// </summary>

        public string ProductDisplayId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Status
        /// </summary>

        public long OrderStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Quantity
        /// </summary>

        public long Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Delivery Date
        /// </summary>

        public string DeliveryDate
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets a flag Send To Sap
        /// </summary>
        public bool SendToSap { get; set; }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>

        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DerivedNDD
        /// </summary>

        public string DerivedNDD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Delivery Frequency
        /// </summary>

        public long? DeliveryFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description
        /// </summary>

        public string ProductName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Exclude Service Charge
        /// </summary>

        public bool? ExcludeServiceCharge
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IDX Prescription Product Id
        /// </summary>

        public long? IDXPrescriptionProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Invoice Number
        /// </summary>

        public string InvoiceNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is FOC
        /// </summary>

        public bool? IsFOC
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Last Delivery Date
        /// </summary>

        public string LastDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Modified Date
        /// </summary>

        public string ModifiedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Next Delivery Date
        /// </summary>

        public string NextDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Date
        /// </summary>

        public DateTime OrderDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Note
        /// </summary>

        public string OrderNote
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Name
        /// </summary>

        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient First Name
        /// </summary>

        public string PatientFirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Last Name
        /// </summary>

        public string PatientLastName
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets Patient Type
        /// </summary>

        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Type
        /// </summary>

        public long PatientTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Description
        /// </summary>

        public string ProductDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Id
        /// </summary>

        public long? ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets QuantityToCompare , will be same as Quantity while loading and uneditable
        /// </summary>

        public long QuantityToCompare
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round ID
        /// </summary>

        public string RoundID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Delivery No
        /// </summary>

        public string SAPDeliveryNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Order No
        /// </summary>

        public string SAPOrderNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Status
        /// </summary>

        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Unit
        /// </summary>

        public string Unit
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets User Id
        /// </summary>

        public Guid ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsOrderActivated
        /// </summary>

        public bool? IsOrderActivated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Status
        /// </summary>

        public string PatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Disclaimer Person Name
        /// </summary>

        public string PersonName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Disclaimer Person Position
        /// </summary>

        public string PersonPosition
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsDisclaimerRequired
        /// </summary>

        public bool? IsDislcaimerRequired
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DisclaimerId
        /// </summary>

        public long? OrderDisclaimerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SortType
        /// </summary>

        public string SortType { get; set; }

        /// <summary>
        /// Gets or sets SortColumn
        /// </summary>

        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets Page
        /// </summary>

        public int Page { get; set; }

        /// <summary>
        /// Gets or sets RowCount
        /// </summary>

        public int RowCount { get; set; }

        /// <summary>
        /// Gets or sets SearchId
        /// </summary>

        public string SearchId { get; set; }

        /// <summary>
        /// Gets or sets RecentOrderType
        /// </summary>

        public string RecentOrderType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is one off order.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is one off order; otherwise, <c>false</c>.
        /// </value>

        public bool IsOneOffOrder { get; set; }

        /// <summary>
        /// Gets or sets isProductLevel
        /// </summary>

        public bool IsProductLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PurchaseOrderNum
        /// </summary>

        public string PurchaseOrderNum { get; set; }

        /// <summary>
        /// Gets or Sets OrderCreationType [Online/Telephonic]
        /// </summary>

        public long? OrderCreationType { get; set; }

        /// <summary>
        /// Gets or Sets EmailId
        /// </summary>

        public string EmailId { get; set; }

    }
}