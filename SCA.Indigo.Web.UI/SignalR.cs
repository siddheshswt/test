﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Saurabh Mayekar
// Created          : 16-05-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="SignalR.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using SCA.Indigo.Common;
using SCA.Indigo.Web.UI.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace SCA.Indigo.Web.UI
{
    /// <summary>
    /// SignalR Implementation
    /// </summary>
    public class SignalR : Hub
    {
        /// <summary>
        /// Custom dictionary for maintaining active users
        /// </summary>
        static DictionaryCollection<string, SignalRUserModel> Users = new DictionaryCollection<string, SignalRUserModel>();

        #region Front-End Facing Methods

        /// <summary>
        /// Register users for every request
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="requestId"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public async Task Register(string userId, string requestId, object model)
        {
            try
            {
                await Task.Run(() => RegisterAsync(userId, requestId, model));
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }

        /// <summary>
        /// Cleras notification for the user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task ClearNotification(string userId)
        {
            try
            {
                await Task.Run(() => ClearNotificationAsync(userId));
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }

        /// <summary>
        /// Remove users from active users
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task Disconnect(string userId)
        {
            try
            {
                await Task.Run(() => DisconnectAsync(userId));
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }

        /// <summary>
        /// Sends notification to all the active users
        /// </summary>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        public async Task SendNotificationToAll(string notificationType)
        {
            try
            {
                await Task.Run(() => sendNotificationToAllAsync(notificationType));
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }

        /// <summary>
        /// Send notification for interaction
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task SendNotificationForInteraction(string userId)
        {
            try
            {
                await Task.Run(() => sendNotificationForInteractionAsync(userId));
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }
        #endregion

        #region Private Methods For Internal Access

        /// <summary>
        /// Custom dictionary for maintaining active users asynchronously
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="requestId"></param>
        /// <param name="Model"></param>
        private void RegisterAsync(string userId, string requestId, object Model)
        {
            userId = userId.ToUpper(CultureInfo.InvariantCulture);

            SignalRUserModel userModel = null;
            if (Model != null)
                userModel = (SignalRUserModel)JsonConvert.DeserializeObject(Model.ToString(), typeof(SignalRUserModel));

            var data = Users.Read(userId);
            if (data == null)
                data = new SignalRUserModel(userId);
            else if (userModel != null && userModel.LastNotificationTime < data.LastNotificationTime)
                sendNotificationForInteractionAsync(requestId, data);

            data.RequestId.Add(requestId);

            Users.AddOrUpdate(userId, data);
        }

        /// <summary>
        /// Cleras notification for the user asynchronously
        /// </summary>
        /// <param name="userId"></param>
        private void ClearNotificationAsync(string userId)
        {
            userId = userId.ToUpper(CultureInfo.InvariantCulture);
            var user = new SignalRUserModel(userId);
            user.RequestId.Add(Context.ConnectionId);
            Users.Update(userId, user);
        }

        /// <summary>
        /// Remove users from active users asynchronously
        /// </summary>
        /// <param name="userId"></param>
        private static void DisconnectAsync(string userId)
        {
            userId = userId.ToUpper(CultureInfo.InvariantCulture);
            Users.Remove(userId);
        }

        /// <summary>
        /// Sends notification to all the active users asynchronously
        /// </summary>
        /// <param name="notificationType"></param>
        private void sendNotificationToAllAsync(string notificationType)
        {
            foreach (var item in Users)
            {
                item.LastNotificationTime = DateTime.Now;
                Users.AddOrUpdate(item.UserId, item);
            }
            Clients.All.sendNotification(notificationType);
        }

        /// <summary>
        /// Send notification for interaction asynchronously
        /// </summary>
        /// <param name="userId"></param>
        private void sendNotificationForInteractionAsync(string userId)
        {
            var User = Users.Read(userId.ToUpper(CultureInfo.InvariantCulture));

            if (User != null)
            {
                User.InteractionCount++;
                User.LastNotificationTime = DateTime.Now;

                Users.Update(userId, User);

                string userModel = JsonConvert.SerializeObject(User);
                foreach (var requestId in User.RequestId)
                    Clients.Client(requestId).sendNotification(userModel);
            }
        }

        /// <summary>
        /// Send notification for interaction asynchronously
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="userModel"></param>
        private void sendNotificationForInteractionAsync(string requestId, SignalRUserModel userModel)
        {
            Clients.Client(requestId).sendNotification(JsonConvert.SerializeObject(userModel));
        }
        #endregion
    }

    /// <summary>
    /// Custome implementation of Dictionary class
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class DictionaryCollection<TKey, TValue> : IEnumerable<TValue>
    {
        /// <summary>
        /// Object for implementing locking
        /// </summary>
        private readonly object sync = new object();
        /// <summary>
        /// Dictionary object for data
        /// </summary>
        private Dictionary<TKey, TValue> dic = new Dictionary<TKey, TValue>();

        /// <summary>
        /// Add new TKey-TValue pair
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value)
        {
            lock (sync)
            {
                dic.Add(key, value);
            }
        }

        /// <summary>
        /// Update TKey-TValue pair
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Update(TKey key, TValue value)
        {
            lock (sync)
            {
                if (!dic.ContainsKey(key))
                    return false;
                dic[key] = value;
            }
            return true;
        }

        /// <summary>
        /// Insert or Update TKey-TValue pair
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddOrUpdate(TKey key, TValue value)
        {
            lock (sync)
            {
                if (dic.ContainsKey(key))
                    dic[key] = value;
                else
                    dic.Add(key, value);
            }
        }

        /// <summary>
        /// Read TKey-TValue pair
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue Read(TKey key)
        {
            lock (sync)
            {
                if (!dic.ContainsKey(key))
                    return default(TValue);
                return dic[key];
            }
        }

        /// <summary>
        /// Remove TKey-TValue pair
        /// </summary>
        /// <param name="key"></param>
        public void Remove(TKey key)
        {
            lock (sync)
            {
                dic.Remove(key);
            }
        }

        /// <summary>
        /// Enumerator for TValue
        /// </summary>
        /// <returns></returns>
        public IEnumerator<TValue> GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns the enumerator of the dictionary
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return dic.GetEnumerator();
        }
    }
}