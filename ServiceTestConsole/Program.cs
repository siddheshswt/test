﻿// ***********************************************************************
// Assembly         : ServiceTestConsole
// Author           : Basantakumar Sahoo
// Created          : 01-29-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-25-2015
// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ServiceTestConsole
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Program
    {
       public static void Main(string[] args)
        {            
            CheckProxy();
        }

        private static void CheckProxy()
        {
            ServiceReference1.SSOTenaUserServiceClient newclient = new ServiceReference1.SSOTenaUserServiceClient();

            Console.WriteLine("Testing the CheckTenaUser Method....");
            ServiceReference1.TenaUser tenaUser = new ServiceReference1.TenaUser();
         
            bool status = false;

            string encrypt = newclient.EncryptToken("UserName=testindigo4545");

            try
            {
                status = newclient.CheckTenaUser("testindigo4545");
            }
            catch (Exception)
            {
                throw;
            }
           
            Console.WriteLine();

            if (!status)
            {                
                tenaUser = new ServiceReference1.TenaUser() { RoleId = 1, UserName = "testindigouser555" };
                string result = newclient.CreateTenaUser(tenaUser);
                if (string.IsNullOrEmpty(result))
                {
                    Console.WriteLine("User Created successfully");
                }
            }
            else
            {
                // Update
                tenaUser = new ServiceReference1.TenaUser() { RoleId = 2, UserName = "testindigo4545" };
                newclient.UpdateUserRole(tenaUser);
                newclient.UpdateUserStatus(tenaUser.UserName, true);
                newclient.UpdateUserStatus(tenaUser.UserName, false);
            }

            Console.WriteLine();

            var data = newclient.GetUserRoles();

            newclient.GetUserRoles().ToList().ForEach(d =>
            {
                if (d.RoleId > 0)
                {
                    Console.WriteLine(d.RoleName);
                }
            });

            // Request Login Method
            var token = newclient.RequestLogin("testindigo4545");
             Console.WriteLine(token);
             Console.WriteLine(newclient.DecryptToken(token));
             newclient.Close();
            Console.ReadLine();
        }       
    }
}
