﻿//-----------------------------------------------------------------------
// <copyright file="ISearch.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// ISearch Interface
    /// </summary>
    [ServiceContract]
    public interface ISearch
    {
        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of SearchBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetAuthorizeCustomers/{UserId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DropDownDataBusinessModel> GetAuthorizeCustomers(string userId);

        /// <summary>
        /// Get SearchResult
        /// </summary>
        /// <param name="searchText">string search Text</param>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of SearchBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetSearchResult/{searchText}/{UserId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SearchBusinessModel> GetSearchResult(string searchText, string userId);

        /// <summary>
        /// Get Patient's Details
        /// </summary>
        /// <param name="customerId">Customer's id</param>
        /// <param name="patientStatus">Patient's Status</param>
        /// <param name="patientType">Patient Type</param>
        /// <param name="orderNo">Order Number</param>
        /// <param name="searchId">Search Id</param>
        /// <param name="searchText">Search text</param>
        /// <param name="userId">User's Id</param>
        /// <param name="page">Page No</param>
        /// <param name="rowCount">No of Rows</param>
        /// <param name="sortColumn">Sort Column</param>
        /// <param name="sortOrder">Sort Order</param>
        /// <returns>List of SearchBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientDetails/{customerId}/{patientStatus}/{patientType}/{orderNo}/{searchId}/{searchText}/{userId}/{page}/{rowCount}/{sortColumn}/{sortOrder}",
        ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SearchBusinessModel> GetPatientDetails(string customerId, string patientStatus, string patientType, string orderNo, string searchId, string searchText, string userId, string page, string rowCount, string sortColumn, string sortOrder);

        /// <summary>
        /// Get Status DropDown
        /// </summary>
        /// <param name="statusIds">Customer's,Patient,carehome status</param>
        /// <param name="userId">User's Id</param>
        /// <returns>List of SearchBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetStatusDropdown/{statusIds}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SearchBusinessModel> GetStatusDropDown(string statusIds, string userId);
    }
}