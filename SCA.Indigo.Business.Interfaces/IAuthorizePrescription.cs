﻿//-----------------------------------------------------------------------
// <copyright file="IAuthorizePrescription.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{        
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// IAuthorizePrescription interface
    /// </summary>    
    [ServiceContract]

    public interface IAuthorizePrescription
    {
        /// <summary>
        /// Gets Patient details whose Prescription is pending for Authorization
        /// </summary>
        /// <param name="userName">User's Name</param>
        /// <param name="userId">User's Id</param>
        /// <returns>List of PrescriptionAuthorizationBusinessModel</returns>   
        [OperationContract]
        [WebGet(UriTemplate = "GetAuthorizePrescription/{userName}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PrescriptionAuthorizationBusinessModel> GetAuthorizePrescription(string userName, string userId);        

        /// <summary>
        /// Approve Patient's Prescription
        /// </summary>
        /// <param name="objPrescriptionNote">object of PrescriptionNoteBusinessModel</param>
        /// <returns>Boolean value</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "ApprovePrescription", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool ApprovePrescription(PrescriptionNoteBusinessModel objPrescriptionNote);

      /// <summary>
      /// Validate the User provided Authorization PIN
      /// </summary>
      /// <param name="userId">User's Id</param>
      /// <param name="PIN">User's Authorization PIN</param>
      /// <returns>Boolean value</returns>
        [OperationContract]
        [WebGet(UriTemplate = "ValidateUserAuthorizationPin/{UserId}/{pin}", BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool ValidateUserAuthorizationPin(string userId, string pin);
    }
}
