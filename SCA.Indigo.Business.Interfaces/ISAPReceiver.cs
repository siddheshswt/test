﻿//-----------------------------------------------------------------------
// <copyright file="ISAPReceiver.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.Xml.Linq;

    /// <summary>
    /// ISAPReceiver Interface
    /// </summary>
    [ServiceContract]
    public interface ISapReceiver
    {
        /// <summary>
        /// Save Customer's ProductList
        /// </summary>
        /// <param name="objXElement">List of Customer Products.</param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CustomerProductList")]
        void SaveCustomerProductList(XElement objXElement);

        /// <summary>
        /// Gets Customer Response
        /// </summary>
        /// <param name="objXElement">Data to be sent to Sap in XML format.</param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CustomerResponse", ResponseFormat = WebMessageFormat.Json)]
        void CustomerResponse(XElement objXElement);

        /// <summary>
        /// Get Order Response
        /// </summary>
        /// <param name="objXElement">Data to be sent to Sap in XML format.</param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "OrderResponse", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void OrderResponse(XElement objXElement);

        /// <summary>
        /// Receives Request from Sap for creating new Telephonic/Online Standard Order in Indigo Application
        /// </summary>
        /// <param name="objXElement">XML document containing Patient and Customer Number for whom, Order is to be created</param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CreateSapOrders", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void CreateSapOrders(XElement objXElement);
    }
}