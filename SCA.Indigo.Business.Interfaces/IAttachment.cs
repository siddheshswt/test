﻿//----------------------------------------------------------------------------------------------
// <copyright file="IAttachment.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------
namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;
    
    /// <summary>
    /// Attachment Interface
    /// </summary>
    [ServiceContract]
    public interface IAttachment
    {
        #region Attachment

        /// <summary>
        /// Uploads the attachment.
        /// </summary>
        /// <param name="attachmentBusinessViewModelList">The attachment business view model list.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UploadAttachment", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string UploadAttachment(List<AttachmentBusinessViewModel> attachmentBusinessViewModelList);

        /// <summary>
        /// Displays the attachment.
        /// </summary>
        /// <param name="typeId">The type identifier.</param>
        /// <param name="inputAttachmentType">Type of the input attachment.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "DisplayAttachment/{typeId}/{inputAttachmentType}/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<AttachmentBusinessViewModel> DisplayAttachment(string typeId, string inputAttachmentType, string userId);

        /// <summary>
        /// Removes the attachment.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RemoveAttachment", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool RemoveAttachment(List<string> attachmentListId);

        /// <summary>
        /// Gets the attachment path.
        /// </summary>
        /// <param name="attachmentListId">The attachment list identifier.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetAttachmentPath", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<string> GetAttachmentPath(List<string> attachmentListId);

        #endregion
    }
}
