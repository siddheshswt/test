﻿//-----------------------------------------------------------------------
// <copyright file="IOrder.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;    

    /// <summary>
    /// IOrder interface
    /// </summary>
    [ServiceContract]
    public interface IOrder
    {     
        /// <summary>
       /// Removes Product[/s] from Order
        /// </summary>
       /// <param name="productIdList">List of products</param>
       /// <returns>Boolean value</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RemoveProductFromOrder", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool RemoveProductFromOrder(Tuple<long, List<long>> productIdList);

        /// <summary>
        /// Save Sample Orders
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">Object of SampleOrderPatientBusinessModel</param>
        /// <returns>Boolean value</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveSampleOrders", ResponseFormat = WebMessageFormat.Json)]
        bool SaveSampleOrders(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel);

        /// <summary>
        /// Check Sample Orders
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">Object of SampleOrderPatientBusinessModel</param>
        /// <returns>sample orders</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "CheckSampleOrdersInHelix", ResponseFormat = WebMessageFormat.Json)]
        SampleOrderPatientBusinessModel CheckSampleOrdersInHelix(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel);

        /// <summary>
        /// This function will send order to helix service after getting confirmation from user.
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">Details of sample order</param>
        /// <returns>SampleOrderPatientBusinessModel object</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SendOrder", ResponseFormat = WebMessageFormat.Json)]
        SampleOrderPatientBusinessModel SendOrder(SampleOrderPatientBusinessModel sampleOrderPatientBusinessModel);

        /// <summary>
        /// Get CareHome Orders
        /// Save Orders
        /// </summary>
        /// <param name="orders">List of PatientOrdersBusinessModel></param>
        /// <returns>Return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveOrders", ResponseFormat = WebMessageFormat.Json)]
        bool SaveOrders(List<PatientOrdersBusinessModel> orders);        

        /// <summary>
        /// GetCareHomeOrders
        /// </summary>
        /// <param name="careHomeId">CareHome ID</param>
        /// <param name="userId">User Id</param>
        /// <returns>Return list of PatientOrdersBusinessModel&gt;</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCareHomeOrders/{careHomeId}/{isProductOrder}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PatientOrdersBusinessModel> GetCareHomeOrders(string careHomeId,string isProductOrder, string userId);

        /// <summary>
        /// Get CommunityOrders
        /// </summary>
        /// <param name="patientId"> Patient ID</param>
        /// <param name="userId">User Id</param>
        /// <returns>Return list of PatientOrdersBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCommunityOrders/{patientId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PatientOrdersBusinessModel> GetCommunityOrders(string patientId, string userId);

        /// <summary>
        /// Get Activated orders of Patient/Customer 
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        ///  /// <param name="customerId">Customer Id</param>
        /// <param name="userId">User Id</param>
        /// <returns>List GetActivatedOrders</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetActivatedOrders/{patientId}/{carehomeId}/{userId}/{showRushFlag}/{isProductOrder}/{isOneOff}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<PatientOrdersBusinessModel> GetActivatedOrders(string patientId, string carehomeId, string userId, string showRushFlag, string isProductOrder, string isOneOff);

        /// <summary>
        /// Get Patients due for next order 
        /// </summary>       
        /// <returns>List GetPatientsDueForNextOrder</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GenerateAutomaticCommunityOrders", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string GenerateAutomaticCommunityOrders();

        /// <summary>
        /// Get Carehomes due for next order 
        /// </summary>
        /// <returns> No of Carehomes Due For NextOrder</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GenerateAutomaticCareHomeOrders", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string GenerateAutomaticCareHomeOrders();

        /// <summary>
        ///Automatic Orders Generate Criteria
        /// </summary>
        /// <param name="sampleOrderPatientBusinessModel">Object of AutomaticOrderBusinessModel</param>
        /// <returns>Boolean value</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GenerateAutomaticOrderCriteria", ResponseFormat = WebMessageFormat.Json)]
        List<AutomaticOrderStatusBusinessModel> GenerateAutomaticOrderCriteria(AutomaticOrderCriteriaBusinessModel automaticOrderBusinessModel);

        /// <summary>
        /// GetCareHomeDetailsForProductOrder
        /// </summary>
        /// <param name="objcareHome"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetCareHomeDetailsForProductOrder", ResponseFormat = WebMessageFormat.Json)]
        CareHomeBusinessModel GetCareHomeDetailsForProductOrder(CareHomeBusinessModel objcareHome);

        /// <summary>
        /// Get RecentOrders
        /// </summary>
        /// <param name="searchId">The search identifier.</param>
        /// <param name="userId">user Id</param>
        /// <param name="recentOrderType">Type of the recent order.</param>
        /// <returns>
        /// Return list of PatientOrdersBusinessModel
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRecentOrders/{searchId}/{userId}/{recentOrderType}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PatientOrdersBusinessModel> GetRecentOrders(string searchId, string userId, string recentOrderType);

        /// <summary>
        /// Gets Community Order Details: ADP & Note
        /// </summary>
        /// <param name="objComminityOrderDetails"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetOrderActivationDetails", ResponseFormat = WebMessageFormat.Json)]
        OrderActivationDetailsBusinessModel GetOrderActivationDetails(OrderActivationDetailsBusinessModel objCommunityOrderDetails);
    }
}
