﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : basahoo
// Created          : 07-04-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="ICustomerInformation.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// Customer Information Interface
    /// </summary>
    [ServiceContract]
    public interface ICustomerInformation 
    {
        /// <summary>
        /// Get Customer Information Details
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomerInformation/{userId}/{customerId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<CustomerBusinessModel> GetCustomerInformationDetails(string userId, string customerId);

        /// <summary>
        /// Get Customer Prescription Approval Information Details
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomerPrescriptionApprovals/{userId}/{customerId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<CustomerPrescriptionApprovalBusinessModel> GetCustomerPrescriptionApprovals(string userId, string customerId);

        /// <summary>
        /// Get Customer Information Details
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveCustomerInformation", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool SaveCustomerInformationDetails(CustomerBusinessModel customerBusinessModel);

        /// <summary>
        /// Remove approved users
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RemoveCustomerPrescriptionApproval", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool RemoveUserPrescription(CustomerBusinessModel customerBusinessModel);

        /// <summary>
        /// save approved users
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveAuthorisedUserPrescription", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool SaveAuthorisedUserPrescription(CustomerBusinessModel customerBusinessModel);

        /// <summary>
        /// Get Customer Active Patient
        /// </summary>
        /// <param name="customerBusinessModel">The customer business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CheckActivePatientsByCustomer", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool CheckActivePatientsByCustomer(CustomerBusinessModel customerBusinessModel);

        /// <summary>
        /// Gets the user details by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="modifyBy">The modify by.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetUserDetailsByUserId/{userId}/{modifyBy}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<CustomerPrescriptionApprovalBusinessModel> GetUserDetailsByUserId(string userId, string modifyBy);

        #region GetHierarchyLinkData

        /// <summary>
        /// Gets the hierarchy link data.
        /// </summary>
        /// <param name="medicalStaffAnalysisInfoBusinessModel">The medical staff analysis information business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetHierarchyLinkData", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<DropDownDataBusinessModel> GetHierarchyLinkData(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel);

        #endregion
	
	}
}
