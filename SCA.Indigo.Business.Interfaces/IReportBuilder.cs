﻿//----------------------------------------------------------------------------------------------
// <copyright file="IReportBuilder.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// Report Builder Interface
    /// </summary>
    [ServiceContract]
    public interface IReportBuilder
    {
        /// <summary>
        /// Saves the report builder.
        /// </summary>
        /// <param name="reportBuilderBusinessViewModel">The report builder business view model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveReportBuilder")]
        string SaveReportBuilder(ReportFormatBusinessModel reportBuilderBusinessViewModel);

        /// <summary>
        /// Gets the report format.
        /// </summary>
        /// <param name="reportFormatId">The report format identifier.</param>
        /// <returns>Report Format data</returns>
        [OperationContract]
        [WebGet( UriTemplate = "GetReportFormat/{reportFormatId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ReportFormatBusinessModel> GetReportFormat(string reportFormatId);

        /// <summary>
        /// Gets the report format drop down.
        /// </summary>
        /// <returns>Report formats</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetReportFormatDropDown", ResponseFormat = WebMessageFormat.Json)]
        List<ReportFormatBusinessModel> GetReportFormatDropDown();

        /// <summary>
        /// Get report data
        /// </summary>
        /// <param name="reportBuilderCriteria">Report Builder Criteria</param>
        /// <returns>report data</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetReportData", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ReportDataBusinessModel> GetReportData(ReportBuilderCriteria reportBuilderCriteria);

    }
}
