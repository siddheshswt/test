﻿//-----------------------------------------------------------------------
// <copyright file="IIncrementalUpdateSearchIndex.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using SCA.Indigo.Model;

    /// <summary>
    /// IIncrementalUpdateSearchIndex Interface
    /// </summary>
    [ServiceContract]
    public interface IIncrementalUpdateSearchIndex
    {
       /// <summary>
       /// Read XML Files From Folder
       /// </summary>
       /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ReadXMLFilesFromFolder", ResponseFormat = WebMessageFormat.Json)]
        bool ReadXMLFilesFromFolder();
    }
}
