﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Sachin
// Created          : 09-Jun-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="IMassChangesService.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    [ServiceContract]
    public interface IMassDataChanges
    {        
        /// <summary>
        /// Download Master Mass Data Change For Carehome
        /// </summary>
        /// <param name="masterMassDataChangeBusinessModel"></param>
        /// <returns>list of details</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "DownloadMasterMassDataChange", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MasterMassDataChangeBusinessModel DownloadMasterMassDataChange(MasterMassDataChangeBusinessModel masterMassDataChangeBusinessModel);

		/// <summary>
		/// Process mass update
		/// </summary>
		/// <param name="massChangesBusinessModel"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "ProcessMassUpdate", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
		string ProcessMassUpdate(MassDataBusinessModel massChangesBusinessModel);
    }
}
