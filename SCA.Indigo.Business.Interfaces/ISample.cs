﻿//// ***********************************************************************
// Assembly         : SCA.Indigo.Business.Interfaces
// Author           : sphapale 
// Created          : 09-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="ISample.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;   

    /// <summary>
    /// ISample Interface
    /// </summary>
    [ServiceContract]
    public interface ISample
    {
        /// <summary>
        /// Test for Service
        /// </summary>
        /// <param name="value">Test data</param>
        /// <returns>return string</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetDataJson/{value}", ResponseFormat = WebMessageFormat.Json)]
        string GetDataJson(string value);       
    }
}
