﻿//-----------------------------------------------------------------------
// <copyright file="IAccountManagement.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;

    /// <summary>
    /// IAccountManagement Interface
    /// </summary>
    [ServiceContract]
	public interface INDDCalculation
    {
        /// <summary>
        /// Validate New Password 
        /// </summary>
        /// <param name="userBusinessModel">Instance of UsersBusinessModel</param>
        /// <returns>return boolean</returns>
		[OperationContract]
		[WebInvoke(UriTemplate = "GetNDDForNewCommunityPatient/{patientId}/{patientType}", ResponseFormat = WebMessageFormat.Json)]
		string GetNDDForNewCommunityPatient(string patientId, string patientType, NDDParametersBusinessModel nddParamaters);

		/// <summary>
		/// Validate New Password 
		/// </summary>
		/// <param name="userBusinessModel">Instance of UsersBusinessModel</param>
		/// <returns>return boolean</returns>
		[OperationContract]
		[WebInvoke(UriTemplate = "GetNDDForPatient", ResponseFormat = WebMessageFormat.Json)]
		string GetNDDForPatient(NDDParametersBusinessModel nddParamaters); 
    }
}
