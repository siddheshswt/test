﻿//-----------------------------------------------------------------------
// <copyright file="IAccountManagement.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using SCA.Indigo.Model;

    /// <summary>
    /// IAccountManagement Interface
    /// </summary>
    [ServiceContract]
    public interface IGoogleSearch
    {
        /// <summary>
        /// Google Search Text
        /// </summary>
		/// <param name="searchText">Instance of string</param>
        /// <returns>return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetGoogleSearchResults", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<SearchBusinessModel> GetGoogleSearchResults(SearchCriteriaBusinessModel objSearchCriteria);

        /// <summary>
        /// Builds the google search Index
        /// </summary>		
        /// <returns>return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "IndexBuilder", ResponseFormat = WebMessageFormat.Json)]
        bool IndexBuilder();

		/// <summary>
        /// Builds the google search Index
        /// </summary>		
        /// <returns>return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "BuildGoogleSearch/{indigoId}/{objectType}", ResponseFormat = WebMessageFormat.Json)]
        bool BuildGoogleSearch(string indigoId, string objectType);
    }
}
