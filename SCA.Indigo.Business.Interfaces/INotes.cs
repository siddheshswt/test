﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.Interfaces
// Author           : Arvind Nishad
// Created          : 29-04-2015
//
// ***********************************************************************
// <copyright file="INotes.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Notes Interface</summary>
// ***********************************************************************
namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// INotes Interface
    /// </summary>
    [ServiceContract]
    public interface INotes
    {     
        /// <summary>
        /// Save Notes Details
        /// </summary>
        /// <param name="objNotesBusinessModel">NotesBusinessModel objNotesBusinessModel</param>
        /// <returns>return Note Id</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveNote", ResponseFormat = WebMessageFormat.Json)]
        long SaveNote(NotesBusinessModel objNotesBusinessModel);

        /// <summary>
        /// Get Notes Details
        /// </summary>
        /// <param name="careHomeId">string NoteType</param>
        /// <param name="userId">string NoteTypeId</param>
        /// <returns>return Note Business Model</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetNotes/{noteType}/{noteTypeId}/{userId}/{alertNotes}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<NotesBusinessModel> GetNotes(string noteType, string noteTypeId, string userId, string alertNotes);

        /// <summary>
        /// Save note alert details
        /// </summary>
        /// <param name="objNotesBusinessModel">List of NotesBusinessModel class object</param>
        /// <returns> </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveAlert", ResponseFormat = WebMessageFormat.Json)]
        long SaveAlert(List<NotesBusinessModel> objNotesBusinessModel);

        /// <summary>
        /// Save Notes Details For Copy Patient
        /// </summary>
        /// <param name="objNotesBusinessModel">NotesBusinessModel objNotesBusinessModel</param>
        /// <returns>return Note Id</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveNoteForCopyPatient", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        void SaveNoteForCopyPatient(List<NotesBusinessModel> objNotesBusinessModel);
    }
}
