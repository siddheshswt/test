﻿//-----------------------------------------------------------------------
// <copyright file="IMenu.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// IMenu Interface
    /// </summary>
    [ServiceContract]
    public interface IMenu
    {
        /// <summary>
        /// Get UserMenuDetails
        /// </summary>
        /// <param name="userName">User Name</param>
        /// <returns>return list of UserMenuDetailsBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetUserMenuDetails/{userName}/{languageId}", ResponseFormat = WebMessageFormat.Json)]
        List<UserMenuDetailsBusinessModel> GetUserMenuDetails(string userName, string languageId);
    }
}
