﻿//-----------------------------------------------------------------------
// <copyright file="IPatient.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// IPatient interface
    /// </summary>
    [ServiceContract]
    public interface IPatient
    {
        /// <summary>
        /// Get PatientDetails
        /// </summary>
        /// <param name="objPatientBusinessModel">Instance of PatientBusinessModel</param>
        /// <returns>
        /// return PatientBusinessModel
        /// </returns>
         [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetPatientDetails", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         PatientBusinessModel GetPatientDetails(PatientBusinessModel objPatientBusinessModel);

         /// <summary>
         /// Get CareHomeDetails
         /// </summary>
         /// <param name="objCareHomeBusinessModel">Instance of CareHomeBusinessModel</param>
         /// <returns>
         /// Return list of CareHomeBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "GetCareHomeDetails", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         List<CareHomeBusinessModel> GetCareHomeDetails(CareHomeBusinessModel objCareHomeBusinessModel);

         /// <summary>
         /// Add PatientNote
         /// </summary>
         /// <param name="objPatientNotesBusinessModel">Instance of PatientNotesBusinessModel</param>
         /// <returns>
         /// Return boolean
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "AddPatientNote", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
         bool AddNote(PatientNotesBusinessModel objPatientNotesBusinessModel);

         /// <summary>
         /// Load PatientNotes
         /// </summary>
         /// <param name="objPatientNotesBusinessModel">Instance of PatientNotesBusinessModel</param>
         /// <returns>
         /// Return list of PatientNotesBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "LoadPatientNotes", ResponseFormat = WebMessageFormat.Json)]
         List<PatientNotesBusinessModel> LoadPatientNotes(PatientNotesBusinessModel objPatientNotesBusinessModel);

         /// <summary>
         /// Product List
         /// </summary>
         /// <param name="objPrescriptionBusinessModel">Instance of PrescriptionBusinessModel</param>
         /// <returns>
         /// Return list of PrescriptionBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "ProductList", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         List<PrescriptionBusinessModel> PrescriptionDetails(PrescriptionBusinessModel objPrescriptionBusinessModel);

         /// <summary>
         /// Get patientStaffDetails
         /// </summary>
         /// <param name="objMedicalStaffAnalysisInfoBusinessModel">Instance of MedicalStaffAnalysisInfoBusinessModel</param>
         /// <returns>
         /// Return list of MedicalStaffAnalysisInfoBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "GetpatientStaffDetails", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         List<MedicalStaffAnalysisInfoBusinessModel> GetMedicalStaffAnalysisInfo(MedicalStaffAnalysisInfoBusinessModel objMedicalStaffAnalysisInfoBusinessModel);

         /// <summary>
         /// Get CustomerForMedicalAnalysis
         /// </summary>
         /// <param name="medicalStaffAnalysisInfoBusinessModel">The medical staff analysis information business model.</param>
         /// <returns>
         /// Return list of MedicalStaffAnalysisInfoBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "GetCustomerForMedicalAnalysis", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         List<MedicalStaffAnalysisInfoBusinessModel> GetCustomerMedicalAnalysis(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel);

         /// <summary>
         /// Get Medical staffList
         /// </summary>
         /// <param name="medicalStaffAnalysisInfoBusinessModel">The medical staff analysis information business model.</param>
         /// <returns>
         /// Return list of Medical Staff AnalysisInfoBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "GetStaffSearchResult", ResponseFormat = WebMessageFormat.Json)]
         List<MedicalStaffAnalysisInfoBusinessModel> GetStaffSearchResult(MedicalStaffAnalysisInfoBusinessModel medicalStaffAnalysisInfoBusinessModel);                       

         /// <summary>
         /// Add MedicalStaffData
         /// </summary>
         /// <param name="objMedicalStaffAnalysisInfoBusinessModel">Instance of MedicalStaffAnalysisInfoBusinessModel</param>
         /// <returns>
         /// Return MedicalStaffAnalysisInfoBusinessModel
         /// </returns>
         [OperationContract]
         [WebInvoke(UriTemplate = "AddMedStaffAnalysisInfo", ResponseFormat = WebMessageFormat.Json)]
         MedicalStaffAnalysisInfoBusinessModel AddMedStaffAnalysisInfo(MedicalStaffAnalysisInfoBusinessModel objMedicalStaffAnalysisInfoBusinessModel);

         /// <summary>
         /// Save Patient details for status removed and stopped
         /// </summary>
         /// <param name="objPatientBusinessModel">object of PatientBusinessModel</param>
         /// <returns>
         /// Boolean value
         /// </returns>
         [OperationContract]
         [WebInvoke(Method = "POST", UriTemplate = "SavePatientDetailsRemovedStopped", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
         bool SavePatientDetailsRemovedStopped(PatientBusinessModel objPatientBusinessModel);

         /// <summary>
         /// Save PatientDetails
         /// </summary>
         /// <param name="objPatientBusinessModel">Instance of PatientBusinessModel</param>
         /// <returns>
         /// Return boolean
         /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SavePatientDetails", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        PatientBusinessModel SavePatientDetails(PatientBusinessModel objPatientBusinessModel);

        /// <summary>
        /// Gets Patient's Basic Details
        /// </summary>
        /// <param name="patientId">Patient's Id</param>
        /// <param name="careHomeId">The care home identifier.</param>
        /// <param name="userId">User's Id</param>
        /// <param name="userId">isProductOrder</param>
        /// <returns>
        /// List of PatientBusinessModel
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientBasicDetails/{patientId}/{careHomeId}/{isProductOrder}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<PatientBusinessModel> GetPatientBasicDetails(string patientId, string careHomeId,string isProductOrder, string userId);

        #region Contactinfo Section

        /// <summary>
        /// Gets the contact information.
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">The object patient contact information business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetPatientContacts", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<PatientContactInfoBusinessModel> GetContactInfo(PatientContactInfoBusinessModel objPatientContactInfoBusinessModel);

        /// <summary>
        /// Save patient Contact info
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">Contact Information</param>
        /// <returns>
        /// boolean value
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveContactInfo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool SaveContactInfo(PatientContactInfoBusinessModel objPatientContactInfoBusinessModel);

        /// <summary>
        /// Remove Contact Info
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">Contact Information</param>
        /// <returns>
        /// Boolean value
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "RemoveContactInfo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool RemoveContactInfo(PatientContactInfoBusinessModel objPatientContactInfoBusinessModel);

        #endregion       

        /// <summary>
        /// Check if Products Exist in Prescription
        /// </summary>
        /// <param name="patientPrescriptionViewModel">object of PatientPrescriptionViewModel</param>
        /// <returns>
        /// Boolean value
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CheckProductExist", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool CheckProductExist(PrescriptionBusinessModel patientPrescriptionViewModel);

        /// <summary>
        /// Get Patient Details to be Edited
        /// </summary>
        /// <param name="userType">User's type</param>
        /// <param name="roleName">User's Role Name</param>
        /// <returns>
        /// List of PatientBusinessModel
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientEditDetails/{userType}/{roleName}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<PatientBusinessModel> GetPatientEditDetails(string userType, string roleName);

        /// <summary>
        /// Get PatientTypes Assigned to Customer
        /// </summary>
        /// <param name="customerId">Customer's Id</param>
        /// <returns>
        /// List of PatientBusinessModel
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomerPatientType/{customerId}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<PatientBusinessModel> GetCustomerPatientType(string customerId, string userId);

        /// <summary>
        /// Loads Nurse details for given patientId
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <returns>
        /// Nurse Comments Details
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "LoadNurseDetails/{patientId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        NurseCommentsBusinessModel LoadNurseDetails(string patientId);

        /// <summary>
        /// Save patient Contact info for Copy patient
        /// </summary>
        /// <param name="objPatientContactInfoBusinessModel">Contact Information</param>
        /// <returns>
        /// boolean value
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveContactInfoForCopyPatient", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void SaveContactInfoForCopyPatient(List<PatientContactInfoBusinessModel> objPatientContactInfoBusinessModel);

		
		/// <summary>
		/// Return the Patient NDD
		/// </summary>
		/// <param name="patientId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(UriTemplate = "GetPatientCurrentNDD/{patientId}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
		string GetPatientCurrentNDD(string patientId, string userId);

        /// <summary>
        /// Checks wheather Patient has Active Order
        /// </summary>
        /// <param name="objPatientBusinessModel"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "IsPatientOrderActivated", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool IsPatientOrderActivated(PatientBusinessModel objPatientBusinessModel);

        /// <summary>
        /// Calculates NDD when new product is added in prescription
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientNDDForFirstProduct/{patientId}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetPatientNDDForFirstProduct(string patientId, string userId);        
    }
}
