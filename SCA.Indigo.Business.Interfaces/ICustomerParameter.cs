﻿//-----------------------------------------------------------------------
// <copyright file="ICustomerParameter.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// ICustomerParameter Interface
    /// </summary>
    [ServiceContract]
    public interface ICustomerParameter
    {
        /// <summary>
        /// Get Postcode Matrix for Customer Parameter maintenance
        /// </summary>
        /// <returns>List of Postcodes And Delivery days</returns>       
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomerPostcodeMatrix/{customerId}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<PostCodeMatrixBusinessModel> GetCustomerPostcodeMatrix(string customerId, string userId);

        /// <summary>
        /// Save Customer's PostcodeMatrix details
        /// </summary>
        /// <param name="postcodeMatrixList">List of Postcodes and their Matrix</param>
        /// <returns>True if Save is successful else returns False</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveCustomerPostcode", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool SaveCustomerPostcode(List<PostCodeMatrixBusinessModel> postcodeMatrixList);

        /// <summary>
        /// Get generic list by passing id
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="userId"></param>
        /// <returns>list of values</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetMasterListById/{listTypeId}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<ListTypeBusinessModel> GetMasterListById(string listTypeId, string userId);

        /// <summary>
        /// Get Patient Type matrix details bu customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>list of types</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientTypeMatrixByCustomerId/{customerId}", ResponseFormat = WebMessageFormat.Json)]
        List<PatientTypeMatrixBusinessModel> GetPatientTypeMatrixByCustomerId(string customerId);

        /// <summary>
        /// Get Patient Associated With Patient
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="patientType"></param>
        /// <param name="userId"></param>
        /// <returns>business model</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientAssociatedWithPatient/{customerId}/{patientType}/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<PatientTypeMatrixBusinessModel> GetPatientAssociatedWithPatient(string customerId, string patientType, string userId);

        /// <summary>
        /// Get Patient Type matrix details bu customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>list of types</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomerParameter/{customerId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CustomerParameterBusinessModel GetCustomerParameter(string customerId, string userId);

        /// <summary>
        /// Save Patient Matrix Data
        /// </summary>
        /// <param name="patientTypeMatrixMainBusinessModel"></param>
        /// <returns>true or false</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SavePatientTypeMatrixDetails", ResponseFormat = WebMessageFormat.Json)]
        bool SavePatientTypeMatrixDetails(PatientTypeMatrixMainBusinessModel patientTypeMatrixMainBusinessModel);

        /// <summary>
        /// Save Customer parameter information
        /// </summary>
        /// <param name="patientTypeMatrixMainBusinessModel"></param>
        /// <returns>true or false</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveCustomerParameter", ResponseFormat = WebMessageFormat.Json)]
        bool SaveCustomerParameter(CustomerParameterBusinessModel customerParameter);

        /// <summary>
        /// Retruns the removed patients reason Code for customer
        /// </summary>
        /// <param name="customerId">customerId of ListType Table</param>
        /// <param name="languageId">LanguageId from TransLang table</param>
        /// <returns>return the list ListTypeBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRemovedPatientReasonCode/{customerId}/{languageId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetRemovedPatientReasonCode(string customerId, string languageId);

        /// <summary>
        /// check whether the reasonCode master data mapping can be removed for the customer
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="reasonCodeId">reasonCodeId</param>
        /// <returns>return true/false, stating whether the reasoncode mapping can be removed or not</returns>
        [OperationContract]
        [WebGet(UriTemplate = "RemoveReasonCodeMapping/{customerId}/{reasonCodeId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool RemoveReasonCodeMapping(string customerId, string reasonCodeId, string userId);

        /// <summary>
        /// Retruns the stopped patients reason Code for customer
        /// </summary>
        /// <param name="customerId">customerId of ListType Table</param>
        /// <param name="languageId">LanguageId from TransLang table</param>
        /// <returns>return the list ListTypeBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetStoppedPatientReasonCode/{customerId}/{languageId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetStoppedPatientReasonCode(string customerId, string languageId);

        /// <summary>
        /// Get clinical contact value mapping
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="roleType"></param>
        /// <param name="userId"></param>
        /// <returns>list of value</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetClinicalContactValueMappingByCustomerId/{customerId}/{roleType}/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<ClinicalContactValueMappingBusinessModel> GetClinicalContactValueMappingByCustomerId(string customerId, string roleType, string userId);

        /// <summary>
        /// Get Clinical Contact Role Mapping By CustomerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>list of values</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetClinicalContactRoleMappingByCustomerId/{customerId}", ResponseFormat = WebMessageFormat.Json)]
        List<ClinicalContactMappingBusinessModel> GetClinicalContactRoleMappingByCustomerId(string customerId);

        /// <summary>
        /// save ClinicalRoleMapping
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>true or false</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveClinicalRoleMapping", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool SaveClinicalRoleMapping(List<ClinicalContactMappingBusinessModel> objClinicalRoleMapping);

        /// <summary>
        /// Get Parent Value For Selected Role
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="childId"></param>
        /// <param name="userId"></param>
        /// <returns>list of value</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetParentValueForSelectedRole/{customerId}/{childId}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<ClinicalContactValueMappingBusinessModel> GetParentValueForSelectedRole(string customerId, string childId, string userId);

        /// <summary>
        /// Save clinical values
        /// </summary>
        /// <param name="clinicalContactMappingBusinessModel"></param>
        /// <returns>true or false</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveClinicalContactValueMapping", ResponseFormat = WebMessageFormat.Json)]
        bool SaveClinicalContactValueMapping(ClinicalContactMappingBusinessModel clinicalContactMappingBusinessModel);

        /// <summary>
        /// Get role name
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="userId"></param>
        /// <returns>role name</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRoleNameForClinicalContact/{roleId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetRoleNameForClinicalContact(string roleId, string userId);

        /// <summary>
        /// Get role name
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="userId"></param>
        /// <param name="customerId"></param>
        /// <returns>role name</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRoleNameForClinicalContactByChildId/{roleId}/{userId}/{customerId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetRoleNameForClinicalContactByChildId(string roleId, string userId, string customerId);

        /// <summary>
        /// Get role type 
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns>list value</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetRoleTypeForClinicalContact/{customerId}/{languageId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetRoleTypeForClinicalContact(string customerId, string languageId, string userId);

        /// <summary>
        /// Remove Clinical Contact Analysis Mapping
        /// </summary>
        /// <param name="medicalAnalysisInfoBusinessModel"></param>
        /// <returns>true or false</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "RemoveClinicalContactAnalysisMapping", ResponseFormat = WebMessageFormat.Json)]
        bool RemoveClinicalContactAnalysisMapping(MedicalStaffAnalysisInfoBusinessModel medicalAnalysisInfoBusinessModel);
    }
}
