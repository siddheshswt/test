﻿//-----------------------------------------------------------------------
// <copyright file="IProduct.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// IProduct Interface
    /// </summary>
    [ServiceContract]
    public interface IProduct
    {
        /// <summary>
        /// Get Products assigned to Customer
        /// </summary>
        /// <param name="customerId">Customer's Id</param>
        /// <returns>List of Products</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetProduct/{customerId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProductBusinessModel> ProductDetails(string customerId);

        /// <summary>
        /// Get Sample Product
        /// </summary>
        /// <returns>list of sample</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetSampleProduct/{customerId}/{sampleOrderCustomerProductType}/{sampleOrderTenaProductType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<SampleProductBusinessModel> SampleProductDetails(string customerId, string sampleOrderCustomerProductType, string sampleOrderTenaProductType);

        /// <summary>
        /// Get Sample Product
        /// </summary>
        /// <param name="searchText">Search input</param>
        /// <returns>list of SampleProductBusinessModel</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetSampleProductDetails", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<SampleProductBusinessModel> GetSampleProductDetails(SampleProductBusinessModel sampleProductBusinessModel);

        /// <summary>
        /// Get SearchResult Product
        /// </summary>
        /// <param name="searchText">search Text</param>
        /// <returns>Return list of ProductBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetSearchResultProduct/{searchText}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProductBusinessModel> GetSearchResult(string searchText);

        /// <summary>
        /// Get Product's Details
        /// </summary>
        /// <param name="searchText">Product Search input</param>
        /// <param name="customerId">Customer's Id</param>
        /// <returns>List of ProductBusinessModel</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetProductDetails", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ProductBusinessModel> GetProductDetails(ProductBusinessModel productBusinessModel);

        /// <summary>
        /// Get list of product name and id
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// /// <param name="userId">User identifier</param>
        /// <returns>Return list of products</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetProductsOnCustomerId/{customerId}/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<DropDownDataBusinessModel> GetProductsOnCustomerId(string customerId, string userId);
    }
}
