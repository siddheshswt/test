﻿//-----------------------------------------------------------------------
// <copyright file="IAccountManagement.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;

    /// <summary>
    /// IAccountManagement Interface
    /// </summary>
    [ServiceContract]
    public interface IAccountManagement
    {
        /// <summary>
        /// Validate New Password 
        /// </summary>
        /// <param name="userBusinessModel">Instance of UsersBusinessModel</param>
        /// <returns>return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "ValidateNewPassword", ResponseFormat = WebMessageFormat.Json)]
        bool ValidateNewPassword(UsersBusinessModel userBusinessModel);

        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="userBusinessModel">Instance of UsersBusinessModel</param>
        /// <returns>return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "ChangePassword", ResponseFormat = WebMessageFormat.Json)]
        bool ChangePassword(UsersBusinessModel userBusinessModel);

        /// <summary>
        /// Get User Details
        /// </summary>
        /// <param name="userBusinessModel">Instance of UsersBusinessModel</param>
        /// <returns>return UsersBusinessModel</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetUserDetails", ResponseFormat = WebMessageFormat.Json)]
        UsersBusinessModel GetUserDetails(UsersBusinessModel userBusinessModel);

        /// <summary>
        /// Get User Details For Reset & Unlock
        /// </summary>
        /// <param name="userResetUnlockDetails">Instance of UserResetUnlockDetailsBusinessModel</param>
        /// <returns>List of UserResetUnlockSummaryBusinessModel</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetUserResetUnlockSummary", ResponseFormat = WebMessageFormat.Json)]
        List<UserResetUnlockSummaryBusinessModel> GetUserResetUnlockSummary(UserResetUnlockDetailsBusinessModel userResetUnlockDetails);

        /// <summary>
        /// Locks or Unlock User
        /// </summary>
        /// <param name="objLockUnlockUser">Instance of LockUnlockBusinessModel</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "LockUnlcokUser", ResponseFormat = WebMessageFormat.Json)]
        bool LockUnlcokUser(LockUnlockBusinessModel objLockUnlockUser);

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="userBusinessModel">Instance of UsersBusinessModel</param>
        /// <returns>return boolean</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "ResetPassword", ResponseFormat = WebMessageFormat.Json)]
        bool ResetPassword(UsersBusinessModel userBusinessModel);

        /// <summary>
        /// Capture Last access time of Home Page for User
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "TrackHomePageVisit", ResponseFormat = WebMessageFormat.Json)]
        bool TrackHomePageVisit(string userId);

        /// <summary>
        /// Check For Interaction Assigned For Notification
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "CheckForNotification/{UserId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<NotificationBusinessModel> CheckForNotification(string userId);
    }
}
