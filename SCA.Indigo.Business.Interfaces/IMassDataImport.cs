﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Sachin
// Created          : 12-Jun-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="IMassDataImport.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.Interfaces
{
    using SCA.Indigo.Business.BusinessModels;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    /// <summary>
    /// Mass Data Import Interface
    /// </summary>
    [ServiceContract]
    public interface IMassDataImport
    {
        #region Process Data Import

        /// <summary>
        /// Upload Customer Clinical Contact
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>true or false</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ProcessImportData", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        string ProcessImportData(MassDataBusinessModel massChangesBusinessModel);

        #endregion     
    }
}
