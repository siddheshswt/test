﻿//-----------------------------------------------------------------------
// <copyright file="IAuditLog.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// Audit Log Interface
    /// </summary>
    [ServiceContract]
    public interface IAuditLog
    {
        /// <summary>
        /// Gets Patient's details and AuditLog
        /// </summary>
        /// <param name="auditLogData">The audit log data.</param>
        /// <returns>
        /// Patient's Audit Logs
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetPatientOrCareHomeAuditLog", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AuditLogBusinessModel GetPatientOrCareHomeAuditLog(AuditLogBusinessModel auditLogData);

        /// <summary>
        /// Gets the Search Results for Customer/CareHome/Patient search on AuditLog scren
        /// </summary>
        /// <param name="searchRequest">Object of AuditLogSearchBusinessModel containing search keyword and user Id who requested search</param>
        /// <returns>
        /// List of Search results including Id, SAP Id, Name of the searched Entity
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetAuditLogSearchResults", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<CommonSearchBusinessModel> GetAuditLogSearchResults(CommonSearchBusinessModel searchRequest);

        /// <summary>
        /// Saves user's Remarks/Notes on AuditLog
        /// </summary>
        /// <param name="auditLogs">List of Audit Log Report Business model</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveAuditLogRemarks", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool SaveAuditLogRemarks(List<AuditLogReportBusinessModel> auditLogs);   
        
    }
}
