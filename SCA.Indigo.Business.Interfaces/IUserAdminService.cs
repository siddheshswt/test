﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.Interfaces
// Author           : sphapale / bsahoo
// Created          : 09-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="IUserAdminService.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// Service contract for User Admin Service
    /// </summary>
    [ServiceContract]
    public interface IUserAdminService
    {
        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="customerUser">The customer user.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isInteraction">The is interaction.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetAllUsers/{customerId}/{customerUser}/{userId}/{isInteraction}", ResponseFormat = WebMessageFormat.Json)]
        List<UsersBusinessModel> GetAllUsers(string customerId, string customerUser, string userId, string isInteraction = "false");

        /// <summary>
        /// Get all roles
        /// </summary>
        /// <returns>
        /// list of roles
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetAllRoles", ResponseFormat = WebMessageFormat.Json)]
        List<UsersBusinessModel> GetAllRoles();

        /// <summary>
        /// Get mapped customer
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// list of customers
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetMappedUserToCustomer/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<UsersBusinessModel> GetMappedUserToCustomer(string userId);

        /// <summary>
        /// Get menu details
        /// </summary>
        /// <param name="roleId">User's Role Id</param>
        /// <returns>
        /// list of menu items
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetMenuDetails/{RoleId}/{languageId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<UserMenuDetailsBusinessModel> GetMenuDetails(string roleId, string languageId);

        /// <summary>
        /// Get Sdmin User Id
        /// </summary>
        /// <param name="userBusinessModel">User details</param>
        /// <returns>
        /// admin user id
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetAdminUserId", ResponseFormat = WebMessageFormat.Json)]
        UsersBusinessModel GetAdminUserId(UsersBusinessModel userBusinessModel);

        /// <summary>
        /// Save customer mapping
        /// </summary>
        /// <param name="userAdminBusinessModel">User's details in UserAdminBusinessModel object</param>
        /// <returns>
        /// response data
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveUserCustomerMapping", ResponseFormat = WebMessageFormat.Json)]
        bool SaveUserCustomerMapping(UserAdminBusinessModel userAdminBusinessModel);

        /// <summary>
        /// Get All User Customer Mapping
        /// </summary>
        /// <param name="userName">User's name</param>
        /// <param name="roleId">User's Role Id</param>
        /// <param name="sapCustomerId">Customer's SAP Id</param>
        /// <returns>
        /// Response Data
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetAllUserCustomerMapping/{userName}/{roleId}/{sapCustomerId}", ResponseFormat = WebMessageFormat.Json)]
        List<UserMenuDetailsBusinessModel> GetAllUserCustomerMapping(string userName, string roleId, string sapCustomerId);

        /// <summary>
        /// Save role mapping
        /// </summary>
        /// <param name="userAdminBusinessModel">User's detail in UserAdminBusinessModel object</param>
        /// <returns>
        /// True/False
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveRoleMenuMapping", ResponseFormat = WebMessageFormat.Json)]
        bool SaveRoleMenuMapping(UserAdminBusinessModel userAdminBusinessModel);

        /// <summary>
        /// Saves the new role.
        /// </summary>
        /// <param name="usersBusinessModel">The users business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveNewRole", ResponseFormat = WebMessageFormat.Json)]
        bool SaveNewRole(UsersBusinessModel usersBusinessModel);

        /// <summary>
        /// Save User Role Mapping
        /// </summary>
        /// <param name="userAdminBusinessModel">User's detail in UserAdminBusinessModel object</param>
        /// <returns>
        /// True/False
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveUserRoleMapping", ResponseFormat = WebMessageFormat.Json)]
        bool SaveUserRoleMapping(UserAdminBusinessModel userAdminBusinessModel);

        /// <summary>
        /// Get Customer Details
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// return list of customers
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomerDetails/{CustomerId}", ResponseFormat = WebMessageFormat.Json)]
        List<HolidayProcessAdminDetails> GetCustomerDetails(string customerId);

        /// <summary>
        /// Save holiday process data
        /// </summary>
        /// <param name="holidayProcessAdminBusinessModels">The holiday process admin business models.</param>
        /// <returns>
        /// return true or false
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveHolidayProcessData", ResponseFormat = WebMessageFormat.Json)]
        bool SaveHolidayProcessData(HolidayProcessAdminBusinessModels holidayProcessAdminBusinessModels);

        /// <summary>
        /// Saves the customer by user mapping.
        /// </summary>
        /// <param name="userAdminBusinessModel">The user admin business model.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveCustomerByUserMapping", ResponseFormat = WebMessageFormat.Json)]
        bool SaveCustomerByUserMapping(UserAdminBusinessModel userAdminBusinessModel);

        /// <summary>
        /// Save User carehome mappings.
        /// </summary>
        /// <param name="userCarehomeBusinessModel">User Carehome Business Model</param>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveUserCarehomeMapping", ResponseFormat = WebMessageFormat.Json)]
        bool SaveUserCareHomeMapping(UserCareHomeBusinessModel userCareHomeBusinessModel);
    }
}
