﻿//-----------------------------------------------------------------------
// <copyright file="ISapSender.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// ISapSender Interface
    /// </summary>
    [ServiceContract]
    public interface ISapSender
    {
        /// <summary>
        /// Get OrderDetails
        /// </summary>
        /// <param name="filters">string filters</param>
        /// <returns>Return list of SAPOrderHeaderBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetOrderDetails/{filters}", RequestFormat = WebMessageFormat.Json)]
        SAPResponseBusinessModel GetOrderDetails(string filters);

        /// <summary>
        /// Get OrderDetails
        /// </summary>
        /// <param name="filters">string filters</param>
        /// <returns>Return list of SAPOrderHeaderBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetAllCustomers/{filters}")]
        SAPResponseBusinessModel GetAllCustomers(string filters);
    }
}
