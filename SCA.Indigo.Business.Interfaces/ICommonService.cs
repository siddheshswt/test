﻿//-----------------------------------------------------------------------
// <copyright file="ICommonService.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// ICommonService Interface
    /// </summary>
    [ServiceContract]

    public interface ICommonService
    {
        /// <summary>
        /// Return GetListType based on listTypeId and languageId
        /// </summary>
        /// <param name="listTypeId">ListTypeId of ListType Table</param>
        /// <param name="languageId">LanguageId from TransLang table</param>
        /// <returns>return the list ListTypeBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetListType/{listTypeId}/{languageId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetListType(string listTypeId, string languageId);

        /// <summary>
        /// Get List of type
        /// </summary>
        /// <param name="listTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns>list of types</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetListTypeForClinicalContact/{listTypeId}/{languageId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetListTypeForClinicalContact(string listTypeId, string languageId);

        /// <summary>
        /// Return List based on listTypeId, ListId and languageId
        /// </summary>
        /// <param name="listTypeId">ListTypeId of ListType Table</param>
        /// <param name="listId">ListId of List Table</param>
        /// <param name="languageId">LanguageId from TransLang table</param>
        /// <returns>return the list ListTypeBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetListTypeForInteraction/{listTypeId}/{listId}/{languageId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetListTypeForInteraction(string listTypeId, string listId, string languageId);

        /// <summary>
        /// Get CustomerParameter
        /// </summary>
        /// <param name="objCustomerParameterBusinessModel">Instance of CustomerParameterBusinessModel</param>
        /// <returns>return list of CustomerParameterBusinessModel</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetCustomerParameter", ResponseFormat = WebMessageFormat.Json)]
        List<CustomerParameterBusinessModel> GetCustomerParameter(CustomerParameterBusinessModel objCustomerParameterBusinessModel);

        /// <summary>
        /// Get HolidayList
        /// </summary>
        /// <param name="userId">Logged in User Id</param>        
        /// <returns>Return the list of HolidayBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetHolidayList/{userId}/{countryId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<HolidayBusinessModel> GetHolidayList(string userId, string countryId);

        /// <summary>
        /// Get PatientType Matrix
        /// </summary>
        /// <param name="patientTypes">List of Patient Types</param>        
        /// <returns>Return the Patient Type Matrix</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetPatientTypeMatrix", ResponseFormat = WebMessageFormat.Json)]
        List<PatientTypeMatrixBusinessModel> GetPatientTypeMatrix(List<long> patientTypes);

        /// <summary>
        /// Load PostcodeMatrix List 
        /// </summary>
        /// <param name="customerID">Customer Id</param>        
        /// <param name="customerID">Patient Id</param>
        /// <param name="customerID">Carehome Id</param>
        /// <returns>Return the list of PostCodeMatrixBusinessModel</returns> 
        [OperationContract]
        [WebGet(UriTemplate = "GetPostCodeMatrixForPatient/{customerId}/{patientId}/{carehomeId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PostCodeMatrixBusinessModel> GetPostcodeMatrixForPatient(string customerId, string patientId, string careHomeId, string userId);

        /// <summary>
        /// Return the Postcode Matrix
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="round"></param>
        /// <param name="roundId"></param>
        /// <param name="postCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPostcodeMatrix/{customerId}/{round}/{roundId}/{postCode}/{userId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        PostCodeMatrixBusinessModel GetPostcodeMatrix(string customerId, string round, string roundId, string postCode, string userId);

        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of ListTypeBusinessModel</returns>
        [OperationContract]
        [WebGet(UriTemplate = "CustomerList/{UserId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ListTypeBusinessModel> GetAuthorizeCustomers(string userId);

        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of ListTypeBusinessModel</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetArrangedDeliveryDate", ResponseFormat = WebMessageFormat.Json)]
        string GetArrangedDeliveryDate(HolidayProcessAdminBusinessModels objHolidayBusinessModel);

        /// <summary>
        /// Get list of Customers name and id
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of Customers</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCustomersForDropDown/{UserId}", ResponseFormat = WebMessageFormat.Json)]
        List<DropDownDataBusinessModel> GetCustomersForDropDown(string userId);

        /// <summary>
        /// Get list of Users for Interaction report
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of Users</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetUsersOnInteractionForDropDown/{UserId}/{customerIds}", ResponseFormat = WebMessageFormat.Json)]
        List<InteractionReportUsersBusinessModel> GetUsersOnInteractionForDropDown(string userId, string customerIds);

        /// <summary>
        /// Get list of careHome name and id
        /// </summary>
        /// <param name="userId">string userId</param>
        /// <returns>Return list of careHome</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCareHomesForDropDown/{UserId}", ResponseFormat = WebMessageFormat.Json)]
        List<DropDownDataBusinessModel> GetCareHomesForDropDown(string userId);

        /// <summary>
        /// Get the list for County
        /// </summary>
        /// <returns>
        /// List of County
        /// </returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCountyList", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<CountyBusinessModel> GetCountyList();

        /// <summary>
        /// Gets Carehome Users
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCareHomeUsersOnCustomerId/{customerId}/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<DropDownBusinessModel> GetCareHomeUsersOnCustomerId(string customerId, string userId);

        /// <summary>
        /// Checks wheather user is carehome user or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //[OperationContract]
        //[WebGet(UriTemplate = "IsCareHomeUser/{userId}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        [WebInvoke(UriTemplate = "IsCareHomeUser", ResponseFormat = WebMessageFormat.Json)]
        bool IsCareHomeUser(string userId);
    }
}
