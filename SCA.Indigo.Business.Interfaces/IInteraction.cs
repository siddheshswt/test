﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.Interfaces
// Author           : Gaurav Lad
// Created          : 23-07-2015
//
// ***********************************************************************
// <copyright file="IInteraction.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Notes Interface</summary>

namespace SCA.Indigo.Business.Interfaces
{
    using SCA.Indigo.Business.BusinessModels;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    /// <summary>
    /// Interface Interaction
    /// </summary>
    [ServiceContract]
    public interface IInteraction
    {
        #region Interaction
        /// <summary>
        /// Gets the interaction summary details.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="careHomeId">The carehome identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="interactionId">The interaction identifier.</param>
        /// <param name="assignedToId">The assigned to identifier.</param>
        /// <returns>Interaction Business Model</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetInteractionSummaryDetails/{patientId}/{carehomeId}/{customerId}/{interactionId}/{assignedToId}")]
        List<InteractionBusinessModel> GetInteractionSummaryDetails(string patientId, string careHomeId, string customerId, string interactionId, string assignedToId);

        /// <summary>
        /// Saves the interaction details.
        /// </summary>
        /// <param name="interactionBusinessModel">The interaction business model.</param>
        /// <returns>true/false</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveInteractionDetails", ResponseFormat = WebMessageFormat.Json)]
        bool SaveInteractionDetails(InteractionBusinessModel interactionBusinessModel);

        /// <summary>
        /// Gets the name of the logistic provider.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>Logistic Provider Name</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetLogisticProviderName/{customerId}")]
        string GetLogisticProviderName(string customerId);

        /// <summary>
        /// Gets the create interaction by intreaction identifier.
        /// </summary>
        /// <param name="interactionId">The interaction identifier.</param>
        /// <param name="assignedToId">The assigned to identifier.</param>
        /// <returns>List of Interaction Business Model</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCreateInteractionByInteractionId/{interactionId}/{assignedToId}/{userId}")]
        List<InteractionBusinessModel> GetCreateInteractionByInteractionId(string interactionId, string assignedToId, string userId);
        #endregion

        #region Interaction Report
        /// <summary>
        /// Gets Interaction report summary
        /// </summary>
        /// <param name="auditLogData">The audit log data.</param>
        /// <returns>
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetInteractionReportSummary", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<InteractionReportSummaryBusinessModel> GetInteractionReportSummary(InteractionReportDetailsBusinessModel interReportDetailsBusModel);
        #endregion
    }
}
