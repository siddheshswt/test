﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.Interfaces
// Author           : Arvind Nishad
// Created          : 06-04-2015
//
// ***********************************************************************
// <copyright file="ICareHome.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Carehome Interface</summary>
// ***********************************************************************
namespace SCA.Indigo.Business.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using SCA.Indigo.Business.BusinessModels;

    /// <summary>
    /// IMenu Interface
    /// </summary>
    [ServiceContract]
    public interface ICareHome
    {     
        /// <summary>
        /// Save Carehome Details
        /// </summary>
        /// <param name="objCareHomeBusinessModel">CareHomeBusinessModel objCareHomeBusinessModel</param>
        /// <returns>boolean result</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SaveCareHome", ResponseFormat = WebMessageFormat.Json)]
        long SaveCareHome(CareHomeBusinessModel objCareHomeBusinessModel);

        /// <summary>
        /// Get Carehome Details
        /// </summary>
        /// <param name="careHomeId">string careHomeId</param>
        /// <param name="userId">string userId</param>
        /// <returns>return Care Home Business Model</returns>
        [OperationContract]        
        [WebGet(UriTemplate = "GetCareHome/{careHomeId}/{userId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        CareHomeBusinessModel GetCareHome(string careHomeId, string userId);

        /// <summary>
        /// Get Patient Details
        /// </summary>
        /// <param name="careHomeId">string careHomeId</param>
        /// <param name="userId">string userId</param>
        /// <returns>return Care Home Business Model</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetPatientDetails/{careHomeId}/{patientUserId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PatientBusinessModel> GetPatientDetails(string careHomeId, string patientUserId);


        /// <summary>
        /// Get Carehome Report Data
        /// </summary>
        /// <param name="careHomeId">string careHomeId</param>
        /// <param name="userId">string userId</param>
        /// <returns>return Care Home Business Model</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCareHomeReportData/{careHomeId}/{patientUserId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PatientBusinessModel> GetCareHomeReportData(string careHomeId, string patientUserId);

        /// <summary>
        /// Gets Patient's details and AuditLog
        /// </summary>
        /// <param name="auditLogData">The audit log data.</param>
        /// <returns>
        /// Patient's Audit Logs
        /// </returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "GetCareHomeReportMaintenaceData", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PatientBusinessModel GetCareHomeReportMaintenaceData(PatientBusinessModel PatientData);

        /// <summary>
        /// Get list of carehome name and id
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// /// <param name="userId">User identifier</param>
        /// <returns>Return list of products</returns>
        [OperationContract]
        [WebGet(UriTemplate = "GetCareHomeOnCustomerId/{customerId}/{userId}", ResponseFormat = WebMessageFormat.Json)]
        List<DropDownDataBusinessModel> GetCareHomeOnCustomerId(string customerId, string userId);
    }
}
