﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.BusinessCore.Interfaces
{
    public interface IIncrementalUpdateCore
    {
        bool ReadXMLFilesFromFolder(IBuildIndexCore iBuildIndex, IFileCore iFileCore, string fileName, string userId);

        bool FileIsReady(string path, string userId);
    }
}
