﻿//-----------------------------------------------------------------------
// <copyright file="IGoogleSearch.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.BusinessCore.Interfaces
{
    
    using System.Collections.Generic;
    

    /// <summary>
    /// IGoogleSearch Interface
    /// </summary>    
    public interface IGoogleSearchCore
    {
        bool GoogleSearch(string indigoId, string objectType);
    }
}
