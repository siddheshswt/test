﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.BusinessCore.Interfaces
{
    public interface IBuildIndexCore
    {
        bool BuildGoogleSearch(string indigoId, string objectType);
    }
}
