﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.BusinessCore.Interfaces
{
    public interface IFileCore
    {
        void DeleteArchivalFile(string path, long noOfDays);

        void MoveLogFileToArchival(string sourceDirectory, string archivalDirectory, string fileName);
       
    }
}
