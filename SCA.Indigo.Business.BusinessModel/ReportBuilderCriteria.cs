﻿//-----------------------------------------------------------------------
//  <copyright file="ReportBuilderCriteria.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Report Builder
    /// </summary>
    [DataContract]
    public class ReportBuilderCriteria
    {
        /// <summary>
        /// Gets or sets the column names.
        /// </summary>
        /// <value>
        /// The column names.
        /// </value>
        [DataMember]
        public string ColumnNames { get; set; }

        /// <summary>
        /// Gets or sets the patient name identifier.
        /// </summary>
        /// <value>
        /// The patient name identifier.
        /// </value>
        [DataMember]
        public string PatientNameId { get; set; }

        /// <summary>
        /// Gets or sets the customer ids.
        /// </summary>
        /// <value>
        /// The customer ids.
        /// </value>
        [DataMember]
        public string CustomerIds { get; set; }

        /// <summary>
        /// Gets or sets the care home ids.
        /// </summary>
        /// <value>
        /// The care home ids.
        /// </value>
        [DataMember]
        public string CareHomeIds { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery date from.
        /// </summary>
        /// <value>
        /// The patient delivery date from.
        /// </value>
        [DataMember]
        public string PatientDeliveryDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery date to.
        /// </summary>
        /// <value>
        /// The patient delivery date to.
        /// </value>
        [DataMember]
        public string PatientDeliveryDateTo { get; set; }

        /// <summary>
        /// Gets or sets the order date from.
        /// </summary>
        /// <value>
        /// The order date from.
        /// </value>
        [DataMember]
        public string OrderDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the order date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string OrderDateTo { get; set; }

        /// <summary>
        /// Gets or sets the invoice date from.
        /// </summary>
        /// <value>
        /// The invoice date from.
        /// </value>
        [DataMember]
        public string InvoiceDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the invoice date to.
        /// </summary>
        /// <value>
        /// The invoice date to.
        /// </value>
        [DataMember]
        public string InvoiceDateTo { get; set; }

        /// <summary>
        /// Gets or sets the Patient Create Date From.
        /// </summary>
        /// <value>
        /// The Patient Create Date From.
        /// </value>
        [DataMember]
        public string PatientCreateDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Patient Create Date To.
        /// </summary>
        /// <value>
        /// The Patient Create Date To.
        /// </value>
        [DataMember]
        public string PatientCreateDateTo { get; set; }


        /// <summary>
        /// Gets or sets the Next Assessment Date From.
        /// </summary>
        /// <value>
        /// The Next Assessment Date From.
        /// </value>
        [DataMember]
        public string NextAssessmentDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Next Assessment Date To.
        /// </summary>
        /// <value>
        /// The Next Assessment Date To.
        /// </value>
        [DataMember]
        public string NextAssessmentDateTo { get; set; }

        /// <summary>
        /// Gets or sets the invoice number.
        /// </summary>
        /// <value>
        /// The invoice number.
        /// </value>
        [DataMember]
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Gets or sets the invoice number.
        /// </summary>
        /// <value>
        /// The invoice number to.
        /// </value>
        [DataMember]
        public string InvoiceNumberTo { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the type of the patient.
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        [DataMember]
        public string PatientType { get; set; }

        /// <summary>
        /// Gets or sets the patient status.
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        [DataMember]
        public string PatientStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [user data only].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [user data only]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool UserDataOnly { get; set; }

        /// <summary>
        /// Gets or sets the customer columns.
        /// </summary>
        /// <value>
        /// The customer columns.
        /// </value>
        [DataMember]
        public string CustomerColumns { get; set; }

        /// <summary>
        /// Gets or sets the customer parameter columns.
        /// </summary>
        /// <value>
        /// The customer parameter columns.
        /// </value>
        [DataMember]
        public string CustomerParameterColumns { get; set; }

        /// <summary>
        /// Gets or sets the care home columns.
        /// </summary>
        /// <value>
        /// The care home columns.
        /// </value>
        [DataMember]
        public string CareHomeColumns { get; set; }

        /// <summary>
        /// Gets or sets the patient columns.
        /// </summary>
        /// <value>
        /// The patient columns.
        /// </value>
        [DataMember]
        public string PatientColumns { get; set; }

        /// <summary>
        /// Gets or sets the patient type matrix.
        /// </summary>
        /// <value>
        /// The patient type matrix.
        /// </value>
        [DataMember]
        public string PatientTypeMatrix { get; set; }

        /// <summary>
        /// Gets or sets the order columns.
        /// </summary>
        /// <value>
        /// The order columns.
        /// </value>
        [DataMember]
        public string OrderColumns { get; set; }

        /// <summary>
        /// Gets or sets the prescription columns.
        /// </summary>
        /// <value>
        /// The prescription columns.
        /// </value>
        [DataMember]
        public string PrescriptionColumns { get; set; }

        /// <summary>
        /// Gets or sets the clinical contacts columns.
        /// </summary>
        /// <value>
        /// The clinical contacts columns.
        /// </value>
        [DataMember]
        public string ClinicalContactsColumns { get; set; }

        /// <summary>
        /// Gets or sets the user maintenance columns.
        /// </summary>
        /// <value>
        /// The user maintenance columns.
        /// </value>
        [DataMember]
        public string UserMaintenanceColumns { get; set; }

        /// <summary>
        /// Gets or sets the language identifier.
        /// </summary>
        /// <value>
        /// The language identifier.
        /// </value>
        [DataMember]
        public string LanguageId { get; set; }

        /// <summary>
        /// Gets or sets the product ids.
        /// </summary>
        /// <value>
        /// The product ids.
        /// </value>
        [DataMember]
        public string ProductIds { get; set; }

        /// <summary>
        /// Gets or sets the page NBR.
        /// </summary>
        /// <value>
        /// The page NBR.
        /// </value>
        [DataMember]
        public int PageNbr { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        [DataMember]
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets the sort column.
        /// </summary>
        /// <value>
        /// The sort column.
        /// </value>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets the sort dir.
        /// </summary>
        /// <value>
        /// The sort dir.
        /// </value>
        [DataMember]
        public string SortDir { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is download.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is download; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsDownload { get; set; }
    }
}
