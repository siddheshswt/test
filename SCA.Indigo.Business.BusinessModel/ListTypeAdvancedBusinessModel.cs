﻿//-----------------------------------------------------------------------
//  <copyright file="CustomeParameterMedicalAnalysisBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holiday Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class ListTypeAdvancedBusinessModel : ListTypeBusinessModel
    {
        /// <summary>
        /// Gets or sets IsSelected
        /// </summary>
        /// <value>
        /// The is active.
        /// </value>
        [DataMember]
        public bool? IsActive
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is mandatory; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsMandatory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is field type.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is field type; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsFieldType
        {
            get;
            set;
        }
    }
}