﻿//-----------------------------------------------------------------------
//  <copyright file="CareHomeBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// CareHomeBusinessModel : for Care home
    /// </summary>
    [DataContract]
    public class CareHomeBusinessModel : BaseBusinessModel
    {
        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeId
        /// </summary>
        [DataMember]
        public long CareHomeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeName
        /// </summary>
        [DataMember]
        public string CareHomeName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        [DataMember]
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets County
        /// </summary>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CountyCode
        /// </summary>
        [DataMember]
        public string CountyCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        [DataMember]
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HouseName
        /// </summary>
        [DataMember]
        public string HouseName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HouseNumber
        /// </summary>
        [DataMember]
        public string HouseNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PostCode
        /// </summary>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Country
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets RoundId
        /// </summary>
        [DataMember]
        public string RoundId { get; set; }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        [DataMember]
        public string Round { get; set; }

        /// <summary>
        /// Gets or sets Delivery Frequency
        /// </summary>
        [DataMember]
        public string DeliveryFrequency { get; set; }

        /// <summary>
        /// Gets or sets Next Delivery Date
        /// </summary>
        [DataMember]
        public string NextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets SAP Care Home Number
        /// </summary>
        [DataMember]
        public string SapCareHomeNumber { get; set; }

        [DataMember]
        public List<PatientContactInfoBusinessModel> ContactPersonList { get; set; }

        /// <summary>
        /// Gets or sets AddressId
        /// </summary>
        [DataMember]
        public long AddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonalInformationId
        /// </summary>
        [DataMember]
        public long PersonalInformationId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets BillTo
        /// </summary>
        [DataMember]
        public long BillTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeType
        /// </summary>
        [DataMember]
        public long? CareHomeType { get; set; }

        /// <summary>
        /// Gets or sets OrderType
        /// </summary>
        [DataMember]
        public long? OrderType { get; set; }

        /// <summary>
        /// Gets or sets PurchaseOrderNo
        /// </summary>
        [DataMember]
        public string PurchaseOrderNo { get; set; }

        /// <summary>
        /// Gets or sets CareHomeStatus
        /// </summary>
        [DataMember]
        public long? CareHomeStatus { get; set; }

        /// <summary>
        /// Gets or sets CommunicationFormat
        /// </summary>
        [DataMember]
        public long? CommunicationFormat { get; set; }

        /// <summary>
        /// Gets or sets SAP Customer Number
        /// </summary>
        [DataMember]
        public string SAPCustomerNumber { get; set; }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        [DataMember]
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the communication preferences.
        /// </summary>
        /// <value>
        /// The communication preferences.
        /// </value>
        [DataMember]
        public string[] CommunicationPreferences { get; set; }

        /// <summary>
        /// Gets or sets the communication preferences.
        /// </summary>
        /// <value>
        /// The communication preferences.
        /// </value>
        [DataMember]
        public string CarehomeCommunicationPreferences { get; set; }

        /// <summary>
        /// Gets or sets HasAnyActiveOrderForOrderType
        /// </summary>
        [DataMember]
        public bool HasAnyActiveOrderForOrderType { get; set; }
        /// <summary>
        /// Sets PabeNumber
        /// </summary>
        /// <value>
        /// The page number.
        /// </value>
        [DataMember]
        public long PageNumber { get; set; }

        /// <summary>
        /// Sets RecordsPerPage
        /// </summary>
        /// <value>
        /// The records per page.
        /// </value>
        [DataMember]
        public long RecordsPerPage { get; set; }

        /// <summary>
        /// Sets TotalRecords
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets IsExport ( Whether the Report is to be Exported )
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is export; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsExport { get; set; }
        /// <summary>
        /// Gets or Sets the UserId of LoggedIn user [one eho requested the search]
        /// </summary>
        /// <value>
        /// The logged in user identifier.
        /// </value>
        [DataMember]
        public Guid LoggedInUserId { get; set; }
        /// <summary>
        /// Gets or sets the UserId of the User, for whom Audit logs are to be viewed
        /// </summary>
        /// <value>
        /// The searched user identifier.
        /// </value>
        [DataMember]
        public Guid SearchedUserId { get; set; }
    }
}
