﻿//-----------------------------------------------------------------------
//  <copyright file="SearchBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Search Business Model : for search related functionality
    /// </summary>
    [DataContract]
    public class SearchBusinessModel
    {
        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerName
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DateOfBirth
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        [DataMember]
        public string DateOfBirth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DeliveryDate
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [DataMember]
        public string DeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets OrderActivated
        /// </summary>
        /// <value>
        /// The order activated.
        /// </value>
        [DataMember]
        public long OrderActivated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HasActiveOrder
        /// </summary>
        /// <value>
        /// The has active order.
        /// </value>
        [DataMember]
        public bool? HasActiveOrder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientCustomerId
        /// </summary>
        /// <value>
        /// The patient customer identifier.
        /// </value>
        [DataMember]
        public long PatientCustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientCustomerName
        /// </summary>
        /// <value>
        /// The name of the patient customer.
        /// </value>
        [DataMember]
        public string PatientCustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientId
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long PatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientName
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientStatus
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        [DataMember]
        public string PatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientType
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        [DataMember]
        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Postcode
        /// </summary>
        /// <value>
        /// The postcode.
        /// </value>
        [DataMember]
        public string Postcode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SearchText
        /// </summary>
        /// <value>
        /// The search text.
        /// </value>
        [DataMember]
        public string SearchText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TitleId
        /// </summary>
        /// <value>
        /// The title identifier.
        /// </value>
        [DataMember]
        public string TitleId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the dob.
        /// </summary>
        /// <value>
        /// The dob.
        /// </value>
        [DataMember]
        public string DOB
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        [DataMember]
        public string CareHomeName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public long? CareHomeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the house.
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        [DataMember]
        public string HouseName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the house number.
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        [DataMember]
        public string HouseNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ad d1.
        /// </summary>
        /// <value>
        /// The ad d1.
        /// </value>
        [DataMember]
        public string ADD1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ad d2.
        /// </summary>
        /// <value>
        /// The ad d2.
        /// </value>
        [DataMember]
        public string ADD2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        [DataMember]
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataMember]
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the telephone number.
        /// </summary>
        /// <value>
        /// The telephone number.
        /// </value>
        [DataMember]
        public string TelephoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sap customer number.
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        [DataMember]
        public string SAPCustomerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sap patient number.
        /// </summary>
        /// <value>
        /// The sap patient number.
        /// </value>
        [DataMember]
        public string SAPPatientNumber
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>
        /// The type of the object.
        /// </value>
        [DataMember]
        public int ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the status drop down text.
        /// </summary>
        /// <value>
        /// The status drop down text.
        /// </value>
        [DataMember]
        public string StatusDropDownText
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the status drop downvalue.
        /// </summary>
        /// <value>
        /// The status drop downvalue.
        /// </value>
        [DataMember]
        public string StatusDropDownvalue { get; set; }

        /// <summary>
        /// Gets or sets the total records.
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }       
    }
}