﻿
//-----------------------------------------------------------------------
//  <copyright file="InteractionReportDetailsBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interaction Report Details
    /// </summary>
    [DataContract]
    [Serializable]
    public class InteractionReportDetailsBusinessModel
    {
        /// <summary>
        /// Gets or sets IsExport ( Whether the Report is to be Exported )
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is export; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsExport { get; set; }

        /// <summary>
        /// Gets or sets the Total No. of Records.
        /// </summary>
        /// <value>
        /// Total Records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets the Row Number.
        /// </summary>
        /// <value>
        /// Row Number.
        /// </value>
        [DataMember]
        public long RowNumber { get; set; }

        /// <summary>
        /// Gets or sets the customers ids.
        /// </summary>
        /// <value>
        /// The customers list.
        /// </value>
        [DataMember]
        public string CustomersIds { get; set; }

        /// <summary>
        /// Gets or sets the Patient Ids.
        /// </summary>
        /// <value>
        /// The Patient list.
        /// </value>
        [DataMember]
        public string PatientIds { get; set; }

        /// <summary>
        /// Gets or sets the Carehome Ids.
        /// </summary>
        /// <value>
        /// The Care Home List.
        /// </value>
        [DataMember]
        public string CareHomeIds { get; set; }

        /// <summary>
        /// Gets or sets the interaction type identifier.
        /// </summary>
        /// <value>
        /// The interaction type identifier.
        /// </value>
        [DataMember]
        public string InteractionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the interaction sub type identifier.
        /// </summary>
        /// <value>
        /// The interaction sub type identifier.
        /// </value>
        [DataMember]
        public string InteractionSubTypeId { get; set; }

        /// <summary>
        /// Gets or sets the status identifier.
        /// </summary>
        /// <value>
        /// The status identifier.
        /// </value>
        [DataMember]
        public string StatusId { get; set; }
        /// <summary>
        /// Gets or sets Created By.
        /// </summary>
        /// <value>
        /// The order date from.
        /// </value>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the Created date from.
        /// </summary>
        /// <value>
        /// The order date from.
        /// </value>
        [DataMember]
        public string CreatedDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Created date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string CreatedDateTo { get; set; }

        /// <summary>
        /// Gets or sets the Modified date from.
        /// </summary>
        /// <value>
        /// The order date from.
        /// </value>
        [DataMember]
        public string ModifiedDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the Modified date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string ModifiedDateTo { get; set; }
        /// <summary>
        /// Gets or sets the Modified date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string SortColumn { get; set; }
        /// <summary>
        /// Gets or sets the Modified date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string SortBy { get; set; }

        /// <summary>
        /// Gets or sets the UserId of the User for nteraction Report Page
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }
    }
}