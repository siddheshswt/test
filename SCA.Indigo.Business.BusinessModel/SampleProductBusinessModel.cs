﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : sphapale
// Created          : 09-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="SampleProductBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Sample order business model
    /// </summary>
    [DataContract]
    public class SampleProductBusinessModel
    {
        /// <summary>
        /// Gets or sets Description
        /// </summary>
        [DataMember]
        public string BaseMaterial
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description UI
        /// </summary>
        [DataMember]
        public string DescriptionUI
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product ID
        /// </summary>
        [DataMember]
        public long? ProductID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the helix product identifier.
        /// </summary>       
        [DataMember]
        public string HelixProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product ID
        /// </summary>
        [DataMember]
        public decimal? Price
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product ID
        /// </summary>
        [DataMember]
        public long? MaxQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Search Text
        /// </summary>
        [DataMember]
        public string SearchText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Product type
        /// </summary>
        [DataMember]
        public string SampleOrderCustomerProductType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets tena Product Type
        /// </summary>
        [DataMember]
        public string SampleOrderTenaProductType
        {
            get;
            set;
        }
    }
}
