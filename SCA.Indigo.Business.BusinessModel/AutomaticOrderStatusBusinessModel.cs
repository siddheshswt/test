﻿//-----------------------------------------------------------------------
//  <copyright file="AutomaticOrderBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Business Model Header
    /// </summary>
    [DataContract]
    public class AutomaticOrderStatusBusinessModel
    {
        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets SAPCustomerId
        /// </summary>
        /// <value>
        /// The sap customer identifier.
        /// </value>
        [DataMember]
        public string SAPCustomerId { get; set; }


        /// <summary>
        /// Gets or sets PatientId
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long PatientId { get; set; }

        /// <summary>
        /// Gets or sets SAPPatientId
        /// </summary>
        /// <value>
        /// The sap patient identifier.
        /// </value>
        [DataMember]
        public string SAPPatientId { get; set; }

        /// <summary>
        /// Gets or sets PatientTypeId
        /// </summary>
        /// <value>
        /// The patient type identifier.
        /// </value>
        [DataMember]
        public long PatientTypeId { get; set; }

        /// <summary>
        /// Gets or sets PatientType
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        [DataMember]
        public string PatientType { get; set; }


        /// <summary>
        /// Gets or sets CareHomeId
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public long CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets SAPCareHomeId
        /// </summary>
        /// <value>
        /// The sap care home identifier.
        /// </value>
        [DataMember]
        public string SAPCareHomeId { get; set; }

        /// <summary>
        /// Gets or sets PatientName
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets OrderId
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        [DataMember]
        public long OrderId { get; set; }

        /// <summary>
        /// Gets or sets DeliveryDate
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [DataMember]
        public string DeliveryDate { get; set; }


        /// <summary>
        /// Gets or sets Status
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DataMember]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets Message
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [DataMember]
        public string Message { get; set; }
    }
}
