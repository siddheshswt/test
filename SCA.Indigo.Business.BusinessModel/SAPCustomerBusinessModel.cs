﻿//-----------------------------------------------------------------------
// <copyright file="SAPCustomerBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAPCustomerBusinessModel Entity
    /// </summary>
    [DataContract]
    public class SAPCustomerBusinessModel
    {
        /// <summary>
        /// Gets or sets Address Line 1
        /// </summary>
        /// <value>
        /// The address line1.
        /// </value>
        [DataMember]
        public string AddressLine1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address Line 2
        /// </summary>
        /// <value>
        /// The address line2.
        /// </value>
        [DataMember]
        public string AddressLine2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        [DataMember]
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Country
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataMember]
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets County
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets District channel
        /// </summary>
        /// <value>
        /// The distrchannel.
        /// </value>
        [DataMember]
        public string Distrchannel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Division
        /// </summary>
        /// <value>
        /// The division.
        /// </value>
        [DataMember]
        public string Division
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [DataMember]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Fax Number
        /// </summary>
        /// <value>
        /// The fax number.
        /// </value>
        [DataMember]
        public string FaxNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Haulier
        /// </summary>
        /// <value>
        /// The haulier.
        /// </value>
        [DataMember]
        public string Haulier
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets House Name
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        [DataMember]
        public string HouseName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets House Number
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        [DataMember]
        public string HouseNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Mobile Number
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        [DataMember]
        public string MobileNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Modified Date
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        [DataMember]
        public DateTime ModifiedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Postcode
        /// </summary>
        /// <value>
        /// The postcode.
        /// </value>
        [DataMember]
        public string Postcode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PostCode
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Salse Org
        /// </summary>
        /// <value>
        /// The sales org.
        /// </value>
        [DataMember]
        public string SalesOrg
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Customer No
        /// </summary>
        /// <value>
        /// The sap customer no.
        /// </value>
        [DataMember]
        public string SAPCustomerNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Street
        /// </summary>
        /// <value>
        /// The street.
        /// </value>
        [DataMember]
        public string Street
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Telephone Number
        /// </summary>
        /// <value>
        /// The telephone number.
        /// </value>
        [DataMember]
        public string TelephoneNumber
        {
            get;
            set;
        }
    }
}