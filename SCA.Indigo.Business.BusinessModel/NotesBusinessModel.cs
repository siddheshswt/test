﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : Arvind Nishad
// Created          : 29-04-2015
//
// ***********************************************************************
// <copyright file="NotesBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Notes Business Model</summary>
// ***********************************************************************
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Patient Notes Business Model
    /// </summary>
    [DataContract]
    public class NotesBusinessModel
    {
        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public string CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDate
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataMember]
        public string CreatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FormattedNote
        /// </summary>
        /// <value>
        /// The formatted note.
        /// </value>
        [DataMember]
        public string FormattedNote
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NoteText
        /// </summary>
        /// <value>
        /// The note text.
        /// </value>
        [DataMember]
        public string NoteText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NoteId
        /// </summary>
        /// <value>
        /// The note identifier.
        /// </value>
        [DataMember]
        public long NoteId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NoteType
        /// </summary>
        /// <value>
        /// The type of the note.
        /// </value>
        [DataMember]
        public long NoteType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Name
        /// </summary>
        /// <value>
        /// The note type identifier.
        /// </value>
        [DataMember]
        public long NoteTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Alert as true/false
        /// </summary>
        [DataMember]
        public bool IsDisplayAsAlert { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        [DataMember]
        public string ModifiedBy
        {
            get;
            set;
        }
    }
}