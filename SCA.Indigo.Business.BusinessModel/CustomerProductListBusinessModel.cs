﻿using System;
using System.Runtime.Serialization;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Customer Product List Business Model
    /// </summary>
    [DataContract]
    [Serializable]
    public class CustomerProductListBusinessModel
    {

		[DataMember]
		public long IdxCustomerProductId { get; set; }

		[DataMember]
        public long CustomerId { get; set; }

		[DataMember]
		public string CustomerName { get; set; }

		[DataMember]
		public long ProductId { get; set; }
		
		[DataMember]
		public string SAPProductId { get; set; }

		[DataMember]
		public string BaseMaterial { get; set; }

		[DataMember]
		public string ProductDescription{ get; set; }

		[DataMember]
		public bool IsDisplayed { get; set; }

		[DataMember]
		public bool IsApprovalRequired { get; set; }

		[DataMember]
		public bool IsOverride { get; set; }

		[DataMember]
		public bool IsDisplayForSample { get; set; }

		[DataMember]
		public long Quantity { get; set; }

		[DataMember]
		public long Frequency { get; set; }

		[DataMember]
		public bool IsActive { get; set; }

		[DataMember]
		public string UserId { get; set; }

		[DataMember]
		public string ModifiedDate { get; set; }

    }
}
