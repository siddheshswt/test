﻿//-----------------------------------------------------------------------
//  <copyright file="ClinicalContactValueMappingBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// The BusinessModels namespace.
/// </summary>
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class ClinicalContactValueMappingBusinessModel.
    /// </summary>
    [DataContract]
    [Serializable]
    public class ClinicalContactValueMappingBusinessModel
    {
        /// <summary>
        /// Gets or sets the clinical contact value mapping identifier.
        /// </summary>
        /// <value>The clinical contact value mapping identifier.</value>
        [DataMember]
        public long? ClinicalContactValueMappingId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the clinical contact role mapping identifier.
        /// </summary>
        /// <value>The clinical contact role mapping identifier.</value>
        [DataMember]
        public long ClinicalContactRoleMappingId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The customer identifier.</value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the child value identifier.
        /// </summary>
        /// <value>The child value identifier.</value>
        [DataMember]
        public string ChildValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parent value identifier.
        /// </summary>
        /// <value>The parent value identifier.</value>
        [DataMember]
        public string ParentValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the child list identifier.
        /// </summary>
        /// <value>The child list identifier.</value>
        [DataMember]
        public long? ChildListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the hierarchy list identifier.
        /// </summary>
        /// <value>The hierarchy list identifier.</value>
        [DataMember]
        public long? HierarchyListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is removed.
        /// </summary>
        /// <value><c>true</c> if this instance is removed; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        /// <summary>
        /// Parent id
        /// </summary>
        [DataMember]
        public long? ParentListId
        {
            get;
            set;
        }

        /// <summary>
        /// Role Name
        /// </summary>
        [DataMember]
        public string ChildRoleName
        {
            get;
            set;
        }

        /// <summary>
        /// Parent Role Name
        /// </summary>
        [DataMember]
        public string ParentRoleName
        {
            get;
            set;
        }

        /// <summary>
        /// Parent Value Id
        /// </summary>
        [DataMember]
        public long? ParentValueId
        {
            get;
            set;
        }

        /// <summary>
        /// Child Value Id
        /// </summary>
        [DataMember]
        public long? ChildValueId
        {
            get;
            set;
        }
    }
}
