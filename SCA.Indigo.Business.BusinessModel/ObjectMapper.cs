﻿//-----------------------------------------------------------------------
//  <copyright file="ObjectMapper.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using EFModel = SCA.Indigo.Model;

    /// <summary>
    /// Object Mapper class
    /// </summary>
    public static class ObjectMapper
    {
        /// <summary>
        /// Convert to List Type Business model
        /// </summary>
        /// <param name="results">Results parameter</param>
        /// <returns>
        /// List List Type Business Model
        /// </returns>
        public static List<ListTypeBusinessModel> ToListTypeBusinessModel(this List<EFModel.GetListDetails_usp_Result> results)
        {
            var response = new List<ListTypeBusinessModel>();
            foreach (var result in results)
            {
                var lst = new ListTypeBusinessModel()
                {
                    ListId = result.ListId,
                    ListIndex = result.ListIndex,
                    ListTypeId = result.ListTypeId,
                    DisplayText = result.DisplayText,
                    ListCode = result.ListCode
                };
                response.Add(lst);
            }

            return response;
        }

        /// <summary>
        /// To clinical Contact List
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        public static List<ListTypeBusinessModel> ToClinicalContactListTypeBusinessModel(this List<EFModel.GetClinicalContactAnalysisMaster_Result> results)
        {
            var response = new List<ListTypeBusinessModel>();
            foreach (var result in results)
            {
                var lst = new ListTypeBusinessModel()
                {
                    ListId = result.clinicalcontactanalysismasterid,
                    ListIndex = Convert.ToInt32(result.Index, CultureInfo.InvariantCulture),
                    ListTypeId = result.TypeId,
                    DisplayText = result.ContactName
                };
                response.Add(lst);
            }

            return response;
        }

        /// <summary>
        /// To Role Type For Clinical Contact
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns></returns>
        public static List<ListTypeBusinessModel> ToRoleTypeForClinicalContactBusinessModel(this List<EFModel.GetRoleTypeForClinicalValueMapping_Result> results)
        {
            var response = new List<ListTypeBusinessModel>();
            foreach (var result in results)
            {
                var lst = new ListTypeBusinessModel()
                {
                    ListId = result.clinicalcontactanalysismasterid,
                    ListIndex = Convert.ToInt32(result.Index, CultureInfo.InvariantCulture),
                    ListTypeId = result.TypeId,
                    DisplayText = result.ContactName
                };
                response.Add(lst);
            }

            return response;
        }

        /// <summary>
        /// Convert to List Type Business model
        /// </summary>
        /// <param name="results">Results parameter</param>
        /// <returns>
        /// List List Type Business Model
        /// </returns>
        public static List<ListTypeBusinessModel> ToListTypeBusinessModel(this List<EFModel.GetListDetailsOnListId_usp_Result> results)
        {
            var response = new List<ListTypeBusinessModel>();
            if (results == null)
            {
                return null;
            }

            foreach (var result in results)
            {
                var lst = new ListTypeBusinessModel()
                {
                    ListId = result.ListId,
                    ListIndex = result.ListIndex,
                    ListTypeId = result.ListTypeId,
                    DisplayText = result.DisplayText
                };
                response.Add(lst);
            }

            return response;
        }

        /// <summary>
        /// Convert to Prescription business model
        /// </summary>
        /// <param name="modelUsers">Model Users</param>
        /// <returns>
        /// List PrescriptionBusinessModel
        /// </returns>
        public static List<PrescriptionBusinessModel> ToPrescriptionBusinessModel(this List<EFModel.IDXPrescriptionProduct> modelUsers)
        {
            return modelUsers.Select(item => new PrescriptionBusinessModel
            {
                ProductId = item.ProductId,
                PrescriptionId = item.PrescriptionId,
                AssessedPadsPerDay = item.AssessedPadsPerDay,
                ActualPadsPerDay = item.ActualPadsPerDay,
                DQ1 = item.Del1,
                DQ2 = item.Del2,
                DQ3 = item.Del3,
                DQ4 = item.Del4,
                Frequency = item.Frequency,
                Status = item.Status,
                ValidFromDate = item.ValidFromDate.ToString(),
                ValidToDate = item.ValidToDate.ToString(),
                IsRemoved = item.IsRemoved
            }).ToList();
        }

        /// <summary>
        /// To user Business Model
        /// </summary>
        /// <param name="modelUsers">Model Users</param>
        /// <param name="roleId">Role Id</param>
        /// <param name="roleName">Role Name</param>
        /// <param name="passwordModifiedDate">Password Modified Date</param>
        /// <param name="userType">Type of the user.</param>
        /// <returns>
        /// Users Business Model
        /// </returns>
        public static UsersBusinessModel ToUsersBusinessModel(this EFModel.Users modelUsers, long roleId, string roleName, DateTime passwordModifiedDate, string userType)
        {
            var dateExpireValid = passwordModifiedDate.Add(new TimeSpan(90, 0, 0, 0));

            if (modelUsers == null)
            {
                return null;
            }

            var users = new UsersBusinessModel
            {
                UserId = modelUsers.UserId,
                PersonId =Convert.ToInt64( modelUsers.PersonId,CultureInfo.InvariantCulture),
                SecurityLevel = modelUsers.SecurityLevel,
                ValidFromDate = modelUsers.ValidFromDate,
                ValidToDate = modelUsers.ValidToDate,
                UserName = modelUsers.UserName,
                Password = modelUsers.Password,
                IsAccountLocked = modelUsers.IsAccountLocked,
                NoOfAttempts = modelUsers.NoOfAttempts,
                IsNewUser = modelUsers.IsNewUser,
                ModifiedBy = modelUsers.ModifiedBy,
                RoleId = roleId,
                RoleName = roleName,
                IsExpired = dateExpireValid < DateTime.Now || dateExpireValid == DateTime.MinValue,
                UserType = userType,
                IsAccountActive = modelUsers.IsActive,
                IsCareHomeUser = modelUsers.IsCarehomeUser,
                IsInternalUser = modelUsers.IsInternalUser
            };
            return users;
        }
    }
}