﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Carehome Report Business Model
    /// </summary>
    [DataContract]
    [Serializable]
    public class CarehomeMaintenanceReportBusinessModel
    {
        /// <summary>
        /// Gets or sets the Carehome Patient name.
        /// </summary>
        /// <value>
        /// The Carehome Patient name.
        /// </value>
        [DataMember]
        public string PatientName { get; set; }
        [DataMember]
        public string PatientSAPId { get; set; }
        [DataMember]
        public string DateOfBirth { get; set; }
        [DataMember]
        public string PatientStatus { get; set; }
        [DataMember]
        public string LastAssessDate { get; set; }
        [DataMember]
        public string NextAssessDate { get; set; }
        [DataMember]
        public string SAPProductId { get; set; }
        [DataMember]
        public string ProductDescription { get; set; }
        [DataMember]
        public string AssPPD { get; set; }
        [DataMember]
        public string ActPPD { get; set; }
        [DataMember]
        public string Frequency { get; set; }
        [DataMember]
        public string NDD { get; set; }
        [DataMember]
        public string Comment { get; set; }        
        [DataMember]
        public long TotalRecords { get; set; }
        [DataMember]
        public long PatientId { get; set; }
    }
}
