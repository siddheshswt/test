﻿//-----------------------------------------------------------------------
//  <copyright file="PrescriptionAuthorizationBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Prescription Authorization Business Model
    /// </summary>
    [DataContract]
    public class PrescriptionAuthorizationBusinessModel : BaseBusinessModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PrescriptionAuthorizationBusinessModel"/> is approve.
        /// </summary>
        /// <value><c>true</c> if approve; otherwise, <c>false</c>.</value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool Approve
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        [DataMember]
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets customer Name
        /// </summary>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DOB
        /// </summary>
        [DataMember]
        public string DOB
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets idx Prescription Ids
        /// </summary>
        [DataMember]
        public List<long> IdxPrescriptionIds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets idx Prescription Product Id
        /// </summary>
        [DataMember]
        public long IdxPrescriptionProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Id
        /// </summary>
        [DataMember]
        public long PatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Name
        /// </summary>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PIN
        /// </summary>
        [DataMember]
        public long PIN
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Post Code
        /// </summary>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Name
        /// </summary>
        [DataMember]
        public string ProductName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Quantity
        /// </summary>
        [DataMember]
        public long Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Patient Number
        /// </summary>
        [DataMember]
        public string SAPPatientNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsAdminUser
        /// </summary>
        [DataMember]
        public bool IsAdminUser
        {
            get;
            set;    
        }

        /// <summary>
        /// Gets or sets Locality of Address1
        /// </summary>
        [DataMember]

        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Locality of Address2
        /// </summary>
        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Status
        /// </summary>
        [DataMember]
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Locality
        /// </summary>
        [DataMember]
        public string Locality
        {
            get;
            set;
        }
    }
}