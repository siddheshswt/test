﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 04-07-2015
//
// Last Modified By : mamshett
// Last Modified On : 06-15-2015
// ***********************************************************************
// <copyright file="CustomerBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Customer Business Model
    /// </summary>
    [DataContract]
    public class CustomerBusinessModel
    {
        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPCustomerNumber
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        [DataMember]
        public string SAPCustomerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerName
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the house number.
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        [DataMember]
        public string HouseNumber { get; set; }


        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        /// <value>
        /// The address line1.
        /// </value>
        [DataMember]
        public string AddressLine1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        /// <value>
        /// The address line2.
        /// </value>
        [DataMember]
        public string AddressLine2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TownCity
        /// </summary>
        /// <value>
        /// The town city.
        /// </value>
        [DataMember]
        public string TownCity
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets Country
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataMember]
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets County
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public string CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDate
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataMember]
        public string CreatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Phone
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        [DataMember]
        public string Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets EmailAddress
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        [DataMember]
        public string EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ADP
        /// </summary>
        /// <value>
        /// The transport company.
        /// </value>
        [DataMember]
        public string TransportCompany
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DataMember]
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// StartDate
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        [DataMember]
        public string StartDate { get; set; }

        /// <summary>
        /// StartDate
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the prescription approvals.
        /// </summary>
        /// <value>
        /// The prescription approvals.
        /// </value>
        [DataMember]
        public List<CustomerPrescriptionApprovalBusinessModel> PrescriptionApprovals { get; set; }

        /// <summary>
        /// Gets or sets the prescription ids.
        /// </summary>
        /// <value>
        /// The prescription ids.
        /// </value>
        [DataMember]
        public List<string> PrescriptionIds { get; set; }

        /// <summary>
        /// Gets or sets PostCode
        /// </summary>
        /// <value>
        /// The PostCode.
        /// </value>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

    }
}
