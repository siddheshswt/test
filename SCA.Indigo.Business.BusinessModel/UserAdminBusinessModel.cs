﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : sphapale / bsahoo
// Created          : 09-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="UserAdminBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// User Admin Business Model
    /// </summary>
    [DataContract]
    public class UserAdminBusinessModel
    {
        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerName
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public List<string> CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the users business model.
        /// </summary>
        /// <value>
        /// The users business model.
        /// </value>
        [DataMember]
        public List<UsersBusinessModel> UsersBusinessModel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Aleardy available or not
        /// </summary>
        /// <value>
        /// The already available.
        /// </value>
        [DataMember]
        public int AlreadyAvailable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets user Menu BusinessModel
        /// </summary>
        /// <value>
        /// The user menu business model.
        /// </value>
        [DataMember]
        public List<UserMenuDetailsBusinessModel> userMenuBusinessModel { get; set; }

        /// <summary>
        /// Gets or sets Role Id
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        [DataMember]
        public string RoleId { get; set; }
    }
}
