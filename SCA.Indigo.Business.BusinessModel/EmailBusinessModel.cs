﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using SCA.Indigo.Common;
using SCA.Indigo.Business.BusinessModels;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Email Model for Sending EMails
    /// </summary>
    public class EmailBusinessModel
    {
        /// <summary>
        /// Email Business Model
        /// </summary>
        public EmailBusinessModel()
        {
            lstToEmailId = new List<string>();
            lstCcEmailId = new List<string>();
            lstBccEmailId = new List<string>();
            lstAttachment = new List<string>();
        }

        /// <summary>
        /// From Email Id
        /// </summary>
        public string FromEmailId { get; set; }

        /// <summary>
        /// Private List for To EmailId
        /// </summary>
        private List<string> lstToEmailId;

        /// <summary>
        /// To EmailId List
        /// </summary>
        public List<string> ToEmailId { get { return this.lstToEmailId; } }

        /// <summary>
        /// Private List For Cc EmailId
        /// </summary>
        private List<string> lstCcEmailId;

        /// <summary>
        /// Cc EmailId List
        /// </summary>
        public List<string> CcEmailId { get { return this.lstCcEmailId; } }

        /// <summary>
        /// Private List For Bcc EmailId
        /// </summary>
        private List<string> lstBccEmailId;

        /// <summary>
        /// Bcc EmailId List
        /// </summary>
        public List<string> BccEmailId { get { return this.lstBccEmailId; } }

        /// <summary>
        /// Private List For Attachment
        /// </summary>
        private List<string> lstAttachment;

        /// <summary>
        /// Email Attachement : Specify file path
        /// </summary>
		public List<string> Attachment { get { return this.lstAttachment; } }

        /// <summary>
        /// Subject of the Email
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Is Body HTML
        /// </summary>
        public bool IsBodyHtml { get; set; }

        /// <summary>
        /// Email Body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Checks the Validity of the email
        /// </summary>
        /// <returns></returns>
        private bool IsValid()
        {
            if (!string.IsNullOrEmpty(FromEmailId))
            {
                if (!Common.CommonHelper.ValidateEmail(FromEmailId))
                    throw new ArgumentNullException("FromEmailId:" + FromEmailId, "Send mail failed from address invalid.");
            }
            else
                throw new ArgumentNullException("FromEmailId", "Send mail failed from address is null or empty.");

            if (ToEmailId.Count > 0)
                foreach (string str in ToEmailId)
                {
                    if (string.IsNullOrEmpty(str) || !Common.CommonHelper.ValidateEmail(str))
                    {
                        throw new ArgumentNullException("ToEmailId: " + str, "Send mail failed to address is invalid.");
                    }
                }
            else
                throw new ArgumentNullException("ToEmailId", "Send mail failed to address is null or empty.");

            foreach (string str in CcEmailId)
                if (string.IsNullOrEmpty(str) || !Common.CommonHelper.ValidateEmail(str))
                    throw new ArgumentNullException("CcEmailId: " + str, "Send mail failed to address is invalid.");

            foreach (string str in BccEmailId)
                if (string.IsNullOrEmpty(str) || !Common.CommonHelper.ValidateEmail(str))
                    throw new ArgumentNullException("BccEmailId: " + str, "Send mail failed to address is invalid.");

            foreach(string str in Attachment)
                if (!File.Exists(str))
                    throw new ArgumentNullException("Attachment: " + str, "Send mail failed Attachement does not exist.");

            return true;
        }

        /// <summary>
        /// Sends Email Using SMTP server.
        /// </summary>
        /// <returns></returns>
        public bool Send()
        {
            SmtpClient smtpClient = null;
            MailMessage mail = null;
            try
            {
                if (IsValid())
                {
                    smtpClient = new SmtpClient();
                    if (smtpClient != null && smtpClient.DeliveryMethod == System.Net.Mail.SmtpDeliveryMethod.SpecifiedPickupDirectory)
                    {
                        var directoryPath = smtpClient.PickupDirectoryLocation;
                        Common.CommonHelper.CreateDirectoryIfNotExists(directoryPath);
                    }

                    mail = new MailMessage();

                    mail.From = new MailAddress(FromEmailId);

                    foreach (string str in ToEmailId)
                        mail.To.Add(str);

                    foreach (string str in CcEmailId)
                        mail.CC.Add(str);

                    foreach (string str in BccEmailId)
                        mail.Bcc.Add(str);

                    foreach (string str in Attachment)
                        mail.Attachments.Add(new System.Net.Mail.Attachment(str));

                    mail.Subject = Subject;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.IsBodyHtml = IsBodyHtml;
                    mail.Body = Body;

                    smtpClient.Send(mail);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (smtpClient != null)
                    smtpClient.Dispose();

                if (mail != null)
                    mail.Dispose();
            }
            return true;
        }
    }
}