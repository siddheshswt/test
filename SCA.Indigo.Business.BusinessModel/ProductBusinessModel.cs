﻿//-----------------------------------------------------------------------
//  <copyright file="ProductBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Product Business Model
    /// </summary>
    [DataContract]
    public class ProductBusinessModel
    {
        /// <summary>
        /// Gets or sets Approval Flag
        /// </summary>
        /// <value>
        /// The approval flag.
        /// </value>
        [DataMember]
        public bool? ApprovalFlag
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Base Unit
        /// </summary>
        /// <value>
        /// The base unit.
        /// </value>
        [DataMember]
        public long? BaseUnit
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string ProductName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description UI
        /// </summary>
        /// <value>
        /// The description UI.
        /// </value>
        [DataMember]
        public string DescriptionUI
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Display ID
        /// </summary>
        /// <value>
        /// The product display identifier.
        /// </value>
        [DataMember]
        public string ProductDisplayID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product ID
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        [DataMember]
        public long? ProductID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Type Id
        /// </summary>
        /// <value>
        /// The product type identifier.
        /// </value>
        [DataMember]
        public long? ProductTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sales Unit
        /// </summary>
        /// <value>
        /// The sales unit.
        /// </value>
        [DataMember]
        public string SalesUnit
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Search Text
        /// </summary>
        /// <value>
        /// The search text.
        /// </value>
        [DataMember]
        public string SearchText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PackSize
        /// </summary>
        /// <value>
        /// The size of the pack.
        /// </value>
        [DataMember]
        public string PackSize
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sort order for Grouping
        /// </summary>
        [DataMember]
        public long SortOrder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsOverridden
        /// </summary>
        [DataMember]
        public bool IsOverridden
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Quantity
        /// </summary>
        [DataMember]
        public long Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Frequency
        /// </summary>
        [DataMember]
        public long Frequency
        {
            get;
            set;
        }
    }
}