﻿//-----------------------------------------------------------------------
//  <copyright file="PatientTypeMatrixBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Patient Type Matrix Business Model : for Patient Type Matrix related functionality
    /// </summary>
    [DataContract]
    [Serializable]
    public class PatientTypeMatrixBusinessModel
    {
        /// <summary>
        /// Gets or sets PatientTypeId
        /// </summary>
        [DataMember]
        public long? PatientTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientType
        /// </summary>
        [DataMember]
        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Minimum Frequency
        /// </summary>
        [DataMember]
        public long? PrescMinFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Maximum Frequency
        /// </summary>
        [DataMember]
        public long? PrescMaxFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is add from prescription allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is add from prescription allowed; otherwise, <c>false</c>.
        /// </value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool IsAddFromPrescriptionAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is add from product allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is add from product allowed; otherwise, <c>false</c>.
        /// </value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool IsAddFromProductAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AdvanceOrderActivationDays
        /// </summary>
        [DataMember]
        public long? AdvanceOrderActivationDays
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Change Order Days
        /// </summary>
        [DataMember]
        public long? ChangeOrderNddDays
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is one off add from prescription allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is one off add from prescription allowed; otherwise, <c>false</c>.
        /// </value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool IsOneOffAddFromPrescriptionAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is one off add from product allowed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is one off add from product allowed; otherwise, <c>false</c>.
        /// </value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool IsOneOffAddFromProductAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsPrescriptionApprovalRequired
        /// </summary>
        [DataMember]
        public bool? IsPrescriptionApprovalRequired
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AdultBillTo
        /// </summary>
        [DataMember]
        public long? AdultBillTo
        {
            get;
            set;
        }        
            
        /// <summary>
        /// Gets or sets Paid Bill To
        /// </summary>
        [DataMember]
        public long? PaedBillTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IncrementFrequency
        /// </summary>
        [DataMember]
        public long? IncrementFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        [DataMember]
        public Guid? CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDateTime
        /// </summary>
        [DataMember]
        public DateTime? CreatedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        [DataMember]
        public Guid? ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedDateTime
        /// </summary>
        [DataMember]
        public DateTime? ModifiedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsRemove
        /// </summary>
        [DataMember]
        public bool IsRemove
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Type Matrix ID
        /// </summary>
        [DataMember]
        public long PatientTypeMatrixID
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets Patient Type Matrix ID
        /// </summary>
        [DataMember]
        public long? customerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DefaultFrequency
        /// </summary>
        [DataMember]
        public long? DefaultFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListIndex
        /// </summary>
        /// <value>
        /// The index of the list.
        /// </value>
        [DataMember]
        public int ListIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets Patient Type Text
        /// </summary>
        [DataMember]
        public string PatientTypeText
        {
            get;
            set;
        }
    }
}