﻿//-----------------------------------------------------------------------
//  <copyright file="CommunicationPreferencesBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Communication Preferences Business Model
    /// </summary>
    [DataContract]
    public class CommunicationPreferencesBusinessModel : MassDataBusinessModel
    {
        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets Patient Id
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string IndigoPatientId { get; set; }

        /// <summary>
        /// Gets or sets Carehome Id
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string IndigoCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets Communication Preference
        /// </summary>
        [DataMember]
        public string CommunicationPreference { get; set; }

        /// <summary>
        /// Gets or sets Marketing Preference
        /// </summary>
        [DataMember]
        public string MarketingPreference { get; set; }

        /// <summary>
        /// Gets or sets Communication Format List Id
        /// </summary>
        [DataMember]
        public string CommunicationPrefListId { get; set; }

        /// <summary>
        /// Gets or sets Marketting Preference List Id
        /// </summary>
        [DataMember]
        public string MarkettingPrefListId { get; set; }

        /// <summary>
        /// Gets or sets IsPatient
        /// </summary>
        [DataMember]
        public bool IsPatient { get; set; }
    }
}
