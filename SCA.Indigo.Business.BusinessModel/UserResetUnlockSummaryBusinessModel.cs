﻿//-----------------------------------------------------------------------
//  <copyright file="InteractionReportSummaryBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// User Reset-Unlock Summary
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserResetUnlockSummaryBusinessModel
    {
        /// <summary>
        /// Row Number
        /// </summary>
        [DataMember]
        public long RowNumber { get; set; }

        /// <summary>
        /// Total Records Count
        /// </summary>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        [DataMember]
        public System.Guid UserId { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// User First Name + Last Name
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Email Id
        /// </summary>
        [DataMember]
        public string EmailId { get; set; }

        /// <summary>
        /// Pin Code
        /// </summary>
        [DataMember]
        public string PinCode { get; set; }

        /// <summary>
        /// Is account locked
        /// </summary>
        [DataMember]
        public string IsAccountLocked { get; set; }

        /// <summary>
        /// Lock-Unlock
        /// </summary>
        [DataMember]
        public string Locked { get; set; }

        /// <summary>
        /// Reset
        /// </summary>
        [DataMember]
        public string Reset { get; set; }
    }
}
