﻿//-----------------------------------------------------------------------
//  <copyright file="ContactPersonBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contact Person Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class ContactPersonBusinessModel : MassDataBusinessModel
    {
        /// <summary>
        /// Gets or sets ContactPersonId
        /// </summary>
        /// <value>
        /// The contact person identifier.
        /// </value>
        [DataMember]
        public string ContactPersonId { get; set; }

        /// <summary>
        /// Gets or Sets Customer Id
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets PatientID
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string IndigoPatientId { get; set; }

        /// <summary>
        /// Gets or Sets CareHome Id
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string IndigoCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets FirstName
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [DataMember]
        public string ContactPersonFirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [DataMember]
        public string ContactPersonSurname { get; set; }

        /// <summary>
        /// Gets or sets Job Title
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
        [DataMember]
        public string JobTitle { get; set; }              

        /// <summary>
        /// Gets or sets TelephoneNumber
        /// </summary>
        /// <value>
        /// The telephone number.
        /// </value>
        [DataMember]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets MobileNumber
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        [DataMember]
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [DataMember]
        public string EmailId { get; set; }     
    }
}
