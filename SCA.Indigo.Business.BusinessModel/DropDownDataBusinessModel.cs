﻿//-----------------------------------------------------------------------
//  <copyright file="DropDownData.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Runtime.Serialization;

    /// <summary>
    /// DropDown Data Class
    /// </summary>
    [DataContract]
    public class DropDownDataBusinessModel
    {
        /// <summary>
        /// Gets or sets DisplayText
        /// </summary>
        [DataMember]
        public string DisplayText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        [DataMember]
        public long Value
        {
            get;
            set;
        }
    }
}
