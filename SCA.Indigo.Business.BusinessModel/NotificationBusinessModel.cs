﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : Saurabh Mayekar
// Created          : 04-05-2016
//
// ***********************************************************************
// <copyright file="NotificationBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Notification Business Model</summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Notification Business Model
    /// </summary>
    [DataContract]
    public class NotificationBusinessModel
    {
        /// <summary>
        /// Type of Notification 
        /// </summary>
        [DataMember]
        public string NotificationType { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Count Of Interaction Assigned
        /// </summary>
        [DataMember]
        public Int64 NoOfInteraction { get; set; }
    }
}
