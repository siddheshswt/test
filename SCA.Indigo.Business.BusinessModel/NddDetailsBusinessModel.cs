﻿//-----------------------------------------------------------------------
// <copyright file="NddDetailsBusinessModel.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo. All rights reserved.
// </copyright>
// <summary>This is the NddDetailsBusinessModel file.</summary>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// NDD Details Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class NddDetailsBusinessModel
    {
        /// <summary>
        /// Gets or sets Day1
        /// If Day1 is allowed in postcodematrix then set Day1=1 else 0
        /// </summary>
        /// <value>
        /// The day1.
        /// </value>
        [DataMember]
        public int Day1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Day2
        /// If Day1 is allowed in postcodematrix then set Day2=2 else 0
        /// </summary>
        /// <value>
        /// The day2.
        /// </value>
        [DataMember]
        public int Day2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Day3
        /// If Day3 is allowed in postcodematrix then set Day3=3 else 0
        /// </summary>
        /// <value>
        /// The day3.
        /// </value>
        [DataMember]
        public int Day3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Day4
        /// If Day4 is allowed in postcodematrix then set Day4=4 else 0
        /// </summary>
        /// <value>
        /// The day4.
        /// </value>
        [DataMember]
        public int Day4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Day5
        /// If Day5 is allowed in postcodematrix then set Day5=5 else 0
        /// </summary>
        /// <value>
        /// The day5.
        /// </value>
        [DataMember]
        public int Day5
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ChangeOrderNddDays
        /// </summary>
        /// <value>
        /// The change order NDD days.
        /// </value>
        [DataMember]
        public long ChangeOrderNddDays
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoundID
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundID
        {
            get;
            set;
        }
    }
}