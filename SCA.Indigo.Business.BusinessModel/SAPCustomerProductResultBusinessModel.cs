﻿//-----------------------------------------------------------------------
// <copyright file="SAPCustomerProductResultBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Result BusinessModel
    /// </summary>
    [DataContract]
    public class SAPCustomerProductResultBusinessModel
    {
        /// <summary>
        /// Gets or sets
        /// </summary>
        [DataMember]
        public string ExceptionMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        [DataMember]
        public long FrontEndCustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        [DataMember]
        public int ProductInserted
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        [DataMember]
        public int ProductUpdated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ProductFailed
        /// </summary>
        [DataMember]
        public int ProductFailed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        [DataMember]
        public string SAPCustomerNo
        {
            get;
            set;
        }
    }
}