﻿//-----------------------------------------------------------------------
//  <copyright file="BaseBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Base Business Model : Common to each BusinessModels
    /// </summary>
    [Serializable]
    [DataContract]
    public class BaseBusinessModel
    {
        /// <summary>
        /// Gets or sets EmailAddress
        /// </summary>
        [DataMember]
        public string EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FirstName
        /// </summary>
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  Gender
        /// </summary>
        [DataMember]
        public long Gender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastName
        /// </summary>
        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets MobileNo
        /// </summary>
        [DataMember]
        public string MobileNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  modifiedDate
        /// </summary>
        [DataMember]
        public string ModifiedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PhoneNo
        /// </summary>
        [DataMember]
        public string PhoneNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FaxNumber
        /// </summary>
        [DataMember]
        public string FaxNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TitleId
        /// </summary>
        [DataMember]
        public long? TitleId
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        [DataMember]
        public string UserId
        {
            get;
            set;
        }
    }
}