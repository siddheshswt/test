﻿//-----------------------------------------------------------------------
// <copyright file="SAPCustomerPatientBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Customer Patient Business Model Entity
    /// </summary>
    [DataContract]
    public class SAPCustomerPatientBusinessModel
    {
        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address3
        /// </summary>
        [DataMember]
        public string Address3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ADP
        /// </summary>
        [DataMember]
        public string ADP
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Care Home Id
        /// </summary>
        [DataMember]
        public string CareHomeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        [DataMember]
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Country
        /// </summary>
        [DataMember]
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets County
        /// </summary>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Type
        /// </summary>
        [DataMember]
        public string CustomerType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Delivery Instruction
        /// </summary>
        [DataMember]
        public string DelInstruction
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Distribution channel
        /// </summary>
        [DataMember]
        public string Distrchannel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Division
        /// </summary>
        [DataMember]
        public string Division
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Frequency
        /// </summary>
        [DataMember]
        public string Frequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Front End Id
        /// </summary>
        [DataMember]
        public long FrontEndId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets House Number
        /// </summary>
        [DataMember]
        public string HouseNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LDDT
        /// </summary>
        [DataMember]
        public string LDDT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Name1
        /// </summary>
        [DataMember]
        public string Name1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Name2
        /// </summary>
        [DataMember]
        public string Name2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Name3
        /// </summary>
        [DataMember]
        public string Name3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Name4
        /// </summary>
        [DataMember]
        public string Name4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NDDT
        /// </summary>
        [DataMember]
        public string NDDT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Status
        /// </summary>
        [DataMember]
        public string PatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Type
        /// </summary>
        [DataMember]
        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Postcode
        /// </summary>
        [DataMember]
        public string Postcode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        [DataMember]
        public string Round
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round Id
        /// </summary>
        [DataMember]
        public string RoundId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sales Org
        /// </summary>
        [DataMember]
        public string SalesOrg
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Customer No
        /// </summary>
        [DataMember]
        public string SAPCustomerNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Id
        /// </summary>
        [DataMember]
        public string SAPId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sequence
        /// </summary>
        [DataMember]
        public string Sequence
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Street
        /// </summary>
        [DataMember]
        public string Street
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Telephone Number
        /// </summary>
        [DataMember]
        public string TelephoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Title
        /// </summary>
        [DataMember]
        public string Title
        {
            get;
            set;
        }
    }
}