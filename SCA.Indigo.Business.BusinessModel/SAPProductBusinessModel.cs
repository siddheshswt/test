﻿//-----------------------------------------------------------------------
// <copyright file="SAPProductBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Product Business Model
    /// </summary>
    [DataContract]
    public class SAPProductBusinessModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether [approval flag].
        /// </summary>
        /// <value><c>true</c> if [approval flag]; otherwise, <c>false</c>.</value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool ApprovalFlag
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Base material
        /// </summary>
        [DataMember]
        public string BaseMaterial
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        [DataMember]
        public long CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Net value
        /// </summary>
        [DataMember]
        public string NetValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Pack size
        /// </summary>
        [DataMember]
        public string PackSize
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Id
        /// </summary>
        [DataMember]
        public long ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Inserted
        /// </summary>
        [DataMember]
        public int ProductInserted
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Updated
        /// </summary>
        [DataMember]
        public int ProductUpdated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Updated
        /// </summary>
        [DataMember]
        public int ProductFailed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sales unit
        /// </summary>
        [DataMember]
        public string SalesUnit
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Product Id
        /// </summary>
        [DataMember]
        public string SAPProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Product Name
        /// </summary>
        [DataMember]
        public string SAPProductName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets VAT
        /// </summary>
        [DataMember]
        public string VAT
        {
            get;
            set;
        }
    }
}