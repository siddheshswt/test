﻿
//-----------------------------------------------------------------------
//  <copyright file="InteractionReportSummaryBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interaction Report Summary
    /// </summary>
    [DataContract]
    [Serializable]
    public class InteractionReportSummaryBusinessModel
    {
        /// <summary>
        /// Gets or sets the Total No. of Records.
        /// </summary>
        /// <value>
        /// Total Records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets the Row Number.
        /// </summary>
        /// <value>
        /// Row Number.
        /// </value>
        [DataMember]
        public long RowNumber { get; set; }

        /// <summary>
        /// Gets or sets the Customer Name.
        /// </summary>
        /// <value>
        /// Customer Name.
        /// </value>
        [DataMember]
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the Customer Id.
        /// </summary>
        /// <value>
        /// Customer Id.
        /// </value>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Patient Name.
        /// </summary>
        /// <value>
        /// Patient Name.
        /// </value>
        [DataMember]
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the Patient Id.
        /// </summary>
        /// <value>
        /// Patient Id.
        /// </value>
        [DataMember]
        public string PatientId { get; set; }

        /// <summary>
        /// Gets or sets the Care Home Name.
        /// </summary>
        /// <value>
        /// Care Home Name.
        /// </value>
        [DataMember]
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the CareHome Id.
        /// </summary>
        /// <value>
        /// Care Home Id.
        /// </value>
        [DataMember]
        public string CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets the Interaction Type.
        /// </summary>
        /// <value>
        /// InteractionType.
        /// </value>
        [DataMember]
        public string InteractionType { get; set; }

        /// <summary>
        /// Gets or sets the Interaction Sub Type.
        /// </summary>
        /// <value>
        /// Interaction Sub Type.
        /// </value>
        [DataMember]
        public string SubType { get; set; }

        /// <summary>
        /// Gets or sets the Status Id.
        /// </summary>
        /// <value>
        /// Status Id.
        /// </value>
        [DataMember]
        public string StatusId { get; set; }

        /// <summary>
        /// Gets or sets the Created On.
        /// </summary>
        /// <value>
        /// Created On.
        /// </value>
        [DataMember]
        public string CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the Created By.
        /// </summary>
        /// <value>
        /// Created By.
        /// </value>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the Modified On.
        /// </summary>
        /// <value>
        /// Modified On.
        /// </value>
        [DataMember]
        public string ModifiedOn { get; set; }

        /// <summary>
        /// Gets or sets the Modified By.
        /// </summary>
        /// <value>
        /// Modified By.
        /// </value>
        [DataMember]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        /// <value>
        /// Description.
        /// </value>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Alert.
        /// </summary>
        /// <value>
        /// Alert.
        /// </value>
        [DataMember]
        public string Alert { get; set; }

        /// <summary>
        /// Gets or sets the Assigned To.
        /// </summary>
        /// <value>
        /// Assigned To.
        /// </value>
        [DataMember]
        public string AssignedTo { get; set; }
    }
}