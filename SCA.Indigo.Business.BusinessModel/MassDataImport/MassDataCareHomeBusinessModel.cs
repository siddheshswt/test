﻿//-----------------------------------------------------------------------
//  <copyright file="CareHomeBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// CareHomeDetailsForMassUpdateBusinessModel : for Care home
    /// </summary>
    [DataContract]
    [Serializable]
    public class MassDataCareHomeBusinessModel : MassDataBusinessModel
    {
        /// <summary>
        /// Gets or sets the load identifier.
        /// </summary>
        /// <value>
        /// The load identifier.
        /// </value>
        [DataMember]
        public string LoadId { get; set; }
        
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string IndigoCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets SAP Care Home Number
        /// </summary>
        [DataMember]
        public string SAPCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        [DataMember]
        public string CarehomeName { get; set; }

        /// <summary>
        /// Gets or sets the name of the house.
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        [DataMember]        
        public string HouseName { get; set; }

        /// <summary>
        /// Gets or sets the AddressLine1.
        /// </summary>
        /// <value>
        /// The AddressLine1.
        /// </value>
        [DataMember]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the AddressLine2.
        /// </summary>
        /// <value>
        /// The AddressLine2.
        /// </value>
        [DataMember]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the town or city.
        /// </summary>
        /// <value>
        /// The town or city.
        /// </value>
        [DataMember]        
        public string TownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        [DataMember]
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        [DataMember]
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        [DataMember]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>
        /// The mobile.
        /// </value>
        [DataMember]
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets CareHomeStatus
        /// </summary>
        /// <value>
        /// The care home status.
        /// </value>        
        [DataMember]
        public string CarehomeStatus { get; set; }

        /// <summary>
        /// Gets or sets the care home status identifier.
        /// </summary>
        /// <value>
        /// The care home status identifier.
        /// </value>
        [DataMember]
        public string CareHomeStatusId { get; set; }

        /// <summary>
        /// Gets or sets the next delivery date.
        /// </summary>
        /// <value>
        /// The next delivery date.
        /// </value>
        [DataMember]
        public string NextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        [DataMember]
        public string Round { get; set; }

        /// <summary>
        /// Gets or sets the round identifier.
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundId { get; set; }

        /// <summary>
        /// Gets or sets the delivery frequency.
        /// </summary>
        /// <value>
        /// The delivery frequency.
        /// </value>
        [DataMember]
        public string DeliveryFrequency { get; set; }

        /// <summary>
        /// Gets or sets CareHome Type
        /// </summary>
        [DataMember]
        public string CarehomeType { get; set; }

        /// <summary>
        /// Gets or sets the care home type identifier.
        /// </summary>
        /// <value>
        /// The care home type identifier.
        /// </value>
        [DataMember]
        public string CareHomeTypeId { get; set; }

        /// <summary>
        /// Gets or sets OrderType
        /// </summary>
        /// <value>
        /// The type of the order.
        /// </value>
        [DataMember]
        public string OrderType { get; set; }

        /// <summary>
        /// Gets or sets the order type identifier.
        /// </summary>
        /// <value>
        /// The order type identifier.
        /// </value>
        [DataMember]
        public string OrderTypeId { get; set; }

        /// <summary>
        /// Gets or sets PurchaseOrderNo
        /// </summary>
        /// <value>
        /// The purchase order no.
        /// </value>
        [DataMember]
        public string PurchaseOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the send to sap.
        /// </summary>
        /// <value>
        /// The send to sap.
        /// </value>
        [DataMember]
        public string SendToSAP { get; set; }

        /// <summary>
        /// Gets or sets BillTo
        /// </summary>
        [DataMember]
        public string BillTo { get; set; }
    }
}
