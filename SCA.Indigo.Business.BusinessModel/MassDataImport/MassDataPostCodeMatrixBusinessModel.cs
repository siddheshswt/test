﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business
// Author           : Gaurav Lad
// Created          : 24-Jun-2015
//
// Last Modified By :
// Last Modified On : 
// ***********************************************************************
// <copyright file="MassDataPostCodeMatrixBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>

namespace SCA.Indigo.Business.BusinessModels.MassDataImport
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Mass Data Post Code Matrix Business Model
    /// </summary>
    public class MassDataPostCodeMatrixBusinessModel : MassDataBusinessModel
    {      
        /// <summary>
        /// Gets or sets CustomerID
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]        
        public string CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        /// <value>
        /// The round.
        /// </value>
        [DataMember]        
        public string Round
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoundID
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Postcode
        /// </summary>
        /// <value>
        /// The postcode.
        /// </value>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }       

        /// <summary>
        /// Gets or sets the monday.
        /// </summary>
        /// <value>
        /// The monday.
        /// </value>
        [DataMember]
        public string Monday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the tuesday.
        /// </summary>
        /// <value>
        /// The tuesday.
        /// </value>
        [DataMember]
        public string Tuesday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the wednesday.
        /// </summary>
        /// <value>
        /// The wednesday.
        /// </value>
        [DataMember]
        public string Wednesday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the thursday.
        /// </summary>
        /// <value>
        /// The thursday.
        /// </value>
        [DataMember]
        public string Thursday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the friday.
        /// </summary>
        /// <value>
        /// The friday.
        /// </value>
        [DataMember]
        public string Friday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public string IsActive
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PostcodeMatrixID
        /// </summary>
        /// <value>
        /// The postcode matrix identifier.
        /// </value>
        [DataMember]
        public string PostcodeMatrixID
        {
            get;
            set;
        }

    }
}
