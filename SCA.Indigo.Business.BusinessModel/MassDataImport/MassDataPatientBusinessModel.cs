﻿//-----------------------------------------------------------------------
//  <copyright file="PatientBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Patient Business Model
    /// </summary>
    [DataContract]
    [Serializable]
    public class MassDataPatientBusinessModel : MassDataBusinessModel
    {
        /// <summary>
        /// Gets or sets the LoadId.
        /// </summary>
        /// <value>
        /// The is data LoadId.
        /// </value>
        [DataMember]
        public string LoadId { get; set; }

        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public string CustomerID 
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientId
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string IndigoPatientID
        {
            get;
            set;
        }

         /// <summary>
        /// Gets or sets PatientSAPID
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string PatientSAPID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  Gender
        /// </summary>
        [DataMember]
        public string Gender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TitleId
        /// </summary>
        [DataMember]
        public string Title
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets FirstName
        /// </summary>
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastName
        /// </summary>
        [DataMember]
        public string Surname
        {
            get;
            set;
        }             

        /// <summary>
        /// Gets or sets DateOfBirth
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        [DataMember]
        public string DateofBirth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Age
        /// </summary>
        /// <value>
        /// Patient Age
        /// </value>
        [DataMember]
        public string Age
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPCarehomeID
        /// </summary>
        /// <value>
        /// The SAP care home Id.
        /// </value>
        [DataMember]
        public string SAPCarehomeID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IndigoCarehomeID
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string IndigoCarehomeID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HouseName
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        [DataMember]
        public string HouseName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AddressLine1
        /// </summary>
        /// <value>
        /// The AddressLine1.
        /// </value>
        [DataMember]
        public string AddressLine1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AddressLine2
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        [DataMember]
        public string AddressLine2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TownOrCity
        /// </summary>
        /// <value>
        /// The TownOrCity.
        /// </value>
        [DataMember]
        public string TownOrCity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets County
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Country
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataMember]
        public string Country
        {
            get;
            set;
        }          

        /// <summary>
        /// Gets or sets PostCode
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the Delivery care home.
        /// </summary>
        /// <value>
        /// The name of the Delivery care home.
        /// </value>
        [DataMember]
        public string DeliveryHouseName { get; set; }

        /// <summary>
        /// Gets or sets the Delivery address1.
        /// </summary>
        /// <value>
        /// The Delivery address1.
        /// </value>
        [DataMember]
        public string DeliveryAddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the Delivery address2.
        /// </summary>
        /// <value>
        /// The Delivery address2.
        /// </value>
        [DataMember]
        public string DeliveryAddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the Delivery town or city.
        /// </summary>
        /// <value>
        /// The Delivery town or city.
        /// </value>
        [DataMember]
        public string DeliveryTownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the Delivery county.
        /// </summary>
        /// <value>
        /// The Delivery county.
        /// </value>
        [DataMember]
        public string DeliveryCounty { get; set; }

        /// <summary>
        /// Gets or sets the Delivery country.
        /// </summary>
        /// <value>
        /// The Delivery country.
        /// </value>
        [DataMember]
        public string DeliveryCountry { get; set; }              

        /// <summary>
        /// Gets or sets the Delivery post code.
        /// </summary>
        /// <value>
        /// The Delivery post code.
        /// </value>
        [DataMember]
        public string DeliveryPostCode { get; set; }

        /// <summary>
        /// Gets or sets PhoneNumber
        /// </summary>
        [DataMember]
        public string PhoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Mobile
        /// </summary>
        [DataMember]
        public string Mobile
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        [DataMember]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the is data protected.
        /// </summary>
        /// <value>
        /// The is data protected.
        /// </value>
        [DataMember]
        public string DataProtection { get; set; }

        /// <summary>
        /// Gets or sets DeliveryFrequency
        /// </summary>
        /// <value>
        /// The delivery frequency.
        /// </value>
        [DataMember]
        public string DeliveryFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        /// <value>
        /// The round.
        /// </value>
        [DataMember]
        public string Round
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoundID
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundID
        {
            get;
            set;
        }      

        /// <summary>
        /// Gets or sets AlternateDeliveryPoint1
        /// </summary>
        /// <value>
        /// The ad p1.
        /// </value>
        [DataMember]
        public string AlternateDeliveryPoint1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AlternateDeliveryPoint2
        /// </summary>
        /// <value>
        /// The ad p2.
        /// </value>
        [DataMember]
        public string AlternateDeliveryPoint2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientStatus
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        [DataMember]
        public string PatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ReasonCode
        /// </summary>
        /// <value>
        /// The reason code identifier.
        /// </value>
        [DataMember]
        public string ReasonCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientTypeId
        /// </summary>
        /// <value>
        /// The patient type identifier.
        /// </value>
        [DataMember]
        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NHSId
        /// </summary>
        /// <value>
        /// The NHS identifier.
        /// </value>
        [DataMember]
        public string NHSID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocalID
        /// </summary>
        /// <value>
        /// The local identifier.
        /// </value>
        [DataMember]
        public string LocalID
        {
            get;
            set;
        }       

        /// <summary>
        /// Gets or sets AssessmentDate
        /// </summary>
        /// <value>
        /// The assessment date.
        /// </value>
        [DataMember]
        public string AssessmentDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NextAssessmentDate
        /// </summary>
        /// <value>
        /// The next assessment date.
        /// </value>
        [DataMember]
        public string NextAssessmentDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DeliveryDate
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [DataMember]
        public string DeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NextDeliveryDate
        /// </summary>
        /// <value>
        /// The next delivery date.
        /// </value>
        [DataMember]
        public string NextDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the SendtoSAP.
        /// </summary>
        /// <value>
        /// The is data SendtoSAP.
        /// </value>
        [DataMember]
        public string SendtoSAP { get; set; }

        /// <summary>
        /// Gets or sets BillTo
        /// </summary>
        /// <value>
        /// The bill to.
        /// </value>
        [DataMember]
        public string BillTo
        {
            get;
            set;
        }                                                                                        

        /// <summary>
        /// Gets or sets FaxNumber
        /// </summary>
        [DataMember]
        public string FaxNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HouseNumber
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        [DataMember]
        public string HouseNumber
        {
            get;
            set;
        }
    }
}