﻿//-----------------------------------------------------------------------
//  <copyright file="PrescriptionBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Prescription Business Model
    /// </summary>
    [DataContract]
    public class MassDataPrescriptionBusinessModel : MassDataBusinessModel
    {

        /// <summary>
        /// Gets or sets Patient Id
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string IndigoPortalPatientID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets the SAP Patient Id
        /// </summary>
        /// <value>
        /// The sap patient identifier.
        /// </value>
        [DataMember]
        public string SAPPatientID { get; set; }

        /// <summary>
        /// Gets or sets Customer Id
        /// </summary>
        /// <value>
        /// The Customer identifier.
        /// </value>
        [DataMember]
        public string IndigoPortalCustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets the SAP Customer Id
        /// </summary>
        /// <value>
        /// The sap Customer identifier.
        /// </value>
        [DataMember]
        public string SAPCustomerID { get; set; }

        /// <summary>
        /// Gets or Sets the Old SAP Product Id
        /// </summary>
        /// <value>
        /// The old sap product identifier.
        /// </value>
        [DataMember]
        public string SAPProductId { get; set; }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string ProductName
        {
            get;
            set;
        }

		/// <summary>
		/// Gets Or Sets Old Product Base Material for Order Update
		/// </summary>
		[DataMember]
		public string BaseMaterial { get; set; }


        /// <summary>
        /// Gets or sets Frequency
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        [DataMember]
        public string Frequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [DataMember]
        public string DeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Old Product Next delivery date
        /// </summary>
        /// <value>
        /// Old Product Next delivery date
        /// </value>
        [DataMember]
        public string NextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets Valid From Date
        /// </summary>
        /// <value>
        /// The valid from date.
        /// </value>
        [DataMember]
        public string ValidFromDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Valid To Date
        /// </summary>
        /// <value>
        /// The valid to date.
        /// </value>
        [DataMember]
        public string ValidToDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DataMember]
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Assessed Pads Per Day
        /// </summary>
        /// <value>
        /// The assessed pads per day.
        /// </value>
        [DataMember]
        public string AssessedPadsPerDay
        {
            get;
            set;
        }
      
        /// <summary>
        /// Gets or sets the actual PPD.
        /// </summary>
        /// <value>
        /// The actual PPD.
        /// </value>
        [DataMember]
        public string ActualPPD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is Overridden
        /// </summary>
        /// <value>
        /// The is overridden.
        /// </value>
        [DataMember]
        public string IsOverridden
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ1
        /// </summary>
        /// <value>
        /// The DQ1.
        /// </value>
        [DataMember]
		public string DQ1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ2
        /// </summary>
        /// <value>
        /// The dq2.
        /// </value>
        [DataMember]
		public string DQ2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ3
        /// </summary>
        /// <value>
        /// The dq3.
        /// </value>
        [DataMember]
		public string DQ3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ4
        /// </summary>
        /// <value>
        /// The dq4.
        /// </value>
        [DataMember]
		public string DQ4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q1.
        /// </summary>
        /// <value>
        /// The flag dq1.
        /// </value>
        [DataMember]
		public string FlagDQ1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q2.
        /// </summary>
        /// <value>
        /// The flag dq2.
        /// </value>
        [DataMember]
		public string FlagDQ2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q3.
        /// </summary>
        /// <value>
        /// The flag dq3.
        /// </value>
        [DataMember]
		public string FlagDQ3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q4.
        /// </summary>
        /// <value>
        /// The flag dq4.
        /// </value>
        [DataMember]
		public string FlagDQ4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets the New SAP Product Id
        /// </summary>
        /// <value>
        /// The new sap product identifier.
        /// </value>
        [DataMember]
        public string NewSAPProductID { get; set; }

        /// <summary>
        /// Gets or Sets the New Product Description
        /// </summary>
        /// <value>
        /// The new product description.
        /// </value>
        [DataMember]
        public string NewProductName { get; set; }

		/// <summary>
		/// Gets Or Sets Old Product Base Material for Order Update
		/// </summary>
		[DataMember]
		public string NewProductBaseMaterial { get; set; }

		/// <summary>
		/// Gets or Sets the Count Changed
		/// </summary>
		/// <value>
		///   <c>Y</c> if [count changed]; otherwise, <c>N</c>.
		/// </value>
		[DataMember]
		public string CountChanged {get; set;}
    }
}