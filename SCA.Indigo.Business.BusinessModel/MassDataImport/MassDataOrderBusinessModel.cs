﻿//-----------------------------------------------------------------------
//  <copyright file="PrescriptionBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels.MassDataImport
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class MassDataOrderBusinessModel : MassDataBusinessModel
    {

        /// <summary>
        /// Gets or Sets Indigo Portal Customer ID
        /// </summary>
        [DataMember]
        public string IndigoCustomerID { get; set; }
        
        /// <summary>
        /// Gets or Sets SAP CustomerId
        /// </summary>
        [DataMember]
        public string SAPCustomerID { get; set; }

        /// <summary>
        /// Gets or Sets Indigo Patient ID
        /// </summary>
        [DataMember]
        public long IndigoPatientID { get; set; }

        /// <summary>
        /// Gets or Sets SAP Patient ID
        /// </summary>
        [DataMember]
        public string SAPPatientID { get; set; }

        /// <summary>
        /// Gets or Sets Carehome ID
        /// </summary>
        [DataMember]
        public long IndigoCarehomeID { get; set; }

        /// <summary>
        /// Gets or Sets SAP CarehomeID
        /// </summary>
        [DataMember]
        public string SAPCarehomeID { get; set; }

        /// <summary>
        /// Gets or Sets Indigo Order Type
        /// </summary>
        [DataMember]
        public string IndigoOrderType { get; set; }

        /// <summary>
        /// Gets or Sets Front End Order ID
        /// </summary>
        [DataMember]
        public long FrontEndOrderID { get; set; }

        /// <summary>
        /// Gets or Sets Old SAP Product ID
        /// </summary>
        [DataMember]
        public string OldSAPProductID { get; set; }

        /// <summary>
        /// Gets or Sets New SAP Product ID
        /// </summary>
        [DataMember]
        public string NewSAPProductID { get; set; }

        /// <summary>
        /// Gets or Sets Base Material
        /// </summary>
        [DataMember]
        public string BaseMaterial { get; set; }

        /// <summary>
        /// Gets or Sets New Product Base Material
        /// </summary>
        [DataMember]
        public string NewProductBaseMaterial { get; set; }

        /// <summary>
        /// Gets or Sets Quantity
        /// </summary>
        [DataMember]
        public long Quantity { get; set; }

        /// <summary>
        /// Gets or Sets Delivery Date
        /// </summary>
        [DataMember]
        public string DeliveryDate { get; set; }

        /// <summary>
        /// Gets or Sets OrderType Id
        /// </summary>
        [DataMember]
        public long lngOrderType { get; set; }        
    }
}
