﻿//-----------------------------------------------------------------------
//  <copyright file="DropDownBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Runtime.Serialization;

    /// <summary>
    /// DropDown Data Class
    /// </summary>
    public class DropDownBusinessModel
    {
        /// <summary>
        /// Gets or sets DisplayText
        /// </summary>
        [DataMember]
        public string DisplayText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        [DataMember]
        public string Value
        {
            get;
            set;
        }
    }
}
