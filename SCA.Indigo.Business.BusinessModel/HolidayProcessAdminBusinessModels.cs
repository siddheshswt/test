﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : sphapale
// Created          : 25-March-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="HolidayProcessAdminBusinessModels.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holiday Process Admin Business Model
    /// </summary>
    [DataContract]
    public class HolidayProcessAdminBusinessModels
    {
        /// <summary>
        /// Gets or Sets Customer Id
        /// </summary>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// List of Holiday time details
        /// </summary>
        [DataMember]
        public List<HolidayProcessAdminDetails> HolidayProcessAdminDetails { get; set; }

        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        [DataMember]
        public string UserID
        {
            get;
            set;
        }
     
        /// <summary>
        /// Gets or sets Arranged Date
        /// </summary>
        [DataMember]
        public string ArrangedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Derived Date
        /// </summary>
        [DataMember]
        public string DerivedNDD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets OrderCreationTime
        /// </summary>
        [DataMember]
        public string OrderCreationTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets userId
        /// </summary>
        [DataMember]
        public Guid UserId
        {
            get;
            set;
        }    
    }
}
