﻿//-----------------------------------------------------------------------
//  <copyright file="AutomaticOrderBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Automatic Order Criteria
    /// </summary>
    [DataContract]
    public class AutomaticOrderCriteriaBusinessModel
    {

        /// <summary>
        /// Gets or sets Customers
        /// </summary>
        /// <value>
        /// The customers.
        /// </value>
        [DataMember]
        public List<long> Customers { get; set; }

        /// <summary>
        /// Gets or sets IsTestRun
        /// </summary>
        /// <value>
        /// The care home ids.
        /// </value>
        [DataMember]
        public string CareHomeIds { get; set; }

        /// <summary>
        /// Gets or sets IsTestRun
        /// </summary>
        /// <value>
        /// The patient ids.
        /// </value>
        [DataMember]
        public string PatientIds { get; set; }

        /// <summary>
        /// Gets or sets IsTestRun
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is test run; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsTestRun { get; set; }

        /// <summary>
        /// Gets or sets RunDate
        /// </summary>
        /// <value>
        /// The rundate.
        /// </value>
        [DataMember]
        public string Rundate { get; set; }

        /// <summary>
        /// Gets or sets IsOnlyCustomerChecked
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is only customer checked; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsOnlyCustomerChecked { get; set; }

    }
}
