﻿//-----------------------------------------------------------------------
// <copyright file="SAPOrderDetailsBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Order BusinessModel Entity
    /// </summary>
    [DataContract]
    public class SAPOrderDetailsBusinessModel
    {
        /// <summary>
        /// Gets or sets BASE MATERIAL
        /// </summary>
        [DataMember]
        public string BASEMATERIAL
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DELIVERY DATE
        /// </summary>
        [DataMember]
        public string DELIVERYDATE
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPOrderDeliveryDate
        /// </summary>
        [DataMember]
        public DateTime? SAPOrderDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPOrderDeliveryDate
        /// </summary>
        [DataMember]
        public DateTime? DerivedNDD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Frequency
        /// </summary>
        [DataMember]
        public string Frequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ITEM FOC
        /// </summary>
        [DataMember]
        public string ITEMFOC
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NDDT
        /// </summary>
        [DataMember]
        public string NDDT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets QUANTITY
        /// </summary>
        [DataMember]
        public long QUANTITY
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SALES UNIT
        /// </summary>
        [DataMember]
        public string SALESUNIT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ASSESSED PPD
        /// </summary>
        [DataMember]
        public  string ASPPD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ACTUAL PPD
        /// </summary>
        [DataMember]
        public string ACPPD
        {
            get;
            set;
        }
    }
}