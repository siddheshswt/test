﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : Sagar Yerva
// Created          : 02-26-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="NurseCommentsBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Nurse Comments Busines Model
    /// </summary>
    [DataContract]    
    public class NurseCommentsBusinessModel
    {
        /// <summary>
        /// Gets or sets Prescription Extended Id
        /// </summary>
        /// <value>
        /// The prescription extended identifier.
        /// </value>
        [DataMember]
        public long PrescriptionExtendedId { get; set; }

        /// <summary>
        /// Gets or sets Prescription Id
        /// </summary>
        /// <value>
        /// The prescription identifier.
        /// </value>
        [DataMember]
        public long PrescriptionId { get; set; }

        /// <summary>
        /// Gets or sets User Id
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets Role Id
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        [DataMember]
        public long RoleId { get; set; }

        /// <summary>
        /// Gets or sets Name
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets MobileNumber
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        [DataMember]
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Comments
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        [DataMember]
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets Modified Date
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        [DataMember]
        public string ModifiedDate { get; set; }
    }
}