﻿//-----------------------------------------------------------------------
//  <copyright file="UsersBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Users Business Model : for user related functionality
    /// </summary>
    [DataContract]
    [Serializable]
    public class UsersBusinessModel
    {
        /// <summary>
        /// Gets or sets CareHomeId
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public long? CareHomeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsAccountLocked
        /// </summary>
        /// <value>
        /// The is account locked.
        /// </value>
        [DataMember]
        public bool? IsAccountLocked
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsExpired
        /// </summary>
        /// <value>
        /// The is expired.
        /// </value>
        [DataMember]
        public bool? IsExpired
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsNewUser
        /// </summary>
        /// <value>
        /// The is new user.
        /// </value>
        [DataMember]
        public bool? IsNewUser
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        [DataMember]
        public System.Guid ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NoOfAttempts
        /// </summary>
        /// <value>
        /// The no of attempts.
        /// </value>
        [DataMember]
        public long? NoOfAttempts
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Password
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [DataMember]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonId
        /// </summary>
        /// <value>
        /// The person identifier.
        /// </value>
        [DataMember]
        public long PersonId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoleId
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        [DataMember]
        public long RoleId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoleName
        /// </summary>
        /// <value>
        /// The name of the role.
        /// </value>
        [DataMember]
        public string RoleName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SecurityLevel
        /// </summary>
        /// <value>
        /// The security level.
        /// </value>
        [DataMember]
        public long? SecurityLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public System.Guid UserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserName
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserType
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        [DataMember]
        public string UserType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ValidFromDate
        /// </summary>
        /// <value>
        /// The valid from date.
        /// </value>
        [DataMember]
        public DateTime? ValidFromDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ValidToDate
        /// </summary>
        /// <value>
        /// The valid to date.
        /// </value>
        [DataMember]
        public DateTime? ValidToDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [sso access].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [sso access]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]

        public bool SSOAccess { get; set; }

        /// <summary>
        /// Gets or sets TenaUser
        /// </summary>
        /// <value>
        /// The customers to display.
        /// </value>
        [DataMember]
        public string CustomersToDisplay { get; set; }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        [DataMember]
        public string SAPCustomerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsAccountActive
        /// </summary>
        /// <value>
        /// The IsAccountActive.
        /// </value>
        [DataMember]
        public bool IsAccountActive
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsCareHomeUser
        /// </summary>
        /// <value>
        /// The IsAccountActive.
        /// </value>
        [DataMember]
        public bool IsCareHomeUser
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets IsInternalUser
        /// </summary>
        /// <value>
        /// The IsInternalUser.
        /// </value>
        [DataMember]
        public bool IsInternalUser
        {
            get;
            set;
        }
    }
}