﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 06-17-2015
//
// Last Modified By : mamshett
// Last Modified On : 06-17-2015
// ***********************************************************************
// <copyright file="CommonSearchBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Audit Log Search Business Model
    /// </summary>
    [DataContract]
    [Serializable]
    public  class CommonSearchBusinessModel
    {
        /// <summary>
        /// Gets or Sets the keyword used for Searching
        /// </summary>
        /// <value>
        /// The search term.
        /// </value>
        [DataMember]
        public string SearchTerm { get; set; }


        /// <summary>
        /// Gets or Sets Customer's Id
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerId { get; set; }
       

        /// <summary>
        /// Gets or Sets
        /// </summary>
        /// <value>
        /// The name of the table.
        /// </value>
        [DataMember]
        public string TableName { get; set; }

        /// <summary>
        /// Gets or Sets SAP Id of the Searched Entity
        /// </summary>
        /// <value>
        /// The sap identifier.
        /// </value>
        [DataMember]
        public string SAPId { get; set; }

        /// <summary>
        /// Gets or Sets Front-end Id of the Searched Entity
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public long Id { get; set; }


        /// <summary>
        /// Gets or Sets Search Result Name
        /// </summary>
        /// <value>
        /// The display name of the search.
        /// </value>
        [DataMember]
        public string SearchDisplayName { get; set; }

        /// <summary>
        /// Gets or Sets Search Result Name
        /// </summary>
        /// <value>
        /// The name of the search automatic complete.
        /// </value>
        [DataMember]
        public string SearchAutoCompleteName { get; set; }

        /// <summary>
        /// Gets or Sets the user's Id who requested the search
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public Guid UserId { get; set; }

    }
}
