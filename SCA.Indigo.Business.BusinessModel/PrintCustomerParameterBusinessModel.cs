﻿//-----------------------------------------------------------------------
//  <copyright file="PrintCustomerParameterBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Print Customer Parameter Business Model Class
    /// </summary>
    public class PrintCustomerParameterBusinessModel
    {
        /// <summary>
        /// Gets or sets the column one.
        /// </summary>
        /// <value>
        /// The column one.
        /// </value>
        public string ColumnOne { get; set; }

        /// <summary>
        /// Gets or sets the column two.
        /// </summary>
        /// <value>
        /// The column two.
        /// </value>
        public string ColumnTwo { get; set; }

        /// <summary>
        /// Gets or sets the column three.
        /// </summary>
        /// <value>
        /// The column three.
        /// </value>
        public string ColumnThree { get; set; }

        /// <summary>
        /// Gets or sets the column four.
        /// </summary>
        /// <value>
        /// The column four.
        /// </value>
        public string ColumnFour { get; set; }

        /// <summary>
        /// Gets or sets the column five.
        /// </summary>
        /// <value>
        /// The column five.
        /// </value>
        public bool IsBold { get; set; }
    }
}
