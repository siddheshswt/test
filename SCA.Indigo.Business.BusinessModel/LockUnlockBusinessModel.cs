﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 15-01-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="AttachmentBusinessViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// LockUnlockBusinessModel
    /// </summary>
    [DataContract]
    public class LockUnlockBusinessModel
    {
        /// <summary>
        /// LockUnlockUserId
        /// </summary>
        [DataMember]
        public string LockUnlockUserId { get; set; }

        /// <summary>
        /// userId
        /// </summary>
        [DataMember]
        public string userId { get; set; }

        /// <summary>
        /// isAccountLocked
        /// </summary>
        [DataMember]
        public bool isAccountLocked { get; set; }
    }
}
