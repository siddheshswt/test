﻿//-----------------------------------------------------------------------
//  <copyright file="InteractionReportDetailsBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Users Reset-Unlock Details 
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserResetUnlockDetailsBusinessModel
    {
        /// <summary>
        /// Gets or sets the Total No. of Records.
        /// </summary>
        /// <value>
        /// Total Records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets the Row Number.
        /// </summary>
        /// <value>
        /// Row Number.
        /// </value>
        [DataMember]
        public long RowNumber { get; set; }

        /// <summary>
        /// Gets or Sets searchtext
        /// </summary>
        /// <value>
        /// Search Text
        /// </value>
        [DataMember]
        public string searchText { get; set; }

        /// <summary>
        /// Gets or sets the Modified date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets the Modified date to.
        /// </summary>
        /// <value>
        /// The order date to.
        /// </value>
        [DataMember]
        public string SortBy { get; set; }

        /// <summary>
        /// Gets or sets the UserId of the User for nteraction Report Page
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }
    }
}
