﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 03-25-2015
//
// Last Modified By : mamshett
// Last Modified On : 06-15-2015
// ***********************************************************************
// <copyright file="AuditLogBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Audit Log Business Model
    /// </summary>
    [DataContract]
    [Serializable]
    public class AuditLogBusinessModel
    {               

        #region Frontend Id's
        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long PatientId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public long? CareHomeId { get; set; }

        /// <summary>
        /// Gets or Sets the UserId of LoggedIn user [one eho requested the search]
        /// </summary>
        /// <value>
        /// The logged in user identifier.
        /// </value>
        [DataMember]
        public Guid LoggedInUserId { get; set; }

        /// <summary>
        /// Gets or sets the UserId of the User, for whom Audit logs are to be viewed
        /// </summary>
        /// <value>
        /// The searched user identifier.
        /// </value>
        [DataMember]
        public Guid SearchedUserId { get; set; }
        #endregion

        #region Names

        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        [DataMember]
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [DataMember]
        public string UserName { get; set; }
        #endregion

        #region SAP Id's

        /// <summary>
        /// Gets or sets the patient sap identifier.
        /// </summary>
        /// <value>
        /// The patient sap identifier.
        /// </value>
        [DataMember]
        public string PatientSAPId { get; set; }

        /// <summary>
        /// Gets or sets the customer sap identifier.
        /// </summary>
        /// <value>
        /// The customer sap identifier.
        /// </value>
        [DataMember]
        public string CustomerSAPId { get; set; }

        /// <summary>
        /// Gets or sets the care home sap identifier.
        /// </summary>
        /// <value>
        /// The care home sap identifier.
        /// </value>
        [DataMember]
        public string CareHomeSAPId { get; set; }
        #endregion

        #region AuditLogReport
        /// <summary>
        /// Gets or sets the logs.
        /// </summary>
        /// <value>
        /// The logs.
        /// </value>
        [DataMember]
        public List<AuditLogReportBusinessModel> Logs
        {
            get;
            set;
        }
        #endregion

        #region DateParameters
        /// <summary>
        /// Gets or sets the fromdate.
        /// </summary>
        /// <value>
        /// The fromdate.
        /// </value>
        [DataMember]
        public string Fromdate { get; set; }

        /// <summary>
        /// Gets or sets the todate.
        /// </summary>
        /// <value>
        /// The todate.
        /// </value>
        [DataMember]
        public string Todate { get; set; }
        #endregion

        /// <summary>
        /// Gets or sets the type of the audit log.
        /// </summary>
        /// <value>
        /// The type of the audit log.
        /// </value>
        [DataMember]
        public long AuditLogType { get; set; }

        /// <summary>
        /// Sets PabeNumber
        /// </summary>
        /// <value>
        /// The page number.
        /// </value>
        [DataMember]
        public long PageNumber { get; set; }

        /// <summary>
        /// Sets RecordsPerPage
        /// </summary>
        /// <value>
        /// The records per page.
        /// </value>
        [DataMember]
        public long RecordsPerPage { get; set; }

        /// <summary>
        /// Sets TotalRecords
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets IsExport ( Whether the Report is to be Exported )
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is export; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsExport { get; set; }

    }    
}
