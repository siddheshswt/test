﻿//-----------------------------------------------------------------------
//  <copyright file="MasterMassDataChangeBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using SCA.Indigo.Business.BusinessModels.MassDataImport;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Master Mass Data Change Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class MasterMassDataChangeBusinessModel
    {
        /// <summary>
        /// Gets or sets carehome id
        /// </summary>
        [DataMember]
        public string CarehomeId { get; set; }

        /// <summary>
        /// Gets or sets customer id
        /// </summary>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets Order type id
        /// </summary>
        [DataMember]
        public string OrderTypeId { get; set; }

        /// <summary>
        /// Gets or sets user id
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets patient id
        /// </summary>
        [DataMember]
        public string PatientId { get; set; }

        /// <summary>
        /// Gets or sets patient type
        /// </summary>
        [DataMember]
        public string PatientType { get; set; }

        /// <summary>
        /// Gets or sets patient status
        /// </summary>
        [DataMember]
        public string PatientStatus { get; set; }

        /// <summary>
        /// Gets or sets is carehome
        /// </summary>
        [DataMember]
        public bool IsCarehome { get; set; }

        /// <summary>
        /// Gets or sets download type id
        /// </summary>
        [DataMember]
        public long DownloadTypeId { get; set; }

        /// <summary>
        /// Gets or sets product id
        /// </summary>
        [DataMember]
        public long ProductId { get; set; }

        /// <summary>
        /// Gets or sets language id
        /// </summary>
        [DataMember]
        public long LanguageId { get; set; }

        /// <summary>
        /// Gets or sets contact patient carehome id
        /// </summary>
        [DataMember]
        public string ContactPatientCarehomeId { get; set; }

        /// <summary>
        /// Gets or sets contact person type
        /// </summary>
        [DataMember]
        public long ContactPersonType { get; set; }

        /// <summary>
        /// Gets or sets carehome list
        /// </summary>
        [DataMember]
        public List<MassDataCareHomeBusinessModel> CarehomeDetailsList { get; set; }

        /// <summary>
        /// Gets or sets patient list
        /// </summary>
        [DataMember]
        public List<MassDataPatientBusinessModel> PatientDetailsList { get; set; }

        /// <summary>
        /// Gets or sets prescription list
        /// </summary>
        [DataMember]
        public List<MassDataPrescriptionBusinessModel> PrescriptionList { get; set; }

        /// <summary>
        /// Gets or sets postcode list
        /// </summary>
        [DataMember]
        public List<MassDataPostCodeMatrixBusinessModel> PostCodeMatrixList { get; set; }

        /// <summary>
        /// Gets or sets clinical contact list
        /// </summary>
        [DataMember]
        public List<ClinicalContactAnalysisMassBusinessModel> ClinicalContactAnalysisList { get; set; }

        /// <summary>
        /// Gets or sets contact person list
        /// </summary>
        [DataMember]
        public List<ContactPersonBusinessModel> ContactPersonInfoList { get; set; }

        /// <summary>
        /// Gets or Sets return data for Order mass updates
        /// </summary>
        [DataMember]
        public List<MassDataOrderBusinessModel> OrderList { get; set; }
        
        /// <summary>
        /// Gets or Sets Patient Types For OrderUpdate
        /// </summary>
        [DataMember]
        public string[] PatientTypesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets CareHome Order Types ForOrderUpdate
        /// </summary>
        [DataMember]
        public string[] CareHomeOrderTypesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets Order Types For OrderUpdate
        /// </summary>
        [DataMember]
        public string[] OrderTypesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets Patient Status For OrderUpdate
        /// </summary>
        [DataMember]
        public string[] PatientStatusForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets CareHome Status For OrderUpdate
        /// </summary>
        [DataMember]
        public string[] CareHomeStatusForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets Order Status For OrderUpdate
        /// </summary>
        [DataMember]
        public string[] OrderStatusForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets Patients For OrderUpdate
        /// </summary>
        [DataMember]
        public string PatientsForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets CareHomes For OrderUpdate
        /// </summary>
        [DataMember]
        public string CareHomesForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or Sets OrderIds For OrderUpdate
        /// </summary>
        [DataMember]
        public string OrderIdsForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets SAP ProductId For OrderUpdate
        /// </summary>
        [DataMember]
        public string SAPProductIdForOrderUpdate { get; set; }

        /// <summary>
        /// Gets or sets Order Data DownloadFor [i.e. for Carehome/Patient]
        /// </summary>
        [DataMember]
        public int OrderDataDownloadFor { get; set; }

        /// <summary>
        /// Gets or sets communication list
        /// </summary>
        [DataMember]
        public List<CommunicationPreferencesBusinessModel> CommunicationInfoList { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier for post code matrix.
        /// </summary>
        /// <value>
        /// The customer identifier for post code matrix.
        /// </value>
        [DataMember]
        public string[] CustomerIdForPostCodeMatrix { get; set; }

        /// <summary>
        /// Gets or sets the care home status for care home.
        /// </summary>
        /// <value>
        /// The care home status for care home.
        /// </value>
        [DataMember]
        public string[] CareHomeStatusForCareHome { get; set; }
    }
}
