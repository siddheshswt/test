﻿//-----------------------------------------------------------------------
//  <copyright file="MassChangesBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Mass Changes Business Model Class
    /// </summary>
    [Serializable]
    [DataContract]
    public class MassDataBusinessModel
    {
        /// <summary>
        /// Gets or sets the remarks.
        /// </summary>
        /// <value>
        /// The remarks.
        /// </value>
        [DataMember]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the transaction status.
        /// </summary>
        /// <value>
        /// The transaction status.
        /// </value>
        [DataMember]
        public string TransactionStatus { get; set; }

        /// <summary>
        /// Gets or sets the error messages.
        /// </summary>
        /// <value>
        /// The error messages.
        /// </value>
        [DataMember]
        public String ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the selected template.
        /// </summary>
        /// <value>
        /// The selected template.
        /// </value>
        [DataMember]
        public int SelectedTemplate { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        [DataMember]
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the UI Log IPAddress
        /// </summary>
        /// <value>
        /// The UI Log IPAddress
        /// </value>
        [DataMember]
        public string UILogIPAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is import.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is import; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsImport { get; set; }
    }
}
