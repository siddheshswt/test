﻿//-----------------------------------------------------------------------
//  <copyright file="ClinicalContactMappingBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// The BusinessModels namespace.
/// </summary>
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class ClinicalContactMappingBusinessModel.
    /// </summary>
    [DataContract]
    [Serializable]
    public class ClinicalContactMappingBusinessModel
    {

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The customer identifier.</value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the clinical contact value mapping list.
        /// </summary>
        /// <value>The clinical contact value mapping list.</value>
        [DataMember]
        public List<ClinicalContactValueMappingBusinessModel> ClinicalContactValueMappingList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ClinicalContactRoleMappingId
        /// </summary>
        /// <value>The ClinicalContactRoleMappingId.</value>
        [DataMember]
        public long? ClinicalContactRoleMappingId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the HierarchyListId
        /// </summary>
        /// <value>The HierarchyListId</value>
        [DataMember]
        public long? HierarchyListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ChildListId
        /// </summary>
        /// <value>The ChildListId.</value>
        [DataMember]
        public long? ChildListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ParentListId.
        /// </summary>
        /// <value>The ParentListId.</value>
        [DataMember]
        public long? ParentListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Hierarchy.
        /// </summary>
        /// <value>The Hierarchy.</value>
        [DataMember]
        public string Hierarchy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Parent.
        /// </summary>
        /// <value>The Parent.</value>
        [DataMember]
        public string Parent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Child.
        /// </summary>
        /// <value>The Child.</value>
        [DataMember]
        public string Child
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or Sets User Id
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or Sets parentExistFlag
        /// </summary>
        [DataMember]
        public bool IsParentExist { get; set; }

        /// <summary>
        /// Gets or Sets New Role
        /// </summary>
        [DataMember]
        public bool IsNewRow { get; set; }
    }
}
