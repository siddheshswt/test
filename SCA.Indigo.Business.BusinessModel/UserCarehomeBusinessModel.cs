﻿//-----------------------------------------------------------------------
//  <copyright file="UserCarehomeBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// User Carehome Business Model
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserCareHomeBusinessModel
    {
        /// <summary>
        /// Gets or Sets Customer Id
        /// </summary>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or Sets Carehome Id
        /// </summary>
        [DataMember]
        public string CareHomeId { get; set; }

        /// <summary>
        /// Gets or Sets User Ids
        /// </summary>
        [DataMember]
        public string UserIds { get; set; }

        /// <summary>
        /// Gets or sets the UserId
        /// </summary>
        [DataMember]
        public string UserId { get; set; }
    }
}
