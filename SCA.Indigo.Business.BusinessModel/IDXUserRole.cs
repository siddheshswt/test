﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.Bom
{
    public  class IDXUserRole
    {
        public long IDXUserRoleID { get; set; }
        public System.Guid UserID { get; set; }
        public long RoleID { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

        public virtual Role Role { get; set; }
        public virtual Users User { get; set; }
    }
}
