﻿//----------------------------------------------------------------------------------------------
// <copyright file="InteractionBusinessModel.cs" company="SCA Indigo">
// Copyright (c) SCA Indigo.  All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Business Model Header
    /// </summary>
    [DataContract]
    [Serializable]
    public class InteractionBusinessModel
    {
        /// <summary>
        /// Gets or sets the interaction type identifier.
        /// </summary>
        /// <value>
        /// The interaction type identifier.
        /// </value>
        [DataMember]
        public string InteractionTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the interaction identifier.
        /// </summary>
        /// <value>
        /// The interaction identifier.
        /// </value>
        [DataMember]
        public long InteractionId { get; set; }

        /// <summary>
        /// Gets or sets PatientType
        /// </summary>
        /// <value>
        /// The interaction sub type identifier.
        /// </value>
        [DataMember]
        public string InteractionSubtypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the interaction sub type category.
        /// </summary>
        /// <value>
        /// The interaction sub type category.
        /// </value>
        [DataMember]
        public string InteractionSubtypeCategory { get; set; }

        /// <summary>
        /// Gets or sets PrescMinFrequency
        /// </summary>
        /// <value>
        /// The status identifier.
        /// </value>
        [DataMember]
        public long StatusId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PrescMinFrequency
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataMember]
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the interaction type category.
        /// </summary>
        /// <value>
        /// The interaction type category.
        /// </value>
        [DataMember]
        public string InteractionTypeCategory { get; set; }

        /// <summary>
        /// Gets or sets the statustext.
        /// </summary>
        /// <value>
        /// The statustext.
        /// </value>
        [DataMember]
        public string Statustext { get; set; }

        /// <summary>
        /// Gets or sets the display as.
        /// </summary>
        /// <value>
        /// The display as.
        /// </value>
        [DataMember]
        public bool? DisplayAs { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string PatientId { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the caller.
        /// </summary>
        /// <value>
        /// The name of the caller.
        /// </value>
        [DataMember]
        public string CallerName { get; set; }

        /// <summary>
        /// Gets or sets the assigned identifier.
        /// </summary>
        /// <value>
        /// The assigned identifier.
        /// </value>
        [DataMember]
        public string AssignedId { get;set;}

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the logistic provider.
        /// </summary>
        /// <value>
        /// The name of the logistic provider.
        /// </value>
        [DataMember]
        public string LogisticProviderName { get; set; }

        /// <summary>
        /// Gets or sets the sap customer identifier.
        /// </summary>
        /// <value>
        /// The sap customer identifier.
        /// </value>
        [DataMember]
        public string SAPCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the sap patient identifier.
        /// </summary>
        /// <value>
        /// The sap patient identifier.
        /// </value>
        [DataMember]
        public string SAPPatientId { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        [DataMember]
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the sap care home number.
        /// </summary>
        /// <value>
        /// The sap care home number.
        /// </value>
        [DataMember]
        public string SAPCareHomeNumber { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        [DataMember]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        [DataMember]
        public long? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the Resolved by.
        /// </summary>
        /// <value>
        /// The Resolved by.
        /// </value>
        [DataMember]
        public string ResolvedBy { get; set; }

        /// <summary>
        /// Gets or sets the Resolved Date.
        /// </summary>
        /// <value>
        /// The Resolved Date.
        /// </value>
        [DataMember]
        public DateTime? ResolvedDate { get; set; }

        /// <summary>
        /// Gets or sets the Closed Date.
        /// </summary>
        /// <value>
        /// The Closed Date.
        /// </value>
        [DataMember]
        public DateTime? ClosedDate { get; set; }

        /// <summary>
        /// Gets or sets the Closed By.
        /// </summary>
        /// <value>
        /// The Closed By.
        /// </value>
        [DataMember]
        public string ClosedBy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [Resolved].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [Resolved]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool? Resolved { get; set; }

        /// <summary>
        /// sets the Resolved Date for Saving.
        /// </summary>
        /// <value>
        /// The Resolved Date.
        /// </value>
        [DataMember]
        public string ResolvedDateToSave { get; set; }


        /// <summary>
        /// Gets Or Sets the IsInteractionViewed.
        /// </summary>
        [DataMember]
        public bool IsInteractionViewed { get; set; }

        /// <summary>
        /// Gets Or Sets the IsBtnEnabled.
        /// </summary>
        [DataMember]
        public bool IsBtnEnabled { get; set; }
    }
}
