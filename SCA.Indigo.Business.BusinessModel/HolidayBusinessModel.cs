﻿//-----------------------------------------------------------------------
//  <copyright file="HolidayBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holiday Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class HolidayBusinessModel
    {
        /// <summary>
        /// Gets or sets HolidayDate
        /// </summary>
        /// <value>
        /// The holiday date.
        /// </value>
        [DataMember]
        public DateTime HolidayDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CountryId
        /// </summary>
        /// <value>
        /// The country identifier.
        /// </value>
        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the holiday date string format.
        /// </summary>
        /// <value>
        /// The holiday date string format.
        /// </value>
        [DataMember]
        public string HolidayDateStringFormat
        {
            get;
            set;
        }

        
    }
}