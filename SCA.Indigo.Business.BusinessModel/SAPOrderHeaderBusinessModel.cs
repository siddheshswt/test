﻿//-----------------------------------------------------------------------
// <copyright file="SAPOrderHeaderBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAPOrderHeaderBusinessModel Entity
    /// </summary>
    [DataContract]
    public class SAPOrderHeaderBusinessModel
    {
        /// <summary>
        /// Gets or sets ASA INDICATOR
        /// </summary>
        [DataMember]
        public string ASAINDICATOR
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets BILL TO
        /// </summary>
        [DataMember]
        public string BILLTO
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CAREHOME ID
        /// </summary>
        [DataMember]
        public int CAREHOMEID
        {
            get;
            set;
        }
       
        /// <summary>
        /// Gets or sets CUSTOMERID
        /// </summary>
        [DataMember]
        public long CUSTOMERID
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets PATIENTID
        /// </summary>
        [DataMember]
        public long PATIENTID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CUSTOMER NO
        /// </summary>
        [DataMember]
        public string CUSTOMERNO
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DISTRIBUTION CHANNEL
        /// </summary>
        [DataMember]
        public string DISTRCHANNEL
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DIVISION
        /// </summary>
        [DataMember]
        public string DIVISION
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets EMAIL
        /// </summary>
        [DataMember]
        public string EMAIL
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FRONTEND ID
        /// </summary>
        [DataMember]
        public long FRONTENDID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NO SERVICE CHARGE
        /// </summary>
        [DataMember]
        public string NOSERVCHARGE
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ORDER FOC
        /// </summary>
        [DataMember]
        public string ORDERFOC
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ORDER ITEM
        /// </summary>
        [DataMember]
        public List<SAPOrderDetailsBusinessModel> ORDERITEM
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ORDER TYPE
        /// </summary>
        [DataMember]
        public string ORDERTYPE
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SALES ORG
        /// </summary>
        [DataMember]
        public string SALESORG
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP ID
        /// </summary>
        [DataMember]
        public string SAPID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ROUND
        /// </summary>
        [DataMember]
        public string ROUND
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ROUNDID
        /// </summary>
        [DataMember]
        public string ROUNDID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ROUNDID
        /// </summary>
        [DataMember]
        public string POSTCODE
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PONUMBER
        /// </summary>
        [DataMember]
        public string PONUMBER
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CAREHOME FRONT END ID
        /// </summary>
        [DataMember]
        public long CHINDIGOID
        {
            get;
            set;
        }

    }
}