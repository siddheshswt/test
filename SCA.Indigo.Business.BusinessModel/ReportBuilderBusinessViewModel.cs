﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 05-18-2015
//
// Last Modified By : mamshett
// Last Modified On : 06-15-2015
// ***********************************************************************
// <copyright file="ReportBuilderBusinessViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Report Format Business Model
    /// </summary>
    [DataContract]
    public class ReportFormatBusinessModel
    {

        /// <summary>
        /// Gets or sets the report builder format identifier.
        /// </summary>
        /// <value>
        /// The report builder format identifier.
        /// </value>
        [DataMember]
        public long ReportBuilderFormatId { get; set; }

        /// <summary>
        /// Gets or sets the customer column.
        /// </summary>
        /// <value>
        /// The customer column.
        /// </value>
        [DataMember]
        public string CustomerColumn { get; set; }

        /// <summary>
        /// Gets or sets the customer parameter column.
        /// </summary>
        /// <value>
        /// The customer parameter column.
        /// </value>
        [DataMember]
        public string CustomerParameterColumn { get; set; }

        /// <summary>
        /// Gets or sets the patient matrix column.
        /// </summary>
        /// <value>
        /// The patient matrix column.
        /// </value>
        [DataMember]
        public string PatientMatrixColumn { get; set; }

        /// <summary>
        /// Gets or sets the order column.
        /// </summary>
        /// <value>
        /// The order column.
        /// </value>
        [DataMember]
        public string OrderColumn { get; set; }

        /// <summary>
        /// Gets or sets the patient column.
        /// </summary>
        /// <value>
        /// The patient column.
        /// </value>
        [DataMember]
        public string PatientColumn { get; set; }

        /// <summary>
        /// Gets or sets the perscription column.
        /// </summary>
        /// <value>
        /// The perscription column.
        /// </value>
        [DataMember]
        public string PerscriptionColumn { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact column.
        /// </summary>
        /// <value>
        /// The clinical contact column.
        /// </value>
        [DataMember]
        public string ClinicalContactColumn { get; set; }

        /// <summary>
        /// Gets or sets the care home column.
        /// </summary>
        /// <value>
        /// The care home column.
        /// </value>
        [DataMember]
        public string CareHomeColumn { get; set; }

        /// <summary>
        /// Gets or sets the user maintainance column.
        /// </summary>
        /// <value>
        /// The user maintainance column.
        /// </value>
        [DataMember]
        public string UserMaintainanceColumn { get; set; }

        /// <summary>
        /// Gets or sets the name of the report format.
        /// </summary>
        /// <value>
        /// The name of the report format.
        /// </value>
        [DataMember]
        public string ReportFormatName { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }
    }
}
