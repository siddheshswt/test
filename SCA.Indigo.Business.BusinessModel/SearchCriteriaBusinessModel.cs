﻿//-----------------------------------------------------------------------
//  <copyright file="PrescriptionBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class SearchCriteriaBusinessModel
    {
        [DataMember]
        public string SearchText { get; set; }

        [DataMember]
        public string SearchID { get; set; }

        [DataMember]
        public string CustomerId { get; set; }

        [DataMember]
        public string PatientStatus { get; set; }

        [DataMember]
        public string PatientType { get; set; }

        [DataMember]
        public string OrderID { get; set; }

        [DataMember]
        public string SortColumn { get; set; }

        [DataMember]
        public string SortOrder { get; set; }

        [DataMember]
        public string UserID;

        [DataMember]
        public string PageNo { get; set; }

        [DataMember]
        public string Rows { get; set; }

    }
}
