﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 04-10-2015
//
// Last Modified By : mamshett
// Last Modified On : 07-03-2015
// ***********************************************************************
// <copyright file="CustomerPrescriptionApprovalBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Business Model Header
    /// </summary>
    [DataContract]
    public class CustomerPrescriptionApprovalBusinessModel
    {
        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FirstName
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SurName
        /// </summary>
        /// <value>
        /// The name of the sur.
        /// </value>
        [DataMember]
        public string SurName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or JobTitle
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
        [DataMember]
        public string JobTitle { get; set; }


        /// <summary>
        /// Gets or sets PhoneNumber
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        [DataMember]
        public string PhoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets MobileNumber
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        [DataMember]
        public string MobileNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets EmailAddress
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        [DataMember]
        public string EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AuthorizationPin
        /// </summary>
        /// <value>
        /// The authorization pin.
        /// </value>
        [DataMember]
        public string AuthorizationPin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserName
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IDXUserCustomerId
        /// </summary>
        /// <value>
        /// The index user customer identifier.
        /// </value>
        [DataMember]
        public long IDXAuthorisedUserPrescriptionID
        {
            get;
            set;
        }

    }
}
