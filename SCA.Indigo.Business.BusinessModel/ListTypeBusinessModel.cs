﻿//-----------------------------------------------------------------------
//  <copyright file="ListTypeBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// ListType Business Model
    /// </summary>
    [DataContract]
    public class ListTypeBusinessModel
    {
        /// <summary>
        /// Gets or sets DisplayText
        /// </summary>
        /// <value>
        /// The display text.
        /// </value>
        [DataMember]
        public string DisplayText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListId
        /// </summary>
        /// <value>
        /// The list identifier.
        /// </value>
        [DataMember]
        public long ListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListIndex
        /// </summary>
        /// <value>
        /// The index of the list.
        /// </value>
        [DataMember]
        public int ListIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListTypeId
        /// </summary>
        /// <value>
        /// The list type identifier.
        /// </value>
        [DataMember]
        public long ListTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the list code.
        /// </summary>
        /// <value>
        /// The list code.
        /// </value>
        [DataMember]
        public string ListCode
        {
            get;
            set;
        }
    }
}