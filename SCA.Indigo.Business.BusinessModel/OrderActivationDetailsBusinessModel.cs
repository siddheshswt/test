﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : saumayek
// Created          : 18-04-2016
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="AttachmentBusinessViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Runtime.Serialization;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Order Details Business Model
    /// </summary>
    [DataContract]
    public class OrderActivationDetailsBusinessModel
    {
        /// <summary>
        /// Gets Or Sets Patient Id
        /// </summary>
        [DataMember]
        public string PatientID { get; set; }

        /// <summary>
        /// Gets Or Sets Order Id
        /// </summary>
        [DataMember]
        public string OrderID { get; set; }

        /// <summary>
        /// Gets Or Sets CustomerId
        /// </summary>
        [DataMember]
        public string CustomerID { get; set; }

        /// <summary>
        /// Gets Or Sets ADP
        /// </summary>
        [DataMember]
        public string ADP { get; set; }

        /// <summary>
        /// Gets Or Sets  NoteId
        /// </summary>
        [DataMember]
        public string NoteID { get; set; }

        /// <summary>
        /// Gets Or Sets Note Text
        /// </summary>
        [DataMember]
        public string NoteText { get; set; }

        /// <summary>
        /// Gets Or Sets Is Mandatory flag for ADP
        /// </summary>
        [DataMember]
        public bool IsADPMandatory { get; set; }

        /// <summary>
        /// Gets Or Sets UserId
        /// </summary>
        [DataMember]
        public string UserID { get; set; }

        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        [DataMember]
        public string UserType { get; set; }

        /// <summary>
        /// Gets Or Sets Carehome Id.
        /// </summary>
        [DataMember]
        public string CarehomeId { get; set; }
    }
}
