﻿//-----------------------------------------------------------------------
// <copyright file="SAPResponseBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using System.Xml.XPath;

    /// <summary>
    /// SAP Result BusinessModel
    /// </summary>
    [DataContract]
    public class SAPResponseBusinessModel
    {
        /// <summary>
        /// Gets or sets StringXml
        /// </summary>
        [DataMember]
        public string StringXml
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListCount
        /// </summary>
        [DataMember]
        public int ListCount
        {
            get;
            set;
        }   

        /// <summary>
        /// Gets or sets a value indicating whether [sap transfer status].
        /// </summary>
        /// <value><c>true</c> if [sap transfer status]; otherwise, <c>false</c>.</value>
        /// <returns>bool.</returns>
        [DataMember]
        public bool SAPTransferStatus
        {
            get;
            set;
        }
    }
}