﻿//-----------------------------------------------------------------------
//  <copyright file="ClinicalContactAnalysisMassBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    [Serializable]
    public class ClinicalContactAnalysisMassBusinessModel : MassDataBusinessModel
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The customer identifier.</value>
        [DataMember]
        public string CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The SAP customer identifier.</value>
        [DataMember]
        public string SAPCustomerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Value Type Id.</value>
        [DataMember]
        public string IndigoPortalPatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the SAP Customer identifier.
        /// </summary>
        /// <value>The SAP Patient identifier.</value>
        [DataMember]
        public string SAPPatientNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Patient identifier.</value>
        [DataMember]
        public string PatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Patient identifier.</value>
        [DataMember]
        public string ClinicalContactTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Type.</value>
        [DataMember]
        public string ClinicalContactType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Value.</value>
        [DataMember]
        public string ClinicalContactValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Value Type Id.</value>
        [DataMember]
        public string ClinicalContactValueId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Patient identifier.</value>
        [DataMember]
        public string AnalysisInformationTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Type.</value>
        [DataMember]
        public string AnalysisInformationType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Value.</value>
        [DataMember]
        public string AnalysisInformationValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>The Clincial Contact Value Type Id.</value>
        [DataMember]
        public string AnalysisInformationValueId
        {
            get;
            set;
        }
    }
}
