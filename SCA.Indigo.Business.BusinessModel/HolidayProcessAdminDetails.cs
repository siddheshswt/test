﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : sphapale
// Created          : 25-March-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="HolidayProcessAdminDetails.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Business Model Header
    /// </summary>
    [DataContract]
    public class HolidayProcessAdminDetails
    {
        /// <summary>
        /// Gets or sets Start Time
        /// </summary>
        /// <value>
        /// The start time.
        /// </value>
        [DataMember]
        public string StartTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets End Time
        /// </summary>
        /// <value>
        /// The end time.
        /// </value>
        [DataMember]
        public string EndTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Arranged NDD
        /// </summary>
        /// <value>
        /// The arranged NDD.
        /// </value>
        [DataMember]
        public string ArrangedNDD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets NDD
        /// </summary>
        /// <value>
        /// The NDD.
        /// </value>
        [DataMember]
        public string NDD
        {
            get;
            set;
        }
    }
}
