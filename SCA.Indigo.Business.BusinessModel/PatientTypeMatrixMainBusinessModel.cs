﻿//-----------------------------------------------------------------------
//  <copyright file="PatientTypeMatrixMainBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Patient Type Matrix Business Model : for Patient Type Matrix related functionality
    /// </summary>
    [DataContract]
    [Serializable]
    public class PatientTypeMatrixMainBusinessModel
    {
        /// <summary>
        /// Gets or Sets Customer Id
        /// </summary>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets Patient Type Matrix List
        /// </summary>
        [DataMember]
        public List<PatientTypeMatrixBusinessModel> PatientTypeMatrixList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets User Id
        /// </summary>
        [DataMember]
        public string UserId { get; set; }
    }
}
