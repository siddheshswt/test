﻿//-----------------------------------------------------------------------
// <copyright file="SAPParameterBusinessModel.cs" company="Capgemini">
//     Copyright (c) Capgemini. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SAP Request Parameter
    /// </summary>
    [DataContract]
    public class SAPParameterBusinessModel
    {      
        [DataMember]
        public string StringXml
        {
            get;
            set;
        }
    }
}