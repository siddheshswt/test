﻿//-----------------------------------------------------------------------
//  <copyright file="CountyBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// County Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class CountyBusinessModel
    {
        /// <summary>
        /// Gets or sets CountyText
        /// </summary>
        [DataMember]
        public string CountyText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CountyCode
        /// </summary>
        [DataMember]
        public string CountyCode
        {
            get;
            set;
        }      
    }
}