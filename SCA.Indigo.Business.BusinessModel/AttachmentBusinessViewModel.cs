﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : mamshett
// Created          : 05-11-2015
//
// Last Modified By : mamshett
// Last Modified On : 06-15-2015
// ***********************************************************************
// <copyright file="AttachmentBusinessViewModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    /// <summary>
    /// Attachment Business View Model
    /// </summary>
    [DataContract]
    public class AttachmentBusinessViewModel
    {
        /// <summary>
        /// Gets or sets the type identifier.
        /// </summary>
        /// <value>
        /// The type identifier.
        /// </value>
        [DataMember]
        public string TypeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        [DataMember]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the type of the attachment.
        /// </summary>
        /// <value>
        /// The type of the attachment.
        /// </value>
        [DataMember]
        public string AttachmentType { get; set; }

        /// <summary>
        /// Gets or sets the attachment identifier.
        /// </summary>
        /// <value>
        /// The attachment identifier.
        /// </value>
        [DataMember]
        public string AttachmentId { get; set; }

        /// <summary>
        /// Gets or sets the attachment location.
        /// </summary>
        /// <value>
        /// The attachment location.
        /// </value>
        [DataMember]
        public string AttachmentLocation { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataMember]
        public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the is success.
        /// </summary>
        /// <value>
        /// The is success.
        /// </value>
        [DataMember]
        public string IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        [DataMember]
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the file identifier.
        /// </summary>
        /// <value>
        /// The file identifier.
        /// </value>
        [DataMember]
        public string FileId { get; set; }

        /// <summary>
        /// Gets or sets the absolute path.
        /// </summary>
        /// <value>
        /// The absolute path.
        /// </value>
        [DataMember]
        public string AbsolutePath { get; set; }
    }
}
