﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : Sagar Yerva
// Created          : 02-13-2015
//
// Last Modified By : Sagar Yerva
// Last Modified On : 03-19-2015
// ***********************************************************************
// <copyright file="PostCodeMatrixBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Post Code Matrix Business Model
    /// </summary>
    [Serializable]
    [DataContract]
    public class PostCodeMatrixBusinessModel : BaseBusinessModel
    {
        /// <summary>
        /// Gets or sets PostcodeMatrixID
        /// </summary>
        /// <value>
        /// The postcode matrix identifier.
        /// </value>
        [DataMember]
        public long PostcodeMatrixID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerID
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        /// <value>
        /// The round.
        /// </value>
        [DataMember]
        public string Round
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Postcode
        /// </summary>
        /// <value>
        /// The postcode.
        /// </value>
        [DataMember]
        public string Postcode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoundID
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the monday.
        /// </summary>
        /// <value>
        /// The monday.
        /// </value>
        [DataMember]
        public bool? Monday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the tuesday.
        /// </summary>
        /// <value>
        /// The tuesday.
        /// </value>
        [DataMember]
        public bool? Tuesday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the wednesday.
        /// </summary>
        /// <value>
        /// The wednesday.
        /// </value>
        [DataMember]
        public bool? Wednesday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the thursday.
        /// </summary>
        /// <value>
        /// The thursday.
        /// </value>
        [DataMember]
        public bool? Thursday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the friday.
        /// </summary>
        /// <value>
        /// The friday.
        /// </value>
        [DataMember]
        public bool? Friday
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsActive
        {
            get;
            set;
        }     
    }
}
