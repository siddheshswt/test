﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : sphapale
// Created          : 09-03-2015
//
// Last Modified By : 
// Last Modified On : 
// ***********************************************************************
// <copyright file="SampleOrderPatientBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Sample Order Patient Business Model
    /// </summary>
    [DataContract]
    public class SampleOrderPatientBusinessModel
    {
        /// <summary>
        /// Gets or sets First Name
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets Last Name
        /// </summary>
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Address 1
        /// </summary>
        [DataMember]
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets Address 2
        /// </summary>
        [DataMember]
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets Town or City
        /// </summary>
        [DataMember]
        public string TownOrCity { get; set; }

        /// <summary>
        /// Gets or sets Post Code
        /// </summary>
        [DataMember]
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets Sample House Name
        /// </summary>
        [DataMember]
        public string SampleHouseName { get; set; }

        /// <summary>
        /// Gets or sets Sample House Number
        /// </summary>
        [DataMember]
        public string SampleHouseNumber { get; set; }

        /// <summary>
        /// Gets or sets Called from edit page or not
        /// </summary>
        [DataMember]
        public bool? IsCalledFromEdit { get; set; }

        /// <summary>
        /// Gets or sets User Id
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets List of sample products
        /// </summary>
        [DataMember]
        public List<SampleProductBusinessModel> SampleProductBusinessModel { get; set; }

        /// <summary>
        /// Gets or sets Patient Id
        /// </summary>
        [DataMember]
        public long PatientID { get; set; }

        /// <summary>
        /// Gets or sets County code
        /// </summary>
        [DataMember]
        public string County { get; set; }

        /// <summary>
        /// Gets or sets County Text
        /// </summary>
        [DataMember]
        public string CountyText { get; set; }

        /// <summary>
        /// Gets or sets Return Message
        /// </summary>
        [DataMember]
        public string ReturnMessage { get; set; }

        /// <summary>
        /// Gets or sets Available Items
        /// </summary>
        [DataMember]
        public List<SampleProductBusinessModel> AvailableItems { get; set; }

        /// <summary>
        /// Gets or sets Not Available Items
        /// </summary>
        [DataMember]
        public List<SampleProductBusinessModel> NotAvailableItems { get; set; }

        /// <summary>
        /// Gets or sets Helix Order Id
        /// </summary>
        [DataMember]
        public string HelixOrderID { get; set; }

        /// <summary>
        /// Gets or sets Helix Helix ResponseCode
        /// </summary>
        [DataMember]
        public string HelixResponseCode { get; set; }

        /// <summary>
        /// Gets or sets Helix Helix Message
        /// </summary>
        [DataMember]
        public string HelixMessage { get; set; }
        /// <summary>
        /// Gets or sets true or false
        /// </summary>
        [DataMember]
        public bool YesToSave { get; set; }

        /// <summary>
        /// Gets or sets NDD
        /// </summary>
        [DataMember]
        public string NextDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer ID
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NoteText
        /// </summary>
        /// <value>
        /// The note text.
        /// </value>
        [DataMember]
        public string NoteText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Title text.
        /// </summary>
        /// <value>
        /// The Title text.
        /// </value>
        [DataMember]
        public string Title
        {
            get;
            set;
        }


		/// <summary>
		/// Gets or sets the SampleOrderProductType text.
		/// </summary>
		/// <value>
		/// The SampleOrderProductType text.
		/// </value>
		[DataMember]
		public string SampleOrderProductType
		{
			get;
			set;
		}
    }
}
