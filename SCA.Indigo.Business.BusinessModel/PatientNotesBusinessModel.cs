﻿//-----------------------------------------------------------------------
//  <copyright file="PatientNotesBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Patient Notes Business Model
    /// </summary>
    [DataContract]
    public class PatientNotesBusinessModel
    {
        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public string CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDate
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataMember]
        public string CreatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FormattedNote
        /// </summary>
        /// <value>
        /// The formatted note.
        /// </value>
        [DataMember]
        public string FormattedNote
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NoteText
        /// </summary>
        /// <value>
        /// The note text.
        /// </value>
        [DataMember]
        public string NoteText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long PatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientNoteId
        /// </summary>
        /// <value>
        /// The patient note identifier.
        /// </value>
        [DataMember]
        public long PatientNoteId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Name
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }
    }
}