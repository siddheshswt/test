﻿//-----------------------------------------------------------------------
//  <copyright file="UserMenuDetailsBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// User Menu Details Business model : for user menu related functionality
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserMenuDetailsBusinessModel
    {
        /// <summary>
        /// Gets or sets Action name
        /// </summary>
        /// <value>
        /// The name of the action.
        /// </value>
        [DataMember]
        public string ActionName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Controller name
        /// </summary>
        /// <value>
        /// The name of the controller.
        /// </value>
        [DataMember]
        public string ControllerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Menu Id
        /// </summary>
        /// <value>
        /// The menu identifier.
        /// </value>
        [DataMember]
        public long MenuId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Menu name
        /// </summary>
        /// <value>
        /// The name of the menu.
        /// </value>
        [DataMember]
        public string MenuName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ParentMenuId
        /// </summary>
        /// <value>
        /// The parent menu identifier.
        /// </value>
        [DataMember]
        public long? ParentMenuId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Username
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sub menu identifier.
        /// </summary>
        /// <value>
        /// The sub menu identifier.
        /// </value>
        [DataMember]
        public long? SubMenuId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [view only].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [view only]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool ViewOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [full rights].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [full rights]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool FullRights
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index role menu identifier.
        /// </summary>
        /// <value>
        /// The index role menu identifier.
        /// </value>
        [DataMember]
        public long IDXRoleMenuID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the role identifier.
        /// </summary>
        /// <value>
        /// The role identifier.
        /// </value>
        [DataMember]
        public long RoleID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sap customer identifier.
        /// </summary>
        /// <value>
        /// The sap customer identifier.
        /// </value>
        [DataMember]
        public string SAPCustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserID
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is role assigned.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is role assigned; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsRoleAssigned { get; set; }

        /// <summary>
        /// Gets or sets the index user role identifier.
        /// </summary>
        /// <value>
        /// The index user role identifier.
        /// </value>
        [DataMember]
        public long IDXUserRoleId { get; set; }

        /// <summary>
        /// Disable the role Menu mapping on the Grid property
        /// </summary>
        /// <value>
        ///   <c>true</c> if [disable map role menu]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool DisableMapRoleMenu { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is display.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is display; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsDisplay { get; set; }

        /// <summary>
        /// Gets or sets the has sub menu.
        /// </summary>
        /// <value>
        /// The has sub menu.
        /// </value>
        [DataMember]
        public bool? HasSubMenu
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index of the parent menu.
        /// </summary>
        /// <value>
        /// The index of the parent menu.
        /// </value>
        [DataMember]
        public int? ParentMenuIndex { get; set; }

        /// <summary>
        /// Gets or sets the display index.
        /// </summary>
        /// <value>
        /// The display index.
        /// </value>
        [DataMember]
        public int? DisplayIndex { get; set; }

        /// <summary>
        /// Disable the viewonly rights of search and Authorisation
        /// </summary>
        /// <value>
        /// <c>true</c> if [disable view only menu]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool DisableViewOnlyMenu { get; set; }

        /// <summary>
        /// Gets or sets the Role name.
        /// </summary>
        /// <value>
        /// The RoleName identifier.
        /// </value>
        [DataMember]
        public string RoleName
        {
            get;
            set;
        }
    }
}