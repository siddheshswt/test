﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Business.BusinessModels
// Author           : Mamatha Shetty
// Created          : 28-12-2016
//
// ***********************************************************************
// <copyright file="NDDParametersBusinessModel.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>NDDParametersBusinessModel Business Model - All parameters affecting the NDD calculation</summary>
// ***********************************************************************
namespace SCA.Indigo.Business.BusinessModels
{
	using System.Runtime.Serialization;

	/// <summary>
	/// NDDParametersBusinessModel Business Model - All parameters affecting the NDD calculation
    /// </summary>
    [DataContract]
	public class NDDParametersBusinessModel
    {
		/// <summary>
		/// Gets or sets CustomerId
		/// </summary>
		/// <value>
		/// The CustomerId.
		/// </value>
		[DataMember]
		public string CustomerId
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets LeadTime
		/// </summary>
		/// <value>
		/// The LeadTime.
		/// </value>
		[DataMember]
		public string LeadTime
		{
			get;
			set;
		}

        /// <summary>
		/// Gets or sets Round
        /// </summary>
        /// <value>
		/// The Round.
        /// </value>
        [DataMember]
        public string Round
        {
            get;
            set;
        }

        /// <summary>
		/// Gets or sets RoundID
        /// </summary>
        /// <value>
		/// The RoundID
        /// </value>
        [DataMember]
        public string RoundID
        {
            get;
            set;
        }

        /// <summary>
		/// Gets or sets PostCode
        /// </summary>
        /// <value>
		/// The PostCode.
        /// </value>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

		/// <summary>
		/// Gets or sets CountryId
		/// </summary>
		/// <value>
		/// The CountryId.
		/// </value>
		[DataMember]
		public string CountryId
		{
			get;
			set;
		}

        /// <summary>
		/// Gets or sets the UserId.
        /// </summary>
        [DataMember]
        public string UserId
        {
            get;
            set;
        }
    }
}