﻿//-----------------------------------------------------------------------
//  <copyright file="OnlineOrdersBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
	using System;
	using System.Collections.Generic;
	using System.Runtime.Serialization;

	/// <summary>
	/// Online Orders Business Model
	/// </summary>
	[DataContract]
	public class OnlineOrdersBusinessModel
	{

		/// <summary>
		/// Gets or sets Customer ID
		/// </summary>
		[DataMember]
		public long IndigoPortalOrderID
		{
			get;
			set;
		}
		
		/// <summary>
		/// Gets or sets Customer ID
		/// </summary>
		[DataMember]
		public long CustomerID
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets SAP Customer ID
		/// </summary>
		[DataMember]
		public string SAPCustomerID
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets Patient ID
		/// </summary>
		[DataMember]
		public long PatientID
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets SAP SAPPatient ID
		/// </summary>
		[DataMember]
		public string SAPPatientID
		{
			get;
			set;
		}


		/// <summary>
		/// Gets or sets Order Create Date
		/// </summary>
		[DataMember]
		public string CreateDate
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets Order Type
		/// </summary>
		[DataMember]
		public string Email
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets Allow By Email
		/// </summary>
		[DataMember]
		public string AllowByEmail
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets Birth Date
		/// </summary>
		[DataMember]
		public string BirthDate
		{
			get;
			set;
		}

		/// <summary>
		/// Gets Or Sets NextDeliveryDate
		/// </summary>
		[DataMember]
		public string NextDeliveryDate { get; set; }

		/// <summary>
		/// Gets Or Sets Status of transaction
		/// </summary>
		[DataMember]
		public string Status { get; set; }

		/// <summary>
		/// Gets Or Sets ErrorMessage if status is Failed
		/// </summary>
		[DataMember]
		public string ErrorMessage { get; set; }



		/// <summary>
		/// Gets or sets Order Create Date
		/// </summary>
		[DataMember]
		public string UserID
		{
			get;
			set;
		}

	}
}