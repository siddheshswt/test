﻿//-----------------------------------------------------------------------
//  <copyright file="CustomerParameterBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Customer Parameter Business Model
    /// </summary>
    [Serializable]
    [DataContract]   
    public class CustomerParameterBusinessModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether [allow increase order quantity].
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow increase order quantity]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool AllowIncreaseOrderQty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [check age].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check age]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool CheckAge
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer ID
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long CustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Parameter ID
        /// </summary>
        /// <value>
        /// The customer parameter identifier.
        /// </value>
        [DataMember]
        public long CustomerParameterID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerParameterBusinessModel" /> is day1.
        /// </summary>
        /// <value>
        ///   <c>true</c> if day1; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Day1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerParameterBusinessModel" /> is day2.
        /// </summary>
        /// <value>
        ///   <c>true</c> if day2; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Day2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerParameterBusinessModel" /> is day3.
        /// </summary>
        /// <value>
        ///   <c>true</c> if day3; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Day3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerParameterBusinessModel" /> is day4.
        /// </summary>
        /// <value>
        ///   <c>true</c> if day4; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Day4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerParameterBusinessModel" /> is day5.
        /// </summary>
        /// <value>
        ///   <c>true</c> if day5; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Day5
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is ADP mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is ADP mandatory; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsADPMandatory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is Customer Approval Required
        /// </summary>
        /// <value>
        /// The is customer approval req.
        /// </value>
        [DataMember]
        public bool? IsCustomerApprovalReq
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is next assessment date mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is next assessment date mandatory; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsNextAssessmentDateMandatory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is NHS Id mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is NHS Id mandatory; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsNHSIDMandatory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is telephone mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is telephone mandatory; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsTelephoneMandatory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Max Pad Per Day
        /// </summary>
        /// <value>
        /// The maximum pad per day.
        /// </value>
        [DataMember]
        public decimal MaxPadPerDay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Next Assessment Date
        /// </summary>
        /// <value>
        /// The next assessment date.
        /// </value>
        [DataMember]
        public int? NextAssessmentDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [order approval].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [order approval]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool OrderApproval
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [order creation allowed].
        /// </summary>
        /// <value>
        /// <c>true</c> if [order creation allowed]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool OrderCreationAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Cut Off Time
        /// </summary>
        /// <value>
        /// The order cut off time.
        /// </value>
        [DataMember]       
        public string OrderCutOffTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Lead Time
        /// </summary>
        /// <value>
        /// The order lead time.
        /// </value>
        [DataMember]
        public int OrderLeadTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round ID
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserID
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Frequency
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        [DataMember]
        public int Frequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Age
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        [DataMember]
        public int? Age
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsorderedAllowed
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is order allowed; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsOrderAllowed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is NDD Allow
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is NDD allow; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsNDDAllow
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round ID
        /// </summary>
        /// <value>
        /// The post code round identifier.
        /// </value>
        [DataMember]
        public string PostCodeRoundID
        {
            get;
            set;
        }


        #region CustomerDetails
        /// <summary>
        /// Gets or sets CustomerName
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPCustomerID
        /// </summary>
        /// <value>
        /// The sap customer identifier.
        /// </value>
        [DataMember]
        public string SAPCustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsCollectionApplicable
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is collection applicable; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsCollectionApplicable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPCustomerID
        /// </summary>
        /// <value>
        /// The medical analysis information.
        /// </value>
        [DataMember]
        public List<ListTypeAdvancedBusinessModel> MedicalAnalysisInformation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RemovePatientReasonCodes
        /// </summary>
        /// <value>
        /// The remove patient reason codes.
        /// </value>
        [DataMember]
        public List<ListTypeAdvancedBusinessModel> RemovePatientReasonCodes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets StoppedPatientReasonCodes
        /// </summary>
        /// <value>
        /// The stop patient reason codes.
        /// </value>
        [DataMember]
        public List<ListTypeAdvancedBusinessModel> StopPatientReasonCodes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [clinical contact linkage].
        /// </summary>
        /// <value>
        /// <c>true</c> if [clinical contact linkage]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsClinicalContactWithLinkage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round ID
        /// </summary>
        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sample Product Type
        /// </summary>
        [DataMember]
        public string SampleOrderProductType
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// Gets Or Sets CountOfMandateField
        /// </summary>
        [DataMember]
        public long CountOfMandatoryAnalysisField { get; set; }
    }
}