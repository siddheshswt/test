﻿//-----------------------------------------------------------------------
//  <copyright file="PatientOrdersBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Patient Orders Business Model
    /// </summary>
    [DataContract]
    public class PatientOrdersBusinessModel : MassDataBusinessModel
    {

        /// <summary>
        /// Gets or sets Customer ID
        /// </summary>
        [DataMember]
        public long CustomerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Patient No
        /// </summary>
        [DataMember]
        public string SAPPatientNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient ID
        /// </summary>
        [DataMember]
        public long PatientID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHome ID
        /// </summary>
        [DataMember]
        public long? CareHomeID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Type
        /// </summary>
        [DataMember]
        public long? OrderType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Id
        /// </summary>
        [DataMember]
        public long? OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Display Id
        /// </summary>
        [DataMember]
        public string ProductDisplayId
        {
            get;
            set;
        }

        /// <summary>
		/// Gets or sets Order Status
        /// </summary>
        [DataMember]
        public long OrderStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Quantity
        /// </summary>
        [DataMember]
        public long Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Delivery Date
        /// </summary>
        [DataMember]
        public string DeliveryDate
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets a flag Send To Sap
        /// </summary>
        public bool SendToSap { get; set; }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DerivedNDD
        /// </summary>
        [DataMember]
        public string DerivedNDD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Delivery Frequency
        /// </summary>
        [DataMember]
        public long? DeliveryFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        [DataMember]
        public string ProductName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Exclude Service Charge
        /// </summary>
        [DataMember]
        public bool? ExcludeServiceCharge
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IDX Prescription Product Id
        /// </summary>
        [DataMember]
        public long? IDXPrescriptionProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Invoice Number
        /// </summary>
        [DataMember]
        public string InvoiceNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is FOC
        /// </summary>
        [DataMember]
        public bool? IsFOC
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Last Delivery Date
        /// </summary>
        [DataMember]
        public string LastDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Modified Date
        /// </summary>
        [DataMember]
        public string ModifiedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Next Delivery Date
        /// </summary>
        [DataMember]
        public string NextDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Date
        /// </summary>
        [DataMember]
        public string OrderDate
        {
            get;
            set;
        }
      
        /// <summary>
        /// Gets or sets Order Note
        /// </summary>
        [DataMember]
        public string OrderNote
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Name
        /// </summary>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient First Name
        /// </summary>
        [DataMember]
        public string PatientFirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Last Name
        /// </summary>
        [DataMember]
        public string PatientLastName
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets Patient Type
        /// </summary>
        [DataMember]
        public string PatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Type
        /// </summary>
        [DataMember]
        public long PatientTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Description
        /// </summary>
        [DataMember]
        public string ProductDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Id
        /// </summary>
        [DataMember]
        public long? ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets QuantityToCompare , will be same as Quantity while loading and uneditable
        /// </summary>
        [DataMember]
        public long QuantityToCompare
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round ID
        /// </summary>
        [DataMember]
        public string RoundID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Delivery No
        /// </summary>
        [DataMember]
        public string SAPDeliveryNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAP Order No
        /// </summary>
        [DataMember]
        public string SAPOrderNo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Order Status
        /// </summary>
        [DataMember]
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Unit
        /// </summary>
        [DataMember]
        public string Unit
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets User Id
        /// </summary>
        [DataMember]
        public Guid ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsOrderActivated
        /// </summary>
        [DataMember]
        public bool? IsOrderActivated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient Status
        /// </summary>
        [DataMember]
        public string PatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Disclaimer Person Name
        /// </summary>
        [DataMember]
        public string PersonName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Disclaimer Person Position
        /// </summary>
        [DataMember]
        public string PersonPosition
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets IsDisclaimerRequired
        /// </summary>
        [DataMember]
        public bool? IsDislcaimerRequired
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DisclaimerId
        /// </summary>
        [DataMember]
        public long? OrderDisclaimerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SortType
        /// </summary>
        [DataMember]
        public string SortType { get; set; }

        /// <summary>
        /// Gets or sets SortColumn
        /// </summary>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets Page
        /// </summary>
        [DataMember]
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets RowCount
        /// </summary>
        [DataMember]
        public int RowCount { get; set; }

        /// <summary>
        /// Gets or sets SearchId
        /// </summary>
        [DataMember]
        public string SearchId { get; set; }

        /// <summary>
        /// Gets or sets RecentOrderType
        /// </summary>
        [DataMember]
        public string RecentOrderType { get; set; }

        /// <summary>
        /// Gets or sets Recent Order Creation Type
        /// </summary>
        [DataMember]
        public string RecentOrderCreationType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is one off order.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is one off order; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsOneOffOrder { get; set; }

        /// <summary>
        /// Gets or sets isProductLevel
        /// </summary>
        [DataMember]
        public bool IsProductLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PurchaseOrderNum
        /// </summary>
        [DataMember]
        public string PurchaseOrderNum { get; set; }

        /// <summary>
        /// Gets or Sets OrderCreationType [Online/Telephonic]
        /// </summary>
        [DataMember]
        public long? OrderCreationType { get; set; }

        /// <summary>
        /// Gets or Sets EmailId
        /// </summary>
        [DataMember]
        public string EmailId { get; set; }

        /// <summary>
        /// Gets or Sets OrderDateDisplay
        /// </summary>       
        public DateTime OrderDateDisplay { get; set; }

        /// <summary>
        /// Gets or sets Invoice Date
        /// </summary>
        [DataMember]
        public DateTime? InvoiceDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Invoice Quantity
        /// </summary>
        [DataMember]
        public decimal? InvoiceQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Plant
        /// </summary>
        [DataMember]
        public string Plant
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ProductCode
        /// </summary>
        [DataMember]
        public string ProductCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets POD Date
        /// </summary>
        [DataMember]
        public DateTime? PODDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Quantity
        /// </summary>
        [DataMember]
        public decimal? DeliveryQty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Next Delivery Date
        /// </summary>
        [DataMember]
        public DateTime? ActDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Actual Delivery Date
        /// </summary>
        [DataMember]
        public string ActualDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Color
        /// </summary>
        [DataMember]
        public string SetColor
        {
            get;
            set;
        }

        [DataMember]
        public List<string> groupingData
        {
            get;
            set;
        }

        /// <summary>
        /// Sets Patient ADP From Community Order Screen
        /// </summary>
        [DataMember]
        public string PatientADP { get; set; }

        /// <summary>
        /// Gets or sets Patient Removed Status
        /// </summary>
        [DataMember]
        public bool IsPatientRemoved { get; set; }

        /// <summary>
        /// Gets or Sets Removed ReasonCode
        /// </summary>
        [DataMember]
        public string ReasonCode{ get; set; }

        /// <summary>
        /// Gets or Sets IsNDDManuallyChanged
        /// </summary>
        [DataMember]
        public bool IsNDDManuallyChanged { get; set; }

        /// <summary>
        /// Gets or Sets IsOrderCancel
        /// </summary>
        [DataMember]
        public bool IsOrderCancel { get; set; }

        /// <summary>
        /// Gets Or Sets IsActivatedOrder
        /// </summary>
        [DataMember]
        public bool IsActivatedOrder { get; set; }

        /// <summary>
        /// Gets Or Sets IsProductOrderActivated
        /// </summary>
        [DataMember]
        public bool IsProductOrderActivated { get; set; }

        /// <summary>
        /// Gets Or Sets IsCommunityOrder
        /// </summary>
        [DataMember]
        public bool IsCommunityOrder { get; set; }
    }
}