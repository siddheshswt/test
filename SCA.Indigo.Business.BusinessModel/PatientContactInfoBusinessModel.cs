﻿//-----------------------------------------------------------------------
//  <copyright file="PatientContactInfoBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Patient ContactInfo Business Model
    /// </summary>
    [DataContract]
    public class PatientContactInfoBusinessModel
    {
        /// <summary>
        /// Gets or sets AddressId
        /// </summary>
        /// <value>
        /// The address identifier.
        /// </value>
        [DataMember]
        public long? AddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedById
        /// </summary>
        /// <value>
        /// The created by identifier.
        /// </value>
        [DataMember]
        public string CreatedById
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDateTime
        /// </summary>
        /// <value>
        /// The created date time.
        /// </value>
        [DataMember]
        public string CreatedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [DataMember]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FaxNumber
        /// </summary>
        /// <value>
        /// The fax number.
        /// </value>
        [DataMember]
        public string FaxNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets FirstName
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Gender
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [DataMember]
        public string Gender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsActive
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Job Title
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastName
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets MobileNumber
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        [DataMember]
        public string MobileNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        [DataMember]
        public string ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedDate
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        [DataMember]
        public string ModifiedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientID
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string PatientID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Patient name
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonalInformationId
        /// </summary>
        /// <value>
        /// The personal information identifier.
        /// </value>
        [DataMember]
        public long PersonalInformationId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonalInformationIds
        /// </summary>
        /// <value>
        /// The personal information ids.
        /// </value>
        [DataMember]
        public List<string> PersonalInformationIds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TelephoneNumber
        /// </summary>
        /// <value>
        /// The telephone number.
        /// </value>
        [DataMember]
        public string TelephoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TitleId
        /// </summary>
        /// <value>
        /// The title identifier.
        /// </value>
        [DataMember]
        public long? TitleId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserID
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Customer Name
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Customer Id
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// CareHome Id
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets SortType
        /// </summary>
        /// <value>
        /// The type of the sort.
        /// </value>
        [DataMember]
        public string SortType { get; set; }

        /// <summary>
        /// Gets or sets SortColumn
        /// </summary>
        /// <value>
        /// The sort column.
        /// </value>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets Page
        /// </summary>
        /// <value>
        /// The page.
        /// </value>
        [DataMember]
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets RowCount
        /// </summary>
        /// <value>
        /// The row count.
        /// </value>
        [DataMember]
        public int RowCount { get; set; }

        /// <summary>
        /// Gets or sets ContactPersonId
        /// </summary>
        /// <value>
        /// The contact person identifier.
        /// </value>
        [DataMember]
        public long ContactPersonId { get; set; }
    }
}