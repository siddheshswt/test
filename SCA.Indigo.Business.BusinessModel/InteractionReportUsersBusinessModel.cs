﻿//-----------------------------------------------------------------------
//  <copyright file="DropDownData.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------

namespace SCA.Indigo.Business.BusinessModels
{
    using System.Runtime.Serialization;

    /// <summary>
    /// DropDown Data Class
    /// </summary>
    [DataContract]
    public class InteractionReportUsersBusinessModel
    {
        /// <summary>
        /// Gets or sets User Name
        /// </summary>
        [DataMember]
        public string DisplayText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        [DataMember]
        public string Value
        {
            get;
            set;
        }
    }
}
