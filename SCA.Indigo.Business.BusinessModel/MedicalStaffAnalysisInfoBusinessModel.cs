﻿//-----------------------------------------------------------------------
//  <copyright file="MedicalStaffAnalysisInfoBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// MedicalStaff and AnalysisInfo Business Model
    /// </summary>
    [DataContract]
    public class MedicalStaffAnalysisInfoBusinessModel 
    {
        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerMedicalStaffAnalysisInfoId
        /// </summary>
        /// <value>
        /// The customer medical staff analysis information identifier.
        /// </value>
        [DataMember]
        public long CustomerMedicalStaffAnalysisInfoId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerMedicalStaffAnalysisInfoId
        /// </summary>
        /// <value>
        /// The customer medical staff analysis information ids.
        /// </value>
        [DataMember]
        public string CustomerMedicalStaffAnalysisInfoIds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the medical staff analysis identifier.
        /// </summary>
        /// <value>
        /// The medical staff analysis identifier.
        /// </value>
        [DataMember]
        public long MedicalStaffAnalysisId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets customerName
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DefaultText
        /// </summary>
        /// <value>
        /// The default text.
        /// </value>
        [DataMember]
        public string DefaultText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets GridName
        /// </summary>
        /// <value>
        /// The name of the grid.
        /// </value>
        [DataMember]
        public string GridName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListId
        /// </summary>
        /// <value>
        /// The list identifier.
        /// </value>
        [DataMember]
        public long? ListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListTypeId
        /// </summary>
        /// <value>
        /// The list type identifier.
        /// </value>
        [DataMember]
        public long? ListTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientId
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long PatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets patientName
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets StaffId
        /// </summary>
        /// <value>
        /// The staff identifier.
        /// </value>
        [DataMember]
        public string StaffId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets StaffName
        /// </summary>
        /// <value>
        /// The name of the staff.
        /// </value>
        [DataMember]
        public string StaffName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TextSearch
        /// </summary>
        /// <value>
        /// The text search.
        /// </value>
        [DataMember]
        public string TextSearch
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TranslationId
        /// </summary>
        /// <value>
        /// The translation identifier.
        /// </value>
        [DataMember]
        public long TranslationId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserId
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public Guid UserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedDate
        /// </summary>
        /// <value>
        /// The modified date.
        /// </value>
        [DataMember]
        public string ModifiedDate 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is mandatory; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsMandatory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the type of the is field.
        /// </summary>
        /// <value>
        /// The type of the is field.
        /// </value>
        [DataMember]
        public bool? IsFieldType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is clinical contact with linkage.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is clinical contact with linkage; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsClinicalContactWithLinkage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HierarchyListId
        /// </summary>
        /// <value>
        /// The hierarchy list identifier.
        /// </value>
        [DataMember]
        public long HierarchyListId
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the hierarchy row number.
        /// </summary>
        /// <value>
        /// The hierarchy row number.
        /// </value>
        [DataMember]
        public long HierarchyRowNum
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the hierarchy link data.
        /// </summary>
        /// <value>
        /// The hierarchy link data.
        /// </value>
        [DataMember]
        public List<DropDownDataBusinessModel> HierarchyLinkData
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [check for child linkage].
        /// </summary>
        /// <value>
        /// <c>true</c> if [check for child linkage]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool CheckForChildLinkage
        {
            get;
            set;
        }

        [DataMember]
        public long IDXPatientMedicalStaffId 
        { 
            get; 
            set; 
        }
    }
}