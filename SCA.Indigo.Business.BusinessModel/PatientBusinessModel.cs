﻿//-----------------------------------------------------------------------
//  <copyright file="PatientBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Patient Business Model
    /// </summary>
    [DataContract]
    public class PatientBusinessModel : BaseBusinessModel
    {

        /// <summary>
        /// Gets or sets LoadId
        /// </summary>
        /// <value>
        /// The LoadId.
        /// </value>
        [DataMember]
        public long? LoadId
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ADP1
        /// </summary>
        /// <value>
        /// The ad p1.
        /// </value>
        [DataMember]
        public string ADP1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ADP2
        /// </summary>
        /// <value>
        /// The ad p2.
        /// </value>
        [DataMember]
        public string ADP2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Age
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        [DataMember]
        public int? Age
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AlternateDeliveryPoint
        /// </summary>
        /// <value>
        /// The alternate delivery point.
        /// </value>
        [DataMember]
        public string AlternateDeliveryPoint
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AssessmentDate
        /// </summary>
        /// <value>
        /// The assessment date.
        /// </value>
        [DataMember]
        public string AssessmentDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeAssignmentDate
        /// </summary>
        /// <value>
        /// The care home assignment date.
        /// </value>
        [DataMember]
        public string CareHomeAssignmentDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeId
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public long? CareHomeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeName
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        [DataMember]
        public string CareHomeName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        [DataMember]
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CommunicationFormat
        /// </summary>
        /// <value>
        /// The communication format.
        /// </value>
        [DataMember]
        public long? CommunicationFormat
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ContactPersonId
        /// </summary>
        /// <value>
        /// The contact person identifier.
        /// </value>
        [DataMember]
        public long? ContactPersonId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Country
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataMember]
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets County
        /// </summary>
        /// <value>
        /// The county.
        /// </value>
        [DataMember]
        public string County
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public string CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedDate
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        [DataMember]
        public string CreatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerId
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public long? CustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DateOfBirth
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        [DataMember]
        public string DateOfBirth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DeliveryDate
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [DataMember]
        public string DeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DeliveryFrequency
        /// </summary>
        /// <value>
        /// The delivery frequency.
        /// </value>
        [DataMember]
        public string DeliveryFrequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DeliveryInstructionText
        /// </summary>
        /// <value>
        /// The delivery instruction text.
        /// </value>
        [DataMember]
        public string DeliveryInstructionText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DropDownCustomerId
        /// </summary>
        /// <value>
        /// The drop down customer identifier.
        /// </value>
        [DataMember]
        public long DropDownCustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DropDownCustomerName
        /// </summary>
        /// <value>
        /// The name of the drop down customer.
        /// </value>
        [DataMember]
        public string DropDownCustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HouseName
        /// </summary>
        /// <value>
        /// The name of the house.
        /// </value>
        [DataMember]
        public string HouseName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HouseNumber
        /// </summary>
        /// <value>
        /// The house number.
        /// </value>
        [DataMember]
        public string HouseNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedBy
        /// </summary>
        /// <value>
        /// The last modified by.
        /// </value>
        [DataMember]
        public long? LastModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocalId
        /// </summary>
        /// <value>
        /// The local identifier.
        /// </value>
        [DataMember]
        public string LocalId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the medicalstaff list.
        /// </summary>
        /// <value>
        /// The medicalstaff list.
        /// </value>
        [DataMember]
        public List<MedicalStaffAnalysisInfoBusinessModel> MedicalstaffList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        [DataMember]
        public string ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NextAssessmentDate
        /// </summary>
        /// <value>
        /// The next assessment date.
        /// </value>
        [DataMember]
        public string NextAssessmentDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NHSId
        /// </summary>
        /// <value>
        /// The NHS identifier.
        /// </value>
        [DataMember]
        public string NHSId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p address identifier.
        /// </summary>
        /// <value>
        /// The p address identifier.
        /// </value>
        [DataMember]
        public long PAddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p assigned care home.
        /// </summary>
        /// <value>
        /// The p assigned care home.
        /// </value>
        [DataMember]
        public long? PAssignedCareHome
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the patient analysis list.
        /// </summary>
        /// <value>
        /// The patient analysis list.
        /// </value>
        [DataMember]
        public List<MedicalStaffAnalysisInfoBusinessModel> PatientAnalysisList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientId
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long PatientId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientStatusId
        /// </summary>
        /// <value>
        /// The patient status identifier.
        /// </value>
        [DataMember]
        public long? PatientStatusId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientTypeId
        /// </summary>
        /// <value>
        /// The patient type identifier.
        /// </value>
        [DataMember]
        public long? PatientTypeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientTypeCode
        /// </summary>
        /// <value>
        /// The patient type code.
        /// </value>
        [DataMember]
        public string PatientTypeCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p email.
        /// </summary>
        /// <value>
        /// The p email.
        /// </value>
        [DataMember]
        public string PEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonId
        /// </summary>
        /// <value>
        /// The person identifier.
        /// </value>
        [DataMember]
        public long? PersonId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the first name of the p.
        /// </summary>
        /// <value>
        /// The first name of the p.
        /// </value>
        [DataMember]
        public string PFirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p gender.
        /// </summary>
        /// <value>
        /// The p gender.
        /// </value>
        [DataMember]
        public long PGender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the last name of the p.
        /// </summary>
        /// <value>
        /// The last name of the p.
        /// </value>
        [DataMember]
        public string PLastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p mobile number.
        /// </summary>
        /// <value>
        /// The p mobile number.
        /// </value>
        [DataMember]
        public string PMobileNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets OldPatientStatusId
        /// </summary>
        /// <value>
        /// The p old patient status.
        /// </value>
        [DataMember]
        public string POldPatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientStatus
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        [DataMember]
        public long? PatientStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientStatusCode
        /// </summary>
        /// <value>
        /// The patient status code.
        /// </value>
        [DataMember]
        public string PatientStatusCode
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the type of the p old patient.
        /// </summary>
        /// <value>
        /// The type of the p old patient.
        /// </value>
        [DataMember]
        public string POldPatientType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PostCode
        /// </summary>
        /// <value>
        /// The post code.
        /// </value>
        [DataMember]
        public string PostCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PrescriptionId
        /// </summary>
        /// <value>
        /// The prescription identifier.
        /// </value>
        [DataMember]
        public long PrescriptionId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets OldPatientTypeId
        /// </summary>
        /// <value>
        /// The prescription list.
        /// </value>
        [DataMember]
        public List<PrescriptionBusinessModel> PrescriptionList
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the Carehome Maintenace.
        /// </summary>
        /// <value>
        /// The logs.
        /// </value>
        [DataMember]
        public List<CarehomeMaintenanceReportBusinessModel> CarehomeMaintanceReport
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ProductId
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        [DataMember]
        public long ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p telephone number.
        /// </summary>
        /// <value>
        /// The p telephone number.
        /// </value>
        [DataMember]
        public string PTelephoneNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the p title identifier.
        /// </summary>
        /// <value>
        /// The p title identifier.
        /// </value>
        [DataMember]
        public long? PTitleId
        {
            get;
            set;
        }

        // <summary>
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        [DataMember]
        public long Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ReasonCodeId
        /// </summary>
        /// <value>
        /// The reason code identifier.
        /// </value>
        [DataMember]
        public long? ReasonCodeId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoundId
        /// </summary>
        /// <value>
        /// The round identifier.
        /// </value>
        [DataMember]
        public string RoundId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Round
        /// </summary>
        /// <value>
        /// The round.
        /// </value>
        [DataMember]
        public string Round
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPPatientNumber
        /// </summary>
        /// <value>
        /// The sap patient number.
        /// </value>
        [DataMember]
        public string SAPPatientNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPCustomerNumber
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        [DataMember]
        public string SAPCustomerNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AnyActiveOrder
        /// </summary>
        /// <value>
        /// Any active order.
        /// </value>
        [DataMember]
        public bool? AnyActiveOrder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerName
        /// </summary>
        /// <value>
        /// The assigned customer.
        /// </value>
        [DataMember]
        public string AssignedCustomer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is medical information editable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is medical information editable; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsMedicalInfoEditable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is patient information editable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is patient information editable; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsPatientInfoEditable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is prescription editable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is prescription editable; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsPrescriptionEditable { get; set; }

        /// <summary>
        /// Gets or sets the list identifier.
        /// </summary>
        /// <value>
        /// The list identifier.
        /// </value>
        [DataMember]
        public string ListId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display text.
        /// </summary>
        /// <value>
        /// The display text.
        /// </value>
        [DataMember]
        public string DisplayText
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NextDeliveryDate
        /// </summary>
        /// <value>
        /// The next delivery date.
        /// </value>
        [DataMember]
        public string NextDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RoleName
        /// </summary>
        /// <value>
        /// The name of the role.
        /// </value>
        [DataMember]
        public string RoleName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CareHomeNextDeliveryDate
        /// </summary>
        /// <value>
        /// The care home next delivery date.
        /// </value>
        [DataMember]
        public string CareHomeNextDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets BillTo
        /// </summary>
        /// <value>
        /// The bill to.
        /// </value>
        [DataMember]
        public int? BillTo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NurseComment
        /// </summary>
        /// <value>
        /// The nurse comment.
        /// </value>
        [DataMember]
        public NurseCommentsBusinessModel NurseComment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerFirstName
        /// </summary>
        /// <value>
        /// The first name of the customer.
        /// </value>
        [DataMember]
        public string CustomerFirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerLastName
        /// </summary>
        /// <value>
        /// The last name of the customer.
        /// </value>
        [DataMember]
        public string CustomerLastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientName
        /// </summary>
        /// <value>
        /// The name of the patient.
        /// </value>
        [DataMember]
        public string PatientName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PatientSaveStatus
        /// </summary>
        /// <value>
        ///   <c>true</c> if [patient save status]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool PatientSaveStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ProductDescription
        /// </summary>
        /// <value>
        /// The product description.
        /// </value>
        [DataMember]
        public string ProductDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SAPProductId
        /// </summary>
        /// <value>
        /// The sap product identifier.
        /// </value>
        [DataMember]
        public string SAPProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the care home status.
        /// </summary>
        /// <value>
        /// The care home status.
        /// </value>
        [DataMember]
        public long? CareHomeStatus { get; set; }

        /// <summary>
        /// Gets or sets the name of the correspondence care home.
        /// </summary>
        /// <value>
        /// The name of the correspondence care home.
        /// </value>
        [DataMember]
        public string CorrespondenceCareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the correspondence address1.
        /// </summary>
        /// <value>
        /// The correspondence address1.
        /// </value>
        [DataMember]
        public string CorrespondenceAddress1 { get; set; }

        /// <summary>
        /// Gets or sets the correspondence address2.
        /// </summary>
        /// <value>
        /// The correspondence address2.
        /// </value>
        [DataMember]
        public string CorrespondenceAddress2 { get; set; }

        /// <summary>
        /// Gets or sets the correspondence town or city.
        /// </summary>
        /// <value>
        /// The correspondence town or city.
        /// </value>
        [DataMember]
        public string CorrespondenceTownOrCity { get; set; }

        /// <summary>
        /// Gets or sets the correspondence post code.
        /// </summary>
        /// <value>
        /// The correspondence post code.
        /// </value>
        [DataMember]
        public string CorrespondencePostCode { get; set; }

        /// <summary>
        /// Gets or sets the correspondence county.
        /// </summary>
        /// <value>
        /// The correspondence county.
        /// </value>
        [DataMember]
        public string CorrespondenceCounty { get; set; }

        /// <summary>
        /// Gets or sets the correspondence country.
        /// </summary>
        /// <value>
        /// The correspondence country.
        /// </value>
        [DataMember]
        public string CorrespondenceCountry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [correspondence same as delivery].
        /// </summary>
        /// <value>
        /// <c>true</c> if [correspondence same as delivery]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool CorrespondenceSameAsDelivery { get; set; }

        /// <summary>
        /// Gets or sets the communication preferences.
        /// </summary>
        /// <value>
        /// The communication preferences.
        /// </value>
        [DataMember]
        public string[] CommunicationPreferences { get; set; }

        /// <summary>
        /// Gets or sets the marketing preferences.
        /// </summary>
        /// <value>
        /// The marketing preferences.
        /// </value>
        [DataMember]
        public string[] MarketingPreferences { get; set; }

        /// <summary>
        /// Gets or sets the is data protected.
        /// </summary>
        /// <value>
        /// The is data protected.
        /// </value>
        [DataMember]
        public bool? IsDataProtected { get; set; }

        /// <summary>
        /// Gets or sets the type of the care home order.
        /// </summary>
        /// <value>
        /// The type of the care home order.
        /// </value>
        [DataMember]
        public long? CareHomeOrderType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is clinical linkage.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is clinical linkage; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsClinicalLinkage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is clinical contact changed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is clinical contact changed; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsClinicalContactChanged { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is analysis information changed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is analysis information changed; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsAnalysisInfoChanged { get; set; }


        /// <summary>
        /// Gets or sets CareHomeSAPId
        /// </summary>
        /// <value>
        /// The care home sap identifier.
        /// </value>
        [DataMember]
        public string CareHomeSAPId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CustomerName
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RemovedStoppedDateTime
        /// </summary>
        /// <value>
        /// The Removed Stopped Date Time.
        /// </value>
        [DataMember]
        public string RemovedStoppedDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets AssessedPadsPerDay
        /// </summary>
        /// <value>
        /// The Assessed Pads Per Day.
        /// </value>
        [DataMember]
        public string AssessedPadsPerDay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ActPPD
        /// </summary>
        /// <value>
        /// The Act PPD.
        /// </value>
        [DataMember]
        public string ActPPD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Comment
        /// </summary>
        /// <value>
        /// The Comment.
        /// </value>
        [DataMember]
        public string Comment
        {
            get;
            set;
        }
        /// <summary>
        /// Sets PabeNumber
        /// </summary>
        /// <value>
        /// The page number.
        /// </value>
        [DataMember]
        public long PageNumber { get; set; }

        /// <summary>
        /// Sets RecordsPerPage
        /// </summary>
        /// <value>
        /// The records per page.
        /// </value>
        [DataMember]
        public long RecordsPerPage { get; set; }

        /// <summary>
        /// Sets TotalRecords
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        [DataMember]
        public long TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets IsExport ( Whether the Report is to be Exported )
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is export; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsExport { get; set; }
        /// <summary>
        /// Gets or Sets the UserId of LoggedIn user [one eho requested the search]
        /// </summary>
        /// <value>
        /// The logged in user identifier.
        /// </value>
        [DataMember]
        public Guid LoggedInUserId { get; set; }
        /// <summary>
        /// Gets or sets the UserId of the User, for whom Audit logs are to be viewed
        /// </summary>
        /// <value>
        /// The searched user identifier.
        /// </value>
        [DataMember]
        public Guid SearchedUserId { get; set; }
        /// <summary>
        /// Sets SortColumn
        /// </summary>
        /// <value>
        /// The SortColumn.
        /// </value>
        [DataMember]
        public string SortColumn { get; set; }
        /// <summary>
        /// Sets SortBy
        /// </summary>
        /// <value>
        /// The SortBy.
        /// </value>
        [DataMember]
        public string SortBy { get; set; }
        /// <summary>
        /// Sets SortBy
        /// </summary>
        /// <value>
        /// The SortBy.
        /// </value>
        [DataMember]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets ListIndex
        /// </summary>
        /// <value>
        /// The index of the list.
        /// </value>
        [DataMember]
        public int ListIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Title text.
        /// </summary>
        /// <value>
        /// The Title text.
        /// </value>
        [DataMember]
        public string Title
        {
            get;
            set;
        }
    }
}