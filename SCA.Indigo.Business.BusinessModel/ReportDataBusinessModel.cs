﻿//-----------------------------------------------------------------------
//  <copyright file="ReportDataBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Report Data Business Model
    /// </summary>
    [DataContract]
    public class ReportDataBusinessModel
    {
        /// <summary>
        /// Gets or sets the <see cref="System.Object"/> with the specified property name.
        /// </summary>
        /// <value>
        /// The <see cref="System.Object"/>.
        /// </value>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        /// <summary>
        /// Gets or sets the report builder identifier.
        /// </summary>
        /// <value>
        /// The report builder identifier.
        /// </value>
        [DataMember]
        public string ReportBuilderId { get; set; }

        /// <summary>
        /// Gets or sets the name of the report format.
        /// </summary>
        /// <value>
        /// The name of the report format.
        /// </value>
        [DataMember]
        public string ReportFormatName { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the customer start date.
        /// </summary>
        /// <value>
        /// The customer start date.
        /// </value>
        [DataMember]
        public string CustomerStartDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        [DataMember]
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public string PatientId { get; set; }

        /// <summary>
        /// Gets or sets the sap patient number.
        /// </summary>
        /// <value>
        /// The sap patient number.
        /// </value>
        [DataMember]
        public string SAPPatientNumber { get; set; }

        /// <summary>
        /// Gets or sets the patient status.
        /// </summary>
        /// <value>
        /// The patient status.
        /// </value>
        [DataMember]
        public string PatientStatus { get; set; }

        /// <summary>
        /// Gets or sets the type of the patient.
        /// </summary>
        /// <value>
        /// The type of the patient.
        /// </value>
        [DataMember]
        public string PatientType { get; set; }

        /// <summary>
        /// Gets or sets the patient next assessment date.
        /// </summary>
        /// <value>
        /// The patient next assessment date.
        /// </value>
        [DataMember]
        public string PatientNextAssessmentDate { get; set; }

        /// <summary>
        /// Gets or sets the care home identifier.
        /// </summary>
        /// <value>
        /// The care home identifier.
        /// </value>
        [DataMember]
        public string CareHomeId { get; set; }

        /// <summary>
        /// Gets or sets the sap care home number.
        /// </summary>
        /// <value>
        /// The sap care home number.
        /// </value>
        [DataMember]
        public string SAPCareHomeNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of the care home.
        /// </summary>
        /// <value>
        /// The type of the care home.
        /// </value>
        [DataMember]
        public string CareHomeType { get; set; }

        /// <summary>
        /// Gets or sets the care home status.
        /// </summary>
        /// <value>
        /// The care home status.
        /// </value>
        [DataMember]
        public string CareHomeStatus { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home.
        /// </summary>
        /// <value>
        /// The name of the care home.
        /// </value>
        [DataMember]
        public string CareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        [DataMember]
        public string OrderId { get; set; }

        /// <summary>
        /// Gets or sets the type of the order.
        /// </summary>
        /// <value>
        /// The type of the order.
        /// </value>
        [DataMember]
        public string OrderType { get; set; }

        /// <summary>
        /// Gets or sets the Order Creation Type.
        /// </summary>
        /// <value>
        /// The Order Creation Type.
        /// </value>
        [DataMember]
        public string OrderCreationType { get; set; }


        /// <summary>
        /// Gets or sets the POD DATE.
        /// </summary>
        /// <value>
        /// The POD DATE.
        /// </value>
        [DataMember]
        public string PODDate { get; set; }

        /// <summary>
        /// Gets or sets the Delivery Quantity.
        /// </summary>
        /// <value>
        /// The Delivery Quantity.
        /// </value>
        [DataMember]
        public string DeliveryQuantity { get; set; }

        /// <summary>
        /// Gets or sets the Delivery Unit.
        /// </summary>
        /// <value>
        /// The Delivery Unit.
        /// </value>
        [DataMember]
        public string DeliveryUnit { get; set; }

        /// <summary>
        /// Gets or sets the sap order no.
        /// </summary>
        /// <value>
        /// The sap order no.
        /// </value>
        [DataMember]
        public string SAPOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the order date.
        /// </summary>
        /// <value>
        /// The order date.
        /// </value>
        [DataMember]
        public string OrderDate { get; set; }

        /// <summary>
        /// Gets or sets the sap order delivery date.
        /// </summary>
        /// <value>
        /// The sap order delivery date.
        /// </value>
        [DataMember]
        public string SAPOrderDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the sap order product delivery date.
        /// </summary>
        /// <value>
        /// The sap order product delivery date.
        /// </value>
        [DataMember]
        public string SAPActualDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the sap customer number.
        /// </summary>
        /// <value>
        /// The sap customer number.
        /// </value>
        [DataMember]
        public string SapCustomerNumber { get; set; }

        /// <summary>
        /// Gets or sets the customer status.
        /// </summary>
        /// <value>
        /// The customer status.
        /// </value>
        [DataMember]
        public string CustomerStatus { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer user.
        /// </summary>
        /// <value>
        /// The name of the customer user.
        /// </value>
        [DataMember]
        public string CustomerUserName { get; set; }

        /// <summary>
        /// Gets or sets the customer authorization pin.
        /// </summary>
        /// <value>
        /// The customer authorization pin.
        /// </value>
        [DataMember]
        public string CustomerAuthorizationPIN { get; set; }

        /// <summary>
        /// Gets or sets the first name of the customer authorized user.
        /// </summary>
        /// <value>
        /// The first name of the customer authorized user.
        /// </value>
        [DataMember]
        public string CustomerAuthorizedUserFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the customer authorized user.
        /// </summary>
        /// <value>
        /// The last name of the customer authorized user.
        /// </value>
        [DataMember]
        public string CustomerAuthorizedUserLastName { get; set; }

        /// <summary>
        /// Gets or sets the customer authorized user job title.
        /// </summary>
        /// <value>
        /// The customer authorized user job title.
        /// </value>
        [DataMember]
        public string CustomerAuthorizedUserJobTitle { get; set; }

        /// <summary>
        /// Gets or sets the customer authorized user telephone number.
        /// </summary>
        /// <value>
        /// The customer authorized user telephone number.
        /// </value>
        [DataMember]
        public string CustomerAuthorizedUserTelephoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the customer authorized user mobile number.
        /// </summary>
        /// <value>
        /// The customer authorized user mobile number.
        /// </value>
        [DataMember]
        public string CustomerAuthorizedUserMobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the customer authorized user email.
        /// </summary>
        /// <value>
        /// The customer authorized user email.
        /// </value>
        [DataMember]
        public string CustomerAuthorizedUserEmail { get; set; }

        /// <summary>
        /// Gets or sets the patient gender.
        /// </summary>
        /// <value>
        /// The patient gender.
        /// </value>
        [DataMember]
        public string PatientGender { get; set; }

        /// <summary>
        /// Gets or sets the patient title.
        /// </summary>
        /// <value>
        /// The patient title.
        /// </value>
        [DataMember]
        public string PatientTitle { get; set; }

        /// <summary>
        /// Gets or sets the first name of the patient.
        /// </summary>
        /// <value>
        /// The first name of the patient.
        /// </value>
        [DataMember]
        public string PatientFirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the patient.
        /// </summary>
        /// <value>
        /// The last name of the patient.
        /// </value>
        [DataMember]
        public string PatientLastName { get; set; }

        /// <summary>
        /// Gets or sets the patient dateof birth.
        /// </summary>
        /// <value>
        /// The patient dateof birth.
        /// </value>
        [DataMember]
        public string PatientDateofBirth { get; set; }

        /// <summary>
        /// Gets or sets the patient reason code.
        /// </summary>
        /// <value>
        /// The patient reason code.
        /// </value>
        [DataMember]
        public string PatientReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery frequency.
        /// </summary>
        /// <value>
        /// The patient delivery frequency.
        /// </value>
        [DataMember]
        public string PatientDeliveryFrequency { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery address line1.
        /// </summary>
        /// <value>
        /// The patient delivery address line1.
        /// </value>
        [DataMember]
        public string PatientDeliveryAddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery address line2.
        /// </summary>
        /// <value>
        /// The patient delivery address line2.
        /// </value>
        [DataMember]
        public string PatientDeliveryAddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery city.
        /// </summary>
        /// <value>
        /// The patient delivery city.
        /// </value>
        [DataMember]
        public string PatientDeliveryCity { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery country.
        /// </summary>
        /// <value>
        /// The patient delivery country.
        /// </value>
        [DataMember]
        public string PatientDeliveryCountry { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient delivery house.
        /// </summary>
        /// <value>
        /// The name of the patient delivery house.
        /// </value>
        [DataMember]
        public string PatientDeliveryHouseName { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery house number.
        /// </summary>
        /// <value>
        /// The patient delivery house number.
        /// </value>
        [DataMember]
        public string PatientDeliveryHouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery post code.
        /// </summary>
        /// <value>
        /// The patient delivery post code.
        /// </value>
        [DataMember]
        public string PatientDeliveryPostCode { get; set; }

        /// <summary>
        /// Gets or sets the patient address address line1.
        /// </summary>
        /// <value>
        /// The patient address address line1.
        /// </value>
        [DataMember]
        public string PatientAddressAddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the patient address address line2.
        /// </summary>
        /// <value>
        /// The patient address address line2.
        /// </value>
        [DataMember]
        public string PatientAddressAddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the patient address city.
        /// </summary>
        /// <value>
        /// The patient address city.
        /// </value>
        [DataMember]
        public string PatientAddressCity { get; set; }

        /// <summary>
        /// Gets or sets the patient address country.
        /// </summary>
        /// <value>
        /// The patient address country.
        /// </value>
        [DataMember]
        public string PatientAddressCountry { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient address house.
        /// </summary>
        /// <value>
        /// The name of the patient address house.
        /// </value>
        [DataMember]
        public string PatientAddressHouseName { get; set; }

        /// <summary>
        /// Gets or sets the patient address house number.
        /// </summary>
        /// <value>
        /// The patient address house number.
        /// </value>
        [DataMember]
        public string PatientAddressHouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the patient address post code.
        /// </summary>
        /// <value>
        /// The patient address post code.
        /// </value>
        [DataMember]
        public string PatientAddressPostCode { get; set; }

        /// <summary>
        /// Gets or sets the patient assessment date.
        /// </summary>
        /// <value>
        /// The patient assessment date.
        /// </value>
        [DataMember]
        public string PatientAssessmentDate { get; set; }

        /// <summary>
        /// Gets or sets the patient telephone number.
        /// </summary>
        /// <value>
        /// The patient telephone number.
        /// </value>
        [DataMember]
        public string PatientTelephoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the patient mobile number.
        /// </summary>
        /// <value>
        /// The patient mobile number.
        /// </value>
        [DataMember]
        public string PatientMobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the patient email.
        /// </summary>
        /// <value>
        /// The patient email.
        /// </value>
        [DataMember]
        public string PatientEmail { get; set; }

        /// <summary>
        /// Gets or sets the patient communication format.
        /// </summary>
        /// <value>
        /// The patient communication format.
        /// </value>
        [DataMember]
        public string PatientCommunicationFormat { get; set; }

        /// <summary>
        /// Gets or sets the patient delivery date.
        /// </summary>
        /// <value>
        /// The patient delivery date.
        /// </value>
        [DataMember]
        public string PatientDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the patient next delivery date.
        /// </summary>
        /// <value>
        /// The patient next delivery date.
        /// </value>
        [DataMember]
        public string PatientNextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the patient round identifier.
        /// </summary>
        /// <value>
        /// The patient round identifier.
        /// </value>
        [DataMember]
        public string PatientRoundId { get; set; }

        /// <summary>
        /// Gets or sets the patient adp LTC.
        /// </summary>
        /// <value>
        /// The patient adp LTC.
        /// </value>
        [DataMember]
        public string PatientAdpLtc { get; set; }

        /// <summary>
        /// Gets or sets the patient adp nc.
        /// </summary>
        /// <value>
        /// The patient adp nc.
        /// </value>
        [DataMember]
        public string PatientAdpNc { get; set; }

        /// <summary>
        /// Gets or sets the patient created date.
        /// </summary>
        /// <value>
        /// The patient created date.
        /// </value>
        [DataMember]
        public string PatientCreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the patient created by.
        /// </summary>
        /// <value>
        /// The patient created by.
        /// </value>
        [DataMember]
        public string PatientCreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the patient NHS identifier.
        /// </summary>
        /// <value>
        /// The patient NHS identifier.
        /// </value>
        [DataMember]
        public string PatientNhsId { get; set; }

        /// <summary>
        /// Gets or sets the patient local identifier.
        /// </summary>
        /// <value>
        /// The patient local identifier.
        /// </value>
        [DataMember]
        public string PatientLocalId { get; set; }

        /// <summary>
        /// Gets or sets the patient clinical contacts.
        /// </summary>
        /// <value>
        /// The patient clinical contacts.
        /// </value>
        [DataMember]
        public string PatientClinicalContacts { get; set; }

        /// <summary>
        /// Gets or sets the patient analysis information.
        /// </summary>
        /// <value>
        /// The patient analysis information.
        /// </value>
        [DataMember]
        public string PatientAnalysisInformation { get; set; }

        /// <summary>
        /// Gets or sets the patient sap care home number.
        /// </summary>
        /// <value>
        /// The patient sap care home number.
        /// </value>
        [DataMember]
        public string PatientSAPCareHomeNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient care home.
        /// </summary>
        /// <value>
        /// The name of the patient care home.
        /// </value>
        [DataMember]
        public string PatientCareHomeName { get; set; }

        /// <summary>
        /// Gets or sets the prescription sap product identifier.
        /// </summary>
        /// <value>
        /// The prescription sap product identifier.
        /// </value>
        [DataMember]
        public string PrescriptionSapProductId { get; set; }

        /// <summary>
        /// Gets or sets the prescription product description.
        /// </summary>
        /// <value>
        /// The prescription product description.
        /// </value>
        [DataMember]
        public string PrescriptionProductDescription { get; set; }

        /// <summary>
        /// Gets or sets the prescription frequency.
        /// </summary>
        /// <value>
        /// The prescription frequency.
        /// </value>
        [DataMember]
        public string PrescriptionFrequency { get; set; }

        /// <summary>
        /// Gets or sets the prescription assessed pads per day.
        /// </summary>
        /// <value>
        /// The prescription assessed pads per day.
        /// </value>
        [DataMember]
        public string PrescriptionAssessedPadsPerDay { get; set; }

        /// <summary>
        /// Gets or sets the prescription actual pads per day.
        /// </summary>
        /// <value>
        /// The prescription actual pads per day.
        /// </value>
        [DataMember]
        public string PrescriptionActualPadsPerDay { get; set; }

        /// <summary>
        /// Gets or sets the prescription d q1.
        /// </summary>
        /// <value>
        /// The prescription d q1.
        /// </value>
        [DataMember]
        public string PrescriptionDQ1 { get; set; }

        /// <summary>
        /// Gets or sets the prescription d q2.
        /// </summary>
        /// <value>
        /// The prescription d q2.
        /// </value>
        [DataMember]
        public string PrescriptionDQ2 { get; set; }

        /// <summary>
        /// Gets or sets the prescription d q3.
        /// </summary>
        /// <value>
        /// The prescription d q3.
        /// </value>
        [DataMember]
        public string PrescriptionDQ3 { get; set; }

        /// <summary>
        /// Gets or sets the prescription d q4.
        /// </summary>
        /// <value>
        /// The prescription d q4.
        /// </value>
        [DataMember]
        public string PrescriptionDQ4 { get; set; }

        /// <summary>
        /// Gets or sets the prescription valid from date.
        /// </summary>
        /// <value>
        /// The prescription valid from date.
        /// </value>
        [DataMember]
        public string PrescriptionValidFromDate { get; set; }

        /// <summary>
        /// Gets or sets the prescription valid to date.
        /// </summary>
        /// <value>
        /// The prescription valid to date.
        /// </value>
        [DataMember]
        public string PrescriptionValidToDate { get; set; }

        /// <summary>
        /// Gets or sets the prescription status.
        /// </summary>
        /// <value>
        /// The prescription status.
        /// </value>
        [DataMember]
        public string PrescriptionStatus { get; set; }

        /// <summary>
        /// Gets or sets the prescription delivery date.
        /// </summary>
        /// <value>
        /// The prescription delivery date.
        /// </value>
        [DataMember]
        public string PrescriptionDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the prescription next delivery date.
        /// </summary>
        /// <value>
        /// The prescription next delivery date.
        /// </value>
        [DataMember]
        public string PrescriptionNextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the care home delivery frequency.
        /// </summary>
        /// <value>
        /// The care home delivery frequency.
        /// </value>
        [DataMember]
        public string CareHomeDeliveryFrequency { get; set; }

        /// <summary>
        /// Gets or sets the care home round identifier.
        /// </summary>
        /// <value>
        /// The care home round identifier.
        /// </value>
        [DataMember]
        public string CareHomeRoundId { get; set; }

        /// <summary>
        /// Gets or sets the care home address line1.
        /// </summary>
        /// <value>
        /// The care home address line1.
        /// </value>
        [DataMember]
        public string CareHomeAddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the care home address line2.
        /// </summary>
        /// <value>
        /// The care home address line2.
        /// </value>
        [DataMember]
        public string CareHomeAddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the care home city.
        /// </summary>
        /// <value>
        /// The care home city.
        /// </value>
        [DataMember]
        public string CareHomeCity { get; set; }

        /// <summary>
        /// Gets or sets the care home country.
        /// </summary>
        /// <value>
        /// The care home country.
        /// </value>
        [DataMember]
        public string CareHomeCountry { get; set; }

        /// <summary>
        /// Gets or sets the name of the care home house.
        /// </summary>
        /// <value>
        /// The name of the care home house.
        /// </value>
        [DataMember]
        public string CareHomeHouseName { get; set; }

        /// <summary>
        /// Gets or sets the care home house number.
        /// </summary>
        /// <value>
        /// The care home house number.
        /// </value>
        [DataMember]
        public string CareHomeHouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the care home post code.
        /// </summary>
        /// <value>
        /// The care home post code.
        /// </value>
        [DataMember]
        public string CareHomePostCode { get; set; }

        /// <summary>
        /// Gets or sets the carehome next delivery date.
        /// </summary>
        /// <value>
        /// The carehome next delivery date.
        /// </value>
        [DataMember]
        public string CarehomeNextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the carehome purchase order no.
        /// </summary>
        /// <value>
        /// The carehome purchase order no.
        /// </value>
        [DataMember]
        public string CarehomePurchaseOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the order sap delivery no.
        /// </summary>
        /// <value>
        /// The order sap delivery no.
        /// </value>
        [DataMember]
        public string OrderSAPDeliveryNo { get; set; }

        /// <summary>
        /// Gets or sets the order sap product identifier.
        /// </summary>
        /// <value>
        /// The order sap product identifier.
        /// </value>
        [DataMember]
        public string OrderSAPProductID { get; set; }

        /// <summary>
        /// Gets or sets the order product description.
        /// </summary>
        /// <value>
        /// The order product description.
        /// </value>
        [DataMember]
        public string OrderProductDescription { get; set; }

        /// <summary>
        /// Gets or sets the order quantity.
        /// </summary>
        /// <value>
        /// The order quantity.
        /// </value>
        [DataMember]
        public string OrderQuantity { get; set; }

        /// <summary>
        /// Gets or sets the order invoice no.
        /// </summary>
        /// <value>
        /// The order invoice no.
        /// </value>
        [DataMember]
        public string OrderInvoiceNo { get; set; }

        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        [DataMember]
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user sap customer no.
        /// </summary>
        /// <value>
        /// The user sap customer no.
        /// </value>
        [DataMember]
        public string UserSapCustomerNo { get; set; }

        /// <summary>
        /// Gets or sets the name of the user customer.
        /// </summary>
        /// <value>
        /// The name of the user customer.
        /// </value>
        [DataMember]
        public string UserCustomerName { get; set; }

        /// <summary>
        /// Gets or sets the name of the user role.
        /// </summary>
        /// <value>
        /// The name of the user role.
        /// </value>
        [DataMember]
        public string UserRoleName { get; set; }

        /// <summary>
        /// Gets or sets the name of the clinical contact role.
        /// </summary>
        /// <value>
        /// The name of the clinical contact role.
        /// </value>
        [DataMember]
        public string ClinicalContactRoleName { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact role identifier.
        /// </summary>
        /// <value>
        /// The clinical contact role identifier.
        /// </value>
        [DataMember]
        public string ClinicalContactRoleId { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact role value.
        /// </summary>
        /// <value>
        /// The clinical contact role value.
        /// </value>
        [DataMember]
        public string ClinicalContactRoleValue { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact modified by.
        /// </summary>
        /// <value>
        /// The clinical contact modified by.
        /// </value>
        [DataMember]
        public string ClinicalContactModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the clinical contact modified date.
        /// </summary>
        /// <value>
        /// The clinical contact modified date.
        /// </value>
        [DataMember]
        public string ClinicalContactModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the continence advisor.
        /// </summary>
        /// <value>
        /// The continence advisor.
        /// </value>
        [DataMember]
        public string ContinenceAdvisor { get; set; }

        /// <summary>
        /// Gets or sets the nurse.
        /// </summary>
        /// <value>
        /// The nurse.
        /// </value>
        [DataMember]
        public string Nurse { get; set; }

        /// <summary>
        /// Gets or sets the nurse base.
        /// </summary>
        /// <value>
        /// The nurse base.
        /// </value>
        [DataMember]
        public string NurseBase { get; set; }

        /// <summary>
        /// Gets or sets the health visitor.
        /// </summary>
        /// <value>
        /// The health visitor.
        /// </value>
        [DataMember]
        public string HealthVisitor { get; set; }

        /// <summary>
        /// Gets or sets the hc.
        /// </summary>
        /// <value>
        /// The hc.
        /// </value>
        [DataMember]
        public string HC { get; set; }

        /// <summary>
        /// Gets or sets the doctor.
        /// </summary>
        /// <value>
        /// The doctor.
        /// </value>
        [DataMember]
        public string Doctor { get; set; }

        /// <summary>
        /// Gets or sets the gp practice.
        /// </summary>
        /// <value>
        /// The gp practice.
        /// </value>
        [DataMember]
        public string GPPractice { get; set; }

        /// <summary>
        /// Gets or sets the caseload holder.
        /// </summary>
        /// <value>
        /// The caseload holder.
        /// </value>
        [DataMember]
        public string CaseloadHolder { get; set; }

        /// <summary>
        /// Gets or sets the po number.
        /// </summary>
        /// <value>
        /// The po number.
        /// </value>
        [DataMember]
        public string PONumber { get; set; }

        /// <summary>
        /// Gets or sets the typeof incontinence.
        /// </summary>
        /// <value>
        /// The typeof incontinence.
        /// </value>
        [DataMember]
        public string TypeofIncontinence { get; set; }

        /// <summary>
        /// Gets or sets the medical condition.
        /// </summary>
        /// <value>
        /// The medical condition.
        /// </value>
        [DataMember]
        public string MedicalCondition { get; set; }

        /// <summary>
        /// Gets or sets the school.
        /// </summary>
        /// <value>
        /// The school.
        /// </value>
        [DataMember]
        public string School { get; set; }

        /// <summary>
        /// Gets or sets the ethnicity.
        /// </summary>
        /// <value>
        /// The ethnicity.
        /// </value>
        [DataMember]
        public string Ethnicity { get; set; }

        /// <summary>
        /// Gets or sets the locality.
        /// </summary>
        /// <value>
        /// The locality.
        /// </value>
        [DataMember]
        public string Locality { get; set; }

        /// <summary>
        /// Gets or sets the sector.
        /// </summary>
        /// <value>
        /// The sector.
        /// </value>
        [DataMember]
        public string Sector { get; set; }

        /// <summary>
        /// Gets or sets the health centre.
        /// </summary>
        /// <value>
        /// The health centre.
        /// </value>
        [DataMember]
        public string HealthCentre { get; set; }

        /// <summary>
        /// Gets or sets the budget centre.
        /// </summary>
        /// <value>
        /// The budget centre.
        /// </value>
        [DataMember]
        public string BudgetCentre { get; set; }

        /// <summary>
        /// Gets or sets the is data protected.
        /// </summary>
        /// <value>
        /// The is data protected.
        /// </value>
        [DataMember]
        public string IsDataProtected { get; set; }

        /// <summary>
        /// Gets or sets the total rows.
        /// </summary>
        /// <value>
        /// The total rows.
        /// </value>
        [DataMember]
        public decimal TotalRows { get; set; }

        /// <summary>
        /// Gets or sets the patient Removed Stopped Date.
        /// </summary>
        /// <value>
        /// The patient Removed Stopped Date.
        /// </value>
        [DataMember]
        public string RemovedStoppedDateTime { get; set; }        

        /// <summary>
        /// Gets or sets the Plant.
        /// </summary>
        /// <value>
        /// The Plant.
        /// </value>
        [DataMember]
        public string Plant { get; set; }

        /// <summary>
        /// Gets or sets the BillingDate.
        /// </summary>
        /// <value>
        /// The Billing Date.
        /// </value>
        [DataMember]
        public string BillingDate { get; set; }

        /// <summary>
        /// Gets or sets the Currency.
        /// </summary>
        /// <value>
        /// The Currency.
        /// </value>
        [DataMember]
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the BillingQuantity.
        /// </summary>
        /// <value>
        /// The Billing Quantity.
        /// </value>
        [DataMember]
        public string BillingQuantity { get; set; }

        /// <summary>
        /// Gets or sets the BillingUnit.
        /// </summary>
        /// <value>
        /// The Billing Unit.
        /// </value>
        [DataMember]
        public string BillingUnit { get; set; }

        /// <summary>
        /// Gets or sets NetValue.
        /// </summary>
        /// <value>
        /// The Net Value.
        /// </value>
        [DataMember]
        public string NetValue { get; set; }

        /// <summary>
        /// Gets or sets the VatValue.
        /// </summary>
        /// <value>
        /// The Vat Value.
        /// </value>
        [DataMember]
        public string VatValue { get; set; }

		/// <summary>
		/// Gets or sets the IsExceptionOccurred.
		/// </summary>
		/// <value>
		/// The IsExceptionOccurred.
		/// </value>
		[DataMember]
		public string IsExceptionOccurred { get; set; }


    }
}
