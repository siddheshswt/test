﻿//-----------------------------------------------------------------------
//  <copyright file="PrescriptionBusinessModel.cs" company="SCA Indigo">
//  Copyright (c) SCA Indigo. All rights reserved.
//  </copyright>
//-----------------------------------------------------------------------
namespace SCA.Indigo.Business.BusinessModels
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Prescription Business Model
    /// </summary>
    [DataContract]
    public class PrescriptionBusinessModel : MassDataBusinessModel
    {

        /// <summary>
        /// Gets or sets Patient Id
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [DataMember]
        public long? PatientID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets the SAP Patient Id
        /// </summary>
        /// <value>
        /// The sap patient identifier.
        /// </value>
        [DataMember]
        public string SAPPatientID { get; set; }

        /// <summary>
        /// Gets or Sets the Old SAP Product Id
        /// </summary>
        /// <value>
        /// The old sap product identifier.
        /// </value>
        [DataMember]
        public string SAPProductId { get; set; }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string ProductName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Frequency
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        [DataMember]
        public long? Frequency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [DataMember]
        public string DeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// Old Product Next delivery date
        /// </summary>
        /// <value>
        /// Old Product Next delivery date
        /// </value>
        [DataMember]
        public string NextDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets Valid From Date
        /// </summary>
        /// <value>
        /// The valid from date.
        /// </value>
        [DataMember]
        public string ValidFromDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Valid To Date
        /// </summary>
        /// <value>
        /// The valid to date.
        /// </value>
        [DataMember]
        public string ValidToDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DataMember]
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Assessed Pads Per Day
        /// </summary>
        /// <value>
        /// The assessed pads per day.
        /// </value>
        [DataMember]
        public long? AssessedPadsPerDay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Actual Pads Per Day
        /// </summary>
        /// <value>
        /// The actual pads per day.
        /// </value>
        [DataMember]
        public long? ActualPadsPerDay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the actual PPD.
        /// </summary>
        /// <value>
        /// The actual PPD.
        /// </value>
        [DataMember]
        public double? ActualPPD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is Overridden
        /// </summary>
        /// <value>
        /// The is overridden.
        /// </value>
        [DataMember]
        public bool IsOverridden
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ1
        /// </summary>
        /// <value>
        /// The d q1.
        /// </value>
        [DataMember]
        public long? DQ1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ2
        /// </summary>
        /// <value>
        /// The d q2.
        /// </value>
        [DataMember]
        public long? DQ2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ3
        /// </summary>
        /// <value>
        /// The d q3.
        /// </value>
        [DataMember]
        public long? DQ3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DQ4
        /// </summary>
        /// <value>
        /// The d q4.
        /// </value>
        [DataMember]
        public long? DQ4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q1.
        /// </summary>
        /// <value>
        /// The flag d q1.
        /// </value>
        [DataMember]
        public bool? FlagDQ1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q2.
        /// </summary>
        /// <value>
        /// The flag d q2.
        /// </value>
        [DataMember]
        public bool? FlagDQ2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q3.
        /// </summary>
        /// <value>
        /// The flag d q3.
        /// </value>
        [DataMember]
        public bool? FlagDQ3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag d q4.
        /// </summary>
        /// <value>
        /// The flag d q4.
        /// </value>
        [DataMember]
        public bool? FlagDQ4
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index prescription product identifier.
        /// </summary>
        /// <value>
        /// The index prescription product identifier.
        /// </value>
        [DataMember]
        public long IDXPrescriptionProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Is Removed
        /// </summary>
        /// <value>
        /// The is removed.
        /// </value>
        [DataMember]
        public bool? IsRemoved
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Page
        /// </summary>
        /// <value>
        /// The page.
        /// </value>
        [DataMember]
        public int Page
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Prescription Authorization Approval Flag
        /// </summary>
        /// <value>
        /// The prescription authorization approval flag.
        /// </value>
        [DataMember]
        public bool? PrescriptionAuthorizationApprovalFlag
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Prescription Id
        /// </summary>
        /// <value>
        /// The prescription identifier.
        /// </value>
        [DataMember]
        public long? PrescriptionId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Display Id
        /// </summary>
        /// <value>
        /// The product display identifier.
        /// </value>
        [DataMember]
        public string ProductDisplayId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Id
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        [DataMember]
        public long? ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Rows
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        [DataMember]
        public int Rows
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Row Status
        /// </summary>
        /// <value>
        /// The row status.
        /// </value>
        [DataMember]
        public string RowStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Sales Unit
        /// </summary>
        /// <value>
        /// The sales unit.
        /// </value>
        [DataMember]
        public string SalesUnit
        {
            get;
            set;
        }  

        /// <summary>
        /// Gets or sets the change NDD.
        /// </summary>
        /// <value>
        /// The change NDD.
        /// </value>
        [DataMember]
        public string ChangeNDD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Product Approval Flag
        /// </summary>
        /// <value>
        /// The product approval flag.
        /// </value>
        [DataMember]
        public bool? ProductApprovalFlag
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PackSize
        /// </summary>
        /// <value>
        /// The size of the pack.
        /// </value>
        [DataMember]
        public string PackSize
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets ModifiedBy
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        [DataMember]
        public string ModifiedBy
        {
            get;
            set;
        } 

    }
}