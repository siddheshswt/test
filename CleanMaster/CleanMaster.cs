﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind
// Created          : 16-06-2015
// ***********************************************************************
// <copyright file="CleanMaster.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Clean uploaded file with specific interwal</summary>
// ***********************************************************************
namespace CleanMaster
{
    using System;
    using System.Configuration;
    using System.IO;

    /// <summary>
    /// This class use to delete all files.
    /// </summary>
    public class CleanMaster
    {
        #region CleanMaster Class
        /// <summary>
        /// Initializes a new instance of the CleanMaster Class
        /// </summary>
        public CleanMaster()
        {
            this.TempPath = @"\\" + Path.Combine(ConfigurationManager.AppSettings["IPAddress"].ToString(), ConfigurationManager.AppSettings["TempAddress"].ToString());
            this.UploadDirectory = ConfigurationManager.AppSettings["SharedLocation"].ToString();
            this.ServerPath = @"\\" + Path.Combine(ConfigurationManager.AppSettings["IPAddress"].ToString(), ConfigurationManager.AppSettings["ServerAddress"].ToString());
        }
        #endregion

        #region Class level variables

        /// <summary>
        /// Gets or sets TempPath
        /// </summary>
        public string TempPath { get; set; }

        /// <summary>
        /// Gets or sets UploadDirectory
        /// </summary>
        public string UploadDirectory { get; set; }

        /// <summary>
        /// Gets or sets ServerPath
        /// </summary>
        public string ServerPath { get; set; }
        #endregion

        #region Common Methods
        /// <summary>
        /// Delete files that were copies in virtual directory in Upload folder to send to client machine
        /// </summary>
        public void DeleteFilesFromVirtualDirectory()
        {
            var tempDataDirectory = Path.Combine(this.TempPath, this.UploadDirectory);
            if (Directory.Exists(tempDataDirectory))
            {
                Array.ForEach(Directory.GetFiles(tempDataDirectory), File.Delete);
            }
        }

        /// <summary>
        /// Delete audit log, automatic order criteria, report builder, patient type matrix, interaction, carehome, PDF log files
        /// </summary>
        /// <param name="csvFolderName">csvfile FolderName</param>
        public void DeleteFilesFromFolder(string csvFolderName)
        {
            // f=get all folder names to delete files
            string[] listSplit = csvFolderName.Split(',');
            var serverDirectory = string.Empty;
            var sharedLocationDirectory = string.Empty;

            if (listSplit != null && listSplit.Length > 0)
            {
                for (int i = 0; i < listSplit.Length; i++)
                {
                    if (Convert.ToString(listSplit[i]).Length > 0)
                    {
                        // build server directory base on folder name
                        serverDirectory = Path.Combine(this.UploadDirectory, listSplit[i]);
                        sharedLocationDirectory = Path.Combine(this.ServerPath, serverDirectory);

                        if (Directory.Exists(sharedLocationDirectory))
                        {
                            Array.ForEach(Directory.GetFiles(sharedLocationDirectory), File.Delete);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
