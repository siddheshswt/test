﻿// ***********************************************************************
// Assembly         : SCA.Indigo.Web.UI
// Author           : Arvind
// Created          : 16-06-2015
// ***********************************************************************
// <copyright file="Program.cs" company="Capgemini">
//     Capgemini. All rights reserved.
// </copyright>
// <summary>Startup class for clean master</summary>
// ***********************************************************************

namespace CleanMaster
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Startup class for clean master
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Delete the files generated as a part of front end operation and not required at all
        /// </summary>
        /// <param name="args">main args</param>
        public static void Main(string[] args)
        {
            try
            {
                var objCleanMaster = new CleanMaster();

                var allFolderNames = ConfigurationManager.AppSettings["AuditLogSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["AutomaticOrderCriteriaSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["ReportBuilderSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["PatientTypeMatrixSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationCarehome"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationCommunicationFormat"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationContactPerson"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationCustomerAnalysisInformation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationCustomerClinicalContacts"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationOrder"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationPatient"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationPatientClinicalContacts"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationPostCodeMatrix"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesLogSharedLocationPrescription"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MassChangesUploadsSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["MasterMassChangesDownloadSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["CustomerParameterSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["InteractionReportSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["CareHomeSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["PDFInteractionReportSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["PDFCareHomeReportSharedLocation"].ToString() + "," +
                                     ConfigurationManager.AppSettings["SampleOrderSharedLocation"].ToString();

                // Delete audit log, automatic order criteria, report builder, patient type matrix, interaction, carehome log files and PDF
                objCleanMaster.DeleteFilesFromFolder(allFolderNames);

                // Delete entire virtual directory
                objCleanMaster.DeleteFilesFromVirtualDirectory();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
