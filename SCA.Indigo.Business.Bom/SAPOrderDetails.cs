﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    public class SAPOrderDetails
    {
        [DataMember] //maxLength value="18"
        public String Basematerial { get; set; }
        [DataMember] //maxLength value="11"
        public String Quantity { get; set; }
        [DataMember] //maxLength value="3"
        public String Salesunit { get; set; }
        [DataMember] //maxLength value="8"
        public String Deliverydate { get; set; }
        [DataMember] //maxLength value="8"
        public String NDD { get; set; }
        [DataMember] //maxLength value="1"
        public String Freeoffcharge { get; set; }
    }
}
