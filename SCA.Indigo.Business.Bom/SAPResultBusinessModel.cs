﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    public class SAPResultBusinessModel
    {
        [DataMember]
        public static int CustomerAdded { get; set; }
        [DataMember]
        public static int ProductAdded { get; set; }
        
    }
}
