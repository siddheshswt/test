﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    public class SAPOrderHeaders
    {
        [DataMember] //maxLength value="2"
        public String OrderType { get; set; }
        [DataMember] //maxLength value="40"
        public String FrontendId { get; set; }
        [DataMember] //maxLength value="10"
        public String CustomerNo { get; set; }
        [DataMember] //maxLength value="4"
        public String SalesOrg { get; set; }
        [DataMember] //maxLength value="2"
        public String DistrChannel { get; set; }
        [DataMember] //maxLength value="2"
        public String Division { get; set; }
        [DataMember] //maxLength value="10"
        public String SapId { get; set; }
        [DataMember] //maxLength value="10"
        public String CarehomeId { get; set; }
        [DataMember] //maxLength value="1"
        public String Nocharge { get; set; }
        [DataMember] //maxLength value="1"
        public String Asaindicator { get; set; }
        [DataMember] //maxLength value="241"
        public String Email { get; set; }
        [DataMember] //maxLength value="10"
        public String Billto { get; set; }
    }
}
