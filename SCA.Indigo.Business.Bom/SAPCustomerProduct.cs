﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    public class SAPProduct:BaseBusinessModel
    {
        [DataMember] 
        public long CustomerId { get; set; }

        [DataMember] //maxLength value="10"
        public long ProductId { get; set; }
        [DataMember] //maxLength value="2"
        public String Customertype { get; set; }
        [DataMember] //maxLength value="10"
        public String SAPId { get; set; }
        [DataMember] //maxLength value="18"
        public String SAPProductId { get; set; }
        [DataMember] //maxLength value="18"
        public String Basematerial { get; set; }
        [DataMember] //maxLength value="40"
        public String SAPProductName { get; set; }
        [DataMember] //maxLength value="15"
        public String Netvalue { get; set; }
        [DataMember] //maxLength value="13"
        public String VAT { get; set; }
        [DataMember] //maxLength value="3"
        public String Salesunit { get; set; }
        [DataMember] //maxLength value="5"
        public String Packsize { get; set; }
    }
}
