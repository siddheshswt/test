﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Indigo.Business.BusinessModels
{
    public class SAPCustomer:BaseBusinessModel
    {
        [DataMember]
        public long CustomerId { get; set; }
        [DataMember] //maxLength value="10"
        public String CustomerNo { get; set; }
        [DataMember] //maxLength value="4"
        public String SalseOrg { get; set; }
        [DataMember] //maxLength value="2"
        public String Distrchannel { get; set; }
        [DataMember] //maxLength value="2"
        public String Division { get; set; }
        [DataMember] //maxLength value="35"
        public String CustomerName { get; set; }
        [DataMember] //maxLength value="35"
        public String Street { get; set; }
        [DataMember] //maxLength value="10"
        public String Postcode { get; set; }
        [DataMember] //maxLength value="35"
        public String City { get; set; }
        [DataMember] //maxLength value="3"
        public String County { get; set; }
        [DataMember] //maxLength value="3"
        public String Country { get; set; }
        [DataMember] //maxLength value="10"
        public String Haulier { get; set; }
    }
}
